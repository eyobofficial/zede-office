# -*- coding: utf-8 -*-
# Generated by Django 1.11.16 on 2018-10-27 08:17
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('intakes', '0003_intakereportfile_report_id'),
    ]

    operations = [
        migrations.AlterField(
            model_name='intakereminder',
            name='date',
            field=models.DateField(),
        ),
    ]
