import requests
from django.conf import settings


class ChapterConsumer:

    def __init__(self):
        self.url = '{}/api/e-learning/chapters'.format(settings.DEBATNL_ONLINE_API)
        self.headers = {
            'Authorization': 'Token {}'.format(settings.DEBATNL_ONLINE_API_KEY)
        }

    def retrieve_all(self):
        url = '{}/'.format(self.url)
        response = requests.get(url, headers=self.headers)
        return response.json()

    def retrieve_object(self, object_id):
        url = '{}/{}/'.format(self.url, object_id)
        response = requests.get(url, headers=self.headers)
        return response.json()

    def update_object(self, object_id, data):
        url = '{}/{}/'.format(self.url, object_id)

        payload = {
            'id': object_id,
            'name': data.get('name'),
            'sequence': data.get('sequence'),
            'is_deleted': data.get('is_deleted', False)
        }

        response = requests.put(url, headers=self.headers, data=payload)
        return response.json()

    def create_object(self, data):
        url = '{}/'.format(self.url)

        payload = {
            'course': data.get('course'),
            'name': data.get('name'),
            'sequence': data.get('sequence'),
        }

        response = requests.post(url, headers=self.headers, data=payload)
        return response.json()
