import datetime

from django.core.validators import validate_email, ValidationError
from django.db.models import Q
from django.shortcuts import get_object_or_404

from jotform.models import Answer, Submission
from handouts.models import HandoutRequest
from webcrm.models import Delivery


class HandoutRequestGenerator:
    question_ids = {
        'trainee_name': 7,
        'trainee_email': 11,
        'training_date': 10,
        'trainer': 13,
        'handout': 14
    }
    handout_question_value = 'Ja, ik wil een handout ontvangen'

    def _get_submission_trainee_name(self, submission):
        """
        Get `trainee_name` of a submission
        """
        trainee_name = get_object_or_404(
            Answer,
            submission=submission,
            qid=self.question_ids['trainee_name']
        )
        return trainee_name.answer

    def _get_submission_trainee_email(self, submission):
        """
        Get `trainee_email` of a submission
        """
        trainee_email = get_object_or_404(
            Answer,
            submission=submission,
            qid=self.question_ids['trainee_email']
        )
        return trainee_email.answer

    def _get_submission_trainer(self, submission):
        """
        Get `trainer` of a submission
        """
        trainer = get_object_or_404(
            Answer,
            submission=submission,
            qid=self.question_ids['trainer']
        )
        return eval(trainer.answer)[0]

    def _get_submission_training_date(self, submission):
        """
        Get `training_date` of a submission
        """
        training_date = get_object_or_404(
            Answer,
            submission=submission,
            qid=self.question_ids['training_date']
        )
        training_date = eval(training_date.answer)
        training_date = datetime.date(
            int(training_date['year']),
            int(training_date['month']),
            int(training_date['day'])
        )
        return training_date

    def _generate_submission_handout_request(self, submission):
        """
        Create a handout request for a submission
        """
        training_date = self._get_submission_training_date(submission)
        trainee_email = self._get_submission_trainee_email(submission)

        # Generate handout only if trainee have a valid e-mail
        try:
            validate_email(trainee_email)
        except ValidationError:
            return

        kwargs = {
            'trainee_name': self._get_submission_trainee_name(submission),
            'trainee_email': self._get_submission_trainee_email(submission),
            'trainer': self._get_submission_trainer(submission),
            'training_date': training_date,
            'submitted_at': submission.created_at
        }
        handout_request, created = HandoutRequest.objects.get_or_create(
            submission_id=submission.pk,
            defaults=kwargs
        )

        if created:
            handout_request.set_corresponding_deliveries()
            handout_request.determine_status()
            handout_request.send_notification_based_on_status()

    @staticmethod
    def _get_corresponding_deliveries(training_date):
        return Delivery.objects.filter(
            order_date__date=training_date
        ).exclude(
            Q(product='Digitale Intake') |
            Q(product='Debat.NL Online') |
            Q(product='SkillsTracker')
        )

    def generate(self):
        """
        Create handout requests for the handout submissions
        """
        handout_submissions = Submission.objects.filter(
            answers__qid=self.question_ids['handout'],
            answers__answer__contains=self.handout_question_value
        )
        for submission in handout_submissions:
            self._generate_submission_handout_request(submission)
