from django.conf import settings
from django.conf.urls import url, include
from django.contrib import admin
from django.conf.urls.static import static
from django.contrib.auth.views import PasswordResetView
from django.urls import reverse_lazy
from django.views.generic import RedirectView

urlpatterns = []
password_reset_kwargs = {
    'html_email_template_name': 'users/emails/reset_password_html_email.html',
    'from_email': 'Debat.NL Olaf <olaf@debat.nl>'
}

if settings.DEBUG:
    urlpatterns += static(
        settings.MEDIA_URL,
        document_root=settings.MEDIA_ROOT
    )

urlpatterns += [
    url(
        r'^password_reset/$',
        PasswordResetView.as_view(**password_reset_kwargs),
        name='password_reset'
    ),
    url(r'^', include('django.contrib.auth.urls')),
    url(r'^admin/', admin.site.urls),
    url(r'^users/', include('users.urls', namespace='users')),
    url(
        r'^configuration/',
        include('backoffice.urls', namespace='configuration')
    ),
    url(
        r'^debatnl-online/',
        include('debatnl_online.urls.urls', namespace='debatnl_online')
    ),
    url(
        r'^debatnl-registration/',
        include('debatnl_registration.urls', namespace='debatnl_registration')
    ),
    url(r'^scans/', include('scans.urls', namespace='scans')),
    url(r'^feedback/', include('feedback.urls', namespace='feedback')),
    url(r'^offers/', include('offers.urls', namespace='offers')),
    url(r'^intakes/', include('intakes.urls', namespace='intakes')),
    url(r'^briefings/', include('briefings.urls', namespace='briefings')),
    url(r'^follow-ups/', include('follow_ups.urls', namespace='follow_ups')),
    url(r'^$', RedirectView.as_view(url=reverse_lazy('feedback:overview')),
        name='main_page'),
    url(r'^planning/', include('planning.urls', namespace='planning')),
    url(r'^schedules/', include('schedules.urls', namespace='schedules')),
    url(r'^ccd/', include('ccd.urls', namespace='ccd')),
    url(
        r'^debatnl-mobile/',
        include('debatnl_mobile.urls', namespace='debatnl_mobile')
    ),
    url(r'^marketing/', include('marketing.urls.urls', namespace='marketing')),
    url(r'^handouts/', include('handouts.urls', namespace='handouts')),
    url(
        r'^declarations/',
        include('declarations.urls', namespace='declarations')
    ),
    url(r'^salaries/', include('salaries.urls', namespace='salaries')),
    url(r'^education/', include('education.urls', namespace='education')),
    url(r'^tinymce/', include('tinymce.urls'))
]
