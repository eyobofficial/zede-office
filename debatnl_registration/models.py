import uuid

from django.contrib.auth.models import User
from django.db import models
from django.utils.translation import ugettext_lazy as _

from backoffice.models import Training, Employee


class Registration(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    form_id = models.IntegerField()
    submission_id = models.IntegerField()
    training = models.CharField(blank=True, default='', max_length=200)
    duration = models.IntegerField(null=True, blank=True)
    participant_count = models.IntegerField(null=True, blank=True)
    registrant_first_name = models.CharField(blank=True, default='',
                                             max_length=200)
    registrant_last_name = models.CharField(blank=True, default='',
                                            max_length=200)
    registrant_email = models.CharField(blank=True, default='', max_length=200)
    registrant_phone = models.CharField(blank=True, default='', max_length=200)
    is_valid = models.BooleanField(default=False)

    def __str__(self):
        return '{} {} - {}'.format(
            self.registrant_first_name,
            self.registrant_last_name,
            self.training
        )


class Participant(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(blank=True, default='', max_length=200)
    phone = models.CharField(blank=True, default='', max_length=200)
    email = models.CharField(blank=True, default='', max_length=200)
    dietary_wishes = models.TextField(blank=True, default='')
    registration = models.ForeignKey(Registration, related_name='participants')
    has_feedback = models.BooleanField(default=False)

    def __str__(self):
        return self.name

    def has_unattended_trainings(self):
        kwargs = {'participant': self, 'has_attended': False}
        return Attendance.objects.filter(**kwargs).exists()


class TrainingDate(models.Model):
    PENDING = 'PENDING'
    FINISHED = 'FINISHED'
    CANCELLED = 'CANCELLED'

    STATUS_CHOICES = (
        (PENDING, 'Pending'),
        (FINISHED, 'Finished'),
        (CANCELLED, 'Cancelled'),
    )

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    date = models.DateField(db_index=True)
    sequence = models.IntegerField(null=True, db_index=True)
    old_trainers = models.ManyToManyField(Employee)
    trainers = models.ManyToManyField(User)
    planned_training = models.ForeignKey('PlannedTraining',
                                         related_name='training_dates')
    attendees = models.ManyToManyField(Participant, through='Attendance')
    need_large_hall = models.BooleanField(default=False)
    status = models.CharField(max_length=30, default='PENDING',
                              choices=STATUS_CHOICES)

    class Meta:
        ordering = ['date']

    def __str__(self):
        if self.planned_training:
            name = '{:%Y-%m-%d} ({})'.format(
                self.date, self.planned_training.training.name)
        else:
            name = '{:%Y-%m-%d} (--)'.format(self.date)

        return name

    def cancel(self):
        self.status = TrainingDate.CANCELLED
        return self.save()


class Attendance(models.Model):
    participant = models.ForeignKey(Participant, on_delete=models.CASCADE)
    training_date = models.ForeignKey(TrainingDate, on_delete=models.CASCADE)
    has_attended = models.BooleanField(default=False)


class PlannedTraining(models.Model):
    AVAILABLE = 'AVAILABLE'
    NOT_AVAILABLE = 'NOT AVAILABLE'
    PENDING = 'PENDING'

    HALL_STATUS_CHOICES = (
        (AVAILABLE, 'Available'),
        (NOT_AVAILABLE, 'Not Available'),
        (PENDING, 'Pending')
    )

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    training = models.ForeignKey(Training, on_delete=models.PROTECT)
    hall_status = models.CharField(max_length=30, default='PENDING',
                                   choices=HALL_STATUS_CHOICES)

    def __str__(self):
        return '{} | {} day(s)'.format(
            self.training.name,
            self.training_dates.count()
        )


class PlannedTrainingToken(models.Model):
    HALL_AVAILABLE = 'HALL AVAILABLE'
    UPDATE_WEBSITE = 'UPDATE WEBSITE'
    BOOK_HALL = 'BOOK HALL'
    CANCEL_HALL = 'CANCEL HALL'
    CANCEL_TRAINING_PARTICIPANT = 'CANCEL TRAINING PARTICIPANT'
    ATTENDANCE_LIST = 'ATTENDANCE LIST'
    ATTENDEES_AMOUNT = 'ATTENDEES AMOUNT'

    TYPE_CHOICES = (
        (HALL_AVAILABLE, 'Hall available'),
        (UPDATE_WEBSITE, 'Update website'),
        (BOOK_HALL, 'Book hall'),
        (CANCEL_HALL, 'Cancel hall'),
        (CANCEL_TRAINING_PARTICIPANT, 'Cancel training participant'),
        (ATTENDANCE_LIST, 'Attendance list'),
        (ATTENDEES_AMOUNT, 'Attendees amount'),
    )

    key = models.UUIDField(default=uuid.uuid4, editable=False, db_index=True)
    created_at = models.DateTimeField(auto_now_add=True)
    planned_training = models.ForeignKey(PlannedTraining)
    training_date = models.ForeignKey(TrainingDate, null=True, blank=True)
    participant = models.ForeignKey(Participant, null=True, blank=True)
    type = models.CharField(max_length=30, default='', choices=TYPE_CHOICES)

    class Meta:
        verbose_name = _('Token')
        verbose_name_plural = _('Tokens')


class SpringestFeedItem(models.Model):
    id = models.CharField(max_length=50, primary_key=True)
    category = models.CharField(max_length=100, default='', blank=True)
    duration = models.IntegerField(
        default=1,
        help_text='Set duration in days'
    )
    description = models.TextField(default='', blank=True)
    website = models.CharField(max_length=200, default='', blank=True)
    price = models.DecimalField(default=0, decimal_places=2, max_digits=6)
    vat_percentage = models.DecimalField(default=0, decimal_places=2,
                                         max_digits=6)
    training = models.ForeignKey(Training, null=True, on_delete=models.PROTECT,
                                 related_name='springest')

    def vat_amount(self):
        return round((self.price / 100) * self.vat_percentage, 2)


class BloomvilleFeedItem(models.Model):
    id = models.CharField(max_length=50, primary_key=True)
    category = models.CharField(max_length=100, default='', blank=True)
    duration = models.IntegerField(
        default=8,
        help_text='Set duration in hours'
    )
    description = models.TextField(default='', blank=True)
    course_format = models.TextField(default='', blank=True)
    price = models.DecimalField(default=0, decimal_places=2, max_digits=6)
    vat_percentage = models.DecimalField(default=21, decimal_places=2,
                                         max_digits=6)
    training = models.ForeignKey(Training, null=True, on_delete=models.PROTECT,
                                 related_name='bloomville')

    def localized_price(self):
        return str(self.price).replace('.', ',')

    def vat_amount(self):
        amount = round((self.price / 100) * self.vat_percentage, 2)
        return str(amount).replace('.', ',')

    def get_classes(self):
        qs = TrainingDate.objects
        qs = qs.filter(planned_training__training=self.training)
        qs = qs.filter(planned_training__hall_status=PlannedTraining.AVAILABLE)
        qs = qs.filter(sequence=0)
        qs = qs.exclude(status=TrainingDate.CANCELLED)
        return qs.all()
