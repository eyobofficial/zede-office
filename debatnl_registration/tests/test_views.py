import datetime

import mock
from django.contrib.auth.models import User
from django.test import TestCase, Client
from django.urls import reverse
from post_office.models import Email

from backoffice.constants import GROUP_DEBATNLREGISTRATION
from backoffice.tests.factories import TrainingFactory
from debatnl_registration.models import Registration, Participant, \
    TrainingDate, PlannedTraining, PlannedTrainingToken, Attendance
from debatnl_registration.tests.factories import TrainingDateFactory, \
    PlannedTrainingFactory, ParticipantFactory, RegistrationFactory, \
    AttendanceFactory
from shared.enums import RoleName
from users.models import AppPermission, Role
from users.tests.factories import UserFactory


class HooksViewTest(TestCase):
    """
    Test the hook view.
    """
    fixtures = ['roles']

    def setUp(self):
        role = Role.objects.get(name=RoleName.ADMIN.value)
        self.administrator = UserFactory()
        self.administrator.profile.roles.add(role)
        self.client = Client()

        training = TrainingFactory(name='Overtuigend Debatteren')
        planned_training = PlannedTrainingFactory(training=training)
        TrainingDateFactory(
            planned_training=planned_training,
            date=datetime.date(2018, 10, 2)
        )
        TrainingDateFactory(
            planned_training=planned_training,
            date=datetime.date(2018, 11, 6)
        )
        TrainingDateFactory(
            planned_training=planned_training,
            date=datetime.date(2018, 12, 4)
        )

    def test_post_request_for_training_registration(self):
        """
        Test a POST request to the hook view.
        """
        payload = {
            'entry[id]': '1962', 'entry[form_id]': '39',
            'entry[status]': 'active',
            'entry[1]': 'Overtuigend Debatteren', 'entry[40]': '3dagen|599',
            'entry[29]': '2 oktober 2018, 6 november 2018 en '
                         '4 december 2018 (Utrecht)',
            'entry[119]': '2', 'entry[114]': '2', 'entry[51]': 'Naam A',
            'entry[52]': 'naama@test.mail', 'entry[64]': '123456789',
            'entry[120]': 'Vegan',
            'entry[87]': 'Naam B', 'entry[84]': 'naamb@test.mail',
            'entry[89]': '123456789',
            'entry[125]': 'Geen', 'entry[111]': '2',
            'entry[110]': 'Ja, ik begin graag vooraf al met de theorie '
                          '(+€ 49 ex. BTW)|49',
            'entry[8]': 'Naam C', 'entry[9]': 'Achternaam C',
            'entry[11]': 'naamc@test.mail',
            'entry[10]': '123456789', 'entry[12]': 'Bedrijf X',
            'entry[13]': 'Factuuradres 123A',
            'entry[14]': '1234AB', 'entry[15]': 'Den Haag',
            'entry[16]': 'Geen opmerkingen',
            'entry[59]': '1071.0743801653', 'entry[79.1]': 'BTW',
            'entry[79.2]': '272,16  &#8364;',
            'entry[79.3]': '1', 'entry[61]': '1296', 'entry[4]': '',
            'entry[19]': '',
            'entry[22]': '', 'entry[25]': '', 'entry[28]': '', 'entry[27]': '',
            'entry[30]': '',
            'entry[78]': '', 'entry[6]': '', 'entry[104]': '',
            'entry[116]': '', 'entry[80]': '',
            'entry[82]': '', 'entry[86]': '', 'entry[91]': '', 'entry[88]': '',
            'entry[85]': '',
            'entry[122]': '', 'entry[90]': '', 'entry[83]': '',
            'entry[96]': '', 'entry[97]': '',
            'entry[121]': '', 'entry[94]': '', 'entry[95]': '',
            'entry[100]': '', 'entry[101]': '',
            'entry[124]': '', 'entry[98]': '', 'entry[99]': '',
            'entry[92]': '', 'entry[93]': '',
            'entry[123]': '', 'entry[103]': '', 'entry[109]': '',
            'entry[117]': '',
            'entry[108]': '', 'entry[55]': '', 'entry[118]': '',
            'entry[36]': '', 'entry[76]': ''
        }

        response = self.client.post('/debatnl-registration/hooks/', payload)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(Registration.objects.count(), 1)
        self.assertEqual(Participant.objects.count(), 2)
        self.assertEqual(TrainingDate.objects.count(), 3)
        self.assertEqual(Attendance.objects.count(), 6)
        self.assertEqual(Email.objects.count(), 2)

    def test_post_request_for_additional_2_day_training_registration(self):
        """
        Test a POST request to the hook view.
        """
        payload = {
            'entry[id]': '1962', 'entry[form_id]': '45',
            'entry[status]': 'active',
            'entry[1]': 'Overtuigend Debatteren',
            'entry[29]': '6 november 2018 en 4 december 2018 (Utrecht)',
            'entry[119]': '2', 'entry[114]': '2', 'entry[51]': 'Naam A',
            'entry[52]': 'naama@test.mail', 'entry[64]': '123456789',
            'entry[120]': 'Vegan',
            'entry[87]': 'Naam B', 'entry[84]': 'naamb@test.mail',
            'entry[89]': '123456789',
            'entry[125]': 'Geen', 'entry[111]': '2',
            'entry[110]': 'Ja, ik begin graag vooraf al met de theorie '
                          '(+€ 49 ex. BTW)|49',
            'entry[8]': 'Naam C', 'entry[9]': 'Achternaam C',
            'entry[11]': 'naamc@test.mail',
            'entry[10]': '123456789', 'entry[12]': 'Bedrijf X',
            'entry[13]': 'Factuuradres 123A',
            'entry[14]': '1234AB', 'entry[15]': 'Den Haag',
            'entry[16]': 'Geen opmerkingen',
            'entry[59]': '1071.0743801653', 'entry[79.1]': 'BTW',
            'entry[79.2]': '272,16  &#8364;',
            'entry[79.3]': '1', 'entry[61]': '1296', 'entry[4]': '',
            'entry[19]': '',
            'entry[22]': '', 'entry[25]': '', 'entry[28]': '', 'entry[27]': '',
            'entry[30]': '',
            'entry[78]': '', 'entry[6]': '', 'entry[104]': '',
            'entry[116]': '', 'entry[80]': '',
            'entry[82]': '', 'entry[86]': '', 'entry[91]': '', 'entry[88]': '',
            'entry[85]': '',
            'entry[122]': '', 'entry[90]': '', 'entry[83]': '',
            'entry[96]': '', 'entry[97]': '',
            'entry[121]': '', 'entry[94]': '', 'entry[95]': '',
            'entry[100]': '', 'entry[101]': '',
            'entry[124]': '', 'entry[98]': '', 'entry[99]': '',
            'entry[92]': '', 'entry[93]': '',
            'entry[123]': '', 'entry[103]': '', 'entry[109]': '',
            'entry[117]': '',
            'entry[108]': '', 'entry[55]': '', 'entry[118]': '',
            'entry[36]': '', 'entry[76]': ''
        }

        response = self.client.post('/debatnl-registration/hooks/', payload)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(Registration.objects.count(), 1)
        self.assertEqual(Participant.objects.count(), 2)
        self.assertEqual(TrainingDate.objects.count(), 3)
        self.assertEqual(Attendance.objects.count(), 4)
        self.assertEqual(Email.objects.count(), 2)


class AddTrainingViewTest(TestCase):
    fixtures = ['roles', 'app_permissions']

    def setUp(self):
        role = Role.objects.get(name=RoleName.ADMIN.value)
        self.administrator = UserFactory()
        self.administrator.profile.roles.add(role)

        role = Role.objects.get(name=RoleName.HALL.value)
        hall_contact = UserFactory()
        hall_contact.profile.roles.add(role)

        role = Role.objects.get(name=RoleName.TRAINER.value)
        trainers = UserFactory.create_batch(3)

        trainer_ids = []
        for trainer in trainers:
            trainer.profile.roles.add(role)
            trainer_ids.append(trainer.pk)

        training = TrainingFactory()

        self.user = User.objects.create_user(username='test_user')
        app_permission = AppPermission.objects.get(
            app_name=GROUP_DEBATNLREGISTRATION)
        self.user.profile.app_permissions.add(app_permission)
        self.client = Client()

        self.url = reverse('debatnl_registration:add')
        self.payload = {
            'training': training.id,
            'training_dates-TOTAL_FORMS': 3,
            'training_dates-INITIAL_FORMS': 0,
            'training_dates-MIN_NUM_FORMS': 1,
            'training_dates-MAX_NUM_FORMS': 1,
            'training_dates-0-date': '2018-01-01',
            'training_dates-0-trainer': trainer_ids[0],
            'training_dates-1-date': '2018-03-03',
            'training_dates-1-trainer': trainer_ids[:-1],
            'training_dates-2-date': '2018-04-04',
            'training_dates-2-trainer': trainer_ids
        }

    def test_POST_request_as_anonymous_user(self):
        """
        Test a request to the view from an anonymous user.
        """
        response = self.client.post(self.url, self.payload, follow=True)
        expected_url = f'{reverse("login")}?next=/feedback/'
        self.assertRedirects(response, expected_url)

    def test_POST_request_as_authenticated_user(self):
        """
        Test a request to the view from an authenticated user.
        """

        # Perform request.
        self.client.force_login(self.user)
        response = self.client.post(self.url, self.payload)

        # Assert outcome for the request.
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.url, '/debatnl-registration/')
        self.assertEqual(PlannedTraining.objects.count(), 1)
        self.assertEqual(TrainingDate.objects.count(), 3)
        self.assertEqual(PlannedTrainingToken.objects.count(), 1)
        self.assertEqual(Email.objects.count(), 1)


class HallAvailabilityView(TestCase):
    fixtures = ['roles']

    def setUp(self):
        for name in [RoleName.ADMIN.value, RoleName.HALL.value,
                     RoleName.WEBSITE.value]:
            role = Role.objects.get(name=name)
            user = UserFactory()
            user.profile.roles.add(role)

        role = Role.objects.get(name=RoleName.TRAINER.value)
        trainer = UserFactory()
        trainer.profile.roles.add(role)

        self.training_date = TrainingDateFactory()
        self.training_date.trainers.add(trainer)
        self.token = PlannedTrainingToken.objects.create(
            planned_training=self.training_date.planned_training,
            type='BOOK HALL'
        )
        self.planned_training = self.token.planned_training

    def test_request_with_accept_parameter(self):
        url = '/debatnl-registration/hall-availability/?' \
              'token={}&rsvp=ACCEPT'.format(self.token.key)
        response = self.client.get(url)

        self.planned_training.refresh_from_db()
        self.assertEqual(response.status_code, 200)
        self.assertEqual(self.planned_training.hall_status, 'AVAILABLE')
        self.assertEqual(PlannedTrainingToken.objects.count(), 2)
        self.assertEqual(
            PlannedTrainingToken.objects.filter(type='UPDATE WEBSITE').count(),
            1
        )
        self.assertEqual(
            PlannedTrainingToken.objects.filter(type='HALL AVAILABLE').count(),
            1
        )
        self.assertEqual(Email.objects.count(), 2)
        self.assertNotEqual(PlannedTrainingToken.objects.first(), self.token)
        self.assertTemplateUsed(
            response,
            'debatnl_registration/public/confirmation.html'
        )

    def test_request_with_deny_parameter(self):
        url = '/debatnl-registration/hall-availability/?' \
              'token={}&rsvp=DENY'.format(self.token.key)
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(PlannedTraining.objects.count(), 0)
        self.assertEqual(PlannedTrainingToken.objects.count(), 0)
        self.assertEqual(Email.objects.count(), 2)
        self.assertTemplateUsed(
            response,
            'debatnl_registration/public/confirmation.html'
        )


class ConfirmJOTFormUpdateView(TestCase):
    def setUp(self):
        self.training_date = TrainingDateFactory()
        self.token = PlannedTrainingToken.objects.create(
            planned_training=self.training_date.planned_training
        )

    def test_request(self):
        url = '/debatnl-registration/confirm/jotform-update/?' \
              'token={}'.format(self.token.key)
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(PlannedTrainingToken.objects.count(), 0)
        self.assertTemplateUsed(
            response,
            'debatnl_registration/public/confirmation.html'
        )


class AddTrainerViewTest(TestCase):
    fixtures = ['roles', 'app_permissions']

    def setUp(self):
        self.training_date = TrainingDateFactory()

        role = Role.objects.get(name=RoleName.TRAINER.value)
        trainers = UserFactory.create_batch(4)
        for trainer in trainers:
            trainer.profile.roles.add(role)

        self.training_date.trainers.add(trainers[0])

        self.user = User.objects.create_user(username='test_user')
        app_permission = AppPermission.objects.get(
            app_name=GROUP_DEBATNLREGISTRATION)
        self.user.profile.app_permissions.add(app_permission)
        self.client = Client()

        self.payload = {}
        for index, trainer in enumerate(trainers[1:]):
            key = '{}_trainer_{}'.format(self.training_date.id, index)
            self.payload[key] = trainer.pk

    def test_POST_request_as_anonymous_user(self):
        """
        Test a request to the view from an anonymous user.
        """
        url = '/debatnl-registration/ongoing-trainings/{}/trainers/' \
              'add/'.format(self.training_date.id)
        response = self.client.post(url, self.payload)

        expected_url = f'{reverse("login")}?next={url}'
        self.assertRedirects(response, expected_url)

    def test_POST_request_as_authenticated_user(self):
        """
        Test a request to the view from an authenticated user.
        """

        # Perform request.
        url = '/debatnl-registration/ongoing-trainings/{}/trainers/' \
              'add/'.format(self.training_date.id)
        self.client.force_login(self.user)
        response = self.client.post(url, self.payload)

        self.training_date.refresh_from_db()

        # Assert outcome for the request.
        self.assertEqual(response.status_code, 200)
        self.assertEqual(TrainingDate.objects.count(), 1)
        self.assertEqual(self.training_date.trainers.count(), 3)


@mock.patch('debatnl_registration.views.TrainingDateDelete.'
            'remove_training_dates_from_gravity_forms')
class CancelTrainingDateViewTest(TestCase):
    fixtures = ['roles', 'app_permissions']

    def setUp(self):
        for name in [RoleName.ADMIN.value, RoleName.HALL.value]:
            role = Role.objects.get(name=name)
            user = UserFactory()
            user.profile.roles.add(role)

        self.planned_training = PlannedTrainingFactory()
        TrainingDateFactory(sequence=0, planned_training=self.planned_training)
        TrainingDateFactory(sequence=1, planned_training=self.planned_training)
        TrainingDateFactory(sequence=2, planned_training=self.planned_training)

        self.user = User.objects.create_user(username='test_user')
        app_permission = AppPermission.objects.get(
            app_name=GROUP_DEBATNLREGISTRATION)
        self.user.profile.app_permissions.add(app_permission)
        self.client = Client()
        self.path = '/debatnl-registration/ongoing-trainings'

    def test_POST_request_as_anonymous_user(self, mock_obj):
        """
        Test a request to the view from an anonymous user.
        """
        training_date = self.planned_training.training_dates.get(sequence=0)
        url = f'{self.path}/{training_date.id}/cancel/'
        response = self.client.post(url, follow=True)
        expected_url = f'{reverse("login")}?next=/feedback/'
        self.assertRedirects(response, expected_url)

    def test_POST_request_with_1st_training_date(self, mock_obj):
        """
        Test a request to the view from an authenticated user.
        """
        training_date = self.planned_training.training_dates.get(sequence=0)

        # Perform request.
        self.client.force_login(self.user)
        response = self.client.post(f'{self.path}/{training_date.id}/cancel/')

        training_date.refresh_from_db()

        # Assert outcome for the request.
        cancelled_count = TrainingDate.objects.filter(
            status=TrainingDate.CANCELLED).count()
        pending_count = TrainingDate.objects.filter(
            status=TrainingDate.PENDING).count()

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.url, '/debatnl-registration/')
        self.assertEqual(cancelled_count, 3)
        self.assertEqual(pending_count, 0)
        self.assertEqual(training_date.status, TrainingDate.CANCELLED)
        self.assertEqual(PlannedTrainingToken.objects.count(), 1)
        self.assertEqual(Email.objects.count(), 1)

    def test_POST_request_with_2nd_training_date(self, mock_obj):
        training_date = self.planned_training.training_dates.get(sequence=1)

        # Perform request.
        self.client.force_login(self.user)
        response = self.client.post(f'{self.path}/{training_date.id}/cancel/')

        training_date.refresh_from_db()

        # Assert outcome for the request.
        cancelled_count = TrainingDate.objects.filter(
            status=TrainingDate.CANCELLED).count()
        pending_count = TrainingDate.objects.filter(
            status=TrainingDate.PENDING).count()

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.url, '/debatnl-registration/')
        self.assertEqual(TrainingDate.objects.count(), 3)
        self.assertEqual(cancelled_count, 1)
        self.assertEqual(pending_count, 2)
        self.assertEqual(training_date.status, TrainingDate.CANCELLED)
        self.assertEqual(PlannedTrainingToken.objects.count(), 0)
        self.assertEqual(Email.objects.count(), 0)


class AvailableTrainingDatesViewTest(TestCase):
    fixtures = ['app_permissions']

    def setUp(self):
        self.planned_training = PlannedTrainingFactory()
        self.training_dates = TrainingDateFactory.create_batch(
            4,
            planned_training=self.planned_training,
            date=datetime.datetime.now().date()
        )
        self.user = User.objects.create_user(username='test_user')
        app_permission = AppPermission.objects.get(
            app_name=GROUP_DEBATNLREGISTRATION)
        self.user.profile.app_permissions.add(app_permission)
        self.client = Client()

    def test_GET_request_as_anonymous_user(self):
        """
        Test a request to the view from an anonymous user.
        """
        url = '/debatnl-registration/ongoing-trainings/{}/'.format(
            self.planned_training.training.id
        )
        response = self.client.get(url, follow=True)
        expected_url = f'{reverse("login")}?next=/feedback/'
        self.assertRedirects(response, expected_url)

    def test_GET_request_with_3_available_dates(self):
        """
        Test a request to the view from an authenticated user.
        """

        today = datetime.datetime.now().date()
        for index, training_date in enumerate(self.training_dates):
            training_date.date = today + datetime.timedelta(days=index)
            training_date.save()

        # Perform request.
        url = '/debatnl-registration/ongoing-trainings/{}/'.format(
            self.planned_training.training.id
        )
        self.client.force_login(self.user)
        response = self.client.get(url)

        # Assert outcome for the request.
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(
            response,
            'debatnl_registration/components/widgets/'
            'training_dates_selector.html'
        )
        self.assertEqual(len(response.context['object_list']), 3)

        for training_date in self.training_dates[1:]:
            with self.subTest(training_date=training_date):
                self.assertIn(training_date, response.context['object_list'])

    def test_GET_request_with_2_available_dates(self):
        """
        Test a request to the view from an authenticated user.
        """

        today = datetime.datetime.now().date()
        for index, training_date in enumerate(self.training_dates[1:]):
            training_date.date = today + datetime.timedelta(days=index)
            training_date.save()

        # Perform request.
        url = '/debatnl-registration/ongoing-trainings/{}/'.format(
            self.planned_training.training.id
        )
        self.client.force_login(self.user)
        response = self.client.get(url)

        # Assert outcome for the request.
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(
            response,
            'debatnl_registration/components/widgets/'
            'training_dates_selector.html'
        )
        self.assertEqual(len(response.context['object_list']), 2)

        for training_date in self.training_dates[2:]:
            with self.subTest(training_date=training_date):
                self.assertIn(training_date, response.context['object_list'])

    def test_GET_request_with_no_available_dates(self):
        """
        Test a request to the view from an authenticated user.
        """

        # Perform request.
        url_name = 'debatnl_registration:available_training_dates'
        pk = self.planned_training.training.pk
        url = reverse(url_name, args=(pk,))
        self.client.force_login(self.user)
        response = self.client.get(url)

        # Assert outcome for the request.
        self.assertContains(response, 'Er is geen data beschikbaar')
        self.assertEqual(len(response.context['object_list']), 0)
        self.assertTemplateUsed(
            response,
            'debatnl_registration/components/widgets/'
            'training_dates_selector.html'
        )


class ConfirmTrainingUpdateView(TestCase):
    fixtures = ['roles']

    def setUp(self):
        role = Role.objects.get(name=RoleName.ADMIN.value)
        user = UserFactory()
        user.profile.roles.add(role)

        registration = RegistrationFactory()
        participant = ParticipantFactory(registration=registration)
        training = TrainingFactory()

        planned_training_1 = PlannedTrainingFactory(
            training=training,
            hall_status='AVAILABLE'
        )

        self.planned_training_2 = PlannedTrainingFactory(
            training=training,
            hall_status='AVAILABLE'
        )

        self.training_date_1_1 = TrainingDateFactory(
            sequence=0,
            planned_training=planned_training_1
        )
        self.training_date_1_2 = TrainingDateFactory(
            sequence=1,
            planned_training=planned_training_1
        )
        self.training_date_1_3 = TrainingDateFactory(
            sequence=2,
            planned_training=planned_training_1
        )

        self.training_date_2_1 = TrainingDateFactory(
            sequence=0,
            planned_training=self.planned_training_2
        )
        self.training_date_2_2 = TrainingDateFactory(
            sequence=1,
            planned_training=self.planned_training_2
        )
        self.training_date_2_3 = TrainingDateFactory(
            sequence=2,
            planned_training=self.planned_training_2
        )

        AttendanceFactory(
            participant=participant,
            training_date=self.training_date_1_1
        )
        AttendanceFactory(
            participant=participant,
            training_date=self.training_date_1_2
        )
        AttendanceFactory(
            participant=participant,
            training_date=self.training_date_1_3
        )

        self.token = PlannedTrainingToken.objects.create(
            planned_training=planned_training_1,
            participant=participant,
            type='CANCEL TRAINING PARTICIPANT'
        )

        self.client = Client()

    def test_GET_request_2_days(self):
        self.training_date_1_2.status = 'CANCELLED'
        self.training_date_1_2.save()

        self.training_date_1_3.status = 'CANCELLED'
        self.training_date_1_3.save()

        url = '/debatnl-registration/confirm/training-update/?token={}'.format(
            self.token.key
        )
        response = self.client.get(url)

        planned_trainings = PlannedTraining.objects.filter(
            training=self.token.planned_training.training,
            hall_status='AVAILABLE'
        ).exclude(id=self.token.planned_training_id).all()

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['token'], self.token)
        self.assertFalse(response.context['exclude_first_option'])
        self.assertEqual(
            response.context['participant'],
            self.token.participant
        )
        self.assertListEqual(
            list(response.context['planned_trainings']),
            list(planned_trainings)
        )
        self.assertTemplateUsed(
            response,
            'debatnl_registration/public/change_training.html'
        )

    def test_GET_request_3_days(self):
        self.training_date_1_1.status = 'CANCELLED'
        self.training_date_1_1.save()

        self.training_date_1_2.status = 'CANCELLED'
        self.training_date_1_2.save()

        self.training_date_1_3.status = 'CANCELLED'
        self.training_date_1_3.save()

        url = '/debatnl-registration/confirm/training-update/?token={}'.format(
            self.token.key
        )
        response = self.client.get(url)

        planned_trainings = PlannedTraining.objects.filter(
            training=self.token.planned_training.training,
            hall_status='AVAILABLE'
        ).exclude(id=self.token.planned_training_id).all()

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['token'], self.token)
        self.assertTrue(response.context['exclude_first_option'])
        self.assertEqual(
            response.context['participant'],
            self.token.participant
        )
        self.assertListEqual(
            list(response.context['planned_trainings']),
            list(planned_trainings)
        )
        self.assertTemplateUsed(
            response,
            'debatnl_registration/public/change_training.html'
        )

    def test_POST_request_2_days_option_1(self):
        self.training_date_1_2.status = 'CANCELLED'
        self.training_date_1_2.save()

        self.training_date_1_3.status = 'CANCELLED'
        self.training_date_1_3.save()

        payload = {
            'option': '1',
            'training_1': self.planned_training_2.id,
            'coaching_date_day_1': '01',
            'coaching_date_month_1': '01',
            'coaching_date_year_1': '2018',
            'coaching_date_time_1': '13:00 - 15:00',
            'name_3': '',
            'email_3': '',
            'invoice_number_3': '',
            'bank_account_number_3': '',
            'account_holder_name_3': '',
            'reference_3': ''
        }

        url = '/debatnl-registration/confirm/training-update/?token={}'.format(
            self.token.key)
        response = self.client.post(url, payload)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(Attendance.objects.count(), 3)
        self.assertEqual(Email.objects.count(), 2)
        self.assertEqual(PlannedTrainingToken.objects.count(), 0)
        self.assertEqual(response.context['messages'],
                         ['Bedankt voor het maken van uw keuze!'])
        self.assertTemplateUsed(
            response,
            'debatnl_registration/public/confirmation.html'
        )

    def test_POST_request_2_days_option_2(self):
        self.training_date_1_2.status = 'CANCELLED'
        self.training_date_1_2.save()

        self.training_date_1_3.status = 'CANCELLED'
        self.training_date_1_3.save()

        payload = {
            'option': '2',
            'training_2': self.planned_training_2.id,
            'coaching_date_day_2': '01',
            'coaching_date_month_2': '01',
            'coaching_date_year_2': '2019',
            'coaching_date_time_2': '11:00 - 13:00',
            'name_3': '',
            'email_3': '',
            'invoice_number_3': '',
            'bank_account_number_3': '',
            'account_holder_name_3': '',
            'reference_3': ''
        }

        url = '/debatnl-registration/confirm/training-update/?token={}'.format(
            self.token.key
        )
        response = self.client.post(url, payload)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(Attendance.objects.count(), 3)
        self.assertEqual(Email.objects.count(), 2)
        self.assertEqual(PlannedTrainingToken.objects.count(), 0)
        self.assertEqual(
            response.context['messages'],
            ['Bedankt voor het maken van uw keuze!']
        )
        self.assertTemplateUsed(
            response,
            'debatnl_registration/public/confirmation.html'
        )

    def test_POST_request_2_days_option_3(self):
        self.training_date_1_2.status = 'CANCELLED'
        self.training_date_1_2.save()

        self.training_date_1_3.status = 'CANCELLED'
        self.training_date_1_3.save()

        payload = {
            'option': '3',
            'name_3': 'test persoon',
            'email_3': 'test@email.test',
            'invoice_number_3': '1234567890',
            'invoice_choice_3': 'Ja',
            'bank_account_number_3': '1000000000',
            'account_holder_name_3': 'test persoon2',
            'reference_3': '123412312'
        }

        url = '/debatnl-registration/confirm/training-update/?token={}'.format(
            self.token.key
        )
        response = self.client.post(url, payload)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(Attendance.objects.count(), 0)
        self.assertEqual(Email.objects.count(), 1)
        self.assertEqual(PlannedTrainingToken.objects.count(), 0)
        self.assertEqual(
            response.context['messages'],
            ['Bedankt voor het maken van uw keuze!']
        )
        self.assertTemplateUsed(
            response,
            'debatnl_registration/public/confirmation.html'
        )

    def test_POST_request_3_days_option_1(self):
        self.training_date_1_1.status = 'CANCELLED'
        self.training_date_1_1.save()

        self.training_date_1_2.status = 'CANCELLED'
        self.training_date_1_2.save()

        self.training_date_1_3.status = 'CANCELLED'
        self.training_date_1_3.save()

        payload = {
            'option': '1',
            'training_1': self.planned_training_2.id,
            'coaching_date_day_1': '01',
            'coaching_date_month_1': '01',
            'coaching_date_year_1': '2018',
            'coaching_date_time_1': '13:00 - 15:00',
            'name_3': '',
            'email_3': '',
            'invoice_number_3': '',
            'bank_account_number_3': '',
            'account_holder_name_3': '',
            'reference_3': ''
        }

        url = '/debatnl-registration/confirm/training-update/?token={}'.format(
            self.token.key
        )
        response = self.client.post(url, payload)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(Attendance.objects.count(), 3)
        self.assertEqual(Email.objects.count(), 2)
        self.assertEqual(PlannedTrainingToken.objects.count(), 0)
        self.assertEqual(
            response.context['messages'],
            ['Bedankt voor het maken van uw keuze!']
        )
        self.assertTemplateUsed(
            response,
            'debatnl_registration/public/confirmation.html'
        )

    def test_POST_request_3_days_option_2(self):
        self.training_date_1_1.status = 'CANCELLED'
        self.training_date_1_1.save()

        self.training_date_1_2.status = 'CANCELLED'
        self.training_date_1_2.save()

        self.training_date_1_3.status = 'CANCELLED'
        self.training_date_1_3.save()

        payload = {
            'option': '2',
            'training_2': self.planned_training_2.id,
            'coaching_date_day_2': '01',
            'coaching_date_month_2': '01',
            'coaching_date_year_2': '2019',
            'coaching_date_time_2': '11:00 - 13:00',
            'name_3': '',
            'email_3': '',
            'invoice_number_3': '',
            'bank_account_number_3': '',
            'account_holder_name_3': '',
            'reference_3': ''
        }

        url = '/debatnl-registration/confirm/training-update/?token={}'.format(
            self.token.key
        )
        response = self.client.post(url, payload)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(Attendance.objects.count(), 3)
        self.assertEqual(Email.objects.count(), 2)
        self.assertEqual(PlannedTrainingToken.objects.count(), 0)
        self.assertEqual(
            response.context['messages'],
            ['Bedankt voor het maken van uw keuze!']
        )
        self.assertTemplateUsed(
            response,
            'debatnl_registration/public/confirmation.html'
        )

    def test_POST_request_3_days_option_3(self):
        self.training_date_1_1.status = 'CANCELLED'
        self.training_date_1_1.save()

        self.training_date_1_2.status = 'CANCELLED'
        self.training_date_1_2.save()

        self.training_date_1_3.status = 'CANCELLED'
        self.training_date_1_3.save()

        payload = {
            'option': '3',
            'name_3': 'test persoon',
            'email_3': 'test@email.test',
            'invoice_number_3': '1234567890',
            'invoice_choice_3': 'Ja',
            'bank_account_number_3': '1000000000',
            'account_holder_name_3': 'test persoon2',
            'reference_3': '123412312'
        }

        url = '/debatnl-registration/confirm/training-update/?token={}'.format(
            self.token.key
        )
        response = self.client.post(url, payload)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(Attendance.objects.count(), 0)
        self.assertEqual(Email.objects.count(), 1)
        self.assertEqual(PlannedTrainingToken.objects.count(), 0)
        self.assertEqual(
            response.context['messages'],
            ['Bedankt voor het maken van uw keuze!']
        )
        self.assertTemplateUsed(
            response,
            'debatnl_registration/public/confirmation.html'
        )


class ConfirmAttendanceListView(TestCase):
    def setUp(self):
        self.training_date = TrainingDateFactory()
        self.token = PlannedTrainingToken.objects.create(
            planned_training=self.training_date.planned_training,
            training_date=self.training_date,
            type='ATTENDANCE LIST'
        )

        registration = RegistrationFactory()
        self.participant = ParticipantFactory(registration=registration)
        self.attendance = AttendanceFactory(
            training_date=self.training_date,
            participant=self.participant
        )
        self.user = User.objects.create_user(username='test_user')
        self.client = Client()

    def test_GET_request(self):
        url = '/debatnl-registration/confirm/attendance-list/?token={}'.format(
            self.token.key
        )

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(
            response,
            'debatnl_registration/public/attendance_list.html'
        )
        self.assertEqual(PlannedTrainingToken.objects.count(), 1)

    def test_POST_request(self):
        payload = {
            '{}-has_attended'.format(self.attendance.id): 'true'
        }

        url = '/debatnl-registration/confirm/attendance-list/?token={}'.format(
            self.token.key
        )
        response = self.client.post(url, payload)

        self.attendance.refresh_from_db()

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(
            response,
            'debatnl_registration/public/confirmation.html'
        )
        self.assertTrue(self.attendance.has_attended)
        self.assertEqual(PlannedTrainingToken.objects.count(), 0)


class CancelAttendanceView(TestCase):
    fixtures = ['app_permissions']

    def setUp(self):
        self.training_date = TrainingDateFactory()
        registration = RegistrationFactory()
        self.participant = ParticipantFactory(registration=registration)
        AttendanceFactory(
            training_date=self.training_date,
            participant=self.participant
        )
        self.user = User.objects.create_user(username='test_user')
        app_permission = AppPermission.objects.get(
            app_name=GROUP_DEBATNLREGISTRATION)
        self.user.profile.app_permissions.add(app_permission)
        self.client = Client()

    def test_POST_request_as_anonymous_user(self):
        """
        Test a request to the view from an anonymous user.
        """

        url = '/debatnl-registration/ongoing-trainings/{}/attendees/{}/cancel/'
        url = url.format(
            self.training_date.id,
            self.participant.id
        )
        response = self.client.post(url)
        expected_url = f'{reverse("login")}?next={url}'
        self.assertRedirects(response, expected_url)

    def test_participant_with_no_other_training_date(self):
        """
        Test a request to the view from an authenticated user.
        """
        self.client.force_login(user=self.user)

        url = '/debatnl-registration/ongoing-trainings/{}/attendees/{}/cancel/'
        url = url.format(
            self.training_date.id,
            self.participant.id
        )

        response = self.client.post(url)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.url, '/debatnl-registration/')
        self.assertEqual(Attendance.objects.count(), 0)
        self.assertEqual(Participant.objects.count(), 0)

    def test_participant_with_other_training_date(self):
        """
        Test a request to the view from an authenticated user.
        """
        training_date = TrainingDateFactory()
        AttendanceFactory(
            training_date=training_date,
            participant=self.participant
        )

        self.client.force_login(user=self.user)

        url = '/debatnl-registration/ongoing-trainings/{}/attendees/{}/cancel/'
        url = url.format(
            self.training_date.id,
            self.participant.id
        )

        response = self.client.post(url)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.url, '/debatnl-registration/')
        self.assertEqual(Attendance.objects.count(), 1)
        self.assertEqual(Participant.objects.count(), 1)


class ChangeAttendanceView(TestCase):
    fixtures = ['app_permissions']

    def setUp(self):
        self.training_date_1 = TrainingDateFactory()
        self.training_date_2 = TrainingDateFactory()

        registration = RegistrationFactory()
        self.participant = ParticipantFactory(registration=registration)
        AttendanceFactory(
            training_date=self.training_date_1,
            participant=self.participant
        )

        self.user = User.objects.create_user(username='test_user')
        app_permission = AppPermission.objects.get(
            app_name=GROUP_DEBATNLREGISTRATION)
        self.user.profile.app_permissions.add(app_permission)
        self.client = Client()

    def test_POST_request_as_anonymous_user(self):
        """
        Test a request to the view from an anonymous user.
        """
        url = '/debatnl-registration/ongoing-trainings/{}/attendees/{}/change/'
        url = url.format(
            self.training_date_1.id,
            self.participant.id
        )
        response = self.client.post(url)
        expected_url = f'{reverse("login")}?next={url}'
        self.assertRedirects(response, expected_url)

    def test_attendance_change(self):
        """
        Test a request to the view from an authenticated user.
        """
        self.client.force_login(user=self.user)

        url = '/debatnl-registration/ongoing-trainings/{}/attendees/{}/change/'
        url = url.format(
            self.training_date_1.id,
            self.participant.id
        )

        payload = {
            'training_date': self.training_date_2.id
        }

        response = self.client.post(url, payload)

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.url, '/debatnl-registration/')
        self.assertEqual(Attendance.objects.count(), 1)
        self.assertEqual(Attendance.objects.filter(
            training_date=self.training_date_2, participant=self.participant
        ).count(), 1)
        self.assertEqual(Participant.objects.count(), 1)
