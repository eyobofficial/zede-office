import pendulum
from django.conf import settings

from DebatNL_BackOffice.celery import app
from offers.emails.reminders import ReviewOfferReminder, CreatedOfferReminder
from offers.models import Offer, OfferToken, OfferAbsenceSchedule


@app.task
def clean_up_outdated_offers(**kwargs):
    """
    Clean up offers that are FINISHED for longer than 4 weeks.
    """
    today = pendulum.today(settings.TIME_ZONE)
    finished_at = today.subtract(weeks=kwargs.get('weeks', 4))
    Offer.objects.filter(status=Offer.FINISHED,
                         finished_at__lt=finished_at).delete()


@app.task
def send_review_reminder_emails():
    """
    Send offer review reminders in the morning (e.g. 10am Europe/Amsterdam)
    """
    tz = settings.TIME_ZONE
    today = pendulum.today(tz=tz)

    for token in OfferToken.objects.filter(type=OfferToken.REVIEW).all():
        is_today = False
        created_at = pendulum.instance(token.created_at, tz=tz)
        last_reminder_at = token.last_reminder_at

        if last_reminder_at:
            last_reminder_at = pendulum.instance(last_reminder_at).in_tz(tz=tz)
            is_today = last_reminder_at.is_same_day(today)

        # Start sending reminders after 72 hours
        if not is_today and today.diff(created_at).in_hours() > 72:
            ReviewOfferReminder(token).send()
            token.last_reminder_at = pendulum.now()
            token.save()


@app.task
def send_upload_reminders_emails():
    """
    Send offer upload reminders in the morning (e.g. 10am Europe/Amsterdam) and
    in the evening (e.g. 8pm Europe/Amsterdam)
    """
    tz = settings.TIME_ZONE
    today = pendulum.today(tz=tz)
    kwargs = {'deadline_at__lte': today, 'type': OfferToken.UPLOAD}

    for token in OfferToken.objects.filter(**kwargs).all():
        is_today = False
        deadline_at = pendulum.instance(token.deadline_at).in_tz(tz)
        last_reminder_at = token.last_reminder_at

        if last_reminder_at:
            last_reminder_at = pendulum.instance(last_reminder_at).in_tz(tz=tz)
            is_today = last_reminder_at.is_same_day(today)

        if not is_today and today.diff(deadline_at).in_days() > 0:
            CreatedOfferReminder(token).send()
            token.last_reminder_at = pendulum.now()
            token.save()


@app.task
def clean_up_absence_schedules():
    cutoff_dt = pendulum.now(tz=settings.TIME_ZONE).date()
    OfferAbsenceSchedule.objects.filter(end_date__lt=cutoff_dt).all().delete()
