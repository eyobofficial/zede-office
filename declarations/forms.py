from email.policy import default

from django import forms
from django.core.exceptions import ValidationError
from django.utils.translation import gettext as _

from backoffice.forms import SplitTimeField

from .models import Declaration, ExternalMeeting, InternalMeeting, \
    AbsenceEntry, Expense, EmployeeBonus, ClientBonus, ELearningItem, \
    HubstaffItem, TravelExpenseRoute, TravelExpense


class DeclarationModelForm(forms.ModelForm):
    RATING_CHOICES = [('', '--')] + [(n, n) for n in range(10, 0, -1)]

    billable_hourly_rate = forms.DecimalField(
        label='Declarabel uurtarief',
        max_digits=8, decimal_places=2,
        required=True
    )
    internal_hourly_rate = forms.DecimalField(
        label='Intern uurtarief',
        max_digits=8, decimal_places=2,
        required=True
    )
    work_satisfaction = forms.ChoiceField(
        label='We zijn benieuwd! Hoe tevreden ben je over jouw werkzaamheden?',
        choices=RATING_CHOICES,
        required=False
    )
    company_satisfaction = forms.ChoiceField(
        label='En hoe tevreden ben je over de organisatie?',
        choices=RATING_CHOICES,
        required=False
    )
    feedback = forms.CharField(
        label='Wat kunnen we doen om jouw werkplezier te vergroten?',
        widget=forms.Textarea
    )

    class Meta:
        model = Declaration
        fields = ['billable_hourly_rate', 'internal_hourly_rate',
                  'work_satisfaction', 'company_satisfaction', 'feedback']

    def clean_work_satisfaction(self):
        """
        Require `work_satisfaction` when the form is finally submitted.
        """
        data = self.cleaned_data['work_satisfaction'] or None
        if 'submit' in self.data and data is None:
            raise ValidationError(_('This field is required.'))
        return data

    def clean_company_satisfaction(self):
        """
        Require `company_satisfaction` when the form is finally submitted.
        """
        data = self.cleaned_data['company_satisfaction'] or None
        if 'submit' in self.data and data is None:
            raise ValidationError(_('This field is required.'))
        return data

    def save(self, commit=True):
        if 'submit' in self.data:
            self.instance.status = Declaration.IN_PROGRESS
            self.save_related_absence_entries()
        return super().save(commit)

    def save_related_absence_entries(self):
        self.instance.absence_entries.all().delete()
        for period in self.instance.get_related_vacation_periods():
            absence_entry = AbsenceEntry()
            absence_entry.set_data_from_vacation_period(period)
            self.instance.absence_entries.add(absence_entry, bulk=False)


class ExternalMeetingModelForm(forms.ModelForm):
    date = forms.DateField(widget=forms.HiddenInput())
    write_period = forms.DecimalField(required=False)
    start_time = SplitTimeField()
    end_time = SplitTimeField()
    travel_expense = forms.CharField(widget=forms.HiddenInput(),
                                     required=False)

    class Meta:
        model = ExternalMeeting
        fields = '__all__'

    def clean_travel_expense(self):
        return self.cleaned_data['travel_expense'] or None

    def clean_write_period(self):
        return self.cleaned_data['write_period'] or 0


class InternalMeetingModelForm(forms.ModelForm):
    type = forms.ChoiceField(choices=InternalMeeting.TYPE_CHOICES)
    date = forms.DateField()
    start_time = SplitTimeField()
    end_time = SplitTimeField()
    travel_expense = forms.CharField(widget=forms.HiddenInput(),
                                     required=False)

    class Meta:
        model = InternalMeeting
        fields = '__all__'

    def clean_travel_expense(self):
        return self.cleaned_data['travel_expense'] or None


class ExpenseModelForm(forms.ModelForm):
    description = forms.CharField(label='Omschrijving')
    amount = forms.DecimalField(label='Bedrag')
    file = forms.FileField(label='Upload bon')

    class Meta:
        model = Expense
        fields = ('description', 'amount', 'file')


class EmployeeBonusModelForm(forms.ModelForm):
    name = forms.CharField(label='Naam medewerker')
    type = forms.ChoiceField(choices=EmployeeBonus.TYPE_CHOICES,
                             widget=forms.RadioSelect())

    class Meta:
        model = EmployeeBonus
        fields = ('type', 'name')


class ClientBonusModelForm(forms.ModelForm):
    invoice_number = forms.CharField(label='Factuurnummer')
    name = forms.CharField(label='Klantnaam')
    amount = forms.DecimalField(label='Bedrag')

    class Meta:
        model = ClientBonus
        fields = ('invoice_number', 'name', 'amount')


class AllowanceModelForm(forms.ModelForm):
    date = forms.DateField()
    internal_meetings = forms.ModelMultipleChoiceField(
        queryset=InternalMeeting.objects.all(),
        widget=forms.CheckboxSelectMultiple,
        required=False
    )
    external_meetings = forms.ModelMultipleChoiceField(
        queryset=ExternalMeeting.objects.all(),
        widget=forms.CheckboxSelectMultiple,
        required=False
    )

    class Meta:
        model = Expense
        fields = (
            'date', 'internal_meetings', 'external_meetings',
            'amount', 'file'
        )

    def __init__(self, *args, **kwargs):
        declaration = kwargs.pop('declaration')
        date = kwargs.pop('date', None)
        super().__init__(*args, **kwargs)
        if date:
            internal_qs = declaration.internal_meetings.filter(date=date)
            internal_qs = internal_qs.exclude(type=InternalMeeting.INTERNAL)
            external_qs = declaration.external_meetings.filter(date=date)
            self.fields['internal_meetings'].queryset = internal_qs
            self.fields['external_meetings'].queryset = external_qs


class ELearningItemModelForm(forms.ModelForm):
    name = forms.ChoiceField(label='Welke elearning wil je toevoegen?',
                             choices=ELearningItem.NAME_CHOICES)
    certificate = forms.FileField(label='Upload certificaat')

    class Meta:
        model = ELearningItem
        fields = ('name', 'certificate')


class HubstaffItemModelForm(forms.ModelForm):
    hours = forms.IntegerField(min_value=0)
    minutes = forms.IntegerField(min_value=0, max_value=59)
    from_api = forms.BooleanField(required=False)

    class Meta:
        model = HubstaffItem
        fields = ('hours', 'minutes', 'from_api', 'declaration')


class TravelExpenseRouteModelForm(forms.ModelForm):
    travel_mode = forms.CharField(required=False)
    depart_time = SplitTimeField(required=False)
    return_depart_time = SplitTimeField(required=False)

    class Meta:
        model = TravelExpenseRoute
        fields = ('travel_mode', 'depart_time', 'return_depart_time',
                  'location', 'travel_expense')

    def clean_depart_time(self):
        return self.cleaned_data['depart_time'] or None

    def clean_return_depart_time(self):
        return self.cleaned_data['return_depart_time'] or None


class TravelExpenseModelForm(forms.ModelForm):
    CHOICES = [(True, 'Enkele reis'), (False, 'Retour')]

    type = forms.ChoiceField(choices=TravelExpense.TYPE_CHOICES,
                             widget=forms.RadioSelect())
    is_one_way = forms.ChoiceField(choices=CHOICES, widget=forms.RadioSelect())
    date = forms.DateField(widget=forms.HiddenInput())
    distance = forms.DecimalField(required=False, widget=forms.TextInput(
        attrs={'readonly': 'readonly'}))
    duration = forms.DecimalField(required=False, widget=forms.TextInput(
        attrs={'readonly': 'readonly'}))

    class Meta:
        model = TravelExpense
        fields = ('type', 'is_one_way', 'date', 'distance', 'duration')
