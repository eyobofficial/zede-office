from jotform.client.api.base import JOTFormAPI
from jotform.client.objects import Submission, Answer


class Submissions(JOTFormAPI):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.url = 'form/{}/submissions'.format(self._form_id)

    def list(self):
        content = []
        limit = 1000
        iteration = 0
        break_on_next = False

        while not break_on_next:
            params = {
                'order_by': 'created_at',
                'limit': limit,
                'offset': iteration * limit,
            }

            response = self._get(self.url, params=params)
            content += response['content']
            iteration += 1
            break_on_next = limit > response['resultSet']['count']

        return self._parse_content(content)

    def get(self, submission_id):
        url = 'submission/{}'.format(submission_id)
        response = self._get(url)
        submissions = self._parse_content([response['content']])
        return submissions

    @staticmethod
    def _parse_content(content):
        results = []
        for item in content:
            submission = Submission.construct_from(item)

            if submission.id == '#SampleSubmissionID':
                continue

            answers = []
            for key, value in submission.answers.items():
                value['qid'] = key
                answers.append(Answer.construct_from(value))

            submission.answers = answers
            results.append(submission)

        return results
