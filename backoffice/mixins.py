from django.contrib.auth.mixins import UserPassesTestMixin, LoginRequiredMixin
from django.urls import reverse_lazy
from django.views.generic.base import ContextMixin

from backoffice.constants import GROUP_BACKOFFICE
from backoffice.models import Training
from users.models import UserProfile


class GroupAccessMixin(UserPassesTestMixin, LoginRequiredMixin):
    login_url = reverse_lazy('main_page')
    redirect_field_name = None
    access_groups = []
    request = None

    def test_func(self):
        try:
            profile = self.request.user.profile
            kwargs = {'app_name__in': self.access_groups}
            return profile.app_permissions.filter(**kwargs).exists()

        except (UserProfile.DoesNotExist, AttributeError):
            return self.request.user.is_superuser


class BaseTrainingMixin(GroupAccessMixin):
    template_name = 'backoffice/components/modals/modal-training-form.html'
    model = Training
    fields = ('name', )
    success_url = reverse_lazy('configuration:overview')
    access_groups = [GROUP_BACKOFFICE]
