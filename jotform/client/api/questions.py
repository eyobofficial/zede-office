from jotform.client.api.base import JOTFormAPI
from jotform.client.objects import Question


class Questions(JOTFormAPI):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.url = 'form/{}/questions'.format(self._form_id)

    def list(self, all_types=False):
        response = self._get(self.url)
        return self._parse_response(response, all_types)

    @staticmethod
    def _parse_response(response, all_types):
        exclude_temp_types = ['control_text', 'control_pagebreak',
                              'control_head', 'control_collapse']

        exclude_perm_types = ['control_divider', 'control_widget',
                              'control_image', 'control_button']
        questions = []
        for item in response['content'].values():
            if item['type'] in exclude_perm_types:
                continue

            if not all_types and item['type'] in exclude_temp_types:
                continue

            question = Question.construct_from(item)
            questions.append(question)

        return questions
