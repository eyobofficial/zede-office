from jotform.client.api.forms import Forms
from jotform.client.api.questions import Questions
from jotform.client.api.submissions import Submissions
from jotform.client.api.webhooks import Webhooks


class JOTFormClient:
    BASE_URL = 'https://api.jotform.com/form'

    def __init__(self, api_key='', form_id=''):
        self._api_key = api_key
        self._form_id = form_id
        self._forms = None
        self._webhooks = None
        self._questions = None
        self._submissions = None
        self.args = (self._api_key, self.BASE_URL, self._form_id)

    @property
    def forms(self):
        if not self._forms:
            self._forms = Forms(*self.args)
        return self._forms

    @property
    def questions(self):
        if not self._questions:
            self._questions = Questions(*self.args)
        return self._questions

    @property
    def submissions(self):
        if not self._submissions:
            self._submissions = Submissions(*self.args)
        return self._submissions

    @property
    def webhooks(self):
        if not self._webhooks:
            self._webhooks = Webhooks(*self.args)
        return self._webhooks
