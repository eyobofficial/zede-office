from .base import BaseConsumer


class TrainingConsumer(BaseConsumer):
    uri = 'trainings'
