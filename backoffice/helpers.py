from typing import Union

import holidays
import pendulum
from pendulum import DateTime, Date


def is_workday(date: Union[DateTime, Date]) -> bool:
    return not is_weekend(date)


def is_weekend(date: Union[DateTime, Date]) -> bool:
    return date.day_of_week in [pendulum.SATURDAY, pendulum.SUNDAY]


def count_workdays(start: DateTime, end: DateTime) -> int:
    period = start - end

    work_days = 0
    for dt in period.range('days'):
        if is_workday(dt):
            work_days += 1

    return work_days


def convert_to_workday(date: Union[DateTime, Date]) -> Union[DateTime, Date]:
    nl_holidays = holidays.Netherlands()
    while is_weekend(date) or date in nl_holidays:
        date = date.add(days=1)

    return date


def perform_simple_sync(names, klass):
    item_qs = klass.objects

    for name in names:
        item, _ = klass.objects.get_or_create(name=name)
        item_qs = item_qs.exclude(id=item.id)

    item_qs.delete()


def split_person_name(name: str):
    str_split = name.strip().split(' ')
    first_name = str_split[0]
    last_name = ''

    word_count = len(str_split)
    if word_count == 2:
        last_name = str_split[1]
    elif word_count > 2:
        for name in str_split[1:]:
            last_name += name + ' '

    return first_name, last_name
