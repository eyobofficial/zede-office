from django.db.models.signals import pre_delete, post_delete
from django.dispatch import receiver

from briefings.models import Deliverable
from scans.models import Scan


@receiver(pre_delete, sender=Scan)
def change_related_deliverable_status(sender, instance, **kwargs):
    for deliverable in instance.deliverables.all():
        has_scans = deliverable.scans.exclude(id=instance.id).count() > 0
        is_submitted = deliverable.status == Deliverable.SUBMITTED

        if is_submitted and not has_scans:
            deliverable.status = Deliverable.PENDING
            deliverable.save()
