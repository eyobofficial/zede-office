# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-07-10 09:29
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('backoffice', '0014_auto_20170710_0913'),
    ]

    operations = [
        migrations.AlterField(
            model_name='scan',
            name='status',
            field=models.CharField(blank=True, choices=[('DEBATE SCAN', 'Debate Scan'), ('CHAIRMAN SCAN', 'Chairman Scan')], default='NEW', max_length=30, null=True),
        ),
    ]
