from django.conf import settings
from django.contrib.auth.models import User
from django.urls import reverse

from backoffice.utilities.emails import NotificationEmailMixin
from offers.emails.bases import OfferEmail, BaseOfferEmail, ReviewOfferEmail
from offers.models import OfferToken


class CreatedOfferNotification(NotificationEmailMixin, OfferEmail):
    def __init__(self, offer):
        super().__init__(offer)
        self.template_name = 'email_1_new_offer_notification.html'
        self.subject = 'Aanvraag offerte {}'.format(offer.organization)
        self.token = self.obtain_offer_token(OfferToken.UPLOAD)


class ChangedOfferNotification(NotificationEmailMixin, OfferEmail):
    def __init__(self, offer):
        super().__init__(offer)
        self.template_name = 'email_6_changed_offer.html'
        self.subject = 'Offerte {} is gewijzigd'.format(offer.organization)
        self.token = self.obtain_offer_token(OfferToken.UPLOAD)


class RemovedOfferNotification(NotificationEmailMixin, BaseOfferEmail):
    def __init__(self, offer):
        # Note: The offer object can be a partial copy from OfferViewMixin.
        # Most of the fields will be EMPTY if that is the case.
        self.template_name = 'email_7_removed_offer.html'
        self.subject = 'Offerte {} is geannuleerd'.format(offer.organization)
        self.recipient = offer.editor
        self.offer = offer


class ReviewOfferNotification(NotificationEmailMixin, ReviewOfferEmail):
    def __init__(self, offer):
        super().__init__(offer)
        self.template_name = 'email_2_review_offer_notification.html'
        self.subject = 'Offerte {} is binnen'.format(offer.organization)
        self.token = self.obtain_offer_token(OfferToken.REVIEW)


class AcceptedOfferNotification(NotificationEmailMixin, BaseOfferEmail):
    def __init__(self, offer):
        self.template_name = 'email_4_accepted_offer_notification.html'
        self.subject = 'Offerte goedgekeurd'
        self.recipient = offer.editor
        self.offer = offer


class DeniedOfferNotification(NotificationEmailMixin, BaseOfferEmail):
    def __init__(self, offer):
        self.template_name = 'email_3_denied_offer_notification.html'
        self.subject = 'Offerte {} is niet goedgekeurd'
        self.subject = self.subject.format(offer.organization)
        self.recipient = offer.editor
        self.offer = offer
        self.token = self.obtain_offer_token(OfferToken.UPLOAD)

    def get_context_data(self):
        context = {
            'review': self.offer.reviews.last(),
            'url': '{}{}?token={}&email={}'.format(
                settings.APP_HOSTNAME,
                reverse('offers:upload'),
                str(self.token.key),
                self.recipient.email
            ),
        }

        return super().get_context_data(**context)


class MarketingOfferNotification(NotificationEmailMixin, BaseOfferEmail):
    def __init__(self, offer):
        self.offer = offer
        self.template_name = 'email_8_marketing_offer_notification.html'
        self.recipient = User(first_name='Sharon', last_name='Kroes',
                              email='kroes@debat.nl')
        self.subject = 'Gefeliciteerd! Iemand heeft een offerte uit ' \
                       'marketing behaald'
