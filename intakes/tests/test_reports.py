from django.test import TestCase, SimpleTestCase
from reportlab.graphics.shapes import Drawing
from reportlab.platypus import Paragraph, PageBreak, KeepTogether, ListFlowable
from reportlab.platypus.flowables import LIIndenter

from intakes.reports.bases import BaseIntakeReport
from intakes.reports.insights import OrganizationInsightsReport, \
    TrainerInsightsReport
from intakes.reports.participants import ParticipantsReport
from intakes.reports.submissions import SubmissionReport
from intakes.tests.factories import IntakeFactory, ReportQuestionFactory
from jotform.models import Answer
from jotform.tests.factories import QuestionFactory, SubmissionFactory


class BaseReportTestCase(TestCase):
    report_class = None

    def setUp(self):
        self.expected_text = []
        self.actual_text = []
        self.line_count = 0
        self.paragraph_count = 0
        self.drawing_count = 0
        self.page_break_count = 0
        self.intake = IntakeFactory()

    def analyze_content_structure(self):
        content = self.report_class(self.intake).get_content()
        self._dispatch_content_types(content)

    def _dispatch_content_types(self, content):
        for line in content:
            if isinstance(line, PageBreak):
                self.page_break_count += 1

            if isinstance(line, Drawing):
                self.drawing_count += 1

            if isinstance(line, Paragraph):
                self.actual_text.append(line.text)
                self.paragraph_count += 1

            if isinstance(line, LIIndenter):
                self.actual_text.append(line.text)
                self.paragraph_count += 1

            if isinstance(line, KeepTogether):
                self._dispatch_content_types(line._content)

            if isinstance(line, ListFlowable):
                self._dispatch_content_types(line._content)

            self.line_count += 1

    def _convert_to_static_structure(self):
        # Rebuild form with static question types.
        form = self.intake.form
        form.submissions.all().delete()
        form.questions.all().delete()

        dropdown_q = QuestionFactory(type='control_dropdown', form=form)
        textbox_q = QuestionFactory(type='control_textbox', form=form)
        QuestionFactory.create_batch(2, type='control_textbox', form=form)

        SubmissionFactory.create_batch(4, form=form)

        # Add report questions to intake
        ReportQuestionFactory(qid=dropdown_q.qid, intake=self.intake)
        ReportQuestionFactory(qid=textbox_q.qid, intake=self.intake)

    def _build_insight_expected_text_list(self, trainer_version=False):
        # Add answers to the expected text list.
        answers = Answer.objects.filter(
            submission__form=self.intake.form).only('answer').all()

        for answer in answers:
            value = self.report_class.evaluate_value(answer.answer)
            self.expected_text.append(value)

        # Add questions to the expected text list.
        questions = self.intake.form.questions.order_by('order').all()
        for question in questions:
            text = question.text

            if question.type != 'control_dropdown' and not trainer_version:
                text = '{} (Top {})'.format(
                    text, self.intake.report_submission_amount)
            self.expected_text.append(text)

    def assert_results(self, submission_count, line_count, paragraph_count,
                       drawing_count, page_break_count):
        form = self.intake.form
        self.assertEqual(form.submissions.count(), submission_count)
        self.assertEqual(self.line_count, line_count)
        self.assertEqual(self.paragraph_count, paragraph_count)
        self.assertEqual(self.drawing_count, drawing_count)
        self.assertEqual(self.page_break_count, page_break_count)

        for line in self.actual_text:
            self.assertIn(line, self.expected_text)


class BaseIntakeReportTest(SimpleTestCase):
    def test_value_evaluation(self):
        values = [
            {'input': '', 'expected': '--'},
            {'input': '   ', 'expected': '   '},
            {'input': '  a ', 'expected': '  a '},
            {'input': 'value', 'expected': 'value'},
            {'input': 'value 123', 'expected': 'value 123'},
            {'input': '["v1", "v2"]', 'expected': 'v1<br/>v2'},
            {'input': '{"k1": "v1", "k2": "v2"}', 'expected': 'v1<br/>v2'},
        ]

        for index, value in enumerate(values):
            input_value = value['input']
            expected_value = value['expected']

            with self.subTest(value=input_value):
                eval_value = BaseIntakeReport.evaluate_value(input_value)
                self.assertEqual(eval_value, expected_value)


class ParticipantsReportTest(BaseReportTestCase):
    report_class = ParticipantsReport

    def setUp(self):
        super().setUp()
        self.expected_text = [
            'Digitale intake',
            'Overzicht deelnemers',
            ('In deze rapportage vindt u een lijst van alle deelnemers die de '
             'intake al ingevuld hebben. Op basis van de intake maken wij een '
             'maatwerk training. Dat betekent dat wij bij meer inzendingen '
             'meer mogelijkheden hebben om de training goed vorm te geven.')
        ]

    def test_report_with_submissions(self):
        # Analyze the structure of content from report.
        self.analyze_content_structure()

        # Add participant names to the expected text list.
        answers = Answer.objects.filter(submission__form=self.intake.form,
                                        qid=self.intake.qid_name).all()
        for answer in answers:
            if answer.answer:
                self.expected_text.append(answer.answer)

        # Assert results.
        stats = {
            'submission_count': 4,
            'line_count': 7,
            'paragraph_count': 7,
            'drawing_count': 0,
            'page_break_count': 0,
        }
        self.assert_results(**stats)

    def test_report_without_submissions(self):
        # Delete all submissions from form.
        self.intake.form.submissions.all().delete()
        self.intake.refresh_from_db()

        # Analyze the structure of content from report.
        self.analyze_content_structure()

        # Add no submissions message to the expected text.
        self.expected_text.append('Er zijn nog geen intakes ingevuld.')

        # Assert results.
        stats = {
            'submission_count': 0,
            'line_count': 4,
            'paragraph_count': 4,
            'drawing_count': 0,
            'page_break_count': 0,
        }
        self.assert_results(**stats)


class SubmissionReportTest(BaseReportTestCase):
    report_class = SubmissionReport

    def setUp(self):
        super().setUp()
        self.expected_text = ['Overzicht inzendingen']

    def test_report_with_submissions(self):
        # Analyze the structure of content from report.
        self.analyze_content_structure()

        # Add answers to the expected text list.
        answers = Answer.objects.filter(
            submission__form=self.intake.form).only('answer').all()

        for answer in answers:
            value = self.report_class.evaluate_value(answer.answer)
            self.expected_text.append(value)

        # Add questions to the expected text list.
        questions = self.intake.form.questions.only('text').all()
        for question in questions:
            self.expected_text.append(question.text)

        # Assert results.
        stats = {
            'submission_count': 4,
            'line_count': 36,
            'paragraph_count': 32,
            'drawing_count': 0,
            'page_break_count': 4,
        }
        self.assert_results(**stats)

    def test_report_without_submissions(self):
        # Delete all submissions from form.
        self.intake.form.submissions.all().delete()

        # Analyze the structure of content from report.
        self.analyze_content_structure()

        # Assert results.
        stats = {
            'submission_count': 0,
            'line_count': 0,
            'paragraph_count': 0,
            'drawing_count': 0,
            'page_break_count': 0,
        }
        self.assert_results(**stats)


class OrganizationInsightsReportTest(BaseReportTestCase):
    report_class = OrganizationInsightsReport

    def setUp(self):
        super().setUp()
        self.expected_text = ['Overzicht antwoorden']
        self._convert_to_static_structure()

    def test_report_with_submissions(self):
        # Analyze the structure of content from report.
        self.analyze_content_structure()

        # Build the expected text list for the insight report.
        self._build_insight_expected_text_list()

        # Assert results.
        stats = {
            'submission_count': 4,
            'line_count': 11,
            'paragraph_count': 7,
            'drawing_count': 1,
            'page_break_count': 0,
        }
        self.assert_results(**stats)

    def test_report_without_submissions(self):
        # Delete all submissions from form.
        self.intake.form.submissions.all().delete()
        self.intake.refresh_from_db()

        # Analyze the structure of content from report.
        self.analyze_content_structure()

        # Add no submissions message to the expected text.
        self.expected_text.append('Er zijn nog geen intakes ingevuld.')

        # Assert results.
        stats = {
            'submission_count': 0,
            'line_count': 2,
            'paragraph_count': 2,
            'drawing_count': 0,
            'page_break_count': 0,
        }
        self.assert_results(**stats)


class TrainerInsightsReportTest(BaseReportTestCase):
    report_class = TrainerInsightsReport

    def setUp(self):
        super().setUp()
        self.expected_text = ['Overzicht antwoorden']
        self._convert_to_static_structure()

    def test_report_with_submissions(self):
        # Analyze the structure of content from report.
        self.analyze_content_structure()

        # Build the expected text list for the insight report.
        self._build_insight_expected_text_list(trainer_version=True)

        # Assert results.
        stats = {
            'submission_count': 4,
            'line_count': 25,
            'paragraph_count': 17,
            'drawing_count': 1,
            'page_break_count': 0,
        }
        self.assert_results(**stats)

    def test_report_without_submissions(self):
        # Delete all submissions from form.
        self.intake.form.submissions.all().delete()

        # Analyze the structure of content from report.
        self.analyze_content_structure()

        # Add no submissions message to the expected text.
        self.expected_text.append('Er zijn nog geen intakes ingevuld.')

        # Assert results.
        stats = {
            'submission_count': 0,
            'line_count': 2,
            'paragraph_count': 2,
            'drawing_count': 0,
            'page_break_count': 0,
        }
        self.assert_results(**stats)
