import mock
import pendulum
from django.conf import settings
from django.test import TestCase

from users.models import Role
from users.tasks import send_no_hubstaff_account_email_every_10_days
from users.tests.factories import UserFactory


class SendNoHubstaffAccountEmailTaskTests(TestCase):
    """
    Tests for `send_no_hubstaff_account_email_every_10_days` task
    """
    fixtures = ['roles']

    def setUp(self):
        self.tz = settings.TIME_ZONE
        date_joined = pendulum.datetime(2020, 3, 6, tz=self.tz)
        role = Role.objects.get(name='Trainer')

        # Valid user 1
        self.user_1 = UserFactory(date_joined=date_joined)
        self.user_1.profile.roles.add(role)

        # Invalid user 2
        UserFactory(is_active=False)

        # Invalid user 3
        user_3 = UserFactory()
        role = Role.objects.get(name='Redacteur')
        user_3.profile.roles.add(role)

        # Invalid user 4
        user_4 = UserFactory()
        user_4.profile.hubstaff_user_id = 999
        user_4.profile.save()

        # Invalid user 5
        user_5 = UserFactory()
        user_5.profile.do_hubstaff_check = False
        user_5.profile.save()

    @mock.patch(
        'users.emails.notifications.NoHubstaffAccountNotificationEmail.send')
    def test_task_not_on_10th_day(self, mock_obj):
        fake_dates = [
            pendulum.datetime(2020, 3, 8, tz=self.tz),
            pendulum.datetime(2020, 3, 10, tz=self.tz),
            pendulum.datetime(2020, 3, 12, tz=self.tz),
            pendulum.datetime(2020, 3, 14, tz=self.tz),
        ]

        for fake_date in fake_dates:
            with self.subTest(date=fake_date), pendulum.test(fake_date):
                send_no_hubstaff_account_email_every_10_days()
                self.assertEqual(mock_obj.call_count, 0)

    @mock.patch(
        'users.emails.notifications.NoHubstaffAccountNotificationEmail.send')
    def test_task_on_10th_day(self, mock_obj):
        fake_date = pendulum.datetime(2020, 3, 16, tz=self.tz)

        with pendulum.test(fake_date):
            send_no_hubstaff_account_email_every_10_days()

        self.assertEqual(mock_obj.call_count, 1)

    @mock.patch(
        'users.emails.notifications.NoHubstaffAccountNotificationEmail.send')
    def test_task_on_20th_day(self, mock_obj):
        fake_date = pendulum.datetime(2020, 3, 26, tz=self.tz)
        print(self.user_1.get_full_name())
        with pendulum.test(fake_date):
            send_no_hubstaff_account_email_every_10_days()

        self.assertEqual(mock_obj.call_count, 1)
