from django.shortcuts import redirect
from django.views.generic import TemplateView, FormView, DeleteView

from debatnl_mobile.consumers.challenges import ParticipantConsumer, \
    ChallengeSubmissionConsumer
from debatnl_mobile.consumers.trainings import TrainingConsumer
from debatnl_mobile.forms import ChallengeForm, ParticipantForm, FeedbackForm
from debatnl_mobile.mixins import BaseChallengeView, BaseParticipantView, \
    BaseChallengeSubmissionView


class ChallengeOverviewView(BaseChallengeView, TemplateView):
    template_name = 'debatnl_mobile/challenge_overview.html'
    participant_consumer = ParticipantConsumer
    training_consumer = TrainingConsumer

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        challenge_consumer = self.consumer()
        participant_consumer = self.participant_consumer()
        training_consumer = self.training_consumer()
        if self.connection:
            context['challenge_list'] = challenge_consumer.retrieve_all(
                training=context['training_id']
            )
            context['participant_list'] = participant_consumer.retrieve_all(
                profile__training=context['training_id']
            )
            context['training'] = training_consumer.retrieve_object(
                context['training_id']
            )
        return context


class ChallengeCreateView(BaseChallengeView, FormView):
    template_name = 'debatnl_mobile/components/modals/challenge_form.html'
    form_class = ChallengeForm

    def form_valid(self, form):
        training_id = self.kwargs.get('id')
        form.save(self.consumer(), training_id)
        return super().form_valid(form)


class ChallengeUpdateView(BaseChallengeView, FormView):
    template_name = 'debatnl_mobile/components/modals/challenge_form.html'
    form_class = ChallengeForm

    def get_initial(self):
        challenge = self.get_object()
        return challenge

    def form_valid(self, form):
        training_id = self.kwargs.get('id')
        challenge_id = self.kwargs.get('pk')
        form.save(self.consumer(), training_id, challenge_id)
        return super().form_valid(form)


class ChallengeDeleteView(BaseChallengeView, DeleteView):
    template_name = 'debatnl_mobile/components/modals/delete.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['warning_message'] = 'Weet je zeker dat deze challenge '\
                                     'moet worden verwijderd?'
        return context

    def delete(self, request, *args, **kwargs):
        challenge_pk = self.kwargs.get('pk')
        self.consumer().delete_object(challenge_pk)
        return redirect(self.get_success_url())


class ParticipantDetailView(BaseParticipantView, TemplateView):
    template_name = 'debatnl_mobile/components/modals/participant_detail.html'
    submission_consumer = ChallengeSubmissionConsumer

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['submission_list'] = self.submission_consumer().retrieve_all(
            submitted_by=context['object']['id']
        )
        return context


class ParticipantCreateView(BaseParticipantView, FormView):
    template_name = 'debatnl_mobile/components/modals/participant_form.html'
    form_class = ParticipantForm

    def form_valid(self, form):
        training_id = self.kwargs.get('id')
        form.save(self.consumer(), training_id)
        return super().form_valid(form)


class ParticipantUpdateView(BaseParticipantView, FormView):
    template_name = 'debatnl_mobile/components/modals/participant_form.html'
    form_class = ParticipantForm

    def get_initial(self):
        participant = self.get_object()
        initial = {
            'first_name': participant['first_name'],
            'last_name': participant['last_name'],
            'email': participant['email'],
            'username': participant['username'],
            'profile': {
                'training': participant['profile']['training']
            }
        }
        return initial

    def form_valid(self, form):
        training_id = self.kwargs.get('id')
        user_id = self.kwargs.get('pk')
        form.save(self.consumer(), training_id, user_id)
        return super().form_valid(form)


class ParticipantDeleteView(BaseParticipantView, DeleteView):
    template_name = 'debatnl_mobile/components/modals/delete.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['warning_message'] = 'Weet je het zeker dat deze deelnemer '\
                                     'moet worden verwijderd?'
        return context

    def delete(self, request, *args, **kwargs):
        participant_pk = self.kwargs.get('pk')
        self.consumer().delete_object(participant_pk)
        return redirect(self.get_success_url())


class ChallengeSubmissionUpdateView(BaseChallengeSubmissionView, FormView):
    template_name = 'debatnl_mobile/components/modals/feedback_form.html'
    form_class = FeedbackForm

    def get_initial(self):
        challenge_submission = self.get_object()
        return {'challenge_score': challenge_submission.get('score')}

    def form_valid(self, form):
        submission_id = self.kwargs.get('pk')
        form.save(self.consumer(), submission_id)
        return super().form_valid(form)
