from django.db.models.functions import Length
from django.utils.text import Truncator
from reportlab.graphics.charts.barcharts import HorizontalBarChart
from reportlab.graphics.shapes import Drawing
from reportlab.lib.colors import HexColor
from reportlab.platypus import Paragraph, KeepTogether, ListFlowable

from intakes.reports.bases import BaseIntakeReport
from jotform.models import Question, Answer


class BaseInsightsReport(BaseIntakeReport):
    title = 'Overzicht antwoorden'
    show_all_answers = False
    questions = []

    def get_content(self):
        content = [Paragraph('Overzicht antwoorden', self.styles['header'])]

        if self.intake.form.submissions.count() > 0:
            content += self._dispatch_questions(self.get_questions())
        else:
            content += [Paragraph('Er zijn nog geen intakes ingevuld.',
                                  self.styles['question'])]
        return content

    def get_questions(self):
        raise NotImplementedError

    def _dispatch_questions(self, questions):
        content = []
        for question in questions:
            args = (self.intake, question.qid,)
            if question.options:
                content += self._generate_horizontal_bar_chart(*args)
            else:
                content += self._generate_answers_list(*args)
        return content

    def _generate_horizontal_bar_chart(self, intake, qid):
        question, data = self._retrieve_chart_data(intake, qid)
        chart = HorizontalBarChart()
        categories = [Truncator(k).chars(41) for k in data.keys()]
        chart.categoryAxis.categoryNames = categories
        category_count = len(chart.categoryAxis.categoryNames)
        max_height = category_count * 15

        drawing = Drawing(200, max_height + 50)
        drawing.hAlign = 'RIGHT'
        drawing.shift(x=-100, y=0)
        chart.y = 50
        chart.width = 200
        chart.height = max_height
        chart.data = [[v for v in data.values()]]
        chart.valueAxis.forceZero = True
        chart.groupSpacing = 10
        chart.bars[0].fillColor = HexColor('#FF6600')
        chart.bars[0].strokeColor = HexColor('#FF6600')
        drawing.add(chart)

        title = Paragraph(question.text, self.styles['question'])
        return [KeepTogether([title, drawing])]

    def _generate_answers_list(self, intake, qid):
        question = Question.objects.get(form=intake.form, qid=qid)
        unordered_answers = Answer.objects.filter(submission__form=intake.form,
                                                  qid=qid)

        title_text = question.text
        if not self.show_all_answers:
            answers = unordered_answers.order_by(Length('answer').desc())
            answers = answers[:intake.report_submission_amount]
            title_text = '{} (Top {})'.format(title_text,
                                              intake.report_submission_amount)
        else:
            answers = unordered_answers.order_by('submission_id')

        paragraphs = []
        for answer in answers:
            value = self.evaluate_value(answer.answer)
            paragraphs.append(Paragraph(value, self.styles['body']))

        numbered_list = ListFlowable(paragraphs, bulletType='1',
                                     bulletFontName='Calibri',
                                     bulletFontSize=11,
                                     bulletFormat='%s.',
                                     bulletColor=HexColor('#08304F'))

        title = Paragraph(title_text, style=self.styles['question'])
        return [KeepTogether([title, numbered_list])]

    @staticmethod
    def _retrieve_chart_data(intake, qid):
        question = Question.objects.filter(form=intake.form, qid=qid).get()
        keys = question.get_options_list()
        data = dict.fromkeys(keys, 0)

        for key, v in data.items():
            data[key] = Answer.objects.filter(submission__form=intake.form,
                                              qid=qid,
                                              answer__icontains=key).count()
        return question, data


class OrganizationInsightsReport(BaseInsightsReport):
    def get_questions(self):
        questions = []
        report_questions = self.intake.report_questions.all()
        for report_question in report_questions:
            question = report_question.get_question()

            if question:
                questions.append(question)

        questions = sorted(questions, key=lambda q: q.order)
        return questions


class TrainerInsightsReport(BaseInsightsReport):
    show_all_answers = True

    def get_questions(self):
        return self.intake.form.questions.order_by('order').all()
