import pendulum
from django.conf import settings
from django.contrib.auth.models import User
from django.db import transaction
from django.shortcuts import redirect, get_object_or_404
from django.template.loader import render_to_string
from django.urls import reverse_lazy
from django.utils import timezone
from django.views.generic import DetailView, FormView, ListView, DeleteView, \
    UpdateView, TemplateView, View

from backoffice.constants import GROUP_PLANNING
from backoffice.mixins import GroupAccessMixin
from backoffice.views import BaseEmailView
from shared.enums import RoleName

from .models import PlanningRequest, Delivery, Schedule
from planning.emails.notifications import NewPlanningRequestEmail, \
    TrainerScheduleConfirmEmail, PlanningRequestStatusChangeEmail, \
    NewOptionPlanningRequestEmail, OptionTrainerConfirmEmail, \
    OptionPlanningRequestDeleteEmail, FinalizeConfirmationEmail, \
    OverdueNotificationEmail
from planning.emails.reminders import PendingPlanningOptionReminderEmail, \
    FinalizeConfirmationReminderEmail, TrainerConfirmationReminderEmail, \
    AdminConfirmationReminderEmail

from .forms import ScheduleFormSet, ProcessTypeForm
from .mixins import BasePlanningMixin, PlanningRequestBaseView, \
    PlanningRequestCreateBaseView
from .utilities.forms import calculate_delivery_type
from .utilities.timetables import calculate_trainers_timetable


class OverviewView(BasePlanningMixin, ListView):
    template_name = 'planning/base.html'
    section = 'planning'
    model = Delivery


class ProcessTypeView(PlanningRequestBaseView, FormView):
    template_name = 'planning/components/modals/process_type.html'
    form_class = ProcessTypeForm
    access_groups = [GROUP_PLANNING]

    def form_valid(self, form):
        return redirect(form.get_redirect_url())


class DefinitePlanningRequestCreateView(PlanningRequestCreateBaseView):

    @transaction.atomic
    def form_valid(self, formset):
        delivery_dates = []
        for form in formset:
            form_date = form.cleaned_data.get('date')
            delivery_date = pendulum.parse(form_date).in_tz(settings.TIME_ZONE)
            delivery_dates.append(delivery_date)

        request_type = calculate_delivery_type(delivery_dates)
        self.instance = PlanningRequest.objects.create(
            type=request_type,
            created_by=self.request.user
        )

        formset.instance = self.instance
        formset.save()

        NewPlanningRequestEmail(formset.instance).send()
        return super().form_valid(formset)

    def get_success_message(self, cleaned_data):
        if self.instance and self.instance.type == PlanningRequest.IMMEDIATE:
            base_path = 'planning/components/notifications'
            path = f'{base_path}/immediate_planning_warning.html'
            return render_to_string(path)


class OptionPlanningRequestCreateView(PlanningRequestCreateBaseView):

    @transaction.atomic
    def form_valid(self, formset):
        self.instance = PlanningRequest.objects.create(
            type=PlanningRequest.OPTION,
            created_by=self.request.user
        )
        formset.instance = self.instance
        formset.save()
        NewOptionPlanningRequestEmail(formset.instance).send()
        return super().form_valid(formset)


class PlanningRequestDetailView(PlanningRequestBaseView, FormView):
    template_name = 'planning/components/modals/planning_request_detail.html'
    form_class = ScheduleFormSet
    initial = []

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['employee_list'] = User.objects.filter(
            profile__roles__name=RoleName.TRAINER.value,
            is_active=True
        ).all()
        planning_request = get_object_or_404(
            PlanningRequest,
            pk=self.kwargs['pk']
        )
        context['planning_request'] = planning_request
        context['formset'] = context.get('form')
        return context

    def form_valid(self, formset):
        context = self.get_context_data()
        planning_request = context['planning_request']

        formset.save()

        if not planning_request.is_confirmed():
            planning_request.status = PlanningRequest.WAITING_FOR_APPROVAL
            planning_request.request_approval_date = timezone.now()
            planning_request.is_confirmed_by_admin = False
            planning_request.save()

        # Send email to trainers
        schedules = Schedule.objects.filter(
            delivery__planning_request=planning_request,
            is_confirmed=False
        )
        trainers = []

        # E-mail class
        if planning_request.type == PlanningRequest.OPTION:
            Email = OptionTrainerConfirmEmail
        else:
            Email = TrainerScheduleConfirmEmail

        for schedule in schedules:
            if schedule.user not in trainers:
                Email(schedule).send()
                trainers.append(schedule.user)

        return super().form_valid(formset)

    def form_invalid(self, formset):
        return redirect('planning:overview')

    def get_success_message(self, cleaned_data):
        context = self.get_context_data()
        planning_request = context['planning_request']
        is_immediate = planning_request.type == PlanningRequest.IMMEDIATE
        if is_immediate and not planning_request.is_confirmed():
            path = 'planning/components/notifications/planning_finalized.html'
            return render_to_string(path)


class PlanningRequestConfirmView(TemplateView):
    template_name = 'planning/public/confirmation.html'
    planning_request = None

    def get(self, request, *args, **kwargs):
        request_id = self.kwargs.get('pk')
        trainer_id = self.kwargs.get('trainer_pk')
        self.planning_request = get_object_or_404(
            PlanningRequest,
            pk=request_id
        )

        schedules = Schedule.objects.filter(
            delivery__planning_request=self.planning_request)

        # FIXME: This can be removed when the Employee model is removed.
        if trainer_id.isdigit():
            schedules.filter(user_id=trainer_id).update(
                is_confirmed=True)
        else:
            schedules.filter(employee_id=trainer_id).update(
                is_confirmed=True)

        confirmations = list(schedules.values_list('is_confirmed',
                                                   flat=True).all())

        if all(confirmations):
            if self.planning_request.type == PlanningRequest.OPTION:
                self.planning_request.status = PlanningRequest.READY
                self.planning_request.save()
            else:
                PlanningRequestStatusChangeEmail(self.planning_request).send()

        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        kwargs['planning_request'] = self.planning_request
        return super().get_context_data(**kwargs)


class PlanningRequestAdminConfirmView(TemplateView):
    template_name = 'planning/public/confirmation_admin.html'

    def get(self, request, *args, **kwargs):
        request_id = self.kwargs.get('pk')

        try:
            planning_request = PlanningRequest.objects.get(id=request_id)
            planning_request.is_confirmed_by_admin = True
            planning_request.save()

            if planning_request.is_confirmed():
                planning_request.status = PlanningRequest.READY
                planning_request.save()

        except PlanningRequest.DoesNotExist:
            pass

        return super().get(request, *args, **kwargs)


class FinalizeOptionPlanningRequestView(GroupAccessMixin, View):
    """
    Finalize Pending OPTION planning request.
    """
    access_groups = [GROUP_PLANNING]

    def post(self, request, *args, **kwargs):
        planning_request = get_object_or_404(
            PlanningRequest,
            pk=self.kwargs.get('pk')
        )
        planning_request = self.convert_planning_request(planning_request)
        self.reset_schedules(planning_request)
        return redirect('planning:overview')

    @staticmethod
    def convert_planning_request(planning_request):
        """
        Convert planning request type to definite planning request and
        update the status.
        """
        delivery_dates = []
        for delivery in planning_request.deliveries.all():
            date = pendulum.parse(delivery.date.isoformat())
            delivery_dates.append(date)

        request_type = calculate_delivery_type(delivery_dates)
        planning_request.type = request_type
        planning_request.status = PlanningRequest.WAITING_FOR_APPROVAL
        planning_request.finalized_at = timezone.now()
        planning_request.save()
        return planning_request

    @staticmethod
    def reset_schedules(planning_request):
        """
        Reset trainer schedule confirmations
        """
        schedules = Schedule.objects.filter(
            delivery__planning_request=planning_request
        )
        for schedule in schedules:
            schedule.is_confirmed = False
            schedule.save()
            FinalizeConfirmationEmail(schedule).send()


class PlanningRequestDeleteView(GroupAccessMixin, DeleteView):
    model = PlanningRequest
    template_name = 'planning/components/modals/planning_request_delete.html'
    success_url = reverse_lazy('planning:overview')
    access_groups = [GROUP_PLANNING]

    @transaction.atomic
    def delete(self, request, *args, **kwargs):
        self.object = self.get_object()
        success_url = self.get_success_url()
        if self.object.type == PlanningRequest.OPTION:
            OptionPlanningRequestDeleteEmail(self.object).send()
        self.object.delete()
        return redirect(success_url)


class DeliveryUpdateView(UpdateView, GroupAccessMixin):
    model = Delivery
    template_name = 'planning/components/modals/delivery_update.html'
    success_url = reverse_lazy('planning:overview')
    fields = ('date', 'organization', 'time', 'location', 'description',
              'trainer_amount')
    access_groups = [GROUP_PLANNING]

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['hours'] = list(range(0, 24))
        context['minutes'] = list(range(0, 60, 10))
        return context


class AvailabilityDetailView(GroupAccessMixin, DetailView):
    template_name = 'planning/components/modals/availability_detail.html'
    model = Delivery
    access_groups = [GROUP_PLANNING]

    def get_context_data(self, **kwargs):
        kwargs['trainer_list'] = calculate_trainers_timetable(self.object)
        return super().get_context_data(**kwargs)


class NewPlanningRequestNotification(BaseEmailView):
    url = 'admin:planning_planningrequest_change'
    model = PlanningRequest
    email_class = NewPlanningRequestEmail
    email_name = 'new planning request notification'


class NewOptionPlanningRequestNotification(BaseEmailView):
    url = 'admin:planning_planningrequest_change'
    model = PlanningRequest
    email_class = NewOptionPlanningRequestEmail
    email_name = 'new option planning request notification'


class TrainerScheduleNotification(BaseEmailView):
    url = 'admin:planning_schedule_change'
    model = Schedule
    email_class = TrainerScheduleConfirmEmail
    email_name = 'trainer schedule notification'


class TrainerConfirmationReminder(BaseEmailView):
    url = 'admin:planning_schedule_change'
    model = Schedule
    email_class = TrainerConfirmationReminderEmail
    email_name = 'trainer confirmation reminder'


class OptionTrainerScheduleNotification(BaseEmailView):
    url = 'admin:planning_schedule_change'
    model = Schedule
    email_class = OptionTrainerConfirmEmail
    email_name = 'option request schedule notification'


class FinalizeConfirmationNotification(BaseEmailView):
    url = 'admin:planning_schedule_change'
    model = Schedule
    email_class = FinalizeConfirmationEmail
    email_name = 'finalize planning request schedule notification'


class FinalizeConfirmationReminder(BaseEmailView):
    url = 'admin:planning_schedule_change'
    model = Schedule
    email_class = FinalizeConfirmationReminderEmail
    email_name = 'finalize planning request schedule reminder'


class PlanningRequestStatusChangeNotification(BaseEmailView):
    url = 'admin:planning_planningrequest_change'
    model = PlanningRequest
    email_class = PlanningRequestStatusChangeEmail
    email_name = 'planning request status changed notification'


class AdminConfirmationReminder(BaseEmailView):
    url = 'admin:planning_planningrequest_change'
    model = PlanningRequest
    email_class = AdminConfirmationReminderEmail
    email_name = 'admin confirmation reminder'


class OptionPlanningRequestDeleteNotification(BaseEmailView):
    url = 'admin:planning_planningrequest_change'
    model = PlanningRequest
    email_class = OptionPlanningRequestDeleteEmail
    email_name = 'option planning request deleted notification'


class PendingPlanningOptionReminder(BaseEmailView):
    url = 'admin:planning_planningrequest_change'
    model = PlanningRequest
    email_class = PendingPlanningOptionReminderEmail
    email_name = 'Pending option planining request reminder'


class OverdueNotification(BaseEmailView):
    url = 'admin:planning_planningrequest_change'
    model = PlanningRequest
    email_class = OverdueNotificationEmail
    email_name = 'option planning request is overdue'
