from django.http import HttpResponseRedirect
from django.urls import reverse_lazy
from django.views.generic import TemplateView, FormView

from backoffice.constants import GROUP_DEBATNLONLINE
from backoffice.mixins import GroupAccessMixin
from debatnl_online.consumers.users import UserConsumer
from debatnl_online.forms.learning_goals import RoleForm


class RoleBaseView(GroupAccessMixin, FormView):
    form_class = RoleForm
    base_template = 'debatnl_online/components/learning_goals/roles/{}'
    template_name = base_template.format('modals/modal-role.html')
    consumer = UserConsumer()
    access_groups = [GROUP_DEBATNLONLINE]

    def get_success_url(self):
        self.success_url = reverse_lazy(
            'debatnl_online:learning_goals:organizations:details',
            kwargs={'organization': self.kwargs['organization']}
        )
        return super().get_success_url()

    def get_initial(self):
        self.initial['organization'] = self.kwargs['organization']
        return super().get_initial()

    def get_context_data(self, **kwargs):
        role_name = self.request.GET.get('name')
        kwargs.update({
            'role_name': role_name,
            'users': UserConsumer().retrieve_all(
                subscriptions='LEARNING GOALS'),
        })
        return super().get_context_data(**kwargs)


class RoleCreate(RoleBaseView):
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        if context['role_name'] == 'PARTICIPANT':
            context.update({
                'modal_title': 'DEELNEMER TOEVOEGEN',
                'button_text': 'Toevoegen',
            })

        if context['role_name'] == 'MANAGER':
            context.update({
                'modal_title': 'MANAGER TOEVOEGEN',
                'button_text': 'Toevoegen',
            })

        return context

    def form_valid(self, form):
        self.consumer.update_object(form.cleaned_data['id'], form.cleaned_data)
        return super().form_valid(form)


class RoleDelete(GroupAccessMixin, TemplateView):
    template_name = 'debatnl_online/modals/modal-delete.html'
    access_groups = [GROUP_DEBATNLONLINE]

    def get_context_data(self, **kwargs):
        role_name = self.request.GET.get('name')
        if role_name == 'PARTICIPANT':
            kwargs.update({
                'modal_title': 'DEELNEMER',
                'modal_description': 'de deelnemer',
            })

        if role_name == 'MANAGER':
            kwargs.update({
                'modal_title': 'MANAGER',
                'modal_description': 'de manager',
            })

        return super().get_context_data(**kwargs)

    def post(self, request, organization, pk):
        payload = {
            'organization': None,
            'role': 'PARTICIPANT'
        }
        UserConsumer().update_object(pk, payload)

        success_url = reverse_lazy(
            'debatnl_online:learning_goals:organizations:details',
            kwargs={'organization': organization}
        )

        return HttpResponseRedirect(success_url)
