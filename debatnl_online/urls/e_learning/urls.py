from django.conf.urls import url, include

from debatnl_online.views import overviews, courses, chapters, lessons, course_test, lesson_test

urlpatterns = [
    url(r'^lessons/(?P<course_id>[a-zA-Z0-9-]+)/(?P<chapter_id>[a-zA-Z0-9-]+)/(?P<lesson_id>[a-zA-Z0-9-]+)/questions/add/', lesson_test.view_add_lesson_test_question),
    url(r'^lessons/(?P<course_id>[a-zA-Z0-9-]+)/(?P<chapter_id>[a-zA-Z0-9-]+)/(?P<lesson_id>[a-zA-Z0-9-]+)/questions/(?P<question_id>[a-zA-Z0-9-]+)/change/', lesson_test.view_change_lesson_test_question),
    url(r'^lessons/(?P<course_id>[a-zA-Z0-9-]+)/(?P<chapter_id>[a-zA-Z0-9-]+)/(?P<lesson_id>[a-zA-Z0-9-]+)/questions/(?P<question_id>[a-zA-Z0-9-]+)/delete/', lesson_test.view_delete_lesson_test_question),
    url(r'^lessons/(?P<course_id>[a-zA-Z0-9-]+)/(?P<chapter_id>[a-zA-Z0-9-]+)/(?P<lesson_id>[a-zA-Z0-9-]+)/questions/', chapters.view_chapter, name='lesson_questions'),

    url(r'^lessons/(?P<course_id>[a-zA-Z0-9-]+)/(?P<chapter_id>[a-zA-Z0-9-]+)/add/', lessons.view_add_lesson),
    url(r'^lessons/(?P<course_id>[a-zA-Z0-9-]+)/(?P<chapter_id>[a-zA-Z0-9-]+)/(?P<lesson_id>[a-zA-Z0-9-]+)/change/', lessons.view_change_lesson),
    url(r'^lessons/(?P<course_id>[a-zA-Z0-9-]+)/(?P<chapter_id>[a-zA-Z0-9-]+)/(?P<lesson_id>[a-zA-Z0-9-]+)/delete/', lessons.view_delete_lesson),
    url(r'^lessons/(?P<course_id>[a-zA-Z0-9-]+)/(?P<chapter_id>[a-zA-Z0-9-]+)/', chapters.view_chapter, name='lesson'),

    url(r'^course-test/(?P<course_id>[a-zA-Z0-9-]+)/add/', course_test.view_add_course_test_question),
    url(r'^course-test/(?P<course_id>[a-zA-Z0-9-]+)/(?P<question_id>[a-zA-Z0-9-]+)/change/', course_test.view_change_course_test_question),
    url(r'^course-test/(?P<course_id>[a-zA-Z0-9-]+)/(?P<question_id>[a-zA-Z0-9-]+)/delete/', course_test.view_delete_course_test_question),
    url(r'^course-test/(?P<course_id>[a-zA-Z0-9-]+)/', courses.view_course, name='course-test'),

    url(r'^chapter/(?P<course_id>[a-zA-Z0-9-]+)/add/', chapters.view_add_chapter),
    url(r'^chapter/(?P<course_id>[a-zA-Z0-9-]+)/(?P<chapter_id>[a-zA-Z0-9-]+)/change/', chapters.view_change_chapter),
    url(r'^chapter/(?P<course_id>[a-zA-Z0-9-]+)/(?P<chapter_id>[a-zA-Z0-9-]+)/delete/', chapters.view_delete_chapter),
    url(r'^chapter/(?P<course_id>[a-zA-Z0-9-]+)/(?P<chapter_id>[a-zA-Z0-9-]+)/', chapters.view_chapter),
    url(r'^chapter/(?P<course_id>[a-zA-Z0-9-]+)/', chapters.view_chapter, name='chapter'),

    url(r'^course/add/', courses.view_add_course),
    url(r'^course/(?P<course_id>[a-zA-Z0-9-]+)/change/', courses.view_change_course),
    url(r'^course/(?P<course_id>[a-zA-Z0-9-]+)/delete/', courses.view_delete_course),
    url(r'^course/(?P<course_id>[a-zA-Z0-9-]+)/', courses.view_course),
    url(r'^course/', courses.view_course, name='course'),

    # url(r'^student/add/', students.view_add_student),
    # url(r'^student/(?P<student_id>[a-zA-Z0-9-]+)/change/', students.view_change_student),
    # url(r'^student/(?P<student_id>[a-zA-Z0-9-]+)/delete/', students.view_delete_student),
    # # url(r'^student/', overviews.overview, name='student'),
    # url(r'^student/', overviews.ELearningOverviewView.as_view(), name='student'),

    url(
        r'^participants/',
        include(
            'debatnl_online.urls.e_learning.participants',
            namespace='participants'
        )
    ),
    url(r'^', overviews.ELearningOverviewView.as_view(), name='overview'),
]
