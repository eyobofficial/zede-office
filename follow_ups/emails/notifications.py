from django.conf import settings
from django.urls import reverse

from follow_ups.emails.bases import BaseFollowUpEmail
from follow_ups.models import FollowUpEvent


class FollowUpEventEmail(BaseFollowUpEmail):
    template_name = 'email_4_follow_up_event.html'

    def __init__(self, event):
        self.event = event
        self.follow_up = self.event.follow_up
        self.recipient = self.follow_up.opportunity.person
        self.subject = self.event.subject

    def get_context_data(self, **kwargs):
        kwargs['follow_up'] = self.follow_up
        kwargs['event'] = self.event
        return super().get_context_data(**kwargs)

    def send(self):
        super().send()
        self.event.status = FollowUpEvent.SENT
        self.event.save()


class NoFollowUpEventSetupEmail(BaseFollowUpEmail):
    template_name = 'email_5_no_follow_up_event_setup.html'

    def __init__(self, follow_up):
        self.follow_up = follow_up
        self.recipient = self.follow_up.opportunity.assigned_to
        self.subject = 'Olaf wil graag je klanten iets sturen!'

    def get_context_data(self, **kwargs):
        path = reverse('follow_ups:overview')
        kwargs['url'] = f'{settings.APP_HOSTNAME}{path}'
        return super().get_context_data(**kwargs)
