from django.contrib.auth.models import User
from django.shortcuts import redirect
from django.views.generic import TemplateView, FormView, DeleteView

from debatnl_mobile.forms import TrainingForm
from debatnl_mobile.mixins import BaseTrainingView


class TrainingOverviewView(BaseTrainingView, TemplateView):
    template_name = 'debatnl_mobile/training_overview.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        organization_id = self.kwargs.get('id')
        if self.connection:
            context['training_list'] = self.consumer().retrieve_all(
                organization=organization_id
            )
            context['organization_id'] = organization_id
        return context


class TrainingCreateView(BaseTrainingView, FormView):
    template_name = 'debatnl_mobile/components/modals/training_form.html'
    form_class = TrainingForm

    def form_valid(self, form):
        organization_id = self.kwargs.get('id')
        form.save(self.consumer(), organization_id)
        return super().form_valid(form)


class TrainingUpdateView(BaseTrainingView, FormView):
    template_name = 'debatnl_mobile/components/modals/training_form.html'
    form_class = TrainingForm

    def get_initial(self):
        training = self.get_object()
        initial = {
            'name': training['name'],
            'date': training['date'],
            'trainer': User.objects.get(email=training['trainer_email'])
        }
        return initial

    def form_valid(self, form):
        organization_id = self.kwargs.get('id')
        training_id = self.kwargs.get('pk')
        form.save(self.consumer(), organization_id, training_id)
        return super().form_valid(form)


class TrainingDeleteView(BaseTrainingView, DeleteView):
    template_name = 'debatnl_mobile/components/modals/delete.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['warning_message'] = 'Weet je het zeker dat je deze ' \
                                     'training wilt verwijderen?'
        return context

    def delete(self, request, *args, **kwargs):
        training_pk = self.kwargs.get('pk')
        self.consumer().delete_object(training_pk)
        return redirect(self.get_success_url())
