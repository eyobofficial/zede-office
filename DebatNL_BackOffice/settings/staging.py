from .base import *

DEBUG = True

SECRET_KEY = env('SECRET_KEY')

ALLOWED_HOSTS = ['*']

INSTALLED_APPS += []

# Cache Settings

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
    }
}

# Logging Settings

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'file': {
            'level': env('DJANGO_LOG_LEVEL', default='INFO'),
            'class': 'logging.FileHandler',
            'filename': '/logs/django-debug.log',
        },
        'sentry': {
            'level': 'ERROR',
            'class': 'raven.contrib.django.raven_compat.'
                     'handlers.SentryHandler',
        },
    },
    'loggers': {
        'django': {
            'handlers': ['file', 'sentry'],
            'level': env('DJANGO_LOG_LEVEL', default='INFO'),
            'propagate': True,
        },
    },
}

# Host Settings

APP_HOSTNAME = 'http://debatnl-backoffice.cirx.io'
DEBATNL_ONLINE_API = 'http://debatnl-api.cirx.io'

# E-mail Settings

ALLOW_EMAIL_SENDING = True

# Frontend Settings

FRONTEND_URL = 'http://debatnl-online.cirx.io'

# Debat.NL Online Settings

DEBATNL_ONLINE_API_KEY = env('DEBATNL_ONLINE_API_KEY')
# This is for the readonly access to a participant dashboard.
DEBATNL_ONLINE_ADMIN_KEY = env('DEBATNL_ONLINE_ADMIN_KEY')

# Gravity Form API Settings

ALLOW_GRAVITY_FORM_REQUESTS = env.bool(
    'ALLOW_GRAVITY_FORM_REQUESTS', default=False
)
GRAVITY_FORM_ENDPOINT = env('GRAVITY_FORM_ENDPOINT')
GRAVITY_FORM_PUBLIC_KEY = env('GRAVITY_FORM_PUBLIC_KEY')
GRAVITY_FORM_PRIVATE_KEY = env('GRAVITY_FORM_PRIVATE_KEY')

# JotForm API Settings

JOT_FORM_API_KEY = env('JOT_FORM_API_KEY')
JOT_FORM_AWS_QUEUE = 'DebatNLBackoffice-JOTForm-Staging'
JOT_FORM_AWS_SUBMISSION_WEBHOOK = env('JOT_FORM_AWS_SUBMISSION_WEBHOOK')

# Mandrill Settings

MANDRILL_API_KEY = env('MANDRILL_API_KEY')

# AWS S3 Settings

AWS_ACCESS_KEY = env('AWS_ACCESS_KEY')
AWS_SECRET_KEY = env('AWS_SECRET_KEY')
AWS_S3_BUCKET = 'debatnl-online'
AWS_S3_BUCKET_SUB_PATH = 'staging'

# Vimeo API Settings

VIMEO_ACCESS_TOKEN = env('VIMEO_ACCESS_TOKEN')
VIMEO_PRESET_ID = 120402792
VIMEO_ALLOWED_DOMAIN = env('VIMEO_ALLOWED_DOMAIN')
