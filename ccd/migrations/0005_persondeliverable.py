# -*- coding: utf-8 -*-
# Generated by Django 1.11.16 on 2018-10-11 10:31
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('ccd', '0004_auto_20181004_2003'),
    ]

    operations = [
        migrations.CreateModel(
            name='PersonDeliverable',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateField()),
                ('training', models.CharField(max_length=120)),
                ('organization', models.CharField(max_length=120)),
                ('description', models.TextField(default='')),
                ('person', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='deliverables', to='ccd.Person')),
            ],
        ),
    ]
