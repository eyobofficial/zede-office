import pendulum
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import DeleteView, UpdateView, CreateView, \
    TemplateView, FormView

from backoffice.constants import GROUP_OFFERS
from backoffice.mixins import GroupAccessMixin
from backoffice.views import BaseEmailView
from offers.emails.notifications import RemovedOfferNotification, \
    ReviewOfferNotification, AcceptedOfferNotification, \
    DeniedOfferNotification, CreatedOfferNotification, \
    ChangedOfferNotification, MarketingOfferNotification
from offers.emails.reminders import CreatedOfferReminder, ReviewOfferReminder
from offers.forms import ReviewOfferForm
from offers.mixins import OfferViewMixin
from offers.models import Offer, OfferPhoto, OfferToken, OfferAbsenceSchedule
from offers.utilities.tokens import retrieve_offer_token
from shared.enums import RoleName


class OverviewView(GroupAccessMixin, TemplateView):
    template_name = 'offers/base.html'
    access_groups = [GROUP_OFFERS]

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        today = pendulum.today()
        finished_at = today.subtract(weeks=4)

        ongoing_offers = Offer.objects.exclude(status=Offer.FINISHED)
        finished_offers = Offer.objects.filter(status=Offer.FINISHED,
                                               finished_at__gte=finished_at)
        trainers = User.objects.filter(
            profile__roles__name=RoleName.TRAINER.value,
            is_active=True
        )

        editors = User.objects.filter(
            profile__roles__name=RoleName.EDITOR.value,
            is_active=True
        )

        context.update({
            'ongoing_offers': ongoing_offers.order_by('created_at'),
            'finished_offers': finished_offers.order_by('-finished_at'),
            'trainers': trainers,
            'editors': editors,
            'photos': OfferPhoto.objects.all(),
            'absence_schedules': OfferAbsenceSchedule.objects.all().order_by(
                'start_date', 'employee__first_name')
        })

        return context


class OfferCreate(GroupAccessMixin, OfferViewMixin, CreateView):
    access_groups = [GROUP_OFFERS]

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'modal_title': 'NIEUWE OFFERTE AANVRAGEN',
            'button_text': 'Offerte aanvragen',
            'editors': self.get_available_employees(context['editors'])
        })
        return context

    def form_valid(self, form):
        self.object = form.save()

        if self.object.type == Offer.TYPE_PHONECARE_SPRINGEST:
            MarketingOfferNotification(self.object).send()

        return super().form_valid(form)

    @staticmethod
    def get_available_employees(employees):
        available_employees = []
        for employee in employees:
            schedules = employee.offer_absence_schedules.all()
            is_absence = any([s.is_absence() for s in schedules])

            if not is_absence:
                available_employees.append(employee)

        return available_employees


class OfferUpdate(GroupAccessMixin, OfferViewMixin, UpdateView):
    access_groups = [GROUP_OFFERS]

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'modal_title': 'OFFERTE WIJZIGEN',
            'button_text': 'Offerte wijzigen'
        })
        return context

    def form_valid(self, form):
        self.object = form.save()
        is_marketing_type = self.object.type == Offer.TYPE_PHONECARE_SPRINGEST

        if 'type' in form.changed_data and is_marketing_type:
            MarketingOfferNotification(self.object).send()

        return super().form_valid(form)


class OfferDelete(GroupAccessMixin, DeleteView):
    model = Offer
    template_name = 'offers/components/modals/modal-delete.html'
    success_url = reverse_lazy('offers:overview')
    access_groups = [GROUP_OFFERS]

    def delete(self, request, *args, **kwargs):
        offer = self.get_object()
        RemovedOfferNotification(offer).send()
        return super().delete(request, *args, **kwargs)


def upload_offer(request):
    token = retrieve_offer_token(request, 'UPLOAD')

    if not token:
        template = 'offers/public/expired_token.html'
        return render(request, template)

    context = {
        'token': token.key,
        'email': token.email,
        'offer': token.offer
    }

    if request.method == 'POST':
        file_location_name = request.POST.get('file_location_name')
        file_location_year = request.POST.get('file_location_year')
        token.offer.file_location_name = file_location_name
        token.offer.file_location_year = file_location_year
        token.offer.save()

        # Send review notification
        ReviewOfferNotification(token.offer).send()

        # Delete token.
        token.delete()

        # Update context.
        context.update({'status': 'SUCCESSFUL'})

    template = 'offers/public/upload-offer/base.html'
    return render(request, template, context=context)


class ReviewOfferView(FormView):
    template_name = 'offers/public/review-offer/base.html'
    success_url = reverse_lazy('offers:review_submission')
    form_class = ReviewOfferForm
    token = None

    def dispatch(self, request, *args, **kwargs):
        self.token = self.retrieve_offer_token()

        if not self.token:
            template = 'offers/public/expired_token.html'
            return render(request, template)

        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        kwargs.update({'token': self.token, 'offer': self.token.offer})
        return super().get_context_data(**kwargs)

    def form_valid(self, form):
        form.save(self.token.offer)
        status = form.cleaned_data.get('status')

        if status == form.ACCEPTED:
            AcceptedOfferNotification(self.token.offer).send()
        else:
            DeniedOfferNotification(self.token.offer).send()

        self.token.delete()
        self.success_url = f'{self.success_url}?status={status}'
        return super().form_valid(form)

    def retrieve_offer_token(self):
        key = self.request.GET.get('token', self.request.POST.get('token'))
        email = self.request.GET.get('email', self.request.POST.get('email'))

        try:
            kwargs = {'key': key, 'email': email, 'type': OfferToken.REVIEW}
            token = OfferToken.objects.get(**kwargs)
        except (OfferToken.DoesNotExist, ValidationError):
            token = None

        return token


class ReviewSubmissionView(TemplateView):
    template_name = 'offers/public/review-offer/review_submission.html'


class AbsenceScheduleCreateView(GroupAccessMixin, CreateView):
    template_name = 'offers/components/modals/' \
                    'modal-absence-schedules-create.html'
    model = OfferAbsenceSchedule
    fields = ('start_date', 'end_date', 'employee')
    success_url = reverse_lazy('offers:overview')
    access_groups = [GROUP_OFFERS]

    def get_context_data(self, **kwargs):
        kwargs['editors'] = User.objects.filter(
            profile__roles__name=RoleName.EDITOR.value,
            is_active=True
        )
        return super().get_context_data(**kwargs)


class AbsenceScheduleDeleteView(GroupAccessMixin, DeleteView):
    template_name = 'offers/components/modals/' \
                    'modal-absence-schedules-delete.html'
    model = OfferAbsenceSchedule
    success_url = reverse_lazy('offers:overview')
    access_groups = [GROUP_OFFERS]


class AbsenceScheduleUpdateView(AbsenceScheduleCreateView, UpdateView):
    pass


class BaseOfferEmailView(BaseEmailView):
    url = 'admin:offers_offer_change'
    model = Offer


class BaseOfferReminderEmailView(BaseEmailView):
    url = 'admin:offers_offertoken_change'
    model = OfferToken


class CreatedOfferNotificationEmail(BaseOfferEmailView):
    email_class = CreatedOfferNotification
    email_name = 'created offer notification'


class CreatedOfferReminderEmail(BaseOfferReminderEmailView):
    email_class = CreatedOfferReminder
    email_name = 'created offer reminder'


class ChangedOfferNotificationEmail(BaseOfferEmailView):
    email_class = ChangedOfferNotification
    email_name = 'changed offer notification'


class RemovedOfferNotificationEmail(BaseOfferEmailView):
    email_class = RemovedOfferNotification
    email_name = 'removed offer notification'


class ReviewOfferNotificationEmail(BaseOfferEmailView):
    email_class = ReviewOfferNotification
    email_name = 'review offer notification'


class ReviewOfferReminderEmail(BaseOfferReminderEmailView):
    email_class = ReviewOfferReminder
    email_name = 'review offer reminder'


class AcceptedOfferNotificationEmail(BaseOfferEmailView):
    email_class = AcceptedOfferNotification
    email_name = 'accepted offer notification'


class DeniedOfferNotificationEmail(BaseOfferEmailView):
    email_class = DeniedOfferNotification
    email_name = 'denied offer notification'


class MarketingOfferNotificationEmail(BaseOfferEmailView):
    email_class = MarketingOfferNotification
    email_name = 'marketing offer notification'
