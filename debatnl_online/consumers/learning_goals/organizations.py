from debatnl_online.consumers.base import BaseConsumer


class OrganizationConsumer(BaseConsumer):
    def __init__(self):
        self.url = '{}/api/learning-goals/organizations'.format(self.base_url)


class OrganizationGoalConsumer(OrganizationConsumer):
    def __init__(self, organization_id):
        super().__init__()
        self.url = '{}/{}/goals'.format(self.url, organization_id)
