import datetime
import pytz
from django.test import TestCase
from post_office.models import Email

from backoffice.tests.factories import TrainingFactory
from debatnl_registration.models import PlannedTrainingToken, Attendance
from debatnl_registration.tests.factories import RegistrationFactory, \
    ParticipantFactory, PlannedTrainingFactory, TrainingDateFactory, \
    AttendanceFactory
from debatnl_registration.tasks import \
    generate_attendees_list_reminder, \
    generate_training_continuation_notifications, \
    generate_book_hall_reminder, generate_hall_availability_reminder, \
    generate_hall_cancellation_reminder, generate_update_website_reminder, \
    generate_attendees_amount_notification, \
    generate_attendees_amount_reminder, \
    generate_training_preparation_notifications, \
    generate_review_training_notification
from shared.enums import RoleName
from users.models import Role
from users.tests.factories import UserFactory


class AttendeesListReminderTest(TestCase):
    fixtures = ['roles']

    def setUp(self):
        tz = pytz.timezone('Europe/Amsterdam')
        self.today = datetime.datetime.now(tz).date()

        role = Role.objects.get(name=RoleName.ADMIN.value)
        admin = UserFactory()
        admin.profile.roles.add(role)

        role = Role.objects.get(name=RoleName.TRAINER.value)
        trainers = UserFactory.create_batch(2)
        for trainer in trainers:
            trainer.profile.roles.add(role)

        planned_t = PlannedTrainingFactory(training__name='Bewust Beinvloeden')
        self.training_date = TrainingDateFactory(planned_training=planned_t)
        self.training_date.trainers.add(*trainers)
        AttendanceFactory.create_batch(3, training_date=self.training_date)

    def test_creation_on_n_days(self):
        for n in range(-10, 10):
            input_date = self.today + datetime.timedelta(days=n)
            Email.objects.all().delete()
            PlannedTrainingToken.objects.all().delete()

            with self.subTest(delta=n, training_date=input_date):
                PlannedTrainingToken.objects.create(
                    type='ATTENDANCE LIST',
                    planned_training=self.training_date.planned_training,
                    training_date=self.training_date
                )
                self.training_date.date = input_date
                self.training_date.save()

                generate_attendees_list_reminder()

                result = 2 if n <= -1 else 0
                self.assertEqual(Email.objects.count(), result)

    def test_cancelled_training_days(self):
        PlannedTrainingToken.objects.create(
            type='ATTENDANCE LIST',
            planned_training=self.training_date.planned_training,
            training_date=self.training_date
        )

        self.training_date.status = 'CANCELLED'
        self.training_date.date = self.today + datetime.timedelta(days=-1)
        self.training_date.save()

        generate_attendees_list_reminder()

        self.assertEqual(Email.objects.count(), 0)


class TrainingContinuationNotificationTest(TestCase):
    fixtures = ['roles']

    def setUp(self):
        role = Role.objects.get(name=RoleName.ADMIN.value)
        self.admin = UserFactory()
        self.admin.profile.roles.add(role)
        self.registration = RegistrationFactory()
        self.participant = ParticipantFactory(registration=self.registration)
        training = TrainingFactory(name='Ontspannen Presenteren')
        self.planned_training = PlannedTrainingFactory(training=training)
        training_date_1 = TrainingDateFactory(
            sequence=1,
            status='FINISHED',
            planned_training=self.planned_training
        )

        training_date_2 = TrainingDateFactory(
            sequence=2,
            status='FINISHED',
            planned_training=self.planned_training
        )
        training_date_3 = TrainingDateFactory(
            sequence=3,
            status='FINISHED',
            planned_training=self.planned_training
        )

        AttendanceFactory(
            training_date=training_date_1,
            participant=self.participant
        )

        AttendanceFactory(
            training_date=training_date_2,
            participant=self.participant
        )

        AttendanceFactory(
            training_date=training_date_3,
            participant=self.participant
        )

    def test_finished_training_date(self):
        tz = pytz.timezone('Europe/Amsterdam')
        today = datetime.datetime.now(tz)

        training_date = self.planned_training.training_dates.get(sequence=3)

        for n in range(-2, 1):
            training_date.date = today + datetime.timedelta(days=n)
            training_date.save()

            generate_training_continuation_notifications()

        self.assertEqual(Attendance.objects.count(), 3)
        self.assertEqual(Email.objects.count(), 1)

    def test_pending_training_date(self):
        tz = pytz.timezone('Europe/Amsterdam')
        today = datetime.datetime.now(tz)

        training_date = self.planned_training.training_dates.get(sequence=3)

        for n in range(-2, 1):
            training_date.date = today + datetime.timedelta(days=n)
            training_date.status = 'PENDING'
            training_date.save()

            generate_training_continuation_notifications()

        self.assertEqual(Attendance.objects.count(), 3)
        self.assertEqual(Email.objects.count(), 0)

    def test_cancelled_training_date(self):
        tz = pytz.timezone('Europe/Amsterdam')
        today = datetime.datetime.now(tz)

        training_date = self.planned_training.training_dates.get(sequence=3)

        for n in range(-2, 1):
            training_date.date = today + datetime.timedelta(days=n)
            training_date.status = 'CANCELLED'
            training_date.save()

            generate_training_continuation_notifications()

        self.assertEqual(Attendance.objects.count(), 3)
        self.assertEqual(Email.objects.count(), 0)


class BookHallReminderTest(TestCase):
    fixtures = ['roles']

    def setUp(self):
        tz = pytz.timezone('Europe/Amsterdam')
        self.today = datetime.datetime.now(tz)

        role = Role.objects.get(name=RoleName.ADMIN.value)
        admin = UserFactory()
        admin.profile.roles.add(role)

        role = Role.objects.get(name=RoleName.HALL.value)
        hall_contact = UserFactory()
        hall_contact.profile.roles.add(role)

        self.training_date = TrainingDateFactory(
            planned_training__training__name='Bewust Beinvloeden'
        )

    def test_creation_on_n_days(self):
        for n in range(-10, 10):
            input_date = self.today + datetime.timedelta(days=n)
            Email.objects.all().delete()
            PlannedTrainingToken.objects.all().delete()

            with self.subTest(delta=n, training_date=input_date):
                token = PlannedTrainingToken.objects.create(
                    type='BOOK HALL',
                    planned_training=self.training_date.planned_training,
                )
                token.created_at = input_date
                token.save()

                generate_book_hall_reminder()

                result = 1 if n in [-3, -6, -9] else 0
                self.assertEqual(Email.objects.count(), result)

    def test_cancelled_training_days(self):
        self.training_date.status = 'CANCELLED'
        self.training_date.save()

        token = PlannedTrainingToken.objects.create(
            type='BOOK HALL',
            planned_training=self.training_date.planned_training,
        )
        token.created_at = self.today + datetime.timedelta(days=-3)
        token.save()

        generate_book_hall_reminder()

        self.assertEqual(Email.objects.count(), 0)


class HallAvailabilityReminderTest(TestCase):
    fixtures = ['roles']

    def setUp(self):
        tz = pytz.timezone('Europe/Amsterdam')
        self.today = datetime.datetime.now(tz)

        role = Role.objects.get(name=RoleName.ADMIN.value)
        admin = UserFactory()
        admin.profile.roles.add(role)

        self.training_date = TrainingDateFactory(
            planned_training__training__name='Bewust Beinvloeden'
        )

    def test_creation_on_n_days(self):
        for n in range(-10, 10):
            input_date = self.today + datetime.timedelta(days=n)
            Email.objects.all().delete()
            PlannedTrainingToken.objects.all().delete()

            with self.subTest(delta=n, training_date=input_date):
                token = PlannedTrainingToken.objects.create(
                    type='HALL AVAILABLE',
                    planned_training=self.training_date.planned_training,
                )
                token.created_at = input_date
                token.save()

                generate_hall_availability_reminder()

                result = 1 if n <= -3 else 0
                self.assertEqual(Email.objects.count(), result)

    def test_cancelled_training_days(self):
        self.training_date.status = 'CANCELLED'
        self.training_date.save()

        token = PlannedTrainingToken.objects.create(
            type='HALL AVAILABLE',
            planned_training=self.training_date.planned_training,
        )
        token.created_at = self.today + datetime.timedelta(days=-3)
        token.save()

        generate_hall_availability_reminder()

        self.assertEqual(Email.objects.count(), 0)


class HallCancellationReminderTest(TestCase):
    fixtures = ['roles']

    def setUp(self):
        tz = pytz.timezone('Europe/Amsterdam')
        self.today = datetime.datetime.now(tz)

        role = Role.objects.get(name=RoleName.ADMIN.value)
        admin = UserFactory()
        admin.profile.roles.add(role)

        role = Role.objects.get(name=RoleName.HALL.value)
        hall_contact = UserFactory()
        hall_contact.profile.roles.add(role)

        self.training_date = TrainingDateFactory(
            planned_training__training__name='Bewust Beinvloeden'
        )

    def test_creation_on_n_days(self):
        for n in range(-10, 10):
            input_date = self.today + datetime.timedelta(days=n)
            Email.objects.all().delete()
            PlannedTrainingToken.objects.all().delete()

            with self.subTest(delta=n, training_date=input_date):
                token = PlannedTrainingToken.objects.create(
                    type='CANCEL HALL',
                    planned_training=self.training_date.planned_training,
                )
                token.created_at = input_date
                token.save()

                generate_hall_cancellation_reminder()

                result = 1 if n <= -3 else 0
                self.assertEqual(Email.objects.count(), result)

    def test_cancelled_training_days(self):
        self.training_date.status = 'CANCELLED'
        self.training_date.save()

        token = PlannedTrainingToken.objects.create(
            type='CANCEL HALL',
            planned_training=self.training_date.planned_training,
        )
        token.created_at = self.today + datetime.timedelta(days=-3)
        token.save()

        generate_hall_availability_reminder()

        self.assertEqual(Email.objects.count(), 0)


class UpdateWebsiteReminderTest(TestCase):
    fixtures = ['roles']

    def setUp(self):
        tz = pytz.timezone('Europe/Amsterdam')
        self.today = datetime.datetime.now(tz)

        role = Role.objects.get(name=RoleName.ADMIN.value)
        admin = UserFactory()
        admin.profile.roles.add(role)

        role = Role.objects.get(name=RoleName.WEBSITE.value)
        website_contact = UserFactory()
        website_contact.profile.roles.add(role)

        self.training_date = TrainingDateFactory(
            planned_training__training__name='Bewust Beinvloeden'
        )

    def test_creation_on_n_days(self):
        for n in range(-10, 10):
            input_date = self.today + datetime.timedelta(days=n)
            Email.objects.all().delete()
            PlannedTrainingToken.objects.all().delete()

            with self.subTest(delta=n, training_date=input_date):
                token = PlannedTrainingToken.objects.create(
                    type='UPDATE WEBSITE',
                    planned_training=self.training_date.planned_training,
                )
                token.created_at = input_date
                token.save()

                generate_update_website_reminder()

                result = 1 if n <= -3 else 0
                self.assertEqual(Email.objects.count(), result)

    def test_cancelled_training_days(self):
        self.training_date.status = 'CANCELLED'
        self.training_date.save()

        token = PlannedTrainingToken.objects.create(
            type='UPDATE WEBSITE',
            planned_training=self.training_date.planned_training,
        )
        token.created_at = self.today + datetime.timedelta(days=-3)
        token.save()

        generate_update_website_reminder()

        self.assertEqual(Email.objects.count(), 0)


class AttendeesLimitAmountNotificationTest(TestCase):
    fixtures = ['roles']

    def setUp(self):
        tz = pytz.timezone('Europe/Amsterdam')
        self.today = datetime.datetime.now(tz)

        role = Role.objects.get(name=RoleName.ADMIN.value)
        admin = UserFactory()
        admin.profile.roles.add(role)

        self.training_date = TrainingDateFactory(
            planned_training__training__name='Bewust Beinvloeden'
        )

    def test_with_n_attendees(self):
        for n in range(1, 15):
            Email.objects.all().delete()
            AttendanceFactory(training_date=self.training_date)

            self.training_date.need_large_hall = False
            self.training_date.save()

            with self.subTest(n_participants=n):
                generate_attendees_amount_notification()

                result = 1 if n > 9 else 0

                self.training_date.refresh_from_db()
                self.assertEqual(Email.objects.count(), result)
                self.assertEqual(
                    self.training_date.need_large_hall,
                    bool(result)
                )

    def test_when_already_created(self):
        AttendanceFactory.create_batch(10, training_date=self.training_date)
        self.training_date.need_large_hall = True
        self.training_date.save()

        generate_attendees_amount_notification()

        self.training_date.refresh_from_db()
        self.assertEqual(Email.objects.count(), 0)
        self.assertTrue(self.training_date.need_large_hall)

    def test_cancelled_training_days(self):
        AttendanceFactory.create_batch(10, training_date=self.training_date)
        self.training_date.status = 'CANCELLED'
        self.training_date.save()

        generate_attendees_amount_notification()

        self.training_date.refresh_from_db()
        self.assertEqual(Email.objects.count(), 0)
        self.assertFalse(self.training_date.need_large_hall)


class AttendeesLimitAmountReminderTest(TestCase):
    fixtures = ['roles']

    def setUp(self):
        tz = pytz.timezone('Europe/Amsterdam')
        self.today = datetime.datetime.now(tz)

        role = Role.objects.get(name=RoleName.ADMIN.value)
        admin = UserFactory()
        admin.profile.roles.add(role)

        self.training_date = TrainingDateFactory(
            planned_training__training__name='Bewust Beinvloeden'
        )

    def test_creation_on_n_days(self):
        for n in range(-10, 10):
            input_date = self.today + datetime.timedelta(days=n)
            Email.objects.all().delete()
            PlannedTrainingToken.objects.all().delete()

            with self.subTest(delta=n, training_date=input_date):
                token = PlannedTrainingToken.objects.create(
                    type='ATTENDEES AMOUNT',
                    planned_training=self.training_date.planned_training,
                    training_date=self.training_date,
                )
                token.created_at = input_date
                token.save()

                generate_attendees_amount_reminder()

                result = 1 if n <= -3 else 0
                self.assertEqual(Email.objects.count(), result)

    def test_cancelled_training_days(self):
        self.training_date.status = 'CANCELLED'
        self.training_date.save()

        token = PlannedTrainingToken.objects.create(
            type='ATTENDEES AMOUNT',
            planned_training=self.training_date.planned_training,
            training_date=self.training_date,
        )
        token.created_at = self.today + datetime.timedelta(days=-3)
        token.save()

        generate_attendees_amount_reminder()

        self.assertEqual(Email.objects.count(), 0)


class TrainingPreparationNotificationTest(TestCase):
    fixtures = ['roles']

    def setUp(self):
        tz = pytz.timezone('Europe/Amsterdam')
        self.today = datetime.datetime.now(tz)

        role = Role.objects.get(name=RoleName.ADMIN.value)
        admin = UserFactory()
        admin.profile.roles.add(role)

        role = Role.objects.get(name=RoleName.HALL.value)
        hall_contact = UserFactory()
        hall_contact.profile.roles.add(role)

        role = Role.objects.get(name=RoleName.TRAINER.value)
        trainer = UserFactory()
        trainer.profile.roles.add(role)

        self.training_date = TrainingDateFactory(
            planned_training__training__name='Bewust Beinvloeden'
        )
        self.training_date.trainers.add(trainer)

    def test_creation_on_n_days(self):
        for n in range(-10, 10):
            input_date = self.today + datetime.timedelta(days=n)
            Email.objects.all().delete()

            with self.subTest(delta=n, training_date=input_date):
                self.training_date.date = input_date
                self.training_date.save()

                generate_training_preparation_notifications()

                result = 3 if n == 3 else 0
                self.assertEqual(Email.objects.count(), result)

    def test_cancelled_training_days(self):
        self.training_date.status = 'CANCELLED'
        self.training_date.date = self.today + datetime.timedelta(days=3)
        self.training_date.save()

        generate_training_preparation_notifications()

        self.assertEqual(Email.objects.count(), 0)


class ReviewTrainingNotificationTest(TestCase):
    fixtures = ['roles']

    def setUp(self):
        tz = pytz.timezone('Europe/Amsterdam')
        self.today = datetime.datetime.now(tz)

        role = Role.objects.get(name=RoleName.ADMIN.value)
        admin = UserFactory()
        admin.profile.roles.add(role)

        self.training_date = TrainingDateFactory(
            planned_training__training__name='Bewust Beinvloeden'
        )

        AttendanceFactory.create_batch(3, training_date=self.training_date)

    def test_creation_1_day_training(self):
        for n in range(-10, 10):
            input_date = self.today + datetime.timedelta(days=n)
            Email.objects.all().delete()

            with self.subTest(delta=n, training_date=input_date):
                for attendee in self.training_date.attendees.all():
                    generate_review_training_notification(
                        attendee,
                        input_date,
                        1
                    )

                    self.assertTrue(attendee.has_feedback)

                    attendee.has_feedback = False
                    attendee.save()

                self.assertEqual(Email.objects.count(), 3)

    def test_creation_3_day_training(self):
        for n in range(-10, 10):
            input_date = self.today + datetime.timedelta(days=n)
            Email.objects.all().delete()

            with self.subTest(delta=n, training_date=input_date):
                for attendee in self.training_date.attendees.all():
                    generate_review_training_notification(
                        attendee,
                        input_date,
                        3
                    )

                    self.assertEqual(attendee.has_feedback, n < 0)

                    attendee.has_feedback = False
                    attendee.save()

                result = 3 if n < 0 else 0
                self.assertEqual(Email.objects.count(), result)

    def test_when_already_created(self):
        tomorrow = self.today + datetime.timedelta(days=-1)

        for attendee in self.training_date.attendees.all():
            attendee.has_feedback = True
            attendee.save()

            generate_review_training_notification(attendee, self.today, 1)
            generate_review_training_notification(attendee, tomorrow, 3)

        self.assertEqual(Email.objects.count(), 0)
