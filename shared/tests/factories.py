import factory

from shared.models import Training, Infographic


class TrainingFactory(factory.django.DjangoModelFactory):
    name = factory.Faker('sentence', nb_words=2)

    class Meta:
        model = Training


class InfographicFactory(factory.django.DjangoModelFactory):
    label = factory.Sequence(lambda n: f'Page {n}')
    file = factory.Faker('file_name', extension='pdf')
    preview = factory.Faker('file_name', extension='jpeg')

    class Meta:
        model = Infographic
