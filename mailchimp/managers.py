from django.db import models


class FeedbackSubmissionManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(form_id=70395163921962)

    def create(self, **kwargs):
        kwargs.update({'form_id': 70395163921962})
        return super().create(**kwargs)
