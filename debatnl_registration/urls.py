from django.conf.urls import url

from debatnl_registration import views
from debatnl_registration.views import TrainingDateDelete, \
    BloomvilleFeedView, SpringestFeedView, AvailableTrainingDatesListView, \
    PlannedTrainingCreateView

urlpatterns = [
    url(
        r'^ongoing-trainings/(?P<pk>[a-zA-Z0-9-]+)/'
        r'attendees/(?P<attendee_id>[a-zA-Z0-9-]+)/change/$',
        views.change_attendee_training,
        name='change_attendee_training'
    ),
    url(
        r'^ongoing-trainings/(?P<pk>[a-zA-Z0-9-]+)/'
        r'attendees/(?P<attendee_id>[a-zA-Z0-9-]+)/cancel/$',
        views.cancel_attendance,
        name='cancel_attendance'
    ),
    url(r'^ongoing-trainings/(?P<pk>[a-zA-Z0-9-]+)/trainers/add/$',
        views.add_trainers, name='add_trainers'),
    url(
        r'^ongoing-trainings/(?P<pk>[a-zA-Z0-9-]+)/cancel/$',
        TrainingDateDelete.as_view(),
        name='cancel_training_date'
    ),
    url(
        r'^ongoing-trainings/(?P<pk>[a-zA-Z0-9-]+)/$',
        AvailableTrainingDatesListView.as_view(),
        name='available_training_dates'
    ),
    url(r'^confirm/jotform-update/$', views.confirm_jot_form_update,
        name='confirm_jot_form_update'),
    url(r'^confirm/website-update/$', views.confirm_website_update,
        name='confirm_website_update'),
    url(r'^confirm/training-update/$', views.confirm_training_update,
        name='confirm_training_update'),
    url(r'^confirm/attendees-amount/$', views.confirm_attendees_amount,
        name='confirm_attendees_amount'),
    url(r'^confirm/attendance-list/$', views.confirm_attendance_list,
        name='confirm_attendance_list'),
    url(r'^hall-availability/$', views.hall_availability,
        name='hall_availability'),
    url(r'^hall-cancellation/$', views.hall_cancellation,
        name='hall_cancellation'),
    url(r'^hooks/$', views.hooks, name='hooks'),
    url(r'^add/$', PlannedTrainingCreateView.as_view(), name='add'),
    url(r'^feeds/springest/$', SpringestFeedView.as_view()),
    url(r'^feeds/bloomville/v1/courses/$', BloomvilleFeedView.as_view()),
    url(r'^feeds/bloomville/courses/$', views.feed_bloomville_courses),
    url(r'^feeds/bloomville/classes/$', views.feed_bloomville_classes),
    url(r'^$', views.overview, name='overview'),
]
