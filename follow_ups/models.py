import uuid

import pendulum
from django.conf import settings
from django.db import models
from django.utils import timezone
from tinymce.models import HTMLField

from webcrm.client import WebCRMClient
from webcrm.models import Opportunity, Delivery


class FollowUp(models.Model):
    NEW = 'NEW'
    NO_SALES_RESPONSIBLE = 'NO_SALES_RESPONSIBLE'
    NO_CUSTOMER_CONTACT = 'NO_CUSTOMER_CONTACT'
    READY = 'READY'
    DELETED = 'DELETED'

    status_choices = (
        (NEW, 'New'),
        (NO_SALES_RESPONSIBLE, 'No Sales Responsible'),
        (NO_CUSTOMER_CONTACT, 'No Customer Contact'),
        (READY, 'Ready'),
        (DELETED, 'Deleted')
    )

    id = models.UUIDField(primary_key=True, editable=False, default=uuid.uuid4)
    opportunity = models.OneToOneField(Opportunity, on_delete=models.CASCADE,
                                       null=True, related_name='follow_up')
    status = models.CharField(max_length=50, choices=status_choices,
                              default=NEW)

    class Meta:
        ordering = ('opportunity__organization',)
        permissions = (
            ('view_all_followups', 'Can see all follow_ups'),
        )

    def __str__(self):
        return f'{self.opportunity} followup'

    def get_past_deliveries(self):
        kwargs = {
            'organization': self.opportunity.organization,
            'order_date__lte': pendulum.today(tz=settings.TIME_ZONE),
            'state': Delivery.STATE_AVAILABLE
        }
        return Delivery.objects.filter(**kwargs).order_by('order_date')

    def get_future_deliveries(self):
        kwargs = {
            'organization': self.opportunity.organization,
            'order_date__gt': pendulum.today(tz=settings.TIME_ZONE),
            'state': Delivery.STATE_AVAILABLE
        }
        return Delivery.objects.filter(**kwargs).order_by('order_date')

    def get_upcoming_events(self):
        today = timezone.now().date()
        kwargs = {'status': FollowUpEvent.PENDING, 'date__gt': today}
        return self.events.filter(**kwargs)

    def add_new_history_entry(self, description):
        client = WebCRMClient(settings.WEBCRM_AUTH_CODE)
        opportunity = client.get_opportunity_by_id(self.opportunity.id)
        history = opportunity['OpportunityHistory']
        now = timezone.now().strftime("%d-%m-%Y %H:%M")
        col_now = f'<td>{now}</td>'
        col_user = '<td>Olaf</td>'
        col_description = f'<td colspan="3">{description}</td>'
        entry = f'<tr>{col_now}{col_user}{col_description}</tr>'
        return f'{entry}{history}'

    def update_webcrm_opportunity(self, upcoming_event):
        description = 'Automatische opvolgingsmail door Olaf verzonden.'
        new_history = self.add_new_history_entry(description)
        dt = upcoming_event.date
        dt = pendulum.datetime(year=dt.year, month=dt.month, day=dt.day)
        payload = {
            'OpportunityId': self.opportunity.pk,
            'OpportunityOrderDate': dt.isoformat(),
            'OpportunityNextFollowUp': dt.isoformat(),
            'OpportunityHistory': new_history
        }
        self.opportunity.update_on_webcrm(payload)


class FollowUpEvent(models.Model):
    TYPE_OT_INVITATION = 'OPEN TRAINING INVITATION'
    TYPE_INFOGRAPHIC = 'INFOGRAPHIC'
    TYPE_OTHER = 'OTHER'

    TYPE_CHOICES = (
        (TYPE_OT_INVITATION, 'Open Training Invitation'),
        (TYPE_INFOGRAPHIC, 'Infographic'),
        (TYPE_OTHER, 'Other'),
    )

    PENDING = 'PENDING'
    SENT = 'SENT'

    STATUS_CHOICES = (
        (PENDING, 'Pending'),
        (SENT, 'Sent'),
    )

    id = models.UUIDField(primary_key=True, editable=False, default=uuid.uuid4)
    date = models.DateField()
    type = models.CharField(max_length=35, default=TYPE_OTHER,
                            choices=TYPE_CHOICES)
    subject = models.CharField(max_length=100)
    message = HTMLField()
    button_text = models.CharField(max_length=50, blank=True, default='')
    button_url = models.URLField(blank=True, default='')
    follow_up = models.ForeignKey(FollowUp, related_name='events')
    status = models.CharField(max_length=25, choices=STATUS_CHOICES,
                              default=PENDING)

    class Meta:
        ordering = ('date', )

    def __str__(self):
        return f'Event: {self.date} - {self.subject}'

    def get_next_event(self):
        return self.follow_up.get_upcoming_events().exclude(pk=self.pk).first()
