import pendulum
import uuid

from django.conf import settings
from django.db import models

from webcrm.models import Delivery


class Person(models.Model):
    first_name = models.CharField(max_length=120)
    last_name = models.CharField(max_length=120)
    email = models.EmailField()
    is_active = models.BooleanField('Active', default=True)

    class Meta:
        ordering = ('first_name', 'last_name',)

    def __str__(self):
        return self.email

    def get_full_name(self):
        return f'{self.first_name} {self.last_name}'

    def get_ccd_deliveries(self):
        deliveries = Delivery.objects.filter(trainers__email=self.email)
        deliveries = deliveries.filter(state=Delivery.STATE_AVAILABLE)
        deliveries = deliveries.exclude(opportunity_custom5='Ja')
        deliveries = deliveries.exclude(product='Digitale intake')
        deliveries = deliveries.exclude(product='Digitale outtake')
        deliveries = deliveries.exclude(product='Debat.NL Online')
        deliveries = deliveries.exclude(organization=755)
        return deliveries

    def get_future_ccd_deliveries(self):
        today = pendulum.today(settings.TIME_ZONE)
        deliveries = self.get_ccd_deliveries()
        deliveries = deliveries.filter(order_date__gte=today)
        return deliveries

    def get_monthly_ccd_deliveries(self):
        today = pendulum.today(settings.TIME_ZONE)

        # On the 27th of each month the CDD Purchase E-mail will be sent. For
        # this reason we set the conditional on 27.
        if today.day < 27:
            today = today.subtract(months=1)

        month_start = today.start_of('month')
        month_end = today.end_of('month')
        deliveries = self.get_ccd_deliveries()
        deliveries = deliveries.filter(order_date__gte=month_start)
        deliveries = deliveries.filter(order_date__lte=month_end)
        deliveries = deliveries.order_by('order_date')
        return deliveries


class URLToken(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    person = models.ForeignKey(
        Person,
        related_name='url_tokens',
        on_delete=models.CASCADE
    )
    valid_until = models.DateField()

    class Meta:
        verbose_name = 'URL Token'
        verbose_name_plural = 'URL Tokens'

    def save(self, *args, **kwargs):
        if not self.valid_until:
            today = pendulum.today(settings.TIME_ZONE)
            self.valid_until = today.add(days=13).date()
        return super().save(*args, **kwargs)

    def is_valid(self):
        """
        Checks if the token is still valid
        """
        today = pendulum.today(settings.TIME_ZONE).date()
        return today < self.valid_until
