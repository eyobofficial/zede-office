from django.contrib.auth.models import User
from django.urls import reverse_lazy
from django.views.generic.edit import FormMixin

from offers.forms import InvestmentFormSet, OfferForm
from offers.models import OfferPhoto, Offer
from offers.emails.notifications import CreatedOfferNotification, \
    RemovedOfferNotification, ChangedOfferNotification
from shared.enums import RoleName


class OfferViewMixin(FormMixin):
    """
    A mixin that provides a way to show and handle an offer form in views.
    """
    model = Offer
    form_class = OfferForm
    template_name = 'offers/components/modals/modal-offer.html'
    success_url = reverse_lazy('offers:overview')
    employee_qs = User.objects.order_by('first_name', 'last_name')
    request = None
    object = None
    prev_object = None

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        data = self.request.POST

        trainers = self.employee_qs.filter(
            profile__roles__name=RoleName.TRAINER.value,
            is_active=True
        )
        editors = self.employee_qs.filter(
            profile__roles__name=RoleName.EDITOR.value,
            is_active=True
        )

        photos = OfferPhoto.objects.all().order_by('name')

        if data:
            formset = InvestmentFormSet(data, instance=self.object)
        else:
            formset = InvestmentFormSet(instance=self.object)
            formset.extra = formset.extra if self.object else 1

        context.update({
            'formset_investments': formset,
            'trainers': trainers,
            'editors': editors,
            'photos': photos,
        })
        return context

    def form_valid(self, form):
        context = self.get_context_data()
        formset = context['formset_investments']

        if formset.is_valid():
            self.object = form.save()
            formset.instance = self.object
            formset.save()

            self.send_notification_email(form, formset)

            # Set invalidate current files.
            self.object.files.all().update(is_denied=True)

        return super().form_valid(form)

    def send_notification_email(self, form, formset):
        created = len(formset.initial_forms) == 0

        if created:
            CreatedOfferNotification(self.object).send()

        elif 'editor' in form.changed_data:
            prev_object = Offer(
                organization=form.initial['organization'],
                editor=User.objects.get(pk=form.initial['editor'])
            )

            RemovedOfferNotification(prev_object).send()
            CreatedOfferNotification(formset.instance).send()

        else:
            ChangedOfferNotification(formset.instance).send()
