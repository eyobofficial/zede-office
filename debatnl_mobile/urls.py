from django.conf.urls import url

from debatnl_mobile.views.shared import SignS3DebatNLMobileView
from .views.organizations import OrganizationOverviewView, \
    OrganizationCreateView, OrganizationUpdateView, OrganizationDeleteView
from .views.trainings import TrainingOverviewView, TrainingCreateView, \
    TrainingUpdateView, TrainingDeleteView
from .views.challenges import ChallengeOverviewView, ChallengeCreateView, \
    ChallengeUpdateView, ChallengeDeleteView, ParticipantDetailView, \
    ParticipantCreateView, ParticipantUpdateView, ParticipantDeleteView, \
    ChallengeSubmissionUpdateView

urlpatterns = [
    url(
        r'^$',
        OrganizationOverviewView.as_view(),
        name='organization-overview'
    ),
    url(
        r'^sign-s3-upload-url/',
        SignS3DebatNLMobileView.as_view(),
        name='sign_s3_upload_url'
    ),
    url(
        r'^organizations/create/',
        OrganizationCreateView.as_view(),
        name='organization-create'
    ),
    url(
        r'^organizations/(?P<pk>[0-9a-f-]+)/update/$',
        OrganizationUpdateView.as_view(),
        name='organization-update'
    ),
    url(
        r'^organizations/(?P<pk>[0-9a-f-]+)/delete/$',
        OrganizationDeleteView.as_view(),
        name='organization-delete'
    ),
    url(
        r'^organizations/(?P<id>[0-9a-f-]+)/trainings/$',
        TrainingOverviewView.as_view(),
        name='training-overview'
    ),
    url(
        r'^organizations/(?P<id>[0-9a-f-]+)/trainings/create/$',
        TrainingCreateView.as_view(),
        name='training-create'
    ),
    url(
        r'^organizations/(?P<id>[0-9a-f-]+)/trainings/(?P<pk>[0-9a-f-]+)/update/$',
        TrainingUpdateView.as_view(),
        name='training-update'
    ),
    url(
        r'^organizations/(?P<id>[0-9a-f-]+)/trainings/(?P<pk>[0-9a-f-]+)/delete/$',
        TrainingDeleteView.as_view(),
        name='training-delete'
    ),
    url(
        r'^trainings/(?P<id>[0-9a-f-]+)/challenges/$',
        ChallengeOverviewView.as_view(),
        name='challenge-overview'
    ),
    url(
        r'^trainings/(?P<id>[0-9a-f-]+)/challenges/create/$',
        ChallengeCreateView.as_view(),
        name='challenge-create'
    ),
    url(
        r'^trainings/(?P<id>[0-9a-f-]+)/challenges/(?P<pk>[0-9a-f-]+)/update/$',
        ChallengeUpdateView.as_view(),
        name='challenge-update'
    ),
    url(
        r'^trainings/(?P<id>[0-9a-f-]+)/challenges/(?P<pk>[0-9a-f-]+)/delete/$',
        ChallengeDeleteView.as_view(),
        name='challenge-delete'
    ),
    url(
        r'^trainings/(?P<id>[0-9a-f-]+)/participants/(?P<pk>[0-9]+)/$',
        ParticipantDetailView.as_view(),
        name='participant-detail'
    ),
    url(
        r'^trainings/(?P<id>[0-9a-f-]+)/participants/create/$',
        ParticipantCreateView.as_view(),
        name='participant-create'
    ),
    url(
        r'^trainings/(?P<id>[0-9a-f-]+)/participants/(?P<pk>[0-9]+)/update/$',
        ParticipantUpdateView.as_view(),
        name='participant-update'
    ),
    url(
        r'^trainings/(?P<id>[0-9a-f-]+)/participants/(?P<pk>[0-9]+)/delete/$',
        ParticipantDeleteView.as_view(),
        name='participant-delete'
    ),
    url(
        r'^trainings/(?P<id>[0-9a-f-]+)/submissions/(?P<pk>[0-9a-f-]+)/$',
        ChallengeSubmissionUpdateView.as_view(),
        name='submission-update'
    )
]
