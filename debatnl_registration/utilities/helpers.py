import datetime
import logging
import os
from io import BytesIO

import pytz
from PyPDF2 import PdfFileReader, PdfFileWriter
from django.conf import settings
from django.contrib.auth.models import User
from django.core.files.base import ContentFile
from reportlab.lib.colors import HexColor
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.pdfgen import canvas

from debatnl_registration.models import Registration, Participant, \
    PlannedTraining, \
    TrainingDate, Attendance

logger = logging.getLogger('django')


def persist_registration(data):
    training_dates = data.pop('training_dates')
    data_participants = data.pop('participants')
    registration = Registration.objects.create(**data)

    participants = []
    for data_participant in data_participants:
        participants.append(
            Participant.objects.create(
                registration=registration,
                **data_participant
            )
        )

    try:
        attendance = []
        for date in training_dates:
            training_date = TrainingDate.objects.get(
                date=date,
                planned_training__training__name=registration.training,
                status='PENDING'
            )

            for participant in participants:
                attendance.append(
                    Attendance(
                        training_date=training_date,
                        participant=participant
                    )
                )

        Attendance.objects.bulk_create(attendance)

    except TrainingDate.DoesNotExist:
        logger.error(
            'Type: Persistence Error. Registration ID: {}.'.format(
                registration.submission_id
            )
        )
        message = 'The training \'{}\' is not found in the system'.format(
            registration.training
        )
        logger.error(message)

    return registration


def persist_planned_training(data):
    training = data.pop('training')
    dates = data.pop('dates')

    planned_training = PlannedTraining.objects.create(training_id=training)

    for date in dates:
        training_date, _ = TrainingDate.objects.get_or_create(
            date=date['date'], planned_training=planned_training
        )
        training_date.sequence = date['sequence']
        training_date.planned_training = planned_training
        training_date.save()

        for trainer in date['trainers']:
            training_date.trainers.add(trainer)

    return planned_training


def update_training_date_trainers(training_date, data):
    training_date.trainers.clear()

    trainer_ids = [
        v for k, v in data.items() if k.startswith(
            '{}_trainer_'.format(
                training_date.id
            )
        )
    ]

    for trainer_id in trainer_ids:
        try:
            user = User.objects.get(pk=trainer_id)
            training_date.trainers.add(user)

        except User.DoesNotExist:
            continue


def get_jotform_url(training):
    selection = {
        'Bewust Beinvloeden': 'https://form.jotform.com/71505246784965',
        'Ontspannen Presenteren': 'https://form.jotform.com/71505088484966',
        'Overtuigend Debatteren': 'https://form.jotform.com/71505115884961',
        'Professioneel Gespreksleiden': 'https://form.jotform.com/'
                                        '71505606784965',
    }

    return selection.get(training)


def get_training_urls(training):
    selection = {
        'Bewust Beinvloeden': {
            'schedule_url': 'https://debat.nl/training/beinvloeden/#3-daagse',
            'registration_url': 'https://debat.nl/inschrijving-dag-2-3/?'
                                'type=Bewust%20Beinvloeden'
        },
        'Ontspannen Presenteren': {
            'schedule_url': 'https://debat.nl/training/presenteren/#3-daagse',
            'registration_url': 'https://debat.nl/inschrijving-dag-2-3/?'
                                'type=Ontspannen%20Presenteren'
        },
        'Overtuigend Debatteren': {
            'schedule_url': 'https://debat.nl/training/debatteren/#3-daagse',
            'registration_url': 'https://debat.nl/inschrijving-dag-2-3/?'
                                'type=Overtuigend%20Debatteren'
        },
        'Professioneel Gespreksleiden': {
            'schedule_url': 'https://debat.nl/training/gespreksleiden/#'
                            '3-daagse',
            'registration_url': 'https://debat.nl/inschrijving-dag-2-3/?'
                                'type=Professioneel%20Gespreksleiden'
        },
    }

    return selection.get(training)


def get_springest_url(training, duration):
    base_url = 'https://www.springest.nl/debat-nl'

    selection = {
        'Bewust Beinvloeden': {
            1: '{}/bewust-beinvloeden-2/ervaring-invoeren',
            3: '{}/bewust-beinvloeden-3-dagen/ervaring-invoeren'
        },
        'Ontspannen Presenteren': {
            1: '{}/ontspannen-presenteren-2/ervaring-invoeren',
            3: '{}/ontspannen-presenteren-3-dagen/ervaring-invoeren'
        },
        'Overtuigend Debatteren': {
            1: '{}/overtuigend-debatteren-2/ervaring-invoeren',
            3: '{}/overtuigend-debatteren-3-dagen/ervaring-invoeren'
        },
        'Professioneel Gespreksleiden': {
            1: '{}/professioneel-gespreksleiden-2/ervaring-invoeren',
            3: '{}/professioneel-gespreksleiden-3-dagen/ervaring-invoeren'
        },
    }

    uri = selection.get(training, {}).get(duration)
    url = uri.format(base_url) if uri else None
    return url


def get_training_continuation_data(training):
    url = 'https://debat.nl/training/presenteren/'
    button_text = 'Bekijk programma Ontspannen Presenteren'

    if training == 'Ontspannen Presenteren':
        url = 'https://debat.nl/training/beinvloeden/'
        button_text = 'Bekijk programma Bewust Beinvloeden'

    return {'url': url, 'button_text': button_text}


def generate_certificate(participant_name, training_name, duration, date):
    utilities_path = os.path.join(
        settings.BASE_DIR,
        'debatnl_registration',
        'utilities'
    )

    # Get document size
    template_pdf = PdfFileReader(
        open(
            os.path.join(
                utilities_path,
                'templates',
                'template_certificate.pdf'
            ),
            "rb"
        )
    )
    _, _, width, height = template_pdf.getPage(0).mediaBox

    # Register fonts
    pdfmetrics.registerFont(
        TTFont(
            'Baskerville SemiBold',
            os.path.join(
                utilities_path,
                'fonts',
                'Baskerville-SemiBold.ttf'
            )
        )
    )
    pdfmetrics.registerFont(
        TTFont(
            'Dancing Script',
            os.path.join(
                   utilities_path,
                   'fonts',
                   'Dancing-Script.ttf'
            )
        )
    )

    # Create certificate layout
    packet = BytesIO()
    c = canvas.Canvas(packet)
    c.setPageSize((width, height))
    c.setFillColor(HexColor('#08304F'))

    # Course Name
    c.setFont('Baskerville SemiBold', 18)
    c.drawCentredString(width / 2.0, 385, training_name.upper())

    # Study Load
    text = 'Met een studielast van {} uur'.format(duration * 8)
    c.setFont('Baskerville SemiBold', 18)
    c.drawCentredString(width / 2.0, 345, text)

    # Student Name
    c.setFont('Dancing Script', 45)
    c.drawCentredString(width / 2.0, 270, participant_name)

    # Date
    c.setFont('Baskerville SemiBold', 20)
    c.drawString(312, 165, date)

    c.showPage()
    c.save()

    packet.seek(0)

    # Create certificate pdf
    pdf = PdfFileReader(packet)

    page = template_pdf.getPage(0)
    page.mergePage(pdf.getPage(0))

    output = PdfFileWriter()
    output.addPage(page)

    packet_out = BytesIO()
    output.write(packet_out)

    return ContentFile(packet_out.getvalue())


def change_attendance(token, data, exclude_first_day):
    new_planned_training = data.get('new_planned_training')

    for training_date in new_planned_training.training_dates.all():
        if exclude_first_day and training_date.sequence == 0:
            continue

        old_training_dates = token.planned_training.training_dates.filter(
            sequence=training_date.sequence
        ).all()
        old_training_date = old_training_dates.first()

        try:
            attendance = Attendance.objects.get(
                training_date=old_training_date,
                participant=token.participant
            )
            attendance.training_date = training_date
            attendance.save()

        except Attendance.DoesNotExist:
            pass


def delete_attendance(token):
    for training_date in token.planned_training.training_dates.all():
        attendance = Attendance.objects.get(
            training_date=training_date,
            participant=token.participant
        )
        attendance.delete()


def calculate_deadline(days):
    tz = pytz.timezone('Europe/Amsterdam')
    today = datetime.datetime.now(tz)
    return today + datetime.timedelta(days=days)

