from debatnl_online.consumers.base import BaseConsumer


class UserConsumer(BaseConsumer):
    def __init__(self):
        self.url = '{}/api/users'.format(self.base_url)
