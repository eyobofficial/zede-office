import pendulum
from django.conf import settings
from django.contrib.messages.views import SuccessMessageMixin
from django.forms import modelformset_factory
from django.shortcuts import redirect
from django.template.loader import render_to_string
from django.urls import reverse_lazy, reverse
from django.views.generic import CreateView, FormView, TemplateView, UpdateView

from backoffice.constants import GROUP_SCHEDULES
from backoffice.mixins import GroupAccessMixin
from backoffice.views import BaseEmailView
from schedules.emails.notifications import NewVacationPeriodEmail, \
    ApprovedVacationPeriodEmail, DeniedVacationPeriodEmail, \
    PhoneCareVacationPeriodEmail, AdminVacationPeriodEmail
from schedules.forms import VacationPeriodForm, TimetableForm, PaidLeaveForm, \
    ScheduleForm, LeaveBalanceForm, ContractTypeForm, EntitledHoursForm, \
    UsedHoursForm
from schedules.models import Timetable, VacationPeriod, VacationPeriodApproval
from schedules.mixins import VacationFormMixin
from schedules.utilities import generate_period_ids


class OverviewView(SuccessMessageMixin, GroupAccessMixin, FormView):
    form_class = modelformset_factory(Timetable, form=TimetableForm, extra=0)
    template_name = 'schedules/base.html'
    success_url = reverse_lazy('schedules:overview')
    access_groups = [GROUP_SCHEDULES]
    period_ids = []
    queryset = None

    def get(self, request, *args, **kwargs):
        self.init_period_ids()
        return super().get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        self.init_period_ids()
        return super().post(request, *args, **kwargs)

    def get_queryset(self):
        if not self.queryset:
            self.queryset = Timetable.objects.filter(
                user=self.request.user, period_id__in=self.period_ids
            ).all()
        return self.queryset

    def get_context_data(self, **kwargs):
        kwargs['period_ids'] = self.period_ids
        return super().get_context_data(**kwargs)

    def get_success_message(self, cleaned_data):
        path = 'schedules/components/notifications/schedule_saved.html'
        return render_to_string(path)

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['queryset'] = self.get_queryset()
        return kwargs

    def form_valid(self, formset):
        formset.save()
        return super().form_valid(formset)

    def init_period_ids(self):
        today = pendulum.today(tz=settings.TIME_ZONE)
        self.period_ids = generate_period_ids(today, self.request.user)


class VacationFormStep1View(VacationFormMixin, FormView):
    form_class = PaidLeaveForm
    template_name = 'schedules/components/modals/vacation_period.html'


class VacationFormStep2View(VacationFormMixin, FormView):
    form_class = ScheduleForm
    template_name = 'schedules/components/modals/steps/step2.html'


class VacationFormStep3View(VacationFormMixin, TemplateView):
    template_name = 'schedules/components/modals/steps/step3.html'


class VacationFormStep4View(VacationFormMixin, FormView):
    form_class = LeaveBalanceForm
    template_name = 'schedules/components/modals/steps/step4.html'


class VacationFormStep5View(VacationFormMixin, CreateView):
    form_class = VacationPeriodForm
    template_name = 'schedules/components/modals/steps/step5.html'
    success_url = reverse_lazy('schedules:step-6')

    def form_valid(self, form):
        obj = form.save(commit=False)
        obj.user = self.request.user
        obj.save()
        NewVacationPeriodEmail(vacation_period=obj).send()
        return redirect(self.success_url)

    def get_initial(self):
        leave_type = self.request.GET.get('type')
        if leave_type == VacationPeriod.UNPAID:
            self.initial['type'] = VacationPeriod.UNPAID
        else:
            self.initial['type'] = VacationPeriod.PAID

        amount_hours = self.request.GET.get('amount_hours')
        if amount_hours:
            self.initial['amount_hours'] = amount_hours

        return super().get_initial()


class VacationFormStep6View(VacationFormMixin, TemplateView):
    template_name = 'schedules/components/modals/steps/step6.html'


class VacationFormStep7View(VacationFormMixin, FormView):
    form_class = ContractTypeForm
    template_name = 'schedules/components/modals/steps/step7.html'


class VacationFormStep8View(VacationFormMixin, TemplateView):
    template_name = 'schedules/components/modals/steps/step8.html'


class VacationFormStep9View(VacationFormMixin, FormView):
    form_class = EntitledHoursForm
    template_name = 'schedules/components/modals/steps/step9.html'

    def form_valid(self, form):
        hours = form.cleaned_data['hours']
        allowed_hours = round((hours * 12 * 0.08), 1)
        self.request.session['allowed_hours'] = allowed_hours
        return super().form_valid(form)


class VacationFormStep10View(VacationFormMixin, FormView):
    form_class = UsedHoursForm
    template_name = 'schedules/components/modals/steps/step10.html'


class VacationFormStep11View(VacationFormMixin, TemplateView):
    template_name = 'schedules/components/modals/steps/step11.html'


class VacationApprovalView(UpdateView):
    template_name = 'schedules/public/vacation_approval.html'
    model = VacationPeriodApproval
    fields = ('status', 'reason')

    def get_success_url(self):
        kwargs = {'pk': self.object.pk}
        path = reverse('schedules:vacation-approval', kwargs=kwargs)
        return f'{path}?status=UPDATED'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['submitted_status'] = self.request.GET.get('status')

        if (
            self.object.status == VacationPeriodApproval.PENDING and
            context['submitted_status'] == VacationPeriodApproval.APPROVED
        ):
            context['submitted_status'] = 'UPDATED'
            self.object.approve()
            self.send_emails()

        return context

    def form_valid(self, form):
        self.object = form.save()
        self.send_emails()
        return super().form_valid(form)

    def send_emails(self):
        vacation_period = self.object.vacation_period
        status = vacation_period.get_status()
        if status == VacationPeriodApproval.APPROVED:
            ApprovedVacationPeriodEmail(vacation_period).send()
            PhoneCareVacationPeriodEmail(vacation_period).send()
            AdminVacationPeriodEmail(vacation_period).send()

        elif status == VacationPeriodApproval.DENIED:
            DeniedVacationPeriodEmail(vacation_period).send()


class BaseVacationPeriodEmailView(BaseEmailView):
    url = 'admin:schedules_vacationperiod_change'
    model = VacationPeriod


class NewVacationPeriodEmailView(BaseVacationPeriodEmailView):
    email_class = NewVacationPeriodEmail
    email_name = 'new vacation period notification'


class ApprovedVacationPeriodEmailView(BaseVacationPeriodEmailView):
    email_class = ApprovedVacationPeriodEmail
    email_name = 'approved vacation period notification'


class DeniedVacationPeriodEmailView(BaseVacationPeriodEmailView):
    email_class = DeniedVacationPeriodEmail
    email_name = 'denied vacation period notification'


class PhoneCareVacationPeriodEmailView(BaseVacationPeriodEmailView):
    email_class = PhoneCareVacationPeriodEmail
    email_name = 'phonecare vacation period notification'


class AdminVacationPeriodEmailView(BaseVacationPeriodEmailView):
    email_class = AdminVacationPeriodEmail
    email_name = 'admin vacation period notification'
