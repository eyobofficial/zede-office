from .base import *

DEBUG = False

SECRET_KEY = 'u#)g=qh6b9n8pxcw8im04)nvqpb#85_5k2h88b(*5vn&a@x$$*'

INSTALLED_APPS += []

# E-mail Settings

ALLOW_EMAIL_SENDING = False

# Cache Settings

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.dummy.DummyCache'
    }
}

# E-mail Settings

EMAIL_BACKEND = 'post_office.EmailBackend'
POST_OFFICE = {
    'BACKENDS': {
        'default': 'django.core.mail.backends.locmem.EmailBackend'
    }
}

# JotForm API Settings

JOT_FORM_API_KEY = 'test key'

# Celery Settings

CELERY_BROKER_URL = 'memory://'

# Host Settings

APP_HOSTNAME = 'http://testserver.test'
FRONTEND_URL = 'http://www.debatnlonline.test:4200'

# Debat.NL Online Settings

DEBATNL_ONLINE_API = 'http://api.testserver.test'
DEBATNL_ONLINE_API_KEY = 'test key'
# This is for the readonly access to a participant dashboard.
DEBATNL_ONLINE_ADMIN_KEY = 'test key'
