import datetime

from django.test import SimpleTestCase, TestCase

from backoffice.tests.factories import TrainingFactory
from debatnl_registration.tests.factories import PlannedTrainingFactory
from debatnl_registration.utilities.parsers import parse_training_duration, \
    parse_planned_training_data, \
    parse_training_update, parse_training_registration, \
    parse_2day_additional_training_registration
from shared.enums import RoleName
from users.models import Role
from users.tests.factories import UserFactory


class TrainingDurationParserTest(SimpleTestCase):
    """
    Test the parsing of the training duration from the Gravity Form Submission.
    """

    def test_null_value(self):
        # TODO
        pass

    def test_0_starting_digits_in_string(self):
        training_type = parse_training_duration('abc0abc')
        self.assertIsNone(training_type)

    def test_with_0_as_starting_digit_in_string(self):
        training_type = parse_training_duration('01abc0abc')
        self.assertEqual(training_type, 1)

    def test_1_starting_digits_in_string(self):
        training_type = parse_training_duration('1abc0abc')
        self.assertEqual(training_type, 1)

    def test_2_starting_digits_in_string(self):
        training_type = parse_training_duration('12abc0abc')
        self.assertEqual(training_type, 12)

    def test_3_starting_digits_in_string(self):
        training_type = parse_training_duration('123abc0abc')
        self.assertEqual(training_type, 123)


class TrainingRegistrationParserTest(SimpleTestCase):
    """
    Test the parsing of the registration submission from
    the Gravity Form Submission.
    """

    def test_all_fields_filled(self):
        post = {
            'entry[id]': '1962', 'entry[form_id]': '39',
            'entry[status]': 'active',
            'entry[1]': 'Overtuigend Debatteren', 'entry[40]': '3dagen|599',
            'entry[29]': '2 oktober 2018, 6 november 2018 en '
                         '4 december 2018 (Utrecht)',
            'entry[119]': '2', 'entry[114]': '2', 'entry[51]': 'Naam A',
            'entry[52]': 'naama@test.mail', 'entry[64]': '123456789',
            'entry[120]': 'Vegan',
            'entry[87]': 'Naam B', 'entry[84]': 'naamb@test.mail',
            'entry[89]': '123456789',
            'entry[125]': 'Geen', 'entry[111]': '2',
            'entry[110]': 'Ja, ik begin graag vooraf al met de theorie '
                          '(+€ 49 ex. BTW)|49',
            'entry[8]': 'Naam C', 'entry[9]': 'Achternaam C',
            'entry[11]': 'naamc@test.mail',
            'entry[10]': '123456789', 'entry[12]': 'Bedrijf X',
            'entry[13]': 'Factuuradres 123A',
            'entry[14]': '1234AB', 'entry[15]': 'Den Haag',
            'entry[16]': 'Geen opmerkingen',
            'entry[59]': '1071.0743801653', 'entry[79.1]': 'BTW',
            'entry[79.2]': '272,16  &#8364;',
            'entry[79.3]': '1', 'entry[61]': '1296', 'entry[4]': '',
            'entry[19]': '',
            'entry[22]': '', 'entry[25]': '', 'entry[28]': '', 'entry[27]': '',
            'entry[30]': '',
            'entry[78]': '', 'entry[6]': '', 'entry[104]': '',
            'entry[116]': '', 'entry[80]': '',
            'entry[82]': '', 'entry[86]': '', 'entry[91]': '', 'entry[88]': '',
            'entry[85]': '',
            'entry[122]': '', 'entry[90]': '', 'entry[83]': '',
            'entry[96]': '', 'entry[97]': '',
            'entry[121]': '', 'entry[94]': '', 'entry[95]': '',
            'entry[100]': '', 'entry[101]': '',
            'entry[124]': '', 'entry[98]': '', 'entry[99]': '',
            'entry[92]': '', 'entry[93]': '',
            'entry[123]': '', 'entry[103]': '', 'entry[109]': '',
            'entry[117]': '',
            'entry[108]': '', 'entry[55]': '', 'entry[118]': '',
            'entry[36]': '', 'entry[76]': ''
        }

        registration = parse_training_registration(post)

        expected_dates = [
            datetime.date(2018, 10, 2),
            datetime.date(2018, 11, 6),
            datetime.date(2018, 12, 4),
        ]

        self.assertEqual(registration['submission_id'], post['entry[id]'])
        self.assertEqual(registration['form_id'], post['entry[form_id]'])
        self.assertEqual(registration['training'], post['entry[1]'])
        self.assertListEqual(registration['training_dates'], expected_dates)
        self.assertEqual(registration['duration'], 3)
        self.assertEqual(registration['participant_count'], 2)
        self.assertEqual(
            registration['registrant_first_name'],
            post['entry[8]']
        )
        self.assertEqual(
            registration['registrant_last_name'],
            post['entry[9]']
        )
        self.assertEqual(registration['registrant_email'], post['entry[11]'])
        self.assertEqual(registration['registrant_phone'], post['entry[10]'])

        participant_a = registration['participants'][0]
        self.assertEqual(participant_a['name'], post['entry[51]'])
        self.assertEqual(participant_a['email'], post['entry[52]'])
        self.assertEqual(participant_a['phone'], post['entry[64]'])
        self.assertEqual(participant_a['dietary_wishes'], post['entry[120]'])

        participant_b = registration['participants'][1]
        self.assertEqual(participant_b['name'], post['entry[87]'])
        self.assertEqual(participant_b['email'], post['entry[84]'])
        self.assertEqual(participant_b['phone'], post['entry[89]'])
        self.assertEqual(participant_b['dietary_wishes'], post['entry[125]'])


class Training2DaysRegistrationParserTest(SimpleTestCase):
    """
    Test the parsing of the registration submission from
    the Gravity Form Submission.
    """

    def test_all_fields_filled(self):
        post = {
            'entry[id]': '1962', 'entry[form_id]': '45',
            'entry[status]': 'active',
            'entry[1]': 'Overtuigend Debatteren',
            'entry[29]': '10 april 2018 en 15 mei 2018 (Utrecht)',
            'entry[119]': '2', 'entry[114]': '2', 'entry[51]': 'Naam A',
            'entry[52]': 'naama@test.mail', 'entry[64]': '123456789',
            'entry[120]': 'Vegan',
            'entry[87]': 'Naam B', 'entry[84]': 'naamb@test.mail',
            'entry[89]': '123456789',
            'entry[125]': 'Geen', 'entry[111]': '2',
            'entry[110]': 'Ja, ik begin graag vooraf al met de theorie '
                          '(+€ 49 ex. BTW)|49',
            'entry[8]': 'Naam C', 'entry[9]': 'Achternaam C',
            'entry[11]': 'naamc@test.mail',
            'entry[10]': '123456789', 'entry[12]': 'Bedrijf X',
            'entry[13]': 'Factuuradres 123A',
            'entry[14]': '1234AB', 'entry[15]': 'Den Haag',
            'entry[16]': 'Geen opmerkingen',
            'entry[59]': '1071.0743801653', 'entry[79.1]': 'BTW',
            'entry[79.2]': '272,16  &#8364;',
            'entry[79.3]': '1', 'entry[61]': '1296', 'entry[4]': '',
            'entry[19]': '',
            'entry[22]': '', 'entry[25]': '', 'entry[28]': '', 'entry[27]': '',
            'entry[30]': '',
            'entry[78]': '', 'entry[6]': '', 'entry[104]': '',
            'entry[116]': '', 'entry[80]': '',
            'entry[82]': '', 'entry[86]': '', 'entry[91]': '', 'entry[88]': '',
            'entry[85]': '',
            'entry[122]': '', 'entry[90]': '', 'entry[83]': '',
            'entry[96]': '', 'entry[97]': '',
            'entry[121]': '', 'entry[94]': '', 'entry[95]': '',
            'entry[100]': '', 'entry[101]': '',
            'entry[124]': '', 'entry[98]': '', 'entry[99]': '',
            'entry[92]': '', 'entry[93]': '',
            'entry[123]': '', 'entry[103]': '', 'entry[109]': '',
            'entry[117]': '',
            'entry[108]': '', 'entry[55]': '', 'entry[118]': '',
            'entry[36]': '', 'entry[76]': ''
        }

        registration = parse_2day_additional_training_registration(post)

        expected_dates = [
            datetime.date(2018, 4, 10),
            datetime.date(2018, 5, 15)
        ]

        self.assertEqual(registration['submission_id'], post['entry[id]'])
        self.assertEqual(registration['form_id'], post['entry[form_id]'])
        self.assertEqual(registration['training'], post['entry[1]'])
        self.assertListEqual(registration['training_dates'], expected_dates)
        self.assertEqual(registration['duration'], 2)
        self.assertEqual(registration['participant_count'], 2)
        self.assertEqual(registration['registrant_first_name'],
                         post['entry[8]'])
        self.assertEqual(registration['registrant_last_name'],
                         post['entry[9]'])
        self.assertEqual(registration['registrant_email'], post['entry[11]'])
        self.assertEqual(registration['registrant_phone'], post['entry[10]'])

        participant_a = registration['participants'][0]
        self.assertEqual(participant_a['name'], post['entry[51]'])
        self.assertEqual(participant_a['email'], post['entry[52]'])
        self.assertEqual(participant_a['phone'], post['entry[64]'])
        self.assertEqual(participant_a['dietary_wishes'], post['entry[120]'])

        participant_b = registration['participants'][1]
        self.assertEqual(participant_b['name'], post['entry[87]'])
        self.assertEqual(participant_b['email'], post['entry[84]'])
        self.assertEqual(participant_b['phone'], post['entry[89]'])
        self.assertEqual(participant_b['dietary_wishes'], post['entry[125]'])


class PlannedTrainingParserTest(TestCase):
    """
    Test the parsing of the POST data when creating a new planned training.
    """
    fixtures = ['roles']

    def setUp(self):
        self.training = TrainingFactory()

        role = Role.objects.get(name=RoleName.TRAINER.value)
        self.trainers = UserFactory.create_batch(3)
        for trainer in self.trainers:
            trainer.profile.roles.add(role)

    def test_all_fields_filled(self):
        post = {
            'training': self.training.id,
            'date_0_day': '01',
            'date_0_month': '01',
            'date_0_year': '2018',
            'date_0_trainer_0': self.trainers[0].pk,
            'date_1_day': '02',
            'date_1_month': '02',
            'date_1_year': '2018',
            'date_1_trainer_0': self.trainers[0].pk,
            'date_1_trainer_1': self.trainers[1].pk,
            'date_2_day': '03',
            'date_2_month': '03',
            'date_2_year': '2018',
            'date_2_trainer_0': self.trainers[0].pk,
            'date_2_trainer_1': self.trainers[1].pk,
            'date_2_trainer_2': self.trainers[2].pk
        }

        planned_training = parse_planned_training_data(post)

        expected_dates = [
            datetime.date(2018, 1, 1),
            datetime.date(2018, 2, 2),
            datetime.date(2018, 3, 3),
        ]

        expected_trainers = [
            [self.trainers[0].pk],
            [self.trainers[0].pk, self.trainers[1].pk],
            [self.trainers[0].pk, self.trainers[1].pk, self.trainers[2].pk]
        ]

        self.assertEqual(self.training.id, planned_training['training'])

        for index, date in enumerate(planned_training['dates']):
            with self.subTest(date=date):
                self.assertEqual(index, date['sequence'])
                self.assertEqual(expected_dates[index], date['date'])
                self.assertListEqual(
                    expected_trainers[index],
                    date['trainers']
                )


class TrainingUpdateParser(TestCase):
    def setUp(self):
        self.planned_training = PlannedTrainingFactory()

    def test_option_1(self):
        data = {
            'option': '1',
            'training_1': self.planned_training.id,
            'coaching_date_day_1': '01',
            'coaching_date_month_1': '01',
            'coaching_date_year_1': '2018',
            'coaching_date_time_1': '13:00 - 15:00',
            'name_3': '',
            'email_3': '',
            'invoice_number_3': '',
            'bank_account_number_3': '',
            'account_holder_name_3': '',
            'reference_3': ''
        }

        parsed_data = parse_training_update(data)

        self.assertEqual(parsed_data['option'], data['option'])
        self.assertEqual(
            parsed_data['new_planned_training'],
            self.planned_training
        )
        self.assertEqual(
            parsed_data['coaching_date'],
            datetime.datetime(2018, 1, 1, 0, 0)
        )
        self.assertEqual(
            parsed_data['coaching_time'],
            data['coaching_date_time_1']
        )
        self.assertIsNone(parsed_data['name'])
        self.assertIsNone(parsed_data['email'])
        self.assertIsNone(parsed_data['invoice_choice'])
        self.assertIsNone(parsed_data['invoice_number'])
        self.assertIsNone(parsed_data['bank_account_number'])
        self.assertIsNone(parsed_data['account_holder_name'])
        self.assertIsNone(parsed_data['reference'])

    def test_option_2(self):
        data = {
            'option': '2',
            'training_2': self.planned_training.id,
            'coaching_date_day_2': '01',
            'coaching_date_month_2': '01',
            'coaching_date_year_2': '2019',
            'coaching_date_time_2': '11:00 - 13:00',
            'name_3': '',
            'email_3': '',
            'invoice_number_3': '',
            'bank_account_number_3': '',
            'account_holder_name_3': '',
            'reference_3': ''
        }

        parsed_data = parse_training_update(data)

        self.assertEqual(parsed_data['option'], data['option'])
        self.assertEqual(
            parsed_data['new_planned_training'],
            self.planned_training
        )
        self.assertEqual(
            parsed_data['coaching_date'],
            datetime.datetime(2019, 1, 1, 0, 0)
        )
        self.assertEqual(
            parsed_data['coaching_time'],
            data['coaching_date_time_2']
        )
        self.assertIsNone(parsed_data['name'])
        self.assertIsNone(parsed_data['email'])
        self.assertIsNone(parsed_data['invoice_choice'])
        self.assertIsNone(parsed_data['invoice_number'])
        self.assertIsNone(parsed_data['bank_account_number'])
        self.assertIsNone(parsed_data['account_holder_name'])
        self.assertIsNone(parsed_data['reference'])

    def test_option_3_with_yes(self):
        data = {
            'option': '3',
            'name_3': 'test persoon',
            'email_3': 'test@email.test',
            'invoice_number_3': '1234567890',
            'invoice_choice_3': 'Ja',
            'bank_account_number_3': '1000000000',
            'account_holder_name_3': 'test persoon2',
            'reference_3': '123412312'
        }

        parsed_data = parse_training_update(data)

        self.assertEqual(parsed_data['option'], data['option'])
        self.assertIsNone(parsed_data['new_planned_training'])
        self.assertIsNone(parsed_data['coaching_date'])
        self.assertIsNone(parsed_data['coaching_time'])
        self.assertEqual(parsed_data['name'], data['name_3'])
        self.assertEqual(parsed_data['email'], data['email_3'])
        self.assertEqual(
            parsed_data['invoice_choice'],
            data['invoice_choice_3']
        )
        self.assertEqual(
            parsed_data['invoice_number'],
            data['invoice_number_3']
        )
        self.assertEqual(
            parsed_data['bank_account_number'],
            data['bank_account_number_3']
        )
        self.assertEqual(
            parsed_data['account_holder_name'],
            data['account_holder_name_3']
        )
        self.assertEqual(parsed_data['reference'], data['reference_3'])

    def test_option_3_with_no(self):
        data = {
            'option': '3',
            'name_3': 'test persoon',
            'email_3': 'test@email.test',
            'invoice_number_3': '1234567890',
            'invoice_choice_3': 'Nee',
            'bank_account_number_3': '',
            'account_holder_name_3': '',
            'reference_3': ''
        }

        parsed_data = parse_training_update(data)

        self.assertEqual(parsed_data['option'], data['option'])
        self.assertIsNone(parsed_data['new_planned_training'])
        self.assertIsNone(parsed_data['coaching_date'])
        self.assertIsNone(parsed_data['coaching_time'])
        self.assertEqual(parsed_data['name'], data['name_3'])
        self.assertEqual(parsed_data['email'], data['email_3'])
        self.assertEqual(
            parsed_data['invoice_choice'],
            data['invoice_choice_3']
        )
        self.assertEqual(
            parsed_data['invoice_number'],
            data['invoice_number_3']
        )
        self.assertEqual(parsed_data['bank_account_number'], '')
        self.assertEqual(parsed_data['account_holder_name'], '')
        self.assertEqual(parsed_data['reference'], '')

    def test_option_3_with_unknown(self):
        data = {
            'option': '3',
            'name_3': 'test persoon',
            'email_3': 'test@email.test',
            'invoice_number_3': '1234567890',
            'invoice_choice_3': 'Weet ik niet',
            'bank_account_number_3': '1000000000',
            'account_holder_name_3': 'test persoon2',
            'reference_3': '123412312'
        }

        parsed_data = parse_training_update(data)

        self.assertEqual(parsed_data['option'], data['option'])
        self.assertIsNone(parsed_data['new_planned_training'])
        self.assertIsNone(parsed_data['coaching_date'])
        self.assertIsNone(parsed_data['coaching_time'])
        self.assertEqual(parsed_data['name'], data['name_3'])
        self.assertEqual(parsed_data['email'], data['email_3'])
        self.assertEqual(
            parsed_data['invoice_choice'],
            data['invoice_choice_3']
        )
        self.assertEqual(
            parsed_data['invoice_number'],
            data['invoice_number_3']
        )
        self.assertEqual(
            parsed_data['bank_account_number'],
            data['bank_account_number_3']
        )
        self.assertEqual(
            parsed_data['account_holder_name'],
            data['account_holder_name_3']
        )
        self.assertEqual(parsed_data['reference'], data['reference_3'])
