from django.contrib.auth.models import User
from django.db import models

from shared.enums import Position
from users.models import Role


class LearningGoal(models.Model):
    name = models.CharField(max_length=200)
    order = models.IntegerField()
    description = models.TextField(blank=True, default='')
    position = models.CharField(max_length=30,
                                choices=[(p.name, p.value) for p in Position])
    finished_by = models.ManyToManyField(User)

    class Meta:
        ordering = ('order',)
        permissions = (
            ('view_all_learning_goals', 'Can see all learning goals'),
            ('edit_all_learning_goals', 'Can edit all learning goals'),
        )

    def __str__(self):
        return f'{self.name} ({self.position})'

    def _increase_order(self, pk, position, order):
        kwargs = {'position': position, 'order': order}
        if LearningGoal.objects.filter(**kwargs).exclude(pk=pk).exists():
            goal = LearningGoal.objects.get(**kwargs)
            goal.order += 1
            goal.save()
            return self._increase_order(goal.pk, goal.position, goal.order)

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        self._increase_order(self.pk, self.position, self.order)
        return super().save(force_insert, force_update, using, update_fields)
