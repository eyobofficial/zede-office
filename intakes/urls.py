from django.conf.urls import url
from django.contrib.auth.decorators import login_required

from intakes.views import OverviewView, IntakeCreate, JotFormView, \
    IntakeDelete, IntakeReportDownload, IntakeUpdate

urlpatterns = [
    url(
        r'^report/(?P<report_id>[a-zA-Z0-9-]+)/$',
        IntakeReportDownload.as_view(),
        name='download-report'
    ),
    url(
        r'^update/(?P<pk>[a-zA-Z0-9-]+)/$',
        login_required(IntakeUpdate.as_view()),
        name='update'
    ),
    url(
        r'^delete/(?P<pk>[a-zA-Z0-9-]+)/$',
        login_required(IntakeDelete.as_view()),
        name='delete'
    ),
    url(r'^create/$', login_required(IntakeCreate.as_view()), name='create'),
    url(r'^form/$', login_required(JotFormView.as_view()), name='form'),
    url(r'^$', login_required(OverviewView.as_view()), name='overview'),
]
