from django.apps import AppConfig


class DebatnlMobileConfig(AppConfig):
    name = 'debatnl_mobile'
