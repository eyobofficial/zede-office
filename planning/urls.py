from django.conf.urls import url

from .views import OverviewView, DefinitePlanningRequestCreateView, \
    PlanningRequestDetailView, PlanningRequestConfirmView, \
    AvailabilityDetailView, PlanningRequestDeleteView, \
    PlanningRequestAdminConfirmView, DeliveryUpdateView, ProcessTypeView, \
    OptionPlanningRequestCreateView, FinalizeOptionPlanningRequestView

urlpatterns = [
    url(r'^$', OverviewView.as_view(), name='overview'),
    url(
        r'^availability/(?P<pk>[0-9]+)/$',
        AvailabilityDetailView.as_view(),
        name='availability-detail'
    ),
    url(
        r'^deliveries/(?P<pk>[0-9]+)/update/$',
        DeliveryUpdateView.as_view(),
        name='delivery-update'
    ),
    url(
        r'^process-type/$',
        ProcessTypeView.as_view(),
        name='process-type'
    ),
    url(
        r'^create/$',
        DefinitePlanningRequestCreateView.as_view(),
        name='definite-request-create'
    ),
    url(
        r'^option-request-create/$',
        OptionPlanningRequestCreateView.as_view(),
        name='option-request-create'
    ),
    url(
        r'^(?P<pk>[0-9]+)/trainer/(?P<trainer_pk>[0-9a-f-]+)/confirm/$',
        PlanningRequestConfirmView.as_view(),
        name='planning-request-confirm'
    ),
    url(
        r'^(?P<pk>[0-9]+)/confirm/$',
        PlanningRequestAdminConfirmView.as_view(),
        name='planning-request-admin-confirm'
    ),
    url(
        r'^(?P<pk>[0-9]+)/$',
        PlanningRequestDetailView.as_view(),
        name='planning-request-detail'
    ),
    url(
        r'^(?P<pk>[0-9]+)/delete/$',
        PlanningRequestDeleteView.as_view(),
        name='planning-request-delete'
    ),
    url(
        r'^(?P<pk>[0-9]+)/finalize/$',
        FinalizeOptionPlanningRequestView.as_view(),
        name='finalize-pending-request'
    )
]
