from django.db.models.signals import pre_save
from django.dispatch import receiver

from intakes.models import Intake
from intakes.tasks import refresh_form_data
from jotform.helpers import sync_questions
from jotform.models import Form


@receiver(pre_save, sender=Intake)
def update_jotform_data(sender, instance, **kwargs):
    form, created = Form.objects.get_or_create(id=instance.form_id)

    if created:
        # The questions need to be synced immediately, because they are
        # being displayed in the details section of the table ongoing
        # intakes.
        sync_questions(form.id)
        refresh_form_data.delay(form.id)
