from django.conf.urls import url
from django.contrib import admin
from django.urls import reverse
from django.utils.html import format_html
from post_office.admin import EmailAdmin, AttachmentAdmin
from post_office.models import Email, Attachment

from backoffice.models import Employee, Training, ContactPerson
from backoffice.views import EmailPreview, RequestAvailabilityEmailView


class CustomURLModelAdmin(admin.ModelAdmin):
    def get_urls(self):
        urls = super().get_urls()
        additional_urls = []

        for custom_url in self.custom_urls:
            view = custom_url['view'].as_view()
            custom_url['view'] = self.admin_site.admin_view(view)
            additional_urls.append(url(**custom_url))

        return additional_urls + urls


class EmailModelAdmin(admin.ModelAdmin):
    email_views = []

    def get_urls(self):
        urls = super().get_urls()
        for view in self.email_views:
            admin_view = self.admin_site.admin_view(view.as_view())
            url_regex = view.get_admin_url_regex()
            name = view.get_admin_url_name()
            urls.append(url(url_regex, admin_view, name=name))
        return urls

    def change_view(self, request, object_id, form_url='', extra_context=None):
        extra_context = extra_context or {}
        extra_context['email_views'] = self.email_views
        return super().change_view(request, object_id, form_url=form_url,
                                   extra_context=extra_context)


@admin.register(Employee)
class EmployeeAdmin(CustomURLModelAdmin):
    list_display = ('last_name', 'first_name', 'phone', 'email', 'category',
                    'has_availability')
    list_filter = ('is_active', )
    custom_urls = [
        {
            'regex': r'^(?P<pk>.+)/request-availability-reminder/$',
            'view': RequestAvailabilityEmailView,
            'name': 'send_request_availability_reminder'
        }
    ]


@admin.register(ContactPerson)
class ContactPersonAdmin(admin.ModelAdmin):
    list_display = ('last_name', 'first_name', 'phone', 'email', 'category',)


@admin.register(Training)
class TrainingAdmin(admin.ModelAdmin):
    list_display = ('name',)


class EmailPreviewAdmin(EmailAdmin):
    list_display = ('id', 'to_display', 'subject', 'template',
                    'status', 'last_updated', 'account_actions',)
    readonly_fields = ('account_actions', 'attachments')
    search_fields = ('to', 'subject',)

    def get_urls(self):
        urls = super().get_urls()
        custom_urls = [
            url(
                r'^(?P<pk>.+)/preview/$',
                self.admin_site.admin_view(EmailPreview.as_view()),
                name='preview_emails',
            ),
        ]
        return custom_urls + urls

    def account_actions(self, obj):
        return format_html(
            '<a class="button" href="{}" target="_blank">Preview</a>',
            reverse('admin:preview_emails', kwargs={'pk': obj.pk}),
        )

    account_actions.short_description = 'Actions'
    account_actions.allow_tags = True


class CustomAttachmentAdmin(AttachmentAdmin):
    list_display = ('name', 'file', )
    search_fields = ('id', 'name', )


admin.site.unregister(Email)
admin.site.unregister(Attachment)
admin.site.register(Email, EmailPreviewAdmin)
admin.site.register(Attachment, CustomAttachmentAdmin)
