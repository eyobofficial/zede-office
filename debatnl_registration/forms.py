from django import forms
from django.contrib.auth.models import User

from backoffice.models import Training
from debatnl_registration.models import TrainingDate, PlannedTraining
from shared.enums import RoleName


class PlannedTrainingModelForm(forms.ModelForm):
    training = forms.ModelChoiceField(queryset=Training.objects.all(),
                                      empty_label='Kies type training')

    class Meta:
        model = PlannedTraining
        fields = ('training', )


class TrainingDateModelForm(forms.ModelForm):
    date = forms.DateField(required=False)
    trainers = forms.ModelMultipleChoiceField(
        required=False,
        queryset=User.objects.filter(
            profile__roles__name=RoleName.TRAINER.value,
            is_active=True
        ).order_by('first_name', 'last_name').all())

    class Meta:
        model = TrainingDate
        fields = ('date', 'trainers')

    def has_changed(self):
        key = f'{self.prefix}-trainers'
        if key in self.data and not self.data[key]:
            self.data.pop(key)
        return super().has_changed()
