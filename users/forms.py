from django import forms
from django.contrib.auth.models import User, Permission

from shared.enums import Position
from users.models import AppPermission, Role, UserProfile


class UserForm(forms.ModelForm):
    phone = forms.CharField(required=False)
    position = forms.CharField(
        required=False,
        widget=forms.Select(
            choices=[('', '---')] + [(p.name, p.value) for p in Position]
        )
    )
    app_permissions = forms.ModelMultipleChoiceField(
        queryset=AppPermission.objects.all(),
        widget=forms.CheckboxSelectMultiple,
        required=False
    )
    roles = forms.ModelMultipleChoiceField(
        queryset=Role.objects.all(),
        widget=forms.CheckboxSelectMultiple,
        required=False
    )

    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'email', 'phone', 'roles',
                  'position', 'app_permissions')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # Remove e-mail field when in update mode.
        instance = getattr(self, 'instance', None)
        if instance and instance.pk:
            self.fields.pop('email')

    def _save_user(self):
        if 'email' in self.fields:
            self.instance.username = self.cleaned_data['email']

        try:
            existing_user = User.objects.get(username=self.instance.username)
            self.instance.pk = existing_user.pk
            self.instance.is_active = True
        except User.DoesNotExist:
            pass

        return super().save()

    def _save_profile(self, instance):
        roles = self.cleaned_data.get('roles', [])
        app_permissions = self.cleaned_data.get('app_permissions', [])
        position = self.cleaned_data.get('position', '')

        instance.profile.phone = self.cleaned_data.get('phone', '')
        instance.profile.app_permissions = app_permissions
        instance.profile.roles = roles
        instance.profile.position = position
        instance.profile.save()
        return instance.profile

    def save(self, commit=True):
        instance = self._save_user()
        profile = self._save_profile(instance)

        # Some roles can only have one user assigned to them.
        # When a new user gets assigned to that certain role, the following
        # logic makes sure the role is remove from the other user(s).
        roles = profile.roles.filter(allow_multiple_users=False).all()
        user_profiles = UserProfile.objects.filter(roles__in=roles)
        user_profiles = user_profiles.exclude(pk=profile.pk)

        for user_profile in user_profiles:
            user_profile.roles.remove(*roles)

        return instance


class UserProfileForm(forms.ModelForm):
    briefings = forms.BooleanField(required=False)
    feedback_trainers = forms.BooleanField(required=False)
    follow_ups = forms.BooleanField(required=False)
    learning_goals = forms.BooleanField(required=False)
    edit_learning_goals = forms.BooleanField(required=False)

    class Meta:
        model = UserProfile
        fields = ('briefings', 'feedback_trainers', 'follow_ups',
                  'learning_goals', 'edit_learning_goals')

    def _add_or_remove_permission(self, user, field_name, codename):
        permission = Permission.objects.filter(codename=codename).first()
        if permission:
            if self.cleaned_data.get(field_name):
                user.user_permissions.add(permission)
            else:
                user.user_permissions.remove(permission)

    def save(self, commit=True):
        profile = super().save(commit)
        permission_data = [
            {'field_name': 'briefings', 'codename': 'view_all_briefings'},
            {'field_name': 'follow_ups', 'codename': 'view_all_followups'},
            {
                'field_name': 'learning_goals',
                'codename': 'view_all_learning_goals'
            },
            {
                'field_name': 'edit_learning_goals',
                'codename': 'edit_all_learning_goals'
            },
            {
                'field_name': 'feedback_trainers',
                'codename': 'view_all_trainers'
            }
        ]

        for data in permission_data:
            self._add_or_remove_permission(profile.user, **data)

        return profile
