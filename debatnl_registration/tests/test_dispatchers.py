import datetime

from django.test import TestCase
from post_office.models import Email

from backoffice.tests.factories import TrainingFactory
from debatnl_registration.models import Registration, Participant, \
    TrainingDate, Attendance
from debatnl_registration.tests.factories import PlannedTrainingFactory, \
    TrainingDateFactory
from debatnl_registration.utilities.dispatcher import gravity_form_dispatcher
from shared.enums import RoleName
from users.models import Role
from users.tests.factories import UserFactory


class RegistrationDispatcherTest(TestCase):
    """
    Test the registration dispatcher.
    """
    fixtures = ['roles']

    def setUp(self):
        role = Role.objects.get(name=RoleName.ADMIN.value)
        user = UserFactory()
        user.profile.roles.add(role)

        training = TrainingFactory(name='Overtuigend Debatteren')
        planned_training = PlannedTrainingFactory(training=training)
        TrainingDateFactory(
            planned_training=planned_training,
            date=datetime.date(2018, 10, 2),
            sequence=0
        )
        TrainingDateFactory(
            planned_training=planned_training,
            date=datetime.date(2018, 11, 6),
            sequence=1
        )
        TrainingDateFactory(
            planned_training=planned_training,
            date=datetime.date(2018, 12, 4),
            sequence=2
        )

    def test_training_3days_registration(self):
        """
        Test a POST request to the hook view.
        """
        payload = {
            'entry[id]': '1962', 'entry[form_id]': '39',
            'entry[status]': 'active',
            'entry[1]': 'Overtuigend Debatteren', 'entry[40]': '3dagen|599',
            'entry[29]': '2 oktober 2018, 6 november 2018 en '
                         '4 december 2018 (Utrecht)',
            'entry[119]': '2', 'entry[114]': '2', 'entry[51]': 'Naam A',
            'entry[52]': 'naama@test.mail', 'entry[64]': '123456789',
            'entry[120]': 'Vegan',
            'entry[87]': 'Naam B', 'entry[84]': 'naamb@test.mail',
            'entry[89]': '123456789',
            'entry[125]': 'Geen', 'entry[111]': '2',
            'entry[110]': 'Ja, ik begin graag vooraf al met de theorie '
                          '(+€ 49 ex. BTW)|49',
            'entry[8]': 'Naam C', 'entry[9]': 'Achternaam C',
            'entry[11]': 'naamc@test.mail',
            'entry[10]': '123456789', 'entry[12]': 'Bedrijf X',
            'entry[13]': 'Factuuradres 123A',
            'entry[14]': '1234AB', 'entry[15]': 'Den Haag',
            'entry[16]': 'Geen opmerkingen',
            'entry[59]': '1071.0743801653', 'entry[79.1]': 'BTW',
            'entry[79.2]': '272,16  &#8364;',
            'entry[79.3]': '1', 'entry[61]': '1296', 'entry[4]': '',
            'entry[19]': '',
            'entry[22]': '', 'entry[25]': '', 'entry[28]': '', 'entry[27]': '',
            'entry[30]': '',
            'entry[78]': '', 'entry[6]': '', 'entry[104]': '',
            'entry[116]': '', 'entry[80]': '',
            'entry[82]': '', 'entry[86]': '', 'entry[91]': '', 'entry[88]': '',
            'entry[85]': '',
            'entry[122]': '', 'entry[90]': '', 'entry[83]': '',
            'entry[96]': '', 'entry[97]': '',
            'entry[121]': '', 'entry[94]': '', 'entry[95]': '',
            'entry[100]': '', 'entry[101]': '',
            'entry[124]': '', 'entry[98]': '', 'entry[99]': '',
            'entry[92]': '', 'entry[93]': '',
            'entry[123]': '', 'entry[103]': '', 'entry[109]': '',
            'entry[117]': '',
            'entry[108]': '', 'entry[55]': '', 'entry[118]': '',
            'entry[36]': '', 'entry[76]': ''
        }

        gravity_form_dispatcher(payload)

        self.assertEqual(Registration.objects.count(), 1)
        self.assertEqual(Participant.objects.count(), 2)
        self.assertEqual(TrainingDate.objects.count(), 3)
        self.assertEqual(Attendance.objects.count(), 6)
        self.assertEqual(Email.objects.count(), 2)

    def test_training_2days_registration(self):
        payload = {
            'entry[id]': '1962', 'entry[form_id]': '45',
            'entry[status]': 'active',
            'entry[1]': 'Overtuigend Debatteren',
            'entry[29]': '6 november 2018 en 4 december 2018 (Utrecht)',
            'entry[119]': '2', 'entry[114]': '2', 'entry[51]': 'Naam A',
            'entry[52]': 'naama@test.mail', 'entry[64]': '123456789',
            'entry[120]': 'Vegan',
            'entry[87]': 'Naam B', 'entry[84]': 'naamb@test.mail',
            'entry[89]': '123456789',
            'entry[125]': 'Geen', 'entry[111]': '2',
            'entry[110]': 'Ja, ik begin graag vooraf al met de theorie '
                          '(+€ 49 ex. BTW)|49',
            'entry[8]': 'Naam C', 'entry[9]': 'Achternaam C',
            'entry[11]': 'naamc@test.mail',
            'entry[10]': '123456789', 'entry[12]': 'Bedrijf X',
            'entry[13]': 'Factuuradres 123A',
            'entry[14]': '1234AB', 'entry[15]': 'Den Haag',
            'entry[16]': 'Geen opmerkingen',
            'entry[59]': '1071.0743801653', 'entry[79.1]': 'BTW',
            'entry[79.2]': '272,16  &#8364;',
            'entry[79.3]': '1', 'entry[61]': '1296', 'entry[4]': '',
            'entry[19]': '',
            'entry[22]': '', 'entry[25]': '', 'entry[28]': '', 'entry[27]': '',
            'entry[30]': '',
            'entry[78]': '', 'entry[6]': '', 'entry[104]': '',
            'entry[116]': '', 'entry[80]': '',
            'entry[82]': '', 'entry[86]': '', 'entry[91]': '', 'entry[88]': '',
            'entry[85]': '',
            'entry[122]': '', 'entry[90]': '', 'entry[83]': '',
            'entry[96]': '', 'entry[97]': '',
            'entry[121]': '', 'entry[94]': '', 'entry[95]': '',
            'entry[100]': '', 'entry[101]': '',
            'entry[124]': '', 'entry[98]': '', 'entry[99]': '',
            'entry[92]': '', 'entry[93]': '',
            'entry[123]': '', 'entry[103]': '', 'entry[109]': '',
            'entry[117]': '',
            'entry[108]': '', 'entry[55]': '', 'entry[118]': '',
            'entry[36]': '', 'entry[76]': ''
        }

        gravity_form_dispatcher(payload)

        self.assertEqual(Registration.objects.count(), 1)
        self.assertEqual(Participant.objects.count(), 2)
        self.assertEqual(TrainingDate.objects.count(), 3)
        self.assertEqual(Attendance.objects.count(), 4)
        self.assertEqual(
            Attendance.objects.filter(training_date__sequence=1).count(),
            2
        )
        self.assertEqual(
            Attendance.objects.filter(training_date__sequence=2).count(),
            2
        )
        self.assertEqual(Email.objects.count(), 2)
