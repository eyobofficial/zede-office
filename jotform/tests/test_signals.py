from unittest import mock

from django.test import TestCase, override_settings

from jotform.client.api.webhooks import Webhooks
from jotform.tests.factories import WebhookFactory


@override_settings(JOT_FORM_API_KEY='12345')
class WebhookSignalTest(TestCase):

    @mock.patch.object(Webhooks, 'create')
    def test_webhook_creation(self, mock_create):
        WebhookFactory()
        self.assertEqual(mock_create.call_count, 1)

    @mock.patch.object(Webhooks, 'list')
    @mock.patch.object(Webhooks, 'delete')
    @mock.patch.object(Webhooks, 'create')
    def test_webhook_deletion(self, mock_create, mock_delete, mock_list):
        webhooks = WebhookFactory.create_batch(5)

        for webhook in webhooks:
            webhook.__setattr__('wid', 0)

        mock_list.return_value = webhooks

        webhooks[0].delete()
        webhooks[1].delete()

        self.assertEqual(mock_list.call_count, 2)
        self.assertEqual(mock_create.call_count, 5)
        self.assertEqual(mock_delete.call_count, 2)
