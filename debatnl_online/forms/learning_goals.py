from django import forms


class OrganizationForm(forms.Form):
    id = forms.CharField(required=False)
    name = forms.CharField()


class RoleForm(forms.Form):
    id = forms.CharField()
    role = forms.CharField()
    organization = forms.CharField()


class WelcomeMessageForm(forms.Form):
    id = forms.CharField(required=False)
    welcome_message = forms.CharField(widget=forms.Textarea)


class GoalForm(forms.Form):
    id = forms.CharField(required=False)
    title = forms.CharField()
    description = forms.CharField(widget=forms.Textarea)
    video_url = forms.CharField(required=False)
    vimeo_video_id = forms.CharField(required=False)
    organization = forms.CharField()


class BasePersonForm(forms.Form):
    id = forms.CharField(required=False)
    user = forms.CharField(required=False)
    organization = forms.CharField()


class EmailTemplateForm(forms.Form):
    id = forms.CharField(required=False)
    type = forms.CharField()
    role = forms.CharField()
    subject = forms.CharField()
    description = forms.CharField(widget=forms.Textarea)
    organization = forms.CharField()
    sender_name = forms.CharField()
    sender_email = forms.CharField()
