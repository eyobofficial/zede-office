import factory
from django.utils import timezone

from backoffice.tests.factories import UserFactory

from declarations.models import Declaration, InternalMeeting, HubstaffItem, \
    ClientBonus


class DeclarationFactory(factory.django.DjangoModelFactory):
    user = factory.SubFactory(UserFactory)
    year = timezone.now().year
    month = timezone.now().month

    class Meta:
        model = Declaration


class InternalMeetingFactory(factory.django.DjangoModelFactory):
    type = InternalMeeting.INTERNAL
    meeting_with = factory.Faker('name')
    declaration = factory.SubFactory(DeclarationFactory)

    class Meta:
        model = InternalMeeting


class HubstaffItemFactory(factory.django.DjangoModelFactory):
    declaration = factory.SubFactory(DeclarationFactory)

    class Meta:
        model = HubstaffItem


class ClientBonusFactory(factory.django.DjangoModelFactory):
    declaration = factory.SubFactory(DeclarationFactory)
    name = factory.Faker('name')
    invoice_number = factory.Sequence(lambda n: f'invoice-number-{n}')

    class Meta:
        model = ClientBonus
