from django.contrib import messages
from django.contrib.admin.views.decorators import staff_member_required
from django.shortcuts import redirect
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.views import View

from webcrm.tasks import sync_organizations_with_webcrm, \
    sync_persons_with_webcrm, sync_deliveries_with_webcrm, \
    sync_users_with_webcrm, sync_opportunities_with_webcrm, \
    sync_events_with_webcrm


@method_decorator(staff_member_required, name='dispatch')
class SyncOrganizationsView(View):
    def get(self, request):
        sync_organizations_with_webcrm()
        messages.success(request, 'Organizations have been synced.')
        return redirect(reverse('admin:webcrm_organization_changelist'))


@method_decorator(staff_member_required, name='dispatch')
class SyncPersonsView(View):
    def get(self, request):
        sync_persons_with_webcrm()
        messages.success(request, 'Persons have been synced.')
        return redirect(reverse('admin:webcrm_person_changelist'))


@method_decorator(staff_member_required, name='dispatch')
class SyncUsersView(View):
    def get(self, request):
        sync_users_with_webcrm()
        messages.success(request, 'Users have been synced.')
        return redirect(reverse('admin:webcrm_user_changelist'))


@method_decorator(staff_member_required, name='dispatch')
class SyncDeliveriesView(View):
    def get(self, request):
        sync_deliveries_with_webcrm()
        messages.success(request, 'Deliveries have been synced.')
        return redirect(reverse('admin:webcrm_delivery_changelist'))


@method_decorator(staff_member_required, name='dispatch')
class SyncOpportunitiesView(View):
    def get(self, request):
        sync_opportunities_with_webcrm()
        messages.success(request, 'Opportunities have been synced.')
        return redirect(reverse('admin:webcrm_opportunity_changelist'))


@method_decorator(staff_member_required, name='dispatch')
class SyncEventsView(View):
    def get(self, request):
        sync_events_with_webcrm()
        messages.success(request, 'Events have been synced.')
        return redirect(reverse('admin:webcrm_event_changelist'))
