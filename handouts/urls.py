from django.conf.urls import url

from .views import HandoutOverviewView, HandoutCreateSuccessView, \
    HandoutUpdateView, HandoutUpdateSuccessView, AssignHandoutView, \
    MismatchedDeliveryUpdateView, UnknownDeliveryUpdateView, \
    InvalidTokenView, HandoutDownloadView

urlpatterns = [
    url(
        r'^handout-requests/(?P<pk>[a-zA-Z0-9-]+)/$',
        HandoutOverviewView.as_view(),
        name='handout-overview'
    ),
    url(
        r'^handout-create-done/$',
        HandoutCreateSuccessView.as_view(),
        name='handout-create-success'
    ),
    url(
        r'^handout-update/(?P<pk>[a-zA-Z0-9-]+)/$',
        HandoutUpdateView.as_view(),
        name='handout-update'
    ),
    url(
        r'^handout-update-done/$',
        HandoutUpdateSuccessView.as_view(),
        name='handout-update-success'
    ),
    url(
        r'^assign-handout/(?P<pk>[a-zA-Z0-9-]+)/$',
        AssignHandoutView.as_view(),
        name='assign-handout'
    ),
    url(
        r'^correct-mismatched-delivery/$',
        MismatchedDeliveryUpdateView.as_view(),
        name='correct-mismatched-delivery'
    ),
    url(
        r'^handout-download/(?P<pk>[a-zA-Z0-9-]+)/$',
        HandoutDownloadView.as_view(),
        name='handout-download'
    ),
    url(
        r'^correct-unknown-delivery/$',
        UnknownDeliveryUpdateView.as_view(),
        name='correct-unknown-delivery'
    ),
    url(
        r'^invalid-token/$',
        InvalidTokenView.as_view(),
        name='invalid-token'
    )
]
