from faker import Faker

from django.test import TestCase, Client
from django.contrib.auth.models import User, Group
from django.urls import reverse, reverse_lazy

from backoffice.constants import GROUP_BRIEFINGS
from backoffice.tests.factories import UserFactory, AdministratorFactory

from briefings.models import Deliverable, BriefingQuestion, \
    BriefingQuestionToken
from shared.enums import RoleName
from users.models import AppPermission, Role
from .factories import DeliverableFactory, BriefingFactory
from webcrm.factories import UserFactory as WebCRMUserFactory, \
    OrganizationFactory


class BriefingCreateViewTest(TestCase):

    fixtures = ['app_permissions']

    def setUp(self):
        self.test_user = User.objects.create_user(
            username='testuser1',
            email='registered@email.com',
            password='password1234'
        )

        app_permission = AppPermission.objects.get(app_name=GROUP_BRIEFINGS)
        self.test_user.profile.app_permissions.add(app_permission)
        self.client = Client()
        self.faker = Faker()

        responsible = WebCRMUserFactory(email=self.test_user.email)

        # PENDING delivery status and non-registered user email
        DeliverableFactory.create_batch(5)

        # PENDING delivery status and registered user email
        organization = OrganizationFactory(name='Test Organization X')

        DeliverableFactory.create_batch(
            5, delivery__organization=organization,
            delivery__order_date=self.faker.date_time_between(
                start_date='now',
                end_date='+2w'
            ),
            delivery__responsible=responsible
        )

        # SUBMITTED delivery status and registered user email
        DeliverableFactory.create_batch(
            5,
            delivery__responsible=responsible,
            delivery__order_date=self.faker.date_time_between(
                start_date='now',
                end_date='+2w'
            ),
            status=Deliverable.SUBMITTED
        )

        # TO_BE_DELETED delivery status and registered user email
        DeliverableFactory.create_batch(
            5,
            delivery__responsible=responsible,
            delivery__order_date=self.faker.date_time_between(
                start_date='-2w',
                end_date='-1d'
            ),
            status=Deliverable.TO_BE_DELETED
        )

    def test_overview_view_as_anonymous_user(self):
        """
        Test for a request to the overview view from an
        anonymous user
        """
        response = self.client.get(reverse('briefings:overview'))
        self.assertEqual(response.status_code, 302)

    def test_overview_view_as_authenticated_user(self):
        """
        Test for a request to the overview view from an
        an authenticated user. Based on the permissions of the user, the user
        will see a subset of the deliverables.
        """
        is_authenticated = self.client.login(
            username='testuser1',
            password='password1234'
        )
        response = self.client.get(reverse('briefings:overview'))
        self.assertTrue(is_authenticated)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            len(response.context['deliverable_list']),
            10  # number of test deliverables excl. with status TO BE DELETED
        )
        self.assertTemplateUsed(response, 'briefings/deliverable_list.html')

    def test_briefing_create_view_as_anonymous_user(self):
        """
        Test for a request to form view to create briefings
        from an anonymous user
        """
        test_deliverable = Deliverable.objects.first()
        response = self.client.get(
            reverse_lazy('briefings:form', args=[str(test_deliverable.id)])
        )
        self.assertEqual(response.status_code, 302)

    def test_briefing_create_view_as_authenticated_user(self):
        """
        Test for a request to form view to create briefings
        from authenticated user with GET request. Based on the permissions
        of the user, the user will see a subset of the deliverables.
        """
        self.client.force_login(self.test_user)
        test_deliverable = Deliverable.objects.filter(
            delivery__organization__name='Test Organization X').first()
        response = self.client.get(
            reverse_lazy('briefings:form', args=[test_deliverable.id])
        )

        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            len(response.context['pending_deliverables']),
            4
        )
        self.assertTemplateUsed(
            response,
            'briefings/components/modals/modal_add_briefing.html'
        )


class DeliverableDetailViewTest(TestCase):
    fixtures = ['app_permissions']

    def setUp(self):
        self.test_user = User.objects.create_user(
            username='testuser',
            email='registered@email.com',
            password='password1234'
        )
        app_permission = AppPermission.objects.get(app_name=GROUP_BRIEFINGS)
        self.test_user.profile.app_permissions.add(app_permission)
        self.client = Client()
        self.briefing = BriefingFactory()
        self.deliverable = DeliverableFactory()
        self.deliverable.briefing = self.briefing
        self.deliverable.status = Deliverable.SUBMITTED
        self.deliverable.save()

    def test_deliverable_detail_view_as_anonymous_user(self):
        """
        Test for a request to the deliverable detail view from an
        anonymous user
        """
        response = self.client.get(
            reverse(
                'briefings:deliverable-detail', args=[str(self.deliverable.id)]
            )
        )
        self.assertEqual(response.status_code, 302)

    def test_deliverable_detail_as_authenticated_user(self):
        """
        Test for a request to the deliverable detail view from an
        an authenticated user
        """
        is_authenticated = self.client.login(
            username='testuser',
            password='password1234'
        )
        response = self.client.get(
            reverse(
                'briefings:deliverable-detail', args=[str(self.deliverable.id)]
            )
        )
        self.assertTrue(is_authenticated)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(
            response,
            'briefings/components/modals/deliverable_detail.html'
        )


class BriefingUpdateViewTest(TestCase):
    """
    Tests for Briefing Update View
    """
    fixtures = ['app_permissions']

    def setUp(self):
        self.test_user = User.objects.create_user(
            username='testuser',
            email='registered@email.com',
            password='password1234'
        )
        app_permission = AppPermission.objects.get(app_name=GROUP_BRIEFINGS)
        self.test_user.profile.app_permissions.add(app_permission)
        self.client = Client()
        self.briefing = BriefingFactory()
        self.deliverable = DeliverableFactory()
        self.deliverable.briefing = self.briefing
        self.deliverable.status = Deliverable.SUBMITTED
        self.deliverable.save()

    def test_deliverable_update_view_as_anonymous_user(self):
        """
        Test for a request to the deliverable update view from an
        anonymous user
        """
        response = self.client.get(
            reverse(
                'briefings:briefing-update',
                args=[
                    str(self.deliverable.id),
                    str(self.deliverable.briefing.id)
                ]
            )
        )
        self.assertEqual(response.status_code, 302)

    def test_deliverable_detail_as_authenticated_user(self):
        """
        Test for a request to the deliverable update view from an
        an authenticated user
        """
        is_authenticated = self.client.login(
            username='testuser',
            password='password1234'
        )
        response = self.client.get(
            reverse(
                'briefings:briefing-update',
                args=[
                    str(self.deliverable.id),
                    str(self.deliverable.briefing.id)
                ]
            )
        )
        self.assertTrue(is_authenticated)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(
            response,
            'briefings/components/modals/modal_update_briefing.html'
        )


class BriefingQuestionCreateViewTests(TestCase):
    """
    Tests for `BriefingQuestionCreateView` view
    """
    fixtures = ['roles', 'app_permissions']

    def setUp(self):
        role = Role.objects.get(name=RoleName.ADMIN.value)
        admin = UserFactory()
        admin.profile.roles.add(role)

        self.user = UserFactory()
        app_permission = AppPermission.objects.get(app_name=GROUP_BRIEFINGS)
        self.user.profile.app_permissions.add(app_permission)
        self.client = Client()
        self.briefing = BriefingFactory()
        self.deliverable = DeliverableFactory(briefing=self.briefing)
        self.url = reverse(
            'briefings:briefing-question-create',
            args=(self.deliverable.pk, )
        )
        self.template = 'briefings/components/modals/question_create.html'

    def test_request_from_anonymous_user(self):
        """
        Test GET request to `BriefingQuestionCreateView` from
        anonymous user
        """
        response = self.client.get(self.url, follow=True)

        # Assertions
        expected_url = f'{reverse("login")}?next=/feedback/'
        self.assertRedirects(response, expected_url)

    def test_request_from_authenticated_user(self):
        """
        Test GET request to `BriefingQuestionCreateView` from
        authenticated user
        """
        self.client.force_login(self.user)
        response = self.client.get(self.url)

        # Assertions
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, self.template)
        self.assertContains(response, 'formset')
        self.assertContains(response, 'briefing')
        self.assertEqual(response.context['deliverable'], self.deliverable)

    def test_post_request_from_authenticated_user(self):
        """
        Test a valid POST request to `BriefingQuestionCreateView` from
        authenticated user
        """
        self.client.force_login(self.user)

        form_data = {
            # management_form
            'questions-INITIAL_FORMS': '0',
            'questions-TOTAL_FORMS': '3',
            'questions-MIN_NUM_FORMS': '0',
            'questions-MAX_NUM_FORMS': '1000',

            # Form data 1
            'questions-0-question': 'Test question 1',

            # Form data 2
            'questions-1-question': 'Test question 2',

            # Form data 3
            'questions-2-question': 'Test question 3',
        }

        response = self.client.post(self.url, form_data, follow=True)

        # Assertions
        self.assertEqual(response.status_code, 200)
        self.assertEqual(BriefingQuestion.objects.count(), 3)


class BriefingQuestionUpdateViewTests(TestCase):
    """
    Tests for `BriefingQuestionUpdateView` view
    """

    def setUp(self):
        self.client = Client()
        user = UserFactory()
        AdministratorFactory()
        self.deliverable = DeliverableFactory()
        briefing = BriefingFactory()
        self.question1 = BriefingQuestion.objects.create(
            briefing=briefing,
            question='Test question 1',
            user=user
        )
        self.question2 = BriefingQuestion.objects.create(
            briefing=briefing,
            question='Test question 2',
            user=user
        )
        self.token = BriefingQuestionToken.objects.create(
            deliverable=self.deliverable,
        )
        self.token.questions.add(self.question1, self.question2)
        self.token.save()

        self.url = reverse(
            'briefings:briefing-question-update',
            args=(self.token.pk, )
        )
        self.template = 'briefings/public/add_answers.html'

    def test_request_with_expired_token(self):
        """
        Test GET request to `BriefingQuestionUpdateView` view with
        an expired briefing question token.
        """
        self.token.is_expired = True
        self.token.save()

        response = self.client.get(self.url)

        # Assertions
        self.assertEqual(response.status_code, 404)
        self.assertTemplateNotUsed(response, self.template)

    def test_request_with_unexpired_token(self):
        """
        Test GET request to `BriefingQuestionUpdateView` view with
        an unexpired token.
        """
        response = self.client.get(self.url)

        # Assertions
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, self.template)
        self.assertContains(response, self.question1)
        self.assertContains(response, self.question2)

    def test_post_request_with_valid_data(self):
        """
        Test a valid POST request to `BriefingQuestionUpdateView`
        view.
        """
        form_data = {
            # management_form
            'form-INITIAL_FORMS': '2',
            'form-TOTAL_FORMS': '2',
            'form-MIN_NUM_FORMS': '0',
            'form-MAX_NUM_FORMS': '1000',

            # Form data 1
            'form-0-answer': 'Test answer 1',
            'form-0-id': self.question1.pk,

            # Form data 2
            'form-1-answer': 'Test answer 2',
            'form-1-id': self.question2.pk,
        }
        response = self.client.post(self.url, form_data, follow=True)

        # Refresh question instances
        self.question1.refresh_from_db()
        self.question2.refresh_from_db()
        self.token.refresh_from_db()

        # Assertions
        self.assertEqual(response.status_code, 200)
        self.assertTrue(self.token.is_expired)
        self.assertEqual(self.question1.answer, form_data['form-0-answer'])
        self.assertEqual(self.question2.answer, form_data['form-1-answer'])


class QuestionUpdateSuccessViewTests(TestCase):
    """
    Tests for `QuestionUpdateSuccessView` view
    """

    def test_request_with_anonymous_user(self):
        """
        Test request to `QuestionUpdateSuccessView` view
        """
        client = Client()
        url = reverse('briefings:question-update-success')
        template = 'briefings/public/question_success.html'
        response = client.get(url)

        # Assertions
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, template)
