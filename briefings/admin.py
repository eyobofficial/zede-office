from django.contrib import admin

from backoffice.admin import CustomURLModelAdmin

from briefings.views import SyncDeliverablesView, MaxTrainerNotificationEmail,\
    BriefingNotAddedReminderEmail, TwoWeeksForDeliveryReminderEmail, \
    ChangedBriefingNotificationEmail, NewAddedBriefingReminderEmail, \
    BriefingQuestionNotificationEmail, AnswerNotificationEmail
from .models import Deliverable, Briefing, BriefingFile, DeliverableTrainer, \
    BriefingEmailItem, DeliverableMembership, BriefingQuestion, \
    BriefingQuestionFile, BriefingQuestionToken


@admin.register(Briefing)
class BriefingAdmin(admin.ModelAdmin):
    list_display = ('pk', 'reason')
    search_fields = ('reason', 'program')


@admin.register(BriefingFile)
class BriefingFiledAdmin(admin.ModelAdmin):
    list_display = ('file_name', 'briefing')
    list_filter = ('briefing', )
    search_fields = ('file_name', )


@admin.register(BriefingEmailItem)
class BriefingEmailItem(admin.ModelAdmin):
    list_display = ('briefing', 'type')
    list_filter = ('type', )


@admin.register(Deliverable)
class DeliverableAdmin(CustomURLModelAdmin):
    list_display = (
        'order_date',
        'organization',
        'product',
        'briefing',
        'delivery_responsible',
        'status'
    )
    list_filter = ('delivery__order_date', 'status', 'briefing')
    search_fields = ('delivery__organization__name', 'delivery__product')
    custom_urls = [
        {
            'regex': r'^sync_deliverables/$',
            'view': SyncDeliverablesView,
            'name': 'sync_briefings_deliverables'
        },
        {
            'regex': r'^(?P<pk>.+)/send_max_trainers_email/$',
            'view': MaxTrainerNotificationEmail,
            'name': 'send_max_trainers_email'
        },
        {
            'regex': r'^(?P<pk>.+)/send_add_briefing_email/$',
            'view': BriefingNotAddedReminderEmail,
            'name': 'send_add_briefing_email'
        },
        {
            'regex': r'^(?P<pk>.+)/send_two_weeks_for_delivery_email/$',
            'view': TwoWeeksForDeliveryReminderEmail,
            'name': 'send_two_weeks_for_delivery_email'
        },
        {
            'regex': r'^(?P<pk>.+)/send_changed_briefing_email/$',
            'view': ChangedBriefingNotificationEmail,
            'name': 'send_changed_briefing_email'
        },
        {
            'regex': r'^(?P<pk>.+)/send_new_briefing_added_email/$',
            'view': NewAddedBriefingReminderEmail,
            'name': 'send_new_briefing_added_email'
        }
    ]

    @staticmethod
    def order_date(obj):
        if obj.delivery:
            return obj.delivery.order_date.date()
        return '-'

    @staticmethod
    def organization(obj):
        if obj.delivery:
            return obj.delivery.organization
        return '-'

    @staticmethod
    def product(obj):
        if obj.delivery:
            return obj.delivery.product
        return '-'

    @staticmethod
    def delivery_responsible(obj):
        if obj.delivery:
            name = obj.delivery.responsible.name
            email = obj.delivery.responsible.email
            return f'{name} <{email}>'
        return '-'


@admin.register(DeliverableTrainer)
class DeliverableTrainerAdmin(admin.ModelAdmin):
    list_display = ('name', 'email',)
    search_fields = ('name', 'email',)


@admin.register(DeliverableMembership)
class DeliverableMembershipAdmin(admin.ModelAdmin):
    list_display = ('trainer', 'deliverable', 'is_manually_added', 'position')
    list_filter = ('trainer', 'is_manually_added')
    search_fields = ('trainer__name', 'deliverable__organization',)


@admin.register(BriefingQuestion)
class BriefingQuestionAdmin(admin.ModelAdmin):
    list_display = (
        'question', 'briefing', 'user',
        'created_at', 'is_answered'
    )
    list_filter = ('briefing', 'created_at')
    search_fields = ('question', )


@admin.register(BriefingQuestionFile)
class BriefingQuestionFileAdmin(admin.ModelAdmin):
    list_display = ('filename', 'briefing_question')
    list_display_links = ('filename', 'briefing_question')


@admin.register(BriefingQuestionToken)
class BriefingQuestionTokenAdmin(CustomURLModelAdmin):
    list_display = ('id', 'deliverable', 'is_expired')
    list_filter = ('is_expired', )
    filter_horizontal = ('questions', )
    custom_urls = [
        {
            'regex': r'^(?P<pk>.+)/send_briefing_question_added_email/$',
            'view': BriefingQuestionNotificationEmail,
            'name': 'send_briefing_question_added_email'
        },
        {
            'regex': r'^(?P<pk>.+)/send_answer_added_email/$',
            'view': AnswerNotificationEmail,
            'name': 'send_answer_added_email'
        }
    ]
