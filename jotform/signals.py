from django.conf import settings
from django.db.models.signals import pre_save, pre_delete
from django.dispatch import receiver

from jotform.client.client import JOTFormClient
from jotform.models import Webhook


@receiver(pre_save, sender=Webhook)
def create_webhook_on_api(sender, instance, **kwargs):
    api_key = settings.JOT_FORM_API_KEY
    client = JOTFormClient(api_key=api_key, form_id=instance.form.id)
    client.webhooks.create(data={'webhookURL': instance.url})


@receiver(pre_delete, sender=Webhook)
def remove_webhook_from_api(sender, instance, **kwargs):
    api_key = settings.JOT_FORM_API_KEY
    client = JOTFormClient(api_key=api_key, form_id=instance.form.id)
    for webhook in client.webhooks.list():
        if webhook.url == instance.url:
            client.webhooks.delete(hook_id=webhook.wid)
