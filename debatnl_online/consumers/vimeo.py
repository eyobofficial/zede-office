import json

import requests
from django.conf import settings


class VimeoConsumer:

    def __init__(self):
        self.url = 'https://api.vimeo.com'
        self.headers = {
            'Authorization': f'Bearer {settings.VIMEO_ACCESS_TOKEN}',
            'Accept': 'application/vnd.vimeo.*+json;version=3.4',
            'Content-Type': 'application/json'
        }

    def request_pull(self, file_location):
        url = f'{self.url}/me/videos'
        payload = {
            'upload': {
                'approach': 'pull',
                'link': file_location
            },
            'privacy': {
                'add': False,
                'comments': 'nobody',
                'download': False,
                'embed': 'whitelist',
                'view': 'disable'
            },
            'review_page': {
                'active': False
            }
        }

        response = requests.post(url, headers=self.headers, json=payload)
        response.raise_for_status()
        return response.json()

    def set_allowed_domain(self, uri, allowed_domain):
        url = f'{self.url}{uri}/privacy/domains/{allowed_domain}'
        response = requests.put(url, headers=self.headers)
        response.raise_for_status()

    def assign_preset_to_video(self, uri):
        url = f'{self.url}{uri}/presets/{ settings.VIMEO_PRESET_ID}'
        response = requests.put(url, headers=self.headers)
        response.raise_for_status()

    def retrieve_video_details(self, uri):
        response = requests.get(f'{self.url}{uri}', headers=self.headers)
        response.raise_for_status()
        return response.json()

    def delete_video(self, uri):
        requests.delete(f'{self.url}{uri}', headers=self.headers)
