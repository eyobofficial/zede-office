import os

from django.test import Client
from xmlunittest import XmlTestCase

from backoffice.tests.factories import TrainingFactory
from debatnl_registration.tests.factories import TrainingDateFactory, \
    BloomvilleFeedItemFactory, SpringestFeedItemFactory
from shared.enums import RoleName
from users.models import Role
from users.tests.factories import UserFactory


class FeedTestCase(XmlTestCase):
    fixtures = ['roles']

    def setUp(self):
        self.client = Client()
        self.base_url = '/debatnl-registration/feeds'
        self.xsd_dir = os.path.join(os.path.dirname(__file__), 'test_files')

        role = Role.objects.create(name=RoleName.ADMIN.value)
        user = UserFactory()
        user.profile.roles.add(role)

        trainings = TrainingFactory.create_batch(2)

        for training in trainings:
            TrainingDateFactory.reset_sequence()
            TrainingDateFactory.create_batch(
                3,
                planned_training__training=training,
                planned_training__hall_status='AVAILABLE'
            )

            BloomvilleFeedItemFactory.create_batch(2, training=training)
            SpringestFeedItemFactory.create_batch(2, training=training)

    def test_bloomville_feeds(self):
        feeds = [
            {'uri': 'courses', 'filename': 'bloomville_courses.xsd'},
            {'uri': 'classes', 'filename': 'bloomville_classes.xsd'},
        ]

        for feed in feeds:
            with self.subTest(uri=feed['uri']):
                xsd_location = os.path.join(self.xsd_dir, feed['filename'])
                path = f'{self.base_url}/bloomville/{feed["uri"]}/'
                response = self.client.get(path)
                root = self.assertXmlDocument(response.content)
                self.assertXmlValidXSchema(root, filename=xsd_location)

    def test_bloomville_v1_feeds(self):
        xsd_location = os.path.join(self.xsd_dir, 'bloomville_courses_1.0.xsd')
        response = self.client.get(f'{self.base_url}/bloomville/v1/courses/')
        root = self.assertXmlDocument(response.content)
        self.assertXmlValidXSchema(root, filename=xsd_location)

    def test_springest_feeds(self):
        xsd_location = os.path.join(self.xsd_dir, 'springest_product.xsd')
        response = self.client.get(f'{self.base_url}/springest/')
        root = self.assertXmlDocument(response.content)
        self.assertXmlValidXSchema(root, filename=xsd_location)
