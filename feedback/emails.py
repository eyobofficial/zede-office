from django.conf import settings
from django.urls import reverse

from backoffice.utilities.emails import BaseEmail


class BaseFeedbackEmail(BaseEmail):
    template_location = '../templates/feedback/emails'
    email_type = ''

    def __init__(self, recipient):
        self.recipient = recipient

    def get_context_data(self, **kwargs):
        uri = reverse('feedback:unsubscribe-training-outtake')
        sub_id = self.recipient.subscriber_id
        kwargs['url'] = f'{settings.APP_HOSTNAME}{uri}?subscriber={sub_id}'
        return super().get_context_data(**kwargs)


class OuttakeIntroEmail(BaseFeedbackEmail):
    template_name = 'email_intro_training_outtake.html'
    subject = 'Aan de slag!'


class OuttakeReminderEmail(BaseFeedbackEmail):
    template_name = 'email_reminder_training_outtake.html'
    subject = 'Weet u het nog?'
