from django.contrib.auth.models import User
from django.db import models

from shared.enums import Position


class Role(models.Model):
    name = models.CharField(max_length=100)
    allow_multiple_users = models.BooleanField(default=True)

    def __str__(self):
        return self.name


class AppPermission(models.Model):
    app_name = models.CharField(max_length=100, unique=True)
    display_name = models.CharField(max_length=100)

    class Meta:
        ordering = ('display_name', )

    def __str__(self):
        return f'{self.app_name} ({self.display_name})'


class UserProfile(models.Model):
    user = models.OneToOneField(User, related_name='profile',
                                on_delete=models.CASCADE)
    position = models.CharField(max_length=30,
                                choices=[(p.name, p.value) for p in Position],
                                blank=True, default='')
    roles = models.ManyToManyField(Role, blank=True)
    app_permissions = models.ManyToManyField(AppPermission, blank=True)
    phone = models.CharField(max_length=30, blank=True, default='')
    hubstaff_user_id = models.IntegerField(blank=True, null=True)
    do_hubstaff_check = models.BooleanField(default=True)
    has_availability = models.BooleanField(default=False)
    has_planning = models.BooleanField(default=False)

    def __str__(self):
        return f'{self.user.get_full_name()} profile'
