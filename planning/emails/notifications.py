from django.conf import settings
from django.contrib.auth.models import User
from django.urls import reverse

from backoffice.utilities.emails import NotificationEmailMixin

from planning.models import PlanningRequest
from shared.enums import RoleName
from .bases import BasePlanningRequestEmail, BaseScheduleEmail


class NewPlanningRequestEmail(
        NotificationEmailMixin,
        BasePlanningRequestEmail):
    template_name = 'new_planning_request_email.html'
    recipient = User(first_name='Ilona', last_name='Eichhorn',
                     email='eichhorn@debat.nl')

    @property
    def subject(self):
        if self.planning_request.type == PlanningRequest.IMMEDIATE:
            return 'Planningsverzoek SPOED'
        elif self.planning_request.type == PlanningRequest.SHORT_TERM:
            return 'Planningsverzoek regulier'
        else:
            return 'Planningsverzoek lange termijn'

    def get_context_data(self, **kwargs):
        uri = reverse('planning:overview')
        kwargs['url'] = f'{settings.APP_HOSTNAME}{uri}'
        return super().get_context_data(**kwargs)


class NewOptionPlanningRequestEmail(
        NotificationEmailMixin,
        BasePlanningRequestEmail):
    template_name = 'new_option_planning_request_email.html'
    recipient = User(first_name='Ilona', last_name='Eichhorn',
                     email='eichhorn@debat.nl')

    @property
    def subject(self):
        delivery = self.planning_request.deliveries.first()
        return f'Aanvraag optie ({delivery.organization})'

    def get_context_data(self, **kwargs):
        uri = reverse('planning:overview')
        kwargs['url'] = f'{settings.APP_HOSTNAME}{uri}'
        return super().get_context_data(**kwargs)


class PlanningRequestStatusChangeEmail(
        NotificationEmailMixin,
        BasePlanningRequestEmail):
    template_name = 'planning_request_status_change_email.html'

    def __init__(self, planning_request):
        super().__init__(planning_request)
        self.recipient = User.objects.filter(
            profile__roles__name=RoleName.ADMIN.value,
            is_active=True
        ).first()

    @property
    def subject(self):
        first_delivery = self.planning_request.deliveries.first()
        organization = first_delivery.organization
        return f'Pas planning in CRM aan van {organization}'

    def get_context_data(self, **kwargs):
        uri = reverse(
            'planning:planning-request-admin-confirm',
            args=[self.planning_request.pk]
        )
        kwargs['url'] = f'{settings.APP_HOSTNAME}{uri}'
        return super().get_context_data(**kwargs)


class TrainerScheduleConfirmEmail(NotificationEmailMixin, BaseScheduleEmail):
    template_name = 'trainer_scheduled_email.html'

    def __init__(self, schedule):
        super().__init__(schedule)
        self.recipient = schedule.user
        delivery = self.schedule.delivery
        self.subject = 'Je bent zojuist ingepland'
        self.subject = f'{self.subject} - {delivery.organization}'
        self.subject = f'{self.subject} ({delivery.date:%d-%m-%Y})'

    def get_context_data(self, **kwargs):
        planning_request = self.schedule.delivery.planning_request
        trainer = self.schedule.user
        kwargs['schedule_list'] = trainer.schedules.filter(
            is_confirmed=False,
            delivery__planning_request=planning_request
        )
        uri = reverse(
            'planning:planning-request-confirm',
            args=[planning_request.pk, trainer.pk]
        )
        kwargs['url'] = f'{settings.APP_HOSTNAME}{uri}'
        return super().get_context_data(**kwargs)


class OptionTrainerConfirmEmail(TrainerScheduleConfirmEmail):
    """
    Confirmation notification e-mail for OPTION planning request
    trainers.
    """
    template_name = 'option_trainer_schedule_email.html'

    def __init__(self, schedule):
        super().__init__(schedule)
        delivery = self.schedule.delivery
        self.subject = 'Je bent zojuist ingepland voor een optie'
        self.subject = f'{self.subject} {delivery.organization}'


class FinalizeConfirmationEmail(TrainerScheduleConfirmEmail):
    """
    Confirmation notification e-mail for confirmed OPTION planning
    request trainers schedule.
    """
    template_name = 'finalized_trainer_schedule_email.html'

    def __init__(self, schedule):
        super().__init__(schedule)
        delivery = self.schedule.delivery
        date = delivery.date.strftime('%d-%m-%Y')
        self.subject = f'Optie {delivery.organization} op {date} bevestigd'


class OptionPlanningRequestDeleteEmail(
        NotificationEmailMixin,
        BasePlanningRequestEmail):
    """
    Notification e-mail sent when an OPTION PlanningRequest is deleted
    """
    template_name = 'option_planning_request_deleted_email.html'

    @property
    def subject(self):
        delivery = self.planning_request.deliveries.first()
        organization = delivery.organization
        delivery_date = delivery.date.strftime('%d-%m-%Y')
        return f'Optie {organization} op {delivery_date} geannuleerd'

    def get_context_data(self, **kwargs):
        kwargs['planning_request'] = self.planning_request
        return super().get_context_data(**kwargs)

    def send(self):
        for trainer in self.planning_request.get_trainers():
            self.recipient = trainer
            super().send()


class OverdueNotificationEmail(
        NotificationEmailMixin,
        BasePlanningRequestEmail):
    """
    Notification e-mail sent when option plan is overdue.
    """
    template_name = 'option_planning_request_overdue_email.html'
    recipient = User(first_name='Ilona', last_name='Eichhorn',
                     email='eichhorn@debat.nl')

    @property
    def subject(self):
        delivery = self.planning_request.deliveries.first()
        return f'Optie {delivery.organization} verlopen'
