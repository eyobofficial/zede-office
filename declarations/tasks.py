import pendulum

from django.conf import settings

from DebatNL_BackOffice.celery import app

from declarations.emails.reminders import DeclarationReminder
from declarations.models import Declaration, TravelExpense


@app.task
def send_declaration_reminder_email_every_month():
    """
    Send `DeclarationReminder` every on 8:00 a.m. on 2nd of the month.
    """
    today = pendulum.today(tz=settings.TIME_ZONE)

    if today.day == 2:
        first_of_month = today.start_of('month')
        reminder_date = first_of_month.subtract(days=1)
        kwargs = {'month': reminder_date.month, 'year': reminder_date.year}
        declarations = Declaration.objects.filter(**kwargs)
        declarations = declarations.exclude(status=Declaration.SUBMITTED)

        for declaration in declarations:
            DeclarationReminder(declaration).send()


@app.task
def clean_unassigned_travel_expenses():
    """
    Clean unassigned travel expenses.
    """
    today = pendulum.today(tz=settings.TIME_ZONE)
    cutoff_date = today.subtract(days=7)
    kwargs = {
        'created_at__lte': cutoff_date,
        'internalmeeting__isnull': True,
        'externalmeeting__isnull': True
    }
    TravelExpense.objects.filter(**kwargs).all().delete()
