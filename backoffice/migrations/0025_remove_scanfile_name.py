# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-07-16 14:02
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('backoffice', '0024_scanfile_name'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='scanfile',
            name='name',
        ),
    ]
