import factory
from django.utils import timezone

from feedback.models import JotFormTrainingReview, JotFormTraining, \
    JotFormTrainer


class JotFormTrainerFactory(factory.django.DjangoModelFactory):
    first_name = factory.Faker('first_name')
    last_name = factory.Faker('last_name')

    class Meta:
        model = JotFormTrainer


class JotFormTrainingFactory(factory.django.DjangoModelFactory):
    date = timezone.now()
    trainer = factory.SubFactory(JotFormTrainerFactory)

    class Meta:
        model = JotFormTraining


class JotFormTrainingReviewFactory(factory.django.DjangoModelFactory):
    training = factory.SubFactory(JotFormTrainingFactory)
    reviewer_name = factory.Faker('name')
    reviewer_email = factory.LazyAttribute(
        lambda p: '{}@test.mail'.format(
            p.reviewer_name.replace('.', '').replace(' ', '').lower()
        )
    )
    score = 9
    comment = factory.Faker('text')
    resolution = factory.Faker('text')
    submission_date = timezone.now()

    class Meta:
        model = JotFormTrainingReview
