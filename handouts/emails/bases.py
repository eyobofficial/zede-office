from backoffice.utilities.emails import BaseEmail


class BaseHandoutEmail(BaseEmail):
    template_location = '../templates/handouts/emails'
    email_type = ''


class BaseHandoutRequestEmail(BaseHandoutEmail):
    def __init__(self, handout_request):
        self.handout_request = handout_request

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['handout_request'] = self.handout_request
        return context
