from django.apps import AppConfig


class BriefingsConfig(AppConfig):
    name = 'briefings'
