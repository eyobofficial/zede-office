import datetime

import boto3
from django.conf import settings
from django.contrib.auth.decorators import login_required, user_passes_test
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse, reverse_lazy

from debatnl_online.consumers.lessons import LessonConsumer
from debatnl_online.consumers.vimeo import VimeoConsumer
from debatnl_online.permissions import group_check


@login_required
@user_passes_test(group_check, login_url=reverse_lazy('main_page'))
def view_add_lesson(request, course_id, chapter_id):
    if request.method == 'GET':
        context = {
            'course_id': course_id,
            'chapter_id': chapter_id,
        }
        return render(request,
                      'debatnl_online/modals/lessons/modal-create.html',
                      context=context)

    if request.method == 'POST':
        client = boto3.client(
            's3',
            aws_access_key_id=settings.AWS_ACCESS_KEY,
            aws_secret_access_key=settings.AWS_SECRET_KEY,
            region_name='eu-central-1'
        )

        file = request.FILES.get('video')

        bucket = settings.AWS_S3_BUCKET
        key = '{}/media/{}'.format(settings.AWS_S3_BUCKET_SUB_PATH, file.name)
        expiration_date = datetime.datetime.now() + datetime.timedelta(days=1)

        client.upload_fileobj(file, bucket, key, {
            'ACL': 'public-read',
            'Expires': expiration_date
        })

        file_url = '{}/{}/{}'.format(client.meta.endpoint_url, bucket, key)
        file_url = file_url.replace(' ', '+')

        vimeo_consumer = VimeoConsumer()
        video = vimeo_consumer.request_pull(file_url)
        vimeo_consumer.assign_preset_to_video(video.get('uri'))
        vimeo_consumer.set_allowed_domain(video.get('uri'),
                                          settings.VIMEO_ALLOWED_DOMAIN)

        vimeo_video_id = None
        video_url = video.get('uri', None)
        if video_url:
            vimeo_video_id = video_url.split('/')[-1]

        data = {
            'chapter_id': chapter_id,
            'name': request.POST.get('name'),
            'sequence': request.POST.get('sequence'),
            'is_first_lesson': request.POST.get('is_first_lesson'),
            'vimeo_video_id': vimeo_video_id,
            'video_url': video_url,
        }

        lesson_consumer = LessonConsumer()
        lesson_consumer.create_object(data)

    return HttpResponseRedirect(
        reverse('debatnl_online:e_learning:chapter',
                kwargs={'course_id': course_id}) + chapter_id + '/')


@login_required
@user_passes_test(group_check, login_url=reverse_lazy('main_page'))
def view_change_lesson(request, course_id, chapter_id, lesson_id):
    if request.method == 'GET':
        consumer = LessonConsumer()
        lesson = consumer.retrieve_object(lesson_id)

        context = {
            'lesson': lesson,
            'course_id': course_id,
            'chapter_id': chapter_id,
        }

        return render(request, 'debatnl_online/modals/lessons/modal-edit.html',
                      context=context)

    if request.method == 'POST':
        consumer = LessonConsumer()
        lesson = consumer.retrieve_object(lesson_id)

        if request.FILES.get('video'):
            client = boto3.client(
                's3',
                aws_access_key_id=settings.AWS_ACCESS_KEY,
                aws_secret_access_key=settings.AWS_SECRET_KEY,
                region_name='eu-central-1'
            )

            file = request.FILES.get('video')

            bucket = settings.AWS_S3_BUCKET
            key = '{}/media/{}'.format(settings.AWS_S3_BUCKET_SUB_PATH,
                                       file.name)
            expiration_date = datetime.datetime.now() + datetime.timedelta(
                days=1)

            client.upload_fileobj(file, bucket, key, {
                'ACL': 'public-read',
                'Expires': expiration_date
            })

            file_url = '{}/{}/{}'.format(client.meta.endpoint_url, bucket, key)
            file_url = file_url.replace(' ', '+')

            vimeo_consumer = VimeoConsumer()
            vimeo_consumer.delete_video(lesson.get('video_url'))

            video = vimeo_consumer.request_pull(file_url)
            vimeo_consumer.assign_preset_to_video(video.get('uri'))
            vimeo_consumer.set_allowed_domain(video.get('uri'),
                                              settings.VIMEO_ALLOWED_DOMAIN)

            vimeo_video_id = None
            video_url = video.get('uri', None)
            if video_url:
                vimeo_video_id = video_url.split('/')[-1]

            request.POST = request.POST.copy()
            request.POST['video_url'] = video.get('uri', None)
            request.POST['vimeo_video_id'] = vimeo_video_id

        consumer.update_object(lesson_id, request.POST)

    return HttpResponseRedirect(
        reverse('debatnl_online:e_learning:chapter',
                kwargs={'course_id': course_id}) + chapter_id + '/')


@login_required
@user_passes_test(group_check, login_url=reverse_lazy('main_page'))
def view_delete_lesson(request, course_id, chapter_id, lesson_id):
    if request.method == 'GET':
        consumer = LessonConsumer()
        lesson = consumer.retrieve_object(lesson_id)

        context = {
            'lesson': lesson,
            'course_id': course_id,
            'chapter_id': chapter_id
        }
        return render(request,
                      'debatnl_online/modals/lessons/modal-delete.html',
                      context=context)

    if request.method == 'POST':
        consumer = LessonConsumer()
        payload = {
            "is_deleted": True
        }
        consumer.update_object(lesson_id, payload)

    return HttpResponseRedirect(
        reverse('debatnl_online:e_learning:chapter',
                kwargs={'course_id': course_id}) + chapter_id + '/')
