from django.http import Http404
from django.views.generic.detail import SingleObjectMixin

from handouts.models import HandoutRequest, HandoutToken


class HandoutRequestTokenMixin(SingleObjectMixin):
    model = HandoutRequest

    def verify_token(self):
        """
        Verify if token is valid/exists.
        """
        token_key = self.request.GET.get('token')
        return HandoutToken.objects.filter(id=token_key).exists()

    def delete_token(self):
        """
        Delete token
        """
        obj = self.get_object()
        HandoutToken.objects.filter(handout_request=obj).delete()
