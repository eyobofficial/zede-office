from backoffice.models import Employee
from backoffice.utilities.emails import NotificationEmailMixin

from .bases import BaseBonusEmail


class NewClientBonusNotification(NotificationEmailMixin, BaseBonusEmail):
    template_name = 'email_3_new_bonus_notification.html'

    def __init__(self, bonus):
        super().__init__(bonus)
        trainer_name = self.bonus.declaration.user.first_name
        number = self.bonus.invoice_number
        self.subject = f'Bonus {trainer_name} ingediend met nummer {number}'
        self.recipient = Employee(first_name='Sharon', last_name='Kroes',
                                  email='kroes@debat.nl')
