# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-07-09 00:30
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('backoffice', '0012_auto_20170706_2258'),
    ]

    operations = [
        migrations.AlterField(
            model_name='employee',
            name='category',
            field=models.CharField(blank=True, choices=[('TRAINER', 'Trainer'), ('ADMINISTRATOR', 'Administrator'), ('EDITOR', 'Editor')], max_length=30, null=True),
        ),
    ]
