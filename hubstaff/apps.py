from django.apps import AppConfig


class HubstaffConfig(AppConfig):
    name = 'hubstaff'
