import mock
from django.contrib.auth.models import User
from django.test import TestCase, Client
from django.urls import reverse

from jotform.tests.factories import FormFactory


@mock.patch('jotform.views.sync_form')
@mock.patch('jotform.views.sync_questions')
@mock.patch('jotform.views.sync_submissions')
@mock.patch('jotform.views.sync_webhooks')
class SyncViewsTest(TestCase):
    def setUp(self):
        self.user = User.objects.create_user(username='test_user')
        self.client = Client()
        self.form = FormFactory()
        self.urls = [
            reverse('admin:sync_form', kwargs={'pk': self.form.pk}),
            reverse('admin:sync_webhooks', kwargs={'pk': self.form.pk}),
            reverse('admin:sync_questions', kwargs={'pk': self.form.pk}),
            reverse('admin:sync_submissions', kwargs={'pk': self.form.pk}),
        ]

    def _assert_get_request(self):
        for url in self.urls:
            with self.subTest(url=url):
                expected_url = '{}?next={}'.format(reverse('admin:login'), url)
                response = self.client.get(url, follow=True)
                self.assertRedirects(response, expected_url)

    def test_urls_as_anonymous_user(self, *_):
        self._assert_get_request()

    def test_urls_as_authenticated_user(self, *_):
        self.client.force_login(user=self.user)
        self._assert_get_request()

    def test_urls_as_admin_user(self, *_):
        self.user.is_superuser = True
        self.user.is_staff = True
        self.user.is_active = True
        self.user.save()
        self.client.force_login(user=self.user)

        for url in self.urls:
            with self.subTest(url=url):
                expected_template = 'admin/jotform/form/change_form.html'
                response = self.client.get(url, follow=True)
                self.assertEqual(response.status_code, 200)
                self.assertTemplateUsed(response, expected_template)
