import factory

from briefings.models import Deliverable, Briefing, BriefingFile, \
    DeliverableTrainer
from webcrm.factories import DeliveryFactory


class DeliverableTrainerFactory(factory.django.DjangoModelFactory):
    name = factory.Faker('name')
    email = factory.Faker('email')

    class Meta:
        model = DeliverableTrainer


class BriefingFactory(factory.django.DjangoModelFactory):
    time = factory.Faker('time')
    reason = factory.Faker('text')
    program = factory.Faker('text')
    address = factory.Faker('address')

    class Meta:
        model = Briefing


class DeliverableFactory(factory.django.DjangoModelFactory):
    briefing = factory.SubFactory(BriefingFactory)
    delivery = factory.SubFactory(DeliveryFactory)
    trainers_amount = 0
    status = Deliverable.PENDING

    class Meta:
        model = Deliverable


class BriefingFileFactory(factory.django.DjangoModelFactory):
    briefing = factory.SubFactory(BriefingFactory)
    file_name = factory.Sequence(lambda n: 'file_{}'.format(n))
    file = factory.django.FileField()

    class Meta:
        model = BriefingFile
