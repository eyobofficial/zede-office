from django.conf import settings
from django.urls import reverse

from backoffice.tests.bases import BaseEmailTestCase
from handouts.emails.reminders import MismatchingDeliveriesNotification, UnknownDeliveriesNotification
from handouts.models import HandoutToken
from handouts.tests.factories import HandoutRequestFactory
from webcrm.factories import DeliveryFactory


class HandoutEmailTest(BaseEmailTestCase):
    template_dir = '../templates/handouts/emails'
    obj_context_name = 'handouts'

    def setUp(self):
        super().setUp()
        self.delivery = DeliveryFactory()
        self.handout_request = HandoutRequestFactory()
        self.handout_request.deliveries.add(self.delivery)
        organization = self.delivery.organization
        product = self.delivery.product
        subject_addition = f'({organization} - {product})'

        name = self.handout_request.trainer
        dt = self.handout_request.training_date

        self.items = [
            {
                'class': MismatchingDeliveriesNotification,
                'args': (self.handout_request,),
                'template_name': 'email_7_mismatching_deliveries.html',
                'subject':  f'Olaf is in de war! ({name} - {dt:%d-%m-%Y})',
                'recipients': [self.admin.email],
                'context': self.get_mismatching_deliveries_assertions,
            },
            {
                'class': UnknownDeliveriesNotification,
                'args': (self.delivery,),
                'template_name': 'email_8_unknown_deliveries.html',
                'subject': f'Training niet herkend! {subject_addition}',
                'recipients': ['eichhorn@debat.nl'],
                'context': self.get_unknown_deliveries_assertions,
            }
        ]

    def get_mismatching_deliveries_assertions(self, context):
        token = HandoutToken.objects.get(handout_request=self.handout_request)
        view_name = 'handouts:correct-mismatched-delivery'
        path = reverse(view_name)
        expected_url = f'{settings.APP_HOSTNAME}{path}?token={token.id}'
        self.assertEqual(context.get('url'), expected_url)

    def get_unknown_deliveries_assertions(self, context):
        token = HandoutToken.objects.get(delivery=self.delivery)
        view_name = 'handouts:correct-unknown-delivery'
        path = reverse(view_name)
        expected_url = f'{settings.APP_HOSTNAME}{path}?token={token.id}'
        self.assertEqual(context.get('url'), expected_url)
        self.assertEqual(context.get('delivery'), self.delivery)
