from backoffice.utilities.emails import BaseEmail


class BaseSalaryEmail(BaseEmail):
    template_location = '../templates/salaries/emails'

    def __init__(self, declaration):
        self.declaration = declaration

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['declaration'] = self.declaration
        return context


class FeedbackNotificationEmail(BaseSalaryEmail):
    template_name = 'email_1_feedback_notification.html'

    def __init__(self, feedback_item):
        super().__init__(feedback_item.declaration)
        self.feedback_item = feedback_item
        self.recipient = self.declaration.user
        creator = f'{feedback_item.created_by.first_name}'
        self.subject = f'{creator} heeft een vraag over je declaratie'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['feedback_item'] = self.feedback_item
        return context


class ApprovedDeclarationEmail(BaseSalaryEmail):
    template_name = 'email_2_approved_declaration.html'
    subject = 'Hoera! Je declaratie is goedgekeurd'

    def __init__(self, declaration):
        super().__init__(declaration)
        self.recipient = declaration.user
