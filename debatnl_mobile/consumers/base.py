import requests
from requests.exceptions import ConnectionError

from django.conf import settings


class BaseConsumer:
    uri = None
    base_url = settings.DEBATNL_MOBILE_API
    headers = {
        'Authorization': f'Token {settings.DEBATNL_MOBILE_API_KEY}'
    }

    def get_url(self, object_id=None):
        """
        Build resource enpoint from the base URL & resource URI
        """
        url = f'{self.base_url}{self.uri}'
        if object_id:
            return f'{url}/{object_id}/'
        return f'{url}/'

    def ping(self, **kwargs):
        """
        Check for connection
        """
        connection = False
        url = self.get_url()
        try:
            requests.head(url, headers=self.headers)
            connection = True
        except ConnectionError:
            pass
        return connection

    def retrieve_all(self, **kwargs):
        """
        Retrieve a collection of resources
        """
        url = self.get_url()
        response = requests.get(url, headers=self.headers, params=kwargs)
        response.raise_for_status()
        return response.json()

    def retrieve_object(self, object_id, **kwargs):
        """
        Retrieve a single resource
        """
        url = self.get_url(object_id)
        response = requests.get(url, headers=self.headers)
        response.raise_for_status()
        return response.json()

    def update_object(self, object_id, data):
        """
        Update a resource
        """
        url = self.get_url(object_id)
        response = requests.patch(url, headers=self.headers, json=data)
        response.raise_for_status()
        return response.json()

    def create_object(self, data):
        """
        Create a new resource
        """
        url = self.get_url()
        response = requests.post(url, headers=self.headers, json=data)
        response.raise_for_status()
        return response.json()

    def delete_object(self, object_id):
        """
        Delete a resource
        """
        url = self.get_url(object_id)
        response = requests.delete(url, headers=self.headers)
        response.raise_for_status()
