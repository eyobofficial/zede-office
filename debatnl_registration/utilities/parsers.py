from dateutil.parser import parse

from debatnl_registration.models import PlannedTraining
from debatnl_registration.utilities.gravity_form import GravityFormClient


def parse_training_registration(data):
    training_duration = data.get('entry[40]', data.get('entry[126]'))
    duration = parse_training_duration(training_duration)

    # Retrieve training dates.
    training_dates = []
    for field_id in [4, 19, 22, 25, 27, 28, 29, 30]:
        field_name = 'entry[{}]'.format(field_id)
        dates = GravityFormClient.parse_date(data.get(field_name))

        if len(dates) == duration:
            dates.sort()
            training_dates = dates
            break

    # Retrieve participant count.
    participant_count = data.get('entry[119]')
    participant_count = int(
        participant_count) if participant_count.isdigit() else None

    # Retrieve participants.
    participants = parse_participant_data(data)

    return {
        'submission_id': data.get('entry[id]'),
        'form_id': data.get('entry[form_id]'),
        'training': data.get('entry[1]'),
        'training_dates': training_dates,
        'duration': duration,
        'participant_count': participant_count,
        'participants': participants,
        'registrant_first_name': data.get('entry[8]'),
        'registrant_last_name': data.get('entry[9]'),
        'registrant_email': data.get('entry[11]'),
        'registrant_phone': data.get('entry[10]'),
    }


def parse_2day_additional_training_registration(data):
    # Retrieve training dates.
    training_dates = []
    for field_id in [27, 28, 29, 30]:
        field_name = 'entry[{}]'.format(field_id)
        dates = GravityFormClient.parse_date(data.get(field_name))

        if len(dates) == 2:
            dates.sort()
            training_dates = dates
            break

    # Retrieve participant count.
    participant_count = data.get('entry[119]')
    participant_count = int(
        participant_count) if participant_count.isdigit() else None

    # Retrieve participants.
    participants = parse_participant_data(data)

    return {
        'submission_id': data.get('entry[id]'),
        'form_id': data.get('entry[form_id]'),
        'training': data.get('entry[1]'),
        'training_dates': training_dates,
        'duration': 2,
        'participant_count': participant_count,
        'participants': participants,
        'registrant_first_name': data.get('entry[8]'),
        'registrant_last_name': data.get('entry[9]'),
        'registrant_email': data.get('entry[11]'),
        'registrant_phone': data.get('entry[10]'),
    }


def parse_training_duration(training_type):
    days = ''
    for c in training_type:
        if c.isdigit():
            days = '{}{}'.format(days, c)
        else:
            break

    return int(days) if days else None


def parse_participant_data(data):
    field_groups = {
        '1': [51, 52, 64, 120],
        '2': [87, 84, 89, 125],
        '3': [91, 88, 85, 122],
        '4': [83, 96, 97, 121],
        '5': [95, 100, 101, 124],
        '6': [99, 92, 93, 123],
    }

    participants = []
    for key, value in field_groups.items():
        name = 'entry[{}]'.format(value[0])
        email = 'entry[{}]'.format(value[1])
        phone = 'entry[{}]'.format(value[2])
        dietary_wishes = 'entry[{}]'.format(value[3])

        if data.get(name):
            participants.append({
                'name': data.get(name),
                'email': data.get(email),
                'phone': data.get(phone),
                'dietary_wishes': data.get(dietary_wishes)
            })

    return participants


def parse_planned_training_data(data):
    dates = []

    for n in range(0, 3):
        day = data['date_{}_day'.format(n)]
        month = data['date_{}_month'.format(n)]
        year = data['date_{}_year'.format(n)]
        date = parse(
            '{}-{}-{}'.format(year, month, day),
            yearfirst=True,
            ignoretz=True
        )

        trainers = [
            v for k, v in data.items() if k.startswith(
                'date_{}_trainer_'.format(n)
            )
        ]

        dates.append({
            'sequence': n,
            'date': date.date() if date else None,
            'trainers': trainers
        })

    dates.sort(key=lambda d: d['date'])
    for index, date in enumerate(dates):
        date['sequence'] = index

    return {
        'training': data.get('training'),
        'dates': dates
    }


def parse_training_update(data):
    option = data.get('option')

    day = data.get('coaching_date_day_{}'.format(option))
    month = data.get('coaching_date_month_{}'.format(option))
    year = data.get('coaching_date_year_{}'.format(option))

    coaching_date = None
    if all([day, month, year]):
        coaching_date = parse(
            '{}-{}-{}'.format(year, month, day),
            yearfirst=True,
            ignoretz=True
        )

    new_planned_training = None
    if data.get('training_{}'.format(option)):
        try:
            new_planned_training = PlannedTraining.objects.get(
                id=data.get('training_{}'.format(option))
            )
        except PlannedTraining.DoesNotExist:
            pass

    return {
        'option': data.get('option'),
        'new_planned_training': new_planned_training,
        'coaching_date': coaching_date,
        'coaching_time': data.get('coaching_date_time_{}'.format(option)),
        'name': data.get('name_{}'.format(option)),
        'email': data.get('email_{}'.format(option)),
        'invoice_choice': data.get('invoice_choice_{}'.format(option)),
        'invoice_number': data.get('invoice_number_{}'.format(option)),
        'bank_account_number': data.get(
            'bank_account_number_{}'.format(option)
        ),
        'account_holder_name': data.get(
            'account_holder_name_{}'.format(option)
        ),
        'reference': data.get('reference_{}'.format(option)),
    }
