from backoffice.utilities.emails import BaseEmail


class RegistrationBaseEmail(BaseEmail):
    template_location = '../templates/debatnl_registration/emails/'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({'recipient': self.recipient})
        return context


class ParticipantBaseEmail(RegistrationBaseEmail):
    def __init__(self, participant):
        super().__init__()
        self.recipient = participant
        self.registration = participant.registration


class AttendanceBaseEmail(RegistrationBaseEmail):
    def __init__(self, attendance):
        super().__init__()
        self.recipient = attendance.participant


class TrainingDateBaseEmail(RegistrationBaseEmail):
    def __init__(self, training_date):
        super().__init__()
        self.training_date = training_date

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'training_date': self.training_date
        })
        return context


class PlannedTrainingBaseEmail(RegistrationBaseEmail):
    def __init__(self, planned_training):
        super().__init__()
        self.planned_training = planned_training

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({'planned_training': self.planned_training})
        return context
