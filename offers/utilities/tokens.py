from django.core.exceptions import ValidationError

from offers.models import OfferToken


def retrieve_offer_token(request, token_type):
    key = request.GET.get('token', request.POST.get('token'))
    email = request.GET.get('email', request.POST.get('email'))

    try:
        token = OfferToken.objects.get(key=key, email=email, type=token_type)
    except (OfferToken.DoesNotExist, ValidationError):
        token = None

    return token
