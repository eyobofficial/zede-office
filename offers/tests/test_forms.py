from django.test import TestCase

from offers.forms import OfferForm
from offers.tests.factories import OfferFactory, OfferInvestmentFactory


class OfferFormTest(TestCase):
    """
    Test the offer form.
    """

    def setUp(self):
        self.offer = OfferFactory()
        self.investment = OfferInvestmentFactory(offer=self.offer)

    def test_removal_of_trainers_when_no_trainer_proposed(self):
        self.assertEqual(self.offer.trainers.count(), 2)

        dr_name = self.offer.delivery_responsible_name
        dr_email = self.offer.delivery_responsible_email
        trainers = self.offer.trainers.all()
        trainer_ids = sorted([t.pk for t in trainers])

        data = {
            'organization': 'update organization',
            'contact_person': 'update cp',
            'photo': self.offer.photo.id,
            'delivery_responsible_name': dr_name,
            'delivery_responsible_email': dr_email,
            'question_background': self.offer.question_background,
            'program_structure': self.offer.program_structure,
            'is_trainer_proposed': '0',
            'editor': self.offer.editor.pk,
            'trainers': trainer_ids,
            'investments-TOTAL_FORMS': 1,
            'investments-INITIAL_FORMS': 1,
            'investments-MIN_NUM_FORMS': 0,
            'investments-MAX_NUM_FORMS': 1000,
            'investments-0-id': self.investment.id,
            'investments-0-subject': self.investment.subject,
            'investments-0-range': self.investment.range,
        }

        form = OfferForm(instance=self.offer, data=data)
        if form.is_valid():
            form.save()

        self.offer.refresh_from_db()

        self.assertTrue(form.is_valid())
        self.assertEqual(self.offer.trainers.count(), 0)
