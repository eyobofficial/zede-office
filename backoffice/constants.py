"""
Groups
"""
GROUP_BACKOFFICE = 'backoffice'
GROUP_DEBATNLONLINE = 'debatnl_online'
GROUP_DEBATNLREGISTRATION = 'debatnl_registration'
GROUP_BRIEFINGS = 'briefings'
GROUP_FEEDBACK = 'feedback'
GROUP_INTAKES = 'intakes'
GROUP_OFFERS = 'offers'
GROUP_SCANS = 'scans'
GROUP_FOLLOWUPS = 'follow_ups'
GROUP_PLANNING = 'planning'
GROUP_SCHEDULES = 'schedules'
GROUP_CCD = 'ccd'
GROUP_DEBATNL_MOBILE = 'debatnl_mobile'
GROUP_MARKETING = 'marketing'
GROUP_DECLARATIONS = 'declarations'
GROUP_SALARIES = 'salaries'
GROUP_EDUCATION = 'education'

"""
E-mails
"""
EMAIL_REMINDER = 'REMINDER'
EMAIL_NOTIFICATION = 'NOTIFICATION'
