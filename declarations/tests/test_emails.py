from backoffice.tests.bases import BaseEmailTestCase

from declarations.emails.reminders import DeclarationReminder
from declarations.emails.notifications import NewClientBonusNotification
from .factories import DeclarationFactory, ClientBonusFactory


class DeclarationReminderEmailTest(BaseEmailTestCase):
    """
    Tests for the `DeclarationReminder` email
    """
    template_dir = '../templates/declarations/emails'
    obj_context_name = 'declarations'

    def setUp(self):
        super().setUp()
        self.declaration = DeclarationFactory()

        self.items = [
            {
                'class': DeclarationReminder,
                'args': (self.declaration, ),
                'template_name': 'email_2_declaration_reminder.html',
                'subject': 'SPOED: We missen je declaratie',
                'recipients': [self.declaration.user.email]
            },
        ]


class NewClientBonusNotificationEmailTests(BaseEmailTestCase):
    """
    Tests for the `NewClientBonusNotification` email
    """
    template_dir = '../templates/declarations/emails'
    obj_context_name = 'declarations'

    def setUp(self):
        super().setUp()
        self.bonus = ClientBonusFactory()
        trainer_name = self.bonus.declaration.user.first_name
        invoice_number = self.bonus.invoice_number

        self.items = [
            {
                'class': NewClientBonusNotification,
                'args': (self.bonus, ),
                'template_name': 'email_3_new_bonus_notification.html',
                'subject': f'Bonus {trainer_name} ingediend met nummer '
                           f'{invoice_number}',
                'recipients': ['kroes@debat.nl']
            },
        ]

