import requests


class WebCRMClient:
    _base_url = 'https://api.webcrm.com'

    def __init__(self, auth_code):
        self._auth_code = auth_code
        self._retrieve_access_token()

    def _get_headers(self):
        return {'Authorization': f'{self._token_type} {self._access_token}'}

    def _retrieve_access_token(self):
        url = f'{self._base_url}/auth/apilogin'
        response = requests.post(url, headers={'authCode': self._auth_code})
        response.raise_for_status()
        content = response.json()

        self._token_type = content['TokenType']
        self._access_token = content['AccessToken']

    def _get_with_query(self, query, page, size=500):
        url = f'{self._base_url}/Queries'
        headers = self._get_headers()
        payload = {'script': query, 'page': page, 'size': size}
        response = requests.get(url, params=payload, headers=headers)
        response.raise_for_status()
        return response.json()

    def _get_all_pages_of_items(self, query):
        items = []
        page = 1
        while True:
            content = self._get_with_query(query, page)
            items += content
            page += 1

            if len(content) == 0:
                break

        return items

    def get_briefings_deliverables(self, start_date):
        query = "SELECT DeliveryId, DeliveryOrderDate, DeliveryProduct, " \
                "DeliveryDescription, DeliveryResponsible, " \
                "OrganisationName, PersonFirstName, PersonLastName," \
                "PersonEmail, PersonDirectPhone, PersonMobilePhone,  " \
                "DeliveryOpportunityCustom5, DeliveryCustom5, " \
                "DeliveryCustom6, DeliveryCustom7, DeliveryCustom8, " \
                "DeliveryOpportunityCustom1, DeliveryOpportunityCustom2, " \
                "DeliveryOpportunityCustom3, DeliveryOpportunityCustom4 " \
                "FROM Delivery " \
                "INNER JOIN Organisation ON Organisation.OrganisationId=" \
                "Delivery.DeliveryOrganisationId " \
                "INNER JOIN Person ON Person.PersonId=" \
                "Delivery.DeliveryPersonId " \
                "WHERE DeliveryOpportunityCustom5='Ja' " \
                "AND DeliveryProduct<>'Digitale intake' " \
                "AND DeliveryProduct<>'Digitale outtake' " \
                "AND DeliveryProduct<>'Debat.NL Online' " \
                "AND DeliveryOrganisationId <> 755"
        query = f"{query} AND DeliveryOrderDate >= '{start_date}'"
        return self._get_all_pages_of_items(query)

    def get_follow_ups_deliverables(self, start_date):
        query = "SELECT DeliveryId, DeliveryOrderDate, DeliveryProduct, " \
                "OrganisationName, OrganisationIndustry, " \
                "OrganisationDivisionName, " \
                "PersonName, PersonFirstName, PersonLastName, PersonEmail " \
                "FROM Delivery " \
                "INNER JOIN Organisation ON Organisation.OrganisationId=" \
                "Delivery.DeliveryOrganisationId " \
                "INNER JOIN Person ON Person.PersonId=" \
                "Delivery.DeliveryPersonId " \
                "WHERE DeliveryOpportunityCustom5='Ja' " \
                "AND DeliveryProduct<>'Digitale intake' " \
                "AND DeliveryProduct<>'Digitale outtake' " \
                "AND DeliveryProduct<>'Debat.NL Online'"
        query = f"{query} AND DeliveryOrderDate > '{start_date}'"
        return self._get_all_pages_of_items(query)

    def get_ccd_persons(self):
        query = "SELECT PersonId, PersonFirstName, PersonLastName, " \
                "PersonEmail FROM Person " \
                "WHERE PersonStatus='Active' AND PersonCustom2='Yes'"
        return self._get_all_pages_of_items(query)

    def get_monthly_deliverables_by_ccd_person(self, person, start_date,
                                               end_date):
        query = "SELECT DeliveryId, DeliveryOrderDate, DeliveryProduct, " \
                "DeliveryDescription, DeliveryResponsible, " \
                "OrganisationName, " \
                "DeliveryOpportunityCustom5, DeliveryCustom5, " \
                "DeliveryCustom6, DeliveryCustom7, DeliveryCustom8, " \
                "DeliveryOpportunityCustom1, DeliveryOpportunityCustom2, " \
                "DeliveryOpportunityCustom3, DeliveryOpportunityCustom4 " \
                "FROM Delivery " \
                "INNER JOIN Organisation ON Organisation.OrganisationId=" \
                "Delivery.DeliveryOrganisationId " \
                "WHERE DeliveryOpportunityCustom5='Ja' " \
                "AND DeliveryProduct<>'Digitale intake' " \
                "AND DeliveryProduct<>'Digitale outtake' " \
                "AND DeliveryProduct<>'Debat.NL Online'"
        query = f"{query} AND DeliveryOrderDate >= '{start_date}'"
        query = f"{query} AND DeliveryOrderDate <= '{end_date}'"
        query = f"{query} AND (" \
                f"DeliveryOpportunityCustom1='{person.get_full_name()}' OR " \
                f"DeliveryOpportunityCustom2='{person.get_full_name()}' OR " \
                f"DeliveryOpportunityCustom3='{person.get_full_name()}' OR " \
                f"DeliveryOpportunityCustom4='{person.get_full_name()}' OR " \
                f"DeliveryCustom5='{person.get_full_name()}' OR " \
                f"DeliveryCustom6='{person.get_full_name()}' OR " \
                f"DeliveryCustom7='{person.get_full_name()}' OR " \
                f"DeliveryCustom8='{person.get_full_name()}'" \
                f")"
        return self._get_all_pages_of_items(query)

    def get_user_by_id(self, user_id):
        url = f'{self._base_url}/Users/{user_id}'
        headers = self._get_headers()
        response = requests.get(url, headers=headers)
        response.raise_for_status()
        return response.json()

    def get_person_by_name(self, name):
        query = f"SELECT * FROM Person WHERE PersonName='{name}'"
        query = f"{query} AND PersonEmail<>'' ORDER BY PersonUpdatedAt DESC"
        content = self._get_with_query(query, page=1, size=1)
        return content[0] if len(content) > 0 else []

    def get_person_by_first_and_last_name(self, first_name, last_name):
        query = "SELECT * FROM Person WHERE"
        query = f"{query} PersonFirstName LIKE '%{first_name}%'"
        query = f"{query} AND PersonLastName LIKE '%{last_name}%'"
        query = f"{query} AND PersonEmail<>'' ORDER BY PersonUpdatedAt DESC"
        content = self._get_with_query(query, page=1, size=1)
        return content[0] if len(content) > 0 else []

    def get_industry_names(self):
        query = "SELECT OrganisationIndustry FROM Organisation"
        content = self._get_all_pages_of_items(query)

        industry_names = set()
        for item in content:
            industry_name = item['OrganisationIndustry']

            if industry_name and industry_name != '--Kies--':
                industry_names.add(industry_name)

        return list(industry_names)

    def get_product_names(self):
        query = "SELECT DeliveryProduct FROM Delivery"
        content = self._get_all_pages_of_items(query)

        product_names = set()
        for item in content:
            product_name = item['DeliveryProduct']

            if product_name and product_name != '--Kies--':
                product_names.add(product_name)

        return list(product_names)
