from backoffice.utilities.emails import ReminderEmailMixin

from .bases import DeclarationEmail


class DeclarationReminder(ReminderEmailMixin, DeclarationEmail):
    template_name = 'email_2_declaration_reminder.html'

    def __init__(self, declaration):
        super().__init__(declaration)
        self.subject = 'SPOED: We missen je declaratie'
        self.recipient = self.declaration.user
