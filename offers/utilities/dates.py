from datetime import timedelta


def calculate_workday(initial_date, amount_days=0):
    """
    Calculate a date by adding the amount of days to the initial date.
    If the date is a day in the weekend, the date will be set
    to the next working day (Monday).

    :param initial_date: (datetime) The initial date.
    :param amount_days: (int) The amount of days to add.
    """
    business_days_to_add = amount_days
    current_date = initial_date

    while business_days_to_add > 0:
        current_date += timedelta(days=1)

        if current_date.weekday() in [5, 6]:
            continue

        business_days_to_add -= 1

    return current_date


def is_workday(date):
    return date.weekday() not in [5, 6]
