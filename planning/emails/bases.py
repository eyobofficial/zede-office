from backoffice.utilities.emails import BaseEmail


class BasePlanningEmail(BaseEmail):
    template_location = '../templates/planning/emails'
    email_type = ''


class BasePlanningRequestEmail(BasePlanningEmail):
    def __init__(self, planning_request):
        self.planning_request = planning_request

    def get_context_data(self, **kwargs):
        kwargs['planning_request'] = self.planning_request
        return super().get_context_data(**kwargs)


class BaseScheduleEmail(BasePlanningEmail):
    def __init__(self, schedule):
        self.schedule = schedule

    def get_context_data(self, **kwargs):
        kwargs['schedule'] = self.schedule
        return super().get_context_data(**kwargs)
