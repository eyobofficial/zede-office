from unittest import mock
from unittest.mock import Mock

from django.test import override_settings, SimpleTestCase

from jotform.tasks import consume_jotform_submissions_queue


@override_settings(AWS_ACCESS_KEY='12345')
@override_settings(AWS_SECRET_KEY='12345')
@override_settings(JOT_FORM_AWS_QUEUE='test-queue')
class ConsumeSubmissionsTaskTest(SimpleTestCase):

    @mock.patch('boto3.resource')
    @mock.patch('jotform.tasks.process_submission_from_webhook')
    def test_message_processor_loop(self, mock_processor, mock_client):
        results_1 = [Mock()] * 10
        results_2 = [Mock()] * 10
        results_3 = [Mock()] * 5
        side_effect = [results_1, results_2, results_3]

        mock_queue = Mock()
        mock_sqs = Mock()
        mock_queue.receive_messages.side_effect = side_effect
        mock_sqs.get_queue_by_name.return_value = mock_queue
        mock_client.return_value = mock_sqs

        consume_jotform_submissions_queue()

        self.assertEqual(mock_queue.receive_messages.call_count, 3)
        self.assertEqual(mock_processor.call_count, 25)
