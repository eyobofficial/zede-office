from django.conf import settings
from django.db import models

from webcrm.client import WebCRMClient


class Organization(models.Model):
    STATE_AVAILABLE = 'AVAILABLE'
    STATE_DELETED = 'DELETED'

    STATE_CHOICES = (
        (STATE_AVAILABLE, 'Available'),
        (STATE_DELETED, 'Deleted'),
    )

    name = models.CharField(max_length=120)
    division_name = models.CharField(max_length=120, default='', blank=True)
    industry = models.CharField(max_length=120, default='', blank=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()
    state = models.CharField(max_length=50, choices=STATE_CHOICES,
                             default=STATE_AVAILABLE)

    class Meta:
        ordering = ('name', )

    def __str__(self):
        return self.name


class Person(models.Model):
    STATE_AVAILABLE = 'AVAILABLE'
    STATE_DELETED = 'DELETED'

    STATE_CHOICES = (
        (STATE_AVAILABLE, 'Available'),
        (STATE_DELETED, 'Deleted'),
    )

    first_name = models.CharField(max_length=120)
    last_name = models.CharField(max_length=120)
    name = models.CharField(max_length=120)
    email = models.EmailField(default='', blank=True)
    direct_phone = models.CharField(max_length=120, default='', blank=True)
    mobile_phone = models.CharField(max_length=120, default='', blank=True)
    status = models.CharField(max_length=120, default='', blank=True)
    custom1 = models.CharField(max_length=120, default='', blank=True)
    custom2 = models.CharField(max_length=120, default='', blank=True)
    custom3 = models.CharField(max_length=120, default='', blank=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()
    state = models.CharField(max_length=50, choices=STATE_CHOICES,
                             default=STATE_AVAILABLE)

    class Meta:
        ordering = ('name', )

    def __str__(self):
        return self.name


class User(models.Model):
    STATE_AVAILABLE = 'AVAILABLE'
    STATE_DELETED = 'DELETED'

    STATE_CHOICES = (
        (STATE_AVAILABLE, 'Available'),
        (STATE_DELETED, 'Deleted'),
    )

    name = models.CharField(max_length=120)
    email = models.EmailField(default='', blank=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()
    state = models.CharField(max_length=50, choices=STATE_CHOICES,
                             default=STATE_AVAILABLE)

    class Meta:
        ordering = ('name', )

    def __str__(self):
        return self.name


class Delivery(models.Model):
    STATE_AVAILABLE = 'AVAILABLE'
    STATE_DELETED = 'DELETED'

    STATE_CHOICES = (
        (STATE_AVAILABLE, 'Available'),
        (STATE_DELETED, 'Deleted'),
    )

    organization = models.ForeignKey(Organization)
    order_date = models.DateTimeField()
    product = models.CharField(max_length=120, default='', blank=True)
    description = models.TextField(default='', blank=True)
    responsible = models.ForeignKey(User, null=True, blank=True,
                                    related_name='deliveries')
    person = models.ForeignKey(Person, null=True, blank=True,
                               related_name='deliveries')
    custom5 = models.CharField(max_length=120, default='', blank=True,
                               help_text='Trainer 1')
    custom6 = models.CharField(max_length=120, default='', blank=True,
                               help_text='Trainer 2')
    custom7 = models.CharField(max_length=120, default='', blank=True,
                               help_text='Trainer 3')
    custom8 = models.CharField(max_length=120, default='', blank=True,
                               help_text='Trainer 4')
    opportunity_custom1 = models.CharField(max_length=120, default='',
                                           blank=True, help_text='Trainer 5')
    opportunity_custom2 = models.CharField(max_length=120, default='',
                                           blank=True, help_text='Trainer 6')
    opportunity_custom3 = models.CharField(max_length=120, default='',
                                           blank=True, help_text='Trainer 7')
    opportunity_custom4 = models.CharField(max_length=120, default='',
                                           blank=True, help_text='Trainer 8')
    opportunity_custom5 = models.CharField(max_length=120, default='',
                                           blank=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()
    state = models.CharField(max_length=50, choices=STATE_CHOICES,
                             default=STATE_AVAILABLE)

    trainers = models.ManyToManyField(Person,
                                      related_name='training_deliveries')

    class Meta:
        verbose_name_plural = 'deliveries'
        ordering = ('order_date', 'organization')

    def __str__(self):
        name = f'{self.order_date.date()} | {self.organization}'
        return f'{name} - {self.product}'

    def get_trainer_names(self):
        fields = [self.custom5, self.custom6, self.custom7, self.custom8,
                  self.opportunity_custom1, self.opportunity_custom2,
                  self.opportunity_custom3, self.opportunity_custom4]

        return [value for value in fields if value]

    def assign_trainers(self):
        self.trainers.clear()

        trainer_names = self.get_trainer_names()

        for name in trainer_names:
            persons = Person.objects.filter(name=name, email__isnull=False)
            persons = persons.exclude(email='')
            persons = persons.exclude(state=Person.STATE_DELETED)
            person = persons.order_by('updated_at').last()

            if person:
                self.trainers.add(person)


class DeliveryTrainer(models.Model):
    name = models.CharField(max_length=120)
    email = models.CharField(max_length=120)

    def __str__(self):
        return self.name


class Opportunity(models.Model):
    STATE_AVAILABLE = 'AVAILABLE'
    STATE_DELETED = 'DELETED'

    STATE_CHOICES = (
        (STATE_AVAILABLE, 'Available'),
        (STATE_DELETED, 'Deleted'),
    )

    organization = models.ForeignKey(Organization)
    order_date = models.DateTimeField()
    product = models.CharField(max_length=120, default='', blank=True)
    description = models.TextField(default='', blank=True)
    person = models.ForeignKey(Person, null=True, blank=True,
                               related_name='opportunities',
                               help_text='Customer contact')
    level = models.IntegerField(blank=True, null=True)
    level_text = models.CharField(max_length=120, default='', blank=True)
    number = models.CharField(max_length=25, default='', blank=True)
    assigned_to = models.ForeignKey(User, blank=True, null=True,
                                    related_name='opportunities_as_1',
                                    help_text='Sales responsible')
    assigned_to2 = models.ForeignKey(User, blank=True, null=True,
                                     related_name='opportunities_as_2')
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()
    state = models.CharField(max_length=50, choices=STATE_CHOICES,
                             default=STATE_AVAILABLE)

    class Meta:
        verbose_name_plural = 'opportunities'
        ordering = ('order_date', 'organization')

    def __str__(self):
        name = f'{self.order_date.date()} | {self.organization}'
        return f'{name} - {self.product}'

    def update_on_webcrm(self, payload):
        if settings.ENVIRONMENT == 'PRODUCTION':
            client = WebCRMClient(settings.WEBCRM_AUTH_CODE)
            return client.update_opportunity_by_id(self.pk, payload)


class Event(models.Model):
    STATE_AVAILABLE = 'AVAILABLE'
    STATE_DELETED = 'DELETED'

    STATE_CHOICES = (
        (STATE_AVAILABLE, 'Available'),
        (STATE_DELETED, 'Deleted'),
    )

    organization = models.ForeignKey(Organization)
    assigned_to = models.ForeignKey(User, blank=True, null=True)
    description = models.TextField(default='', blank=True)
    type = models.CharField(max_length=120, default='', blank=True)
    date_time = models.DateTimeField(null=True, blank=True)
    person = models.ForeignKey(Person, null=True, blank=True)
    participant_ids = models.CharField(max_length=120, default='', blank=True)
    custom1 = models.CharField(max_length=120, default='', blank=True)
    custom2 = models.CharField(max_length=120, default='', blank=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()
    state = models.CharField(max_length=50, choices=STATE_CHOICES,
                             default=STATE_AVAILABLE)
    participants = models.ManyToManyField(User, related_name='events')

    class Meta:
        ordering = ('date_time', 'organization')

    def __str__(self):
        name = f'{self.date_time.date()} | {self.organization}'

        if self.assigned_to:
            name = f'{name} - {self.assigned_to.name}'

        return name

    def assign_participants(self):
        participant_ids = self.participant_ids.split(';')
        participant_ids = [n for n in participant_ids if n and n.isnumeric()]

        self.participants.clear()

        for pk in participant_ids:
            users = User.objects.filter(pk=pk, email__isnull=False)
            users = users.exclude(email='')
            users = users.exclude(state=Person.STATE_DELETED)
            user = users.order_by('updated_at').last()

            if user:
                self.participants.add(user)
