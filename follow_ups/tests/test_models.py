from datetime import datetime

import pendulum
import pytz
from django.conf import settings
from django.test import TestCase

from follow_ups.tests.factories import FollowUpFactory
from webcrm.factories import OpportunityFactory, DeliveryFactory, OrganizationFactory
from webcrm.models import Delivery


class FollowUpModelTest(TestCase):
    """
    Tests for FollowUpEvent model
    """

    def setUp(self):
        self.organization = OrganizationFactory()
        opportunity = OpportunityFactory(organization=self.organization)
        self.follow_up = FollowUpFactory(opportunity=opportunity)

        tz = pytz.timezone(settings.TIME_ZONE)
        past_dt = datetime(2019, 6, 1, tzinfo=tz)
        future_dt = datetime(2019, 12, 12, tzinfo=tz)
        self.past_delivery = DeliveryFactory(order_date=past_dt,
                                             organization=self.organization,
                                             state=Delivery.STATE_AVAILABLE)
        self.future_delivery = DeliveryFactory(order_date=future_dt,
                                               organization=self.organization,
                                               state=Delivery.STATE_AVAILABLE)

        # Deleted deliveries
        DeliveryFactory(order_date=past_dt,
                        organization=self.organization,
                        state=Delivery.STATE_DELETED)

        DeliveryFactory(order_date=future_dt,
                        organization=self.organization,
                        state=Delivery.STATE_DELETED)

    def test_get_past_deliveries(self):
        """
        Test the retrieval of past deliveries
        """
        known = pendulum.datetime(2019, 6, 20, tz=settings.TIME_ZONE)
        with pendulum.test(known):
            past_deliveries = self.follow_up.get_past_deliveries()

        self.assertEqual(past_deliveries.count(), 1)
        self.assertEqual(past_deliveries.first(), self.past_delivery)

    def test_get_future_deliveries(self):
        """
        Test the retrieval of past deliveries
        """
        known = pendulum.datetime(2019, 6, 20, tz=settings.TIME_ZONE)
        with pendulum.test(known):
            future_deliveries = self.follow_up.get_future_deliveries()

        self.assertEqual(future_deliveries.count(), 1)
        self.assertEqual(future_deliveries.first(), self.future_delivery)
