from django.db.models import Q
from django.urls import reverse_lazy

from backoffice.constants import GROUP_BRIEFINGS
from backoffice.mixins import GroupAccessMixin

from .forms import BriefingForm, DeliverableFormSet
from .models import Briefing, Deliverable


class BaseBriefingMixin(GroupAccessMixin):
    model = Briefing
    form_class = BriefingForm
    success_url = reverse_lazy('briefings:overview')
    access_groups = [GROUP_BRIEFINGS]

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        deliverable_id = self.kwargs['deliverable_id']
        data = self.request.POST
        context['pending_deliverables'] = self.get_related_deliverables()
        context['deliverable'] = Deliverable.objects.get(pk=deliverable_id)
        if data:
            context['deliverable_formset'] = DeliverableFormSet(data)
        else:
            context['deliverable_formset'] = DeliverableFormSet()

        return context

    def get_related_deliverables(self):
        deliverable = Deliverable.objects.get(pk=self.kwargs['deliverable_id'])
        organization = deliverable.delivery.organization
        deliverables = Deliverable.objects.filter(
            status=Deliverable.PENDING, delivery__organization=organization
        )
        deliverables = deliverables.exclude(pk=deliverable.pk)
        deliverables = deliverables.exclude(delivery__product='Debatscan')
        deliverables = deliverables.exclude(
            delivery__product='Voorzittersscan'
        )

        if not self.request.user.has_perm('briefings.view_all_briefings'):
            deliverables = deliverables.filter(
                Q(delivery__responsible__email=self.request.user.email) |
                Q(delivery__trainers__email=self.request.user.email)
            ).distinct()

        return deliverables
