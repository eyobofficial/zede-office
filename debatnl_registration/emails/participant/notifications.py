from django.contrib.auth.models import User

from debatnl_registration.emails.bases import ParticipantBaseEmail, \
    RegistrationBaseEmail
from debatnl_registration.models import TrainingDate
from debatnl_registration.utilities.helpers import get_jotform_url, \
    get_springest_url, \
    get_training_continuation_data
from shared.enums import RoleName


class RegistrationConfirmationEmail(ParticipantBaseEmail):
    template_name = 'email_1_registration_confirmation.html'

    def __init__(self, participant, with_travel_directions=False):
        super().__init__(participant)
        self.with_travel_directions = with_travel_directions
        training_name = self.registration.training
        self.subject = f'Uw inschrijving voor de training {training_name}'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        qs_kwargs = {'attendance__participant': self.recipient}
        training_dates = TrainingDate.objects.filter(**qs_kwargs).all()
        context.update({
            'with_travel_directions': self.with_travel_directions,
            'button_url': get_jotform_url(self.registration.training),
            'training_dates': training_dates
        })
        return context


class ReviewTrainingNotificationEmail(ParticipantBaseEmail):
    template_name = 'email_5_review_training_notification.html'
    subject = 'Uw feedback'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        training = self.registration.training
        duration = self.registration.duration
        context.update({'button_url': get_springest_url(training, duration)})
        return context


class TrainingContinuationNotificationEmail(ParticipantBaseEmail):
    template_name = 'email_6_training_continuation_notification.html'
    subject = 'Wat is de volgende?'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        data = get_training_continuation_data(self.registration.training)
        context.update({
            'button_url': data['url'],
            'button_text': data['button_text']
        })
        return context


class ParticipantCancellationChoiceEmail(RegistrationBaseEmail):
    template_name = 'email_18b_participant_choice.html'

    def __init__(self, token, data):
        super().__init__()
        self.planned_training = token.planned_training
        self.training = self.planned_training.training
        self.subject = f'Wijziging deelnemer {self.training.name}'
        self.token = token
        self.data = data
        self.recipient = User.objects.filter(
            profile__roles__name=RoleName.ADMIN.value,
            is_active=True
        ).first()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'recipient': self.recipient,
            'participant': self.token.participant,
            'data': self.data
        })
        return context
