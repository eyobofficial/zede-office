import pendulum

from django.conf import settings
from django.test import TestCase, Client
from django.urls import reverse

from backoffice.constants import GROUP_CCD
from backoffice.tests.factories import UserFactory
from users.models import AppPermission

from .factories import PersonFactory, URLTokenFactory


class OverviewViewTest(TestCase):
    """
    Tests for `OverviewView` view
    """
    fixtures = ['app_permissions']

    def setUp(self):
        self.user = UserFactory()
        app_permission = AppPermission.objects.get(app_name=GROUP_CCD)
        self.user.profile.app_permissions.add(app_permission)
        self.client = Client()
        self.url = reverse('ccd:overview')
        self.template = 'ccd/overview.html'

    def test_request_with_anonymous_user(self):
        """
        Test a request to `OverviewView` view from anonymous user
        """
        response = self.client.get(self.url)

        # Assertions
        self.assertEqual(response.status_code, 302)

    def test_request_with_authenticated_user(self):
        """
        Test a request to `OverviewView` view from authenticated user
        """
        self.client.force_login(self.user)
        PersonFactory.create_batch(5)
        response = self.client.get(self.url)

        # Assertions
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.context['person_list']), 5)
        self.assertTemplateUsed(response, self.template)


class PurchaseOrderViewTest(TestCase):
    """
    Tests for `PurchaseOrderView` view
    """
    fixtures = ['app_permissions']

    def setUp(self):
        self.user = UserFactory()
        app_permission = AppPermission.objects.get(app_name=GROUP_CCD)
        self.user.profile.app_permissions.add(app_permission)
        self.client = Client()
        self.person = PersonFactory()

    def test_request_with_anonymous_user(self):
        """
        Test a request to `PurchaseOrderView` view from anonymous user
        """
        url_token = URLTokenFactory(person=self.person)
        url = reverse('ccd:purchase-orders', args=[url_token.pk])
        response = self.client.get(url)

        # Assertions
        self.assertEqual(response.status_code, 200)

    def test_request_with_authenticated_user_with_valid_date(self):
        """
        Test a request to `PurchaseOrderView` view from authenticated user
        with token having a valid date (today < token.valid_date )
        """
        self.client.force_login(self.user)
        url_token = URLTokenFactory(person=self.person)
        url = reverse('ccd:purchase-orders', args=[url_token.pk])
        template = 'ccd/public/person_deliverables.html'
        response = self.client.get(url)

        # Assertions
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, template)

    def test_request_with_authenticated_user_with_expired_date(self):
        """
        Test a request to `PurchaseOrderView` view from authenticated user
        with expired token (today > token.valid_date )
        """
        self.client.force_login(self.user)
        today = pendulum.today(settings.TIME_ZONE)
        expired_date = today.subtract(days=10).date()
        url_token = URLTokenFactory(
            person=self.person,
            valid_until=expired_date
        )
        url = reverse('ccd:purchase-orders', args=[url_token.pk])
        response = self.client.get(url)

        # Assertions
        self.assertEqual(response.status_code, 302)


class ExpiredPageViewTest(TestCase):
    """
    Tests for `ExpiredPageView` views
    """
    fixtures = ['app_permissions']

    def setUp(self):
        self.client = Client()
        self.url = reverse('ccd:expired-page')
        self.template = 'ccd/public/expired_page.html'

    def test_request_with_anonymous_user(self):
        response = self.client.get(self.url)

        # Assertions
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, self.template)


class PersonUpdateViewTest(TestCase):
    """
    Tests for `PersonUpdateView` view
    """
    fixtures = ['app_permissions']

    def setUp(self):
        self.user = UserFactory()
        app_permission = AppPermission.objects.get(app_name=GROUP_CCD)
        self.user.profile.app_permissions.add(app_permission)
        self.client = Client()
        self.person = PersonFactory()
        self.url = reverse('ccd:person-update', args=(self.person.pk, ))
        self.template = 'ccd/overview.html'

    def test_request_with_anonymous_user(self):
        """
        Test a request to `PersonUpdateView` view from anonymous user
        """
        response = self.client.get(self.url)

        # Assertions
        self.assertEqual(response.status_code, 302)

    def test_request_with_authenticated_user(self):
        """
        Test a request to `PersonUpdateView` view from authenticated
        user
        """
        self.client.force_login(self.user)
        response = self.client.get(self.url)

        # Assertions
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, self.template)

    def test_post_request_when_user_checks_a_person(self):
        """
        Test a valid post request to `PersonUpdateView` when user
        checks a person
        """
        self.client.force_login(self.user)
        self.person.is_active = False
        self.person.save()

        payload = {
            'is_active': True
        }
        response = self.client.post(self.url, payload)
        self.person.refresh_from_db()

        # Assertions
        self.assertEqual(response.status_code, 302)
        self.assertTrue(self.person.is_active)

    def test_post_request_when_user_unchecks_a_person(self):
        """
        Test a valid post request to `PersonUpdateView` when user
        unchecks a person
        """
        self.client.force_login(self.user)
        payload = {
            'is_active': False
        }
        response = self.client.post(self.url, payload)
        self.person.refresh_from_db()

        # Assertions
        self.assertEqual(response.status_code, 302)
        self.assertFalse(self.person.is_active)
