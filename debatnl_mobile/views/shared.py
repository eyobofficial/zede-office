from backoffice.views import BaseSignS3UploadURLView


class SignS3DebatNLMobileView(BaseSignS3UploadURLView):
    aws_s3_bucket = 'debatnl-mobile'
