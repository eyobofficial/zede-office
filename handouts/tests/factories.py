import factory

from django.utils import timezone

from handouts.models import HandoutRequest, Handout


class HandoutRequestFactory(factory.django.DjangoModelFactory):
    submission_id = factory.Sequence(lambda n: n)
    trainee_name = factory.Faker('name')
    trainee_email = factory.Faker('email')
    training_date = factory.Faker('date_this_year')
    trainer = factory.Faker('name')
    submitted_at = factory.LazyFunction(timezone.now)

    class Meta:
        model = HandoutRequest


class HandoutFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Handout
