import uuid
from unicodedata import normalize

from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _

from tinymce.models import HTMLField
from post_office.models import Email
from field_history.tracker import FieldHistoryTracker

from briefings.emails.notifications import MaxTrainersNotification
from webcrm.models import Delivery
from .managers import BriefingQuestionManager


def hash_location(instance, filename):
    """
    Location for saving a file for BriefingFile instances
    """
    filename = normalize('NFKD', filename).encode('ascii', 'ignore')
    filename = filename.decode('UTF-8')
    return f'uploads/briefings/{uuid.uuid4()}/{filename}'


class Briefing(models.Model):
    id = models.UUIDField(
        primary_key=True,
        default=uuid.uuid4,
        editable=False
    )
    time = models.TextField()
    reason = HTMLField()
    program = HTMLField()
    description = models.TextField()
    address = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    field_history = FieldHistoryTracker(
        ['time', 'address', 'reason', 'program'])

    class Meta:
        permissions = (
            ('view_all_briefings', 'Can see all briefings'),
        )

    def __str__(self):
        return str(self.id)

    def get_organization(self):
        if self.deliveries:
            return self.deliveries.first().delivery.organization.name
        return

    def time_updated_on(self):
        """
        Get last updated datetime for the `time` field
        """
        history = self.get_time_history().last()
        if history:
            return history.date_created
        return

    def address_updated_on(self):
        """
        Get last updated datetime for the `address` field
        """
        history = self.get_address_history().last()
        if history:
            return history.date_created
        return

    def reason_updated_on(self):
        """
        Get last updated datetime for the `reason` field
        """
        history = self.get_reason_history().last()
        if history:
            return history.date_created
        return

    def previous_reason(self):
        """
        Get previous value for `reason` field
        """
        histories = self.get_reason_history().order_by('date_created')
        if histories.count() > 1:
            return histories[histories.count() - 2].field_value
        return

    def program_updated_on(self):
        """
        Get last updated datetime for the `program` field
        """
        history = self.get_program_history().last()
        if history:
            return history.date_created
        return

    def previous_program(self):
        """
        Get previous value for `program` field
        """
        histories = self.get_program_history().order_by('date_created')
        if histories.count() > 1:
            return histories[histories.count() - 2].field_value
        return

    def files_updated_on(self):
        """
        Get last updated datetime for the related `files` field
        """
        file = self.files.last()
        if file:
            return file.get_file_name_history().last().date_created
        return


class BriefingFile(models.Model):
    briefing = models.ForeignKey(
        Briefing,
        related_name='files',
        on_delete=models.CASCADE
    )
    file_name = models.CharField(max_length=255, blank=True)
    file = models.FileField(upload_to=hash_location, blank=True)
    field_history = FieldHistoryTracker(['file_name'])

    class Meta:
        order_with_respect_to = 'briefing'

    def __str__(self):
        return self.file_name

    def get_size_mb(self):
        """
        Return the file size MBs
        """
        return round(self.file.size / 1000000, 2)

    def get_file_extension(self):
        """
        Returns the file extension
        """
        extension = self.file_name.split('.')[-1]
        return extension.lower()


class DeliverableTrainer(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=120)
    email = models.CharField(max_length=120)

    def __str__(self):
        return self.name


class Deliverable(models.Model):
    PENDING = 'P'
    SUBMITTED = 'S'
    TO_BE_DELETED = 'D'
    WAITING_FOR_APPROVAL = 'W'

    STATUS_CHOICES = (
        (PENDING, 'PENDING'),
        (SUBMITTED, 'SUBMITTED'),
        (TO_BE_DELETED, 'TO BE DELETED'),
        (WAITING_FOR_APPROVAL, 'WAITING FOR APPROVAL')
    )

    briefing = models.ForeignKey(Briefing, related_name='deliveries',
                                 on_delete=models.SET_NULL, blank=True,
                                 null=True)
    delivery = models.OneToOneField(Delivery, blank=True, null=True)
    trainers_amount = models.IntegerField(default=0)
    status = models.CharField(max_length=1, choices=STATUS_CHOICES,
                              default=PENDING)
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)

    class Meta:
        ordering = ['delivery__order_date', 'delivery__organization',
                    'delivery__product']

    def __str__(self):
        if self.delivery:
            name = self.delivery.organization.name
            return f'Deliverable - {name}'
        return f'Deliverable - {self.pk}'

    def is_delivery_person_registered(self):
        """
        Check if delivery_name_email is in the registered users list
        """
        dr_email = self.delivery.responsible.email
        return User.objects.filter(email=dr_email).exists()

    def get_pending_related(self):
        """
        Returns deliverables from the same organization and PENDING
        status
        """
        return Deliverable.objects.filter(
            delivery__organization=self.delivery.organization,
            status=self.PENDING
        ).exclude(pk=self.pk)

    def notify_trainers_amount(self):
        curr_amount = self.delivery.trainers.count()

        if curr_amount == 8 and curr_amount != self.trainers_amount:
            self.status = self.WAITING_FOR_APPROVAL
            self.trainers_amount = 8
            self.save()

            MaxTrainersNotification(self).send()


class DeliverableMembership(models.Model):
    trainer = models.ForeignKey(DeliverableTrainer, on_delete=models.CASCADE)
    deliverable = models.ForeignKey(Deliverable, on_delete=models.CASCADE)
    is_manually_added = models.BooleanField(default=False)
    position = models.IntegerField(blank=True, default=-1, null=True)


class BriefingEmailItem(Email):
    NOTIFICATION = 'NOTIFICATION'
    REMINDER = 'REMINDER'

    TYPE_CHOICES = (
        (NOTIFICATION, 'Notification'),
        (REMINDER, 'Reminder'),
    )

    type = models.CharField(
        max_length=30,
        blank=True,
        default='',
        choices=TYPE_CHOICES
    )
    briefing = models.ForeignKey(
        'Briefing',
        related_name='emails',
        null=True, on_delete=models.SET_NULL
    )

    class Meta:
        verbose_name = _('Briefing e-mail')
        verbose_name_plural = _('Briefing e-mails')


class DeliverableToken(models.Model):
    MAX_TRAINER = 'MAX TRAINERS'

    TYPE_CHOICES = (
        (MAX_TRAINER, 'MAX TRAINERS'),
    )

    key = models.UUIDField(default=uuid.uuid4, editable=False, db_index=True)
    deliverable = models.ForeignKey('Deliverable', null=True,
                                    related_name='tokens')
    type = models.CharField(max_length=30, blank=True, default='',
                            choices=TYPE_CHOICES)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = _('Token')
        verbose_name_plural = _('Tokens')


class BriefingQuestion(models.Model):
    id = models.UUIDField(
        primary_key=True,
        default=uuid.uuid4,
        editable=False
    )
    briefing = models.ForeignKey(
        Briefing,
        related_name='questions',
        on_delete=models.CASCADE
    )
    user = models.ForeignKey(
        User,
        related_name='briefing_questions',
        null=True, on_delete=models.SET_NULL
    )
    question = models.TextField()
    answer = HTMLField(blank=True, default='')
    created_at = models.DateTimeField(auto_now_add=True)

    objects = BriefingQuestionManager()

    class Meta:
        ordering = ('created_at', )

    def __str__(self):
        return self.question

    def is_answered(self):
        return bool(self.answer)


class BriefingQuestionFile(models.Model):
    briefing_question = models.ForeignKey(
        BriefingQuestion,
        related_name='files',
        on_delete=models.CASCADE
    )
    filename = models.CharField(max_length=120)
    file = models.FileField(upload_to=hash_location, blank=True)

    class Meta:
        order_with_respect_to = 'briefing_question'

    def __str__(self):
        return self.filename

    def get_size_mb(self):
        """
        Return the file size MBs
        """
        return round(self.file.size / 1000000, 2)

    def get_file_extension(self):
        """
        Returns the file extension
        """
        extension = self.filename.split('.')[-1]
        return extension.lower()


class BriefingQuestionToken(models.Model):
    id = models.UUIDField(
        'key',
        primary_key=True,
        default=uuid.uuid4,
        editable=False
    )
    deliverable = models.ForeignKey(
        Deliverable,
        related_name='question_tokens',
        on_delete=models.CASCADE
    )
    questions = models.ManyToManyField(
        BriefingQuestion,
        related_name='question_tokens',
        blank=True
    )
    is_expired = models.BooleanField('expired', default=False)
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ('is_expired', '-created_at')

    def __str__(self):
        organization = self.deliverable.delivery.organization[:20]
        return f'Token - {organization}'

    def get_creator(self):
        return self.questions.first().user

    def get_briefing(self):
        return self.questions.first().briefing
