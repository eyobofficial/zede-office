import pendulum
import six


class JOTFormObject(dict):
    integer_fields = ()
    datetime_fields = ()
    boolean_fields = ()

    def __setattr__(self, key, value):
        if key in self.__dict__:
            return super().__setattr__(key, value)
        self[key] = value

    def __getattr__(self, item):
        return self[item]

    def __delattr__(self, item):
        del self[item]

    @classmethod
    def construct_from(cls, values):
        instance = cls()
        instance.refresh_from(values)
        return instance

    def refresh_from(self, values):
        for key, value in six.iteritems(values.copy()):
            if key in self.integer_fields:
                value = int(value)

            if key in self.datetime_fields and value:
                value = pendulum.parse(value)

            if key in self.boolean_fields:
                value = bool(int(value))

            self.__setitem__(key, value)


class Form(JOTFormObject):
    integer_fields = ('height', 'new', 'count',)
    datetime_fields = ('created_at', 'updated_at', 'last_submission',)


class Question(JOTFormObject):
    integer_fields = ('qid', 'order',)


class Submission(JOTFormObject):
    integer_fields = ('order',)
    boolean_fields = ('new', 'flag',)
    datetime_fields = ('created_at', 'updated_at',)


class Answer(JOTFormObject):
    integer_fields = ('qid',)


class Webhook(JOTFormObject):
    integer_fields = ('wid',)
