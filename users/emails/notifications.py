from django.conf import settings
from django.contrib.auth.models import User
from django.contrib.auth.tokens import default_token_generator
from django.urls import reverse
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode

from users.emails.bases import BaseUserEmail


class UserInvitationEmail(BaseUserEmail):
    template_name = 'user_invitation_email.html'
    subject = 'Welkom bij Debat.NL Olaf!'

    def __init__(self, user):
        self.recipient = user

    def get_context_data(self, **kwargs):
        token = default_token_generator.make_token(self.recipient)
        uid = urlsafe_base64_encode(force_bytes(self.recipient.pk))
        hostname = settings.APP_HOSTNAME
        path = reverse('password_reset_confirm', args=[uid, token])
        kwargs['url'] = f'{hostname}{path}'
        return super().get_context_data(**kwargs)


class NoHubstaffAccountNotificationEmail(BaseUserEmail):
    template_name = 'email_1_no_hubstaff_account.html'

    def __init__(self, user_profile):
        self.subject = 'Koppel Hubstaff account'
        self.user_profile = user_profile
        self.recipient = User(first_name='Sharon', last_name='Kroes',
                              email='kroes@debat.nl')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        args = (self.user_profile.pk,)
        path = reverse('users:profile-hubstaff-disable', args=args)
        context['url'] = f'{settings.APP_HOSTNAME}{path}'
        context['user'] = self.user_profile.user
        return context
