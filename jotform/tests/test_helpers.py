import json
import os
from unittest import mock

from django.conf import settings
from django.test import SimpleTestCase, override_settings, TestCase

from jotform.client.api.submissions import Submissions
from jotform.helpers import process_submission_from_webhook, \
    persist_submission_data
from jotform.models import Submission, Answer
from jotform.tests.factories import FormFactory, EmptyFormFactory


@override_settings(JOT_FORM_API_KEY='12345')
class WebhookSubmissionProcessorTest(SimpleTestCase):

    @mock.patch('jotform.helpers.JOTFormClient')
    @mock.patch('jotform.helpers.persist_submission_data')
    def test_submission_processing(self, mock_persist, mock_client):
        base_dir = os.path.dirname(__file__)
        file_name = 'webhooks_item.json'
        test_case_location = os.path.join(base_dir, 'mock_data', file_name)
        mock_get = mock_client.return_value.submissions.get
        mock_get.return_value = []

        with open(test_case_location) as file:
            message = file.read()
            message_json = json.loads(message)
            process_submission_from_webhook(message)

            api_key = settings.JOT_FORM_API_KEY
            form_id = message_json['formID']
            submission_id = message_json['submissionID']

            mock_client.assert_called_with(api_key=api_key, form_id=form_id)
            mock_persist.assert_called_with(form_id, [])
            mock_get.assert_called_with(submission_id)

            self.assertEqual(mock_client.call_count, 1)
            self.assertEqual(mock_persist.call_count, 1)
            self.assertEqual(mock_get.call_count, 1)


class SubmissionDataPersistenceTest(TestCase):
    def test_submission_persistence(self):
        base_dir = os.path.dirname(__file__)
        file_name = 'submission.json'
        test_case_location = os.path.join(base_dir, 'mock_data', file_name)

        with open(test_case_location) as file:
            data = json.loads(file.read())
            content = data['content']
            expected_submissions = Submissions._parse_content([content])
            expected_submission = expected_submissions[0]
            expected_answers = expected_submission['answers']

        form = EmptyFormFactory(id=expected_submission.form_id)
        persist_submission_data(form.id, expected_submissions)

        submission = Submission.objects.get(id=expected_submission.id)

        self.assertEqual(Submission.objects.count(), 1)
        self.assertEqual(submission.id, expected_submission.id)
        self.assertEqual(submission.status, expected_submission.status)
        self.assertEqual(submission.new, expected_submission.new)
        self.assertEqual(submission.flag, expected_submission.flag)
        self.assertEqual(submission.created_at, expected_submission.created_at)
        self.assertEqual(submission.updated_at, expected_submission.updated_at)
        self.assertEqual(submission.form, form)

        self.assertEqual(Answer.objects.count(), 4)
        for expected_answer in expected_answers:
            answer = Answer.objects.get(qid=expected_answer['qid'])

            with self.subTest(qid=answer.qid):
                self.assertEqual(answer.qid, expected_answer['qid'])
                self.assertEqual(answer.text, expected_answer['text'])
                self.assertEqual(answer.type, expected_answer['type'])
                self.assertEqual(answer.answer, expected_answer['answer'])
                self.assertEqual(answer.answer, expected_answer['answer'])
                self.assertEqual(answer.submission, submission)

