import requests
from django.conf import settings


class EnrolmentConsumer:
    def __init__(self):
        self.url = '{}/api/e-learning/enrolment/admin'.format(settings.DEBATNL_ONLINE_API)
        self.headers = {
            'Authorization': 'Token {}'.format(settings.DEBATNL_ONLINE_API_KEY)
        }

    def create_object(self, student_id, course_id, active):
        url = '{}/'.format(self.url)

        payload = {
            'student': student_id,
            'course': course_id,
            'active': active
        }

        response = requests.post(url, headers=self.headers, data=payload)
        return response.json()

    def update_object(self, student_id, course_id, active):
        url = '{}/'.format(self.url)

        payload = {
            'student': student_id,
            'course': course_id,
            'active': active
        }

        response = requests.put(url, headers=self.headers, data=payload)
        return response.json()
