from django.apps import AppConfig


class WebcrmConfig(AppConfig):
    name = 'webcrm'
