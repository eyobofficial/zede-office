from django.conf import settings
from django.contrib.auth.models import User
from django.urls import reverse

from backoffice.utilities.emails import NotificationEmailMixin
from schedules.emails.bases import BaseScheduleEmail
from schedules.models import VacationPeriodApproval
from shared.enums import RoleName


class NewVacationPeriodEmail(NotificationEmailMixin, BaseScheduleEmail):
    template_name = 'email_1_new_vacation_request.html'
    approval = None

    def __init__(self, vacation_period):
        super().__init__(vacation_period)
        full_name = vacation_period.user.get_full_name()
        self.subject = f'Verlofaanvraag {full_name}'
        self.recipient = User(first_name='Ilona', last_name='Eichhorn',
                              email='eichhorn@debat.nl')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        path = reverse('schedules:vacation-approval', kwargs={
            'pk': self.approval.pk
        })
        context['url'] = f'{settings.APP_HOSTNAME}{path}'
        return context

    def create_pending_approval(self, email):
        self.approval, _ = VacationPeriodApproval.objects.get_or_create(
            email=email, vacation_period=self.vacation_period)
        self.approval.status = VacationPeriodApproval.PENDING
        self.approval.save()

    def send(self):
        recipients_data = [
            {
                'first_name': 'Ilona',
                'last_name': 'Eichhorn',
                'email': 'eichhorn@debat.nl'
            },
            {
                'first_name': 'Sharon',
                'last_name': 'Kroes',
                'email': 'kroes@debat.nl'
            },

        ]

        for recipient_data in recipients_data:
            self.recipient = User(**recipient_data)
            self.create_pending_approval(self.recipient.email)
            super().send()


class ApprovedVacationPeriodEmail(NotificationEmailMixin, BaseScheduleEmail):
    template_name = 'email_3_approved_vacation_request.html'
    subject = 'Verlofaanvraag goedgekeurd'

    def __init__(self, vacation_period):
        super().__init__(vacation_period)
        self.recipient = vacation_period.user


class DeniedVacationPeriodEmail(NotificationEmailMixin, BaseScheduleEmail):
    template_name = 'email_4_denied_vacation_request.html'
    subject = 'Verlofaanvraag afgewezen'

    def __init__(self, vacation_period):
        super().__init__(vacation_period)
        self.recipient = vacation_period.user


class PhoneCareVacationPeriodEmail(NotificationEmailMixin, BaseScheduleEmail):
    template_name = 'email_5_phonecare_notification.html'
    subject = 'Doorgeven afwezigheid'

    def __init__(self, vacation_period):
        super().__init__(vacation_period)
        self.recipient = User(email='info@phonecare.nl')


class AdminVacationPeriodEmail(NotificationEmailMixin, BaseScheduleEmail):
    template_name = 'email_6_administrator_notification.html'

    def __init__(self, vacation_period):
        super().__init__(vacation_period)
        full_name = vacation_period.user.get_full_name()
        self.subject = f'Doorgeven afwezigheid: {full_name}'
        self.recipient = User.objects.filter(
            profile__roles__name=RoleName.ADMIN.value,
            is_active=True
        ).first()
