import datetime

import mock
import pendulum
from django.conf import settings
from django.test import TestCase

from debatnl_registration.models import TrainingDate
from debatnl_registration.tasks import generate_travel_direction_reminder, \
    generate_extend_invitation_notification, generate_certificate_email
from debatnl_registration.tests.factories import TrainingDateFactory, \
    AttendanceFactory, ParticipantFactory
from shared.enums import RoleName
from users.models import Role
from users.tests.factories import UserFactory


@mock.patch('debatnl_registration.tasks.TravelDirectionReminderEmail')
class TaskTravelDirectionReminderTest(TestCase):
    fixtures = ['roles']

    def setUp(self):
        role = Role.objects.get(name=RoleName.ADMIN.value)
        user = UserFactory()
        user.profile.roles.add(role)

        date = datetime.date(2019, 10, 8)
        self.training_date = TrainingDateFactory(date=date)
        attendance_1 = AttendanceFactory(training_date=self.training_date)
        attendance_2 = AttendanceFactory(training_date=self.training_date)
        self.participant_1 = attendance_1.participant
        self.participant_2 = attendance_2.participant
        self.expected_args = [
            mock.call(self.participant_1),
            mock.call(self.participant_2)
        ]

    def test_on_the_3rd_day_before_training_date(self, mock_class):
        known = pendulum.datetime(2019, 10, 5, tz=settings.TIME_ZONE)
        with pendulum.test(known):
            generate_travel_direction_reminder()

        self.assertTrue(mock_class.called)
        self.assertEqual(mock_class.call_count, 2)
        self.assertEqual(mock_class.call_args_list, self.expected_args)

    def test_on_the_6th_day_before_training_date(self, mock_class):
        known = pendulum.datetime(2019, 10, 2, tz=settings.TIME_ZONE)
        with pendulum.test(known):
            generate_travel_direction_reminder()

        self.assertTrue(mock_class.called)
        self.assertEqual(mock_class.call_count, 2)
        self.assertEqual(mock_class.call_args_list, self.expected_args)

    def test_on_invalid_day_differences(self, mock_class):
        test_dates = [
            pendulum.datetime(2019, 10, 1, tz=settings.TIME_ZONE),
            pendulum.datetime(2019, 10, 3, tz=settings.TIME_ZONE),
            pendulum.datetime(2019, 10, 4, tz=settings.TIME_ZONE),
            pendulum.datetime(2019, 10, 6, tz=settings.TIME_ZONE),
            pendulum.datetime(2019, 10, 7, tz=settings.TIME_ZONE),
            pendulum.datetime(2019, 10, 8, tz=settings.TIME_ZONE),
        ]

        for test_date in test_dates:
            with self.subTest(test_date=test_date), pendulum.test(test_date):
                generate_travel_direction_reminder()

        self.assertFalse(mock_class.called)
        self.assertEqual(mock_class.call_count, 0)

    def test_with_invalid_training_date_status(self, mock_class):
        known = pendulum.datetime(2019, 10, 5, tz=settings.TIME_ZONE)

        invalid_statuses = [
            c[0] for c in TrainingDate.STATUS_CHOICES
            if c[0] is not TrainingDate.PENDING
        ]

        for status in invalid_statuses:
            self.training_date.status = status
            self.training_date.save()

            with self.subTest(status=status), pendulum.test(known):
                generate_travel_direction_reminder()

            self.assertFalse(mock_class.called)
            self.assertEqual(mock_class.call_count, 0)


@mock.patch('debatnl_registration.tasks.ExtendInvitationNotificationEmail')
class TaskExtendInvitationNotificationTest(TestCase):
    fixtures = ['roles']

    def setUp(self):
        role = Role.objects.get(name=RoleName.ADMIN.value)
        user = UserFactory()
        user.profile.roles.add(role)

        date = datetime.date(2019, 10, 8)
        self.training_date = TrainingDateFactory(date=date)
        attendance_1 = AttendanceFactory(training_date=self.training_date)
        attendance_2 = AttendanceFactory(training_date=self.training_date)
        self.participant_1 = attendance_1.participant
        self.participant_2 = attendance_2.participant
        self.expected_args = [mock.call(attendance_1), mock.call(attendance_2)]

    def test_on_the_1_day_after_training_date(self, mock_class):
        known = pendulum.datetime(2019, 10, 9, tz=settings.TIME_ZONE)
        with pendulum.test(known):
            generate_extend_invitation_notification()

        self.assertTrue(mock_class.called)
        self.assertEqual(mock_class.call_count, 2)
        self.assertEqual(mock_class.call_args_list, self.expected_args)

    def test_on_invalid_day_differences(self, mock_class):
        test_dates = [
            pendulum.datetime(2019, 10, 6, tz=settings.TIME_ZONE),
            pendulum.datetime(2019, 10, 7, tz=settings.TIME_ZONE),
            pendulum.datetime(2019, 10, 8, tz=settings.TIME_ZONE),
            pendulum.datetime(2019, 10, 10, tz=settings.TIME_ZONE),
            pendulum.datetime(2019, 10, 11, tz=settings.TIME_ZONE),
            pendulum.datetime(2019, 10, 12, tz=settings.TIME_ZONE),
        ]

        for test_date in test_dates:
            with self.subTest(test_date=test_date), pendulum.test(test_date):
                generate_travel_direction_reminder()

        self.assertFalse(mock_class.called)
        self.assertEqual(mock_class.call_count, 0)

    def test_on_invalid_duration(self, mock_class):
        self.participant_1.registration.duration = 3
        self.participant_1.registration.save()
        self.participant_2.registration.duration = 3
        self.participant_2.registration.save()

        known = pendulum.datetime(2019, 10, 9, tz=settings.TIME_ZONE)
        with pendulum.test(known):
            generate_extend_invitation_notification()

        self.assertFalse(mock_class.called)
        self.assertEqual(mock_class.call_count, 0)

    def test_with_invalid_training_date_status(self, mock_class):
        known = pendulum.datetime(2019, 10, 9, tz=settings.TIME_ZONE)

        invalid_statuses = [
            c[0] for c in TrainingDate.STATUS_CHOICES
            if c[0] is not TrainingDate.PENDING
        ]

        for status in invalid_statuses:
            with self.subTest(status=status), pendulum.test(known):
                self.training_date.status = status
                self.training_date.save()
                generate_extend_invitation_notification()

        self.assertFalse(mock_class.called)
        self.assertEqual(mock_class.call_count, 0)


@mock.patch('debatnl_registration.tasks.CertificateEmail')
class TaskCertificateEmailTest(TestCase):
    fixtures = ['roles']

    def setUp(self):
        role = Role.objects.get(name=RoleName.ADMIN.value)
        user = UserFactory()
        user.profile.roles.add(role)

        t_date_1_kwargs = {'sequence': 1, 'date': datetime.date(2019, 10, 8)}
        t_date_1 = TrainingDateFactory(**t_date_1_kwargs)
        t_date_2_kwargs = {'sequence': 2, 'date': datetime.date(2019, 10, 9)}
        t_date_2 = TrainingDateFactory(**t_date_2_kwargs)
        t_date_3_kwargs = {'sequence': 3, 'date': datetime.date(2019, 10, 10)}
        t_date_3 = TrainingDateFactory(**t_date_3_kwargs)

        participant = ParticipantFactory(registration__duration=3)
        kwargs = {'participant': participant, 'has_attended': True}
        self.attendance_1 = AttendanceFactory(training_date=t_date_1, **kwargs)
        self.attendance_2 = AttendanceFactory(training_date=t_date_2, **kwargs)
        self.attendance_3 = AttendanceFactory(training_date=t_date_3, **kwargs)
        self.expected_args = [mock.call(self.attendance_3)]

    def test_on_the_2_day_after_training_date(self, mock_class):
        known = pendulum.datetime(2019, 10, 7, tz=settings.TIME_ZONE)
        with pendulum.test(known):
            generate_certificate_email()

        self.assertTrue(mock_class.called)
        self.assertEqual(mock_class.call_count, 1)
        self.assertEqual(mock_class.call_args_list, self.expected_args)

    def test_with_1_unattended_training(self, mock_class):
        self.attendance_2.has_attended = False
        self.attendance_2.save()

        known = pendulum.datetime(2019, 10, 11, tz=settings.TIME_ZONE)
        with pendulum.test(known):
            generate_certificate_email()

        self.assertFalse(mock_class.called)
        self.assertEqual(mock_class.call_count, 0)
