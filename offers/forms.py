from django.contrib.auth.models import User
from django.forms import inlineformset_factory, ModelForm, ChoiceField, \
    RadioSelect, BaseInlineFormSet, Form, CharField
from django.utils import timezone

from offers.models import Offer, OfferInvestment, OfferReview


class OfferForm(ModelForm):
    IS_TRAINER_PROPOSED_CHOICES = (
        ('1', True),
        ('0', False)
    )

    is_trainer_proposed = ChoiceField(
        widget=RadioSelect,
        choices=IS_TRAINER_PROPOSED_CHOICES
    )

    class Meta:
        model = Offer
        exclude = ['id', 'created_at', 'updated_at', 'status', 'tokens',
                   'old_editor', 'old_trainers']

    def save(self, commit=True):
        if self.cleaned_data['is_trainer_proposed'] == '0':
            self.cleaned_data['trainers'] = User.objects.none()

        return super().save(commit)


class ReviewOfferForm(Form):
    ACCEPTED = 'ACCEPTED'
    DENIED = 'DENIED'

    STATUS_CHOICES = (
        ('ACCEPTED', 'ACCEPTED'),
        ('DENIED', 'DENIED')
    )

    status = ChoiceField(choices=STATUS_CHOICES)
    reason = CharField(required=False)

    def save(self, offer):
        status = self.cleaned_data.get('status')
        reason = self.cleaned_data.get('reason')

        if status == self.ACCEPTED:
            offer.status = Offer.FINISHED
            offer.finished_at = timezone.now()
            offer.save()

        else:
            OfferReview.objects.create(review=reason, offer=offer)


class OfferInvestmentFormset(BaseInlineFormSet):
    def save(self, commit=True):
        # Delete the investments, which are not in the POST data
        # when updating the object.
        for investment in self.instance.investments.all():
            if not any(f.instance == investment for f in self.forms):
                investment.delete()

        return super().save(commit)


InvestmentFormSet = inlineformset_factory(
    Offer,
    OfferInvestment,
    formset=OfferInvestmentFormset,
    exclude=('id',),
    extra=0
)
