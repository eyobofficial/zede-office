from django.conf import settings
from django.urls import reverse
from django.utils import timezone

from backoffice.utilities.emails import NotificationEmailMixin
from ccd.models import URLToken
from .bases import BasePersonEmail


class CCDNotificationEmail(NotificationEmailMixin, BasePersonEmail):
    template_name = 'email_1_ccd_notification.html'

    @property
    def subject(self):
        today = timezone.now()
        return f"Met iedereen contact gehad? {today.strftime('%d-%m-%Y')}"

    def get_context_data(self, **kwargs):
        kwargs['deliveries'] = self.recipient.get_future_ccd_deliveries()
        return super().get_context_data(**kwargs)


class PurchaseOrderEmail(NotificationEmailMixin, BasePersonEmail):
    template_name = 'email_2_purchase_order.html'
    subject = 'Dien je declaratie voor de 1e van de maand in'

    def get_context_data(self, **kwargs):
        token = URLToken.objects.create(
            person=self.recipient
        )
        uri = reverse('ccd:purchase-orders', args=[token.pk])
        kwargs['url'] = f'{settings.APP_HOSTNAME}{uri}'
        return super().get_context_data(**kwargs)
