from django.conf.urls import url

from marketing.views.kpi import OverviewView

urlpatterns = [
    url(r'^$', OverviewView.as_view(), name='overview'),
]
