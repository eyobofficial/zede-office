from django.views.generic import ListView
from backoffice.constants import GROUP_MARKETING
from backoffice.mixins import GroupAccessMixin
from marketing.models import OfferTimePeriod


class OverviewView(GroupAccessMixin, ListView):
    template_name = 'marketing/kpi/overview.html'
    queryset = OfferTimePeriod.objects.exclude(offers__isnull=True)
    access_groups = [GROUP_MARKETING]
