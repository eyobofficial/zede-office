import factory
from django.conf import settings
from pendulum import timezone

from scans.models import Scan
from users.tests.factories import UserFactory


class ScanFactory(factory.django.DjangoModelFactory):
    title = factory.Faker('sentence', nb_words=4)
    organization = factory.Faker('company')
    dr_name = factory.Faker('name')
    dr_email = factory.Faker('email')
    issues = factory.Faker('text')
    deadline_at = factory.Faker('date_time_this_decade', after_now=True,
                                tzinfo=timezone(settings.TIME_ZONE))
    status = Scan.PENDING
    trainer = factory.SubFactory(UserFactory)

    class Meta:
        model = Scan
