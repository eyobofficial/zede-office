from django.apps import AppConfig


class IntakesConfig(AppConfig):
    name = 'intakes'

    def ready(self):
        import intakes.signals  # noqa
