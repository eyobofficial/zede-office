from django.contrib.auth.decorators import login_required, user_passes_test
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse, reverse_lazy

from debatnl_online.consumers.chapters import ChapterConsumer
from debatnl_online.consumers.lessons import LessonTestQuestionConsumer
from debatnl_online.permissions import group_check


@login_required
@user_passes_test(group_check, login_url=reverse_lazy('main_page'))
def view_chapter(request, course_id, chapter_id):
    consumer = ChapterConsumer()
    chapter = consumer.retrieve_object(chapter_id)

    questions_consumer = LessonTestQuestionConsumer()
    for lesson in chapter.get('lessons'):
        lesson['questions'] = questions_consumer.retrieve_all(lesson.get('id'))

    context = {
        'chapter': chapter,
        'course_id': course_id
    }
    return render(request, 'debatnl_online/chapters_details.html',
                  context=context)


@login_required
@user_passes_test(group_check, login_url=reverse_lazy('main_page'))
def view_add_chapter(request, course_id):
    if request.method == 'GET':
        context = {
            'course_id': course_id
        }
        return render(request,
                      'debatnl_online/modals/chapters/modal-create.html',
                      context=context)

    if request.method == 'POST':
        consumer = ChapterConsumer()
        consumer.create_object(request.POST)

    return HttpResponseRedirect(
        reverse('debatnl_online:e_learning:course') + course_id + '/')


@login_required
@user_passes_test(group_check, login_url=reverse_lazy('main_page'))
def view_change_chapter(request, course_id, chapter_id):
    if request.method == 'GET':
        consumer = ChapterConsumer()
        chapter = consumer.retrieve_object(chapter_id)

        context = {
            'course_id': course_id,
            'chapter': chapter
        }
        return render(request,
                      'debatnl_online/modals/chapters/modal-edit.html',
                      context=context)

    if request.method == 'POST':
        consumer = ChapterConsumer()
        consumer.update_object(chapter_id, request.POST)

    return HttpResponseRedirect(
        reverse('debatnl_online:e_learning:course') + course_id + '/')


@login_required
@user_passes_test(group_check, login_url=reverse_lazy('main_page'))
def view_delete_chapter(request, course_id, chapter_id):
    if request.method == 'GET':
        consumer = ChapterConsumer()
        chapter = consumer.retrieve_object(chapter_id)

        context = {
            'course_id': course_id,
            'chapter': chapter
        }
        return render(request,
                      'debatnl_online/modals/chapters/modal-delete.html',
                      context=context)

    if request.method == 'POST':
        consumer = ChapterConsumer()
        payload = {
            "is_deleted": True
        }
        consumer.update_object(chapter_id, payload)

    return HttpResponseRedirect(
        reverse('debatnl_online:e_learning:course') + course_id + '/')
