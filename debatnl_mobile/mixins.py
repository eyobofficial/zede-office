from django.urls import reverse_lazy
from backoffice.constants import GROUP_DEBATNL_MOBILE
from backoffice.mixins import GroupAccessMixin

from .consumers.organizations import OrganizationConsumer
from .consumers.trainings import TrainingConsumer
from .consumers.challenges import ChallengeConsumer, ParticipantConsumer, \
    ChallengeSubmissionConsumer


class BaseView(GroupAccessMixin):
    access_groups = [GROUP_DEBATNL_MOBILE]
    consumer = None
    connection = None

    def dispatch(self, request, *args, **kwargs):
        self.connection = self.consumer().ping()
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['api_connection'] = self.connection
        context['object'] = self.get_object()
        return context

    def get_object(self):
        object_id = self.kwargs.get('pk')
        if object_id:
            return self.consumer().retrieve_object(object_id)
        return


class BaseOrganizationView(BaseView):
    consumer = OrganizationConsumer


class BaseTrainingView(BaseView):
    consumer = TrainingConsumer

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['organization_id'] = self.kwargs.get('id')
        return context

    def get_success_url(self):
        organization_id = self.kwargs.get('id')
        return reverse_lazy(
            'debatnl_mobile:training-overview',
            args=[organization_id]
        )


class BaseChallengeView(BaseView):
    consumer = ChallengeConsumer
    training_consumer = TrainingConsumer

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['training_id'] = self.kwargs.get('id')
        return context

    def get_success_url(self):
        training_id = self.kwargs.get('id')
        return reverse_lazy(
            'debatnl_mobile:challenge-overview',
            args=[training_id]
        )


class BaseParticipantView(BaseChallengeView):
    consumer = ParticipantConsumer

    def get_success_url(self):
        url = super().get_success_url()
        return f'{url}#participant-overview'


class BaseChallengeSubmissionView(BaseView):
    consumer = ChallengeSubmissionConsumer
    challenge_consumer = ChallengeConsumer

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        submission = self.get_object()
        context['training_id'] = self.kwargs.get('id')
        context['challenge'] = self.challenge_consumer().retrieve_object(
            submission['challenge']
        )
        return context

    def get_success_url(self):
        training_id = self.kwargs.get('id')
        url = reverse_lazy(
            'debatnl_mobile:challenge-overview',
            args=[training_id]
        )
        return f'{url}#participant-overview'
