from backoffice.utilities.emails import BaseEmail


class BaseIntakeEmail(BaseEmail):
    template_location = '../templates/intakes/emails'
    intake = None

    def __init__(self, intake):
        self.intake = intake

    def get_context_data(self, **kwargs):
        if 'intake' not in kwargs:
            kwargs['intake'] = self.intake
        return super().get_context_data(**kwargs)
