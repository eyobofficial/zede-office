import tempfile
import uuid
from uuid import uuid4
from unicodedata import normalize

import pendulum
from PyPDF2 import PdfFileReader, PdfFileWriter
from django.core.exceptions import ValidationError
from django.db import models

from shared.models import Training, Infographic

from handouts.emails.notifications import NoDeliveryNotification, \
    TrainerNotAssignedNotification, PageSelectionNotification
from webcrm.models import Delivery


def hash_location(instance, filename):
    filename = normalize('NFKD', filename).encode('ascii', 'ignore')
    filename = filename.decode('UTF-8')
    return f'uploads/handouts/{uuid4()}/{filename}'


class HandoutRequest(models.Model):
    NEW = 'NEW'
    DELIVERY_NOT_FOUND = 'DNF'  # Delivery by training date not found
    TRAINER_NOT_FOUND = 'TNF'   # Trainer not found in delivery
    MISMATCHING_DELIVERIES = 'MMD'   # Deliveries with different products
    UNKNOWN_DELIVERIES = 'UKD'   # Deliveries with unknown products
    HANDOUT_SELECTION = 'HOS'
    SENT = 'SNT'

    STATUS_CHOICES = (
        (NEW, 'NEW'),
        (DELIVERY_NOT_FOUND, 'DELIVERY BY TRAINING DATE NOT FOUND'),
        (TRAINER_NOT_FOUND, 'TRAINER NOT FOUND IN DELIVERY'),
        (MISMATCHING_DELIVERIES, 'MISMATCHING DELIVERIES FOUND'),
        (UNKNOWN_DELIVERIES, 'UNKNOWN DELIVERIES FOUND'),
        (HANDOUT_SELECTION, 'HANDOUT SELECTION'),
        (SENT, 'SENT')
    )

    id = models.UUIDField(primary_key=True, editable=False, default=uuid4)
    submission_id = models.CharField(max_length=150)
    trainee_name = models.CharField(max_length=120)
    trainee_email = models.EmailField(max_length=120)
    training_date = models.DateField()
    trainer = models.CharField(max_length=120)
    status = models.CharField(
        max_length=3,
        choices=STATUS_CHOICES,
        default=NEW
    )
    submitted_at = models.DateTimeField()
    handout = models.OneToOneField(
        'Handout',
        blank=True, null=True,
        related_name='handout_request'
    )
    deliveries = models.ManyToManyField(Delivery, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        default_related_name = 'handout_requests'

    def __str__(self):
        return f'{self.trainee_name} - {self.training_date}'

    def _check_for_trainer(self):
        deliveries = self.deliveries.all()
        return any([self.trainer in d.get_trainer_names() for d in deliveries])

    def _check_for_trainer_in_one_delivery(self):
        deliveries = self.deliveries.all()
        items = [self.trainer in d.get_trainer_names() for d in deliveries]
        return items.count(True) == 1

    def _check_for_matching_deliveries(self):
        deliveries = self.deliveries.all()
        trainer = self.trainer
        pks = [d.pk for d in deliveries if trainer in d.get_trainer_names()]
        products = self.deliveries.filter(pk__in=pks)
        products = products.values_list('product', flat=True).distinct()
        return products.count() == 1 and products.first()

    def _check_for_known_deliveries(self):
        deliveries = self.deliveries.all()
        trainer = self.trainer
        pks = [d.pk for d in deliveries if trainer in d.get_trainer_names()]
        products = self.deliveries.filter(pk__in=pks)
        products = products.values_list('product', flat=True).distinct()
        trainings = Training.objects.all()

        known_deliveries = []
        for product in products:
            names = [t.name.lower() in product.lower() for t in trainings]
            is_found = any(names)
            known_deliveries.append(is_found)

        return all(known_deliveries)

    def set_corresponding_deliveries(self):
        dt = self.training_date
        dt = pendulum.date(dt.year, dt.month, dt.day)
        deliveries = Delivery.objects.filter(order_date__date=dt)
        deliveries = deliveries.exclude(product='Digitale Intake')
        deliveries = deliveries.exclude(product='Debat.NL Online')
        deliveries = deliveries.exclude(product='SkillsTracker')
        self.deliveries.set(deliveries)
        return deliveries.all()

    def determine_status(self):
        """
        Determine the status of the handout request instance when its status
        is NEW.
        """
        self.status = self.HANDOUT_SELECTION

        if self.deliveries.count() == 0:
            self.status = self.DELIVERY_NOT_FOUND

        elif not self._check_for_trainer():
            self.status = self.TRAINER_NOT_FOUND

        elif (not self._check_for_trainer_in_one_delivery() and
                not self._check_for_matching_deliveries()):
            self.status = self.MISMATCHING_DELIVERIES

        elif not self._check_for_known_deliveries():
            self.status = self.UNKNOWN_DELIVERIES

        self.save()
        return self.status

    def send_notification_based_on_status(self):
        """
        Send notification based on the current status of the handout request.
        """
        try:
            training = self.get_training()
            email_dispatcher = {
                self.HANDOUT_SELECTION: PageSelectionNotification(
                    self, training=training),
                self.DELIVERY_NOT_FOUND: NoDeliveryNotification(self),
                self.TRAINER_NOT_FOUND: TrainerNotAssignedNotification(self)
            }

            email = email_dispatcher.get(self.status)

            if email:
                email.send()
        except ValidationError:
            pass

    def get_training(self):
        """
        Return the corresponding training/product from `webcrm` Delivery
        """
        for delivery in self.deliveries.all():
            for training in Training.objects.all():
                if (delivery.product and
                        training.name.lower() in delivery.product.lower()):
                    return training.name
        return


class Handout(models.Model):
    id = models.UUIDField(primary_key=True, editable=False, default=uuid4)
    file = models.FileField(upload_to=hash_location, max_length=255)
    pages = models.ManyToManyField(Infographic)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        default_related_name = 'handouts'

    def __str__(self):
        return str(self.id)

    @staticmethod
    def combine_pdfs(pages):
        """
        Accepts a queryset of `Infographic` instances.
        Combine the instance files and return a single file
        """
        writer = PdfFileWriter()
        output = tempfile.TemporaryFile()

        for page in pages:
            reader = PdfFileReader(page.file.path)
            for file in range(reader.getNumPages()):
                writer.addPage(reader.getPage(file))

        writer.write(output)
        return output

    def generate_pdf(self):
        """
        Generate a PDF file from related pages and save to `file` field
        """
        if not self.pages.exists():
            return

        pages = self.pages.all()
        file = self.combine_pdfs(pages)
        self.file.save('handout.pdf', file, save=True)


class HandoutToken(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    delivery = models.ForeignKey(Delivery, null=True, blank=True,
                                 on_delete=models.SET_NULL)
    handout_request = models.ForeignKey(HandoutRequest, null=True, blank=True,
                                        on_delete=models.CASCADE)
