import mock
import pendulum

from django.conf import settings
from django.test import TestCase

from backoffice.tests.factories import AdministratorFactory

from webcrm.factories import PersonFactory as WebCRMPersonFactory, \
    DeliveryFactory

from ccd.tests.factories import PersonFactory
from ccd.tasks import send_ccd_email_notification_every_friday, \
    send_purchase_order_emails


class CCDEmailNotificationTests(TestCase):
    """
    Tests for the `send_ccd_email_notifications_every_friday` task
    """
    fixures = ['groups']

    def setUp(self):
        self.person = PersonFactory(email='johndoe@test.email')
        AdministratorFactory()

    @mock.patch('ccd.tasks.CCDNotificationEmail.send')
    def test_task_without_related_deliveries(self, mock_obj):
        """
        Ensure task does not send email if there are no related deliveries.
        """
        send_ccd_email_notification_every_friday()
        self.assertEqual(mock_obj.call_count, 0)

    @mock.patch('ccd.tasks.CCDNotificationEmail.send')
    def test_task_with_past_deliveries(self, mock_obj):
        """
        Ensure task does not send email with past related deliveries.
        """
        today = pendulum.now(settings.TIME_ZONE)
        past_date = today.subtract(days=1)
        webcrm_person = WebCRMPersonFactory(email='johndoe@test.email')
        deliveries = DeliveryFactory.create_batch(10, order_date=past_date)
        for delivery in deliveries:
            delivery.trainers.add(webcrm_person)
        send_ccd_email_notification_every_friday()
        self.assertEqual(mock_obj.call_count, 0)

    @mock.patch('ccd.tasks.CCDNotificationEmail.send')
    def test_task_with_future_deliveries(self, mock_obj):
        """
        Ensure task sends notification email when there are related
        future deliveries.
        """
        today = pendulum.now(settings.TIME_ZONE)
        future_date = today.add(days=30)
        webcrm_person = WebCRMPersonFactory(email='johndoe@test.email')
        deliveries = DeliveryFactory.create_batch(10, order_date=future_date)
        for delivery in deliveries:
            delivery.trainers.add(webcrm_person)
            delivery.save()

        send_ccd_email_notification_every_friday()
        self.assertEqual(mock_obj.call_count, 1)


class SendPurchaseOrderEmailsTests(TestCase):
    """
    Tests for `send_purchase_order_emails` task
    """

    def setUp(self):
        self.person = PersonFactory(email='johndoe@test.email')
        self.webcrm_person = WebCRMPersonFactory(email='johndoe@test.email')
        AdministratorFactory()

    @mock.patch('ccd.tasks.PurchaseOrderEmail.send')
    def test_task_with_no_related_monthly_deliveries(self, mock_obj):
        """
        Ensure task does not send purchase order email if there are
        no related monthly deliveries.
        """
        today = pendulum.now(settings.TIME_ZONE)
        two_months_ago = today.subtract(months=2)
        delivery = DeliveryFactory(order_date=two_months_ago)
        delivery.trainers.add(self.webcrm_person)
        send_purchase_order_emails()
        self.assertEqual(mock_obj.call_count, 0)

    @mock.patch('ccd.tasks.PurchaseOrderEmail.send')
    def test_task_with_related_monthly_deliveries(self, mock_obj):
        """
        Ensure task sends purchase order email if there are related
        monthly deliveries.
        """
        known = pendulum.datetime(2019, 5, 27, tz=settings.TIME_ZONE)
        with pendulum.test(known):
            order_date_1 = pendulum.datetime(2019, 5, 1, tz=settings.TIME_ZONE)
            order_date_2 = pendulum.datetime(2019, 5, 2, tz=settings.TIME_ZONE)
            delivery_1 = DeliveryFactory(order_date=order_date_1)
            delivery_2 = DeliveryFactory(order_date=order_date_2)
            delivery_1.trainers.add(self.webcrm_person)
            delivery_2.trainers.add(self.webcrm_person)
            send_purchase_order_emails()
            self.assertEqual(mock_obj.call_count, 1)
