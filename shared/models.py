import uuid
from unicodedata import normalize

from django.db import models


def hash_location(instance, filename):
    filename = normalize('NFKD', filename).encode('ascii', 'ignore')
    filename = filename.decode('UTF-8')
    return f'uploads/shared/{uuid.uuid4()}/{filename}'


class Training(models.Model):
    id = models.UUIDField(primary_key=True, editable=False, default=uuid.uuid4)
    name = models.CharField(max_length=120)

    class Meta:
        ordering = ('name', )

    def __str__(self):
        return self.name


class Infographic(models.Model):
    id = models.UUIDField(primary_key=True, editable=False, default=uuid.uuid4)
    label = models.CharField(max_length=120)
    file = models.FileField(upload_to=hash_location)
    preview = models.ImageField(upload_to=hash_location)
    training = models.ManyToManyField(Training)

    class Meta:
        default_related_name = 'infographics'
        ordering = ('label',)

    def __str__(self):
        return self.label
