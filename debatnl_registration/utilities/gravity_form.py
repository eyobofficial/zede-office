import base64
import hmac
import json
import time
from collections import OrderedDict
from hashlib import sha1

import dateparser
import requests
import urllib3
from babel.dates import format_date
from django.conf import settings

from debatnl_registration.models import TrainingDate


class GravityFormClient:
    def __init__(self, form_id):
        self._url_base = settings.GRAVITY_FORM_ENDPOINT
        self._api_key = settings.GRAVITY_FORM_PUBLIC_KEY
        self._private_key = settings.GRAVITY_FORM_PRIVATE_KEY
        self._form_id = form_id

    def _build_gf_url(self, method, route, valid_for=300, query_args=None):
        expires = int(time.time()) + valid_for
        string_to_sign = '{0}:{1}:{2}:{3}'.format(
            self._api_key,
            method,
            route,
            expires
        )

        sig = hmac.new(
            self._private_key.encode('utf-8'),
            string_to_sign.encode('utf-8'),
            sha1
        )
        sig_b64 = base64.b64encode(sig.digest())

        required_args = {
            'api_key': self._api_key,
            'signature': sig_b64,
            'expires': expires,
        }

        # Create URL here rather than passing a dict to requests avoids
        # double-encoding percent signs
        if query_args:
            query = urllib3.request.urlencode(
                dict(query_args.items() + required_args.items())
            )
        else:
            query = urllib3.request.urlencode(required_args)
        return '{}{}?{}'.format(self._url_base, route, query)

    def _gf_request(self, route, method, data=None, query_args=None):
        url = self._build_gf_url(method, route, query_args=query_args)
        if data:
            json_data = json.dumps(data)
        else:
            json_data = None
        r = requests.request(method, url, data=json_data)
        return r.json()[u'response']

    @staticmethod
    def parse_date(input_string):
        input_string = input_string.replace(',', '')
        input_string = input_string.replace('en ', '')
        input_string = input_string.replace('& ', '')
        chars = input_string.split(' ')

        string_date = ''
        parsed_dates = []
        for index, char in enumerate(chars):
            if index % 3 == 0:
                parsed_date = dateparser.parse(string_date)

                if parsed_date:
                    parsed_dates.append(parsed_date.date())
                    string_date = ''

            string_date = '{} {}'.format(string_date, char)

        return sorted(parsed_dates)

    @staticmethod
    def serialize_dates(dates):
        values = []
        for key, value in dates.items():

            serialized_date = ''
            date_count = len(value)
            for index, date in enumerate(value):
                f_date = format_date(date, format='d MMMM Y', locale='nl_NL')
                f_date = f_date.lstrip("0").replace(" 0", " ")

                if date_count == 1:
                    serialized_date = '{} (Utrecht)'.format(f_date)
                else:

                    if index == 0:
                        serialized_date += '{}'.format(f_date)
                    elif index == (date_count - 1):
                        serialized_date += ' en {} (Utrecht)'.format(f_date)
                    else:
                        serialized_date += ', {}'.format(f_date)

            values.append({
                'text': serialized_date,
                'value': serialized_date,
                'isSelected': False,
                'price': ''
            })

        return values

    def _order_dates(self, values):
        dates = {}
        for value in values:
            parsed_dates = self.parse_date(value['value'])
            dates[parsed_dates[0]] = parsed_dates

        return OrderedDict(sorted(dates.items()))

    def add_training_dates(self, planned_training):
        if not settings.ALLOW_GRAVITY_FORM_REQUESTS:
            return

        url = 'forms/{}'.format(self._form_id)
        response = self._gf_request(url, 'GET')

        t_dates_qs = planned_training.training_dates
        t_dates_qs = t_dates_qs.exclude(status=TrainingDate.CANCELLED)
        training_dates = t_dates_qs.order_by('date').all()

        field_ids = {
            'Bewust Beinvloeden': [4, 28],
            'Ontspannen Presenteren': [19, 27],
            'Overtuigend Debatteren': [22, 29],
            'Professioneel Gespreksleiden': [25, 30],
        }

        field_id = field_ids[planned_training.training.name]

        for field in response['fields']:
            if field['id'] == field_id[0]:
                dates = [training_dates[0].date]
                data = OrderedDict([(dates[0], [dates])])
                field['choices'] += self.serialize_dates(data)
                ordered_dates = self._order_dates(field['choices'])
                field['choices'] = self.serialize_dates(ordered_dates)

            if field['id'] == field_id[1] and training_dates.count() == 3:
                dates = [d.date for d in training_dates]
                data = OrderedDict([(training_dates[0].date, dates)])
                field['choices'] += self.serialize_dates(data)
                ordered_dates = self._order_dates(field['choices'])
                field['choices'] = self.serialize_dates(ordered_dates)

        self._gf_request(url, 'PUT', response)

    def add_2_day_additional_training_dates(self, planned_training):
        if not settings.ALLOW_GRAVITY_FORM_REQUESTS:
            return

        url = 'forms/{}'.format(self._form_id)
        response = self._gf_request(url, 'GET')

        t_dates_qs = planned_training.training_dates
        t_dates_qs = t_dates_qs.exclude(status=TrainingDate.CANCELLED)
        t_dates_qs = t_dates_qs.exclude(sequence=0)
        training_dates = t_dates_qs.order_by('date').all()

        field_ids = {
            'Bewust Beinvloeden': 28,
            'Ontspannen Presenteren': 27,
            'Overtuigend Debatteren': 29,
            'Professioneel Gespreksleiden': 30
        }

        field_id = field_ids[planned_training.training.name]

        for field in response['fields']:
            if field['id'] == field_id and training_dates.count() == 3:
                dates = [d.date for d in training_dates]
                data = OrderedDict([(training_dates[0].date, dates)])
                field['choices'] += self.serialize_dates(data)
                ordered_dates = self._order_dates(field['choices'])
                field['choices'] = self.serialize_dates(ordered_dates)

        self._gf_request(url, 'PUT', response)

    def remove_1_day_training_date(self, planned_training):
        if not settings.ALLOW_GRAVITY_FORM_REQUESTS:
            return

        url = 'forms/{}'.format(self._form_id)
        response = self._gf_request(url, 'GET')

        training_dates = planned_training.training_dates.order_by(
            'sequence'
        ).filter(sequence=0).all()

        field_ids = {
            'Bewust Beinvloeden': 4,
            'Ontspannen Presenteren': 19,
            'Overtuigend Debatteren': 22,
            'Professioneel Gespreksleiden': 25,
        }

        dates = [d.date for d in training_dates]

        data = OrderedDict([
            (training_dates[0].date, dates)
        ])

        serialized_data = self.serialize_dates(data)

        for field in response['fields']:
            if field['id'] == field_ids[planned_training.training.name]:
                date_string = serialized_data[0]
                if date_string in field['choices']:
                    field['choices'].remove(date_string)

        self._gf_request(url, 'PUT', response)

    def remove_2_day_training_dates(self, planned_training):
        if not settings.ALLOW_GRAVITY_FORM_REQUESTS:
            return

        url = 'forms/{}'.format(self._form_id)
        response = self._gf_request(url, 'GET')

        training_dates = planned_training.training_dates.order_by(
            'sequence'
        ).all()

        field_ids = {
            'Bewust Beinvloeden': 28,
            'Ontspannen Presenteren': 27,
            'Overtuigend Debatteren': 29,
            'Professioneel Gespreksleiden': 30
        }

        dates = [d.date for d in training_dates]

        data = OrderedDict([
            (training_dates[0].date, dates)
        ])

        serialized_data = self.serialize_dates(data)

        field_id = field_ids[planned_training.training.name]

        for field in response['fields']:
            if field['id'] == field_id and training_dates.count() == 3:
                date_string = serialized_data[0]
                if date_string in field['choices']:
                    field['choices'].remove(date_string)

        self._gf_request(url, 'PUT', response)

    def remove_2_day_additional_training_dates(self, planned_training):
        if not settings.ALLOW_GRAVITY_FORM_REQUESTS:
            return

        url = 'forms/{}'.format(self._form_id)
        response = self._gf_request(url, 'GET')

        training_dates = planned_training.training_dates.order_by(
            'sequence'
        ).exclude(sequence=0).all()

        field_ids = {
            'Bewust Beinvloeden': 28,
            'Ontspannen Presenteren': 27,
            'Overtuigend Debatteren': 29,
            'Professioneel Gespreksleiden': 30
        }

        dates = [d.date for d in training_dates]

        data = OrderedDict([
            (training_dates[0].date, dates)
        ])

        serialized_data = self.serialize_dates(data)
        field_id = field_ids[planned_training.training.name]

        for field in response['fields']:
            if field['id'] == field_id and training_dates.count() == 3:
                date_string = serialized_data[0]
                if date_string in field['choices']:
                    field['choices'].remove(date_string)

        self._gf_request(url, 'PUT', response)
