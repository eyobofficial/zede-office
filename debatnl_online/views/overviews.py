from django.views.generic import TemplateView

from backoffice.constants import GROUP_DEBATNLONLINE
from backoffice.mixins import GroupAccessMixin
from debatnl_online.consumers.e_learning.courses import CourseConsumer
from debatnl_online.consumers.e_learning.participants import \
    ParticipantConsumer
from debatnl_online.consumers.users import UserConsumer


class ELearningOverviewView(GroupAccessMixin, TemplateView):
    template_name = 'debatnl_online/e_learning_overview.html'
    access_groups = [GROUP_DEBATNLONLINE]

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context.update({
            'courses': CourseConsumer().retrieve_all(),
            'participants': ParticipantConsumer().retrieve_all()
        })

        return context


class UserOverviewView(GroupAccessMixin, TemplateView):
    template_name = 'debatnl_online/user_overview.html'
    access_groups = [GROUP_DEBATNLONLINE]

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['users'] = UserConsumer().retrieve_all()
        return context
