from django.contrib.auth.decorators import login_required, user_passes_test
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse, reverse_lazy

from debatnl_online.consumers.courses import CourseTestAnswerConsumer, \
    CourseTestQuestionConsumer
from debatnl_online.permissions import group_check


@login_required
@user_passes_test(group_check, login_url=reverse_lazy('main_page'))
def view_add_course_test_question(request, course_id):
    if request.method == 'GET':
        context = {
            'course_id': course_id
        }
        return render(
            request,
            'debatnl_online/modals/course-questions/modal-create.html',
            context=context)

    if request.method == 'POST':
        question_consumer = CourseTestQuestionConsumer()
        question = question_consumer.create_object(course_id,
                                                   data=request.POST)

        answer_consumer = CourseTestAnswerConsumer()

        questions = []
        for n in range(1, 5):
            answer = request.POST.get('answer_{}'.format(n), '')

            if answer.strip():
                questions.append({
                    'question': question.get('id'),
                    'answer': request.POST.get('answer_{}'.format(n)),
                    'is_correct_answer': request.POST.get(
                        'answer_{}_is_correct'.format(n), False),
                    'sequence': n
                })

        answer_consumer.create_object(course_id, question.get('id'), questions)

    return HttpResponseRedirect(
        reverse('debatnl_online:e_learning:course') + course_id + '/')


@login_required
@user_passes_test(group_check, login_url=reverse_lazy('main_page'))
def view_change_course_test_question(request, course_id, question_id):
    if request.method == 'GET':
        consumer = CourseTestQuestionConsumer()
        question = consumer.retrieve_object(course_id, question_id)

        answers = question.get('answers')
        answers_count = len(answers)

        context = {
            'course_id': course_id,
            'question': question,
            'empty_answers': range(answers_count + 1, 5)
        }

        return render(request,
                      'debatnl_online/modals/course-questions/modal-edit.html',
                      context=context)

    if request.method == 'POST':
        consumer = CourseTestQuestionConsumer()
        consumer.update_object(course_id, question_id, request.POST)

        answer_consumer = CourseTestAnswerConsumer()

        questions = []
        for n in range(1, 5):
            answer = request.POST.get('answer_{}'.format(n), '')

            if answer.strip():
                questions.append({
                    'question': question_id,
                    'answer': answer,
                    'is_correct_answer': request.POST.get(
                        'answer_{}_is_correct'.format(n), False),
                    'sequence': n
                })

        answer_consumer.update_object(course_id, question_id, questions)

    return HttpResponseRedirect(
        reverse('debatnl_online:e_learning:course') + course_id + '/')


@login_required
@user_passes_test(group_check, login_url=reverse_lazy('main_page'))
def view_delete_course_test_question(request, course_id, question_id):
    if request.method == 'GET':
        consumer = CourseTestQuestionConsumer()
        question = consumer.retrieve_object(course_id, question_id)

        context = {
            'course_id': course_id,
            'question': question
        }
        return render(
            request,
            'debatnl_online/modals/course-questions/modal-delete.html',
            context=context)

    if request.method == 'POST':
        consumer = CourseTestQuestionConsumer()
        payload = {
            "is_deleted": True
        }
        consumer.update_object(course_id, question_id, payload)

    return HttpResponseRedirect(
        reverse('debatnl_online:e_learning:course') + course_id + '/')
