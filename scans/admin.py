from django.contrib import admin

from backoffice.admin import CustomURLModelAdmin

from scans.models import Scan, ScanReview
from scans.views import NewScanEmailView, UploadConfirmationEmailView, \
    UploadDRNotificationEmailView, ScanDeniedEmailView, \
    ScanApprovedEmailView,  ScanSubmissionReminderEmailView, \
    ScanReviewReminderEmailView


@admin.register(Scan)
class ScanAdmin(CustomURLModelAdmin):
    list_display = ('title', 'organization', 'dr_name', 'dr_email', 'type',
                    'status', 'created_at',)
    list_filter = ('type', 'status')
    filter_horizontal = ('deliverables',)
    custom_urls = [
        {
            'regex': r'^(?P<pk>.+)/send_new_scan_email/$',
            'view': NewScanEmailView,
            'name': 'send_new_scan_email'
        },
        {
            'regex': r'^(?P<pk>.+)/send_scan_denied_email/$',
            'view': ScanDeniedEmailView,
            'name': 'send_scan_denied_email'
        },
        {
            'regex': r'^(?P<pk>.+)/send_scan_approved_email/$',
            'view': ScanApprovedEmailView,
            'name': 'send_scan_approved_email'
        },
        {
            'regex': r'^(?P<pk>.+)/send_upload_email/$',
            'view': UploadConfirmationEmailView,
            'name': 'send_upload_email'
        },
        {
            'regex': r'^(?P<pk>.+)/send_dr_upload_email/$',
            'view': UploadDRNotificationEmailView,
            'name': 'send_dr_upload_email'
        },
        {
            'regex': r'^(?P<pk>.+)/send_submission_reminder_email/$',
            'view': ScanSubmissionReminderEmailView,
            'name': 'send_submission_reminder_email'
        },
        {
            'regex': r'^(?P<pk>.+)/send_scan_review_reminder_email/$',
            'view': ScanReviewReminderEmailView,
            'name': 'send_scan_review_reminder_email'
        }
    ]


@admin.register(ScanReview)
class ScanReviewAdmin(admin.ModelAdmin):
    list_display = ('scan', 'created_at')
