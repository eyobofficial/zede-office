from datetime import timedelta

from django.test import TestCase
from django.utils import timezone

from schedules.models import VacationPeriod
from users.tests.factories import UserFactory


class VacationPeriodTest(TestCase):
    """
    Tests for `VacationPeriod` model
    """

    def setUp(self):
        self.today = timezone.now().date()
        self.user = UserFactory()

    def test_on_vacation_method_with_the_right_dates(self):
        """
        Test the `on_vacation()` method with the right dates
        """
        vacation = VacationPeriod.objects.create(
            start_date=self.today - timedelta(days=10),
            end_date=self.today + timedelta(days=10),
            amount_hours=1,
            reason='Test reason',
            user=self.user
        )

        self.assertTrue(vacation.on_vacation(self.today))

    def test_on_vacation_method_with_the_wrong_dates(self):
        """
        Test the `on_vacation()` method with the wrong dates
        """
        vacation = VacationPeriod.objects.create(
            start_date=self.today + timedelta(days=10),
            end_date=self.today + timedelta(days=20),
            amount_hours=1,
            reason='Test reason',
            user=self.user
        )

        self.assertFalse(vacation.on_vacation(self.today))
