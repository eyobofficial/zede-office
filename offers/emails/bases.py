from django.conf import settings
from django.contrib.auth.models import User
from django.urls import reverse

from backoffice.utilities.emails import BaseEmail
from offers.models import OfferEmailItem, OfferToken


class BaseOfferEmail(BaseEmail):
    template_location = '../templates/offers/emails'
    offer = None
    token = None
    email_type = ''

    def get_context_data(self, **kwargs):
        if 'offer' not in kwargs:
            kwargs['offer'] = self.offer
        return super().get_context_data(**kwargs)

    def send(self):
        email = super().send()

        # Check if offer is persisted to the database.
        # RemovedOfferNotification in send_notification_email() of the
        # OfferViewMixin uses a non-persisted object.
        if self.offer.created_at:
            offer_email = OfferEmailItem(
                email_ptr=email, offer=self.offer, type=self.email_type)
            offer_email.save_base(raw=True)
            email = offer_email

        return email

    def obtain_offer_token(self, token_type):
        kwargs = {
            'offer': self.offer,
            'type': token_type
        }

        # Delete all tokens of the same type, despite the e-mail address.
        OfferToken.objects.filter(**kwargs).delete()

        # Add e-mail when creating a new token.
        kwargs['email'] = self.recipient.email
        return OfferToken.objects.create(**kwargs)


class OfferEmail(BaseOfferEmail):
    def __init__(self, offer):
        self.recipient = offer.editor
        self.offer = offer

    def get_context_data(self):
        context = {
            'offer': self.offer,
            'deadline': self.token.deadline_at,
            'url': '{}{}?token={}&email={}'.format(
                settings.APP_HOSTNAME,
                reverse('offers:upload'),
                str(self.token.key),
                self.recipient.email
            )
        }
        return super().get_context_data(**context)

    def get_attachments(self):
        attachments = {}
        if self.offer.photo:
            filename = '{}{}'.format(
                self.offer.photo.name,
                self.offer.photo.extension()
            )

            attachments = {filename: self.offer.photo.image}

        return super().get_attachments(**attachments)


class ReviewOfferEmail(BaseOfferEmail):
    def __init__(self, offer):
        self.recipient = User(email=offer.delivery_responsible_email)
        self.offer = offer

    def get_context_data(self):
        context = {
            'offer': self.offer,
            'url': '{}{}?token={}&email={}'.format(
                settings.APP_HOSTNAME,
                reverse('offers:review'),
                str(self.token.key),
                self.recipient.email
            )
        }
        return super().get_context_data(**context)
