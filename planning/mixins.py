from django.contrib.auth.models import User
from django.urls import reverse_lazy
from django.contrib import messages
from django.contrib.messages.views import SuccessMessageMixin
from django.shortcuts import redirect
from django.views.generic import FormView
from django.views.generic.base import ContextMixin

from backoffice.constants import GROUP_PLANNING
from backoffice.mixins import GroupAccessMixin

from .forms import DeliveryFormSet


class BasePlanningMixin(GroupAccessMixin, ContextMixin):
    section = None
    access_groups = [GROUP_PLANNING]

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        try:
            trainer = User.objects.get(email=self.request.user.email,
                                       is_active=True)
        except User.DoesNotExist:
            trainer = None

        context['section'] = self.section
        context['user_trainer'] = trainer
        return context


class PlanningRequestBaseView(GroupAccessMixin, SuccessMessageMixin):
    success_url = reverse_lazy('planning:overview')
    access_groups = [GROUP_PLANNING]


class PlanningRequestCreateBaseView(PlanningRequestBaseView, FormView):
    template_name = 'planning/components/modals/planning_create_form.html'
    form_class = DeliveryFormSet
    prefix = 'form'
    instance = None
    initial = []

    def form_invalid(self, formset):
        for error in formset.errors:
            messages.error(self.request, error)
        return redirect('planning:overview')
