from django.utils.text import camel_case_to_spaces


def convert_api_data_to_model_data(name, item):
    key_replacements = {
        'organisation_id': 'organization_id',
        'responsible': 'responsible_id',
        'assigned_to': 'assigned_to_id',
        'assigned_to2': 'assigned_to2_id',
    }

    value_replacements = {
        '--Kies--': '',
        '--KIES--': ''
    }

    result = {}
    for key, value in item.items():
        if name not in key:
            continue

        key = key.replace(name, '')
        key = camel_case_to_spaces(key)
        key = key.replace(' ', '_')

        key = key_replacements.get(key, key)
        value = value_replacements.get(value, value)

        if key == 'person_id' and value == 0:
            continue

        if key == 'assigned_to_id' and value == 0:
            continue

        if key == 'assigned_to2_id' and value == 0:
            continue

        result[key] = value

    return result
