import factory

from planning.models import PlanningRequest, Delivery, Schedule
from users.tests.factories import UserFactory


class PlanningRequestFactory(factory.django.DjangoModelFactory):
    type = PlanningRequest.IMMEDIATE
    created_by = factory.SubFactory(UserFactory)

    class Meta:
        model = PlanningRequest


class DeliveryFactory(factory.django.DjangoModelFactory):
    date = factory.Faker('date_this_year')
    organization = factory.Faker('company')
    time = factory.Faker('time')
    location = factory.Faker('address')
    trainer_amount = 1

    class Meta:
        model = Delivery


class ScheduleFactory(factory.django.DjangoModelFactory):
    delivery = factory.SubFactory(DeliveryFactory)
    user = factory.SubFactory(UserFactory)

    class Meta:
        model = Schedule
