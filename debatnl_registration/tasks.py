import datetime

import pendulum
import pytz
from django.conf import settings

from DebatNL_BackOffice.celery import app
from debatnl_registration.emails.attendance.notifications import \
    ExtendInvitationNotificationEmail, CertificateEmail
from debatnl_registration.emails.participant.notifications import \
    ReviewTrainingNotificationEmail, TrainingContinuationNotificationEmail
from debatnl_registration.emails.reminders import \
    TravelDirectionReminderEmail, BookHallReminderEmail, \
    HallAvailableReminderEmail, CancelHallReminderEmail, \
    UpdateWebsiteReminderEmail, AttendeesListReminderEmail, \
    AttendeesAmountReminderEmail
from debatnl_registration.emails.training_date.notifications import \
    AttendeesAmountNotificationEmail, TrainingConfirmationHallEmail, \
    TrainingInformationNotificationEmail, AttendeesListNotificationEmail
from debatnl_registration.models import PlannedTraining
from debatnl_registration.models import PlannedTrainingToken, Participant, \
    TrainingDate, Attendance
from debatnl_registration.utilities.gravity_form import GravityFormClient
from debatnl_registration.utilities.helpers import calculate_deadline


@app.task
def add_training_dates_to_gravity_form(planned_training_id):
    planned_training = PlannedTraining.objects.get(id=planned_training_id)
    gf_client = GravityFormClient(form_id=39)
    gf_client.add_training_dates(planned_training)


@app.task
def add_2_day_additional_training_dates_to_gravity_form(planned_training_id):
    planned_training = PlannedTraining.objects.get(id=planned_training_id)
    gf_client = GravityFormClient(form_id=45)
    gf_client.add_2_day_additional_training_dates(planned_training)


@app.task
def remove_training_dates_from_gravity_form(planned_training_id):
    planned_training = PlannedTraining.objects.get(id=planned_training_id)
    gf_client = GravityFormClient(form_id=39)
    gf_client.remove_1_day_training_date(planned_training)
    gf_client.remove_2_day_training_dates(planned_training)


@app.task
def remove_2_day_additional_training_dates_from_gravity_form(
        planned_training_id):
    planned_training = PlannedTraining.objects.get(id=planned_training_id)
    gf_client = GravityFormClient(form_id=45)
    gf_client.remove_2_day_additional_training_dates(planned_training)


@app.task
def generate_travel_direction_reminder():
    today = pendulum.today(settings.TIME_ZONE)
    for delta in [3, 6]:
        kwargs = {
            'trainingdate__date': today.add(days=delta).date(),
            'trainingdate__status': TrainingDate.PENDING
        }
        for participant in Participant.objects.filter(**kwargs).all():
            TravelDirectionReminderEmail(participant).send()


@app.task
def generate_extend_invitation_notification():
    today = pendulum.today(settings.TIME_ZONE)
    kwargs = {
        'date': today.subtract(days=1).date(),
        'status': TrainingDate.PENDING
    }

    for training_date in TrainingDate.objects.filter(**kwargs).all():
        participants = training_date.attendees.filter(registration__duration=1)

        for participant in participants.all():
            attendance_kwargs = {
                'participant': participant,
                'training_date': training_date
            }
            attendance = Attendance.objects.get(**attendance_kwargs)
            ExtendInvitationNotificationEmail(attendance).send()


@app.task
def generate_certificate_email(remind_date=None):
    if not remind_date:
        today = pendulum.today(settings.TIME_ZONE)
        remind_date = today.add(days=2).date()

    participants = Participant.objects.filter(
        attendance__training_date__date=remind_date
    ).all()

    for participant in participants:
        if not participant.has_unattended_trainings():
            # Get the training date with highest sequence,
            # to calculate the workload in certificate.
            last_training_day = TrainingDate.objects.filter(
                attendance__participant=participant,
                attendance__has_attended=True
            ).order_by('sequence').last()
            attendance = Attendance.objects.get(
                participant=participant, training_date=last_training_day)
            CertificateEmail(attendance).send()


@app.task
def generate_book_hall_reminder():
    tz = pytz.timezone('Europe/Amsterdam')
    today = datetime.datetime.now(tz)
    tokens = PlannedTrainingToken.objects.filter(type='BOOK HALL').all()

    for token in tokens:
        delta = today - token.created_at
        cancelled = token.planned_training.training_dates.filter(
            status='CANCELLED'
        ).exists()

        if delta.days > 0 and delta.days % 3 == 0 and not cancelled:
            BookHallReminderEmail(token).send()


@app.task
def generate_hall_availability_reminder():
    kwargs = {
        'type': PlannedTrainingToken.HALL_AVAILABLE,
        'created_at__lt': calculate_deadline(days=-3),
        'email_class': HallAvailableReminderEmail
    }
    execute_token_email_with_active_training_dates(**kwargs)


@app.task
def generate_hall_cancellation_reminder():
    kwargs = {
        'type': PlannedTrainingToken.CANCEL_HALL,
        'created_at__lt': calculate_deadline(days=-3),
        'email_class': CancelHallReminderEmail
    }
    execute_token_email(**kwargs)


@app.task
def generate_update_website_reminder():
    kwargs = {
        'type': PlannedTrainingToken.UPDATE_WEBSITE,
        'created_at__lt': calculate_deadline(days=-3),
        'email_class': UpdateWebsiteReminderEmail
    }
    execute_token_email_with_active_training_dates(**kwargs)


@app.task
def generate_attendees_list_reminder():
    deadline = calculate_deadline(days=-1)
    tokens = PlannedTrainingToken.objects.filter(
        type='ATTENDANCE LIST',
        training_date__date__lte=deadline,
        training_date__status='PENDING'
    ).all()

    for token in tokens:
        AttendeesListReminderEmail(token).send()


@app.task
def generate_attendees_amount_notification():
    training_dates = TrainingDate.objects.filter(need_large_hall=False).all()

    for training_date in training_dates.filter(status='PENDING').all():
        if training_date.attendees.count() > 9:
            AttendeesAmountNotificationEmail(training_date).send()
            training_date.need_large_hall = True
            training_date.save()


@app.task
def generate_attendees_amount_reminder():
    kwargs = {
        'type': PlannedTrainingToken.ATTENDEES_AMOUNT,
        'created_at__lt': calculate_deadline(days=-3),
        'training_date__status': TrainingDate.PENDING,
        'email_class': AttendeesAmountReminderEmail,
    }
    execute_token_email(**kwargs)


@app.task
def generate_review_training_notification(
    participant, review_date,
    amount_training_days
):
    if amount_training_days == 1 and not participant.has_feedback:
        ReviewTrainingNotificationEmail(participant).send()

        participant.has_feedback = True
        participant.save()

    elif not participant.has_feedback:
        tz = pytz.timezone('Europe/Amsterdam')
        today = datetime.datetime.now(tz)
        delta = today - review_date

        if delta.days > 0:
            ReviewTrainingNotificationEmail(participant).send()
            participant.has_feedback = True
            participant.save()


@app.task
def generate_training_preparation_notifications():
    deadline = calculate_deadline(days=3)
    for training_date in TrainingDate.objects.filter(
        date=deadline,
        status='PENDING'
    ).all():
        TrainingConfirmationHallEmail(training_date).send()
        TrainingInformationNotificationEmail(training_date).send()
        AttendeesListNotificationEmail(training_date).send()


@app.task
def generate_training_continuation_notifications():
    deadline = calculate_deadline(days=-1)
    for training_date in TrainingDate.objects.filter(
        date=deadline,
        status='FINISHED',
        sequence=3
    ).all():

        for participant in training_date.attendees.all():
            TrainingContinuationNotificationEmail(participant).send()


@app.task
def finalize_training_dates():
    deadline = calculate_deadline(days=-1)
    for training_date in TrainingDate.objects.filter(
        date__lte=deadline,
        status='PENDING'
    ).all():
        training_date.status = 'FINISHED'
        training_date.save()

        if training_date.sequence == 0:
            remove_training_dates_from_gravity_form.delay(
                training_date.planned_training.id
            )
            remove_2_day_additional_training_dates_from_gravity_form.delay(
                training_date.planned_training.id
            )


def execute_token_email(email_class, **kwargs):
    for token in PlannedTrainingToken.objects.filter(**kwargs).all():
        email_class(token).send()


def execute_token_email_with_active_training_dates(email_class, **kwargs):
    for token in PlannedTrainingToken.objects.filter(**kwargs).all():
        training = token.planned_training
        status = TrainingDate.CANCELLED
        if not training.training_dates.filter(status=status).exists():
            email_class(token).send()
