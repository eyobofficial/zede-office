from datetime import timedelta

import mock
import pendulum
from django.conf import settings
from django.test import TestCase, SimpleTestCase
from django.utils import timezone

from intakes.models import Intake, IntakeReminder
from intakes.tasks import process_intake_status, \
    process_intake_reminders_of_today, process_report_reminders_of_today, \
    process_submission_reminders_of_today, process_intake_check_reminder
from intakes.tests.factories import IntakeFactory, IntakeReminderFactory
from jotform.tests.factories import EmptyFormFactory


class IntakeStatusTaskTest(TestCase):
    def setUp(self):
        today = timezone.now()
        self.fake_today = today + timedelta(days=2)
        self.intakes = IntakeFactory.create_batch(
            2, report_date=today.date(), trainers_report_date=today.date())
        IntakeReminderFactory.create_batch(3, intake=self.intakes[0])
        IntakeReminderFactory.create_batch(1, intake=self.intakes[1])

    def set_intake_dates(self, report_date=None, trainer_date=None,
                         reminder_index=None, reminder_date=None):
        intake = self.intakes[0]
        intake.report_date = report_date
        intake.trainers_report_date = trainer_date

        if reminder_index:
            reminders = list(intake.reminders.all())
            reminders[0].date = reminder_date
            reminders[0].save()

        intake.save()

        with pendulum.test(self.fake_today):
            process_intake_status()
            intake.refresh_from_db()

    def test_multiple_status_change(self):
        for intake in self.intakes:
            with pendulum.test(self.fake_today), self.subTest(intake=intake):
                process_intake_status()
                intake.refresh_from_db()
                self.assertTrue(intake.is_finished())
                self.assertEqual(intake.status, Intake.FINISHED)

    def test_status_change_without_date(self):
        self.set_intake_dates()
        self.intakes[0].reminders.all().delete()

        with pendulum.test(self.fake_today):
            process_intake_status()
            self.intakes[0].refresh_from_db()

            self.assertTrue(self.intakes[0].is_finished())
            self.assertEqual(self.intakes[0].status, Intake.FINISHED)

    def test_no_status_change(self):
        scenarios = [
            {'report_date': self.fake_today.date()},
            {'reminder_index': 1, 'reminder_date': self.fake_today},
        ]

        for scenario in scenarios:
            with self.subTest(**scenario):
                self.set_intake_dates(**scenario)
                self.assertFalse(self.intakes[0].is_finished())
                self.assertEqual(self.intakes[0].status, Intake.ONGOING)
                self.assertTrue(Intake.objects.filter(
                    status=Intake.FINISHED).exists())


class BaseOrganizationEmailTaskTestCase(TestCase):
    planned_status = None
    sent_status = None
    mock_path = ''
    intake = None

    def setUp(self):
        today = timezone.now().date()
        yesterday = today - timedelta(days=1)
        tomorrow = today + timedelta(days=1)

        self.scenarios = [
            {
                'input': {'date': yesterday},
                'output': {'called': False, 'status': self.planned_status}
            },
            {
                'input': {'date': today},
                'output': {'called': True, 'status': self.sent_status}
            },
            {
                'input': {'date': tomorrow},
                'output': {'called': False, 'status': self.planned_status}
            },
            {
                'input': {'date': yesterday, 'status': self.sent_status},
                'output': {'called': False, 'status': self.sent_status}
            },
            {
                'input': {'date': today, 'status': self.sent_status},
                'output': {'called': False, 'status': self.sent_status}
            },
            {
                'input': {'date': tomorrow, 'status': self.sent_status},
                'output': {'called': False, 'status': self.sent_status}
            }
        ]

    def start_test_task(self):
        for scenario in self.scenarios:
            in_val = scenario['input']
            out_val = scenario['output']

            with self.subTest(**in_val), mock.patch(self.mock_path) as m_class:
                if self.intake:
                    self.intake.delete()

                self.intake = IntakeFactory()
                self.execute_test_task(in_val, out_val)

                if out_val['called']:
                    m_class.assert_called_once_with(self.intake)
                    m_class().send.assert_called_once()
                else:
                    self.assertFalse(m_class.called)

    def execute_test_task(self, in_val, out_val, *kwargs):
        raise NotImplementedError


class IntakeEmailReminderTaskTest(BaseOrganizationEmailTaskTestCase):
    planned_status = IntakeReminder.PLANNED
    sent_status = IntakeReminder.SENT
    mock_path = 'intakes.tasks.IntakeReminderEmail'

    def test_task(self):
        super().start_test_task()

    def execute_test_task(self, in_val, out_val, **kwargs):
        self.intake.reminders.all().delete()
        reminder = IntakeReminderFactory(intake=self.intake, **in_val)
        process_intake_reminders_of_today()
        reminder.refresh_from_db()
        self.assertEqual(reminder.status, out_val['status'])


class IntakeOrganizationInsightTaskTest(BaseOrganizationEmailTaskTestCase):
    planned_status = Intake.REPORT_PLANNED
    sent_status = Intake.REPORT_SENT
    mock_path = 'intakes.tasks.IntakeOrganizationInsightReportEmail'

    def test_task(self):
        super().start_test_task()

    def execute_test_task(self, in_val, out_val, **kwargs):
        self.intake.report_date = in_val['date']
        self.intake.report_status = in_val.get('status',
                                               self.intake.report_status)
        self.intake.save()

        process_report_reminders_of_today()
        self.intake.refresh_from_db()

        self.assertEqual(self.intake.report_status, out_val['status'])


class IntakeTrainerInsightTaskTest(BaseOrganizationEmailTaskTestCase):
    planned_status = Intake.REPORT_PLANNED
    sent_status = Intake.REPORT_SENT
    mock_path = 'intakes.tasks.IntakeTrainerInsightReportEmail'

    def test_task(self):
        super().start_test_task()

    def execute_test_task(self, in_val, out_val, *args, **kwargs):
        self.intake.trainers_report_date = in_val['date']
        self.intake.trainers_report_status = in_val.get(
            'status', self.intake.trainers_report_status)
        self.intake.save()

        process_submission_reminders_of_today()
        self.intake.refresh_from_db()

        self.assertEqual(self.intake.trainers_report_status, out_val['status'])


class IntakeReminderCheckTest(TestCase):
    @mock.patch('intakes.tasks.IntakeCheckReminderEmail')
    def test_task_with_empty_form(self, mock_class):
        today = pendulum.today(settings.TIME_ZONE)
        future_dt = today.add(days=8).date()

        form = EmptyFormFactory()
        intake = IntakeFactory(form=form, qid_name=1, report_date=future_dt)

        for index in range(1, 8):
            fake_today = today.add(days=index)
            with pendulum.test(fake_today), self.subTest(today=fake_today):
                process_intake_check_reminder()

                if index == 4:
                    mock_class.assert_called_once_with(intake)
                    mock_class().send.assert_called_once()
                else:
                    self.assertFalse(mock_class.called)

            mock_class.reset_mock()

    @mock.patch('intakes.tasks.IntakeCheckReminderEmail')
    def test_task_with_filled_form(self, mock_class):
        today = pendulum.today(settings.TIME_ZONE)
        future_dt = today.add(days=8).date()
        IntakeFactory(report_date=future_dt)

        for index in range(1, 8):
            fake_today = today.add(days=index)
            with pendulum.test(fake_today), self.subTest(today=fake_today):
                process_intake_check_reminder()
                self.assertFalse(mock_class.called)
            mock_class.reset_mock()


class CleanUpOutdatedIntakeTaskTest(TestCase):
    pass


class RefreshFormDataIntakeTest(SimpleTestCase):
    pass


