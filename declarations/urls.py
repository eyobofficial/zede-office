from django.conf.urls import url
from django.views.generic import RedirectView

from .views import DeclarationOverview, ClientBonusCreateView, \
    ClientBonusListView, ClientBonusDeleteView, \
    EmployeeBonusCreateView, EmployeeBonusDeleteView, EmployeeBonusListView, \
    ExpenseCreateView, ExpenseDeleteView, ExpenseListView, \
    ELearningItemCreateView, ELearningItemListView, ELearningItemDeleteView, \
    TravelExpenseCreateView, \
    TravelExpenseCalculationView, TravelExpenseUpdateView, \
    TravelExpenseDeleteView, ExternalMeetingTimeDifferencesListView, \
    AllowanceCreateView, AllowanceListView, InternalAllowanceDeleteView, \
    ExternalAllowanceDeleteView, TabExternalMeetingUpdateView, \
    TabInternalMeetingUpdateView, TabELearningUpdateView, \
    TabHubstaffUpdateView, TabAbsenceUpdateView, TabExpenseUpdateView, \
    TabExpenseDetailView, TabAbsenceDetailView, TabHubstaffDetailView, \
    TabELearningDetailView, TabInternalMeetingDetailView, \
    TabExternalMeetingDetailView, DeclarationRedirectView

urlpatterns = [
    url(r'^$', DeclarationOverview.as_view(), name='overview'),
    url(
        r'^(?P<pk>[0-9]+)/update/$',
        DeclarationRedirectView.as_view(),
        name='update'
    ),
    url(
        r'^(?P<pk>[0-9]+)/external-meetings/update/$',
        TabExternalMeetingUpdateView.as_view(),
        name='external-meeting-update'
    ),
    url(
        r'^(?P<pk>[0-9]+)/internal-meetings/update/$',
        TabInternalMeetingUpdateView.as_view(),
        name='internal-meeting-update'
    ),
    url(
        r'^(?P<pk>[0-9]+)/e-learning/update/$',
        TabELearningUpdateView.as_view(),
        name='e-learning-update'
    ),
    url(
        r'^(?P<pk>[0-9]+)/hubstaff/update/$',
        TabHubstaffUpdateView.as_view(),
        name='hubstaff-update'
    ),
    url(
        r'^(?P<pk>[0-9]+)/absences/update/$',
        TabAbsenceUpdateView.as_view(),
        name='absence-update'
    ),
    url(
        r'^(?P<pk>[0-9]+)/expenses/update/$',
        TabExpenseUpdateView.as_view(),
        name='expense-update'
    ),
    url(
        r'^(?P<pk>[0-9]+)/$',
        RedirectView.as_view(
            url='/declarations/%(pk)s/external-meetings/'
        ),
        name='detail'
    ),
    url(
        r'^(?P<pk>[0-9]+)/external-meetings/$',
        TabExternalMeetingDetailView.as_view(),
        name='external-meeting-detail'
    ),
    url(
        r'^(?P<pk>[0-9]+)/internal-meetings/$',
        TabInternalMeetingDetailView.as_view(),
        name='internal-meeting-detail'
    ),
    url(
        r'^(?P<pk>[0-9]+)/e-learning/$',
        TabELearningDetailView.as_view(),
        name='e-learning-detail'
    ),
    url(
        r'^(?P<pk>[0-9]+)/hubstaff/$',
        TabHubstaffDetailView.as_view(),
        name='hubstaff-detail'
    ),
    url(
        r'^(?P<pk>[0-9]+)/absences/$',
        TabAbsenceDetailView.as_view(),
        name='absence-detail'
    ),
    url(
        r'^(?P<pk>[0-9]+)/expenses/$',
        TabExpenseDetailView.as_view(),
        name='expense-detail'
    ),
    url(
        r'^travel-expenses/create/$',
        TravelExpenseCreateView.as_view(),
        name='travel-expenses-create'
    ),
    url(
        r'^travel-expenses/(?P<pk>[0-9]+)/update/$',
        TravelExpenseUpdateView.as_view(),
        name='travel-expenses-update'
    ),
    url(
        r'^travel-expenses/(?P<pk>[0-9]+)/delete/$',
        TravelExpenseDeleteView.as_view(),
        name='travel-expenses-delete'
    ),
    url(
        r'^travel-expenses/calculations/$',
        TravelExpenseCalculationView.as_view(),
        name='travel-expenses-calculations'
    ),
    url(
        r'^(?P<pk>[0-9]+)/expenses/create/$',
        ExpenseCreateView.as_view(),
        name='expenses-create'
    ),
    url(
        r'^(?P<declaration>[0-9]+)/expenses/(?P<pk>[0-9]+)/delete/$',
        ExpenseDeleteView.as_view(),
        name='expenses-delete'
    ),
    url(
        r'^(?P<pk>[0-9]+)/expenses/$',
        ExpenseListView.as_view(),
        name='expenses-list'
    ),
    url(
        r'^(?P<pk>[0-9]+)/allowances/$',
        AllowanceListView.as_view(),
        name='allowances-list'
    ),
    url(
        r'^allowances/(?P<pk>[0-9]+)/int-meetings/(?P<meeting>[0-9]+)/delete/$',
        InternalAllowanceDeleteView.as_view(),
        name='internal-allowances-delete'
    ),
    url(
        r'^allowances/(?P<pk>[0-9]+)/ext-meetings/(?P<meeting>[0-9]+)/delete/$',
        ExternalAllowanceDeleteView.as_view(),
        name='external-allowances-delete'
    ),
    url(
        r'^(?P<pk>[0-9]+)/employee-bonuses/create/$',
        EmployeeBonusCreateView.as_view(),
        name='employee-bonuses-create'
    ),
    url(
        r'^(?P<declaration>[0-9]+)/employee-bonuses/(?P<pk>[0-9]+)/delete/$',
        EmployeeBonusDeleteView.as_view(),
        name='employee-bonuses-delete'
    ),
    url(
        r'^(?P<pk>[0-9]+)/employee-bonuses/$',
        EmployeeBonusListView.as_view(),
        name='employee-bonuses-list'
    ),
    url(
        r'^(?P<pk>[0-9]+)/client-bonuses/create/$',
        ClientBonusCreateView.as_view(),
        name='client-bonuses-create'
    ),
    url(
        r'^(?P<declaration>[0-9]+)/client-bonuses/(?P<pk>[0-9]+)/delete/$',
        ClientBonusDeleteView.as_view(),
        name='client-bonuses-delete'
    ),
    url(
        r'^(?P<pk>[0-9]+)/client-bonuses/$',
        ClientBonusListView.as_view(),
        name='client-bonuses-list'
    ),
    url(
        r'^(?P<pk>[0-9]+)/allowances/form/$',
        AllowanceCreateView.as_view(),
        name='allowance-create'
    ),
    url(
        r'^(?P<pk>[0-9]+)/elearning-items/$',
        ELearningItemListView.as_view(),
        name='elearning-items-list'
    ),
    url(
        r'^(?P<pk>[0-9]+)/elearning-items/create/$',
        ELearningItemCreateView.as_view(),
        name='elearning-items-create'
    ),
    url(
        r'^(?P<declaration>[0-9]+)/elearning-items/(?P<pk>[0-9]+)/delete/$',
        ELearningItemDeleteView.as_view(),
        name='elearning-items-delete'
    ),
    url(
        r'^external-meeting/(?P<pk>[0-9]+)/time-difference/$',
        ExternalMeetingTimeDifferencesListView.as_view(),
        name='external-meeting-time-difference'
    )
]
