from django.contrib.auth.models import User

from schedules.models import Timetable
from shared.enums import RoleName


def get_daily_timetable(user, date):
    """
    Calculate timetable for a trainer for a given date
    """
    timetable = {
        'date': date,
        'vacation': False,
        'morning': None,
        'afternoon': None,
        'evening': None
    }

    day_name = date.strftime("%A").lower()
    week_number = date.isocalendar()[1]

    for vacation in user.vacations.all():
        if vacation.on_vacation(date):
            timetable['vacation'] = True
            break

    for key, value in Timetable.DAYPART_CHOICES:
        user_timetable = user.timetables.filter(year=date.year,
                                                week=week_number,
                                                daypart=key).last()
        if user_timetable:
            timetable[value.lower()] = getattr(user_timetable, day_name)

    return timetable


def calculate_trainers_timetable(delivery):
    """
    Calculate a timetable for a list for trainers
    """
    trainer_list = User.objects.filter(
        profile__roles__name=RoleName.TRAINER.value,
        is_active=True
    )
    trainers = []

    for trainer in trainer_list:
        previous_timetable = get_daily_timetable(
            trainer,
            delivery.previous_date()
        )

        current_timetable = get_daily_timetable(
            trainer,
            delivery.date
        )

        next_timetable = get_daily_timetable(
            trainer,
            delivery.next_date()
        )

        trainers.append({
            'full_name': trainer.get_full_name(),
            'previous': previous_timetable,
            'current': current_timetable,
            'next': next_timetable,
        })

    return trainers
