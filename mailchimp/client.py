import hashlib
import requests
from django.conf import settings
from requests.auth import HTTPBasicAuth


class MailChimpClient:
    _base_url = 'https://us10.api.mailchimp.com/3.0'

    def __init__(self):
        self._auth = HTTPBasicAuth('Olaf', settings.MAILCHIMP_API_KEY)

    def _get(self, path):
        url = f'{self._base_url}{path}'
        response = requests.get(url, auth=self._auth)
        response.raise_for_status()
        return response.json()

    def _put(self, path, payload):
        url = f'{self._base_url}{path}'
        response = requests.put(url, json=payload, auth=self._auth)
        response.raise_for_status()
        return response.json()

    def get_lists(self):
        return self._get('/lists')

    def get_list(self, list_id):
        return self._get(f'/lists/{list_id}')

    def create_or_update_member(self, list_id, payload):
        email = payload['email_address'].lower()
        subscriber_hash = hashlib.md5(email.encode()).hexdigest()
        path = f'/lists/{list_id}/members/{subscriber_hash}'
        return self._put(path, payload)
