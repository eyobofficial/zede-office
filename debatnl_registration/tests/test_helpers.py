import datetime

import pytz
from django.test import TestCase, SimpleTestCase

from backoffice.tests.factories import TrainingFactory, UserFactory
from debatnl_registration.models import Registration, Participant, \
    TrainingDate, PlannedTraining, PlannedTrainingToken, \
    Attendance
from debatnl_registration.tasks import finalize_training_dates
from debatnl_registration.tests.factories import PlannedTrainingFactory, \
    TrainingDateFactory, RegistrationFactory, \
    ParticipantFactory, AttendanceFactory
from debatnl_registration.utilities.helpers import persist_registration, \
    get_jotform_url, get_training_urls, \
    get_springest_url, get_training_continuation_data, \
    persist_planned_training, change_attendance, delete_attendance
from shared.enums import RoleName
from users.models import Role


class RegistrationPersistenceTest(TestCase):
    def test_with_all_fields_filled(self):
        training = TrainingFactory(name='Overtuigend Debatteren')
        planned_training = PlannedTrainingFactory(training=training)
        TrainingDateFactory(
            planned_training=planned_training,
            date=datetime.date(2018, 10, 2)
        )
        TrainingDateFactory(
            planned_training=planned_training,
            date=datetime.date(2018, 11, 6)
        )
        TrainingDateFactory(
            planned_training=planned_training,
            date=datetime.date(2018, 12, 4)
        )

        data = {
            'submission_id': '1962',
            'form_id': '39',
            'training': 'Overtuigend Debatteren',
            'training_dates': [
                datetime.date(2018, 10, 2),
                datetime.date(2018, 11, 6),
                datetime.date(2018, 12, 4)
            ],
            'duration': 3,
            'participant_count': 2,
            'participants': [
                {
                    'name': 'Naam A',
                    'email': 'naama@test.mail',
                    'phone': '123456789',
                    'dietary_wishes': 'Vegan'
                },
                {
                    'name': 'Naam B',
                    'email': 'naamb@test.mail',
                    'phone': '123456789',
                    'dietary_wishes': 'Geen'
                }
            ],
            'registrant_first_name': 'Naam C',
            'registrant_last_name': 'Achternaam C',
            'registrant_email': 'naamc@test.mail',
            'registrant_phone': '123456789'
        }

        registration = persist_registration(data.copy())

        self.assertEqual(Registration.objects.count(), 1)
        self.assertEqual(registration.submission_id, data['submission_id'])
        self.assertEqual(registration.form_id, data['form_id'])
        self.assertEqual(registration.training, data['training'])
        self.assertEqual(
            registration.participant_count,
            data['participant_count']
        )
        self.assertEqual(
            registration.registrant_first_name,
            data['registrant_first_name']
        )
        self.assertEqual(
            registration.registrant_last_name,
            data['registrant_last_name']
        )
        self.assertEqual(
            registration.registrant_email,
            data['registrant_email']
        )
        self.assertEqual(
            registration.registrant_phone,
            data['registrant_phone']
        )

        self.assertEqual(Participant.objects.count(), 2)
        for index, participant in enumerate(
                Participant.objects.order_by('name').all()
        ):
            with self.subTest(participant=participant):
                participant_data = data['participants'][index]
                self.assertEqual(
                    participant.name,
                    participant_data.get('name')
                )
                self.assertEqual(
                    participant.email,
                    participant_data.get('email')
                )
                self.assertEqual(
                    participant.phone,
                    participant_data.get('phone')
                )
                self.assertEqual(
                    participant.dietary_wishes,
                    participant_data.get('dietary_wishes')
                )
                self.assertEqual(participant.registration, registration)
                self.assertTrue(
                    TrainingDate.objects.filter(
                        attendance__participant=participant
                    ).exists()
                )

        self.assertEqual(TrainingDate.objects.count(), 3)
        for index, training_date in enumerate(
                TrainingDate.objects.order_by('date').all()
        ):
            with self.subTest(training_date=training_date):
                date = data['training_dates'][index]
                self.assertEqual(training_date.date, date)


class PlannedTrainingPersistenceTest(TestCase):
    fixtures = ['roles']

    def setUp(self):
        self.training = TrainingFactory()

        role = Role.objects.get(name=RoleName.TRAINER.value)
        self.trainers = UserFactory.create_batch(3)
        for trainer in self.trainers:
            trainer.profile.roles.add(role)

    def test_with_all_fields_filled(self):
        data = {
            'training': self.training.id,
            'dates': [
                {
                    'sequence': 0,
                    'date': datetime.date(2018, 1, 1),
                    'trainers': [self.trainers[0].pk]
                },
                {
                    'sequence': 1,
                    'date': datetime.date(2018, 2, 2),
                    'trainers': [self.trainers[0].pk, self.trainers[1].pk]
                },
                {
                    'sequence': 2,
                    'date': datetime.date(2018, 3, 3),
                    'trainers': [
                        self.trainers[0].pk,
                        self.trainers[1].pk,
                        self.trainers[2].pk
                    ]
                }
            ]
        }

        planned_training = persist_planned_training(data.copy())

        self.assertEqual(PlannedTraining.objects.count(), 1)
        self.assertEqual(TrainingDate.objects.count(), 3)
        self.assertEqual(planned_training.training.id, self.training.id)

        for index, date in enumerate(
                TrainingDate.objects.all().order_by('sequence')
        ):
            with self.subTest(date=date):
                self.assertEqual(index, date.sequence)
                self.assertEqual(data['dates'][index]['date'], date.date)

                for trainer in data['dates'][index]['trainers']:
                    self.assertTrue(
                        date.trainers.filter(
                            pk=trainer
                        ).exists())


class EmailHelpersTest(SimpleTestCase):

    def test_retrieval_of_jot_form_url(self):
        selection = {
            'Bewust Beinvloeden':
                'https://form.jotform.com/71505246784965',
            'Ontspannen Presenteren':
                'https://form.jotform.com/71505088484966',
            'Overtuigend Debatteren':
                'https://form.jotform.com/71505115884961',
            'Professioneel Gespreksleiden':
                'https://form.jotform.com/71505606784965',
        }

        for key, value in selection.items():
            url = get_jotform_url(key)

            with self.subTest(value=value):
                self.assertEqual(url, value)

    def test_retrieval_training_urls(self):
        selection = {
            'Bewust Beinvloeden': {
                'schedule_url': 'https://debat.nl/training/beinvloeden/#'
                                '3-daagse',
                'registration_url': 'https://debat.nl/inschrijving-dag-2-3/?'
                                    'type=Bewust%20Beinvloeden'
            },
            'Ontspannen Presenteren': {
                'schedule_url': 'https://debat.nl/training/presenteren/#'
                                '3-daagse',
                'registration_url': 'https://debat.nl/inschrijving-dag-2-3/?'
                                    'type=Ontspannen%20Presenteren'
            },
            'Overtuigend Debatteren': {
                'schedule_url': 'https://debat.nl/training/debatteren/#'
                                '3-daagse',
                'registration_url': 'https://debat.nl/inschrijving-dag-2-3/?'
                                    'type=Overtuigend%20Debatteren'
            },
            'Professioneel Gespreksleiden': {
                'schedule_url': 'https://debat.nl/training/gespreksleiden/#'
                                '3-daagse',
                'registration_url': 'https://debat.nl/inschrijving-dag-2-3/?'
                                    'type=Professioneel%20Gespreksleiden'
            },
        }

        for key, value in selection.items():
            url = get_training_urls(key)

            with self.subTest(value=value):
                self.assertDictEqual(url, value)

    def test_retrieval_springest_url(self):
        base_url = 'https://www.springest.nl/debat-nl'

        selection = {
            'Bewust Beinvloeden': {
                1: '{}/bewust-beinvloeden-2/ervaring-invoeren',
                3: '{}/bewust-beinvloeden-3-dagen/ervaring-invoeren'
            },
            'Ontspannen Presenteren': {
                1: '{}/ontspannen-presenteren-2/ervaring-invoeren',
                3: '{}/ontspannen-presenteren-3-dagen/ervaring-invoeren'
            },
            'Overtuigend Debatteren': {
                1: '{}/overtuigend-debatteren-2/ervaring-invoeren',
                3: '{}/overtuigend-debatteren-3-dagen/ervaring-invoeren'
            },
            'Professioneel Gespreksleiden': {
                1: '{}/professioneel-gespreksleiden-2/ervaring-invoeren',
                3: '{}/professioneel-gespreksleiden-3-dagen/ervaring-invoeren'
            },
        }

        for training, urls in selection.items():
            for duration, expected_uri in urls.items():
                url = get_springest_url(training, duration)
                expected_url = expected_uri.format(base_url)

                with self.subTest(expected_url=expected_url):
                    self.assertEqual(expected_url, url)

    def test_training_continuation_data(self):
        selection = {
            'Bewust Beinvloeden': {
                'button_text': 'Bekijk programma Ontspannen Presenteren',
                'url': 'https://debat.nl/training/presenteren/'
            },
            'Ontspannen Presenteren': {
                'button_text': 'Bekijk programma Bewust Beinvloeden',
                'url': 'https://debat.nl/training/beinvloeden/'
            },
            'Overtuigend Debatteren': {
                'button_text': 'Bekijk programma Ontspannen Presenteren',
                'url': 'https://debat.nl/training/presenteren/'
            },
            'Professioneel Gespreksleiden': {
                'button_text': 'Bekijk programma Ontspannen Presenteren',
                'url': 'https://debat.nl/training/presenteren/'
            },
        }

        for training, expected_data in selection.items():
            data = get_training_continuation_data(training)

            with self.subTest(expected_data=expected_data):
                self.assertEqual(expected_data, data)


class ChangeTrainingPersistenceTest(TestCase):
    def setUp(self):
        planned_training = PlannedTrainingFactory()
        registration = RegistrationFactory()
        participant = ParticipantFactory(registration=registration)

        self.date_1 = TrainingDateFactory(
            planned_training=planned_training,
            sequence=0
        )
        self.date_2 = TrainingDateFactory(
            planned_training=planned_training,
            sequence=1
        )
        self.date_3 = TrainingDateFactory(
            planned_training=planned_training,
            sequence=2
        )

        self.attendance_1 = AttendanceFactory(
            training_date=self.date_1,
            participant=participant
        )
        self.attendance_2 = AttendanceFactory(
            training_date=self.date_2,
            participant=participant
        )
        self.attendance_3 = AttendanceFactory(
            training_date=self.date_3,
            participant=participant
        )

        self.token = PlannedTrainingToken.objects.create(
            planned_training=planned_training,
            participant=participant,
            type='CANCEL TRAINING PARTICIPANT')

    def test_change_training_exclude_first_day(self):
        new_date_1 = TrainingDateFactory(sequence=0)
        new_date_2 = TrainingDateFactory(
            sequence=1,
            planned_training=new_date_1.planned_training
        )
        new_date_3 = TrainingDateFactory(
            sequence=2,
            planned_training=new_date_1.planned_training
        )

        data = {
            'new_planned_training': new_date_1.planned_training
        }

        change_attendance(self.token, data, exclude_first_day=True)

        self.attendance_1.refresh_from_db()
        self.attendance_2.refresh_from_db()
        self.attendance_3.refresh_from_db()

        self.assertEqual(self.attendance_1.training_date, self.date_1)
        self.assertEqual(self.attendance_2.training_date, new_date_2)
        self.assertEqual(self.attendance_3.training_date, new_date_3)

    def test_change_training_include_first_day(self):
        planned_training = PlannedTrainingFactory()

        new_date_1 = TrainingDateFactory(
            planned_training=planned_training,
            sequence=0
        )
        new_date_2 = TrainingDateFactory(
            planned_training=planned_training,
            sequence=1
        )
        new_date_3 = TrainingDateFactory(
            planned_training=planned_training,
            sequence=2
        )

        planned_training.refresh_from_db()

        data = {
            'new_planned_training': planned_training
        }

        change_attendance(self.token, data, exclude_first_day=False)

        self.attendance_1.refresh_from_db()
        self.attendance_2.refresh_from_db()
        self.attendance_3.refresh_from_db()

        self.assertEqual(self.attendance_1.training_date, new_date_1)
        self.assertEqual(self.attendance_2.training_date, new_date_2)
        self.assertEqual(self.attendance_3.training_date, new_date_3)

    def test_attendance_deletion(self):
        delete_attendance(self.token)
        self.assertEqual(Attendance.objects.count(), 0)


class FinalizeTrainingDatesTest(TestCase):
    def test_pending_training_date(self):
        tz = pytz.timezone('Europe/Amsterdam')
        today = datetime.datetime.now(tz)

        for n in range(-1, 2):
            training_date = TrainingDateFactory()
            training_date.date = today + datetime.timedelta(days=n)
            training_date.save()

        finalize_training_dates()

        self.assertEqual(
            TrainingDate.objects.filter(status='FINISHED').count(),
            1
        )
        self.assertEqual(
            TrainingDate.objects.filter(status='PENDING').count(),
            2
        )

    def test_cancelled_training_dates(self):
        tz = pytz.timezone('Europe/Amsterdam')
        today = datetime.datetime.now(tz)

        for n in range(-2, 1):
            training_date = TrainingDateFactory(status='CANCELLED')
            training_date.date = today + datetime.timedelta(days=n)
            training_date.save()

        finalize_training_dates()

        self.assertEqual(
            TrainingDate.objects.filter(status='FINISHED').count(),
            0
        )
        self.assertEqual(
            TrainingDate.objects.filter(status='PENDING').count(),
            0
        )
        self.assertEqual(
            TrainingDate.objects.filter(status='CANCELLED').count(),
            3
        )
