import datetime

import mock
import pendulum
from django.conf import settings
from django.test import TestCase
from django.utils import timezone

from scans.tasks import send_trainer_scan_submission_reminder, \
    send_trainer_scan_review_reminder
from scans.tests.factories import ScanFactory
from shared.enums import RoleName
from users.models import Role
from users.tests.factories import UserFactory


class SendTrainerScanSubmissionReminderTest(TestCase):
    fixtures = ['roles']

    def setUp(self):
        role = Role.objects.get(name=RoleName.ADMIN.value)
        admin = UserFactory()
        admin.profile.roles.add(role)

        deadline = pendulum.datetime(2019, 10, 4).date()
        self.scan = ScanFactory(deadline_at=deadline)

    @mock.patch('scans.tasks.ScanSubmissionReminderEmail')
    def test_task(self, mock_email):
        fake_today = pendulum.datetime(2019, 10, 5, tz=settings.TIME_ZONE)
        with pendulum.test(fake_today):
            send_trainer_scan_submission_reminder()

        mock_email.assert_called_once_with(self.scan)
        mock_email().send.assert_called_once()

    @mock.patch('scans.tasks.ScanSubmissionReminderEmail')
    def test_task_with_invalid_deadline(self, mock_email):
        fake_today = pendulum.datetime(2019, 10, 4, tz=settings.TIME_ZONE)
        with pendulum.test(fake_today):
            send_trainer_scan_submission_reminder()

        self.assertFalse(mock_email.called)


class SendTrainerScanReviewReminderTest(TestCase):
    fixtures = ['roles']

    def setUp(self):
        role = Role.objects.get(name=RoleName.ADMIN.value)
        admin = UserFactory()
        admin.profile.roles.add(role)

        tz = timezone.get_current_timezone()
        submitted_at = datetime.datetime(2019, 10, 4, 12)
        submitted_at = timezone.make_aware(submitted_at, tz)
        self.scan = ScanFactory(submitted_at=submitted_at)

    @mock.patch('scans.tasks.ScanReviewReminderEmail')
    def test_task_after_24_hours(self, mock_email):
        fake_today = pendulum.datetime(2019, 10, 5, 12, 15,
                                       tz=settings.TIME_ZONE)
        with pendulum.test(fake_today):
            send_trainer_scan_review_reminder()

        mock_email.assert_called_once_with(self.scan)
        mock_email().send.assert_called_once()

    @mock.patch('scans.tasks.ScanReviewReminderEmail')
    def test_task_after_48_hours(self, mock_email):
        fake_today = pendulum.datetime(2019, 10, 6, 12, 45,
                                       tz=settings.TIME_ZONE)
        with pendulum.test(fake_today):
            send_trainer_scan_review_reminder()

        mock_email.assert_called_once_with(self.scan)
        mock_email().send.assert_called_once()

    @mock.patch('scans.tasks.ScanSubmissionReminderEmail')
    def test_task_with_invalid_dates(self, mock_email):
        invalid_dates = [
            pendulum.datetime(2019, 10, 4, 12, tz=settings.TIME_ZONE),
            pendulum.datetime(2019, 10, 5, 11, tz=settings.TIME_ZONE),
            pendulum.datetime(2019, 10, 6, 13, tz=settings.TIME_ZONE)
        ]

        for invalid_date in invalid_dates:
            with pendulum.test(invalid_date), self.subTest(date=invalid_date):
                send_trainer_scan_review_reminder()
                self.assertFalse(mock_email.called)
