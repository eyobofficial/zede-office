from django.contrib import admin

from backoffice.admin import EmailModelAdmin
from debatnl_registration.models import TrainingDate, PlannedTraining, \
    Participant, Registration, Attendance, PlannedTrainingToken, \
    BloomvilleFeedItem, SpringestFeedItem
from debatnl_registration.views import BookHallNotificationEmailView, \
    BookHallReminderEmailView, CancelHallNotificationEmailView, \
    CancelTrainingToParticipantEmailView, CancelTrainingToTrainersEmailView, \
    HallNotAvailableToAdminEmailView, HallNotAvailableToTrainersEmailView, \
    HallAvailableNotificationEmailView, UpdateWebsiteNotificationEmailView, \
    TrainingInformationNotificationEmailView, \
    TrainingConfirmationHallEmailView, AttendeesListNotificationEmailView, \
    AttendeesAmountNotificationEmailView, RegistrationConfirmationEmailView, \
    TravelDirectionReminderEmailView, ReviewTrainingNotificationEmailView, \
    TrainingContinuationNotificationEmailView, \
    ExtendInvitationNotificationEmailView, CertificateEmailView, \
    CancelHallReminderEmailView, HallAvailableReminderEmailView, \
    UpdateWebsiteReminderEmailView, AttendeesListReminderEmailView, \
    AttendeesAmountReminderEmailView


@admin.register(PlannedTraining)
class PlannedTrainingAdmin(EmailModelAdmin):
    list_display = ('training', 'hall_status')
    email_views = [
        BookHallNotificationEmailView,
        CancelHallNotificationEmailView,
        CancelTrainingToParticipantEmailView,
        CancelTrainingToTrainersEmailView,
        HallNotAvailableToAdminEmailView,
        HallNotAvailableToTrainersEmailView,
        HallAvailableNotificationEmailView,
        UpdateWebsiteNotificationEmailView
    ]


@admin.register(TrainingDate)
class TrainingDateAdmin(EmailModelAdmin):
    list_display = ('date', 'sequence', 'planned_training', 'status')
    email_views = [
        TrainingInformationNotificationEmailView,
        TrainingConfirmationHallEmailView,
        AttendeesListNotificationEmailView,
        AttendeesAmountNotificationEmailView
    ]


@admin.register(Participant)
class ParticipantAdmin(EmailModelAdmin):
    list_display = ('name', 'has_feedback')
    email_views = [
        RegistrationConfirmationEmailView,
        TravelDirectionReminderEmailView,
        ReviewTrainingNotificationEmailView,
        TrainingContinuationNotificationEmailView
    ]


@admin.register(Registration)
class RegistrationAdmin(admin.ModelAdmin):
    list_display = ('submission_id', 'registrant_first_name',
                    'registrant_last_name')
    search_fields = ('submission_id', 'registrant_first_name',
                     'registrant_last_name')


@admin.register(Attendance)
class AttendanceAdmin(EmailModelAdmin):
    list_display = ('participant', 'training_date', 'has_attended')
    email_views = [
        ExtendInvitationNotificationEmailView,
        CertificateEmailView,
    ]


@admin.register(PlannedTrainingToken)
class PlannedTrainingTokenAdmin(EmailModelAdmin):
    list_display = ('type', 'planned_training', 'training_date', 'participant')
    email_views = [
        BookHallReminderEmailView,
        CancelHallReminderEmailView,
        HallAvailableReminderEmailView,
        UpdateWebsiteReminderEmailView,
        AttendeesListReminderEmailView,
        AttendeesAmountReminderEmailView
    ]


@admin.register(SpringestFeedItem)
class SpringestFeedItemAdmin(admin.ModelAdmin):
    list_display = ('id', 'training', 'description', 'website')


@admin.register(BloomvilleFeedItem)
class BloomvilleFeedItemAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'category',
        'training',
        'description',
        'course_format'
    )
