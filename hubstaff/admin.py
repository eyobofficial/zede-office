from django.contrib import admin

from hubstaff.models import HubstaffAuthToken


@admin.register(HubstaffAuthToken)
class HubstaffAuthTokenAdmin(admin.ModelAdmin):
    list_display = ('token', 'last_activity', 'updated_at', 'created_at')
