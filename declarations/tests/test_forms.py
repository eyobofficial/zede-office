from django.test import TestCase

from declarations.forms import DeclarationModelForm


class DeclarationModelFormTests(TestCase):
    """
    Tests for `DeclarationModelForm`
    """

    def test_work_and_company_satisfaction_field_when_form_is_saved(self):
        """
        When the `work_satisfaction` & `company_satisfaction` fields are blank
        and the user saves the form using the `Tussentijds opslaan` button,
        ensures the form is saved without any validation errors.
        """
        data = {
            'internal_hourly_rate': 10,
            'billable_hourly_rate': 20,
            'feedback': 'Sample text',
            'save': True
        }
        form = DeclarationModelForm(data=data)

        # Assertions
        self.assertTrue(form.is_valid())

    def test_work_satisfaction_when_form_is_submitted_with_blank_value(self):
        """
        When the `work_satisfaction` field is blank & the user submits the form
        using the `Definitief inzenden` button, ensures validation error is
        raised.
        """
        data = {
            'internal_hourly_rate': 10,
            'billable_hourly_rate': 20,
            'company_satisfaction': 5,
            'feedback': 'Sample text',
            'submit': True
        }
        form = DeclarationModelForm(data=data)

        # Assertions
        self.assertFalse(form.is_valid())

    def test_company_satisfaction_when_form_is_submitted_with_blank_value(self):
        """
        When the `company_satisfaction` field is blank & the user submits the
        form using the `Definitief inzenden` button, ensures validation error is
        raised.
        """
        data = {
            'internal_hourly_rate': 10,
            'billable_hourly_rate': 20,
            'work_satisfaction': 5,
            'feedback': 'Sample text',
            'submit': True
        }
        form = DeclarationModelForm(data=data)

        # Assertions
        self.assertFalse(form.is_valid())

    def test_when_form_is_submitted_with_valid_values(self):
        """
        When `work_satisfaction` & `company_satisfaction` fields have valid
        integer values, ensure that the form is saved without any validation
        error.
        """
        data = {
            'internal_hourly_rate': 10,
            'billable_hourly_rate': 20,
            'work_satisfaction': 5,
            'company_satisfaction': 5,
            'feedback': 'Sample text',
            'submit': True
        }
        form = DeclarationModelForm(data=data)

        # Assertions
        self.assertTrue(form.is_valid())
