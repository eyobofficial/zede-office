from datetime import datetime
from django.test import SimpleTestCase

from offers.utilities.dates import calculate_workday, is_workday


class WorkdayCalculationTest(SimpleTestCase):
    def test_calculation(self):
        expectations = [
            datetime(2018, 1, 3, 0, 0),
            datetime(2018, 1, 4, 0, 0),
            datetime(2018, 1, 5, 0, 0),
            datetime(2018, 1, 8, 0, 0),
            datetime(2018, 1, 9, 0, 0),
            datetime(2018, 1, 9, 0, 0),
            datetime(2018, 1, 9, 0, 0),
            datetime(2018, 1, 10, 0, 0),
            datetime(2018, 1, 11, 0, 0),
            datetime(2018, 1, 12, 0, 0),
        ]

        for index in range(0, 10):
            date = datetime(2018, 1, index + 1, 0, 0)

            with self.subTest(date=date):
                outcome = calculate_workday(date, 2)
                self.assertEqual(outcome, expectations[index])


class DetermineWorkdayTest(SimpleTestCase):
    def test_if_workday(self):
        for index in range(0, 5):
            with self.subTest():
                date = datetime(2018, 1, index + 1, 0, 0)
                self.assertTrue(is_workday(date))

    def test_if_not_a_workday(self):
        for index in range(5, 7):
            with self.subTest():
                date = datetime(2018, 1, index + 1, 0, 0)
                self.assertFalse(is_workday(date))
