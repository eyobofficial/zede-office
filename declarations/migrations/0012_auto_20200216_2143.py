# -*- coding: utf-8 -*-
# Generated by Django 1.11.26 on 2020-02-16 20:43
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('declarations', '0011_merge_20200205_1338'),
    ]

    operations = [
        migrations.AlterField(
            model_name='internalmeeting',
            name='travel_expense',
            field=models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='internal_meetings', to='declarations.TravelExpense'),
        ),
    ]
