from django.conf.urls import url, include

urlpatterns = [
    url(r'^seo/', include('marketing.urls.seo', namespace='seo')),
    url(r'^kpi/', include('marketing.urls.kpi', namespace='kpi')),
]
