from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User

from backoffice.admin import CustomURLModelAdmin
from users.views import UserInvitationEmailView, \
    NoHubstaffAccountNotificationEmailView

from .models import Role, UserProfile, AppPermission


class CustomUserAdmin(CustomURLModelAdmin, UserAdmin):
    custom_urls = [
        {
            'regex': r'^(?P<pk>.+)/send_user_invitation_email/$',
            'view': UserInvitationEmailView,
            'name': 'send_user_invitation_email'
        }
    ]


admin.site.unregister(User)
admin.site.register(User, CustomUserAdmin)


@admin.register(Role)
class RoleAdmin(admin.ModelAdmin):
    list_display = ('name', 'allow_multiple_users')
    search_fields = ('name', )
    ordering = ('name', )


@admin.register(AppPermission)
class AppPermissionAdmin(admin.ModelAdmin):
    list_display = ('display_name', 'app_name')
    search_fields = ('display_name', 'app_name')
    ordering = ('display_name', )


@admin.register(UserProfile)
class UserProfileAdmin(CustomURLModelAdmin):
    list_display = ('user', 'position', 'get_roles')
    search_fields = ('user__first_name', 'user__last_name', 'user__email',
                     'position', 'roles__name')
    filter_horizontal = ('roles', 'app_permissions')
    custom_urls = [
        {
            'regex': r'^(?P<pk>.+)/no-hubstaff-notification/$',
            'view': NoHubstaffAccountNotificationEmailView,
            'name': 'send_no_hubstaff_notification'
        }
    ]

    def get_roles(self, obj):
        return ' | '.join([r.name for r in obj.roles.all()])
