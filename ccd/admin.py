from django.contrib import admin

from backoffice.admin import CustomURLModelAdmin
from .models import Person, URLToken
from .views import CCDNotificationEmailView, PurchaseOrderEmailView


@admin.register(Person)
class PersonAdmin(CustomURLModelAdmin):
    list_display = ('first_name', 'last_name', 'email', 'is_active')
    list_filter = ('is_active',)
    custom_urls = [
        {
            'regex': r'^(?P<pk>.+)/send_ccd_notification/$',
            'view': CCDNotificationEmailView,
            'name': 'send_ccd_notification'
        },
        {
            'regex': r'(?P<pk>.+)/send_purchase_order/',
            'view': PurchaseOrderEmailView,
            'name': 'send_purchase_order'
        }
    ]


@admin.register(URLToken)
class URLTokenAdmin(admin.ModelAdmin):
    list_display = ('id', 'person', 'valid_until')
