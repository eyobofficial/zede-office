from django.contrib import admin

from backoffice.admin import CustomURLModelAdmin

from .models import PlanningRequest, Delivery, Schedule
from .views import NewPlanningRequestNotification, \
    TrainerScheduleNotification, PlanningRequestStatusChangeNotification, \
    NewOptionPlanningRequestNotification, OptionTrainerScheduleNotification, \
    FinalizeConfirmationNotification, \
    OptionPlanningRequestDeleteNotification, FinalizeConfirmationReminder, \
    PendingPlanningOptionReminder, OverdueNotification, \
    TrainerConfirmationReminder, AdminConfirmationReminder


@admin.register(PlanningRequest)
class PlanningRequestAdmin(CustomURLModelAdmin):
    list_display = ['type', 'status', 'is_confirmed_by_admin', 'created_by']
    list_editable = ['is_confirmed_by_admin']
    list_filter = ['type', 'status', 'is_confirmed_by_admin', 'created_by']
    custom_urls = [
        {
            'regex': r'^(?P<pk>.+)/send_new_planning_request_email/$',
            'view': NewPlanningRequestNotification,
            'name': 'send_new_planning_request_notification'
        },
        {
            'regex': r'^(?P<pk>.+)/send_new_option_planning_request_email/$',
            'view': NewOptionPlanningRequestNotification,
            'name': 'send_new_option_planning_request_notification'
        },
        {
            'regex': r'^(?P<pk>.+)/send_planning_request_changed_email/$',
            'view': PlanningRequestStatusChangeNotification,
            'name': 'send_planning_request_changed_notification'
        },
        {
            'regex': r'^(?P<pk>.+)/send_admin_confirmation_reminder_email/$',
            'view': AdminConfirmationReminder,
            'name': 'send_admin_confirmation_reminder_email'
        },
        {
            'regex': r'^(?P<pk>.+)/send_planning_request_delete_email/$',
            'view': OptionPlanningRequestDeleteNotification,
            'name': 'send_planning_request_delete_notification'
        },
        {
            'regex': r'^(?P<pk>.+)/send_pending_option_reminder_email/$',
            'view': PendingPlanningOptionReminder,
            'name': 'send_pending_option_reminder'
        },
        {
            'regex': r'^(?P<pk>.+)/send_overdue_notification_email/$',
            'view': OverdueNotification,
            'name': 'send_overdue_notification'
        }
    ]


@admin.register(Delivery)
class DeliveryAdmin(admin.ModelAdmin):
    list_display = ['organization', 'planning_request', 'date', 'time']
    list_filter = ['date', 'planning_request']
    search_fields = ['organization', 'description', 'notes']


@admin.register(Schedule)
class ScheduleAdmin(CustomURLModelAdmin):
    list_display = ['delivery', 'user', 'is_confirmed']
    list_filter = ['is_confirmed']
    custom_urls = [
        {
            'regex': r'^(?P<pk>.+)/send_confirmation_email/$',
            'view': TrainerScheduleNotification,
            'name': 'send_confirmation_email'
        },
        {
            'regex': r'^(?P<pk>.+)/send_trainer_reminder_email/$',
            'view': TrainerConfirmationReminder,
            'name': 'send_trainer_reminder_email'
        },
        {
            'regex': r'^(?P<pk>.+)/send_option_confirmation_email/$',
            'view': OptionTrainerScheduleNotification,
            'name': 'send_option_confirmation_email'
        },
        {
            'regex': r'^(?P<pk>.+)/send_finalize_confirmation_email/$',
            'view': FinalizeConfirmationNotification,
            'name': 'send_finalize_confirmation_email'
        },
        {
            'regex': r'^(?P<pk>.+)/send_finalize_confirmation_reminder/$',
            'view': FinalizeConfirmationReminder,
            'name': 'send_finalize_confirmation_reminder_email'
        }
    ]
