from DebatNL_BackOffice.celery import app
from shared.models import Training
from handouts.emails.reminders import MismatchingDeliveriesNotification, \
    UnknownDeliveriesNotification
from handouts.models import HandoutRequest

from .generator import HandoutRequestGenerator


@app.task
def generate_handout_requests():
    """
    Create `HandoutRequest` instances from `jotform` submissions.
    """
    HandoutRequestGenerator().generate()


@app.task
def notify_mismatched_deliveries():
    status = HandoutRequest.MISMATCHING_DELIVERIES
    h_requests = HandoutRequest.objects.filter(status=status).all()
    unique_combos = h_requests.values('trainer', 'training_date').distinct()

    for combo in unique_combos:
        kwargs = {
            'trainer': combo['trainer'],
            'training_date':  combo['training_date']
        }
        first_h_request = h_requests.filter(**kwargs).first()
        MismatchingDeliveriesNotification(first_h_request).send()


@app.task
def notify_unknown_deliveries():
    status = HandoutRequest.UNKNOWN_DELIVERIES
    requests = HandoutRequest.objects.filter(status=status).all()
    trainings = Training.objects.all()

    deliveries = []
    for r in requests:
        deliveries += [d for d in r.deliveries.all()
                       if r.trainer in d.get_trainer_names()]

    unknown_deliveries = set()
    for delivery in deliveries:
        names = [t.name.lower() in delivery.product.lower() for t in trainings]
        if not any(names):
            unknown_deliveries.add(delivery)

    for unknown_delivery in unknown_deliveries:
        UnknownDeliveriesNotification(unknown_delivery).send()
