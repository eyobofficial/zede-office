from django.conf.urls import url
from django.contrib.auth.decorators import login_required

from debatnl_online.views.learning_goals.email_templates import \
    EmailTemplateCreate, EmailTemplateUpdate, EmailTemplateDelete

urlpatterns = [
    url(
        r'^create/$',
        login_required(EmailTemplateCreate.as_view()),
        name='create'
    ),
    url(
        r'^update/(?P<pk>[-\w]+)/$',
        login_required(EmailTemplateUpdate.as_view()),
        name='update'
    ),
    url(
        r'^delete/(?P<pk>[-\w]+)/$',
        login_required(EmailTemplateDelete.as_view()),
        name='delete'
    )
]
