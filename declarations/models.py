import uuid
from decimal import Decimal, InvalidOperation
from unicodedata import normalize

import pendulum
from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.validators import MinValueValidator as MinValue, \
    MaxValueValidator as MaxValue
from django.db import models
from django.forms import model_to_dict
from django.utils.translation import ugettext_lazy as _

from declarations.googlemap.directions import GoogleMapAPIClient
from declarations.managers import ExternalMeetingManager, \
    InternalMeetingManager, ExpenseManager
from schedules.models import VacationPeriod
from webcrm.models import Delivery

User = get_user_model()


def hash_location(instance, filename):
    filename = normalize('NFKD', filename).encode('ascii', 'ignore')
    filename = filename.decode('UTF-8')
    return f'uploads/declarations/{uuid.uuid4()}/{filename}'


class Declaration(models.Model):
    # Status choices
    OPEN = 'OPEN'
    SUBMITTED = 'SUBMITTED'
    NEED_CHANGES = 'NEED CHANGES'
    IN_PROGRESS = 'IN PROGRESS'

    STATUS_CHOICES = (
        (OPEN, 'Open'),
        (SUBMITTED, 'Ingediend'),
        (NEED_CHANGES, 'Aanpassen'),
        (IN_PROGRESS, 'In behandeling')
    )

    # Month Choices
    JANUARY = 1
    FEBRUARY = 2
    MARCH = 3
    APRIL = 4
    MAY = 5
    JUNE = 6
    JULY = 7
    AUGUST = 8
    SEPTEMBER = 9
    OCTOBER = 10
    NOVEMBER = 11
    DECEMBER = 12

    MONTH_CHOICES = (
        (JANUARY, 'Januari'),
        (FEBRUARY, 'Februari'),
        (MARCH, 'Maart'),
        (APRIL, 'April'),
        (MAY, 'Mei'),
        (JUNE, 'Juni'),
        (JULY, 'Juli'),
        (AUGUST, 'Augustus'),
        (SEPTEMBER, 'September'),
        (OCTOBER, 'Oktober'),
        (NOVEMBER, 'November'),
        (DECEMBER, 'December')
    )

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    month = models.IntegerField(choices=MONTH_CHOICES)
    year = models.IntegerField()
    status = models.CharField(
        max_length=30,
        choices=STATUS_CHOICES,
        default=OPEN
    )
    billable_hourly_rate = models.DecimalField(default=1, max_digits=8,
                                               decimal_places=2, blank=True)
    internal_hourly_rate = models.DecimalField(default=1, max_digits=8,
                                               decimal_places=2, blank=True)
    work_satisfaction = models.IntegerField(null=True, blank=True)
    company_satisfaction = models.IntegerField(null=True, blank=True)
    feedback = models.TextField(blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ('-year', 'month')
        default_related_name = 'declarations'
        unique_together = ('user', 'month', 'year')

    def __str__(self):
        return f'{self.month}-{self.year} | {self.status}'

    def get_related_vacation_periods(self):
        month = self.month
        year = self.year
        periods = VacationPeriod.objects.filter(user=self.user).all()
        return [p for p in periods if p.is_approved_in_month(month, year)]

    def get_total_hours(self):
        total_hours = Decimal(0)

        for external_meeting in self.external_meetings.all():
            total_hours += external_meeting.get_total_declarable_hours()

        for internal_meeting in self.internal_meetings.all():
            total_hours += internal_meeting.get_total_declarable_hours()

        for item in self.elearning_items.all():
            total_hours += item.get_amount()

        try:
            total_hours += self.hubstaff_item.get_total_billable_hour()
        except HubstaffItem.DoesNotExist:
            pass

        return round(total_hours, 2)

    def get_total_travel_costs(self):
        total_costs = Decimal(0)
        rate = Decimal(0.19)

        for external_meeting in self.external_meetings.all():
            distance = 0
            if external_meeting.travel_expense:
                distance = external_meeting.travel_expense.distance
            total_costs += distance if distance else 0

        for internal_meeting in self.internal_meetings.all():
            distance = 0
            if internal_meeting.travel_expense:
                distance = internal_meeting.travel_expense.distance
            total_costs += distance if distance else 0

        return round(total_costs * rate, 2)

    def get_total_costs(self):
        total_costs = Decimal(0)
        for expense in self.expenses.non_allowances():
            total_costs += expense.amount
        return round(total_costs, 2)

    def get_total_allowances(self):
        total_costs = Decimal(0)
        for allowance in self.expenses.allowances():
            total_costs += allowance.amount
        return round(total_costs, 2)

    def get_total_bonuses(self):
        total_bonuses = Decimal(0)
        for bonus in self.employee_bonuses.all():
            total_bonuses += bonus.amount

        for bonus in self.client_bonuses.all():
            total_bonuses += bonus.amount

        return round(total_bonuses, 2)

    def get_phone_compensation(self):
        compensation = Decimal(self.external_meetings.count() * 2)
        return round(compensation, 2)

    def get_paid_vacations_in_hours(self):
        hours = Decimal(0)
        vacations = self.get_related_vacation_periods()
        for vacation in vacations:
            if vacation.type == VacationPeriod.PAID:
                hours += vacation.amount_hours
        return round(hours, 2)

    def get_unpaid_vacations_in_hours(self):
        hours = Decimal(0)
        vacations = self.get_related_vacation_periods()
        for vacation in vacations:
            if vacation.type == VacationPeriod.UNPAID:
                hours += vacation.amount_hours
        return round(hours, 2)

    @staticmethod
    def get_month_by_name(month_name):
        for value, name in Declaration.MONTH_CHOICES:
            if name == month_name:
                return value, name


class ExternalMeeting(models.Model):
    # Type Choices
    TRAINING = 'Training/Gespreksleiding'
    SCAN = 'Scan/Voorgesprek/Annulering'
    COACHING = 'Coaching'
    ZIEK_70 = 'Ziek 70%'
    ZIEK_WACHTDAG = 'Ziek wachtdag'

    TYPE_CHOICES = (
        (TRAINING, 'Training/Gespreksleiding'),
        (SCAN, 'Scan/Voorgesprek/Annulering'),
        (COACHING, 'Coaching'),
        (ZIEK_70, 'Ziek 70%'),
        (ZIEK_WACHTDAG, 'Ziek wachtdag')
    )

    date = models.DateField(null=True, blank=True)
    organization = models.CharField(max_length=120, default='', blank='')
    type = models.CharField(max_length=30, choices=TYPE_CHOICES, default='')
    write_period = models.DecimalField(max_digits=8, decimal_places=2,
                                       default=0, blank=True)
    start_time = models.TimeField(null=True, blank=True)
    end_time = models.TimeField(null=True, blank=True)
    travel_expense = models.OneToOneField('TravelExpense', null=True,
                                          blank=True,
                                          on_delete=models.SET_NULL)
    declaration = models.ForeignKey(Declaration, on_delete=models.CASCADE)
    webcrm_delivery_id = models.IntegerField(null=True, blank=True)

    objects = ExternalMeetingManager()

    class Meta:
        ordering = ('-date',)
        default_related_name = 'external_meetings'

    def __str__(self):
        return f'{self.organization} ({self.date})'

    def get_declarable_travel_time(self):
        travel_time = 0
        if self.travel_expense:
            int_hr = self.declaration.internal_hourly_rate
            bill_hr = self.declaration.billable_hourly_rate
            rate = int_hr / bill_hr if all([int_hr, bill_hr]) else 0
            total_travel_time = self.travel_expense.duration
            travel_time = (total_travel_time - 2) * rate
            travel_time = travel_time if travel_time > 0 else 0
        return Decimal(round(travel_time, 2))

    def get_time_difference(self):
        if not self.start_time or not self.end_time:
            return 0

        start_hour = self.start_time.hour
        start_minute = self.start_time.minute

        end_hour = self.end_time.hour
        end_minute = self.end_time.minute

        start_time = pendulum.Time(hour=start_hour, minute=start_minute)
        end_time = pendulum.Time(hour=end_hour, minute=end_minute)
        time_diff = start_time.diff(end_time).in_minutes() / 60
        return Decimal(round(time_diff, 2))

    def get_total_declarable_hours(self):
        time_diff = self.get_time_difference()
        travel_time = self.get_declarable_travel_time()
        declarable_hours = self.write_period + time_diff + travel_time
        calculations = {
            self.ZIEK_WACHTDAG: 0,
            self.SCAN: declarable_hours,
            self.COACHING: Decimal(0.5) + declarable_hours,
            self.ZIEK_70: Decimal(0.7) * declarable_hours,
            self.TRAINING: 1 + declarable_hours
        }
        return round(calculations.get(self.type, 0), 2)

    def is_start_time_same_for_all_trainers(self):
        if self.webcrm_delivery_id:
            kwargs = {'webcrm_delivery_id': self.webcrm_delivery_id}
            meetings = ExternalMeeting.objects.filter(**kwargs).all()

            if meetings.count() > 0:
                start_time = meetings.first().start_time

                for meeting in meetings:
                    if start_time != meeting.start_time:
                        return False
        return True

    def is_end_time_same_for_all_trainers(self):
        if self.webcrm_delivery_id:
            kwargs = {'webcrm_delivery_id': self.webcrm_delivery_id}
            meetings = ExternalMeeting.objects.filter(**kwargs).all()

            if meetings.count() > 0:
                end_time = meetings.first().end_time

                for meeting in meetings:
                    if end_time != meeting.end_time:
                        return False
        return True


class InternalMeeting(models.Model):
    # Type Choices
    INTERNAL = 'Interne meeting'
    SALES = 'Salesmeeting'

    TYPE_CHOICES = (
        (INTERNAL, INTERNAL),
        (SALES, SALES)
    )

    type = models.CharField(max_length=30, choices=TYPE_CHOICES, default='')
    meeting_with = models.CharField(max_length=120, default='')
    date = models.DateField(null=True, blank=True)
    start_time = models.TimeField(null=True, blank=True)
    end_time = models.TimeField(null=True, blank=True)
    travel_expense = models.OneToOneField('TravelExpense', null=True,
                                          blank=True,
                                          on_delete=models.SET_NULL)
    declaration = models.ForeignKey(Declaration, on_delete=models.CASCADE)
    webcrm_delivery_id = models.IntegerField(null=True, blank=True)

    objects = InternalMeetingManager()

    class Meta:
        ordering = ('pk',)
        default_related_name = 'internal_meetings'

    def __str__(self):
        return f'{self.meeting_with}'

    def get_declarable_travel_time(self):
        travel_time = 0
        if self.travel_expense:
            int_hr = self.declaration.internal_hourly_rate
            bill_hr = self.declaration.billable_hourly_rate
            total_travel_time = self.travel_expense.duration
            rate = int_hr / bill_hr if all([int_hr, bill_hr]) else 0
            travel_time = (total_travel_time - 2) * rate
            travel_time = travel_time if travel_time > 0 else 0
        return Decimal(round(travel_time, 2))

    def get_time_difference(self):
        if not self.start_time or not self.end_time:
            return 0

        start_hour = self.start_time.hour
        start_minute = self.start_time.minute

        end_hour = self.end_time.hour
        end_minute = self.end_time.minute

        start_time = pendulum.Time(hour=start_hour, minute=start_minute)
        end_time = pendulum.Time(hour=end_hour, minute=end_minute)
        time_diff = start_time.diff(end_time).in_minutes() / 60
        return Decimal(round(time_diff, 2))

    def get_total_declarable_hours(self):
        time_diff = self.get_time_difference()
        travel_time = self.get_declarable_travel_time()
        internal_hr = self.declaration.internal_hourly_rate
        billable_hr = self.declaration.billable_hourly_rate

        try:
            total_rate = (time_diff * (internal_hr / billable_hr))
            total_rate = total_rate + travel_time
        except InvalidOperation:
            total_rate = 0

        return round(total_rate, 2)


class TravelExpense(models.Model):
    TRAVEL_TIME_ONLY = 'TRAVEL_TIME_ONLY'
    TRAVEL_TIME_AND_DISTANCE = 'TRAVEL_TIME_AND_DISTANCE'
    DISTANCE_ONLY = 'DISTANCE_ONLY'

    TYPE_CHOICES = (
        (TRAVEL_TIME_ONLY, 'Meer dan twee uur reistijd'),
        (TRAVEL_TIME_AND_DISTANCE,
         'Meer dan twee uur reistijd + km vergoeding'),
        (DISTANCE_ONLY, 'Alleen een km vergoeding')
    )

    type = models.CharField(max_length=30, default='', blank=True,
                            choices=TYPE_CHOICES)
    is_one_way = models.NullBooleanField()
    date = models.DateField(null=True, blank=True)
    distance = models.DecimalField(max_digits=6, decimal_places=2, default=0)
    duration = models.DecimalField(max_digits=6, decimal_places=2, default=0)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f'({self.pk}) {self.type}'

    def _construct_depart_ts(self, time):
        # Google API does not accept dates in the past. So we deliberately set
        # the day of week in the future from now. The day of the week is the
        # same as the date of the travel expense.
        now = pendulum.now(tz=settings.TIME_ZONE)
        date_of_week = self.date.toordinal() % 7 or 0
        next_date = now.next(day_of_week=date_of_week)
        depart_datetime = next_date.set(hour=time.hour, minute=time.minute)
        return int(depart_datetime.timestamp())

    def _construct_waypoints(self, routes=None):
        if not routes:
            routes = self.routes.all()

        waypoints = []
        for index, route in enumerate(routes):
            if index == 0:
                continue

            prev_route = routes[index - 1]

            depart_time = prev_route.depart_time
            return_depart_time = prev_route.return_depart_time

            depart_time = self._construct_depart_ts(depart_time)
            return_depart_time = self._construct_depart_ts(return_depart_time)

            trip_1 = {
                'origin': prev_route.location,
                'destination': route.location,
                'mode': prev_route.travel_mode,
                'departure_time': depart_time
            }
            waypoints.append(trip_1)

            if not self.is_one_way:
                trip_2 = {
                    'origin': route.location,
                    'destination': prev_route.location,
                    'mode': prev_route.travel_mode,
                    'departure_time': return_depart_time
                }
                waypoints.append(trip_2)

        return waypoints

    def _calculate_travel_time_and_distance(self, distance, duration, mode):
        if self.is_one_way and mode != TravelExpenseRoute.MODE_CAR:
            distance = 0
        return distance, duration

    def _calculate_distance_only(self, distance, mode):
        duration = 0
        if self.is_one_way and mode != TravelExpenseRoute.MODE_CAR:
            distance = 0
        return distance, duration

    def get_total_distance_and_duration(self, routes=None):
        waypoints = self._construct_waypoints(routes)
        client = GoogleMapAPIClient()

        total_distance, total_duration = 0, 0
        for waypoint in waypoints:
            mode = waypoint['mode']
            output = client.get_travel_distance_and_duration(**waypoint)
            distance, duration = output
            kwargs = {'distance': distance, 'duration': duration, 'mode': mode}

            if self.type == self.TRAVEL_TIME_ONLY:
                distance = 0
            elif self.type == self.TRAVEL_TIME_AND_DISTANCE:
                output = self._calculate_travel_time_and_distance(**kwargs)
                distance, duration = output
            else:
                kwargs.pop('duration')
                distance, duration = self._calculate_distance_only(**kwargs)

            total_distance += distance
            total_duration += duration

        total_distance = round(total_distance / 1000, 2)  # KMs
        total_duration = round(total_duration / (60 * 60), 2)  # hours
        return total_distance, total_duration


class TravelExpenseRoute(models.Model):
    MODE_CAR = 'CAR'
    MODE_BIKE = 'BIKE'
    MODE_TRANSIT = 'TRANSIT'
    MODE_WALKING = 'WALKING'

    MODE_CHOICES = (
        (MODE_CAR, 'Auto'),
        (MODE_BIKE, 'Fiets'),
        (MODE_TRANSIT, 'Transit'),
        (MODE_WALKING, 'Lopen'),
    )

    travel_mode = models.CharField(max_length=20, choices=MODE_CHOICES,
                                   default='')
    depart_time = models.TimeField(blank=True, null=True)
    return_depart_time = models.TimeField(blank=True, null=True)
    location = models.CharField(max_length=200)
    travel_expense = models.ForeignKey(TravelExpense, on_delete=models.CASCADE)

    class Meta:
        ordering = ('pk',)
        default_related_name = 'routes'

    def __str__(self):
        return f'{self.location} ({self.travel_mode})'


class AbsenceEntry(models.Model):
    PAID = 'PAID'
    UNPAID = 'UNPAID'

    TYPE_CHOICES = (
        (PAID, 'Paid'),
        (UNPAID, 'Unpaid'),
    )

    type = models.CharField(max_length=7, blank=True, default='',
                            choices=TYPE_CHOICES)
    start_date = models.DateField()
    end_date = models.DateField()
    amount_hours = models.DecimalField(default=0, decimal_places=2,
                                       max_digits=6)
    declaration = models.ForeignKey(Declaration, on_delete=models.CASCADE)

    class Meta:
        ordering = ('start_date',)
        default_related_name = 'absence_entries'

    def set_data_from_vacation_period(self, period: VacationPeriod):
        kwargs = model_to_dict(period)
        self.type = kwargs['type']
        self.start_date = kwargs['start_date']
        self.end_date = kwargs['end_date']
        self.amount_hours = kwargs['amount_hours']


class Expense(models.Model):
    description = models.CharField(max_length=200, blank=True, default='')
    amount = models.DecimalField(default=0, decimal_places=2, max_digits=6)
    file = models.FileField(upload_to=hash_location, null=True, blank=True)
    declaration = models.ForeignKey(Declaration, on_delete=models.CASCADE)
    date = models.DateField(null=True, blank=True)
    internal_meetings = models.ManyToManyField(InternalMeeting, blank=True)
    external_meetings = models.ManyToManyField(ExternalMeeting, blank=True)

    # Custom managers
    objects = ExpenseManager()

    class Meta:
        default_related_name = 'expenses'

    def __str__(self):
        return self.description


class EmployeeBonus(models.Model):
    FIRST_CONTRACT = 'FIRST CONTRACT'
    CONTRACT_EXTENSION = 'CONTRACT EXTENSION'

    TYPE_CHOICES = (
        (FIRST_CONTRACT, 'Eerste contract'),
        (CONTRACT_EXTENSION, 'Contractverlenging'),
    )

    type = models.CharField(max_length=30, blank=True, default='',
                            choices=TYPE_CHOICES)
    name = models.CharField(max_length=200, blank=True, default='')
    amount = models.DecimalField(default=0, decimal_places=2, max_digits=6)
    declaration = models.ForeignKey(Declaration, on_delete=models.CASCADE)

    class Meta:
        default_related_name = 'employee_bonuses'

    def get_amount_based_on_type(self):
        categories = {
            self.FIRST_CONTRACT: 100,
            self.CONTRACT_EXTENSION: 150,
        }
        return categories.get(self.type, 0)

    def save(self, **kwargs):
        self.amount = self.get_amount_based_on_type()
        return super().save(**kwargs)


class ClientBonus(models.Model):
    invoice_number = models.CharField(max_length=200, blank=True, default='')
    name = models.CharField(max_length=200, blank=True, default='')
    amount = models.DecimalField(default=0, decimal_places=2, max_digits=6)
    declaration = models.ForeignKey(Declaration, on_delete=models.CASCADE)

    class Meta:
        default_related_name = 'client_bonuses'
        verbose_name = _('Client Bonus')
        verbose_name_plural = _('Client Bonuses')


class ELearningItem(models.Model):
    BEWUST_BEINVLOEDEN_INTERN = 'Bewust Beinvloeden (intern)'
    BEWUST_BEINVLOEDEN = 'Bewust Beinvloeden'
    DEBATSCAN = 'Debatscan'
    INTRO_DEEL_1 = 'Intro deel 1'
    INTRO_DEEL_2 = 'Intro deel 2'
    INTRO_DEEL_3 = 'Intro deel 3'
    INTRO_DEEL_4 = 'Intro deel 4'
    ONTSPANNEN_PRESENTEREN = 'Ontspannen Presenteren'
    POLITIEKE_DEBATVAARDIGHEDEN = 'Politieke debatvaardigheden'

    NAME_CHOICES = (
        (BEWUST_BEINVLOEDEN_INTERN, 'Bewust Beinvloeden (intern)'),
        (BEWUST_BEINVLOEDEN, 'Bewust Beinvloeden'),
        (DEBATSCAN, 'Debatscan'),
        (INTRO_DEEL_1, 'Intro deel 1'),
        (INTRO_DEEL_2, 'Intro deel 2'),
        (INTRO_DEEL_3, 'Intro deel 3'),
        (INTRO_DEEL_4, 'Intro deel 4'),
        (ONTSPANNEN_PRESENTEREN, 'Ontspannen Presenteren'),
        (POLITIEKE_DEBATVAARDIGHEDEN, 'Politieke debatvaardigheden')
    )

    name = models.CharField(max_length=120, choices=NAME_CHOICES)
    certificate = models.FileField(upload_to=hash_location)
    hours = models.DecimalField(default=0, decimal_places=2, max_digits=8)
    declaration = models.ForeignKey(Declaration, on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'E-learning item'
        verbose_name_plural = 'E-learning items'
        default_related_name = 'elearning_items'

    def __str__(self):
        return self.name

    def get_amount(self):
        int_hr = self.declaration.internal_hourly_rate
        bill_hr = self.declaration.billable_hourly_rate
        rate = int_hr / bill_hr if all([int_hr, bill_hr]) else 0
        return round(rate * self.hours, 2)

    def get_hours_based_on_name(self):
        hours = {
            self.BEWUST_BEINVLOEDEN_INTERN: 1.75,
            self.BEWUST_BEINVLOEDEN: 1.7,
            self.DEBATSCAN: 2.5,
            self.INTRO_DEEL_1: 1.5,
            self.INTRO_DEEL_2: 1.4,
            self.INTRO_DEEL_3: 4,
            self.INTRO_DEEL_4: 5,
            self.ONTSPANNEN_PRESENTEREN: 0.75,
            self.POLITIEKE_DEBATVAARDIGHEDEN: 1.5,
        }
        return hours.get(self.name, 0)

    def save(self, **kwargs):
        self.hours = self.get_hours_based_on_name()
        return super().save(**kwargs)


class HubstaffItem(models.Model):
    hours = models.IntegerField(default=0, validators=[MinValue(0)])
    minutes = models.IntegerField(default=0, validators=[MinValue(0),
                                                         MaxValue(59)])
    from_api = models.BooleanField(default=False)
    declaration = models.OneToOneField(Declaration, on_delete=models.CASCADE,
                                       related_name='hubstaff_item')

    def get_time_in_decimal(self):
        return Decimal(self.hours + (self.minutes / 60))

    def get_total_billable_hour(self):
        int_hr = self.declaration.internal_hourly_rate
        bill_hr = self.declaration.billable_hourly_rate
        rate = int_hr / bill_hr if all([int_hr, bill_hr]) else 0
        return round(self.get_time_in_decimal() * rate, 2)


class FeedbackItem(models.Model):
    feedback = models.TextField()
    declaration = models.ForeignKey(Declaration, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE)
