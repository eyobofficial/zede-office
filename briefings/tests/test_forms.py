from django.test import TestCase
from django.contrib.auth import get_user_model

from briefings.forms import BriefingForm, BriefingFileFormSet, \
    DeliverableFormSet
from webcrm.factories import UserFactory
from .factories import DeliverableFactory, BriefingFactory, BriefingFileFactory


class BriefingFormTest(TestCase):

    def test_form_has_deliverable_fields(self):
        """
        Test the existance of custom deliverables
        field
        """
        form = BriefingForm()
        self.assertTrue('deliverables' in form.fields)


class DeliverableFormSetTest(TestCase):

    def setUp(self):
        self.user = get_user_model().objects.create_user(
            username='testuser',
            email='test@email.com',
            password='password1234'
        )
        self.user.save()

    def test_save_method_for_a_single_deliverable(self):
        """
        Test the formset save() method for a single
        deliverable
        """
        briefing = BriefingFactory()
        form_data = {
            # management_form and form data
            'deliveries-INITIAL_FORMS': '0',
            'deliveries-TOTAL_FORMS': '1',
            'deliveries-MAX_NUM_FORMS': '',
        }
        formset = DeliverableFormSet(data=form_data, instance=briefing)
        self.assertTrue(formset.is_valid())

    def test_save_method_for_multiple_deliverables(self):
        """
        Test the formset save() method for multiple
        deliverable
        """
        briefing = BriefingFactory()
        responsible = UserFactory(email=self.user.email)
        deliverables = DeliverableFactory.create_batch(
            5,
            delivery__responsible=responsible
        )
        form_data = {
            # management_form data
            'deliveries-INITIAL_FORMS': '0',
            'deliveries-TOTAL_FORMS': '1',
            'deliveries-MAX_NUM_FORMS': '',
        }
        formset = DeliverableFormSet(data=form_data, instance=briefing)
        formset.save(deliverable_list=deliverables)
        self.assertTrue(formset.is_valid())


class BriefingFileFormSetTest(TestCase):

    def test_save_method_for_a_single_file(self):
        """
        Test the formset save() method for a single file
        """
        briefing = BriefingFactory()
        briefing_file = BriefingFileFactory(file=None)

        form_data = {
            # management_form and form data
            'files-INITIAL_FORMS': '0',
            'files-TOTAL_FORMS': '1',
            'files-MAX_NUM_FORMS': '',
            'files-0-file_name': briefing_file.file_name,
            'files-0-file': briefing_file.file,
        }
        formset = BriefingFileFormSet(data=form_data, instance=briefing)
        self.assertTrue(formset.is_valid())
