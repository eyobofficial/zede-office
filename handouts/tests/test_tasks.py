import datetime

import mock

from django.test import TestCase

from shared.tests.factories import TrainingFactory

from handouts.models import HandoutRequest
from handouts.tasks import generate_handout_requests, notify_mismatched_deliveries, notify_unknown_deliveries
from handouts.tests.factories import HandoutRequestFactory
from webcrm.factories import DeliveryFactory


class GenerateHandoutRequestTest(TestCase):
    """
    Tests for the `generate_handout_requests` task
    """
    @mock.patch('handouts.tasks.HandoutRequestGenerator.generate')
    def test_generate_handout_request_task(self, mock_obj):
        """
        Ensure task calls `generate` method to create HandoutRequests
        """
        generate_handout_requests()
        self.assertEqual(mock_obj.call_count, 1)


class NotifyMismatchedDeliveriesTaskTest(TestCase):

    @mock.patch('handouts.emails.reminders.'
                'MismatchingDeliveriesNotification.send')
    def test_identical_delivery_in_different_handout_request(self, mock_obj):
        dt = datetime.datetime(2019, 5, 1)
        delivery = DeliveryFactory(custom5='Bob', order_date=dt)
        status = HandoutRequest.MISMATCHING_DELIVERIES
        kwargs = {'trainer': 'Bob', 'training_date': dt}
        handout_request_1 = HandoutRequestFactory(status=status, **kwargs)
        handout_request_1.deliveries.add(delivery)
        handout_request_2 = HandoutRequestFactory(status=status, **kwargs)
        handout_request_2.deliveries.add(delivery)
        notify_mismatched_deliveries()
        self.assertEqual(mock_obj.call_count, 1)

    @mock.patch('handouts.emails.reminders.'
                'MismatchingDeliveriesNotification.send')
    def test_multiple_deliveries_with_own_hand_request(self, mock_obj):
        status = HandoutRequest.MISMATCHING_DELIVERIES
        handout_request_1 = HandoutRequestFactory(status=status)
        handout_request_1.deliveries.add(DeliveryFactory())
        handout_request_2 = HandoutRequestFactory(status=status)
        handout_request_2.deliveries.add(DeliveryFactory())
        notify_mismatched_deliveries()
        self.assertEqual(mock_obj.call_count, 2)

    @mock.patch('handouts.emails.reminders.'
                'MismatchingDeliveriesNotification.send')
    def test_multiple_deliveries_with_same_hand_request(self, mock_obj):
        dt_1 = datetime.datetime(2019, 5, 1)
        delivery_1 = DeliveryFactory(custom5='Bob', order_date=dt_1)
        kwargs = {'trainer': 'Bob', 'training_date': dt_1}

        dt_2 = datetime.datetime(2019, 5, 2)
        delivery_2 = DeliveryFactory(custom5='John', order_date=dt_2)

        status = HandoutRequest.MISMATCHING_DELIVERIES
        handout_request = HandoutRequestFactory(status=status, **kwargs)
        handout_request.deliveries.add(delivery_1)
        handout_request.deliveries.add(delivery_2)
        notify_mismatched_deliveries()
        self.assertEqual(mock_obj.call_count, 1)

    @mock.patch('handouts.emails.reminders.'
                'MismatchingDeliveriesNotification.send')
    def test_hand_requests_with_different_status(self, mock_obj):
        choices = HandoutRequest.STATUS_CHOICES
        excluded_choice = HandoutRequest.MISMATCHING_DELIVERIES
        statuses = [c[0] for c in choices if c[0] != excluded_choice]

        delivery = DeliveryFactory()

        for status in statuses:
            handout_request = HandoutRequestFactory(status=status)
            handout_request.deliveries.add(delivery)

            with self.subTest(status=status):
                notify_mismatched_deliveries()
                self.assertEqual(mock_obj.call_count, 0)


class NotifyUnknownDeliveriesTaskTest(TestCase):

    def setUp(self):
        TrainingFactory(name='Simulatie')

    @mock.patch('handouts.emails.reminders.UnknownDeliveriesNotification.send')
    def test_identical_delivery_in_different_handout_request(self, mock_obj):
        delivery = DeliveryFactory(custom5='Bob')
        status = HandoutRequest.UNKNOWN_DELIVERIES
        handout_request_1 = HandoutRequestFactory(trainer='Bob', status=status)
        handout_request_1.deliveries.add(delivery)
        handout_request_2 = HandoutRequestFactory(trainer='Bob', status=status)
        handout_request_2.deliveries.add(delivery)
        notify_unknown_deliveries()
        self.assertEqual(mock_obj.call_count, 1)

    @mock.patch('handouts.emails.reminders.UnknownDeliveriesNotification.send')
    def test_multiple_deliveries_with_own_hand_request(self, mock_obj):
        status = HandoutRequest.UNKNOWN_DELIVERIES
        handout_request_1 = HandoutRequestFactory(trainer='Bob', status=status)
        handout_request_1.deliveries.add(DeliveryFactory(custom5='Bob'))
        handout_request_2 = HandoutRequestFactory(trainer='Bob', status=status)
        handout_request_2.deliveries.add(DeliveryFactory(custom5='Bob'))
        notify_unknown_deliveries()
        self.assertEqual(mock_obj.call_count, 2)

    @mock.patch('handouts.emails.reminders.UnknownDeliveriesNotification.send')
    def test_multiple_deliveries_with_same_hand_request(self, mock_obj):
        status = HandoutRequest.UNKNOWN_DELIVERIES
        handout_request = HandoutRequestFactory(trainer='Bob', status=status)
        handout_request.deliveries.add(DeliveryFactory(custom5='Bob'))
        handout_request.deliveries.add(DeliveryFactory(custom5='Bob'))
        notify_unknown_deliveries()
        self.assertEqual(mock_obj.call_count, 2)

    @mock.patch('handouts.emails.reminders.UnknownDeliveriesNotification.send')
    def test_multiple_deliveries_with_one_known_product(self, mock_obj):
        status = HandoutRequest.UNKNOWN_DELIVERIES
        delivery_1 = DeliveryFactory(custom5='Bob')
        delivery_2 = DeliveryFactory(product='Simulatie', custom5='Bob')
        handout_request = HandoutRequestFactory(trainer='Bob', status=status)
        handout_request.deliveries.add(delivery_1)
        handout_request.deliveries.add(delivery_2)
        notify_unknown_deliveries()
        self.assertEqual(mock_obj.call_count, 1)

    @mock.patch('handouts.emails.reminders.UnknownDeliveriesNotification.send')
    def test_multiple_deliveries_with_known_products(self, mock_obj):
        status = HandoutRequest.UNKNOWN_DELIVERIES
        delivery_1 = DeliveryFactory(product='Simulatie', custom5='Bob')
        delivery_2 = DeliveryFactory(product='Simulatie', custom5='Bob')
        handout_request = HandoutRequestFactory(trainer='Bob', status=status)
        handout_request.deliveries.add(delivery_1)
        handout_request.deliveries.add(delivery_2)
        notify_unknown_deliveries()
        self.assertEqual(mock_obj.call_count, 0)

    @mock.patch('handouts.emails.reminders.UnknownDeliveriesNotification.send')
    def test_hand_requests_with_different_status(self, mock_obj):
        choices = HandoutRequest.STATUS_CHOICES
        excluded_choice = HandoutRequest.UNKNOWN_DELIVERIES
        statuses = [c[0] for c in choices if c[0] != excluded_choice]

        delivery = DeliveryFactory()

        for status in statuses:
            handout_request = HandoutRequestFactory(status=status)
            handout_request.deliveries.add(delivery)

            with self.subTest(status=status):
                notify_unknown_deliveries()
                self.assertEqual(mock_obj.call_count, 0)
