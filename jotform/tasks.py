from django.conf import settings

import boto3

from DebatNL_BackOffice.celery import app
from jotform.helpers import process_submission_from_webhook


@app.task
def consume_jotform_submissions_queue():
    sqs = boto3.resource(
        'sqs',
        aws_access_key_id=settings.AWS_ACCESS_KEY,
        aws_secret_access_key=settings.AWS_SECRET_KEY,
        region_name='eu-central-1'
    )
    queue = sqs.get_queue_by_name(QueueName=settings.JOT_FORM_AWS_QUEUE)

    count = max_messages = 10
    while count == max_messages:
        messages = queue.receive_messages(MaxNumberOfMessages=max_messages)

        for message in messages:
            process_submission_from_webhook(message.body)
            message.delete()

        count = len(messages)
