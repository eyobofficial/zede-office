import pendulum

from django.conf import settings
from django.test import TestCase

from webcrm.factories import OrganizationFactory, DeliveryFactory, \
    PersonFactory as WebCRMPersonFactory
from webcrm.models import Delivery

from .factories import PersonFactory as CCDPersonFactory, URLTokenFactory


class PersonModelTest(TestCase):
    """
    Tests for `Person` model
    """

    def setUp(self):
        self.person = CCDPersonFactory(
            first_name='John',
            last_name='Doe',
            email='johndoe@test.email'
        )
        self.webcrm_person = WebCRMPersonFactory(email='johndoe@test.email')

    def test_get_full_name(self):
        """
        Test the `get_full_name()` method of the `Person` model
        """
        self.assertEqual(self.person.get_full_name(), 'John Doe')

    def test_get_ccd_deliveries_method_without_related_delivery(self):
        """
        Ensure `get_ccd_deliveries` method does NOT return any delivery
        when any of the delivery instance has no an associated trainer
        with the current person email.
        """
        DeliveryFactory.create_batch(10)
        self.assertEqual(self.person.get_ccd_deliveries().count(), 0)

    def test_get_ccd_deliveries_method_with_related_delivery(self):
        """
        Ensure `get_ccd_deliveries` method returns deliveries which have
        trainers with the same email as the current person instance.
        """
        deliveries = DeliveryFactory.create_batch(10)
        for delivery in deliveries:
            delivery.trainers.add(self.webcrm_person)

        self.assertEqual(self.person.get_ccd_deliveries().count(), 10)

    def test_get_ccd_deliveries_method_with_deleted_state(self):
        """
        Ensure `get_ccd_deliveries` method excludes related deliveries
        with a deleted state.
        """
        deliveries = DeliveryFactory.create_batch(
            10,
            state=Delivery.STATE_DELETED
        )
        for delivery in deliveries:
            delivery.trainers.add(self.webcrm_person)

        self.assertEqual(self.person.get_ccd_deliveries().count(), 0)

    def test_get_ccd_deliveries_method_with_Ja_opportunity_custom5_value(self):
        """
        Ensure `get_ccd_deliveries` method excludes related deliveries
        which have a `Ja` value for `opportunity_custom5` field.
        """
        deliveries = DeliveryFactory.create_batch(10, opportunity_custom5='Ja')
        for delivery in deliveries:
            delivery.trainers.add(self.webcrm_person)

        self.assertEqual(self.person.get_ccd_deliveries().count(), 0)

    def test_get_ccd_deliveries_method_with_digitale_intake_product(self):
        """
        Ensure `get_ccd_deliveries` method exclude related deliveries
        when product field is equal to `Digitale intake`.
        """
        deliveries = DeliveryFactory.create_batch(
            10,
            product='Digitale intake'
        )
        for delivery in deliveries:
            delivery.trainers.add(self.webcrm_person)

        self.assertEqual(self.person.get_ccd_deliveries().count(), 0)

    def test_get_ccd_deliveries_method_with_digitale_outtake_product(self):
        """
        Ensure `get_ccd_deliveries` method exclude related deliveries
        when product field is equal to `Digitale outtake`.
        """
        deliveries = DeliveryFactory.create_batch(
            10,
            product='Digitale outtake'
        )
        for delivery in deliveries:
            delivery.trainers.add(self.webcrm_person)

        self.assertEqual(self.person.get_ccd_deliveries().count(), 0)

    def test_get_ccd_deliveries_method_with_debatnl_online_product(self):
        """
        Ensure `get_ccd_deliveries` method exclude related deliveries
        when product field is equal to `Debat.NL Online`.
        """
        deliveries = DeliveryFactory.create_batch(
            10,
            product='Debat.NL Online'
        )
        for delivery in deliveries:
            delivery.trainers.add(self.webcrm_person)

        self.assertEqual(self.person.get_ccd_deliveries().count(), 0)

    def test_get_ccd_deliveries_method_with_organization_id_of_755(self):
        """
        Ensure `get_ccd_deliveries` method exclude related deliveries
        when organization field is equal to `755`.
        """
        organization = OrganizationFactory(id=755)
        deliveries = DeliveryFactory.create_batch(
            10,
            organization=organization
        )
        for delivery in deliveries:
            delivery.trainers.add(self.webcrm_person)

        self.assertEqual(self.person.get_ccd_deliveries().count(), 0)


class PersonModelMonthlyCCDDeliveriesTest(TestCase):
    """
    Tests for the method `get_monthly_ccd_deliveries` of the `Person` model.
    """

    def setUp(self):
        self.person = CCDPersonFactory(email='johndoe@test.email')
        self.webcrm_person = WebCRMPersonFactory(email='johndoe@test.email')
        order_date_1 = pendulum.datetime(2019, 5, 1, tz=settings.TIME_ZONE)
        order_date_2 = pendulum.datetime(2019, 5, 31, tz=settings.TIME_ZONE)
        delivery_1 = DeliveryFactory(order_date=order_date_1)
        delivery_2 = DeliveryFactory(order_date=order_date_2)
        delivery_1.trainers.add(self.webcrm_person)
        delivery_2.trainers.add(self.webcrm_person)

    def test_current_date_is_after_26th_of_current_month(self):
        """
        Ensure `get_monthly_ccd_deliveries` method returns related deliveries
        with order date set in current month.
        """

        known = pendulum.datetime(2019, 5, 27, tz=settings.TIME_ZONE)
        with pendulum.test(known):
            deliveries_count = self.person.get_monthly_ccd_deliveries().count()
            self.assertEqual(deliveries_count, 2)

    def test_current_date_is_before_27th_of_next_month(self):
        """
        Ensure `get_monthly_ccd_deliveries` method returns related deliveries
        with order date set in current month.
        """

        known = pendulum.datetime(2019, 6, 2, tz=settings.TIME_ZONE)
        with pendulum.test(known):
            deliveries_count = self.person.get_monthly_ccd_deliveries().count()
            self.assertEqual(deliveries_count, 2)

    def test_current_date_before_26th_of_current_month(self):
        """
        Ensure `get_monthly_ccd_deliveries` method returns 0 related
        deliveries.
        """

        known = pendulum.datetime(2019, 5, 26, tz=settings.TIME_ZONE)
        with pendulum.test(known):
            deliveries_count = self.person.get_monthly_ccd_deliveries().count()
            self.assertEqual(deliveries_count, 0)


class PersonModelFutureCCDDeliveriesTest(TestCase):
    """
    Tests for the method `get_future_ccd_deliveries` of the `Person` model.
    """

    def setUp(self):
        self.person = CCDPersonFactory(email='johndoe@test.email')
        self.webcrm_person = WebCRMPersonFactory(email='johndoe@test.email')
        order_date_1 = pendulum.datetime(2019, 5, 1, tz=settings.TIME_ZONE)
        order_date_2 = pendulum.datetime(2019, 5, 31, tz=settings.TIME_ZONE)
        delivery_1 = DeliveryFactory(order_date=order_date_1)
        delivery_2 = DeliveryFactory(order_date=order_date_2)
        delivery_1.trainers.add(self.webcrm_person)
        delivery_2.trainers.add(self.webcrm_person)

    def test_current_date_is_a_month_before(self):
        """
        Ensure `get_future_ccd_deliveries` method returns related deliveries
        with order date set in the future.
        """

        known = pendulum.datetime(2019, 4, 1, tz=settings.TIME_ZONE)
        with pendulum.test(known):
            deliveries_count = self.person.get_future_ccd_deliveries().count()
            self.assertEqual(deliveries_count, 2)

    def test_current_date_is_in_between_current_month(self):
        """
        Ensure `get_future_ccd_deliveries` method returns related deliveries
        with order date set in the future.
        """

        known = pendulum.datetime(2019, 5, 5, tz=settings.TIME_ZONE)
        with pendulum.test(known):
            deliveries_count = self.person.get_future_ccd_deliveries().count()
            self.assertEqual(deliveries_count, 1)

    def test_current_date_is_a_month_after(self):
        """
        Ensure `get_future_ccd_deliveries` method returns 0 related deliveries.
        """

        known = pendulum.datetime(2019, 6, 1, tz=settings.TIME_ZONE)
        with pendulum.test(known):
            deliveries_count = self.person.get_future_ccd_deliveries().count()
            self.assertEqual(deliveries_count, 0)


class URLTokenModelTest(TestCase):
    """
    Tests for `URLToken` model
    """

    def test_save_method_with_empty_valid_until_field(self):
        """
        Test the save() method with a blank/empty `valid_until`
        field. (i.e. When new token is saved)
        """
        url_token = URLTokenFactory()
        today = pendulum.today(settings.TIME_ZONE)

        # Assertions
        self.assertEqual(url_token.valid_until, today.add(days=13).date())

    def test_save_method_with_valid_until_field(self):
        """
        Test the save() method for an instance with an already populated
        `valid_until` field. (i.e. When updating an existing token)
        """
        today = pendulum.today(settings.TIME_ZONE)
        test_date = today.add(days=10).date()
        url_token = URLTokenFactory(valid_until=test_date)

        # Assertions
        self.assertEqual(url_token.valid_until, test_date)

    def test_is_valid_method_with_a_valid_date(self):
        """
        Test the is_valid() method with a valid date
        """
        url_token = URLTokenFactory()

        # Assertions
        self.assertTrue(url_token.is_valid())

    def test_is_valid_method_with_an_expired_date(self):
        """
        Test the is_valid() method with a invalid (expired) date
        """
        date = pendulum.today(settings.TIME_ZONE).subtract(days=10).date()
        url_token = URLTokenFactory(valid_until=date)

        # Assertions
        self.assertFalse(url_token.is_valid())


