import requests


class WebCRMClient:
    _base_url = 'https://api.webcrm.com'

    def __init__(self, auth_code):
        self._auth_code = auth_code
        self._retrieve_access_token()

    def _get_headers(self):
        return {'Authorization': f'{self._token_type} {self._access_token}'}

    def _retrieve_access_token(self):
        url = f'{self._base_url}/auth/apilogin'
        response = requests.post(url, headers={'authCode': self._auth_code})
        response.raise_for_status()
        content = response.json()

        self._token_type = content['TokenType']
        self._access_token = content['AccessToken']

    def _get_with_query(self, query, page, size=500):
        url = f'{self._base_url}/Queries'
        headers = self._get_headers()
        payload = {'script': query, 'page': page, 'size': size}
        response = requests.get(url, params=payload, headers=headers)
        response.raise_for_status()
        return response.json()

    def _get_all_pages_of_items(self, query):
        items = []
        page = 1
        while True:
            content = self._get_with_query(query, page)
            items += content
            page += 1

            if len(content) == 0:
                break

        return items

    def get_organizations(self):
        query = "SELECT OrganisationId, OrganisationName, " \
                "OrganisationDivisionName, OrganisationIndustry, " \
                "OrganisationCreatedAt, OrganisationUpdatedAt " \
                "FROM Organisation"
        return self._get_all_pages_of_items(query)

    def get_persons(self):
        query = "SELECT PersonId, PersonFirstName, PersonLastName, " \
                "PersonName, PersonEmail, PersonDirectPhone, " \
                "PersonMobilePhone, PersonStatus, PersonCustom1, " \
                "PersonCustom2, PersonCustom3, PersonCreatedAt, " \
                "PersonUpdatedAt  " \
                "FROM Person"
        return self._get_all_pages_of_items(query)

    def get_deliveries(self):
        query = "SELECT DeliveryId, DeliveryOrganisationId, " \
                "DeliveryOrderDate, DeliveryProduct, DeliveryDescription, " \
                "DeliveryResponsible, DeliveryPersonId, DeliveryCustom5, " \
                "DeliveryCustom6, DeliveryCustom7, DeliveryCustom8, " \
                "DeliveryOpportunityCustom1, DeliveryOpportunityCustom2, " \
                "DeliveryOpportunityCustom3, DeliveryOpportunityCustom4, " \
                "DeliveryCreatedAt, DeliveryUpdatedAt " \
                "FROM Delivery WHERE DeliveryOrderDate >= '2019-01-01'"
        return self._get_all_pages_of_items(query)

    def get_opportunities(self):
        query = "SELECT OpportunityId, OpportunityOrganisationId, " \
                "OpportunityAssignedTo, OpportunityAssignedTo2, " \
                "OpportunityProduct, OpportunityOrderDate, " \
                "OpportunityLevel, OpportunityLevelText, OpportunityNumber, " \
                "OpportunityPersonId, OpportunityDescription, " \
                "OpportunityCreatedAt, OpportunityUpdatedAt " \
                "FROM Opportunity WHERE OpportunityOrderDate >= '2019-01-01'"
        return self._get_all_pages_of_items(query)

    def get_events(self):
        query = "SELECT EventId, EventOrganisationId, EventAssignedTo, " \
                "EventDescription, EventType, EventDateTime, EventPersonId, " \
                "EventCustom1, EventCustom2, EventParticipantIds, " \
                "EventCreatedAt, EventUpdatedAt " \
                "FROM Event WHERE EventDateTime >= '2019-10-01'"
        return self._get_all_pages_of_items(query)

    def get_users(self):
        url = f'{self._base_url}/Users/'
        headers = self._get_headers()
        response = requests.get(url, headers=headers)
        response.raise_for_status()
        data_users = response.json()

        users = []
        for data_user in data_users:
            users.append({
                'UserId': data_user['UserId'],
                'UserName': data_user['UserName'],
                'UserEmail': data_user['UserEmail'],
                'UserUpdatedAt': data_user['UserUpdatedAt'],
                'UserCreatedAt': data_user['UserCreatedAt'],
            })
        return users

    def get_opportunity_by_id(self, id):
        url = f'{self._base_url}/Opportunities/{id}'
        headers = self._get_headers()
        response = requests.get(url, headers=headers)
        response.raise_for_status()
        return response.json()

    def update_opportunity_by_id(self, id, payload):
        url = f'{self._base_url}/Opportunities/{id}'
        headers = self._get_headers()
        response = requests.put(url, json=payload, headers=headers)
        response.raise_for_status()
