from django.db.models.signals import post_save
from django.dispatch import receiver

from marketing.models import OfferTimePeriod
from offers.models import Offer


@receiver(post_save, sender=Offer)
def add_to_offer_time_period(sender, instance, created,  **kwargs):
    instance.kpi_time_periods.clear()

    if instance.type == Offer.TYPE_PHONECARE_SPRINGEST:
        month = instance.created_at.month
        year = instance.created_at.year
        kwargs = {'year': year, 'month': month}
        period, _ = OfferTimePeriod.objects.get_or_create(**kwargs)
        period.offers.add(instance)
