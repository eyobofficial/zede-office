from django.conf.urls import url
from django.contrib.auth.decorators import login_required

from briefings.views import OverviewView, DeliverableDetail, \
    BriefingCreate, BriefingUpdate, DeliverableMaxTrainerDetail, \
    DeliverableMaxTrainerUpdate, DeliverableMaxTrainerConfirm, \
    BriefingQuestionCreateView, BriefingQuestionUpdateView, \
    QuestionUpdateSuccessView

urlpatterns = [
    url(r'^$', login_required(OverviewView.as_view()), name='overview'),
    url(
        r'^deliverables/(?P<deliverable_id>[0-9]+)/form/$',
        BriefingCreate.as_view(),
        name='form'
    ),
    url(
        r'^deliverables/(?P<deliverable_id>[0-9]+)/' \
        'briefings/(?P<pk>[0-9a-f-]+)/update/$',
        BriefingUpdate.as_view(),
        name='briefing-update'
    ),
    url(
        r'^deliverables/(?P<pk>[0-9]+)/max-trainers/$',
        DeliverableMaxTrainerDetail.as_view(),
        name='deliverable-max-trainers'
    ),
    url(
        r'^deliverables/(?P<pk>[0-9]+)/max-trainers/update/$',
        DeliverableMaxTrainerUpdate.as_view(),
        name='deliverable-max-trainers-update'
    ),
    url(
        r'^deliverables/(?P<pk>[0-9]+)/max-trainers/confirm/$',
        DeliverableMaxTrainerConfirm.as_view(),
        name='deliverable-max-trainers-confirm'
    ),
    url(
        r'^deliverables/(?P<pk>[0-9]+)/$',
        DeliverableDetail.as_view(),
        name='deliverable-detail'
    ),
    url(
        r'^deliverables/(?P<pk>[0-9]+)/questions/create/$',
        BriefingQuestionCreateView.as_view(),
        name='briefing-question-create'
    ),
    url(
        r'^questions/(?P<pk>[0-9a-f-]+)/$',
        BriefingQuestionUpdateView.as_view(),
        name='briefing-question-update'
    ),
    url(
        r'^questions/answer/success/$',
        QuestionUpdateSuccessView.as_view(),
        name='question-update-success'
    )
]
