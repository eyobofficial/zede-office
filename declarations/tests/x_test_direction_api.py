from unittest.mock import patch
from django.test import TestCase

from declarations.googlemap.directions import GoogleMapAPIClient
from declarations.googlemap.exceptions import APILimitError, \
    LocationNotFoundError


class GoogleMapDirectionTests(TestCase):
    """
    Tests for `GoogleMapDirectionTests`
    """

    def setUp(self):
        self.routes = [
            {
                'location': 'loc 1',
                'mode': 'driving',
                'declarationType': 'HOUR',
                'tripType': 'ONE_WAY'
            },
            {
                'location': 'loc 2',
                'mode': 'driving',
                'declarationType': 'HOUR_KM',
                'tripType': 'TWO_WAY'
            },
            {
                'location': 'loc 3',
                'mode': 'walking',
                'declarationType': 'KM',
                'tripType': 'ONE_WAY'
            }
        ]
        self.expected_response = {
            'status': 'OK',
            'routes': [
                {
                    'legs': [
                        {
                            'distance': {'text': '1 km', 'value': 1000},
                            'duration': {'text': '50 mins', 'value': 3000}
                        }
                    ]
                }
            ]
        }

    @patch('declarations.googlemap.directions.requests.get')
    def test_make_api_call_method(self, mock_get):
        """
        Ensure `make_api_call` method assigns the JSON response to route_data
        attribute.
        """
        mock_get.return_value.json.return_value = self.expected_response
        direction = GoogleMapAPIClient(self.routes)
        direction.make_api_call('loc 1', 'driving', 'driving')

        # Assertions
        self.assertEqual(direction.route_data, self.expected_response)
        self.assertEqual(direction.routes, self.routes)

    @patch('declarations.googlemap.directions.requests.get')
    def test_raise_errors_method_when_there_is_no_error(self, mock_get):
        """
        Ensure no error is raised when the API call returns a `status` value of
        `OK`.
        """
        mock_get.return_value.json.return_value = self.expected_response
        direction = GoogleMapAPIClient(self.routes)
        direction.make_api_call('origin', 'destination', 'driving')

        # Assertions
        self.assertIsNone(direction.raise_errors())

    @patch('declarations.googlemap.directions.requests.get')
    def test_raise_errors_method_when_api_call_limit_is_reached(self, mock_get):
        """
        Ensure `APILimitError` exception is raised when the API call returns a
        `status` of `OVER_DAILY_LIMIT` value.
        """
        self.expected_response['status'] = 'OVER_DAILY_LIMIT'
        self.expected_response.pop('routes')
        mock_get.return_value.json.return_value = self.expected_response

        direction = GoogleMapAPIClient(self.routes)
        direction.make_api_call('origin', 'destination', 'driving')

        # Assertions
        self.assertRaises(APILimitError, direction.raise_errors)

    @patch('declarations.googlemap.directions.requests.get')
    def test_raise_errors_method_when_location_is_not_found(self, mock_get):
        """
        Ensure `LocationNotFoundError` is raised when the API call returns other
        status values.
        """
        self.expected_response['status'] = 'NOT_FOUND'
        self.expected_response.pop('routes')
        mock_get.return_value.json.return_value = self.expected_response

        direction = GoogleMapAPIClient(self.routes)
        direction.make_api_call('origin', 'destination', 'driving')

        # Assertions
        self.assertRaises(LocationNotFoundError, direction.raise_errors)

    @patch('declarations.googlemap.directions.requests.get')
    def test_calculate_direction_with_hour_declaration_type(self, mock_get):
        """
        Ensure `calculate_direction` returns the correct results for `HOUR`
        delaration type depending on the trip type.
        """
        mock_get.return_value.json.return_value = self.expected_response
        routes = [
            # One-way trip route
            {
                'location': 'loc 1',
                'mode': 'driving',
                'declarationType': 'HOUR',
                'tripType': 'ONE_WAY'
            },

            # Two-way trip route
            {
                'location': 'loc 1',
                'mode': 'driving',
                'declarationType': 'HOUR',
                'tripType': 'TWO_WAY'
            },
        ]

        direction = GoogleMapAPIClient(routes)
        direction.make_api_call('loc 1', 'loc 2', 'driving')

        # One-way
        distance_1, duration_1 = direction.calculate_direction(routes[0])

        # Two-way
        distance_2, duration_2 = direction.calculate_direction(routes[1])

        # Expected results
        expected_distance_1, expected_duration_1 = 0, 3000
        expected_distance_2, expected_duration_2 = 0, 6000

        # Assertions
        self.assertEqual(distance_1, expected_distance_1)
        self.assertEqual(duration_1, expected_duration_1)
        self.assertEqual(distance_2, expected_distance_2)
        self.assertEqual(duration_2, expected_duration_2)

    @patch('declarations.googlemap.directions.requests.get')
    def test_calculate_direction_with_hour_km_declaration_type(self, mock_get):
        """
        Ensure `calculate_direction` method with `HOUR_KM` declaration type
        returns the correct the result depending on the trip type & mode.
        """
        mock_get.return_value.json.return_value = self.expected_response
        routes = [
            # One-way trip with driving mode
            {
                'location': 'loc 1',
                'mode': 'driving',
                'declarationType': 'HOUR_KM',
                'tripType': 'ONE_WAY'
            },

            # One-way trip with non-driving mode
            {
                'location': 'loc 2',
                'mode': 'walking',
                'declarationType': 'HOUR_KM',
                'tripType': 'ONE_WAY'
            },

            # Two-way trip with driving mode
            {
                'location': 'loc 3',
                'mode': 'driving',
                'declarationType': 'HOUR_KM',
                'tripType': 'TWO_WAY'
            },

            # Two-way trip with non-driving mode
            {
                'location': 'loc 4',
                'mode': 'bicycling',
                'declarationType': 'HOUR_KM',
                'tripType': 'TWO_WAY'
            }
        ]

        direction = GoogleMapAPIClient(routes)
        direction.make_api_call('origin', 'destination', 'driving')

        # One way with driving mode
        distance_1, duration_1 = direction.calculate_direction(routes[0])
        expected_distance_1, expected_duration_1 = 1000, 3000

        # One way with non-driving mode
        distance_2, duration_2 = direction.calculate_direction(routes[1])
        expected_distance_2, expected_duration_2 = 0, 3000

        # Two way with driving mode
        distance_3, duration_3 = direction.calculate_direction(routes[2])
        expected_distance_3, expected_duration_3 = 2000, 6000

        # Two way with non-driving mode
        distance_4, duration_4 = direction.calculate_direction(routes[3])
        expected_distance_4, expected_duration_4 = 0, 6000

        # Assertions
        self.assertEqual(distance_1, expected_distance_1)
        self.assertEqual(duration_1, expected_duration_1)
        self.assertEqual(distance_2, expected_distance_2)
        self.assertEqual(duration_2, expected_duration_2)
        self.assertEqual(distance_3, expected_distance_3)
        self.assertEqual(duration_3, expected_duration_3)
        self.assertEqual(distance_4, expected_distance_4)
        self.assertEqual(duration_4, expected_duration_4)

    @patch('declarations.googlemap.directions.requests.get')
    def test_calculate_direction_with_km_declaration_type(self, mock_get):
        """
        Ensure `calculate_direction` returns the correct results for `KM`
        delaration type depending on the trip type & mode.
        """
        mock_get.return_value.json.return_value = self.expected_response
        routes = [
            # One-way trip with driving mode
            {
                'location': 'loc 1',
                'mode': 'driving',
                'declarationType': 'KM',
                'tripType': 'ONE_WAY'
            },

            # One-way trip with non-driving mode
            {
                'location': 'loc 2',
                'mode': 'transit',
                'declarationType': 'KM',
                'tripType': 'ONE_WAY'
            },

            # Two-way trip with driving mode
            {
                'location': 'loc 3',
                'mode': 'driving',
                'declarationType': 'KM',
                'tripType': 'TWO_WAY'
            },

            # Two-way trip with non-driving mode
            {
                'location': 'loc 4',
                'mode': 'walking',
                'declarationType': 'KM',
                'tripType': 'TWO_WAY'
            }
        ]

        direction = GoogleMapAPIClient(routes)
        direction.make_api_call('loc 1', 'loc 2', 'driving')

        # One-way with driving mode
        distance_1, duration_1 = direction.calculate_direction(routes[0])
        expected_distance_1, expected_duration_1 = 1000, 0

        # One-way with non-driving mode
        distance_2, duration_2 = direction.calculate_direction(routes[1])
        expected_distance_2, expected_duration_2 = 0, 0

        # Two-way with driving mode
        distance_3, duration_3 = direction.calculate_direction(routes[2])
        expected_distance_3, expected_duration_3 = 2000, 0

        # Two-way with non-driving mode
        distance_4, duration_4 = direction.calculate_direction(routes[3])
        expected_distance_4, expected_duration_4 = 0, 0

        # Assertions
        self.assertEqual(distance_1, expected_distance_1)
        self.assertEqual(duration_1, expected_duration_1)
        self.assertEqual(distance_2, expected_distance_2)
        self.assertEqual(duration_2, expected_duration_2)
        self.assertEqual(distance_3, expected_distance_3)
        self.assertEqual(duration_3, expected_duration_3)
        self.assertEqual(distance_4, expected_distance_4)
        self.assertEqual(duration_4, expected_duration_4)

    @patch('declarations.googlemap.directions.requests.get')
    def test_get_direction_method(self, mock_get):
        """
        Ensure `get_direction` method returns the right answers.
        """
        mock_get.return_value.json.return_value = self.expected_response
        direction = GoogleMapAPIClient(self.routes)

        # Expected results
        expected_result = (2.0, 2.5)

        # Assertions
        self.assertEqual(direction.get_distance_and_duration(),
                         expected_result)
