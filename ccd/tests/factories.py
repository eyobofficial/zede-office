import factory

from ccd.models import Person, URLToken


class PersonFactory(factory.django.DjangoModelFactory):
    first_name = factory.Faker('first_name')
    last_name = factory.Faker('last_name')
    email = factory.Faker('email')

    class Meta:
        model = Person


class URLTokenFactory(factory.django.DjangoModelFactory):
    person = factory.SubFactory(PersonFactory)

    class Meta:
        model = URLToken
