from django.views.generic import TemplateView

from backoffice.constants import GROUP_DEBATNLONLINE
from backoffice.mixins import GroupAccessMixin
from debatnl_online.consumers.learning_goals.organizations import \
    OrganizationConsumer


class OverviewView(GroupAccessMixin, TemplateView):
    template_name = 'debatnl_online/learning_goals_overview.html'
    access_groups = [GROUP_DEBATNLONLINE]

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        consumer = OrganizationConsumer()
        context['organizations'] = consumer.retrieve_all()
        return context
