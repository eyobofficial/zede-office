import pendulum
from django.conf import settings

from django.test import Client, TestCase
from django.urls import reverse

from backoffice.constants import GROUP_SCHEDULES
from schedules.models import Timetable
from schedules.tests.factories import TimetableFactory
from users.models import AppPermission
from users.tests.factories import UserFactory


class OverviewViewTest(TestCase):
    """
    Tests for `AvailabilityView` view
    """
    fixtures = ['app_permissions']

    def setUp(self):
        self.user = UserFactory()
        app_permission = AppPermission.objects.get(app_name=GROUP_SCHEDULES)
        self.user.profile.app_permissions.add(app_permission)
        self.client = Client()
        self.url = reverse('schedules:overview')
        self.template = 'schedules/base.html'

    def test_request_as_anonymous_user(self):
        """
        Test a request to the `AvailabilityView` view from anonymous
        user
        """
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 302)

    def test_overview_with_timetables(self):
        """
        Test a request to the `AvailabilityView` view from
        authenticated user
        """
        self.client.force_login(self.user)
        response = self.client.get(self.url)

        expected_weeks = 13
        expected_cells = expected_weeks * 3
        expected_total_form = f'name="form-TOTAL_FORMS" ' \
            f'value="{expected_cells}"'
        expected_initial_form = f'name="form-INITIAL_FORMS" ' \
            f'value="{expected_cells}"'

        # Assertions
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, self.template)
        self.assertContains(response, f'"{Timetable.AVAILABLE}"',
                            count=expected_cells * 7)
        self.assertContains(response, f'"{Timetable.MORNING}"',
                            count=expected_weeks)
        self.assertContains(response, f'"{Timetable.AFTERNOON}"',
                            count=expected_weeks)
        self.assertContains(response, f'"{Timetable.EVENING}"',
                            count=expected_weeks)

        self.assertContains(response, expected_total_form, count=1)
        self.assertContains(response, expected_initial_form, count=1)

    def test_success_message(self):
        today = pendulum.today(tz=settings.TIME_ZONE)
        year, week, day = today.isocalendar()
        timetable = TimetableFactory(week=week, year=year, user=self.user)
        period_id = Timetable.get_period_id(timetable.year, timetable.week)

        form_data = {
            # management_form
            'form-INITIAL_FORMS': '1',
            'form-TOTAL_FORMS': '1',
            'form-MAX_NUM_FORMS': '',

            # Form data 1
            'form-0-year': timetable.year,
            'form-0-week': timetable.week,
            'form-0-period_id': period_id,
            'form-0-daypart': Timetable.MORNING,
            'form-0-monday': Timetable.AVAILABLE,
            'form-0-tuesday': Timetable.AVAILABLE,
            'form-0-wednesday': Timetable.AVAILABLE,
            'form-0-thursday': Timetable.AVAILABLE,
            'form-0-friday': Timetable.AVAILABLE,
            'form-0-saturday': Timetable.AVAILABLE,
            'form-0-sunday': Timetable.AVAILABLE,
            'form-0-id': timetable.pk,
        }

        expected_text = 'Beschikbaarheid doorgegeven'

        self.client.force_login(self.user)
        response = self.client.post(self.url, form_data, follow=True)
        self.assertContains(response, expected_text, count=1)


class VacationFormStep1View(TestCase):
    """
    Tests for `VacationFormStep1View` class
    """
    fixtures = ['app_permissions']

    def setUp(self):
        self.client = Client()
        self.user = UserFactory()
        app_permission = AppPermission.objects.get(app_name=GROUP_SCHEDULES)
        self.user.profile.app_permissions.add(app_permission)
        self.url = reverse('schedules:step-1')
        self.template = 'schedules/components/modals/steps/step1.html'

    def test_request_as_anonymous_user(self):
        """
        Test request with anonymous user
        """
        response = self.client.get(self.url)

        # Assertions
        self.assertEqual(response.status_code, 302)

    def test_request_as_authenticated_user(self):
        """
        Test request with authenticated user
        """
        self.client.force_login(self.user)
        response = self.client.get(self.url)

        # Assertions
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, self.template)


class VacationFormStep2View(TestCase):
    """
    Tests for `VacationFormStep2View` class
    """
    fixtures = ['app_permissions']

    def setUp(self):
        self.client = Client()
        self.user = UserFactory()
        app_permission = AppPermission.objects.get(app_name=GROUP_SCHEDULES)
        self.user.profile.app_permissions.add(app_permission)
        self.url = reverse('schedules:step-2')
        self.template = 'schedules/components/modals/steps/step2.html'

    def test_request_as_anonymous_user(self):
        """
        Test request with anonymous user
        """
        response = self.client.get(self.url)

        # Assertions
        self.assertEqual(response.status_code, 302)

    def test_request_as_authenticated_user(self):
        """
        Test request with authenticated user
        """
        self.client.force_login(self.user)
        response = self.client.get(self.url)

        # Assertions
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, self.template)


class VacationFormStep3View(TestCase):
    """
    Tests for `VacationFormStep3View` class
    """
    fixtures = ['app_permissions']

    def setUp(self):
        self.client = Client()
        self.user = UserFactory()
        app_permission = AppPermission.objects.get(app_name=GROUP_SCHEDULES)
        self.user.profile.app_permissions.add(app_permission)
        self.url = reverse('schedules:step-3')
        self.template = 'schedules/components/modals/steps/step3.html'

    def test_request_as_anonymous_user(self):
        """
        Test request with anonymous user
        """
        response = self.client.get(self.url)

        # Assertions
        self.assertEqual(response.status_code, 302)

    def test_request_as_authenticated_user(self):
        """
        Test request with authenticated user
        """
        self.client.force_login(self.user)
        response = self.client.get(self.url)

        # Assertions
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, self.template)


class VacationFormStep4View(TestCase):
    """
    Tests for `VacationFormStep4View` class
    """
    fixtures = ['app_permissions']

    def setUp(self):
        self.client = Client()
        self.user = UserFactory()
        app_permission = AppPermission.objects.get(app_name=GROUP_SCHEDULES)
        self.user.profile.app_permissions.add(app_permission)
        self.url = reverse('schedules:step-4')
        self.template = 'schedules/components/modals/steps/step4.html'

    def test_request_as_anonymous_user(self):
        """
        Test request with anonymous user
        """
        response = self.client.get(self.url)

        # Assertions
        self.assertEqual(response.status_code, 302)

    def test_request_as_authenticated_user(self):
        """
        Test request with authenticated user
        """
        self.client.force_login(self.user)
        response = self.client.get(self.url)

        # Assertions
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, self.template)


class VacationFormStep5View(TestCase):
    """
    Tests for `VacationFormStep5View` class
    """
    fixtures = ['app_permissions']

    def setUp(self):
        self.client = Client()
        self.user = UserFactory()
        app_permission = AppPermission.objects.get(app_name=GROUP_SCHEDULES)
        self.user.profile.app_permissions.add(app_permission)
        self.url = reverse('schedules:step-5')
        self.template = 'schedules/components/modals/steps/step5.html'

    def test_request_as_anonymous_user(self):
        """
        Test request with anonymous user
        """
        response = self.client.get(self.url)

        # Assertions
        self.assertEqual(response.status_code, 302)

    def test_request_as_authenticated_user(self):
        """
        Test request with authenticated user
        """
        self.client.force_login(self.user)
        response = self.client.get(self.url)

        # Assertions
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, self.template)


class VacationFormStep6View(TestCase):
    """
    Tests for `VacationFormStep6View` class
    """
    fixtures = ['app_permissions']

    def setUp(self):
        self.client = Client()
        self.user = UserFactory()
        app_permission = AppPermission.objects.get(app_name=GROUP_SCHEDULES)
        self.user.profile.app_permissions.add(app_permission)
        self.url = reverse('schedules:step-6')
        self.template = 'schedules/components/modals/steps/step6.html'

    def test_request_as_anonymous_user(self):
        """
        Test request with anonymous user
        """
        response = self.client.get(self.url)

        # Assertions
        self.assertEqual(response.status_code, 302)

    def test_request_as_authenticated_user(self):
        """
        Test request with authenticated user
        """
        self.client.force_login(self.user)
        response = self.client.get(self.url)

        # Assertions
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, self.template)


class VacationFormStep7View(TestCase):
    """
    Tests for `VacationFormStep7View` class
    """
    fixtures = ['app_permissions']

    def setUp(self):
        self.client = Client()
        self.user = UserFactory()
        app_permission = AppPermission.objects.get(app_name=GROUP_SCHEDULES)
        self.user.profile.app_permissions.add(app_permission)
        self.url = reverse('schedules:step-7')
        self.template = 'schedules/components/modals/steps/step7.html'

    def test_request_as_anonymous_user(self):
        """
        Test request with anonymous user
        """
        response = self.client.get(self.url)

        # Assertions
        self.assertEqual(response.status_code, 302)

    def test_request_as_authenticated_user(self):
        """
        Test request with authenticated user
        """
        self.client.force_login(self.user)
        response = self.client.get(self.url)

        # Assertions
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, self.template)


class VacationFormStep8View(TestCase):
    """
    Tests for `VacationFormStep8View` class
    """
    fixtures = ['app_permissions']

    def setUp(self):
        self.client = Client()
        self.user = UserFactory()
        app_permission = AppPermission.objects.get(app_name=GROUP_SCHEDULES)
        self.user.profile.app_permissions.add(app_permission)
        self.url = reverse('schedules:step-8')
        self.template = 'schedules/components/modals/steps/step8.html'

    def test_request_as_anonymous_user(self):
        """
        Test request with anonymous user
        """
        response = self.client.get(self.url)

        # Assertions
        self.assertEqual(response.status_code, 302)

    def test_request_as_authenticated_user(self):
        """
        Test request with authenticated user
        """
        self.client.force_login(self.user)
        response = self.client.get(self.url)

        # Assertions
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, self.template)


class VacationFormStep9View(TestCase):
    """
    Tests for `VacationFormStep9View` class
    """
    fixtures = ['app_permissions']

    def setUp(self):
        self.client = Client()
        self.user = UserFactory()
        app_permission = AppPermission.objects.get(app_name=GROUP_SCHEDULES)
        self.user.profile.app_permissions.add(app_permission)
        self.url = reverse('schedules:step-9')
        self.template = 'schedules/components/modals/steps/step9.html'

    def test_request_as_anonymous_user(self):
        """
        Test request with anonymous user
        """
        response = self.client.get(self.url)

        # Assertions
        self.assertEqual(response.status_code, 302)

    def test_request_as_authenticated_user(self):
        """
        Test request with authenticated user
        """
        self.client.force_login(self.user)
        response = self.client.get(self.url)

        # Assertions
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, self.template)


class VacationFormStep10View(TestCase):
    """
    Tests for `VacationFormStep10View` class
    """
    fixtures = ['app_permissions']

    def setUp(self):
        self.client = Client()
        self.user = UserFactory()
        app_permission = AppPermission.objects.get(app_name=GROUP_SCHEDULES)
        self.user.profile.app_permissions.add(app_permission)
        self.url = reverse('schedules:step-10')
        self.template = 'schedules/components/modals/steps/step10.html'

    def test_request_as_anonymous_user(self):
        """
        Test request with anonymous user
        """
        response = self.client.get(self.url)

        # Assertions
        self.assertEqual(response.status_code, 302)

    def test_request_as_authenticated_user(self):
        """
        Test request with authenticated user
        """
        self.client.force_login(self.user)
        response = self.client.get(self.url)

        # Assertions
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, self.template)


class VacationFormStep11View(TestCase):
    """
    Tests for `VacationFormStep11View` class
    """
    fixtures = ['app_permissions']

    def setUp(self):
        self.client = Client()
        self.user = UserFactory()
        app_permission = AppPermission.objects.get(app_name=GROUP_SCHEDULES)
        self.user.profile.app_permissions.add(app_permission)
        self.url = reverse('schedules:step-11')
        self.template = 'schedules/components/modals/steps/step11.html'

    def test_request_as_anonymous_user(self):
        """
        Test request with anonymous user
        """
        response = self.client.get(self.url)

        # Assertions
        self.assertEqual(response.status_code, 302)

    def test_request_as_authenticated_user(self):
        """
        Test request with authenticated user
        """
        self.client.force_login(self.user)
        response = self.client.get(self.url)

        # Assertions
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, self.template)
