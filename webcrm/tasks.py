from django.conf import settings

from DebatNL_BackOffice.celery import app
from webcrm.client import WebCRMClient
from webcrm.models import Organization, Person, Delivery, User, Opportunity, \
    Event
from webcrm.utilities import convert_api_data_to_model_data


@app.task
def sync_organizations_with_webcrm():
    client = WebCRMClient(settings.WEBCRM_AUTH_CODE)
    data_organizations = client.get_organizations()
    organizations = Organization.objects.all()

    for data in data_organizations:
        data = convert_api_data_to_model_data('Organisation', data)
        kwargs = {'pk': data['id'], 'defaults': data}
        organization, _ = Organization.objects.update_or_create(**kwargs)

        if organization.state == Organization.STATE_DELETED:
            organization.state = Organization.STATE_AVAILABLE
            organization.save()

        organizations = organizations.exclude(pk=organization.pk)

    organizations.update(state=Organization.STATE_DELETED)


@app.task
def sync_persons_with_webcrm():
    client = WebCRMClient(settings.WEBCRM_AUTH_CODE)
    data_persons = client.get_persons()
    persons = Person.objects.all()

    for data in data_persons:
        data = convert_api_data_to_model_data('Person', data)
        kwargs = {'pk': data['id'], 'defaults': data}
        person, _ = Person.objects.update_or_create(**kwargs)

        if person.state == Person.STATE_DELETED:
            person.state = Person.STATE_AVAILABLE
            person.save()

        persons = persons.exclude(pk=person.pk)

    persons.update(state=Person.STATE_DELETED)


@app.task
def sync_users_with_webcrm():
    client = WebCRMClient(settings.WEBCRM_AUTH_CODE)
    data_users = client.get_users()
    users = User.objects.all()

    for data in data_users:
        data = convert_api_data_to_model_data('User', data)
        kwargs = {'pk': data['id'], 'defaults': data}
        user, _ = User.objects.update_or_create(**kwargs)

        if user.state == User.STATE_DELETED:
            user.state = User.STATE_AVAILABLE
            user.save()

        users = users.exclude(pk=user.pk)

    users.update(state=Person.STATE_DELETED)


@app.task
def sync_deliveries_with_webcrm():
    client = WebCRMClient(settings.WEBCRM_AUTH_CODE)
    data_deliveries = client.get_deliveries()
    deliveries = Delivery.objects.all()

    for data in data_deliveries:
        data = convert_api_data_to_model_data('Delivery', data)
        kwargs = {'pk': data['id'], 'defaults': data}
        delivery, _ = Delivery.objects.update_or_create(**kwargs)
        delivery.assign_trainers()

        if delivery.state == Delivery.STATE_DELETED:
            delivery.state = Delivery.STATE_AVAILABLE
            delivery.save()

        deliveries = deliveries.exclude(pk=delivery.pk)

    deliveries.update(state=Delivery.STATE_DELETED)


@app.task
def sync_opportunities_with_webcrm():
    client = WebCRMClient(settings.WEBCRM_AUTH_CODE)
    data_opportunities = client.get_opportunities()
    opportunities = Opportunity.objects.all()

    for data in data_opportunities:
        data = convert_api_data_to_model_data('Opportunity', data)
        kwargs = {'pk': data['id'], 'defaults': data}
        opportunity, _ = Opportunity.objects.update_or_create(**kwargs)

        if opportunity.state == Opportunity.STATE_DELETED:
            opportunity.state = Opportunity.STATE_AVAILABLE
            opportunity.save()

        opportunities = opportunities.exclude(pk=opportunity.pk)

    opportunities.update(state=Opportunity.STATE_DELETED)


@app.task
def sync_events_with_webcrm():
    client = WebCRMClient(settings.WEBCRM_AUTH_CODE)
    data_events = client.get_events()
    events = Event.objects.all()

    for data in data_events:
        data = convert_api_data_to_model_data('Event', data)
        kwargs = {'pk': data['id'], 'defaults': data}
        event, _ = Event.objects.update_or_create(**kwargs)
        event.assign_participants()

        if event.state == Event.STATE_DELETED:
            event.state = Event.STATE_AVAILABLE
            event.save()

        events = events.exclude(pk=event.pk)

    events.update(state=Opportunity.STATE_DELETED)
