from django.conf import settings
from django.contrib.auth.models import User
from django.urls import reverse

from handouts.emails.bases import BaseHandoutEmail
from handouts.models import HandoutToken
from shared.enums import RoleName
from shared.models import Training


class MismatchingDeliveriesNotification(BaseHandoutEmail):
    template_name = 'email_7_mismatching_deliveries.html'

    def __init__(self, handout_request):
        self.recipient = User.objects.filter(
            profile__roles__name=RoleName.ADMIN.value,
            is_active=True
        ).first()
        self.handout_request = handout_request
        name = self.handout_request.trainer
        dt = self.handout_request.training_date
        self.subject = f'Olaf is in de war! ({name} - {dt:%d-%m-%Y})'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        view_name = 'handouts:correct-mismatched-delivery'
        path = reverse(view_name)
        token = self.obtain_token()
        context['url'] = f'{settings.APP_HOSTNAME}{path}?token={token.id}'
        return context

    def obtain_token(self):
        kwargs = {'handout_request': self.handout_request}
        token, _ = HandoutToken.objects.get_or_create(**kwargs)
        return token


class UnknownDeliveriesNotification(BaseHandoutEmail):
    template_name = 'email_8_unknown_deliveries.html'

    def __init__(self, delivery):
        self.recipient = User(first_name='Ilona', last_name='Eichhorn',
                              email='eichhorn@debat.nl')
        self.delivery = delivery
        organization = self.delivery.organization
        product = self.delivery.product
        self.subject = f'Training niet herkend! ({organization} - {product})'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        view_name = 'handouts:correct-unknown-delivery'
        path = reverse(view_name)
        token = self.obtain_token()
        context['url'] = f'{settings.APP_HOSTNAME}{path}?token={token.id}'
        context['delivery'] = self.delivery
        context['trainings'] = Training.objects.all()
        return context

    def obtain_token(self):
        token, _ = HandoutToken.objects.get_or_create(delivery=self.delivery)
        return token
