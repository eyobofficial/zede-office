import mock

from django.core.files.uploadedfile import SimpleUploadedFile
from django.test import TestCase

from shared.tests.factories import TrainingFactory, InfographicFactory
from handouts.forms import HandoutPageFormSet
from handouts.tests.factories import HandoutRequestFactory
from handouts.models import Handout


class HandoutPageFormSetTests(TestCase):
    """
    Tests for `HandoutPageFormSet`
    """

    @mock.patch('handouts.models.Handout.combine_pdfs')
    def test_formset_with_valid_data(self, mock_obj):
        """
        When a valid data is provided, ensure `HandoutFormSet`
        creates a related `Handout` instance.
        """
        handout_request = HandoutRequestFactory()
        page1 = InfographicFactory(label='page-1')
        page1.training.add(TrainingFactory())

        page2 = InfographicFactory(label='page-2')
        page2.training.add(TrainingFactory())

        page3 = InfographicFactory(label='page-3')
        page3.training.add(TrainingFactory())

        mock_obj.return_value = SimpleUploadedFile('handout.pdf', b'content')
        data = {
            'form-INITIAL_FORMS': '0',
            'form-TOTAL_FORMS': '3',
            'form-MAX_NUM_FORMS': '',

            # page-1 selected
            'form-0-page_id': page1.pk,
            'form-0-is_selected': True,

            # page-2 selected
            'form-1-page_id': page2.pk,
            'form-1-is_selected': True,

            # page-3 NOT selected
            'form-2-page_id': page3.pk,
            'form-2-is_selected': False
        }
        formset = HandoutPageFormSet(data=data)
        is_valid = formset.is_valid()
        handout = formset.save(handout_request)

        self.assertTrue(is_valid)
        self.assertEqual(Handout.objects.count(), 1)
        self.assertEqual(Handout.objects.first(), handout)
        self.assertTrue(page1 in handout.pages.all())
        self.assertTrue(page2 in handout.pages.all())
        self.assertFalse(page3 in handout.pages.all())
