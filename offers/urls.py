from django.conf.urls import url

from offers import views
from offers.views import OverviewView, OfferDelete, OfferUpdate, OfferCreate, \
    ReviewOfferView, ReviewSubmissionView, AbsenceScheduleCreateView, \
    AbsenceScheduleDeleteView, AbsenceScheduleUpdateView

urlpatterns = [
    url(r'^review/$', ReviewOfferView.as_view(), name='review'),
    url(
        r'^review/submission/$',
        ReviewSubmissionView.as_view(),
        name='review_submission'
    ),
    url(r'^upload/$', views.upload_offer, name='upload'),
    url(r'^create/$', OfferCreate.as_view(), name='create'),
    url(
        r'^update/(?P<pk>[a-zA-Z0-9-]+)/$', OfferUpdate.as_view(),
        name='update'
    ),
    url(r'^delete/(?P<pk>[a-zA-Z0-9-]+)/$', OfferDelete.as_view(),
        name='delete'),
    url(
        r'^absence-schedules/create/$',
        AbsenceScheduleCreateView.as_view(),
        name='create-absence-schedule'
    ),
    url(
        r'^absence-schedules/(?P<pk>[0-9]+)/delete/$',
        AbsenceScheduleDeleteView.as_view(),
        name='delete-absence-schedule'
    ),
    url(
        r'^absence-schedules/(?P<pk>[0-9]+)/$',
        AbsenceScheduleUpdateView.as_view(),
        name='update-absence-schedule'
    ),
    url(r'^$', OverviewView.as_view(), name='overview'),
]
