from django.contrib import admin

from education.models import LearningGoal


@admin.register(LearningGoal)
class LearningGoalAdmin(admin.ModelAdmin):
    list_display = ('name', 'order', 'position')
    list_filter = ('position', )
    search_fields = ('name', 'description')
    filter_horizontal = ('finished_by', )
