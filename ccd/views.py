from django.http import Http404
from django.shortcuts import redirect
from django.views.generic import ListView, DetailView, TemplateView, UpdateView
from django.urls import reverse_lazy

from backoffice.constants import GROUP_CCD
from backoffice.mixins import GroupAccessMixin
from backoffice.views import BaseEmailView

from .emails.notifications import CCDNotificationEmail, PurchaseOrderEmail
from .models import Person, URLToken


class OverviewView(GroupAccessMixin, ListView):
    template_name = 'ccd/overview.html'
    model = Person
    access_groups = [GROUP_CCD]


class PersonUpdateView(GroupAccessMixin, UpdateView):
    model = Person
    fields = ('is_active', )
    success_url = reverse_lazy('ccd:overview')
    template_name = 'ccd/overview.html'
    access_groups = [GROUP_CCD]


class ExpiredPageView(TemplateView):
    template_name = 'ccd/public/expired_page.html'


class PurchaseOrderView(DetailView):
    template_name = 'ccd/public/person_deliverables.html'
    model = URLToken

    def get(self, request, *args, **kwargs):
        try:
            response = super().get(request, *args, **kwargs)
            if self.object.is_valid():
                return response
        except Http404:
            pass

        return redirect('ccd:expired-page')


class BasePersonEmailView(BaseEmailView):
    url = 'admin:ccd_person_change'
    model = Person


class CCDNotificationEmailView(BasePersonEmailView):
    email_class = CCDNotificationEmail
    email_name = 'CCD notification'


class PurchaseOrderEmailView(BasePersonEmailView):
    email_class = PurchaseOrderEmail
    email_name = 'purchase order email notification'
