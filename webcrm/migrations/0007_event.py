# -*- coding: utf-8 -*-
# Generated by Django 1.11.25 on 2019-12-12 12:08
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('webcrm', '0006_auto_20190622_1748'),
    ]

    operations = [
        migrations.CreateModel(
            name='Event',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('description', models.TextField(blank=True, default='')),
                ('type', models.CharField(blank=True, default='', max_length=120)),
                ('date_time', models.DateTimeField(blank=True, null=True)),
                ('participant_ids', models.CharField(blank=True, default='', max_length=120)),
                ('created_at', models.DateTimeField()),
                ('updated_at', models.DateTimeField()),
                ('state', models.CharField(choices=[('AVAILABLE', 'Available'), ('DELETED', 'Deleted')], default='AVAILABLE', max_length=50)),
                ('assigned_to', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='webcrm.User')),
                ('organization', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='webcrm.Organization')),
                ('person', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='webcrm.Person')),
            ],
            options={
                'ordering': ('date_time', 'organization'),
            },
        ),
    ]
