"""
WSGI config for DebatNL_BackOffice project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.11/howto/deployment/wsgi/
"""
import sys
import site

# Tell wsgi to add the Python site-packages to its path.
site.addsitedir('/home/debatnl/.virtualenvs/debatnl_backoffice/lib/python3.6/'
                'site-packages')

# Add the app's directory to the PYTHONPATH
sys.path.append('/home/debatnl/webapps/debatnl_backoffice/DebatNL_BackOffice')
sys.path.append('/home/debatnl/webapps/debatnl_backoffice/DebatNL_BackOffice/'
                'DebatNL_BackOffice')

from django.core.wsgi import get_wsgi_application
from DebatNL_BackOffice.utilities.settings_loader import load_settings
from raven.contrib.django.middleware.wsgi import Sentry

load_settings()
application = Sentry(get_wsgi_application())
