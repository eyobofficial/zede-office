from unittest.mock import patch
from uuid import uuid4

from django.test import TestCase, Client
from django.urls import reverse
from django.utils import timezone

from backoffice.constants import GROUP_DEBATNL_MOBILE
from backoffice.tests.factories import UserFactory, TrainerFactory
from users.models import AppPermission


class OrganizationOverviewViewTest(TestCase):
    """
    Tests for the `OrganizationOverviewView` class
    """
    fixtures = ['app_permissions']

    def setUp(self):
        self.client = Client()
        self.user = UserFactory()
        app_permission = AppPermission.objects.get(
            app_name=GROUP_DEBATNL_MOBILE)
        self.user.profile.app_permissions.add(app_permission)
        self.url = reverse('debatnl_mobile:organization-overview')
        self.template = 'debatnl_mobile/organization_overview.html'
        self.organization1 = {
            'id': uuid4().hex,
            'name': 'Test organization1',
            'created_at': timezone.now().strftime('%Y-%d-%m, %H:%M:%S'),
            'updated_at': timezone.now().strftime('%Y-%d-%m, %H:%M:%S'),
            'created_by': 'admin'
        }
        self.organization2 = {
            'id': uuid4().hex,
            'name': 'Test organization2',
            'created_at': timezone.now().strftime('%Y-%d-%m, %H:%M:%S'),
            'updated_at': timezone.now().strftime('%Y-%d-%m, %H:%M:%S'),
            'created_by': 'admin'
        }

    def test_request_as_anonymous_user(self):
        """
        Ensure unauthenticated users cannot access the view
        """
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 302)

    @patch('debatnl_mobile.views.organizations.OrganizationOverviewView.consumer')
    def test_request_as_authenticated_user(self, mock_consumer):
        """
        Ensure authenticated users can access the view
        """
        mock_consumer().ping.return_value = True
        mock_consumer().retrieve_all.return_value = [
            self.organization1,
            self.organization2
        ]
        self.client.force_login(self.user)
        response = self.client.get(self.url)

        self.assertTemplateUsed(response, self.template)
        self.assertEqual(len(response.context['organization_list']), 2)
        self.assertTrue(response.context['api_connection'])
        self.assertNotContains(
            response,
            'Er is geen connectie met de Debat.NL Mobile server.'
        )

    @patch('debatnl_mobile.views.organizations.OrganizationOverviewView.consumer')
    def test_request_with_non_active_api_connection(self, mock_consumer):
        """
        Test valid request with a non-active mobile API connection
        """
        mock_consumer().ping.return_value = False
        self.client.force_login(self.user)
        response = self.client.get(self.url)

        self.assertTemplateUsed(response, self.template)
        self.assertFalse(response.context['api_connection'])
        self.assertContains(
            response,
            'Er is geen connectie met de Debat.NL Mobile server.'
        )


class OrganizationCreateViewTest(TestCase):
    """
    Tests for `OrganizationCreateView` class
    """
    fixtures = ['app_permissions']

    def setUp(self):
        self.client = Client()
        self.user = UserFactory()
        app_permission = AppPermission.objects.get(
            app_name=GROUP_DEBATNL_MOBILE)
        self.user.profile.app_permissions.add(app_permission)
        self.url = reverse('debatnl_mobile:organization-create')
        self.template = 'debatnl_mobile/components/modals/organization_form.html'

    def test_request_as_anonymous_user(self):
        """
        Ensure unauthenticated users cannot access the view
        """
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 302)

    @patch('debatnl_mobile.views.organizations.OrganizationCreateView.consumer')
    def test_request_as_authenticated_user(self, mock_consumer):
        """
        Ensuer authenticated users can access the view
        """
        mock_consumer().ping.return_value = True
        self.client.force_login(self.user)
        response = self.client.get(self.url)

        self.assertEqual(response.status_code, 200)
        self.assertTrue(response.context['api_connection'])
        self.assertTemplateUsed(response, self.template)
        self.assertTrue('form' in response.context)

    @patch('debatnl_mobile.views.organizations.OrganizationCreateView.consumer')
    def test_post_request_as_authenticated_user(self, mock_consumer):
        """
        Ensure authenticated users can send POST request
        """
        self.client.force_login(self.user)
        payload = {
            'name': 'Test organization'
        }
        mock_consumer().ping.return_value = True
        mock_consumer().create_object.return_value = {
            'id': uuid4().hex,
            'name': payload['name'],
            'created_at': timezone.now().strftime('%Y-%m-%d, %H:%M:%S'),
            'updated_at': timezone.now().strftime('%Y-%m-%d, %H:%M:%S'),
            'created_by': self.user.username
        }
        response = self.client.post(self.url, payload)
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(
            response,
            reverse('debatnl_mobile:organization-overview')
        )


class OrganizationUpdateViewTest(TestCase):
    """
    Tests for `OrganizationUpdateView` class
    """
    fixtures = ['app_permissions']

    def setUp(self):
        self.client = Client()
        self.user = UserFactory()
        app_permission = AppPermission.objects.get(
            app_name=GROUP_DEBATNL_MOBILE)
        self.user.profile.app_permissions.add(app_permission)
        self.organization = {
            'id': '37798c07-a759-436d-bebd-e3824c0e8845',
            'name': 'Test Organization',
            'created_at': timezone.now().strftime('%Y-%m-%d, %H:%M:%S'),
            'updated_at': timezone.now().strftime('%Y-%m-%d, %H:%M:%S'),
            'created_by': 'admin'
        }
        self.url = reverse(
            'debatnl_mobile:organization-update',
            args=[self.organization.get('id')]
        )
        self.template = 'debatnl_mobile/components/modals/organization_form.html'

    def test_request_as_anonymous_user(self):
        """
        Ensure unauthenticated users have no access to the view
        """
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 302)

    @patch('debatnl_mobile.views.organizations.OrganizationUpdateView.consumer')
    def test_request_as_authenticated_user(self, mock_consumer):
        """
        Ensure authenticated users have access to the view
        """
        self.client.force_login(self.user)
        mock_consumer().retrieve_object.return_value = self.organization
        mock_consumer().ping.return_value = True
        response = self.client.get(self.url)

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, self.template)
        self.assertTrue(response.context['api_connection'])
        self.assertTrue('form' in response.context)
        self.assertTrue(response.context['object'], self.organization)

    @patch('debatnl_mobile.views.organizations.OrganizationUpdateView.consumer')
    def test_post_request_as_authenticated_user(self, mock_consumer):
        """
        Ensure authenticated users can make POST requests
        """
        self.client.force_login(self.user)
        mock_consumer().retrieve_object.return_value = self.organization
        mock_consumer().ping.return_value = True
        payload = {
            'name': 'Updated Organization'
        }
        response = self.client.post(self.url, payload)

        self.assertTrue(response.status_code, 302)
        self.assertRedirects(
            response,
            reverse('debatnl_mobile:organization-overview')
        )


class OrganizationDeleteViewTest(TestCase):
    """
    Tests for `OrganizationDeleteView` class
    """
    fixtures = ['app_permissions']

    def setUp(self):
        self.client = Client()
        self.user = UserFactory()
        app_permission = AppPermission.objects.get(
            app_name=GROUP_DEBATNL_MOBILE)
        self.user.profile.app_permissions.add(app_permission)
        self.organization = {
            'id': '37798c07-a759-436d-bebd-e3824c0e8845',
            'name': 'Test Organization',
            'created_at': timezone.now().strftime('%Y-%m-%d, %H:%M:%S'),
            'updated_at': timezone.now().strftime('%Y-%m-%d, %H:%M:%S'),
            'created_by': 'admin'
        }
        self.url = reverse(
            'debatnl_mobile:organization-delete',
            args=[self.organization.get('id')]
        )
        self.template = 'debatnl_mobile/components/modals/delete.html'

    def test_request_as_anonymous_user(self):
        """
        Ensure unauthenticated users have no access to the view
        """
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 302)

    @patch('debatnl_mobile.views.organizations.OrganizationDeleteView.consumer')
    def test_request_as_authenticated_user(self, mock_consumer):
        """
        Ensure authenticated users have access to the view
        """
        self.client.force_login(self.user)
        mock_consumer().ping.return_value = True
        mock_consumer().retrieve_object.return_value = self.organization
        response = self.client.get(self.url)

        self.assertEqual(response.status_code, 200)
        self.assertTrue(response.context['api_connection'])
        self.assertTemplateUsed(response, self.template)
        self.assertEqual(response.context['object'], self.organization)

    @patch('debatnl_mobile.views.organizations.OrganizationDeleteView.consumer')
    def test_post_request_as_authenticated_user(self, mock_consumer):
        """
        Ensure authenticated users can make POST request
        """
        self.client.force_login(self.user)
        mock_consumer().ping.return_value = True
        mock_consumer().retrieve_object.return_value = self.organization
        payload = {
            'name': 'Updated organization'
        }
        response = self.client.post(self.url, payload)

        self.assertEqual(response.status_code, 302)
        self.assertRedirects(
            response,
            reverse('debatnl_mobile:organization-overview')
        )


class TrainingOverviewViewTest(TestCase):
    """
    Tests for `TrainingOverviewView` class
    """
    fixtures = ['app_permissions']

    def setUp(self):
        self.client = Client()
        self.user = UserFactory()
        app_permission = AppPermission.objects.get(
            app_name=GROUP_DEBATNL_MOBILE)
        self.user.profile.app_permissions.add(app_permission)
        self.template = 'debatnl_mobile/training_overview.html'
        self.organization_id = uuid4().hex
        self.training1 = {
            'id': uuid4().hex,
            'name': 'Test training 1',
            'date': timezone.now().strftime('%Y-%m-%d'),
            'trainer_name': 'John Doe',
            'trainer_email': 'johndoe@test.email',
            'organization': self.organization_id,
            'created_at': timezone.now().strftime('%Y-%d-%m, %H:%M:%S'),
            'updated_at': timezone.now().strftime('%Y-%d-%m, %H:%M:%S'),
            'updated_by': 'admin'
        }
        self.training2 = {
            'id': uuid4().hex,
            'name': 'Test training 2',
            'date': timezone.now().strftime('d-m-Y'),
            'trainer_name': 'Jane Roe',
            'trainer_email': 'janeroe@test.email',
            'organization': self.organization_id,
            'created_at': timezone.now().strftime('%Y-%d-%m, %H:%M:%S'),
            'updated_at': timezone.now().strftime('%Y-%d-%m, %H:%M:%S'),
            'updated_by': 'admin'
        }
        self.url = reverse(
            'debatnl_mobile:training-overview',
            args=[self.organization_id]
        )

    def test_request_as_anonymous_user(self):
        """
        Ensure unauthenticated users have no access to the view
        """
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 302)

    @patch('debatnl_mobile.views.trainings.TrainingOverviewView.consumer')
    def test_request_as_authenticated_user(self, mock_consumer):
        """
        Ensure authenticated users have access to the view
        """
        self.client.force_login(self.user)
        mock_consumer().ping.return_value = True
        mock_consumer().retrieve_all.return_value = [
            self.training1,
            self.training2
        ]
        response = self.client.get(self.url)

        self.assertTrue(response.context['api_connection'])
        self.assertTemplateUsed(response, self.template)
        self.assertEqual(len(response.context['training_list']), 2)
        self.assertNotContains(
            response,
            'Er is geen connectie met de Debat.NL Mobile server.'
        )

    @patch('debatnl_mobile.views.trainings.TrainingOverviewView.consumer')
    def test_request_with_inactive_api_connection(self, mock_consumer):
        """
        Ensure authenticated users with inactive API connections
        shows an error alert message
        """
        self.client.force_login(self.user)
        mock_consumer().ping.return_value = False
        response = self.client.get(self.url)

        self.assertFalse(response.context['api_connection'])
        self.assertTemplateUsed(response, self.template)
        self.assertContains(
            response,
            'Er is geen connectie met de Debat.NL Mobile server.'
        )


class TrainingCreateViewTest(TestCase):
    """
    Tests for `TrainingCreateView` class
    """
    fixtures = ['app_permissions']

    def setUp(self):
        self.client = Client()
        self.user = UserFactory()
        self.trainer = TrainerFactory()
        app_permission = AppPermission.objects.get(
            app_name=GROUP_DEBATNL_MOBILE)
        self.user.profile.app_permissions.add(app_permission)
        self.organization_id = uuid4().hex
        self.url = reverse(
            'debatnl_mobile:training-create',
            args=[self.organization_id]
        )
        self.template = 'debatnl_mobile/components/modals/training_form.html'

    def test_request_as_anonymous_user(self):
        """
        Ensure anonymous users cannot create a training
        """
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 302)

    @patch('debatnl_mobile.views.trainings.TrainingCreateView.consumer')
    def test_request_as_authenticated_user(self, mock_consumer):
        """
        Ensure authenticated users can create a training
        """
        self.client.force_login(self.user)
        mock_consumer().ping.return_value = True
        response = self.client.get(self.url)

        self.assertTrue(response.context['api_connection'])
        self.assertTemplateUsed(response, self.template)


class TrainingUpdateView(TestCase):
    """
    Tests for `TrainingUpdateView` class
    """
    fixtures = ['app_permissions']

    def setUp(self):
        self.client = Client()
        self.user = UserFactory()
        app_permission = AppPermission.objects.get(
            app_name=GROUP_DEBATNL_MOBILE)
        self.user.profile.app_permissions.add(app_permission)
        self.organization_id = uuid4().hex
        self.trainer = UserFactory()
        self.training = {
            'id': uuid4().hex,
            'name': 'Test training 1',
            'date': timezone.now().strftime('%Y-%m-%d'),
            'trainer_name': self.trainer.get_full_name(),
            'trainer_email': self.trainer.email,
            'organization': self.organization_id,
            'created_at': timezone.now().strftime('%Y-%d-%m, %H:%M:%S'),
            'updated_at': timezone.now().strftime('%Y-%d-%m, %H:%M:%S'),
            'updated_by': self.user.username
        }
        self.url = reverse(
            'debatnl_mobile:training-update',
            args=[self.organization_id, self.training['id']]
        )
        self.template = 'debatnl_mobile/components/modals/training_form.html'

    def test_request_as_anonymous_user(self):
        """
        Ensure unauthenticated user can update a training
        """
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 302)

    @patch('debatnl_mobile.views.trainings.TrainingUpdateView.consumer')
    def test_request_as_authenticated_user(self, mock_consumer):
        """
        Ensure authenticated users can update a training
        """
        self.client.force_login(self.user)
        mock_consumer().ping.return_value = True
        mock_consumer().retrieve_object.return_value = self.training
        response = self.client.get(self.url)

        self.assertEqual(response.status_code, 200)
        self.assertTrue(response.context['api_connection'])
        self.assertTemplateUsed(response, self.template)


class TrainingDeleteViewTest(TestCase):
    """
    Tests for `TrainingDeleteView` class
    """
    fixtures = ['app_permissions']

    def setUp(self):
        self.client = Client()
        self.user = UserFactory()
        app_permission = AppPermission.objects.get(
            app_name=GROUP_DEBATNL_MOBILE)
        self.user.profile.app_permissions.add(app_permission)
        self.trainer = TrainerFactory()
        self.organization_id = uuid4().hex
        self.training = {
            'id': uuid4().hex,
            'name': 'Test training',
            'date': timezone.now().strftime('%Y-%m-%d'),
            'trainer_name': self.trainer.get_full_name(),
            'trainer_email': self.trainer.email,
            'organization': self.organization_id,
            'created_at': timezone.now().strftime('%Y-%d-%m, %H:%M:%S'),
            'updated_at': timezone.now().strftime('%Y-%d-%m, %H:%M:%S'),
            'updated_by': self.user.username
        }
        self.url = reverse(
            'debatnl_mobile:training-delete',
            args=[self.organization_id, self.training['id']]
        )
        self.template = 'debatnl_mobile/components/modals/delete.html'

    def test_request_as_anonymous_user(self):
        """
        Ensure unauthenticated users cannot delete training
        """
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 302)

    @patch('debatnl_mobile.views.trainings.TrainingDeleteView.consumer')
    def test_request_as_authenticated_user(self, mock_consumer):
        """
        Ensure authenticated users can delete training
        """
        self.client.force_login(self.user)
        mock_consumer().ping.return_value = True
        mock_consumer().retrieve_object.return_value = self.training
        response = self.client.get(self.url)

        self.assertTrue(response.status_code, 200)
        self.assertTrue(response.context['api_connection'])
        self.assertTemplateUsed(response, self.template)


class ChallengeOverviewViewTest(TestCase):
    """
    Tests for the `ChallengeOverviewView` class
    """
    fixtures = ['app_permissions']

    def setUp(self):
        self.client = Client()
        self.user = UserFactory()
        app_permission = AppPermission.objects.get(
            app_name=GROUP_DEBATNL_MOBILE)
        self.user.profile.app_permissions.add(app_permission)
        self.training = {
            'id': str(uuid4()),
            'name': 'Test Training',
            'organization': 1,
        }
        self.challenge1 = {
            'id': str(uuid4()),
            'title': 'Test Challenge 1',
            'training': self.training['id'],
            'order': 0,
            'updated_by': 'admin'
        }
        self.challenge2 = {
            'id': str(uuid4()),
            'title': 'Test Challenge 2',
            'training': self.training['id'],
            'order': 1,
            'updated_by': 'admin'
        }
        self.participant1 = {
            'id': '1',
            'first_name': 'John',
            'last_name': 'Doe',
            'username': 'johndoe@test.email',
            'email': 'johndoe@test.email',
            'profile': {
                'training': self.training['id'],
            }
        }
        self.participant2 = {
            'id': '2',
            'first_name': 'Jane',
            'last_name': 'Roe',
            'username': 'janeroe@test.email',
            'email': 'janeroe@test.email',
            'profile': {
                'training': self.training['id']
            }
        }
        self.url = reverse(
            'debatnl_mobile:challenge-overview',
            args=[self.training['id']]
        )
        self.template = 'debatnl_mobile/challenge_overview.html'

    def test_request_with_anonymous_user(self):
        """
        Ensure unauthenticated users have no access to the view.
        """
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 302)

    def test_request_with_authenticated_user(self):
        """
        Ensure authenticated users have access to the view
        """
        self.client.force_login(self.user)

        # Mock challenge & participant API calls
        challenge_consumer = 'debatnl_mobile.views.challenges' \
                             '.ChallengeOverviewView.consumer'

        participant_consumer = 'debatnl_mobile.views.challenges' \
                               '.ChallengeOverviewView.participant_consumer'

        training_consumer = 'debatnl_mobile.views.challenges' \
                            '.ChallengeOverviewView.training_consumer'

        with patch(challenge_consumer) as mock_challenge_consumer, \
                patch(participant_consumer) as mock_participant_consumer, \
                patch(training_consumer) as mock_trainining_consumer:
            mock_challenge_consumer().retrieve_all.return_value = [
                self.challenge1,
                self.challenge2
            ]
            mock_challenge_consumer().ping.return_value = True
            mock_participant_consumer().retrieve_all.return_value = [
                self.participant1,
                self.participant2
            ]
            mock_trainining_consumer().retrieve_object.return_value = self.training
            response = self.client.get(self.url)

        self.assertTrue(response.context['api_connection'])
        self.assertTemplateUsed(response, self.template)
        self.assertEqual(len(response.context['challenge_list']), 2)
        self.assertEqual(len(response.context['participant_list']), 2)
        self.assertNotContains(
            response,
            'Er is geen connectie met de Debat.NL Mobile server.'
        )


class ChallengeCreateViewTest(TestCase):
    """
    Tests for `ChallengeCreateView` class.
    """
    fixtures = ['app_permissions']

    def setUp(self):
        self.client = Client()
        self.user = UserFactory()
        app_permission = AppPermission.objects.get(
            app_name=GROUP_DEBATNL_MOBILE)
        self.user.profile.app_permissions.add(app_permission)
        self.training_id = str(uuid4())
        self.url = reverse(
            'debatnl_mobile:challenge-create',
            args=[self.training_id]
        )
        self.template = 'debatnl_mobile/components/modals/challenge_form.html'

    def test_request_with_anonymous_user(self):
        """
        Ensure unauthenticated users have no access to the view.
        """
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 302)

    @patch('debatnl_mobile.views.challenges.ChallengeCreateView.consumer')
    def test_request_with_authenticated_user(self, mock_consumer):
        """
        Ensure authenticated users have access to the view.
        """
        self.client.force_login(self.user)
        mock_consumer().ping.return_value = True
        response = self.client.get(self.url)

        self.assertEqual(response.status_code, 200)
        self.assertTrue(response.context['api_connection'])
        self.assertTemplateUsed(response, self.template)
        self.assertContains(response, 'form')

    def test_post_request_with_authenticated_user(self):
        """
        Ensure authenticated users can send POST request
        """
        self.client.force_login(self.user)
        payload = {
            'title': 'Test Challenge',
            'description': 'Test Description',
            'video_url': 'http://test-challenge.url',
            'order': '1',
            'training': self.training_id
        }

        challenge_consumer = 'debatnl_mobile.views.challenges' \
                             '.ChallengeCreateView.consumer'
        participant_consumer = 'debatnl_mobile.views.challenges' \
                               '.ChallengeOverviewView.participant_consumer'
        training_consumer = 'debatnl_mobile.views.challenges' \
                            '.ChallengeOverviewView.training_consumer'

        with patch(challenge_consumer) as mock_challenge_consumer, \
                patch(participant_consumer) as mock_participant_consumer:
            mock_challenge_consumer().ping.return_value = True
            mock_challenge_consumer().create_object.return_value = {
                'id': str(uuid4()),
                'title': payload['title'],
                'description': payload['description'],
                'video_url': payload['video_url'],
                'order': payload['order'],
                'training': payload['training']
            }
            mock_participant_consumer().retrieve_all.return_value = []
            response = self.client.post(self.url, payload)

        self.assertEqual(response.status_code, 302)
        self.assertRedirects(
            response,
            reverse(
                'debatnl_mobile:challenge-overview',
                args=[self.training_id]
            )
        )


class ChallengeUpdateViewTest(TestCase):
    """
    Tests for `ChallengeUpdateView` class
    """
    fixtures = ['app_permissions']

    def setUp(self):
        self.client = Client()
        self.user = UserFactory()
        app_permission = AppPermission.objects.get(
            app_name=GROUP_DEBATNL_MOBILE)
        self.user.profile.app_permissions.add(app_permission)
        self.training_id = str(uuid4())
        self.challenge = {
            'id': str(uuid4()),
            'title': 'Test Challenge',
            'description': 'Test challenge description',
            'video_url': 'http://test.url',
            'order': '1',
            'training': self.training_id
        }
        self.url = reverse(
            'debatnl_mobile:challenge-update',
            args=[self.training_id, self.challenge['id']]
        )
        self.template = 'debatnl_mobile/components/modals/challenge_form.html'

    def test_request_with_anonymous_user(self):
        """
        Ensure unauthenticated users have no access to the view.
        """
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 302)

    @patch('debatnl_mobile.views.challenges.ChallengeUpdateView.consumer')
    def test_request_with_authenticated_user(self, mock_consumer):
        """
        Ensure authenticated users have access to the view.
        """
        self.client.force_login(self.user)
        mock_consumer().ping.return_value = True
        response = self.client.get(self.url)

        self.assertTrue(response.context['api_connection'])
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, self.template)
        self.assertContains(response, 'form')

    @patch('debatnl_mobile.views.challenges.ChallengeUpdateView.consumer')
    def test_post_request_with_authenticated_user(self, mock_consumer):
        """
        Ensure authenticated users can make POST request to update
        challenge.
        """
        self.client.force_login(self.user)
        payload = {
            'id': self.challenge['id'],
            'title': 'Updated Challenge',
            'description': 'Updated challenge description',
            'video_url': 'http://updated-challenge.url',
            'order': '2',
            'training': self.training_id
        }
        mock_consumer().ping.return_value = True
        mock_consumer().retrieve_object.return_value = {
            'id': payload['id'],
            'title': payload['title'],
            'description': payload['description'],
            'video_url': payload['video_url'],
            'order': payload['order'],
            'training': payload['training']
        }
        response = self.client.post(self.url, payload)

        self.assertTrue(response.status_code, 302)
        self.assertRedirects(
            response,
            reverse(
                'debatnl_mobile:challenge-overview',
                args=[self.training_id]
            )
        )


class ChallengeDeleteViewTest(TestCase):
    """
    Tests for `ChallengeDeleteView` class
    """
    fixtures = ['app_permissions']

    def setUp(self):
        self.client = Client()
        self.user = UserFactory()
        app_permission = AppPermission.objects.get(
            app_name=GROUP_DEBATNL_MOBILE)
        self.user.profile.app_permissions.add(app_permission)
        self.training_id = str(uuid4())
        self.challenge = {
            'id': str(uuid4()),
            'title': 'Test Challenge',
            'description': 'Test challenge description',
            'video_url': 'http://test-challenge.url',
            'order': '1',
            'training': self.training_id
        }
        self.url = reverse(
            'debatnl_mobile:challenge-delete',
            args=[self.training_id, self.challenge['id']]
        )
        self.template = 'debatnl_mobile/components/modals/delete.html'

    def test_request_with_anonymous_user(self):
        """
        Ensure unauthenticated users have no access to the view.
        """
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 302)

    @patch('debatnl_mobile.views.challenges.ChallengeDeleteView.consumer')
    def test_request_with_authenticated_user(self, mock_consumer):
        """
        Ensure authenticated users have access to the view.
        """
        self.client.force_login(self.user)
        mock_consumer().ping.return_value = True
        mock_consumer().retrieve_object.return_value = self.challenge
        response = self.client.get(self.url)

        self.assertTrue(response.context['api_connection'])
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, self.template)

    @patch('debatnl_mobile.views.challenges.ChallengeDeleteView.consumer')
    def test_post_request_with_authenticated_user(self, mock_consumer):
        """
        Ensure authenticated users can make POST request to delete
        a challenge.
        """
        self.client.force_login(self.user)
        mock_consumer().ping.return_value = True
        mock_consumer().retrieve_object.return_value = self.challenge
        response = self.client.post(self.url)

        self.assertEqual(response.status_code, 302)
        self.assertRedirects(
            response,
            reverse(
                'debatnl_mobile:challenge-overview',
                args=[self.training_id])
        )


class ParticiapantDetailViewTest(TestCase):
    """
    Tests for `ParticipantDetailView` class
    """
    fixtures = ['app_permissions']

    def setUp(self):
        self.client = Client()
        self.user = UserFactory()
        app_permission = AppPermission.objects.get(
            app_name=GROUP_DEBATNL_MOBILE)
        self.user.profile.app_permissions.add(app_permission)
        self.training_id = str(uuid4())
        self.participant = {
            'id': '1',
            'first_name': 'John',
            'last_name': 'Doe',
            'username': 'johndoe@test.email',
            'email': 'johndoe@test.email',
            'profile': {
                'training': self.training_id
            }
        }
        self.url = reverse(
            'debatnl_mobile:participant-detail',
            args=[self.training_id, self.participant['id']]
        )
        self.template = 'debatnl_mobile/components/modals/participant_detail.html'

    def test_request_with_anonymous_user(self):
        """
        Ensure unauthenticated users have no access to the view.
        """
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 302)

    @patch('debatnl_mobile.views.challenges.ParticipantDetailView.consumer')
    def test_request_with_authenticated_user(self, mock_consumer):
        """
        Ensure authenticated users have access to the view.
        """
        self.client.force_login(self.user)
        mock_consumer().ping.return_value = True
        mock_consumer().retrieve_object.return_value = self.participant

        # Challenge submissoin consumer path
        submission_consumer = 'debatnl_mobile.views.challenges' \
                              '.ParticipantDetailView.submission_consumer'

        with patch(submission_consumer) as mock_submission_consumer:
            mock_submission_consumer().retrieve_all.return_value = []
            response = self.client.get(self.url)

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, self.template)
        self.assertTrue('submission_list' in response.context)


class ParticipantCreateViewTest(TestCase):
    """
    Tests for `ParticipantCreateView` class
    """
    fixtures = ['app_permissions']

    def setUp(self):
        self.client = Client()
        self.user = UserFactory()
        app_permission = AppPermission.objects.get(
            app_name=GROUP_DEBATNL_MOBILE)
        self.user.profile.app_permissions.add(app_permission)
        self.training_id = str(uuid4())
        self.url = reverse(
            'debatnl_mobile:participant-create',
            args=[self.training_id]
        )
        self.template = 'debatnl_mobile/components/modals/participant_form.html'

    def test_request_with_anonymous_user(self):
        """
        Ensure unauthenticated users have no access to the view.
        """
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 302)

    @patch('debatnl_mobile.views.challenges.ParticipantDetailView.consumer')
    def test_request_with_authenticated_user(self, mock_consumer):
        """
        Ensure authenticated users have access to the view.
        """
        self.client.force_login(self.user)
        mock_consumer().ping.return_value = True
        response = self.client.get(self.url)

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, self.template)
        self.assertContains(response, 'form')

    @patch('debatnl_mobile.views.challenges.ParticipantCreateView.consumer')
    def test_post_request_with_authenticated_user(self, mock_consumer):
        """
        Ensure authenticated users can make POST request to create
        participants.
        """
        self.client.force_login(self.user)
        payload = {
            'first_name': 'John',
            'last_name': 'Doe',
            'email': 'johndoe@test.email'
        }
        participant = {
            'id': str(uuid4()),
            'first_name': payload['first_name'],
            'last_name': payload['last_name'],
            'email': payload['email'],
            'username': payload['email'],
            'profile': {
                'training': self.training_id
            }
        }
        mock_consumer().ping.return_value = True
        mock_consumer().create_object.return_value = participant

        response = self.client.post(self.url, payload)
        success_url = reverse(
            'debatnl_mobile:challenge-overview',
            args=[self.training_id]
        )
        success_url = f'{success_url}#participant-overview'

        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, success_url)


class ParticipantUpdateViewTest(TestCase):
    """
    Tests for `ParticipantUpdateView` class
    """
    fixtures = ['app_permissions']

    def setUp(self):
        self.client = Client()
        self.user = UserFactory()
        app_permission = AppPermission.objects.get(
            app_name=GROUP_DEBATNL_MOBILE)
        self.user.profile.app_permissions.add(app_permission)
        self.training_id = str(uuid4())
        self.participant = {
            'id': '1',
            'first_name': 'John',
            'last_name': 'Doe',
            'email': 'johndoe@test.email',
            'username': 'johndoe@test.email',
            'profile': {
                'training': self.training_id
            }
        }
        self.template = 'debatnl_mobile/components/modals/participant_form.html'
        self.url = reverse(
            'debatnl_mobile:participant-update',
            args=[self.training_id, self.participant['id']]
        )

    def test_request_with_anonymous_user(self):
        """
        Ensure unauthenticated users have no access to the view
        """
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 302)

    @patch('debatnl_mobile.views.challenges.ParticipantUpdateView.consumer')
    def test_request_with_authenticated_user(self, mock_consumer):
        """
        Ensure authenticated users have access to the view.
        """
        self.client.force_login(self.user)
        mock_consumer().ping.return_value = True
        response = self.client.get(self.url)

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, self.template)
        self.assertContains(response, 'form')

    @patch('debatnl_mobile.views.challenges.ParticipantUpdateView.consumer')
    def test_post_request_with_authenticated_user(self, mock_consumer):
        """
        Ensure authenticated users can update participant using POST
        request.
        """
        self.client.force_login(self.user)
        payload = {
            'first_name': 'Jane',
            'last_name': 'Roe',
            'email': 'janeroe@test.email'
        }
        mock_consumer().ping.return_value = True
        mock_consumer().update_object.return_value = {
            'id': '1',
            'first_name': payload['first_name'],
            'last_name': payload['last_name'],
            'email': payload['email'],
            'username': payload['email'],
            'profile': {
                'training': self.training_id
            }
        }
        response = self.client.post(self.url, payload)
        success_url = reverse(
            'debatnl_mobile:challenge-overview',
            args=[self.training_id]
        )
        success_url = f'{success_url}#participant-overview'

        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, success_url)


class ParticipantDeleteViewTest(TestCase):
    """
    Tests for `ParticipantDeleteView` class
    """
    fixtures = ['app_permissions']

    def setUp(self):
        self.client = Client()
        self.user = UserFactory()
        app_permission = AppPermission.objects.get(
            app_name=GROUP_DEBATNL_MOBILE)
        self.user.profile.app_permissions.add(app_permission)
        self.training_id = str(uuid4())
        self.participant = {
            'id': '1',
            'first_name': 'John',
            'last_name': 'Doe',
            'email': 'johndoe@test.email',
            'username': 'johndoe@test.email',
            'profile': {
                'training': self.training_id
            }
        }
        self.url = reverse(
            'debatnl_mobile:participant-delete',
            args=[self.training_id, self.participant['id']]
        )
        self.template = 'debatnl_mobile/components/modals/delete.html'

    def test_request_with_anonymous_user(self):
        """
        Ensure unauthenticated users have no access to the view
        """
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 302)

    @patch('debatnl_mobile.views.challenges.ParticipantDeleteView.consumer')
    def test_request_with_authenticated_user(self, mock_consumer):
        """
        Ensure authenticated users have access to the view
        """
        self.client.force_login(self.user)
        mock_consumer().ping.return_value = True
        mock_consumer().retrieve_object.return_value = self.participant
        response = self.client.get(self.url)

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, self.template)

    @patch('debatnl_mobile.views.challenges.ParticipantDeleteView.consumer')
    def test_post_request_with_authenticated_user(self, mock_consumer):
        """
        Ensure authenticated users can delete a partcipant with
        a POST request.
        """
        self.client.force_login(self.user)
        mock_consumer().ping.return_value = True
        mock_consumer().delete_object.return_value = None
        response = self.client.post(self.url)
        success_url = reverse(
            'debatnl_mobile:challenge-overview',
            args=[self.training_id]
        )
        success_url = f'{success_url}#participant-overview'

        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, success_url)


class ChallengeSubmissionUpdateViewTest(TestCase):
    """
    Tests for `ChallengeSubmissionUpdateView` class.
    """
    fixtures = ['app_permissions']

    def setUp(self):
        self.client = Client()
        self.user = UserFactory()
        app_permission = AppPermission.objects.get(
            app_name=GROUP_DEBATNL_MOBILE)
        self.user.profile.app_permissions.add(app_permission)
        self.training_id = str(uuid4())
        self.challenge = {
            'id': str(uuid4()),
            'title': 'Test Challenge',
            'description': 'Test description',
            'order': '1',
            'training': self.training_id
        }
        self.submission = {
            'id': str(uuid4()),
            'challenge': self.challenge['id'],
            'submitted_by': '1'
        }
        self.url = reverse(
            'debatnl_mobile:submission-update',
            args=[self.training_id, self.submission['id']]
        )
        self.template = 'debatnl_mobile/components/modals/feedback_form.html'

    def test_request_with_anonymous_user(self):
        """
        Ensure unauthenticated users have no access to the view.
        """
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 302)

    @patch(
        'debatnl_mobile.views.challenges'
        '.ChallengeSubmissionUpdateView.consumer'
    )
    def test_request_with_authenticated_user(self, mock_consumer):
        """
        Ensure authenticated users have access to view.
        """
        self.client.force_login(self.user)
        mock_consumer().ping.return_value = True
        mock_consumer().retrieve_object.return_value = self.submission

        # Challenge consumer path
        challenge_consumer = 'debatnl_mobile.views.challenges' \
                             '.ChallengeSubmissionUpdateView' \
                             '.challenge_consumer'

        with patch(challenge_consumer) as mock_challenge_consumer:
            mock_challenge_consumer().retrieve_object.return_value = self.challenge
            response = self.client.get(self.url)

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, self.template)
        self.assertContains(response, 'form')

    @patch(
        'debatnl_mobile.views.challenges'
        '.ChallengeSubmissionUpdateView.consumer'
    )
    def test_post_request_with_authenticated_user(self, mock_consumer):
        """
        Ensure authenticated users can make POST request to update
        challenge submission.
        """
        self.client.force_login(self.user)
        payload = {
            'challenge_score': '10',
            'feedback': 'Test feedback'
        }
        mock_consumer().ping.return_value = True
        mock_consumer().retrieve_object.return_value = self.submission
        mock_consumer().update_object.return_value = {
            'id': self.submission['id'],
            'challenge': self.submission['challenge'],
            'challenge_title': self.challenge['title'],
            'feedback': payload['feedback'],
            'score': payload['challenge_score'],
            'submitted_by': '1'
        }

        # Challenge consumer path
        challenge_consumer = 'debatnl_mobile.views.challenges' \
                             '.ChallengeOverviewView.consumer'

        participant_consumer = 'debatnl_mobile.views.challenges' \
                               '.ChallengeOverviewView.participant_consumer'

        with patch(challenge_consumer) as mock_challenge_consumer, \
                patch(participant_consumer) as mock_participant_consumer:
            mock_challenge_consumer().retrieve_all.return_value = []
            mock_participant_consumer().retrieve_all.return_valie = []
            response = self.client.post(self.url, payload)

        success_url = reverse(
            'debatnl_mobile:challenge-overview',
            args=[self.training_id]
        )

        self.assertEqual(response.status_code, 302)
        self.assertRedirects(
            response,
            f'{success_url}#participant-overview'
        )
