from datetime import timedelta, date

from django.test import TestCase
from django.utils import timezone

from planning.models import PlanningRequest, Delivery
from planning.tests.factories import DeliveryFactory, PlanningRequestFactory, \
    ScheduleFactory
from shared.enums import RoleName
from users.models import Role
from users.tests.factories import UserFactory


class PlanningRequestModelTest(TestCase):
    """
    Tests for `PlanningRequest` model
    """
    fixtures = ['roles']

    def setUp(self):
        self.user = UserFactory()

    def test_default_status_field(self):
        """
        Test the default attribute for the `status` field
        """
        planning_request = PlanningRequest.objects.create(
            type=PlanningRequest.IMMEDIATE,
            created_by=self.user
        )
        self.assertEqual(planning_request.status, PlanningRequest.PENDING)

    def test_default_is_confirmed_by_admin_field(self):
        """
        Test the default attribute for the `is_confirmed_by_admin` field
        """
        planning_request = PlanningRequest.objects.create(
            type=PlanningRequest.IMMEDIATE,
            created_by=self.user
        )
        self.assertFalse(planning_request.is_confirmed_by_admin)

    def test_str_method(self):
        """
        Test the string returned by the `__str__()` method
        """
        planning_request = PlanningRequest.objects.create(
            type=PlanningRequest.IMMEDIATE,
            created_by=self.user
        )
        self.assertEqual(
            str(planning_request),
            'Type: IMMEDIATE, Status: PENDING'
        )

    def test_get_trainers_method_with_no_assigned_trainers(self):
        """
        Ensure method returns an empy list if not trainer is assigned.
        """
        planning_request = PlanningRequestFactory(
            type=PlanningRequest.OPTION
        )
        DeliveryFactory.create_batch(5, planning_request=planning_request)

        # Assertions
        self.assertEqual(planning_request.get_trainers(), [])

    def test_get_trainers_method_with_no_duplicate_trainers(self):
        """
        Ensure method returns a list of assigned trainers
        """
        planning_request = PlanningRequestFactory(
            type=PlanningRequest.OPTION
        )
        delivery1 = DeliveryFactory(planning_request=planning_request)
        delivery2 = DeliveryFactory(planning_request=planning_request)

        role = Role.objects.get(name=RoleName.TRAINER.value)
        trainer1 = UserFactory()
        trainer1.profile.roles.add(role)
        trainer2 = UserFactory()
        trainer2.profile.roles.add(role)
        trainer3 = UserFactory()
        trainer3.profile.roles.add(role)
        trainer4 = UserFactory()
        trainer4.profile.roles.add(role)

        ScheduleFactory(delivery=delivery1, user=trainer1)
        ScheduleFactory(delivery=delivery1, user=trainer2)
        ScheduleFactory(delivery=delivery2, user=trainer3)
        ScheduleFactory(delivery=delivery2, user=trainer4)

        # Assertions
        self.assertEqual(len(planning_request.get_trainers()), 4)
        self.assertTrue(trainer1 in planning_request.get_trainers())
        self.assertTrue(trainer2 in planning_request.get_trainers())
        self.assertTrue(trainer3 in planning_request.get_trainers())
        self.assertTrue(trainer4 in planning_request.get_trainers())

    def test_get_trainers_method_with_duplicate_trainers(self):
        """
        Ensure method does not returns duplicate assigned trainers
        """
        planning_request = PlanningRequestFactory(
            type=PlanningRequest.OPTION
        )
        delivery1 = DeliveryFactory(planning_request=planning_request)
        delivery2 = DeliveryFactory(planning_request=planning_request)

        role = Role.objects.get(name=RoleName.TRAINER.value)
        trainer1 = UserFactory()
        trainer1.profile.roles.add(role)
        trainer2 = UserFactory()
        trainer2.profile.roles.add(role)

        ScheduleFactory(delivery=delivery1, user=trainer1)
        ScheduleFactory(delivery=delivery1, user=trainer2)
        ScheduleFactory(delivery=delivery2, user=trainer1)
        ScheduleFactory(delivery=delivery2, user=trainer2)

        # Assertions
        self.assertEqual(len(planning_request.get_trainers()), 2)
        self.assertTrue(trainer1 in planning_request.get_trainers())
        self.assertTrue(trainer2 in planning_request.get_trainers())

    def test_get_date_method_with_related_deliveries(self):
        """
        Ensure `get_date` method returns the earliest date of the
        related deliveries.
        """
        planning_request = PlanningRequestFactory()
        DeliveryFactory(planning_request=planning_request, date='2019-08-20')
        DeliveryFactory(planning_request=planning_request, date='2019-09-20')
        DeliveryFactory(planning_request=planning_request, date='2019-10-20')

        self.assertEqual(planning_request.get_date(), date(2019, 8, 20))

    def test_get_date_method_with_no_related_deliveries(self):
        """
        Ensure `get_date` method returns None if there are no
        related deliveries.
        """
        planning_request = PlanningRequestFactory()
        self.assertIsNone(planning_request.get_date())


class DeliveryModelTest(TestCase):
    """
    Tests for the `Delivery` model
    """

    def test_default_value_of_planning_request_field(self):
        """
        Test default value of the `planning_request` foreign key field
        """
        delivery = DeliveryFactory()
        self.assertIsNone(delivery.planning_request)

    def test_deliveries_are_ordered_by_date_in_ascending(self):
        """
        Test deliveries are returned in ascending date order
        """
        date1 = timezone.localdate()
        date2 = timezone.localdate() + timedelta(days=10)
        date3 = timezone.localdate() + timedelta(days=20)

        # Delivery 1
        DeliveryFactory(date=date1)

        # Delivery 2
        DeliveryFactory(date=date2)

        # Delivery 3
        DeliveryFactory(date=date3)

        first_delivery = Delivery.objects.first()

        self.assertEqual(first_delivery.date, date1)

    def test_str_method(self):
        """
        Test the string returned by the `__str__()` method
        """
        delivery = DeliveryFactory(organization='Test Organization')
        self.assertEqual(str(delivery), 'Test Organization')

    def test_previous_date_method(self):
        """
        Test `previous_date()` method
        """
        today = timezone.localdate()
        yesterday = today - timedelta(days=1)
        delivery = DeliveryFactory(date=today)

        # Assertions
        self.assertEqual(delivery.previous_date(), yesterday)

    def test_next_date_method(self):
        """
        Test `next_date()` method
        """
        today = timezone.localdate()
        tomorrow = today + timedelta(days=1)
        delivery = DeliveryFactory(date=today)

        # Assertions
        self.assertEqual(delivery.next_date(), tomorrow)
