from django.test import TestCase
from django.contrib.auth import get_user_model

from webcrm.factories import UserFactory
from .factories import DeliverableFactory

User = get_user_model()


class DeliverableModelTest(TestCase):

    def setUp(self):
        # Create a dummy user
        self.test_user = User.objects.create_user(
            username='testuser',
            email='testuser@email.com',
        )

    def test_is_delivery_person_registered_with_registered_user(self):
        """
        Test is_delivery_person_registered() method with a user
        email which is the user database
        """
        responsible = UserFactory(email=self.test_user.email)
        deliverable = DeliverableFactory(delivery__responsible=responsible)
        self.assertTrue(deliverable.is_delivery_person_registered())

    def test_is_delivery_person_registered_with_none_registered_user(self):
        """
        Test is_delivery_person_registered() method with a user
        email which is not found in the user database.
        """
        deliverable = DeliverableFactory()
        self.assertFalse(deliverable.is_delivery_person_registered())
