from backoffice.utilities.emails import BaseEmail


class BaseDeclarationEmail(BaseEmail):
    template_location = '../templates/declarations/emails'
    email_type = ''


class DeclarationEmail(BaseDeclarationEmail):
    def __init__(self, declaration):
        self.declaration = declaration

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['declaration'] = self.declaration
        return context


class BaseBonusEmail(BaseDeclarationEmail):
    def __init__(self, bonus):
        self.bonus = bonus

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['bonus'] = self.bonus
        return context
