from django.contrib import admin

from backoffice.admin import CustomURLModelAdmin

from .models import HandoutRequest, Handout, HandoutToken
from .views import GenerateHandoutRequests, PageSelectionEmailView, \
    NoDeliveryByTrainingEmailView, SecondNoDeliveryByTrainingEmailView, \
    TrainerNotAssignedEmailView, MismatchedDeliveriesEmailView, \
    HandoutDownloadEmailView, UnknownDeliveriesEmailView


@admin.register(HandoutRequest)
class HandoutRequestAdmin(CustomURLModelAdmin):
    list_display = (
        'id',
        'submission_id',
        'trainee_name',
        'trainee_email',
        'training_date',
        'trainer',
        'status',
        'submitted_at'
    )
    list_filter = ('status', )
    filter_horizontal = ('deliveries', )
    custom_urls = [
        {
            'regex': r'^generate_handout_requests/$',
            'view': GenerateHandoutRequests,
            'name': 'generate_handout_requests'
        },
        {
            'regex': r'^(?P<pk>.+)/send_page_selection_email/$',
            'view': PageSelectionEmailView,
            'name': 'send_page_selection_email'
        },
        {
            'regex': r'^(?P<pk>.+)/send_no_delivery_by_training_email/$',
            'view': NoDeliveryByTrainingEmailView,
            'name': 'send_no_delivery_by_training_email'
        },
        {
            'regex': r'^(?P<pk>.+)/send_second_no_delivery_found_email/$',
            'view': SecondNoDeliveryByTrainingEmailView,
            'name': 'send_second_no_delivery_by_training_email'
        },
        {
            'regex': r'^(?P<pk>.+)/send_trainer_not_assigned_email/$',
            'view': TrainerNotAssignedEmailView,
            'name': 'send_trainer_not_assigned_email'
        },
        {
            'regex': r'^(?P<pk>.+)/send_mismatched_deliveries_email/$',
            'view': MismatchedDeliveriesEmailView,
            'name': 'send_mismatched_deliveries_email'
        },
        {
            'regex': r'^(?P<pk>.+)/send_handout_download_email/$',
            'view': HandoutDownloadEmailView,
            'name': 'send_handout_download_email'
        },
        {
            'regex': r'^(?P<pk>.+)/send_unknown_deliveries_email/$',
            'view': UnknownDeliveriesEmailView,
            'name': 'send_unknown_deliveries_email'
        }
    ]


@admin.register(Handout)
class HandoutAdmin(admin.ModelAdmin):
    list_display = ('id', 'created_at', 'updated_at')
    filter_horizontal = ('pages', )


@admin.register(HandoutToken)
class HandoutTokenAdmin(admin.ModelAdmin):
    list_display = ('id', 'delivery', 'handout_request')
