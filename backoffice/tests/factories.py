import factory
from django.contrib.auth.models import User

from backoffice.models import Employee, Training, ContactPerson


class UserFactory(factory.django.DjangoModelFactory):
    username = factory.Sequence(lambda n: 'username{}'.format(n))
    first_name = factory.Faker('first_name')
    last_name = factory.Faker('last_name')
    email = factory.Faker('email')

    class Meta:
        model = User


class TrainerFactory(factory.django.DjangoModelFactory):
    first_name = factory.Faker('first_name')
    last_name = factory.Faker('last_name')
    phone = factory.Faker('phone_number')
    email = factory.LazyAttribute(
        lambda t: '{}.{}@example.com'.format(t.first_name, t.last_name).lower()
    )
    category = 'TRAINER'

    class Meta:
        model = Employee


class AdministratorFactory(factory.django.DjangoModelFactory):
    first_name = factory.Faker('first_name')
    last_name = factory.Faker('last_name')
    phone = factory.Faker('phone_number')
    email = factory.LazyAttribute(
        lambda t: '{}.{}@example.com'.format(t.first_name, t.last_name).lower()
    )
    category = 'ADMINISTRATOR'

    class Meta:
        model = Employee


class EditorFactory(factory.django.DjangoModelFactory):
    first_name = factory.Faker('first_name')
    last_name = factory.Faker('last_name')
    phone = factory.Faker('phone_number')
    email = factory.LazyAttribute(
        lambda t: '{}.{}@example.com'.format(t.first_name, t.last_name).lower()
    )
    category = 'EDITOR'

    class Meta:
        model = Employee


class HallContactPersonFactory(factory.django.DjangoModelFactory):
    first_name = factory.Faker('first_name')
    last_name = factory.Faker('last_name')
    phone = factory.Faker('phone_number')
    email = factory.LazyAttribute(
        lambda t: '{}.{}@example.com'.format(t.first_name, t.last_name).lower()
    )
    category = 'HALL'

    class Meta:
        model = ContactPerson


class WebsiteContactPersonFactory(factory.django.DjangoModelFactory):
    first_name = factory.Faker('first_name')
    last_name = factory.Faker('last_name')
    phone = factory.Faker('phone_number')
    email = factory.LazyAttribute(
        lambda t: '{}.{}@example.com'.format(t.first_name, t.last_name).lower()
    )
    category = 'WEBSITE'

    class Meta:
        model = ContactPerson


class TrainingFactory(factory.django.DjangoModelFactory):
    name = factory.Faker('sentence', nb_words=3, variable_nb_words=True)

    class Meta:
        model = Training
