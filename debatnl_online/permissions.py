from backoffice.constants import GROUP_DEBATNLONLINE


def group_check(user):
    return user.groups.filter(name=GROUP_DEBATNLONLINE).exists()
