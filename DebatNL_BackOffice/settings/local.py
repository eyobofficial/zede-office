from .base import *

DEBUG = True
ASSETS_DEBUG = True

SECRET_KEY = 'u#)g=qh6b9n8pxcw8im04)nvqpb#85_5k2h88b(*5vn&a@x$$*'

INSTALLED_APPS += [
    'django_extensions'
]

INTERNAL_IPS = [
    '127.0.0.1',
]

# CORS Configurations

CORS_ORIGIN_ALLOW_ALL = True

# Cache Configurations

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.dummy.DummyCache'
    }
}

# Logging Configurations

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
        },
    },
    'loggers': {
        'django': {
            'handlers': ['console'],
            'level': env('DJANGO_LOG_LEVEL', default='INFO'),
        },
    },
}

# Database

DATABASES['default']['OPTIONS'] = {'charset': 'utf8mb4'}

# Maximum Data Upload Configurations

DATA_UPLOAD_MAX_NUMBER_FIELDS = 10000

# AWS S3 Configurations

AWS_S3_BUCKET_SUB_PATH = 'local'

# Host Settings

APP_HOSTNAME = 'http://www.debatnl.test:8000'
DEBATNL_ONLINE_API = 'http://www.debatnlonline.test:8080'

# E-mail Settings

EMAIL_BACKEND = 'post_office.EmailBackend'
POST_OFFICE = {
    'BACKENDS': {
        'default': 'django.core.mail.backends.console.EmailBackend'
    }
}

# Frontend Settings

FRONTEND_URL = 'http://www.debatnlonline.test:4200'

# JotForm API

JOT_FORM_API_KEY = env('JOT_FORM_API_KEY', default='')
JOT_FORM_AWS_QUEUE = 'DebatNLBackoffice-JOTForm-Local'
JOT_FORM_AWS_SUBMISSION_WEBHOOK = env('JOT_FORM_AWS_SUBMISSION_WEBHOOK',
                                      default='')

# Mandrill Settings

MANDRILL_API_KEY = env('MANDRILL_API_KEY', default='')

# AWS S3 Configurations

AWS_ACCESS_KEY = env('AWS_ACCESS_KEY', default='')
AWS_SECRET_KEY = env('AWS_SECRET_KEY', default='')
AWS_S3_BUCKET = 'debatnl-online'

# Vimeo API Settings

VIMEO_ACCESS_TOKEN = env('VIMEO_ACCESS_TOKEN', default='')
VIMEO_PRESET_ID = 120402792
VIMEO_ALLOWED_DOMAIN = env('VIMEO_ALLOWED_DOMAIN', default='')


# Debat.NL Online Settings

DEBATNL_ONLINE_API_KEY = env('DEBATNL_ONLINE_API_KEY', default='')

# This is for the readonly access to a participant dashboard.
DEBATNL_ONLINE_ADMIN_KEY = env('DEBATNL_ONLINE_ADMIN_KEY', default='')

CELERY_TASK_ALWAYS_EAGER = True
