import uuid
import os
from unicodedata import normalize

import pendulum
from django.conf import settings
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _
from django.db import models
from ordered_model.models import OrderedModel
from post_office.models import Email

from backoffice.helpers import count_workdays
from backoffice.models import Employee
from offers.utilities.dates import calculate_workday


class Offer(models.Model):
    NEW = 'NEW'
    FINISHED = 'FINISHED'

    STATUS_CHOICES = (
        (NEW, 'New'),
        (FINISHED, 'Finished'),
    )

    TYPE_PHONECARE_SPRINGEST = 'PHONECARE_SPRINGEST'

    TYPE_CHOICES = (
        (TYPE_PHONECARE_SPRINGEST, 'Phonecare or Springest'),
    )

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    type = models.CharField(max_length=50, default='', blank=True,
                            choices=TYPE_CHOICES)
    organization = models.CharField(blank=True, default='', max_length=200)
    contact_person = models.CharField(blank=True, default='', max_length=200)
    title = models.CharField(blank=True, default='', max_length=200)
    photo = models.ForeignKey('OfferPhoto', blank=True, null=True)
    delivery_responsible_name = models.CharField(blank=True, default='',
                                                 max_length=200)
    delivery_responsible_email = models.EmailField(blank=True, default='',
                                                   max_length=200)
    question_background = models.TextField(blank=True, default='')
    program_structure = models.TextField(blank=True, default='')
    is_trainer_proposed = models.NullBooleanField()
    old_editor = models.ForeignKey(Employee, related_name='offers',
                                   blank=True, null=True)
    editor = models.ForeignKey(User, related_name='offers')
    old_trainers = models.ManyToManyField(Employee, blank=True)
    trainers = models.ManyToManyField(User, blank=True)
    status = models.CharField(max_length=30, default='NEW',
                              choices=STATUS_CHOICES)
    file_location_name = models.CharField(max_length=200, blank=True,
                                          default='')
    file_location_year = models.CharField(max_length=200, blank=True,
                                          default='')
    finished_at = models.DateTimeField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '{} | {}'.format(self.organization, self.created_at.date())

    def lead_time(self):
        delta = 0
        tz = settings.TIME_ZONE
        today = pendulum.today(tz=tz)
        created_at = pendulum.instance(self.created_at, tz=tz)

        if today > created_at:
            delta = count_workdays(today, created_at)

        return delta

    def finished_time(self):
        delta = 0
        if self.finished_at:
            tz = settings.TIME_ZONE
            created_at = pendulum.instance(self.created_at, tz=tz)
            finished_at = pendulum.instance(self.finished_at, tz=tz)

            if finished_at > created_at:
                delta = count_workdays(finished_at, created_at) - 1

        return delta


class OfferInvestment(models.Model):
    subject = models.TextField(blank=True, default='')
    range = models.TextField(blank=True, default='')
    offer = models.ForeignKey(Offer, related_name='investments')


class OfferPhoto(OrderedModel):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(blank=True, default='', max_length=200)
    image = models.ImageField()

    class Meta(OrderedModel.Meta):
        verbose_name = _('Header image')
        verbose_name_plural = _('Header images')

    def __str__(self):
        return '{} ({})'.format(
            self.name,
            self.image.name
        ) if self.name else self.image.name

    def extension(self):
        name, extension = os.path.splitext(self.image.name)
        return extension


class OfferEmailItem(Email):
    NOTIFICATION = 'NOTIFICATION'
    REMINDER = 'REMINDER'

    TYPE_CHOICES = (
        (NOTIFICATION, 'Notification'),
        (REMINDER, 'Reminder'),
    )

    type = models.CharField(max_length=30, blank=True, default='',
                            choices=TYPE_CHOICES)
    offer = models.ForeignKey('Offer', related_name='emails', null=True,
                              on_delete=models.SET_NULL)

    class Meta:
        verbose_name = _('Offer e-mail')
        verbose_name_plural = _('Offer e-mails')


class OfferToken(models.Model):
    UPLOAD = 'UPLOAD'
    REVIEW = 'REVIEW'

    TYPE_CHOICES = (
        (UPLOAD, 'Upload'),
        (REVIEW, 'Review'),
    )

    key = models.UUIDField(default=uuid.uuid4, editable=False, db_index=True)
    email = models.EmailField()
    offer = models.ForeignKey('Offer', null=True, related_name='tokens')
    type = models.CharField(max_length=30, blank=True, default='',
                            choices=TYPE_CHOICES)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    deadline_at = models.DateTimeField(null=True)
    last_reminder_at = models.DateTimeField(null=True, blank=True)

    class Meta:
        verbose_name = _('Token')
        verbose_name_plural = _('Tokens')

    def save(self, *args, **kwargs):
        today = pendulum.today(tz=settings.TIME_ZONE)

        if self.type == OfferToken.UPLOAD:
            self.deadline_at = calculate_workday(today, 2)

        if self.type == OfferToken.REVIEW:
            self.deadline_at = calculate_workday(today, 1)

        super().save(*args, **kwargs)


def hash_location(instance, filename):
    filename = normalize('NFKD', filename).encode('ascii', 'ignore')
    filename = filename.decode('UTF-8')
    return 'uploads/offers/{}/{}'.format(str(uuid.uuid4()), filename)


class OfferFile(models.Model):
    offer = models.ForeignKey('Offer', null=True, blank=True,
                              related_name='files')
    file = models.FileField(upload_to=hash_location)
    is_denied = models.BooleanField(default=False)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = _('File')
        verbose_name_plural = _('Files')

    def filename(self):
        return os.path.basename(self.file.name)

    def extension(self):
        name, extension = os.path.splitext(self.file.name)
        return extension


class OfferReview(models.Model):
    offer = models.ForeignKey('Offer', null=True, blank=True,
                              related_name='reviews')
    review = models.TextField()
    created_at = models.DateTimeField(auto_now=True)


class OfferAbsenceSchedule(models.Model):
    start_date = models.DateField()
    end_date = models.DateField()
    old_employee = models.ForeignKey(Employee, blank=True, null=True,
                                     related_name='offer_absence_schedules')
    employee = models.ForeignKey(User,
                                 related_name='offer_absence_schedules')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def is_absence(self):
        today = pendulum.today(tz=settings.TIME_ZONE).date()
        return self.start_date <= today <= self.end_date
