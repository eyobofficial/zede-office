import uuid
from unicodedata import normalize

from django.db import models


def hash_location(instance, filename):
    filename = normalize('NFKD', filename).encode('ascii', 'ignore')
    filename = filename.decode('UTF-8')
    return 'uploads/lessons/{}/{}'.format(str(uuid.uuid4()), filename)


class TempLesson(models.Model):
    name = models.CharField(default='', blank=True, max_length=100)
    video = models.FileField(upload_to=hash_location)
    created_at = models.DateTimeField(auto_now_add=True)

