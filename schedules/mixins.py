from django.shortcuts import redirect

from backoffice.constants import GROUP_SCHEDULES
from backoffice.mixins import GroupAccessMixin


class VacationFormMixin(GroupAccessMixin):
    access_groups = [GROUP_SCHEDULES]

    def form_valid(self, form):
        self.request.session['step'] = self.request.path
        redirect_url = form.get_next_step()
        return redirect(redirect_url)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['previous'] = self.request.session.get('step')
        self.request.session['step'] = None
        return context
