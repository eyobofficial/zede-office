import pendulum
from django.conf import settings
from django.core.exceptions import ValidationError

from DebatNL_BackOffice.celery import app
from briefings.emails.notifications import MaxTrainersNotification
from briefings.models import Briefing, Deliverable, DeliverableTrainer
from briefings.emails.reminders import BriefingNotAddedReminder, \
    TwoWeeksForDeliveryReminder, NewAddedBriefingReminder
from webcrm.models import Delivery


@app.task
def create_briefing_deliveries():
    today = pendulum.today()
    cutoff_date = today.subtract(weeks=8)
    deliveries = Delivery.objects.filter(order_date__gte=cutoff_date)
    deliveries = deliveries.filter(state=Delivery.STATE_AVAILABLE)
    deliveries = deliveries.exclude(opportunity_custom5='Ja')
    deliveries = deliveries.exclude(product='Digitale intake')
    deliveries = deliveries.exclude(product='Digitale outtake')
    deliveries = deliveries.exclude(product='Debat.NL Online')
    deliveries = deliveries.exclude(product='SkillsTracker')
    deliveries = deliveries.exclude(organization=755)
    deliveries = deliveries.exclude(order_date__day=31,
                                    order_date__month=12)

    deliverables = Deliverable.objects.all()

    for delivery in deliveries.all():
        dr_email = delivery.responsible.email
        has_one_trainer = delivery.trainers.count() == 1
        is_dr_trainer = delivery.trainers.filter(email=dr_email).exists()

        if has_one_trainer and is_dr_trainer:
            continue

        kwargs = {'delivery': delivery}
        deliverable, created = Deliverable.objects.get_or_create(**kwargs)

        if deliverable.status == Deliverable.TO_BE_DELETED:
            deliverable.status = Deliverable.PENDING
            deliverable.save()

        if deliverable.briefing or deliverable.scans.count() > 0:
            deliverable.status = Deliverable.SUBMITTED
            deliverable.save()

        deliverable.notify_trainers_amount()
        deliverables = deliverables.exclude(pk=deliverable.pk)

    deliverables.update(status=Deliverable.TO_BE_DELETED)


@app.task
def clean_up_deliverables():
    today = pendulum.now(settings.TIME_ZONE)
    cutoff_date = today.subtract(weeks=8).date()
    Deliverable.objects.filter(status=Deliverable.TO_BE_DELETED,
                               date__lte=cutoff_date).all().delete()
    DeliverableTrainer.objects.filter(deliverables__isnull=True).all().delete()


@app.task
def send_email_add_briefing_reminders_every_monday():
    """
    Send add briefings to deliverable reminders at 8:00 am
    every Mondays (Europe/Amsterdam)
    """
    pending_deliverables = Deliverable.objects.filter(
        status=Deliverable.PENDING
    )
    email_list = []
    for deliverable in pending_deliverables.all():
        email = deliverable.delivery.responsible.email
        if email in email_list:
            continue

        email_list.append(email)

        try:
            BriefingNotAddedReminder(deliverable).send()
        except ValidationError:
            pass


@app.task
def send_email_reminder_before_2_weeks_of_a_delivery():
    """
    Send reminder e-mail to every delivery responsible
    two weeks before their deliverable date if the
    deliverable does not still have a briefing
    """
    today = pendulum.today(settings.TIME_ZONE)
    check_date = today.add(weeks=2).date()
    deliverables = Deliverable.objects.filter(
        status=Deliverable.PENDING,
        delivery__order_date=check_date
    )

    for deliverable in deliverables:
        TwoWeeksForDeliveryReminder(deliverable).send()


@app.task
def send_request_for_approval_reminder():
    """
    Send a request for approval reminder every 4 days at 10:00 am
    (Europe/Amsterdam)
    """
    today = pendulum.now().date()
    status = Deliverable.WAITING_FOR_APPROVAL
    deliverables = Deliverable.objects.filter(status=status,
                                              created_at__isnull=False).all()

    for deliverable in deliverables:
        period = today - deliverable.created_at.date()
        is_today = period.in_days() == 0
        is_reminder_day = period.in_days() > 1 and period.in_days() % 4 == 0

        if is_today or is_reminder_day:
            MaxTrainersNotification(deliverable).send()


@app.task
def send_email_reminder_last_week_added_briefings():
    """
    Send reminder e-mails to last week added briefing
    trainers
    """
    now = pendulum.now(settings.TIME_ZONE)
    briefings = Briefing.objects.all()

    for briefing in briefings:
        if now.diff(briefing.created_at).in_days() < 7:
            NewAddedBriefingReminder(briefing).send()
