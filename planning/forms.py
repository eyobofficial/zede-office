from django import forms
from django.contrib.auth.models import User
from django.urls import reverse

from shared.enums import RoleName
from .models import Delivery, Schedule, PlanningRequest


class ProcessTypeForm(forms.Form):
    DEFINITE = 'DE'
    OPTION = 'OP'

    process_choices = (
        (DEFINITE, 'Definite Planning Request'),
        (OPTION, 'Planning Request Option')
    )
    process = forms.ChoiceField(choices=process_choices,
                                widget=forms.RadioSelect)

    def get_redirect_url(self):
        process = self.cleaned_data['process']
        if process == self.DEFINITE:
            return reverse('planning:definite-request-create')
        return reverse('planning:option-request-create')


class DeliveryModelForm(forms.ModelForm):
    date = forms.CharField(label='Kies een datum', required=True)
    organization = forms.CharField(label='Organisatie', required=True)
    time = forms.CharField(label='Tijd zonder eerder aanwezig', required=True)
    location = forms.CharField(label='Locatie', widget=forms.Textarea,
                               required=True)
    description = forms.CharField(label='Korte omschrijving',
                                  widget=forms.Textarea,
                                  required=True)
    notes = forms.CharField(label='Opmerkingen voor Ilona',
                            widget=forms.Textarea,
                            required=False)
    trainer_amount = forms.IntegerField(
        label='Aantal trainers/gespreksleiders', required=True)

    class Meta:
        model = Delivery
        fields = ('id', 'date', 'organization', 'time', 'location',
                  'description', 'notes', 'trainer_amount')


DeliveryFormSet = forms.inlineformset_factory(
    PlanningRequest,
    Delivery,
    form=DeliveryModelForm,
    extra=1
)


class ScheduleForm(forms.Form):
    delivery = forms.ModelChoiceField(
        queryset=Delivery.objects.all()
    )
    employee = forms.ModelMultipleChoiceField(
        queryset=User.objects.filter(
            profile__roles__name=RoleName.TRAINER.value,
            is_active=True
        ),
        widget=forms.CheckboxSelectMultiple,
        required=False
    )

    def save(self):
        cleaned_data = self.cleaned_data
        delivery = cleaned_data['delivery']
        users = cleaned_data['employee']

        # Delete removed trainers
        delivery.schedules.exclude(user__in=users).delete()

        for user in users:
            Schedule.objects.get_or_create(
                delivery=delivery,
                user=user
            )

        return cleaned_data


BaseScheduleFormSet = forms.formset_factory(ScheduleForm)


class ScheduleFormSet(BaseScheduleFormSet):
    def save(self):
        for form in self.forms:
            form.save()
