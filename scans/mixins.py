from django.views.generic.base import ContextMixin

from scans.models import User


class ScanTrainersMixin(ContextMixin):
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['trainers'] = User.objects.filter(
            is_active=True,
            is_staff=False,
            is_superuser=False
        ).order_by('first_name', 'last_name')
        return context
