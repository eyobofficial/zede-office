import pendulum
from django.conf import settings
from django.test import TestCase

from backoffice.helpers import is_workday
from offers.models import OfferToken
from offers.tests.factories import OfferFactory, OfferTokenFactory, \
    OfferAbsenceScheduleFactory


class OfferModelTest(TestCase):
    def setUp(self):
        self.offer = OfferFactory()

    def test_str(self):
        expected_str = '{} | {}'.format(
            self.offer.organization,
            self.offer.created_at.date()
        )

        self.assertEqual(str(self.offer), expected_str)

    def test_lead_time(self):
        # Set the mock
        mock_now = pendulum.datetime(2018, 1, 24)
        pendulum.set_test_now(mock_now)

        expected_lead_time = [0, 0, 0, 1, 2, 2, 2, 3, 4]

        for index, n in enumerate(range(-2, 7)):
            with self.subTest(day=n):
                self.offer.created_at = mock_now.subtract(days=n)

                lead_time = expected_lead_time[index]
                self.assertEqual(self.offer.lead_time(), lead_time)

        # Clear the mock
        pendulum.set_test_now()

    def test_finished_time(self):
        # Set the mock
        mock_now = pendulum.datetime(2018, 1, 24)
        self.offer.created_at = mock_now

        expected_finished_time = [0, 0, 0, 1, 2, 2, 2, 3, 4]

        for index, n in enumerate(range(-2, 7)):
            with self.subTest(day=n):
                self.offer.finished_at = mock_now.add(days=n)

                finished_time = expected_finished_time[index]
                self.assertEqual(self.offer.finished_time(), finished_time)

    def test_no_finished_time(self):
        self.assertEqual(self.offer.finished_time(), 0)


class OfferTokenModelTest(TestCase):
    def setUp(self):
        self.tz = settings.TIME_ZONE

    def run_deadline_test(self, token_type, expected_dates):
        start = pendulum.datetime(2018, 6, 13, tz=self.tz)
        end = pendulum.datetime(2018, 6, 18, tz=self.tz)
        period = pendulum.period(start, end)

        for dt in period.range('days'):
            with self.subTest(date=dt), pendulum.test(dt):
                token = OfferTokenFactory(type=token_type)
                deadline_at = token.deadline_at
                self.assertTrue(is_workday(deadline_at))
                self.assertIn(deadline_at, expected_dates)

    def test_upload_token_deadline_date(self):
        expected_dates = [
            pendulum.datetime(2018, 6, 15, tz=self.tz),
            pendulum.datetime(2018, 6, 18, tz=self.tz),
            pendulum.datetime(2018, 6, 19, tz=self.tz),
            pendulum.datetime(2018, 6, 20, tz=self.tz),
        ]
        self.run_deadline_test(OfferToken.UPLOAD, expected_dates)

    def test_review_token_deadline_date(self):
        expected_dates = [
            pendulum.datetime(2018, 6, 14, tz=self.tz),
            pendulum.datetime(2018, 6, 15, tz=self.tz),
            pendulum.datetime(2018, 6, 18, tz=self.tz),
            pendulum.datetime(2018, 6, 19, tz=self.tz),
        ]
        self.run_deadline_test(OfferToken.REVIEW, expected_dates)


class OfferAbsenceScheduleTest(TestCase):
    def setUp(self):
        start_date = pendulum.datetime(2019, 9, 1, tz=settings.TIME_ZONE)
        kwargs = {
            'start_date': start_date.date(),
            'end_date': start_date.add(days=2).date(),
        }
        self.schedule = OfferAbsenceScheduleFactory(**kwargs)

    def test_if_employee_is_absent(self):
        test_datetimes = [
            pendulum.datetime(2019, 9, 1, tz=settings.TIME_ZONE),
            pendulum.datetime(2019, 9, 2, tz=settings.TIME_ZONE),
            pendulum.datetime(2019, 9, 3, tz=settings.TIME_ZONE),
        ]

        for fake_today in test_datetimes:
            with pendulum.test(fake_today), self.subTest(date=fake_today):
                self.assertTrue(self.schedule.is_absence())

    def test_if_employee_is_not_absent(self):
        test_datetimes = [
            pendulum.datetime(2019, 8, 30, tz=settings.TIME_ZONE),
            pendulum.datetime(2019, 9, 4, tz=settings.TIME_ZONE),
        ]

        for fake_today in test_datetimes:
            with pendulum.test(fake_today), self.subTest(date=fake_today):
                self.assertFalse(self.schedule.is_absence())
