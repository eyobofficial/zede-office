from backoffice.utilities.emails import BaseEmail


class BaseScheduleEmail(BaseEmail):
    template_location = '../templates/schedules/emails'

    def __init__(self, vacation_period):
        self.vacation_period = vacation_period

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['vacation_period'] = self.vacation_period
        return context
