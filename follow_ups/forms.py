from django import forms
from django.forms import formset_factory, modelformset_factory

from shared.models import Infographic

from follow_ups.models import FollowUpEvent


class FollowUpEventForm(forms.ModelForm):
    button_url = forms.CharField()

    class Meta:
        model = FollowUpEvent
        fields = ('date', 'type', 'subject', 'message', 'button_text',
                  'button_url', 'follow_up')

    def clean_button_url(self):
        data = self.cleaned_data['button_url']
        return f'https://{data}' if data else data


class InfographicsForm(forms.Form):
    infographics = forms.ModelMultipleChoiceField(
        queryset=Infographic.objects.all(),
        widget=forms.CheckboxSelectMultiple
    )


class InfographicDateForm(forms.Form):
    infographic = forms.ModelChoiceField(queryset=Infographic.objects.all())
    date = forms.DateField()


InfographicDateFormSet = formset_factory(InfographicDateForm, extra=0)


FollowUpEventFormSet = modelformset_factory(
    FollowUpEvent,
    fields=('follow_up', 'type', 'date', 'subject', 'message', 'button_text',
            'button_url')
)
