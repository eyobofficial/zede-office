from django import forms
from django.db import transaction
from django.forms import formset_factory, BaseFormSet

from shared.models import Infographic
from .models import Handout, HandoutRequest


class HandoutPageForm(forms.Form):
    page_id = forms.UUIDField()
    is_selected = forms.BooleanField(required=False)


class BaseHandoutPageFormSet(BaseFormSet):
    """
    Add M2M page instances to handout, merge PDF pages and
    and save it file field of the `Handout` instance.
    """

    @transaction.atomic
    def save(self, handout_request):
        handout = Handout.objects.create(handout_request=handout_request)
        for form in self.forms:
            if form.cleaned_data.get('is_selected'):
                page_id = form.cleaned_data.get('page_id')
                page = Infographic.objects.get(id=page_id)
                handout.pages.add(page)
                handout.save()

        handout.generate_pdf()
        return handout


HandoutPageFormSet = formset_factory(
    HandoutPageForm,
    formset=BaseHandoutPageFormSet
)


class HandoutRequestModelForm(forms.ModelForm):
    training_date = forms.DateField(widget=forms.HiddenInput)

    class Meta:
        model = HandoutRequest
        fields = ('training_date', 'trainer')
