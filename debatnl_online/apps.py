from django.apps import AppConfig


class DebatnlOnlineConfig(AppConfig):
    name = 'debatnl_online'
