import pendulum
from django.conf import settings
from django.core.exceptions import ValidationError

from DebatNL_BackOffice.celery import app
from follow_ups.emails.notifications import FollowUpEventEmail, \
    NoFollowUpEventSetupEmail
from follow_ups.models import FollowUp, FollowUpEvent
from webcrm.models import Opportunity


@app.task
def send_opportunity_emails():
    """
    Retrieve opportunities from WebCRM and send e-mails.
    """
    kwargs = {'level_text': '[1] Automatische opvolging'}
    opportunities = Opportunity.objects.filter(**kwargs)
    opportunities = opportunities.exclude(state=Opportunity.STATE_DELETED)
    follow_ups = FollowUp.objects.all()

    for opportunity in opportunities.all():
        follow_up, _ = FollowUp.objects.get_or_create(
            opportunity=opportunity)
        follow_ups = follow_ups.exclude(id=follow_up.id)

        if follow_up.status == FollowUp.DELETED:
            continue

        if not opportunity.assigned_to:
            follow_up.status = FollowUp.NO_SALES_RESPONSIBLE
            follow_up.save()

        elif not opportunity.person:
            follow_up.status = FollowUp.NO_CUSTOMER_CONTACT
            follow_up.save()

        elif follow_up.status != FollowUp.READY:
            follow_up.status = FollowUp.READY
            follow_up.save()

    follow_ups.delete()


@app.task
def send_follow_up_event_emails():
    today = pendulum.today(tz=settings.TIME_ZONE)
    kwargs = {'date': today.date(), 'status': FollowUpEvent.PENDING}
    events = FollowUpEvent.objects.filter(**kwargs).all()

    for event in events:
        try:
            FollowUpEventEmail(event).send()

            next_event = event.get_next_event()
            if next_event:
                event.follow_up.update_webcrm_opportunity(next_event)
        except ValidationError:
            pass


@app.task
def send_no_follow_up_event_setup():
    """
    Send e-mail notification to sales responsible of a follow_up if
    there are no follow_up event setups.
    """
    excl_kwargs = {'status': FollowUp.DELETED}
    incl_kwargs = {
        'opportunity__isnull': False,
        'opportunity__assigned_to__isnull': False,
        'opportunity__person__isnull': False
    }
    follow_ups = FollowUp.objects.exclude(**excl_kwargs)
    follow_ups = follow_ups.filter(**incl_kwargs).all()

    used_recipients = []
    for follow_up in follow_ups:
        events = follow_up.get_upcoming_events()
        recipient = follow_up.opportunity.assigned_to

        if events.count() == 0 and recipient not in used_recipients:
            try:
                NoFollowUpEventSetupEmail(follow_up).send()
            except ValidationError:
                pass

            used_recipients.append(recipient)
