from django.conf.urls import url
from scans import views
from scans.views import OverviewView, ScanDetailView, ScanDeleteView, \
    ScanUploadView, ScanApproveView, ScanConfirmationView, ScanUpdateView, \
    ScansDeliverableView


urlpatterns = [
    url(
        r'^(?P<pk>[a-zA-Z0-9-]+)/details/$',
        ScanDetailView.as_view(),
        name='details'
    ),
    url(
        r'^deliverables/(?P<pk>[0-9]+)/$',
        ScansDeliverableView.as_view(),
        name='deliverable-detail'
    ),
    url(
        r'^(?P<pk>[a-zA-Z0-9-]+)/delete/$',
        ScanDeleteView.as_view(),
        name='delete'
    ),
    url(
        r'^(?P<pk>[a-zA-Z0-9-]+)/upload/$',
        ScanUploadView.as_view(),
        name='upload'
    ),
    url(r'^$', OverviewView.as_view(), name='overview'),

    url(
        r'^deliverables/(?P<pk>[0-9]+)/add/$',
        views.ScanCreateView.as_view(),
        name='create'
    ),
    url(
        r'^(?P<pk>[a-zA-Z0-9-]+)/approve/$',
        ScanApproveView.as_view(),
        name='approve'
    ),
    url(
        r'^approve/confirm/$',
        ScanConfirmationView.as_view(),
        name='confirm'),
    url(
        r'^deliverables/(?P<pk>[0-9]+)/update/$',
        ScanUpdateView.as_view(),
        name='update'
    )
]
