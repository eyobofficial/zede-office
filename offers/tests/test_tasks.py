import mock
import pendulum
from django.conf import settings
from django.test import TestCase

from offers.models import OfferAbsenceSchedule, OfferToken
from offers.tasks import clean_up_absence_schedules, \
    send_review_reminder_emails, send_upload_reminders_emails
from offers.tests.factories import OfferAbsenceScheduleFactory, \
    OfferTokenFactory, OfferFactory


class CleanUpAbsenceSchedulesTaskTest(TestCase):
    def setUp(self):
        yesterday = pendulum.yesterday(tz=settings.TIME_ZONE).date()
        OfferAbsenceScheduleFactory(end_date=yesterday)
        self.schedule = OfferAbsenceScheduleFactory()

    def test_task(self):
        self.assertEqual(OfferAbsenceSchedule.objects.count(), 2)

        clean_up_absence_schedules()

        self.assertEqual(OfferAbsenceSchedule.objects.count(), 1)
        self.assertEqual(OfferAbsenceSchedule.objects.first(), self.schedule)


class SendReviewReminderTaskTest(TestCase):
    def setUp(self):
        self.tz = settings.TIME_ZONE
        self.fake_today = pendulum.datetime(2020, 3, 3, tz=self.tz)
        kwargs = {'type': OfferToken.REVIEW, 'offer': OfferFactory()}
        self.token = OfferTokenFactory(**kwargs)

    @mock.patch('offers.tasks.ReviewOfferReminder')
    def test_task_with_a_successful_reminder(self, mock_email):
        self.token.created_at = self.fake_today.add(hours=73)
        self.token.save()

        with pendulum.test(self.fake_today):
            send_review_reminder_emails()

        self.token.refresh_from_db()
        last_reminder_at = self.token.last_reminder_at
        last_reminder_at = pendulum.instance(last_reminder_at).in_tz(self.tz)

        mock_email.assert_called_once_with(self.token)
        mock_email().send.assert_called_once()
        self.assertTrue(last_reminder_at.is_same_day(self.fake_today))

    @mock.patch('offers.tasks.ReviewOfferReminder')
    def test_task_with_last_reminder_on_today(self, mock_email):
        self.token.created_at = self.fake_today.add(hours=73)
        self.token.last_reminder_at = self.fake_today
        self.token.save()

        with pendulum.test(self.fake_today):
            send_review_reminder_emails()

        self.token.refresh_from_db()
        last_reminder_at = self.token.last_reminder_at
        last_reminder_at = pendulum.instance(last_reminder_at).in_tz(self.tz)

        self.assertFalse(mock_email.called)
        self.assertTrue(last_reminder_at.is_same_day(self.fake_today))

    @mock.patch('offers.tasks.ReviewOfferReminder')
    def test_task_with_created_date_earlier_than_72_hours(self, mock_email):
        self.token.created_at = self.fake_today.add(hours=71)
        self.token.save()

        with pendulum.test(self.fake_today):
            send_review_reminder_emails()

        self.token.refresh_from_db()

        self.assertIsNone(self.token.last_reminder_at)
        self.assertFalse(mock_email.called)


class SendUploadReminderTaskTest(TestCase):
    def setUp(self):
        self.tz = settings.TIME_ZONE
        self.fake_today = pendulum.datetime(2020, 3, 5, tz=self.tz)
        self.fake_deadline_at = self.fake_today.subtract(days=3)

        with pendulum.test(self.fake_deadline_at):
            kwargs = {'type': OfferToken.UPLOAD, 'offer': OfferFactory()}
            self.token = OfferTokenFactory(**kwargs)

    @mock.patch('offers.tasks.CreatedOfferReminder')
    def test_task_with_a_successful_reminder(self, mock_email):
        with pendulum.test(self.fake_today):
            send_upload_reminders_emails()

        self.token.refresh_from_db()
        last_reminder_at = self.token.last_reminder_at
        last_reminder_at = pendulum.instance(last_reminder_at).in_tz(self.tz)

        mock_email.assert_called_once_with(self.token)
        mock_email().send.assert_called_once()
        self.assertTrue(last_reminder_at.is_same_day(self.fake_today))

    @mock.patch('offers.tasks.CreatedOfferReminder')
    def test_task_with_last_reminder_on_today(self, mock_email):
        with pendulum.test(self.fake_deadline_at):
            self.token.last_reminder_at = self.fake_today
            self.token.save()

        with pendulum.test(self.fake_today):
            send_upload_reminders_emails()

        self.token.refresh_from_db()
        last_reminder_at = self.token.last_reminder_at
        last_reminder_at = pendulum.instance(last_reminder_at).in_tz(self.tz)

        self.assertFalse(mock_email.called)
        self.assertTrue(last_reminder_at.is_same_day(self.fake_today))

    @mock.patch('offers.tasks.CreatedOfferReminder')
    def test_task_with_created_date_earlier_than_1_day(self, mock_email):
        with pendulum.test(self.fake_deadline_at.add(days=2)):
            self.token.save()

        with pendulum.test(self.fake_today):
            send_upload_reminders_emails()

        self.token.refresh_from_db()

        self.assertIsNone(self.token.last_reminder_at)
        self.assertFalse(mock_email.called)
