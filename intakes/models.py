import os
import uuid
from unicodedata import normalize

from django.contrib.auth.models import User
from django.db import models
from django.utils import timezone

from backoffice.models import Employee
from jotform.models import Form, Question


class Intake(models.Model):
    ONGOING = 'ONGOING'
    FINISHED = 'FINISHED'
    REPORT_PLANNED = 'PLANNED'
    REPORT_SENT = 'SENT'

    STATUS_CHOICES = (
        (ONGOING, 'Ongoing'),
        (FINISHED, 'Finished'),
    )

    REPORT_STATUS_CHOICES = (
        (REPORT_PLANNED, 'Planned'),
        (REPORT_SENT, 'Sent'),
    )

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    organization = models.CharField(blank=True, default='', max_length=200)
    cp_first_name = models.CharField(blank=True, default='', max_length=200)
    cp_last_name = models.CharField(blank=True, default='', max_length=200)
    cp_email = models.CharField(blank=True, default='', max_length=200)
    qid_name = models.IntegerField(blank=True, null=True)
    old_trainers = models.ManyToManyField(Employee, blank=True)
    trainers = models.ManyToManyField(User, blank=True)
    trainers_report_date = models.DateField(blank=True, null=True)
    trainers_report_status = models.CharField(
        max_length=30, default='PLANNED',
        choices=REPORT_STATUS_CHOICES
    )
    report_date = models.DateField(blank=True, null=True)
    report_submission_amount = models.IntegerField(null=True, blank=True)
    report_status = models.CharField(max_length=30, default='PLANNED',
                                     choices=REPORT_STATUS_CHOICES)
    form = models.ForeignKey(Form, on_delete=models.PROTECT)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    status = models.CharField(max_length=30, default='ONGOING',
                              choices=STATUS_CHOICES)

    def __str__(self):
        return '{} | {}'.format(self.organization, self.created_at.date())

    def contact_person(self):
        return '{} {}'.format(self.cp_first_name, self.cp_last_name)

    def report_date_passed(self):
        is_passed = False
        if self.report_date:
            today = timezone.now().date()
            is_passed = today > self.report_date
        return is_passed

    def trainers_report_date_passed(self):
        is_passed = False
        if self.trainers_report_date:
            today = timezone.now().date()
            is_passed = today > self.trainers_report_date
        return is_passed

    def is_finished(self):
        today = timezone.now().date()
        dates = []

        if self.report_date:
            dates += [self.report_date]

        if self.trainers_report_date:
            dates += [self.trainers_report_date]

        dates += [reminder.date for reminder in self.reminders.all()]
        return all(today >= d for d in dates)


class IntakeReportQuestion(models.Model):
    intake = models.ForeignKey(Intake, related_name='report_questions')
    qid = models.IntegerField(blank=True, null=True)

    def get_question(self):
        try:
            q = Question.objects.get(qid=self.qid, form=self.intake.form)
        except Question.DoesNotExist:
            q = None

        return q


class IntakeReminder(models.Model):
    PLANNED = 'PLANNED'
    SENT = 'SENT'

    STATUS_CHOICES = (
        (PLANNED, 'Planned'),
        (SENT, 'Sent'),
    )

    intake = models.ForeignKey(Intake, related_name='reminders')
    date = models.DateField()
    status = models.CharField(max_length=30, default='PLANNED',
                              choices=STATUS_CHOICES)

    class Meta:
        ordering = ['date']

    def date_passed(self):
        today = timezone.now().date()
        return today > self.date


def hash_location(instance, filename):
    filename = normalize('NFKD', filename).encode('ascii', 'ignore')
    filename = filename.decode('UTF-8')
    return 'uploads/intakes/{}/{}'.format(str(uuid.uuid4()), filename)


class IntakeReportFile(models.Model):
    TYPE_CHOICES = (
        ('CLIENT SUBMISSION REPORT', 'Client submission report'),
        ('CLIENT REMINDER REPORT', 'Client reminder report'),
        ('TRAINER SUBMISSION REPORT', 'Trainer submission report'),
    )

    report_id = models.UUIDField(default=uuid.uuid4, db_index=True)
    intake = models.ForeignKey(Intake, related_name='report_files')
    file = models.FileField(upload_to=hash_location)

    def filename(self):
        return os.path.basename(self.file.name)
