from django import forms
from django.urls import reverse

from schedules.models import VacationPeriod, Timetable


class PaidLeaveForm(forms.Form):
    """
    Step 1 Form - checks if requested leave is paid or unpaid
    """
    YES = True
    NO = False

    PAID_CHOICES = (
        (YES, 'yes'),
        (NO, 'no')
    )

    is_paid = forms.ChoiceField(
        choices=PAID_CHOICES,
        widget=forms.RadioSelect
    )

    def get_next_step(self):
        is_paid = eval(self.cleaned_data['is_paid'])
        if not is_paid:
            path = reverse('schedules:step-5')
            return f'{path}?type={VacationPeriod.UNPAID}'
        return reverse('schedules:step-2')


class ScheduleForm(forms.Form):
    """
    Step 2 Form - Checks status of the user's upcoming schedules
    """

    # Training constants
    YES = True
    NO = False

    SCHEDULE_CHOICES = (
        (YES, 'yes'),
        (NO, 'no')
    )

    training = forms.ChoiceField(
        choices=SCHEDULE_CHOICES,
        widget=forms.RadioSelect
    )
    appointement = forms.ChoiceField(
        choices=SCHEDULE_CHOICES,
        widget=forms.RadioSelect
    )
    meeting = forms.ChoiceField(
        choices=SCHEDULE_CHOICES,
        widget=forms.RadioSelect
    )

    def get_next_step(self):
        schedules = [
            eval(self.cleaned_data['training']),
            eval(self.cleaned_data['appointement']),
            eval(self.cleaned_data['meeting'])
        ]

        if any(schedules):
            return reverse('schedules:step-3')
        return reverse('schedules:step-4')


class LeaveBalanceForm(forms.Form):
    """
    Step 4 Form - Checks user's available leave balance
    """
    YES = True
    NO = False

    BALANCE_CHOICES = (
        (YES, 'yes'),
        (NO, 'no')
    )

    leave_balance = forms.ChoiceField(
        choices=BALANCE_CHOICES,
        widget=forms.RadioSelect
    )

    def get_next_step(self):
        leave_balance = eval(self.cleaned_data['leave_balance'])
        if leave_balance:
            return reverse('schedules:step-5')
        return reverse('schedules:step-7')


class ContractTypeForm(forms.Form):
    """
    Step 7 form - Checks user's contract type
    """
    ZERO_HOUR = 'ZH'
    MIN_MAX = 'MM'
    FULL_TIME = 'FT'

    TYPE_CHOICES = (
        (ZERO_HOUR, 'Nulurencontract'),
        (MIN_MAX, 'Min-max contract'),
        (FULL_TIME, 'Full-time contract')
    )

    contract_type = forms.ChoiceField(
        choices=TYPE_CHOICES,
        widget=forms.RadioSelect
    )

    def get_next_step(self):
        contract_type = self.cleaned_data['contract_type']
        if contract_type == self.MIN_MAX:
            return reverse('schedules:step-9')
        return reverse('schedules:step-8')


class EntitledHoursForm(forms.Form):
    """
    Step 9 form - Get user's entitled vacation hours
    """
    hours = forms.FloatField(label='Dit staat in je arbeidscontract')

    def get_next_step(self):
        return reverse('schedules:step-10')


class UsedHoursForm(forms.Form):
    """
    Step 10 form - Get user's already used vacation hours
    """
    allowed_hours = forms.FloatField(widget=forms.HiddenInput)
    requested_hours = forms.FloatField(
        label='Hoeveel uur verlof heb je dit jaar al opgenomen?'
    )

    def get_next_step(self):
        allowed_hours = self.cleaned_data['allowed_hours']
        requested_hours = self.cleaned_data['requested_hours']

        if requested_hours <= allowed_hours:
            path = reverse('schedules:step-5')
            return f'{path}?amount_hours={requested_hours}'
        return reverse('schedules:step-11')


class VacationPeriodForm(forms.ModelForm):
    class Meta:
        model = VacationPeriod
        fields = ('start_date', 'end_date', 'type', 'amount_hours', 'reason')


class TimetableForm(forms.ModelForm):
    class Meta:
        model = Timetable
        exclude = ('user',)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for key, value in self.fields.items():
            value.widget = forms.HiddenInput()

    def has_changed(self, *args, **kwargs):
        return True
