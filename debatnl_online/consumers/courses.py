import requests
from django.conf import settings


class CourseConsumer:

    def __init__(self):
        self.url = '{}/api/e-learning/courses'.format(settings.DEBATNL_ONLINE_API)
        self.headers = {
            'Authorization': 'Token {}'.format(settings.DEBATNL_ONLINE_API_KEY)
        }

    def retrieve_all(self):
        url = '{}/admin/'.format(self.url)
        response = requests.get(url, headers=self.headers)
        return response.json()

    def retrieve_object(self, object_id):
        url = '{}/{}/'.format(self.url, object_id)
        response = requests.get(url, headers=self.headers)
        return response.json()

    def update_object(self, object_id, data):
        url = '{}/{}/'.format(self.url, object_id)

        payload = {
            'id': object_id,
            'name': data.get('name'),
            'sequence': data.get('sequence'),
            'correct_answers_threshold': data.get('correct_answers_threshold'),
            'is_deleted': data.get('is_deleted', False)
        }

        response = requests.put(url, headers=self.headers, data=payload)
        return response.json()

    def create_object(self, data):
        url = '{}/'.format(self.url)

        payload = {
            'name': data.get('name'),
            'sequence': data.get('sequence'),
            'correct_answers_threshold': data.get('correct_answers_threshold'),
        }

        response = requests.post(url, headers=self.headers, data=payload)
        return response.json()


class CourseTestConsumer:
    def __init__(self):
        self.url = '{}/api/e-learning/courses'.format(settings.DEBATNL_ONLINE_API)
        self.headers = {
            'Authorization': 'Token {}'.format(settings.DEBATNL_ONLINE_API_KEY)
        }

    def create_object(self, course_id):
        url = '{}/{}/test/create/'.format(self.url, course_id)

        payload = {
            'course': course_id,
        }

        response = requests.post(url, headers=self.headers, data=payload)
        return response.json()


class CourseTestQuestionConsumer:

    def __init__(self):
        self.url = '{}/api/e-learning/courses'.format(settings.DEBATNL_ONLINE_API)
        self.headers = {
            'Authorization': 'Token {}'.format(settings.DEBATNL_ONLINE_API_KEY)
        }

    def retrieve_all(self, course_id):
        url = '{}/{}/test/'.format(self.url, course_id)
        response = requests.get(url, headers=self.headers)
        return response.json()

    def retrieve_object(self, course_id, question_id):
        url = '{}/{}/test/questions/{}/'.format(self.url, course_id, question_id)
        response = requests.get(url, headers=self.headers)
        return response.json()

    def update_object(self, course_id, question_id, data):
        url = '{}/{}/test/questions/{}/'.format(self.url, course_id, question_id)

        payload = {
            'id': question_id,
            'question': data.get('question'),
            'sequence': data.get('sequence'),
            'is_deleted': data.get('is_deleted', False),
        }

        response = requests.put(url, headers=self.headers, data=payload)
        return response.json()

    def create_object(self, course_id, data):
        url = '{}/{}/test/questions/'.format(self.url, course_id)

        payload = {
            'question': data.get('question'),
            'sequence': data.get('sequence'),
        }

        response = requests.post(url, headers=self.headers, data=payload)
        return response.json()


class CourseTestAnswerConsumer:
    def __init__(self):
        self.url = '{}/api/e-learning/courses'.format(settings.DEBATNL_ONLINE_API)
        self.headers = {
            'Authorization': 'Token {}'.format(settings.DEBATNL_ONLINE_API_KEY)
        }

    def update_object(self, course_id, question_id, data):
        url = '{}/{}/test/questions/{}/answers/'.format(self.url, course_id, question_id)
        response = requests.put(url, headers=self.headers, json=data)
        return response.json()

    def create_object(self, course_id, question_id, data):
        url = '{}/{}/test/questions/{}/answers/'.format(self.url, course_id, question_id)
        response = requests.post(url, headers=self.headers, json=data)
        return response.json()
