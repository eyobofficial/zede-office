## Debat.NL Olaf

### Prerequisites
* [Python 3.6+](https://www.python.org/downloads/)
* [MySQL 5.7+](https://dev.mysql.com/downloads/mysql/)

### Optionals
* [RabbitMQ 3.7+](https://www.rabbitmq.com/download.html)

### Setting up development environment
1. Create a virtualenv container and activate it. For more information: [Click here](https://virtualenv.pypa.io/en/stable/userguide/#usage)
2. Open Terminal and change to the directory of the project.
3. Install the dependencies of the project by entering the following command:
    
    ```bash
    $ pip install -r requirements.txt
    ```

4. Make sure the MySQL Server is up and running.
5. Create a database called `debatnl_backoffice`.
6. Create a `.env` file in the following location: `[PROJECT FOLDER]/DebatNL_BackOffice/utilities/`
7. Add the following lines in the `.env` file:
   
   ```
    ENVIRONMENT=LOCAL
    DATABASE_URL=mysql://[username]:[password]@[host]:[port]/debatnl_backoffice
    RABBITMQ_HOST=[host]
    RABBITMQ_USERNAME=[username]
    RABBITMQ_PASSWORD=[password]
   ```
   
8. Save the file. 

9. Run the following commands to setup database schema and to create dummy data:
     
    ```bash
    $ python ./manage.py migrate --noinput
    $ python ./manage.py loaddata groups
    $ python ./manage.py create_default_superuser
    ```


### Usage
1. Run the following command to run the development web server:
   
    ```bash
    $ python ./manage.py runserver 0.0.0.0:8000
    ```

2. Open a web browser and go to: [http://localhost:8000/](http://localhost:8000/)

3. To login use the following credentials to login.
    * E-mail address: `superuser@debat.nl`
    * Password: `admin`
