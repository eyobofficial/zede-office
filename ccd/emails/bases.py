from backoffice.utilities.emails import BaseEmail


class BaseCCDEmail(BaseEmail):
    template_location = '../templates/ccd/emails'
    email_type = ''


class BasePersonEmail(BaseCCDEmail):
    def __init__(self, person):
        self.recipient = person
