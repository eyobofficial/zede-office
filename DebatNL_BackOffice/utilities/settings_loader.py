import os
from environ import environ

# Load Environment Variables
env = environ.Env()
environ.Env.read_env()
project_name = 'DebatNL_BackOffice'


def load_settings():
    environment = env('ENVIRONMENT').lower()
    os.environ['DJANGO_SETTINGS_MODULE'] = '%s.settings.%s' % (project_name, environment)
