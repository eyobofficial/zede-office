from django.conf import settings

from DebatNL_BackOffice.celery import app
from backoffice.utilities.webcrm.client import WebCRMClient

from ccd.models import Person, URLToken
from ccd.emails.notifications import CCDNotificationEmail, PurchaseOrderEmail


@app.task
def send_ccd_email_notification_every_friday():
    """
    Send CCD email notifications every Friday at 08:00.
    """
    for person in Person.objects.filter(is_active=True).all():
        if person.get_future_ccd_deliveries().exists():
            CCDNotificationEmail(person).send()


@app.task
def send_purchase_order_emails():
    """
    Send purchase order email notifications on
    the 28th of each month @ 10.00.
    """
    for person in Person.objects.filter(is_active=True).all():
        if person.get_monthly_ccd_deliveries().exists():
            PurchaseOrderEmail(person).send()


@app.task
def clean_invalid_url_tokens():
    """
    Clean invalid (expired) tokens.
    """
    for token in URLToken.objects.all():
        if not token.is_valid():
            token.delete()


@app.task
def sync_ccd_persons():
    """
    Sync the CCD Persons from WebCRM every Friday @ 07:00.
    """
    client = WebCRMClient(settings.WEBCRM_AUTH_CODE)
    retrieved_persons = client.get_ccd_persons()
    initial_persons = Person.objects.all()

    for retrieved_person in retrieved_persons:
        email = retrieved_person.pop('PersonEmail')
        defaults = {
            'first_name': retrieved_person['PersonFirstName'],
            'last_name': retrieved_person['PersonLastName']
        }

        Person.objects.update_or_create(email=email, defaults=defaults)
        initial_persons = initial_persons.exclude(email=email)

    initial_persons.delete()
