from django.contrib.auth.models import Permission
from django.test import TestCase

from shared.enums import Position
from users.forms import UserForm, UserProfileForm
from users.models import Role, AppPermission
from users.tests.factories import UserFactory


class UserFormTest(TestCase):
    fixtures = ['roles', 'app_permissions']

    def setUp(self):
        roles = Role.objects.all()
        permissions = AppPermission.objects.all()
        self.form_data = {
            'first_name': 'John',
            'last_name': 'Doe',
            'email': 'john.doe@test.email',
            'phone': '12345678',
            'position': Position.TRAINER.value,
            'roles': [roles[0].pk, roles[1].pk],
            'app_permissions': [permissions[0].pk]
        }

    def test_removal_email_field_in_update_mode(self):
        user = UserFactory()
        form = UserForm(instance=user, data=self.form_data)
        self.assertFalse('email' in form.fields)

    def test_reactivating_an_user(self):
        UserFactory(is_active=False, username=self.form_data['email'])
        form = UserForm(data=self.form_data)

        self.assertTrue(form.is_valid())

        user = form.save()

        self.assertTrue(user.is_active)
        self.assertEqual(user.first_name, self.form_data['first_name'])
        self.assertEqual(user.last_name, self.form_data['last_name'])
        self.assertEqual(user.email, self.form_data['email'])
        self.assertEqual(user.profile.phone, self.form_data['phone'])
        self.assertEqual(user.profile.position, Position.TRAINER.value)
        self.assertEqual(user.profile.roles.count(), 2)
        self.assertEqual(user.profile.app_permissions.count(), 1)

    def test_user_replacement_for_single_user_roles(self):
        role = Role.objects.filter(allow_multiple_users=False).first()
        user_1 = UserFactory()
        user_1.profile.roles.add(role)

        self.form_data['roles'] = [role.pk]
        form = UserForm(data=self.form_data)

        self.assertTrue(form.is_valid())

        user_2 = form.save()
        user_1.profile.refresh_from_db()

        self.assertTrue(user_2.is_active)
        self.assertEqual(user_2.first_name, self.form_data['first_name'])
        self.assertEqual(user_2.last_name, self.form_data['last_name'])
        self.assertEqual(user_2.email, self.form_data['email'])
        self.assertEqual(user_2.profile.phone, self.form_data['phone'])
        self.assertEqual(user_2.profile.position, Position.TRAINER.value)
        self.assertEqual(user_2.profile.app_permissions.count(), 1)
        self.assertEqual(user_2.profile.roles.count(), 1)
        self.assertEqual(user_2.profile.roles.first(), role)
        self.assertEqual(user_1.profile.roles.count(), 0)


class UserProfileFormTest(TestCase):
    fixtures = ['roles', 'app_permissions']

    def setUp(self):
        self.user = UserFactory()

    def test_adding_permissions_to_user(self):
        data = {
            'briefings': True,
            'feedback_trainers': True,
            'follow_ups': True
        }

        form = UserProfileForm(instance=self.user.profile, data=data)
        permission_1 = Permission.objects.get(codename='view_all_briefings')
        permission_2 = Permission.objects.get(codename='view_all_followups')
        permission_3 = Permission.objects.get(codename='view_all_trainers')

        self.assertTrue(form.is_valid())

        form.save()

        self.user.refresh_from_db()
        self.assertIn(permission_1, self.user.user_permissions.all())
        self.assertIn(permission_2, self.user.user_permissions.all())
        self.assertIn(permission_3, self.user.user_permissions.all())

    def test_removing_permissions_to_user(self):
        data = {
            'briefings': False,
            'feedback_trainers': False,
            'follow_ups': False
        }

        form = UserProfileForm(instance=self.user.profile, data=data)
        permission_1 = Permission.objects.get(codename='view_all_briefings')
        permission_2 = Permission.objects.get(codename='view_all_followups')
        permission_3 = Permission.objects.get(codename='view_all_trainers')

        self.assertTrue(form.is_valid())

        form.save()

        self.user.refresh_from_db()
        self.assertNotIn(permission_1, self.user.user_permissions.all())
        self.assertNotIn(permission_2, self.user.user_permissions.all())
        self.assertNotIn(permission_3, self.user.user_permissions.all())
