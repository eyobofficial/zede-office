from django import forms
from django.db import transaction

from briefings.models import Deliverable

from scans.models import Scan
from scans.emails.notifications import ScanDeniedEmail, ScanApprovedEmail


class ScanForm(forms.ModelForm):
    organization = forms.CharField(widget=forms.HiddenInput)
    dr_name = forms.CharField(widget=forms.HiddenInput)
    dr_email = forms.EmailField(widget=forms.HiddenInput)

    class Meta:
        model = Scan
        fields = [
            'title', 'issues', 'type',
            'deadline_at', 'trainer',
            'organization', 'dr_name', 'dr_email'
        ]


ScanBaseFormSet = forms.modelformset_factory(
    Scan,
    form=ScanForm
)


class ScanFormSet(ScanBaseFormSet):
    def save(self, deliverables, commit=True):
        qs = self.queryset

        for form in self.forms:
            scan = form.save()
            scan.deliverables.add(*deliverables)
            scan.save()
            qs = qs.exclude(pk=scan.pk)

        for deliverable in deliverables:
            deliverable.status = Deliverable.SUBMITTED
            deliverable.save()

        qs.all().delete()
        return super().save(commit)


class ScanApproveForm(forms.ModelForm):
    status = forms.CharField(required=True)
    feedback = forms.CharField(widget=forms.Textarea, required=False)

    class Meta:
        model = Scan
        fields = ['status']

    @transaction.atomic
    def save(self, commit=True):
        scan = super().save(commit=commit)
        feedback = self.cleaned_data.get('feedback')
        if feedback:
            scan.reviews.create(feedback=feedback)

        if scan.status == Scan.PENDING:
            ScanDeniedEmail(scan).send()

        if scan.status == Scan.FINISHED:
            ScanApprovedEmail(scan).send()

        return scan
