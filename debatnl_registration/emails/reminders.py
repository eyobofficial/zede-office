from django.conf import settings
from django.contrib.auth.models import User
from django.urls import reverse

from debatnl_registration.emails.bases import ParticipantBaseEmail, \
    PlannedTrainingBaseEmail, TrainingDateBaseEmail
from debatnl_registration.models import TrainingDate
from shared.enums import RoleName


class TravelDirectionReminderEmail(ParticipantBaseEmail):
    template_name = 'email_2_travel_direction_reminder.html'

    def __init__(self, participant):
        super().__init__(participant)
        self.subject = f'Uw training {self.registration.training}'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        qs_kwargs = {'attendance__participant': self.recipient}
        training_dates = TrainingDate.objects.filter(**qs_kwargs).all()
        context.update({'training_dates': training_dates})
        return context


class BookHallReminderEmail(PlannedTrainingBaseEmail):
    template_name = 'email_9_book_hall_reminder.html'

    def __init__(self, token):
        super().__init__(token.planned_training)
        self.token = token
        training = self.planned_training.training.name
        self.subject = f'REMINDER: Verzoek boeking zaal training {training}'
        self.recipient = User.objects.filter(
            profile__roles__name=RoleName.HALL.value, is_active=True
        ).first()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        uri = reverse('debatnl_registration:hall_availability')
        base_url = f'{settings.APP_HOSTNAME}{uri}?token={str(self.token.key)}'
        training_dates = self.planned_training.training_dates.order_by('date')
        context.update({
            'training_dates': training_dates,
            'accept_url': '{}&rsvp=ACCEPT'.format(base_url),
            'deny_url': '{}&rsvp=DENY'.format(base_url),
        })
        return context


class CancelHallReminderEmail(PlannedTrainingBaseEmail):
    template_name = 'email_15b_cancel_hall_reminder.html'

    def __init__(self, token):
        super().__init__(token.planned_training)
        self.token = token
        training = self.planned_training.training.name
        self.subject = f'REMINDER: Annulering zaal training {training}'
        self.recipient = User.objects.filter(
            profile__roles__name=RoleName.HALL.value, is_active=True
        ).first()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        training_dates = self.planned_training.training_dates.filter(
            status=TrainingDate.CANCELLED).order_by('sequence')
        uri = reverse('debatnl_registration:hall_cancellation')
        url = f'{settings.APP_HOSTNAME}{uri}?token={str(self.token.key)}'
        context.update({
            'training_dates': training_dates,
            'button_url': url
        })
        return context


class HallAvailableReminderEmail(PlannedTrainingBaseEmail):
    template_name = 'email_13_hall_available_reminder.html'
    subject = 'Reminder, update Jotform'

    def __init__(self, token):
        super().__init__(token.planned_training)
        self.token = token
        self.recipient = User.objects.filter(
            profile__roles__name=RoleName.ADMIN.value, is_active=True
        ).first()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        uri = reverse('debatnl_registration:confirm_jot_form_update')
        url = f'{settings.APP_HOSTNAME}{uri}?token={str(self.token.key)}'
        context.update({'button_url': url})
        return context


class UpdateWebsiteReminderEmail(PlannedTrainingBaseEmail):
    template_name = 'email_14b_change_website_reminder.html'

    def __init__(self, token):
        super().__init__(token.planned_training)
        self.token = token
        training_name = token.planned_training.training.name
        self.subject = 'REMINDER: Verzoek tot aanpassing data training'
        self.subject = f'{self.subject} {training_name}'
        self.recipient = User.objects.filter(
            profile__roles__name=RoleName.WEBSITE.value, is_active=True
        ).first()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        uri = reverse('debatnl_registration:confirm_website_update')
        url = f'{settings.APP_HOSTNAME}{uri}?token={str(self.token.key)}'
        context.update({'button_url': url})
        return context


class AttendeesListReminderEmail(TrainingDateBaseEmail):
    template_name = 'email_23_attendees_list_reminder.html'

    def __init__(self, token):
        super().__init__(token.training_date)
        self.token = token
        name = self.training_date.planned_training.training.name
        date = self.training_date.date
        self.subject = 'Reminder aanwezigheid deelnemers'
        self.subject = f'{self.subject} {name} van {date:%d-%m-%Y}'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        uri = reverse('debatnl_registration:confirm_attendance_list')
        url = f'{settings.APP_HOSTNAME}{uri}?token={str(self.token.key)}'
        context.update({'button_url': url})
        return context

    def send(self):
        for trainer in self.training_date.trainers.all():
            self.recipient = trainer
            super().send()


class AttendeesAmountReminderEmail(TrainingDateBaseEmail):
    template_name = 'email_24_25_attendees_amount.html'

    def __init__(self, token):
        super().__init__(token.training_date)
        self.token = token
        name = self.training_date.planned_training.training.name
        count = self.training_date.attendees.count()
        self.subject = f'REMINDER - {count} deelnemers voor {name}'
        self.recipient = User.objects.filter(
            profile__roles__name=RoleName.ADMIN.value, is_active=True
        ).first()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        uri = reverse('debatnl_registration:confirm_attendees_amount')
        url = f'{settings.APP_HOSTNAME}{uri}?token={str(self.token.key)}'
        context.update({'button_url': url})
        return context
