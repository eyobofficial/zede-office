from django.contrib.auth.models import User

from backoffice.tests.bases import BaseEmailTestCase
from intakes.emails.notifications import IntakeTrainerDateNotificationEmail, \
    IntakeTrainerDateChangeNotificationEmail
from intakes.emails.reminders import IntakeReminderEmail, \
    IntakeOrganizationInsightReportEmail, IntakeTrainerInsightReportEmail, \
    IntakeCheckReminderEmail
from intakes.tests.factories import IntakeFactory
from shared.enums import RoleName
from users.models import Role
from users.tests.factories import UserFactory


class IntakeEmailTestCase(BaseEmailTestCase):
    template_dir = '../templates/intakes/emails'
    obj_context_name = 'intake'

    fixtures = ['roles']

    def setUp(self):
        super().setUp()
        self.obj = IntakeFactory()

        admin = User.objects.filter(
            profile__roles__name=RoleName.ADMIN.value,
            is_active=True
        ).first()

        role = Role.objects.get(name=RoleName.TRAINER.value)
        trainers = UserFactory.create_batch(2)
        for trainer in trainers:
            trainer.profile.roles.add(role)

        self.obj.trainers.add(*trainers)

        self.items = [
            {
                'class': IntakeReminderEmail,
                'args': (self.obj,),
                'template_name': 'email_1_intake_reminder.html',
                'subject': 'Overzicht deelnemers digitale intake',
                'attachment_count': 1,
                'recipients': [self.obj.cp_email]
            },
            {
                'class': IntakeOrganizationInsightReportEmail,
                'args': (self.obj,),
                'template_name': 'email_2a_intake_organization_insight_report'
                                 '.html',
                'subject': 'Overzicht antwoorden digitale intake',
                'attachment_count': 1,
                'recipients': [self.obj.cp_email]
            },
            {
                'class': IntakeTrainerInsightReportEmail,
                'args': (self.obj,),
                'template_name': 'email_2b_intake_trainer_insight_report.html',
                'subject': 'Overzicht antwoorden/inzendingen digitale intake '
                           '- {}'.format(self.obj.organization),
                'attachment_count': 0,
                'recipients': [trainer.email for trainer in trainers],
                'email_count': 2
            },
            {
                'class': IntakeCheckReminderEmail,
                'args': (self.obj,),
                'template_name': 'email_4_intake_check.html',
                'subject': 'Waarschuwing. Intake {} niet ingevuld'.format(
                    self.obj.organization
                ),
                'attachment_count': 0,
                'recipients': [admin.email]
            },
            {
                'class': IntakeTrainerDateNotificationEmail,
                'args': (self.obj,),
                'template_name': 'email_5_trainer_date_notification.html',
                'subject': 'Deadline intake {}'.format(self.obj.organization),
                'attachment_count': 0,
                'recipients': [trainer.email for trainer in trainers],
                'email_count': 2
            },
            {
                'class': IntakeTrainerDateChangeNotificationEmail,
                'args': (self.obj,),
                'template_name': 'email_6_trainer_date_change_notification.'
                                 'html',
                'subject': 'Deadline intake {} gewijzigd'.format(
                    self.obj.organization),
                'attachment_count': 0,
                'recipients': [trainer.email for trainer in trainers],
                'email_count': 2
            }

        ]
