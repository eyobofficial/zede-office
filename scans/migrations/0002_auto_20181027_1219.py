# -*- coding: utf-8 -*-
# Generated by Django 1.11.16 on 2018-10-27 10:19
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('scans', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='scan',
            options={'ordering': ['deadline_at']},
        ),
    ]
