from django.contrib import admin

from .models import Training, Infographic


@admin.register(Training)
class TrainingAdmin(admin.ModelAdmin):
    list_display = ('name', )
    search_fields = ('name', )


@admin.register(Infographic)
class InfographicAdmin(admin.ModelAdmin):
    list_display = ('label', 'training_names')
    search_fields = ('label', 'training__name')
    filter_horizontal = ('training', )
    list_filter = ('training', )

    @staticmethod
    def training_names(obj):
        names = [t.name for t in obj.training.all()]
        return ', '.join(names)
