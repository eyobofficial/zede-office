from django.test import TestCase
from post_office.models import Email, Attachment

from shared.enums import RoleName
from users.models import Role
from users.tests.factories import UserFactory


class BaseEmailTestCase(TestCase):
    template_dir = None
    items = []
    obj = None
    obj_context_name = None

    fixtures = ['roles']

    def setUp(self):
        role = Role.objects.get(name=RoleName.ADMIN.value)
        self.admin = UserFactory()
        self.admin.profile.roles.add(role)
        self.sender = f'{self.admin.first_name} {self.admin.last_name}'
        self.sender = f'{self.sender} <{self.admin.email}>'

    def test_send(self):
        for item in self.items:
            Email.objects.all().delete()
            Attachment.objects.all().delete()

            with self.subTest(item=item['class']):
                args = item['args']
                email = item['class'](*args)
                subject = item['subject']
                template = item['template_name']
                attachment_count = item.get('attachment_count', 0)
                recipients = item.get('recipients')
                email_count = item.get('email_count', 1)
                email.send()

                # Getting the context after sending, because some reports are
                # being generated and placed during the execution of the send()
                # function (e.g. emails of intake app).
                context = email.get_context_data()
                func = item.get('context')

                self.assertEqual(email_count, Email.objects.count())
                for created_email in Email.objects.all():
                    self.assertEqual(subject, created_email.subject)
                    self.assertEqual(template, email.template_name)
                    self.assertEqual(len(created_email.to), 1)
                    self.assertIn(created_email.to[0], recipients)
                    self.assertEqual(self.sender, created_email.from_email)
                    self.assertEqual(context.get('email_obj'), email)
                    self.assertEqual(self.template_dir,
                                     email.template_location)
                    self.assertEqual(Attachment.objects.count(),
                                     attachment_count)
                    self.assertEqual(context.get(self.obj_context_name),
                                     self.obj)

                # Run additional context test, which are defined in
                # child Class.
                func(context) if func else None
