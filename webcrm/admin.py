from django.contrib import admin

from backoffice.admin import CustomURLModelAdmin
from webcrm.models import Organization, Person, Delivery, User, Opportunity, \
    Event
from webcrm.views import SyncOrganizationsView, SyncPersonsView, \
    SyncDeliveriesView, SyncUsersView, SyncOpportunitiesView, SyncEventsView


@admin.register(Organization)
class OrganizationAdmin(CustomURLModelAdmin):
    list_display = ('id', 'name', 'division_name', 'industry')
    list_display_links = ('id', 'name')
    search_fields = ('id', 'name', 'division_name', 'industry')
    list_filter = ('state', )
    custom_urls = [
        {
            'regex': r'^sync_organizations/$',
            'view': SyncOrganizationsView,
            'name': 'sync_webcrm_organizations'
        }
    ]


@admin.register(Person)
class PersonAdmin(CustomURLModelAdmin):
    list_display = ('id', 'name', 'email', 'status')
    list_display_links = ('id', 'name')
    search_fields = ('id', 'name', 'email', 'status')
    list_filter = ('status', 'state')
    custom_urls = [
        {
            'regex': r'^sync_persons/$',
            'view': SyncPersonsView,
            'name': 'sync_webcrm_persons'
        }
    ]


@admin.register(User)
class UserAdmin(CustomURLModelAdmin):
    list_display = ('id', 'name', 'email')
    list_display_links = ('id', 'name')
    search_fields = ('id', 'name', 'email')
    list_filter = ('state', )
    custom_urls = [
        {
            'regex': r'^sync_users/$',
            'view': SyncUsersView,
            'name': 'sync_webcrm_users'
        }
    ]


class TrainerInline(admin.TabularInline):
    model = Person.training_deliveries.through
    can_delete = False
    extra = 0

    def has_add_permission(self, request):
        return False  # pragma: no cover


@admin.register(Delivery)
class DeliveryAdmin(CustomURLModelAdmin):
    list_display = ('id', 'order_date', 'organization', 'product',
                    'responsible')
    list_display_links = ('id', 'order_date')
    search_fields = ('id', 'organization__name', 'order_date', 'product',
                     'responsible__name')
    list_filter = ('state', )
    exclude = ('trainers', )
    inlines = (TrainerInline, )
    custom_urls = [
        {
            'regex': r'^sync_deliveries/$',
            'view': SyncDeliveriesView,
            'name': 'sync_webcrm_deliveries'
        }
    ]


@admin.register(Opportunity)
class OpportunityAdmin(CustomURLModelAdmin):
    list_display = ('id', 'order_date', 'organization', 'product', 'person')
    list_display_links = ('id', 'order_date')
    search_fields = ('id', 'organization__name', 'order_date', 'product',
                     'person__name')
    list_filter = ('state', )
    custom_urls = [
        {
            'regex': r'^sync_opportunities/$',
            'view': SyncOpportunitiesView,
            'name': 'sync_webcrm_opportunities'
        }
    ]


@admin.register(Event)
class EventAdmin(CustomURLModelAdmin):
    list_display = ('id', 'date_time', 'organization', 'type', 'assigned_to')
    list_display_links = ('id', 'date_time')
    search_fields = ('id', 'date_time', 'organization__name', 'type',
                     'assigned_to__name')
    list_filter = ('state', )
    custom_urls = [
        {
            'regex': r'^sync_events/$',
            'view': SyncEventsView,
            'name': 'sync_webcrm_events'
        }
    ]
