from django.contrib import admin

from backoffice.admin import CustomURLModelAdmin

from .models import Declaration, ExternalMeeting, InternalMeeting, \
    AbsenceEntry, Expense, EmployeeBonus, ClientBonus, ELearningItem, \
    HubstaffItem, FeedbackItem, TravelExpense, TravelExpenseRoute
from .views import DeclarationReminderEmailView, NewClientBonusEmailView


class ExternalMeetingInline(admin.StackedInline):
    model = ExternalMeeting
    extra = 0
    can_delete = True
    raw_id_fields = ('travel_expense', )


class InternalMeetingInline(admin.StackedInline):
    model = InternalMeeting
    extra = 0
    can_delete = True
    raw_id_fields = ('travel_expense', )


class AbsenceEntryInline(admin.StackedInline):
    model = AbsenceEntry
    extra = 0
    can_delete = True


class ExpenseInline(admin.StackedInline):
    model = Expense
    extra = 0
    can_delete = True
    filter_horizontal = ('internal_meetings', 'external_meetings')


class EmployeeBonusInline(admin.StackedInline):
    model = EmployeeBonus
    extra = 0
    can_delete = True


class ClientBonusInline(admin.StackedInline):
    model = ClientBonus
    extra = 0
    can_delete = True


class ELearningItemInline(admin.StackedInline):
    model = ELearningItem
    extra = 0
    can_delete = True


class HubstaffItemInline(admin.StackedInline):
    model = HubstaffItem
    extra = 0
    max_num = 1
    can_delete = True


class FeedbackItemInline(admin.StackedInline):
    model = FeedbackItem
    extra = 0


@admin.register(Declaration)
class DeclarationAdmin(CustomURLModelAdmin):
    list_display = ('user', 'month', 'year', 'status')
    list_filter = ('year', 'month', 'status')
    inlines = [ExternalMeetingInline, InternalMeetingInline,
               AbsenceEntryInline, ExpenseInline, EmployeeBonusInline,
               ClientBonusInline, ELearningItemInline, HubstaffItemInline,
               FeedbackItemInline]
    search_fields = ('year', 'month', 'user__first_name', 'user__last_name',
                     'user__email')

    custom_urls = [
        {
            'regex': r'^(?P<pk>.+)/declaration_reminder/$',
            'view': DeclarationReminderEmailView,
            'name': 'send_declaration_reminder'
        }
    ]


@admin.register(ClientBonus)
class ClientBonusAdmin(CustomURLModelAdmin):
    list_display = ('name', 'amount', 'invoice_number', 'declaration')
    search_fields = ('name', 'invoice_number')
    custom_urls = [
        {
            'regex': r'^(?P<pk>.+)/new_client_bonus/$',
            'view': NewClientBonusEmailView,
            'name': 'send_new_client_bonus_email'
        },
    ]


class TravelExpenseRouteInline(admin.StackedInline):
    model = TravelExpenseRoute
    extra = 0
    can_delete = True


@admin.register(TravelExpense)
class TravelExpenseAdmin(admin.ModelAdmin):
    list_display = ('type', 'is_one_way',)
    inlines = [TravelExpenseRouteInline]


@admin.register(Expense)
class ExpenseAdmin(admin.ModelAdmin):
    list_display = ('description', 'date', 'amount', 'declaration')
    search_fields = ('description', 'declaration__pk',
                     'declaration__user__first_name',
                     'declaration__user__last_name',
                     'declaration__user__email')
