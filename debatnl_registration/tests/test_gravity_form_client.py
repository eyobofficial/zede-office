import datetime
from collections import OrderedDict

from django.test import SimpleTestCase
from debatnl_registration.utilities.gravity_form import GravityFormClient


class DateParserTest(SimpleTestCase):
    """
    Test the parsing of the training dates from the Gravity Form Submission.
    """

    def test_empty_string(self):
        input_string = ''
        dates = GravityFormClient.parse_date(input_string)
        self.assertEqual(len(dates), 0)

    def test_no_date_in_string(self):
        input_string = 'hello and bye'
        dates = GravityFormClient.parse_date(input_string)
        self.assertEqual(len(dates), 0)

    def test_one_date_in_string(self):
        input_string = '12 december 2017 (Utrecht)'
        dates = GravityFormClient.parse_date(input_string)
        self.assertEqual(dates[0], datetime.date(2017, 12, 12))

    def test_two_dates_in_string(self):
        input_string = '12 december 2017 en 1 februari 2018 (Utrecht)'
        date = GravityFormClient.parse_date(input_string)
        self.assertEqual(date[0], datetime.date(2017, 12, 12))
        self.assertEqual(date[1], datetime.date(2018, 2, 1))

    def test_three_dates_in_string(self):
        input_string = '9 oktober 2018, 13 november 2018 en ' \
                       '11 december 2018 (Utrecht)'
        date = GravityFormClient.parse_date(input_string)
        self.assertEqual(date[0], datetime.date(2018, 10, 9))
        self.assertEqual(date[1], datetime.date(2018, 11, 13))
        self.assertEqual(date[2], datetime.date(2018, 12, 11))

    def test_four_dates_in_string(self):
        input_string = '9 oktober 2018, 9 november 2018, 13 november 2018 ' \
                       'en 11 december 2018 (Utrecht)'
        date = GravityFormClient.parse_date(input_string)
        self.assertEqual(date[0], datetime.date(2018, 10, 9))
        self.assertEqual(date[1], datetime.date(2018, 11, 9))
        self.assertEqual(date[2], datetime.date(2018, 11, 13))
        self.assertEqual(date[3], datetime.date(2018, 12, 11))


class DateSerializerTest(SimpleTestCase):
    """
    Test the serialization of the training dates.
    """

    def test_empty_list(self):
        dates = OrderedDict([])
        serialized_dates = GravityFormClient.serialize_dates(dates)
        self.assertEqual(len(serialized_dates), 0)

    def test_one_date(self):
        data = OrderedDict([
            (
                datetime.date(2018, 10, 9), [
                    datetime.date(2018, 10, 9)
                ]
            )
        ])

        serialized_dates = GravityFormClient.serialize_dates(data)

        expected_values = [{
            'text': '9 oktober 2018 (Utrecht)',
            'value': '9 oktober 2018 (Utrecht)',
            'isSelected': False,
            'price': ''
        }]

        self.assertEqual(len(serialized_dates), 1)
        self.assertListEqual(serialized_dates, expected_values)

    def test_two_dates(self):
        data = OrderedDict([
            (
                datetime.date(2017, 12, 12), [
                    datetime.date(2017, 12, 12),
                    datetime.date(2018, 10, 9)
                ]
            )
        ])

        serialized_dates = GravityFormClient.serialize_dates(data)

        expected_values = [{
            'text': '12 december 2017 en 9 oktober 2018 (Utrecht)',
            'value': '12 december 2017 en 9 oktober 2018 (Utrecht)',
            'isSelected': False,
            'price': ''
        }]

        self.assertEqual(len(serialized_dates), 1)
        self.assertListEqual(serialized_dates, expected_values)

    def test_three_dates(self):
        data = OrderedDict([
            (
                datetime.date(2017, 12, 12), [
                    datetime.date(2017, 12, 12),
                    datetime.date(2018, 10, 9),
                    datetime.date(2018, 11, 13)
                ]
            )
        ])

        serialized_dates = GravityFormClient.serialize_dates(data)

        expected_values = [{
            'text': '12 december 2017, 9 oktober 2018 en '
                    '13 november 2018 (Utrecht)',
            'value': '12 december 2017, 9 oktober 2018 en '
                     '13 november 2018 (Utrecht)',
            'isSelected': False,
            'price': ''
        }]

        self.assertEqual(len(serialized_dates), 1)
        self.assertListEqual(serialized_dates, expected_values)

    def test_four_dates(self):
        data = OrderedDict([
            (
                datetime.date(2017, 12, 12), [
                    datetime.date(2017, 12, 12),
                    datetime.date(2018, 10, 9),
                    datetime.date(2018, 11, 13),
                    datetime.date(2018, 12, 11)
                ]
            )
        ])

        serialized_dates = GravityFormClient.serialize_dates(data)

        expected_values = [{
            'text': '12 december 2017, 9 oktober 2018, 13 november 2018 en '
                    '11 december 2018 (Utrecht)',
            'value': '12 december 2017, 9 oktober 2018, 13 november 2018 en '
                     '11 december 2018 (Utrecht)',
            'isSelected': False,
            'price': ''
        }]

        self.assertEqual(len(serialized_dates), 1)
        self.assertListEqual(serialized_dates, expected_values)


class DateSorterTest(SimpleTestCase):
    def setUp(self):
        self.client = GravityFormClient(form_id=39)

    def test_date_sorting_1_day(self):
        data = [
            {
                'text': '9 oktober 2018 (Utrecht)',
                'value': '9 oktober 2018 (Utrecht)',
            },
            {
                'text': '14 oktober 2017 (Utrecht)',
                'value': '14 oktober 2017 (Utrecht)',
            },
            {
                'text': '28 december 2018 (Utrecht)',
                'value': '28 december 2018 (Utrecht)',
            },
            {
                'text': '1 januari 2018 (Utrecht)',
                'value': '1 januari 2018 (Utrecht)',
            }
        ]

        expected_values = OrderedDict([
            (
                datetime.date(2017, 10, 14), [datetime.date(2017, 10, 14)]
            ),
            (
                datetime.date(2018, 1, 1), [datetime.date(2018, 1, 1)]
            ),
            (
                datetime.date(2018, 10, 9), [datetime.date(2018, 10, 9)]
            ),
            (
                datetime.date(2018, 12, 28), [datetime.date(2018, 12, 28)]
            ),
        ])

        serialized_dates = self.client._order_dates(data)
        self.assertEqual(len(serialized_dates), len(expected_values))
        self.assertDictEqual(serialized_dates, expected_values)

    def test_date_sorting_3_days(self):
        data = [
            {
                'text': '9 oktober 2018 (Utrecht)',
                'value': '9 oktober 2018, 10 oktober 2018 en '
                         '11 oktober 2018 (Utrecht)',
            },
            {
                'text': '14 oktober 2017 (Utrecht)',
                'value': '14 oktober 2017, 15 oktober 2017 en '
                         '16 oktober 2017 (Utrecht)',
            },
            {
                'text': '28 december 2018 (Utrecht)',
                'value': '28 december 2018, 29 december 2018 en '
                         '30 december 2018 (Utrecht)',
            },
            {
                'text': '1 januari 2018 (Utrecht)',
                'value': '1 januari 2018, 2 januari 2018 en '
                         '3 januari 2018 (Utrecht)',
            }
        ]

        expected_values = OrderedDict([
            (
                datetime.date(2017, 10, 14), [
                    datetime.date(2017, 10, 14),
                    datetime.date(2017, 10, 15),
                    datetime.date(2017, 10, 16)
                ]
            ),
            (
                datetime.date(2018, 1, 1), [
                    datetime.date(2018, 1, 1),
                    datetime.date(2018, 1, 2),
                    datetime.date(2018, 1, 3)
                ]
            ),
            (
                datetime.date(2018, 10, 9), [
                    datetime.date(2018, 10, 9),
                    datetime.date(2018, 10, 10),
                    datetime.date(2018, 10, 11)
                ]
            ),
            (
                datetime.date(2018, 12, 28), [
                    datetime.date(2018, 12, 28),
                    datetime.date(2018, 12, 29),
                    datetime.date(2018, 12, 30)
                ]
            ),
        ])

        serialized_dates = self.client._order_dates(data)
        self.assertEqual(len(serialized_dates), len(expected_values))
        self.assertDictEqual(serialized_dates, expected_values)
