import datetime

from django.contrib.auth.models import User
from django.test import TestCase, Client
from django.urls import reverse
from django.utils import timezone
from post_office.models import Email

from backoffice.constants import GROUP_PLANNING
from planning.forms import ProcessTypeForm
from planning.models import PlanningRequest, Delivery
from shared.enums import RoleName
from users.models import AppPermission, Role
from users.tests.factories import UserFactory
from .factories import PlanningRequestFactory, DeliveryFactory, ScheduleFactory


class PlanningViewTest(TestCase):
    """
    Tests form `PlanningView` view
    """
    fixtures = ['app_permissions', 'roles']

    def setUp(self):
        self.user = UserFactory()
        app_permission = AppPermission.objects.get(app_name=GROUP_PLANNING)
        self.user.profile.app_permissions.add(app_permission)
        self.client = Client()
        self.url = reverse('planning:overview')
        self.template = 'planning/base.html'

        role = Role.objects.get(name=RoleName.ADMIN.value)
        admin = UserFactory()
        admin.profile.roles.add(role)

    def test_request_as_anonymous_user(self):
        """
        Test a request to the `PlanningView` view from anonymous user
        """
        response = self.client.get(self.url)

        # Assertions
        self.assertEqual(response.status_code, 302)

    def test_request_as_authenticated_user(self):
        """
        Test a request to the `PlanningView` view from authenticated
        user
        """
        self.client.force_login(self.user)
        response = self.client.get(self.url)

        # Assertions
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, self.template)

    def test_request_with_no_pending_deliveries(self):
        """
        Test a request to the `PlanningView` view from authenticated
        user with no `PENDING` deliveries
        """
        self.client.force_login(self.user)
        response = self.client.get(self.url)

        # Assertions
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, self.template)
        self.assertQuerysetEqual(response.context['object_list'], [])
        self.assertContains(
            response,
            'Geen openstaande spoed planningsverzoeken'
        )
        self.assertContains(
            response,
            'Geen openstaande reguliere planningsverzoeken'
        )
        self.assertContains(
            response,
            'Geen openstaande lange termijn planningsverzoeken'
        )

    def test_request_with_IMMEDIATE_type_pending_deliveries(self):
        """
        Test a request to the `PlanningView` view from authenticated
        user with `IMMEDIATE` type `PENDING` deliveries
        """
        self.client.force_login(self.user)
        planning_request = PlanningRequestFactory.create(
            type=PlanningRequest.IMMEDIATE
        )
        DeliveryFactory.create_batch(
            5,
            planning_request=planning_request
        )
        response = self.client.get(self.url)

        # Assertions
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, self.template)
        self.assertEqual(
            len(response.context['object_list']),
            5
        )
        self.assertNotContains(
            response,
            'Geen openstaande spoed planningsverzoeken'
        )

    def test_request_with_SHORT_TERM_type_pending_deliveries(self):
        """
        Test a request to the `PlanningView` view from authenticated
        user with `SHORT_TERM` type `PENDING` deliveries
        """
        self.client.force_login(self.user)
        planning_request = PlanningRequestFactory.create(
            type=PlanningRequest.SHORT_TERM,
            created_by=self.user
        )
        DeliveryFactory.create_batch(
            5,
            planning_request=planning_request
        )
        response = self.client.get(self.url)

        # Assertions
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, self.template)
        self.assertEqual(
            len(response.context['object_list']),
            5
        )
        self.assertNotContains(
            response,
            'Geen openstaande reguliere planningsverzoeken'
        )

    def test_request_with_LONG_TERM_type_pending_deliveries(self):
        """
        Test a request to the `PlanningView` view from authenticated
        user with `LONG_TERM` type `PENDING` deliveries
        """
        self.client.force_login(self.user)
        planning_request = PlanningRequestFactory.create(
            type=PlanningRequest.LONG_TERM,
            created_by=self.user
        )
        DeliveryFactory.create_batch(
            5,
            planning_request=planning_request
        )
        response = self.client.get(self.url)

        # Assertions
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, self.template)
        self.assertEqual(
            len(response.context['object_list']),
            5
        )
        self.assertNotContains(
            response,
            'Geen openstaande lange termijn planningsverzoeken'
        )


class ProcessTypeViewTest(TestCase):
    """
    Tests for `ProcessTypeView` view
    """
    fixtures = ['app_permissions']

    def setUp(self):
        self.user = UserFactory()
        app_permission = AppPermission.objects.get(app_name=GROUP_PLANNING)
        self.user.profile.app_permissions.add(app_permission)
        self.client = Client()
        self.url = reverse('planning:process-type')
        self.template = 'planning/components/modals/process_type.html'

    def test_request_as_anonymous_user(self):
        """
        Test a request to the `ProcessTypeView` from anonymous user
        """
        response = self.client.get(self.url)

        # Assertions
        self.assertEqual(response.status_code, 302)

    def test_request_as_authenticated_user(self):
        """
        Test a request to the `ProcessTypeView` from authenticated user
        """
        self.client.force_login(self.user)
        response = self.client.get(self.url)

        # Assertions
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'form')
        self.assertTemplateUsed(response, self.template)

    def test_post_request_for_definite_planning_request_choice(self):
        self.client.force_login(self.user)
        form_data = {'process': ProcessTypeForm.DEFINITE}
        response = self.client.post(self.url, form_data)

        # Assertions
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(
            response,
            reverse('planning:definite-request-create')
        )

    def test_post_request_for_option_planning_request_choices(self):
        self.client.force_login(self.user)
        form_data = {'process': ProcessTypeForm.OPTION}
        response = self.client.post(self.url, form_data)

        # Assertions
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(
            response,
            reverse('planning:option-request-create')
        )


class DefinitePlanningRequestCreateViewTest(TestCase):
    """
    Tests for `DefinitePlanningRequestCreateView` view
    """
    fixtures = ['app_permissions', 'roles']

    def setUp(self):
        self.user = UserFactory()
        app_permission = AppPermission.objects.get(app_name=GROUP_PLANNING)
        self.user.profile.app_permissions.add(app_permission)
        self.client = Client()
        self.url = reverse('planning:definite-request-create')
        self.template = 'planning/components/modals/planning_create_form.html'
        role = Role.objects.get(name=RoleName.ADMIN.value)
        admin = UserFactory()
        admin.profile.roles.add(role)

    def test_request_as_anonymous_user(self):
        """
        Test a request to the `PlanningRequestCreateView` view from
        anonymous user
        """
        response = self.client.get(self.url)

        # Assertions
        self.assertEqual(response.status_code, 302)

    def test_request_as_authenticated_user(self):
        """
        Test a request to the `PlanningRequestCreateView` view from
        authenticated user
        """
        self.client.force_login(self.user)
        response = self.client.get(self.url)

        # Assertions
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'formset')
        self.assertTemplateUsed(response, self.template)

    def test_request_when_an_IMMEDIATE_type_delivery_is_created(self):
        """
        Test a request to the `PlanningRequestCreateView` view from
        authenticated user when `IMMEDIATE` type delivery is created
        """
        self.client.force_login(self.user)
        today = timezone.now().date()
        date0 = today + datetime.timedelta(days=7)
        date1 = today + datetime.timedelta(days=120)
        date2 = today + datetime.timedelta(days=200)

        form_data = {
            # management_form
            'form-INITIAL_FORMS': '0',
            'form-TOTAL_FORMS': '3',
            'form-MIN_NUM_FORMS': '0',
            'form-MAX_NUM_FORMS': '1000',

            # Form data 1
            'form-0-date': date0.strftime('%Y-%m-%d'),
            'form-0-organization': 'Test Organization 0',
            'form-0-time': '07:00 - 10:00',
            'form-0-location': 'Test Location 0',
            'form-0-description': 'Test description 0',
            'form-0-notes': 'Test notes 1',
            'form-0-trainer_amount': 1,

            # Form data 2
            'form-1-date': date1.strftime('%Y-%m-%d'),
            'form-1-organization': 'Test Organization 1',
            'form-1-time': '07:00 - 10:00',
            'form-1-location': 'Test Location 1',
            'form-1-description': 'Test description 1',
            'form-1-notes': 'Test notes 1',
            'form-1-trainer_amount': 1,

            # Form data 3
            'form-2-date': date2.strftime('%Y-%m-%d'),
            'form-2-organization': 'Test Organization 2',
            'form-2-time': '07:00 - 10:00',
            'form-2-location': 'Test Location 2',
            'form-2-description': 'Test description 2',
            'form-2-notes': 'Test notes 2',
            'form-2-trainer_amount': 1,
        }
        response = self.client.post(self.url, form_data, follow=True)

        # Assertions
        self.assertEqual(response.status_code, 200)
        self.assertEqual(PlanningRequest.objects.count(), 1)
        self.assertEqual(Delivery.objects.count(), 3)
        self.assertEqual(Email.objects.count(), 1)
        self.assertRedirects(response, reverse('planning:overview'))
        self.assertContains(response, 'Stuur daarom Ilona een SMS.')


class OptionPlanningRequestViewTest(TestCase):
    """
    Tests for `OptionPlanningRequestView` view
    """
    fixtures = ['app_permissions', 'roles']

    def setUp(self):
        self.user = UserFactory()
        app_permission = AppPermission.objects.get(app_name=GROUP_PLANNING)
        self.user.profile.app_permissions.add(app_permission)
        self.client = Client()
        self.url = reverse('planning:option-request-create')
        self.template = 'planning/components/modals/planning_create_form.html'
        role = Role.objects.get(name=RoleName.ADMIN.value)
        admin = UserFactory()
        admin.profile.roles.add(role)

    def test_request_as_anonymous_user(self):
        """
        Test a request to `OptionPlanningRequestView` from anonymous user
        """
        response = self.client.get(self.url)

        # Assertions
        self.assertEqual(response.status_code, 302)

    def test_request_as_authenticated_user(self):
        """
        Test a request to `OptionPlanningRequestView` from authenticated
        user
        """
        self.client.force_login(self.user)
        response = self.client.get(self.url)

        # Assertions
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'formset')
        self.assertTemplateUsed(response, self.template)

    def test_request_with_POST_form_data(self):
        """
        Test POST request to `OptionPlanningRequestCreateView` to
        create an option planning request instance.
        """
        self.client.force_login(self.user)
        today = timezone.now().date()
        date0 = today + datetime.timedelta(days=7)
        date1 = today + datetime.timedelta(days=120)
        date2 = today + datetime.timedelta(days=200)

        form_data = {
            # management_form
            'form-INITIAL_FORMS': '0',
            'form-TOTAL_FORMS': '3',
            'form-MIN_NUM_FORMS': '0',
            'form-MAX_NUM_FORMS': '1000',

            # Form data 1
            'form-0-date': date0.strftime('%Y-%m-%d'),
            'form-0-organization': 'Test Organization 0',
            'form-0-time': '07:00 - 10:00',
            'form-0-location': 'Test Location 0',
            'form-0-description': 'Test description 0',
            'form-0-notes': 'Test notes 1',
            'form-0-trainer_amount': 1,

            # Form data 2
            'form-1-date': date1.strftime('%Y-%m-%d'),
            'form-1-organization': 'Test Organization 1',
            'form-1-time': '07:00 - 10:00',
            'form-1-location': 'Test Location 1',
            'form-1-description': 'Test description 1',
            'form-1-notes': 'Test notes 1',
            'form-1-trainer_amount': 1,

            # Form data 3
            'form-2-date': date2.strftime('%Y-%m-%d'),
            'form-2-organization': 'Test Organization 2',
            'form-2-time': '07:00 - 10:00',
            'form-2-location': 'Test Location 2',
            'form-2-description': 'Test description 2',
            'form-2-notes': 'Test notes 2',
            'form-2-trainer_amount': 1,
        }
        response = self.client.post(self.url, form_data, follow=True)

        # Assertions
        self.assertEqual(response.status_code, 200)
        self.assertEqual(PlanningRequest.objects.count(), 1)
        self.assertEqual(Delivery.objects.count(), 3)
        self.assertEqual(Email.objects.count(), 1)
        self.assertRedirects(response, reverse('planning:overview'))


class PlanningRequestDetailViewTest(TestCase):
    """
    Tests for `PlanningRequestDetailView` view
    """
    fixtures = ['app_permissions', 'roles']

    def setUp(self):
        role = Role.objects.get(name=RoleName.ADMIN.value)
        admin = UserFactory()
        admin.profile.roles.add(role)

        self.user = UserFactory()
        app_permission = AppPermission.objects.get(app_name=GROUP_PLANNING)
        self.user.profile.app_permissions.add(app_permission)
        self.client = Client()
        self.template = 'planning/components/modals/' \
                        'planning_request_detail.html'

    def test_request_as_anonymous_user(self):
        """
        Test a request to the `PlanningRequestDetailView` view from
        anonymous user
        """
        planning_request = PlanningRequestFactory(
            status=PlanningRequest.PENDING
        )
        url = reverse(
            'planning:planning-request-detail',
            args=[planning_request.pk]
        )
        response = self.client.get(url)

        # Assertions
        self.assertEqual(response.status_code, 302)

    def test_request_as_authenticated_user(self):
        """
        Test a request to the `PlanningRequestDetailView` view from
        anonymous user
        """
        self.client.force_login(self.user)
        planning_request = PlanningRequestFactory(
            status=PlanningRequest.PENDING
        )
        url = reverse(
            'planning:planning-request-detail',
            args=[planning_request.pk]
        )
        response = self.client.get(url)

        # Assertions
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, self.template)
        self.assertTrue('planning_request' in response.context)
        self.assertTrue('formset' in response.context)
        self.assertTrue('employee_list' in response.context)

    def test_post_data_with_new_trainers_selected(self):
        """
        Ensure confirmation email is sent when new trainers are assigned.
        """
        self.client.force_login(self.user)
        planning_request = PlanningRequestFactory(
            status=PlanningRequest.PENDING
        )
        delivery = DeliveryFactory(planning_request=planning_request)

        role = Role.objects.get(name=RoleName.TRAINER.value)
        trainer1 = UserFactory()
        trainer1.profile.roles.add(role)
        trainer2 = UserFactory()
        trainer2.profile.roles.add(role)

        form_data = {
            # management_form
            'form-INITIAL_FORMS': '0',
            'form-TOTAL_FORMS': '1',
            'form-MIN_NUM_FORMS': '0',
            'form-MAX_NUM_FORMS': '1000',

            # Form data 1
            'form-0-delivery': delivery.pk,
            'form-0-employee': [trainer1.pk, trainer2.pk]
        }

        url = reverse(
            'planning:planning-request-detail',
            args=[planning_request.pk]
        )
        response = self.client.post(url, form_data, follow=True)

        # Assertions
        expected_url = reverse('planning:overview')
        self.assertRedirects(response, expected_url)
        self.assertEqual(delivery.schedules.count(), 2)
        self.assertEqual(Email.objects.count(), 2)

    def test_post_data_with_existing_confirmed_trainers(self):
        """
        Ensure no trainer confirmation email is sent if the trainers already
        confirmed the planning request. Also ensure that `is_confirmed` status
        of the schedule remains `True`.
        """
        self.client.force_login(self.user)
        planning_request = PlanningRequestFactory()
        delivery = DeliveryFactory(planning_request=planning_request)
        schedule1 = ScheduleFactory(delivery=delivery, is_confirmed=True)
        schedule2 = ScheduleFactory(delivery=delivery, is_confirmed=True)

        form_data = {
            # management_form
            'form-INITIAL_FORMS': '0',
            'form-TOTAL_FORMS': '1',
            'form-MIN_NUM_FORMS': '0',
            'form-MAX_NUM_FORMS': '1000',

            # Form data 1
            'form-0-delivery': delivery.pk,
            'form-0-employee': [schedule1.user.pk, schedule2.user.pk]
        }

        url = reverse(
            'planning:planning-request-detail',
            args=[planning_request.pk]
        )
        response = self.client.post(url, form_data, follow=True)

        # Assertions
        expected_url = reverse('planning:overview')
        self.assertRedirects(response, expected_url)
        self.assertEqual(delivery.schedules.count(), 2)
        self.assertEqual(Email.objects.count(), 0)


class AvailabilityDetailViewTest(TestCase):
    """
    Tests for `AvailabilityDetailView` view
    """
    fixtures = ['app_permissions', 'roles']

    def setUp(self):
        self.user = UserFactory()
        app_permission = AppPermission.objects.get(app_name=GROUP_PLANNING)
        self.user.profile.app_permissions.add(app_permission)
        self.client = Client()
        self.template = 'planning/components/modals/availability_detail.html'
        role = Role.objects.get(name=RoleName.TRAINER.value)
        trainers = UserFactory.create_batch(10)
        for trainer in trainers:
            trainer.profile.roles.add(role)

    def test_request_as_anonymous_user(self):
        """
        Test a request to the `AvailabilityDetailView` view from
        anonymous user
        """
        delivery = DeliveryFactory()
        url = reverse(
            'planning:availability-detail',
            args=[str(delivery.pk)]
        )
        response = self.client.get(url)

        # Assertions
        self.assertEqual(response.status_code, 302)

    def test_request_as_authenticated_user(self):
        """
        Test a request to the `AvailabilityDetailView` view from
        authenticated user
        """
        self.client.force_login(self.user)
        delivery = DeliveryFactory()
        url = reverse(
            'planning:availability-detail',
            args=[str(delivery.pk)]
        )
        response = self.client.get(url)

        # Assertions
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['delivery'], delivery)
        self.assertTemplateUsed(response, self.template)
        self.assertTrue('trainer_list' in response.context)
        self.assertEqual(len(response.context['trainer_list']), 10)


class PlanningRequestConfirmView(TestCase):
    """
    Tests for `PlanningRequestConfirmView` view
    """
    fixtures = ['app_permissions', 'roles']

    def setUp(self):
        role = Role.objects.get(name=RoleName.ADMIN.value)
        admin = UserFactory()
        admin.profile.roles.add(role)

        role = Role.objects.get(name=RoleName.TRAINER.value)
        self.trainer = UserFactory()
        self.trainer.profile.roles.add(role)

        self.user = UserFactory()
        app_permission = AppPermission.objects.get(app_name=GROUP_PLANNING)
        self.user.profile.app_permissions.add(app_permission)
        self.client = Client()
        self.planning_request = PlanningRequestFactory()
        delivery = DeliveryFactory(planning_request=self.planning_request)
        self.schedule = ScheduleFactory(
            user=self.trainer,
            delivery=delivery
        )

    def test_request_as_anonymous_user(self):
        """
        Test a request to the `PlanningRequestConfirmView` view from
        anonymous user
        """
        url = reverse(
            'planning:planning-request-confirm',
            args=[self.planning_request.pk, self.trainer.pk]
        )
        response = self.client.get(url)

        # Assertions
        self.assertEqual(response.status_code, 200)

    def test_request_as_authenticated_user(self):
        """
        Test a request to the `PlanningRequestConfirmView` view from
        authenticated user
        """
        self.client.force_login(self.user)
        url = reverse(
            'planning:planning-request-confirm',
            args=[self.planning_request.pk, self.trainer.pk]
        )
        response = self.client.get(url)

        # Assertions
        self.assertEqual(response.status_code, 200)


class PlanningRequestAdminConfirmView(TestCase):
    """
    Tests for `PlanningRequestAdminConfirmView` view
    """
    fixtures = ['app_permissions']

    def setUp(self):
        self.user = User.objects.create_user(
            email='test@test.mail',
            username='test_user',
            password='password'
        )
        app_permission = AppPermission.objects.get(app_name=GROUP_PLANNING)
        self.user.profile.app_permissions.add(app_permission)
        self.client = Client()
        self.planning_request = PlanningRequestFactory()
        DeliveryFactory(planning_request=self.planning_request)

    def test_request_as_anonymous_user(self):
        """
        Test a request to the `PlanningRequestAdminConfirmView` view
        from anonymous user
        """
        url = reverse(
            'planning:planning-request-admin-confirm',
            args=[self.planning_request.pk]
        )
        response = self.client.get(url)

        # Assertions
        self.assertEqual(response.status_code, 200)

    def test_request_as_authenticated_user(self):
        """
        Test a request to the `PlanningRequestAdminConfirmView` view
        from authenticated user
        """
        self.client.force_login(self.user)
        url = reverse(
            'planning:planning-request-admin-confirm',
            args=[self.planning_request.pk]
        )
        response = self.client.get(url)

        # Assertions
        self.assertEqual(response.status_code, 200)


class PlanningRequestDeleteViewTest(TestCase):
    """
    Tests for `PlanningRequestDeleteView` view
    """
    fixtures = ['app_permissions']

    def setUp(self):
        self.user = UserFactory()
        app_permission = AppPermission.objects.get(app_name=GROUP_PLANNING)
        self.user.profile.app_permissions.add(app_permission)
        self.client = Client()
        self.planning_request = PlanningRequestFactory()
        self.url = reverse(
            'planning:planning-request-delete',
            args=[self.planning_request.pk]
        )
        self.template = 'planning/components/modals/planning_request_delete.html'

    def test_request_as_anonymous_user(self):
        """
        Test a request to the `PlanningRequestDeleteView` view
        from anonymous user.
        """
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 302)

    def test_request_as_authenticated_user(self):
        """
        Test a request to the `PlanningRequestDeleteView` view
        from authenticated user.
        """
        self.client.force_login(self.user)
        response = self.client.get(self.url)

        # Assertions
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, self.template)


class FinalizeOptionPlanningRequestViewTests(TestCase):
    """
    Tests for `FinalizeOptionPlanningRequestView` class
    """
    fixtures = ['app_permissions', 'roles']

    def setUp(self):
        role = Role.objects.get(name=RoleName.ADMIN.value)
        admin = UserFactory()
        admin.profile.roles.add(role)

        self.user = UserFactory()
        app_permission = AppPermission.objects.get(app_name=GROUP_PLANNING)
        self.user.profile.app_permissions.add(app_permission)
        self.planning_request = PlanningRequestFactory(
            type=PlanningRequest.OPTION,
            status=PlanningRequest.READY
        )
        delivery = DeliveryFactory(
            planning_request=self.planning_request,
            trainer_amount=2
        )
        self.schedule1 = ScheduleFactory(delivery=delivery, is_confirmed=True)
        self.schedule2 = ScheduleFactory(delivery=delivery, is_confirmed=True)

        self.url = reverse(
            'planning:finalize-pending-request',
            args=[self.planning_request.pk]
        )
        self.client = Client()

    def test_get_request_with_anonymous_user(self):
        """
        Ensure anonymous users have no access to the view.
        """
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 302)

    def test_get_request_with_authenticated_user(self):
        """
        Ensure GET request is not allowed even with an
        authenticated user.
        """
        self.client.force_login(self.user)
        response = self.client.get(self.url)

        self.assertEqual(response.status_code, 405)

    def test_post_request_with_authenticated_user(self):
        """
        Ensure sending a POST request to the view:
         - Converts the planning request type to definite
         - Converts the planning request status to `WAITING_FOR_APPROVAL`.
         - Remove trainer schedule confirmations
         - Resend trainer confirmation e-mails
         - Send new planning request e-mail
        """
        self.client.force_login(self.user)
        response = self.client.post(self.url, follow=True)

        self.planning_request.refresh_from_db()
        self.schedule1.refresh_from_db()
        self.schedule2.refresh_from_db()

        # Assertions
        self.assertEqual(response.status_code, 200)
        self.assertNotEqual(self.planning_request.type, PlanningRequest.OPTION)
        self.assertEqual(
            self.planning_request.status,
            PlanningRequest.WAITING_FOR_APPROVAL
        )
        self.assertFalse(self.schedule1.is_confirmed)
        self.assertFalse(self.schedule2.is_confirmed)
        self.assertEqual(Email.objects.count(), 2)
