from django.conf.urls import url

from .views import OverviewView, FollowUpDetailView, FollowUpDeleteView, \
    FollowUpSetupView, FollowUpEventCreateView, FollowUpSetupStep1View, \
    FollowUpSetupStep2View, FollowUpSetupStep3View, InfographicDownloadView

urlpatterns = [
    url(r'^$', OverviewView.as_view(), name='overview'),
    url(
        r'^(?P<pk>[0-9a-f-]+)/$',
        FollowUpDetailView.as_view(),
        name='followup-detail'
    ),
    url(
        r'^(?P<pk>[0-9a-f-]+)/delete/$',
        FollowUpDeleteView.as_view(),
        name='followup-delete'
    ),
    url(
        r'^(?P<pk>[0-9a-f-]+)/setup/$',
        FollowUpSetupView.as_view(),
        name='followup-setup'
    ),
    url(
        r'^(?P<pk>[0-9a-f-]+)/setup/step-1/$',
        FollowUpSetupStep1View.as_view(),
        name='followup-setup-step1'
    ),
    url(
        r'^(?P<pk>[0-9a-f-]+)/setup/step-2/$',
        FollowUpSetupStep2View.as_view(),
        name='followup-setup-step2'
    ),
    url(
        r'^(?P<pk>[0-9a-f-]+)/setup/step-3/$',
        FollowUpSetupStep3View.as_view(),
        name='followup-setup-step3'
    ),
    url(
        r'^(?P<pk>[0-9a-f-]+)/event-create/$',
        FollowUpEventCreateView.as_view(),
        name='followup-event-create'
    ),
    url(
        r'^infographic-download/(?P<pk>[a-zA-Z0-9-]+)/$',
        InfographicDownloadView.as_view(),
        name='infographic-download'
    ),
]
