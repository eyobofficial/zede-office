from django.db.models import Q
from django.db.models.signals import post_save
from django.dispatch import receiver

from debatnl_registration.models import Participant, Attendance
from debatnl_registration.tasks import generate_review_training_notification
from feedback.models import JotFormTrainingReview


@receiver(post_save, sender=JotFormTrainingReview)
def send_review_training_email(sender, instance, created, **kwargs):
    score = instance.score if instance.score else 0

    if created and score > 8:
        try:
            participant = Participant.objects.get(
                Q(has_feedback=False),
                Q(name=instance.reviewer_name) |
                Q(email=instance.reviewer_email)
            )

            training_date_count = Attendance.objects.filter(
                training_date__date=instance.training.date,
                participant=participant
            ).count()

            generate_review_training_notification(
                participant,
                instance.submission_date,
                training_date_count
            )

        except (Participant.DoesNotExist, Participant.MultipleObjectsReturned):
            pass
