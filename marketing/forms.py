from django.forms import BaseInlineFormSet, inlineformset_factory

from marketing.models import PageScoreReport, PageScore


class BasePageScoreReportFormSet(BaseInlineFormSet):
    def save(self, commit=False):
        if self.files:
            for file in self.files.getlist('files'):
                self.instance.reports.create(file=file)
        return super().save(commit)


PageScoreReportFormSet = inlineformset_factory(
    PageScore,
    PageScoreReport,
    fields=('page_score', 'file'),
    formset=BasePageScoreReportFormSet,
    extra=0, can_delete=True
)
