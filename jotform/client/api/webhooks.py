from jotform.client.api.base import JOTFormAPI
from jotform.client.objects import Webhook


class Webhooks(JOTFormAPI):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.url = 'form/{}/webhooks'.format(self._form_id)

    def list(self):
        response = self._get(self.url)
        return self._parse_response(response)

    def create(self, data):
        response = self._post(self.url, data=data)
        return self._parse_response(response)

    def delete(self, hook_id):
        url = '{}/{}'.format(self.url, hook_id)
        response = self._delete(url)
        return self._parse_response(response)

    @staticmethod
    def _parse_response(response):
        webhooks = []
        content = response['content']

        if len(content) > 0:
            for k, v in content.items():
                webhooks += [Webhook.construct_from({'wid': k, 'url': v})]

        return webhooks
