import datetime

from feedback.models import JotFormTrainer, JotFormTraining, \
    JotFormTrainingReview
from jotform.models import Question, Submission, Answer


class FeedbackFormUpdater:
    form_id = 70395163921962

    def _init_trainers_question(self):
        kwargs = {'form_id': self.form_id, 'name': 'trainers'}
        self.trainers_question = Question.objects.get(**kwargs)

    def _get_submissions_by_trainer(self, trainer):
        """ Retrieve the submissions by trainer.

        :param trainer: (JotFormTrainer) Trainer object.
        """
        self._init_trainers_question()
        name = trainer.get_full_name()
        kwargs = {
            'form_id': self.form_id,
            'answers__qid': self.trainers_question.qid,
            'answers__answer__contains': name
        }
        return Submission.objects.filter(**kwargs).all()

    def _update_training_dates_by_trainer(self, trainer):
        submissions = self._get_submissions_by_trainer(trainer)

        # Get the question ID for the training date.
        question = Question.objects.get(name='datum', form_id=self.form_id)

        # Retrieve unique training dates from JOTForm data.
        dates = set()
        for submission in submissions:
            answer = submission.answers.get(qid=question.qid)
            answer = self._parse_date_from_answer(answer.answer)
            training_date = datetime.date(**answer)
            dates.add(training_date)

        # Sort training days in ascending order and persist the dates.
        for training_date in dates:
            kwargs = {'date': training_date, 'trainer': trainer}
            JotFormTraining.objects.get_or_create(**kwargs)

    def _update_reviews_by_trainer(self, trainer):
        submissions = self._get_submissions_by_trainer(trainer)

        for index, submission in enumerate(submissions):
            score = self._get_training_score(submission)
            training_date = submission.answers.get(qid=10).answer
            training_date = self._parse_date_from_answer(training_date)
            training_date = datetime.date(**training_date)
            training = JotFormTraining.objects.get(date=training_date,
                                                   trainer=trainer)

            kwargs = {
                'training': training,
                'reviewer_name': submission.answers.get(qid=7).answer,
                'reviewer_email': submission.answers.get(qid=11).answer,
                'submission_date': submission.created_at
            }

            review = self._get_or_create_review(**kwargs)
            review.comment = submission.answers.get(qid=5).answer
            review.score = int(score) if score else None
            review.resolution = submission.answers.get(qid=15).answer
            review.save()

    def update(self):
        self._init_trainers_question()
        trainer_names = self.trainers_question.options.split('|')
        trainer_names = list(filter(None, trainer_names))

        for trainer_name in trainer_names:
            names = trainer_name.split(' ')
            kwargs = {'first_name': names[0], 'last_name': ' '.join(names[1:])}
            trainer, _ = JotFormTrainer.objects.get_or_create(**kwargs)
            self._update_training_dates_by_trainer(trainer)
            self._update_reviews_by_trainer(trainer)

    @staticmethod
    def _get_training_score(submission):
        score = submission.answers.get(qid=4).answer.strip()
        if not score:
            try:
                score = submission.answers.get(qid=18).answer.strip()
            except Answer.DoesNotExist:
                pass
        return score

    @staticmethod
    def _parse_date_from_answer(answer):
        answer = eval(answer)
        answer['year'] = int(answer['year'])
        answer['month'] = int(answer['month'])
        answer['day'] = int(answer['day'])
        return answer

    @staticmethod
    def _get_or_create_review(**kwargs):
        """ Get or create training reviews.
        Note: This method can be simplified in the future. This was only
        meant to also remove duplicate entries.
        """
        try:
            review, _ = JotFormTrainingReview.objects.get_or_create(**kwargs)
        except JotFormTrainingReview.MultipleObjectsReturned:
            reviews = JotFormTrainingReview.objects.filter(**kwargs)
            review = reviews.first()
            reviews.exclude(pk=review.training_review_id).delete()

        return review
