from intakes.emails.bases import BaseIntakeEmail


class IntakeTrainerDateNotificationEmail(BaseIntakeEmail):
    def __init__(self, intake):
        super().__init__(intake)
        self.template_name = 'email_5_trainer_date_notification.html'
        self.subject = 'Deadline intake {}'.format(self.intake.organization)

    def send(self):
        for trainer in self.intake.trainers.all():
            self.recipient = trainer
            super().send()


class IntakeTrainerDateChangeNotificationEmail(BaseIntakeEmail):
    def __init__(self, intake):
        super().__init__(intake)
        self.template_name = 'email_6_trainer_date_change_notification.html'
        self.subject = 'Deadline intake {} gewijzigd'
        self.subject = self.subject.format(self.intake.organization)

    def send(self):
        for trainer in self.intake.trainers.all():
            self.recipient = trainer
            super().send()
