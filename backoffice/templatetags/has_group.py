from django import template

from users.models import UserProfile

register = template.Library()


@register.filter(name='has_group')
def has_group(user, group_name):
    try:
        kwargs = {'app_name': group_name}
        return user.profile.app_permissions.filter(**kwargs).exists()

    except (UserProfile.DoesNotExist, AttributeError):
        return False


@register.filter(name='has_permission')
def has_permission(user, permission_name):
    return user.user_permissions.filter(name=permission_name).exists()
