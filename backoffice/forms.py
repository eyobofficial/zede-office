from django import forms


class SplitTimeWidget(forms.MultiWidget):
    template_name = 'forms/widgets/split_time.html'

    def __init__(self, attrs=None):
        hours = [(h, f'{h:02d}') for h in range(0, 24)]
        minutes = [(f'{m:02d}', f'{m:02d}') for m in range(0, 60, 5)]
        widgets = [
            forms.Select(attrs=attrs, choices=hours),
            forms.Select(attrs=attrs, choices=minutes)
        ]
        super().__init__(widgets, attrs)

    def decompress(self, value):
        if value:
            return [value.hour, value.minute]
        return [None, None]


class SplitTimeField(forms.MultiValueField):
    widget = SplitTimeWidget

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields = (
            forms.CharField(),
            forms.CharField()
        )

    def compress(self, data_list):
        return ':'.join(data_list)
