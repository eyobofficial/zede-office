#!/bin/bash

server=$1

if [ "$server" == "test" ]
then
    apt-get update -y
    apt-get install cron nano -y
    service cron start

    crontab -l > mycron
    echo "0 1 * * * cd / && ./scheduled_tasks.sh test >> /logs/cron.log 2>&1" >> mycron
    echo "0 * * * * cd / && ./scheduled_emails.sh test >> /logs/cron.log 2>&1" >> mycron
    echo "0 8 * * * cd / && ./scheduled_outtakes.sh test >> /logs/cron.log 2>&1" >> mycron
    crontab mycron
    rm mycron

    echo "Preparation Done!"
else
    # Activate virtualenv
    source `which virtualenvwrapper.sh`
    workon debatnl_backoffice

    # Install project dependencies
    yarn install
    pip install -r requirements.txt

    # Run Django migrations
    python manage.py migrate --noinput

    # Collecting Static Files
    python manage.py collectstatic --clear --noinput

    # Compile SASS Files
    python manage.py compilescss --delete-files

    echo "Preparation Done!"
fi


