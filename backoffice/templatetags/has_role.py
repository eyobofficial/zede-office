from django import template

register = template.Library()


@register.filter
def has_role(users, role_name):
    kwargs = {'profile__roles__name__iexact': role_name}
    return users.filter(**kwargs).all()
