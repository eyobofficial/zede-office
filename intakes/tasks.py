import pendulum
from celery.schedules import crontab
from celery.task import periodic_task
from django.conf import settings

from DebatNL_BackOffice.celery import app
from intakes.emails.reminders import IntakeReminderEmail, \
    IntakeOrganizationInsightReportEmail, IntakeTrainerInsightReportEmail, \
    IntakeCheckReminderEmail
from intakes.models import IntakeReminder, Intake
from jotform.helpers import sync_form, sync_questions, sync_submissions, \
    sync_webhooks
from jotform.models import Webhook


@periodic_task(run_every=crontab(hour=0, minute=00))
def process_intake_status():
    intakes = Intake.objects.filter(status=Intake.ONGOING).all()

    for intake in intakes:
        if intake.is_finished():
            intake.status = Intake.FINISHED
            intake.save()


@periodic_task(run_every=crontab(hour=10, minute=00))
def process_intake_reminders_of_today():
    today = pendulum.today(settings.TIME_ZONE).date()
    reminders = IntakeReminder.objects.filter(
        date=today, status=IntakeReminder.PLANNED).all()

    for reminder in reminders:
        IntakeReminderEmail(reminder.intake).send()
        reminder.status = IntakeReminder.SENT
        reminder.save()


@periodic_task(run_every=crontab(hour=10, minute=00))
def process_report_reminders_of_today():
    today = pendulum.today(settings.TIME_ZONE).date()
    intakes = Intake.objects.filter(report_date=today,
                                    report_status=Intake.REPORT_PLANNED).all()

    for intake in intakes:
        IntakeOrganizationInsightReportEmail(intake).send()
        intake.report_status = Intake.REPORT_SENT
        intake.save()


@periodic_task(run_every=crontab(hour=10, minute=00))
def process_submission_reminders_of_today():
    today = pendulum.today(settings.TIME_ZONE).date()
    intakes = Intake.objects.filter(
        trainers_report_date=today,
        trainers_report_status=Intake.REPORT_PLANNED
    ).all()

    for intake in intakes:
        IntakeTrainerInsightReportEmail(intake).send()
        intake.trainers_report_status = Intake.REPORT_SENT
        intake.save()


@periodic_task(run_every=crontab(hour=10, minute=00))
def process_intake_check_reminder():
    today = pendulum.today(settings.TIME_ZONE)
    check_date = today.add(days=4).date()
    intakes = Intake.objects.filter(report_date=check_date).all()

    for intake in intakes:
        if intake.form.submissions.count() == 0:
            IntakeCheckReminderEmail(intake).send()


@periodic_task(run_every=crontab(hour=1, minute=00))
def clean_up_dated_intakes():
    """
    Clean up intakes that are FINISHED for longer than 4 weeks.
    """
    today = pendulum.today(settings.TIME_ZONE)
    updated_at = today.subtract(weeks=4)
    Intake.objects.filter(status=Intake.FINISHED,
                          updated_at__lt=updated_at).delete()


@app.task
def refresh_form_data(form_id):
    url = settings.JOT_FORM_AWS_SUBMISSION_WEBHOOK
    sync_questions(form_id)
    sync_form(form_id)
    sync_submissions(form_id)
    sync_webhooks(form_id)
    Webhook.objects.get_or_create(form_id=form_id, url=url)
