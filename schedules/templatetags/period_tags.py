import pendulum
from django import template
from django.conf import settings

register = template.Library()


@register.filter(name='period_date')
def filter_period_date(period_id, day):
    dt = pendulum.parse(f'{period_id}-{day}', tz=settings.TIME_ZONE)
    return dt.format('DD/MM/YYYY')
