import factory
from django.utils import timezone

from intakes.models import Intake, IntakeReportQuestion, IntakeReminder
from jotform.models import Answer
from jotform.tests.factories import FormFactory


class IntakeFactory(factory.django.DjangoModelFactory):
    organization = factory.Faker('company')
    cp_first_name = factory.Faker('first_name')
    cp_last_name = factory.Faker('last_name')
    cp_email = factory.LazyAttribute(lambda o: '{}@test.mail'.format(
        o.cp_first_name))
    qid_name = factory.LazyAttribute(lambda o: Answer.objects.filter(
        submission__form=o.form).first().qid)
    form = factory.SubFactory(FormFactory)

    class Meta:
        model = Intake


class IntakeReminderFactory(factory.django.DjangoModelFactory):
    date = timezone.now().date()

    class Meta:
        model = IntakeReminder


class ReportQuestionFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = IntakeReportQuestion
