describe('Tests for debatnl_mobile organizationW modals', () => {
  beforeEach(() => {
    cy.visit('/login/')
    cy.get('#id_username').type(Cypress.env('username'))
    cy.get('#id_password').type(Cypress.env('password'))
    cy.get('form').submit()
    cy.visit('/debatnl-mobile/')
  })

  it('Test organization create modal', () => {
    // Ensure clicking `Voeg opdrachtgever toe` button in the
    // `organization-overview` opens up organization create modal
    cy.get('.modal-container').parent('.reveal-overlay').should('not.be.visible')
    cy.get('#add-modal-btn')
      .contains('Voeg opdrachtgever toe')
      .click()

    cy.get('.modal-container')
      .find('h4')
      .should('contain', 'NIEUWE ORGANISATIE TOEVOEGEN')
      .should('not.contain', 'ORGANISATIE BEWERKEN')

    // Ensure submit button is initially disabled
    cy.get('.modal-container')
      .find('button[type=submit]')
      .should('be.disabled')

    // After typing organization name, ensure submit button is enabled
    cy.get('.modal-container')
      .find('#id_name')
      .type('Test Organization')
    cy.get('.modal-container')
      .find('button[type=submit]')
      .should('be', 'enabled')

    // Ensure the `Annuleren` button closes the modal
    cy.get('.modal-container')
      .find('button[type=button][data-close]')
      .first()
      .click()
    cy.get('.modal-container').should('not.be.visible')
  })

  it('Test organization update modal', () => {
    // Ensure clicking `Bewerken` button in the `organization-overview`
    // opens up organization update modal
    cy.get('.modal-container')
      .parent('.reveal-overlay')
      .should('not.be.visible')
    cy.get('.update-modal-btn').first()
      .contains('Bewerke')
      .click()
    cy.get('.modal-container')
      .parent('.reveal-overlay')
      .should('be.visible')

    cy.get('.modal-container')
      .find('h4')
      .should('contain', 'ORGANISATIE BEWERKEN')
      .should('not.contain', 'NIEUWE ORGANISATIE TOEVOEGEN')

    // Ensure submit button is initially disabled
    cy.get('.modal-container')
      .find('button[type=submit]')
      .should('be.disabled')

    // After typing organization name, ensure submit button is enabled
    cy.get('.modal-container')
      .find('#id_name')
      .type('Updated Organization')
    cy.get('.modal-container')
      .find('button[type=submit]')
      .should('be', 'enabled')

    // Ensure the `Annuleren` button closes the modal
    cy.get('.modal-container')
      .find('button[type=button][data-close]')
      .first()
      .click()
    cy.get('.modal-container').should('not.be.visible')
  })

  it('Test organization delete modal', () => {
    // Ensure clicking `Verwijderen` button opens up the
    // organization delete modal.
    cy.get('.modal-container')
      .parent('.reveal-overlay')
      .should('not.be.visible')
    cy.get('.delete-modal-btn').first()
      .contains('Verwijderen')
      .click()

    cy.get('.modal-container')
      .parent('.reveal-overlay')
      .should('be.visible')

    // Ensure clicking the `Annuleren` button closes the modal
    cy.get('.modal-container')
      .find('.button-group')
      .find('button[type=button][data-close]')
      .click()
    cy.get('.modal-container')
      .parent('.reveal-overlay')
      .should('not.be.visible')
  })
})


describe('Tests for debatnl_mobile training modals', () => {
  beforeEach(() => {
    cy.visit('/login/')
    cy.get('#id_username').type(Cypress.env('username'))
    cy.get('#id_password').type(Cypress.env('password'))
    cy.get('form').submit()
    cy.visit('/debatnl-mobile/')
    cy.get('.detail-modal-btn').first().click() // Redirects to the related training overview page
  })

  it('Test clicking back button', () => {
    // Ensure clicking `Terug` button returns the user to
    // the organization overview
    cy.get('#back-btn').click()
    cy.url().should('eq', Cypress.config().baseUrl + '/debatnl-mobile/')
  })

  it('Test training create modal', () => {
    // Ensure clicking `Voeg training toe` opens up the
    // training create modal

    // Check if modal is initially invisible
    cy.get('.modal-container')
      .parent('.reveal-overlay')
      .should('not.be.visible')

    // Click button
    cy.get('#add-modal-btn')
      .should('contain', 'Voeg training toe')
      .click()

    // Check if modal is visible
    cy.get('.modal-container')
      .parent('.reveal-overlay')
      .should('be.visible')


    // Check if submit button is disabled by default
    cy.get('.modal-container button[type=submit]')
      .should('be.disabled')

    // Test name field
    cy.get('.modal-container #id_name')
      .type('Test training')
    cy.get('.modal-container button[type=submit]')
      .should('be.disabled')

    // Test date field
    cy.get('.modal-container #id_date')
      .type('01/02/2020\t')
    cy.get('.modal-container #alt-date')
      .should('have.value', '2020-02-01')
    cy.get('.modal-container button[type=submit]')
      .should('be.disabled')

    // Test `annuleren button` closes the modal
    cy.get('.modal-container .button[type=button][data-close]')
      .should('contain', 'Annuleren')
      .click()
    cy.get('.modal-container')
      .parent('.reveal-overlay')
      .should('not.be.visible')
  })

  it('Test training update modal', () => {
    // Ensure clicking `Bewerken` button opens a training
    // update modal

    // Check modal is initially invisible
    cy.get('.modal-container')
      .parent('.reveal-overlay')
      .should('not.be.visible')

    // Click `Bewerken` button
    cy.get('.update-modal-btn')
      .first().contains('Bewerken').click()

    // Check modal is opened
    cy.get('.modal-container')
      .parent('.reveal-overlay')
      .should('be.visible')

    // Check the button `Annuleren` closes the modal
    cy.get('.modal-container .button[type=button][data-close]')
      .should('contain', 'Annuleren')
      .click()

    // Check modal is closed
    cy.get('.modal-container')
      .parent('.reveal-overlay')
      .should('not.be.visible')
  })

  it('Test training delete modal', () => {
    // Check delete modal is initially closed
    cy.get('.modal-container')
      .parent('.reveal-overlay')
      .should('not.be.visible')

    // Click the `Verwijderen` button
    cy.get('.delete-modal-btn')
      .should('contain', 'Verwijderen')
      .click()

    // Check modal is opened
    cy.get('.modal-container')
      .parent('.reveal-overlay')
      .should('be.visible')

    // Check the button `Annuleren` closes the modal
    cy.get('.modal-container .button[type=button][data-close]')
      .should('contain', 'Annuleren')
      .click()

    // Check modal is closed
    cy.get('.modal-container')
      .parent('.reveal-overlay')
      .should('not.be.visible')
  })
})


describe('Tests for debatnl_mobile challenges modals', () => {
  beforeEach(() => {
    cy.visit('/login/')
    cy.get('#id_username').type(Cypress.env('username'))
    cy.get('#id_password').type(Cypress.env('password'))
    cy.get('form').submit()
    cy.visit('/debatnl-mobile/')
    cy.get('.detail-modal-btn').first().click()  // Redirects to the related training overview page
    cy.get('.detail-modal-btn').first().click()  // Redirects to the related challenges overview page
  })

  it('Test back button', () => {
    cy.get('#back-btn')
      .should('contain', 'Terug')
      .click()

    cy.url()
      .should('include', Cypress.config().baseUrl + '/debatnl-mobile/organizations/')
  })

  it('Test challenge create modal', () => {
    // Check modal is closed initially
    cy.get('.modal-container')
      .parent('.reveal-overlay')
      .should('not.be.visible')

    // Click button
    cy.get('#add-challenge-btn')
      .should('contain', 'Maak challenge aan')
      .click()

    // Check modal is opened
    cy.get('.modal-container')
      .parent('reveal-overlay')
      .should('not.be.visible')

    // Check submit button is initially disabled
    cy.get('.modal-container button[type=submit]')
      .should('contain', 'Opslaan')
      .and('be.disabled')

    // Check submit button is enabled after filling
    // required fields (i.e. title & order inputs)
    cy.get('.modal-container #id_title')
      .type('Test challenge title')
    cy.get('.modal-container #id_order')
      .type('123')

    cy.get('.modal-container button[type=submit]')
      .should('be.enabled')

    // Check button `Annuleren` closes the modal
    cy.get('.modal-container .button[type=button][data-close]')
      .should('contain', 'Annuleren')
      .click()

    cy.get('.modal-container')
      .parent('.reveal-overlay')
      .should('not.be.visible')
  })

  it('Test challenge update modal', () => {
    // Check modal is initially closed
    cy.get('.modal-container')
      .parent('.reveal-overlay')
      .should('not.be.visible')

    // Click button
    cy.get('#challenge-table .update-modal-btn')
      .first()
      .should('contain', 'Bewerken')
      .click()

    // Check modal is opened
    cy.get('.modal-container')
      .parent('.reveal-overlay')
      .should('be.visible')

    // Check submit button is initially disabled
    cy.get('.modal-container button[type=submit]')
      .should('contain', 'Opslaan')
      .and('be.disabled')

    // Check submit button is enabled when the
    // value of the form fields are modified.
    cy.get('.modal-container #id_title')
      .type(' - Modified')
    cy.get('.modal-container button[type=submit]')
      .should('be.enabled')

    // Check the button `Annuleren` closes the modal
    cy.get('.modal-container .button[type=button][data-close]')
      .should('contain', 'Annuleren')
      .click()

    cy.get('.modal-container')
      .parent('.reveal-overlay')
      .should('not.be.visible')
  })

  it('Test challenge delete modal', () => {
    // Check modal is initially closed
    cy.get('.modal-container')
      .parent('.reveal-overlay')
      .should('not.be.visible')

    // Click button
    cy.get('#challenge-table .delete-modal-btn')
      .first()
      .should('contain', 'Verwijderen')
      .click()

    // Check modal is opened now
    cy.get('.modal-container')
      .parent('.reveal-overlay')
      .should('be.visible')

    // Check the button `Annuleren` closes the modal
    cy.get('.modal-container .button[type=button][data-close]')
      .should('contain', 'Annuleren')
      .click()

    cy.get('.modal-container')
      .parent('.reveal-overlay')
      .should('not.be.visible')
  })
})


describe('Tests for debatnl_mobile participants modal', () => {
  beforeEach(() => {
    cy.visit('/login/')
    cy.get('#id_username').type(Cypress.env('username'))
    cy.get('#id_password').type(Cypress.env('password'))
    cy.get('form').submit()
    cy.visit('/debatnl-mobile/')
    cy.get('.detail-modal-btn').first().click()  // Redirects to the related training overview page
    cy.get('.detail-modal-btn').first().click()  // Redirects to the related challenges overview page
  })

  it('Test participant create modal', () => {
    // Check modal is closed initially
    cy.get('.modal-container')
      .parent('.reveal-overlay')
      .should('not.be.visible')

    // Click button
    cy.get('#add-participant-modal')
      .should('contain', 'Voeg deelnemer toe')
      .click()

    // Check modal is opened
    cy.get('.modal-container')
      .parent('.reveal-overlay')
      .should('be.visible')

    // Check submit button is initially disabled
    cy.get('.modal-container button[type=submit]')
      .should('contain', 'Opslaan')
      .and('be.disabled')

    // Check submit button is enabled when all the
    // required filled.
    cy.get('.modal-container #id_first_name').type('John')
    cy.get('.modal-container #id_last_name').type('Doe')
    cy.get('.modal-container #id_email').type('johndoe@test.email')

    cy.get('.modal-container button[type=submit]')
      .should('be.enabled')

    // Check `Annuleren` button closes modal
    cy.get('.modal-container .button[type=button][data-close]')
      .should('contain', 'Annuleren')
      .click()

    cy.get('.modal-container')
      .parent('.reveal-overlay')
      .should('not.be.visible')
  })

  it('Test participant update modal', () => {
    // Click button
    cy.get('#participant-table .update-modal-btn')
      .should('contain', 'Bewerken')
      .click()

    // Check button is opened
    cy.get('.modal-container')
      .parent('.reveal-overlay')
      .should('be.visible')

    // Check submit button is initially disabled
    cy.get('.modal-container button[type=submit]')
      .should('contain', 'Opslaan')
      .and('be.disabled')

    // Check submit button is enabled when form is modified
    cy.get('.modal-container #id_first_name').type(' Roe')
    cy.get('.modal-container button[type=submit]')
      .should('be.enabled')

    // Check `Annuleren` button closes modal
    cy.get('.modal-container .button[type=button][data-close]')
      .should('contain', 'Annuleren')
      .click()

    cy.get('.modal-container')
      .parent('.reveal-overlay')
      .should('not.be.visible')
  })

  it('Test participant delete modal', () => {
    // Check modal is initially closed
    cy.get('.modal-container')
      .parent('.reveal-overlay')
      .should('not.be.visible')

    // Click button
    cy.get('#participant-table .delete-modal-btn')
      .first()
      .should('contain', 'Verwijderen')
      .click()

    // Check modal is opened now
    cy.get('.modal-container')
      .parent('.reveal-overlay')
      .should('be.visible')

    // Check the button `Annuleren` closes the modal
    cy.get('.modal-container .button[type=button][data-close]')
      .should('contain', 'Annuleren')
      .click()

    cy.get('.modal-container')
      .parent('.reveal-overlay')
      .should('not.be.visible')
  })

  it('Test participant detail modal', () => {
    // Check modal is initially closed
    cy.get('.modal-container')
      .parent('.reveal-overlay')
      .should('not.be.visible')

    // Click button
    cy.get('#participant-table .detail-modal-btn')
      .first()
      .should('contain', 'Bekijken')
      .click()

    // Check modal is opened
    cy.get('.modal-container')
      .parent('.reveal-overlay')
      .should('be.visible')

    // Check `Terug` button in participanet detail modal
    // Closes the modal
    cy.get('.modal-container .button[type=button][data-close]')
      .should('contain', 'Terug')
      .click()

    cy.get('.modal-container')
      .parent('.reveal-overlay')
      .should('not.be.visible')
  })
})
