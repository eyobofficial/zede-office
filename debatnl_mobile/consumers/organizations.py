from .base import BaseConsumer


class OrganizationConsumer(BaseConsumer):
    uri = 'organizations'
