from django.contrib.auth.decorators import login_required, user_passes_test
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse, reverse_lazy

from debatnl_online.consumers.lessons import LessonTestQuestionConsumer, \
    LessonTestAnswerConsumer
from debatnl_online.permissions import group_check


@login_required
@user_passes_test(group_check, login_url=reverse_lazy('main_page'))
def view_add_lesson_test_question(request, course_id, chapter_id, lesson_id):
    if request.method == 'GET':
        context = {
            'lesson_id': lesson_id,
            'chapter_id': chapter_id,
            'course_id': course_id
        }
        return render(
            request,
            'debatnl_online/modals/lesson-questions/modal-create.html',
            context=context)

    if request.method == 'POST':
        question_consumer = LessonTestQuestionConsumer()
        question = question_consumer.create_object(lesson_id,
                                                   data=request.POST)

        answer_consumer = LessonTestAnswerConsumer()

        questions = []
        for n in range(1, 5):
            answer = request.POST.get('answer_{}'.format(n), '')

            if answer.strip():
                questions.append({
                    'question': question.get('id'),
                    'answer': request.POST.get('answer_{}'.format(n)),
                    'is_correct_answer': request.POST.get(
                        'answer_{}_is_correct'.format(n), False),
                    'sequence': n
                })

        answer_consumer.create_object(question.get('id'), questions)

    return HttpResponseRedirect(
        reverse('debatnl_online:e_learning:lesson', kwargs={
            'chapter_id': chapter_id,
            'course_id': course_id
        }))


@login_required
@user_passes_test(group_check, login_url=reverse_lazy('main_page'))
def view_change_lesson_test_question(request, course_id, chapter_id, lesson_id,
                                     question_id):
    if request.method == 'GET':
        consumer = LessonTestQuestionConsumer()
        question = consumer.retrieve_object(lesson_id, question_id)

        answers = question.get('answers')
        answers_count = len(answers)

        context = {
            'chapter_id': chapter_id,
            'course_id': course_id,
            'lesson_id': lesson_id,
            'question': question,
            'empty_answers': range(answers_count + 1, 5)
        }

        return render(request,
                      'debatnl_online/modals/lesson-questions/modal-edit.html',
                      context=context)

    if request.method == 'POST':
        consumer = LessonTestQuestionConsumer()
        consumer.update_object(lesson_id, question_id, request.POST)

        answer_consumer = LessonTestAnswerConsumer()

        questions = []
        for n in range(1, 5):
            answer = request.POST.get('answer_{}'.format(n), '')

            if answer.strip():
                questions.append({
                    'question': question_id,
                    'answer': answer,
                    'is_correct_answer': request.POST.get(
                        'answer_{}_is_correct'.format(n), False),
                    'sequence': n
                })

        answer_consumer.update_object(question_id, questions)

    return HttpResponseRedirect(
        reverse('debatnl_online:e_learning:lesson', kwargs={
            'chapter_id': chapter_id,
            'course_id': course_id
        }))


@login_required
@user_passes_test(group_check, login_url=reverse_lazy('main_page'))
def view_delete_lesson_test_question(request, course_id, chapter_id, lesson_id,
                                     question_id):
    if request.method == 'GET':
        consumer = LessonTestQuestionConsumer()
        question = consumer.retrieve_object(lesson_id, question_id)

        context = {
            'chapter_id': chapter_id,
            'course_id': course_id,
            'lesson_id': lesson_id,
            'question': question
        }
        return render(
            request,
            'debatnl_online/modals/lesson-questions/modal-delete.html',
            context=context)

    if request.method == 'POST':
        consumer = LessonTestQuestionConsumer()
        payload = {
            "is_deleted": True
        }
        consumer.update_object(lesson_id, question_id, payload)

    return HttpResponseRedirect(
        reverse('debatnl_online:e_learning:lesson', kwargs={
            'chapter_id': chapter_id,
            'course_id': course_id
        }))
