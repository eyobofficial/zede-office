import pendulum
from django.conf import settings
from django.core.exceptions import ValidationError

from DebatNL_BackOffice.celery import app
from feedback.emails import OuttakeIntroEmail, OuttakeReminderEmail
from feedback.models import TrainingOuttakeNotification, JotFormTraining, \
    TrainingOuttakeSubscriber
from feedback.updaters import FeedbackFormUpdater
from feedback.utilities.training_outtakes import activate_outtake_notifications
from jotform.helpers import sync_form, sync_submissions, sync_questions
from jotform.models import Form


@app.task
def send_outtake_reminders():
    """
    Send outtake reminders.
    """
    today = pendulum.today(settings.TIME_ZONE)
    notifications = TrainingOuttakeNotification.objects.filter(
        notification_date=today.date(),
        subscriber__status=TrainingOuttakeSubscriber.SUBSCRIBED
    ).all()

    for notification in notifications:
        try:
            if notification.notification_type == 'INTRO':
                OuttakeIntroEmail(notification.subscriber).send()

            if notification.notification_type == 'REMINDER':
                OuttakeReminderEmail(notification.subscriber).send()

        except ValidationError:
            pass


@app.task
def create_outtake_notifications():
    for training in JotFormTraining.objects.all():
        activate_outtake_notifications(training)


@app.task
def update_feedback_data():
    updater = FeedbackFormUpdater()
    sync_form(updater.form_id)
    sync_questions(updater.form_id)

    form = Form.objects.get(id=updater.form_id)

    if form.count != form.submissions.count():
        sync_submissions(updater.form_id)

    updater.update()
