from django.contrib.auth.models import User
from django.template.loader import render_to_string
from post_office import mail

from backoffice.constants import EMAIL_REMINDER, EMAIL_NOTIFICATION
from backoffice.models import Employee
from shared.enums import RoleName


class BaseEmail:
    template_location = None
    template_name = None
    recipient = None
    subject = None
    sender = None

    def get_sender(self):
        if not self.sender:
            kwargs = {
                'profile__roles__name': RoleName.ADMIN.value,
                'is_active': True
            }
            self.sender = User.objects.filter(**kwargs).first()

        name = f'{self.sender.first_name} {self.sender.last_name}'
        return f'{name} <{self.sender.email}>'

    def get_template(self):
        return f'{self.template_location}/{self.template_name}'

    def get_context_data(self, **kwargs):
        if 'email_obj' not in kwargs:
            kwargs['email_obj'] = self
        return kwargs

    def get_attachments(self, **kwargs):
        return kwargs

    def send(self):
        sender = self.get_sender()
        template = self.get_template()
        context = self.get_context_data()
        attachments = self.get_attachments()

        email = mail.send(
            recipients=self.recipient.email,
            sender=sender,
            subject=self.subject,
            html_message=render_to_string(template, context),
            attachments=attachments
        )

        return email


class ReminderEmailMixin:
    email_type = EMAIL_REMINDER


class NotificationEmailMixin:
    email_type = EMAIL_NOTIFICATION
