import datetime
import json

from django.contrib.auth.models import Permission
from django.test import TestCase, Client
from django.urls import reverse

from backoffice.constants import GROUP_FEEDBACK
from backoffice.tests.factories import UserFactory
from feedback.models import TrainingOuttake, \
    TrainingOuttakeSubscriber, TrainingOuttakeNotification
from feedback.tests.factories import JotFormTrainerFactory, \
    JotFormTrainingFactory, JotFormTrainingReviewFactory
from users.models import AppPermission


class FeedbackURLTest(TestCase):
    """
    Test the URL links of the offers app.
    """

    def test_non_public_urls_as_anonymous_user(self):
        non_public_urls = [
            reverse('feedback:overview'),
            reverse('feedback:tables_feedback'),
            reverse('feedback:overview_monthly_average_review_score'),
            reverse('feedback:activate-training-outtake',
                    kwargs={'training_id': 1}),
        ]

        for url in non_public_urls:
            with self.subTest(url=url):
                login_url = reverse('login')
                expected_url = f'{login_url}?next={url}'
                response = self.client.get(url, follow=True)
                self.assertRedirects(response, expected_url)

    def test_public_urls_as_anonymous_user(self):
        public_urls = [
            reverse('feedback:unsubscribe-training-outtake'),
        ]

        for url in public_urls:
            with self.subTest(url=url):
                response = self.client.get(url, follow=True)
                self.assertEqual(response.status_code, 200)


class FeedbackViewsTest(TestCase):
    """
    Test the views for the feedback app.
    """
    fixtures = ['app_permissions']

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.user = UserFactory()
        app_permission = AppPermission.objects.get(app_name=GROUP_FEEDBACK)
        cls.user.profile.app_permissions.add(app_permission)

        cls.trainer = JotFormTrainerFactory(first_name=cls.user.first_name,
                                            last_name=cls.user.last_name)

        JotFormTrainingFactory.create_batch(2)
        cls.training = JotFormTrainingFactory(trainer=cls.trainer)
        JotFormTrainingReviewFactory.create_batch(2, training=cls.training)

    def setUp(self):
        self.client = Client()
        self.client.force_login(self.user)

    def test_overview_without_all_trainers_permission(self):
        response = self.client.get(reverse('feedback:overview'))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.context['object_list']), 1)
        self.assertEqual(len(response.context['years']), 1)
        self.assertTemplateUsed(response, 'feedback/base.html')

    def test_overview_with_all_trainers_permission(self):
        permission = Permission.objects.get(name='Can see all trainers')
        self.user.user_permissions.add(permission)
        response = self.client.get(reverse('feedback:overview'))

        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.context['object_list']), 3)
        self.assertEqual(len(response.context['years']), 1)
        self.assertTemplateUsed(response, 'feedback/base.html')

    def test_overview_view_with_outdated_trainings(self):
        JotFormTrainingFactory(date=datetime.date(2017, 11, 1))
        permission = Permission.objects.get(name='Can see all trainers')
        self.user.user_permissions.add(permission)
        response = self.client.get(reverse('feedback:overview'))

        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.context['object_list']), 3)
        self.assertEqual(len(response.context['years']), 2)
        self.assertTemplateUsed(response, 'feedback/base.html')

    def test_tables_feedback_view(self):
        base_url = reverse('feedback:tables_feedback')
        trainer_id = self.trainer.id
        month = self.training.date.month
        year = self.training.date.year
        url = f'{base_url}?trainer_id={trainer_id}&month={month}&year={year}'

        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.context['object_list']), 1)
        self.assertTemplateUsed(response,
                                'feedback/components/tables-feedback.html')

    def test_overview_monthly_average_review_score_view(self):
        base_url = reverse('feedback:overview_monthly_average_review_score')
        trainer_id = self.trainer.id
        month = self.training.date.month
        year = self.training.date.year
        url = f'{base_url}?trainer_id={trainer_id}&month={month}&year={year}'
        response = self.client.get(url)
        response_data = json.loads(response.content)

        self.assertEqual(response.status_code, 200)
        self.assertIn('amount_submissions', response_data)
        self.assertIn('average_score', response_data)
        self.assertIn('month', response_data)
        self.assertIn('occurrences', response_data)

    def test_activate_training_outtake_view(self):
        """
        Test a request to the training outtake activation view from an
        authenticated user.
        """
        self.client.force_login(self.user)
        url_kwargs = {'training_id': self.training.id}
        url = reverse('feedback:activate-training-outtake', kwargs=url_kwargs)
        response = self.client.get(url, follow=True)

        self.assertEqual(response.status_code, 200)

        self.assertEqual(TrainingOuttake.objects.count(), 1)
        self.assertEqual(TrainingOuttakeSubscriber.objects.count(), 2)
        self.assertEqual(TrainingOuttakeNotification.objects.count(), 14)
        self.assertEqual(TrainingOuttakeNotification.objects.filter(
            notification_type='INTRO').count(), 2)
        self.assertEqual(TrainingOuttakeNotification.objects.filter(
            notification_type='REMINDER').count(), 12)
