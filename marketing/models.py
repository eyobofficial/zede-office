import os
import uuid
from unicodedata import normalize
from django.db import models
from offers.models import Offer


# SEO Models.
class Page(models.Model):
    title = models.CharField(default='', blank=True, max_length=150)
    url = models.CharField(default='', blank=True, max_length=255)
    updated_at = models.DateTimeField(auto_now=True, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.title


def hash_location(instance, filename):
    filename = normalize('NFKD', filename).encode('ascii', 'ignore')
    filename = filename.decode('UTF-8')
    return 'uploads/marketing/{}/{}'.format(str(uuid.uuid4()), filename)


class PageScore(models.Model):
    performance = models.IntegerField(default=0, blank=True)
    accessibility = models.IntegerField(default=0, blank=True)
    best_practice = models.IntegerField(default=0, blank=True)
    seo = models.IntegerField(default=0, blank=True)
    updated_at = models.DateTimeField(auto_now=True, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    page = models.ForeignKey(Page, related_name='scores')

    class Meta:
        ordering = ('-created_at', )

    def __str__(self):
        return f'{self.page.title} - {self.created_at}'


class PageScoreReport(models.Model):
    file = models.FileField(upload_to=hash_location)
    created_at = models.DateTimeField(auto_now_add=True)
    page_score = models.ForeignKey(PageScore, related_name='reports')

    def get_file_extension(self):
        name, extension = os.path.splitext(self.file.name)
        return extension


# KPI Models.
class OfferTimePeriod(models.Model):
    month = models.IntegerField()
    year = models.IntegerField()
    offers = models.ManyToManyField(Offer, related_name='kpi_time_periods')

    class Meta:
        ordering = ('month', 'year')

    def __str__(self):
        return f'{self.year} - {self.month}'
