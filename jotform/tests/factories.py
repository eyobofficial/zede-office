import random

import factory
from pendulum import UTC

from jotform.models import Form, Webhook, Question, Submission, Answer


class BaseFormFactory(factory.django.DjangoModelFactory):
    id = factory.Faker('isbn13')
    username = factory.Faker('user_name')
    title = factory.Faker('sentence', nb_words=4)
    height = 200
    url = factory.Faker('url')
    status = 'ENABLED'
    new = 0
    count = 20
    created_at = factory.Faker('past_datetime', start_date='-30d', tzinfo=UTC)
    updated_at = factory.Faker('date_time_between', start_date='-20d',
                               tzinfo=UTC)
    last_submission = factory.Faker('date_time_between', start_date='-20d',
                                    tzinfo=UTC)

    class Meta:
        model = Form


class EmptyFormFactory(BaseFormFactory):
    pass


class FormFactory(BaseFormFactory):
    @factory.post_generation
    def generate_questions(self, create, extracted, **kwargs):
        if not create:
            return

        QuestionFactory.create_batch(4, form=self)
        SubmissionFactory.create_batch(4, form=self)


class QuestionFactory(factory.django.DjangoModelFactory):
    qid = factory.Sequence(lambda n: n)
    text = factory.Faker('sentence', nb_words=6, variable_nb_words=True)
    type = factory.Faker('random_element', elements=[
        'control_textbox',
        'control_textarea',
        'control_scale',
        'control_dropdown',
        'control_datetime',
        'control_email',
        'control_checkbox'
    ])
    order = factory.Sequence(lambda n: n)

    class Meta:
        model = Question

    @factory.lazy_attribute
    def options(self):
        options = ''
        if self.type == 'control_dropdown':
            options_list = ['Options {}'.format(n) for n in range(4)]
            options = '|'.join(options_list)

        return options


class SubmissionFactory(factory.django.DjangoModelFactory):
    id = factory.Faker('isbn13')
    status = 'ACTIVE'

    class Meta:
        model = Submission

    @factory.post_generation
    def generate_questions(self, create, extracted, **kwargs):
        if not create:
            return

        for question in self.form.questions.all():
            attributes = {
                'qid': question.qid,
                'text': question.text,
                'type': question.type,
            }

            if question.type == 'control_dropdown':
                index = random.randint(1, 4)
                attributes['answer'] = 'Options {}'.format(index)

            AnswerFactory(submission=self, **attributes)


class AnswerFactory(factory.django.DjangoModelFactory):
    answer = factory.Faker('sentence', nb_words=4)

    class Meta:
        model = Answer


class WebhookFactory(factory.django.DjangoModelFactory):
    url = factory.Faker('url')
    form = factory.SubFactory(FormFactory)

    class Meta:
        model = Webhook
