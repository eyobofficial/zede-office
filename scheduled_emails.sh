#!/bin/bash

server=$1

if [ "$server" == "test" ]
then
    # Change to project directory
    cd /

    # Create and send Scan E-mail Reminders
    /usr/local/bin/python manage.py generate_scans_reminders

    # Create and send Registration E-mail Reminders
    /usr/local/bin/python manage.py generate_registration_reminders

else
    # Change to project directory
    cd ~/webapps/debatnl_backoffice/DebatNL_BackOffice/

    # Activate virtualenv
    source /home/debatnl/.virtualenvs/debatnl_backoffice/bin/activate

    # Create and send Scan E-mail Reminders
    /home/debatnl/.virtualenvs/debatnl_backoffice/bin/python manage.py generate_scans_reminders

    # Create and send Registration E-mail Reminders
    /home/debatnl/.virtualenvs/debatnl_backoffice/bin/python manage.py generate_registration_reminders
fi

echo "Generated E-mails"

