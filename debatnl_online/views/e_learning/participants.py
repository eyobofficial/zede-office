from django.http import HttpResponseRedirect
from django.urls import reverse_lazy
from django.views.generic import FormView, TemplateView

from backoffice.constants import GROUP_DEBATNLONLINE
from backoffice.mixins import GroupAccessMixin
from debatnl_online.consumers.e_learning.courses import CourseConsumer
from debatnl_online.consumers.e_learning.participants import \
    ParticipantConsumer
from debatnl_online.consumers.users import UserConsumer
from debatnl_online.forms.e_learning import ParticipantForm


class ParticipantBaseView(GroupAccessMixin, FormView):
    form_class = ParticipantForm
    template_name = 'debatnl_online/components/e_learning/participants/{}'
    template_name = template_name.format('modal-participant.html')
    consumer = ParticipantConsumer()
    success_url = reverse_lazy('debatnl_online:e_learning:overview')
    access_groups = [GROUP_DEBATNLONLINE]

    def get_context_data(self, **kwargs):
        kwargs.update({
            'users': UserConsumer().retrieve_all(subscriptions='E-LEARNING'),
            'courses': CourseConsumer().retrieve_all()
        })
        return super().get_context_data(**kwargs)


class ParticipantCreate(ParticipantBaseView):
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context.update({
            'modal_title': 'PARTICIPANT TOEVOEGEN',
            'button_text': 'Toevoegen'
        })
        return context

    def form_valid(self, form):
        data = {'courses': self.request.POST.getlist('courses')}
        data.update(form.cleaned_data.copy())
        self.consumer.create_object(data)
        return super().form_valid(form)


class ParticipantUpdate(ParticipantBaseView):
    def get_context_data(self, **kwargs):
        self.initial = self.consumer.retrieve_object(self.kwargs['pk'])

        kwargs.update({
            'is_update_mode': True,
            'modal_title': 'PARTICIPANT WIJZIGEN',
            'button_text': 'Wijzigen'
        })

        return super().get_context_data(**kwargs)

    def form_valid(self, form):
        data = {'courses': self.request.POST.getlist('courses')}
        self.consumer.update_object(form.cleaned_data['id'], data)
        return super().form_valid(form)


class ParticipantDelete(GroupAccessMixin, TemplateView):
    template_name = 'debatnl_online/modals/modal-delete.html'
    success_url = reverse_lazy('debatnl_online:e_learning:overview')
    access_groups = [GROUP_DEBATNLONLINE]

    def get_context_data(self, **kwargs):
        kwargs.update({
            'modal_title': 'PARTICIPANT',
            'modal_description': 'de participant',
        })

        return super().get_context_data(**kwargs)

    def post(self, request, pk):
        ParticipantConsumer().delete_object(pk)
        return HttpResponseRedirect(self.success_url)
