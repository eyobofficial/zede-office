from backoffice.utilities.emails import BaseEmail


class BaseUserEmail(BaseEmail):
    template_location = '../templates/users/emails'
    email_type = ''
