from django.test import TestCase
from django.urls import reverse
from django.utils import timezone

from backoffice.constants import GROUP_SALARIES
from backoffice.tests.factories import UserFactory

from declarations.models import Declaration
from declarations.tests.factories import DeclarationFactory
from users.models import AppPermission


class SalaryOverview(TestCase):
    """
    Tests for the `SalaryOverview` view
    """
    fixtures = ['app_permissions']

    def setUp(self):
        self.user = UserFactory()
        app_permission = AppPermission.objects.get(app_name=GROUP_SALARIES)
        self.user.profile.app_permissions.add(app_permission)
        self.url = reverse('salaries:overview')
        self.template_name = 'salaries/declaration_list.html'

    def test_request_with_anonymous_user(self):
        """
        Ensure view is not accessible by a non-authenticated user.
        """
        response = self.client.get(self.url, follow=True)
        expected_url = f'{reverse("login")}?next=/feedback/'

        # Assertions
        self.assertRedirects(response, expected_url)

    def test_request_with_authenticated_user(self):
        """
        Ensure view is accessible by an authenticated user
        """
        self.client.force_login(self.user)

        user_1 = UserFactory(first_name='A')
        user_2 = UserFactory(first_name='B')
        user_3 = UserFactory(first_name='Z')

        current_year = timezone.now().year

        declaration_1 = DeclarationFactory(
            user=user_1,
            year=current_year,
            month=Declaration.JANUARY,
            status=Declaration.SUBMITTED
        )
        declaration_2 = DeclarationFactory(
            user=user_2,
            year=current_year,
            month=Declaration.JANUARY,
            status=Declaration.SUBMITTED
        )
        declaration_3 = DeclarationFactory(
            user=user_3,
            year=current_year,
            month=Declaration.FEBRUARY,
            status=Declaration.SUBMITTED
        )

        response = self.client.get(self.url)
        expected_qs = [declaration_3, declaration_1, declaration_2]

        # Assertions
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, self.template_name)
        self.assertQuerysetEqual(response.context['object_list'], expected_qs, lambda x: x)
