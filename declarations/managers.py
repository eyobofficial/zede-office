from django.db import models

from webcrm.models import Delivery, Event


class ExternalMeetingManager(models.Manager):
    def create_from_webcrm(self, declaration):
        kwargs = {
            'order_date__month': declaration.month,
            'order_date__year': declaration.year,
            'trainers__email': declaration.user.email
        }
        deliveries = Delivery.objects.filter(**kwargs)
        deliveries = deliveries.exclude(product='Digitale intake')
        deliveries = deliveries.exclude(product='Debat.NL Online')
        deliveries = deliveries.exclude(product='SkillsTracker')
        deliveries = deliveries.exclude(state=Delivery.STATE_DELETED)
        deliveries = self._remove_duplicates(deliveries)

        external_meetings = self.get_queryset().filter(
            declaration=declaration,
            webcrm_delivery_id__isnull=False
        )

        external_meeting_ids = []
        for delivery in deliveries.all():
            kwargs = {
                'declaration': declaration,
                'webcrm_delivery_id': delivery.pk
            }
            external_meeting, _ = self.get_or_create(**kwargs)
            external_meeting.date = delivery.order_date
            external_meeting.organization = delivery.organization.name
            if not external_meeting.type:
                external_meeting.type = self._get_type(delivery.product)

            external_meeting.save()

            external_meeting_ids.append(external_meeting.pk)
            external_meetings = external_meetings.exclude(
                pk=external_meeting.pk)

        external_meetings.all().delete()
        return self.get_queryset().filter(pk__in=external_meeting_ids)

    def _get_type(self, product):
        products = {
            self.model.COACHING: ['Coaching'],
            self.model.SCAN: ['Debatscan', 'Voorzittersscan', 'Observeren'],
            self.model.TRAINING: [
                'Bewust Beinvloeden',
                'Bewust Beinvloeden (open training)',
                'Feedbacktraining', 'Fractietraining', 'Gespreksleiding',
                'Hoe vang ik een rat?', 'Integriteit', 'Lezing',
                'Maak meer Impact', 'Mediatraining', 'Onderhandelen',
                'Ontspannen Presenteren',
                'Ontspannen Presenteren(open training)',
                'Oog in oog met de burger', 'Overtuigend debatteren',
                'Overtuigend debatteren (open training)',
                'Persoonlijke overtuigingskracht',
                'Politieke debatvaardigheden', 'Professioneel faciliteren',
                'Professioneel Gespreksleiden (open training)', 'Simulatie',
                'Spreken als Obama', 'Team training',  'Voorzitterstraining'
            ],
        }

        for key, value in products.items():
            if product in value:
                return key

        return ''

    @staticmethod
    def _remove_duplicates(items):
        item_ids = []
        kwargs = {'organization__name': 'Open inschrijving DNL'}
        for delivery in items.filter(**kwargs):
            item_id = f'{delivery.order_date}-{delivery.custom5}'

            if item_id in item_ids:
                items = items.exclude(pk=delivery.pk)
                continue

            item_ids.append(item_id)

        return items


class InternalMeetingManager(models.Manager):
    def create_from_webcrm(self, declaration):
        kwargs = {
            'date_time__month': declaration.month,
            'date_time__year': declaration.year,
            'participants__email': declaration.user.email,
            'custom2': 'Afspraak op locatie'
        }
        events = Event.objects.filter(**kwargs)
        events = events.exclude(type='Inschrijving event')
        events = events.exclude(state=Event.STATE_DELETED)

        internal_meetings = self.get_queryset().filter(
            declaration=declaration,
            webcrm_delivery_id__isnull=False
        )

        internal_meeting_ids = []
        for event in events.all():
            kwargs = {
                'declaration': declaration,
                'webcrm_delivery_id': event.pk
            }
            internal_meeting, _ = self.get_or_create(**kwargs)
            internal_meeting.date = event.date_time
            internal_meeting.meeting_with = event.organization.name
            internal_meeting.type = self.model.SALES
            internal_meeting.save()

            internal_meeting_ids.append(internal_meeting.pk)
            internal_meetings = internal_meetings.exclude(
                pk=internal_meeting.pk)

        internal_meetings.all().delete()
        return self.get_queryset().filter(pk__in=internal_meeting_ids)


class ExpenseManager(models.Manager):
    ALLOWANCES = ['Dagvergoeding', 'Dinervergoeding', 'Lunchvergoeding']

    def allowances(self):
        """
        Return allowance expenses that are non-empty (having a meeting)
        """
        qs = self.exclude(
            internal_meetings__isnull=True,
            external_meetings__isnull=True
        )
        return qs.filter(description__in=self.ALLOWANCES)

    def non_allowances(self):
        """
        Return expenses that are not allowances
        """
        return self.exclude(description__in=self.ALLOWANCES)
