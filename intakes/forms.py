from django.forms import ModelForm, inlineformset_factory, DateField, \
    SelectDateWidget, CharField, BaseInlineFormSet

from intakes.models import Intake, IntakeReportQuestion, IntakeReminder


class IntakeForm(ModelForm):
    trainers_report_date = DateField(widget=SelectDateWidget(), required=False)
    report_date = DateField(widget=SelectDateWidget(), required=False)
    form = CharField(strip=True)

    class Meta:
        model = Intake
        exclude = ('id', 'form', 'status', 'created_at', 'updated_at',
                   'report_status', 'trainers_report_status')

    def save(self, commit=True):
        self.instance.form_id = self.cleaned_data.get('form')
        return super().save(commit=commit)


class IntakeReportQuestionForm(ModelForm):
    class Meta:
        model = IntakeReportQuestion
        exclude = ('id',)


class IntakeReminderForm(ModelForm):
    date = DateField(widget=SelectDateWidget())

    class Meta:
        model = IntakeReminder
        exclude = ('id', 'status')


class IntakeBaseInlineFormSet(BaseInlineFormSet):
    items = []

    def save(self, commit=True):
        # Delete the items, which are not in the POST data
        # when updating the object.
        for item in self.items:
            if not any(f.instance == item for f in self.forms):
                item.delete()

        return super().save(commit)


class IntakeReportQuestionBaseFormset(IntakeBaseInlineFormSet):
    def save(self, commit=True):
        self.items = self.instance.report_questions.all()
        return super().save(commit)


class IntakeReminderBaseFormset(IntakeBaseInlineFormSet):
    def save(self, commit=True):
        self.items = self.instance.reminders.filter(status='PLANNED').all()
        return super().save(commit)


IntakeReportQuestionFormSet = inlineformset_factory(
    Intake,
    IntakeReportQuestion,
    formset=IntakeReportQuestionBaseFormset,
    exclude=('id',),
    extra=1
)

IntakeReminderFormSet = inlineformset_factory(
    Intake,
    IntakeReminder,
    form=IntakeReminderForm,
    formset=IntakeReminderBaseFormset,
    exclude=('id', 'status'),
    extra=1
)
