from django.contrib import admin
from marketing.models import Page, PageScore, PageScoreReport, OfferTimePeriod


@admin.register(Page)
class PageAdmin(admin.ModelAdmin):
    list_display = ('title', 'url', 'updated_at', 'created_at')
    search_fields = ('title', 'url')


class PageScoreReportInline(admin.TabularInline):
    model = PageScoreReport
    extra = 0


@admin.register(PageScore)
class PageScoreAdmin(admin.ModelAdmin):
    list_display = ('page', 'performance', 'accessibility', 'best_practice',
                    'seo', 'created_at')
    list_filter = ('page', )
    search_fields = ('page__title', 'page__url')
    inlines = (PageScoreReportInline, )


@admin.register(OfferTimePeriod)
class OfferTimePeriodAdmin(admin.ModelAdmin):
    list_display = ('month', 'year')
