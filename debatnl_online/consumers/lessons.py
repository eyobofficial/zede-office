import json

import requests
from django.conf import settings


class LessonConsumer:

    def __init__(self):
        self.url = '{}/api/e-learning/lessons/admin'.format(settings.DEBATNL_ONLINE_API)
        self.headers = {
            'Authorization': 'Token {}'.format(settings.DEBATNL_ONLINE_API_KEY)
        }

    def retrieve_all(self):
        url = self.url
        response = requests.get(url, headers=self.headers)
        return response.json()

    def retrieve_object(self, object_id):
        url = '{}/{}/'.format(self.url, object_id)
        response = requests.get(url, headers=self.headers)
        return response.json()

    def update_object(self, object_id, data):
        url = '{}/{}/'.format(self.url, object_id)

        payload = {
            'id': object_id,
            'name': data.get('name'),
            'sequence': data.get('sequence'),
            'is_first_lesson': data.get('is_first_lesson'),
            'vimeo_video_id': data.get('vimeo_video_id'),
            'video_url': data.get('video_url'),
            'is_deleted': data.get('is_deleted', False)
        }

        response = requests.put(url, headers=self.headers, data=payload)
        return response.json()

    def create_object(self, data):
        url = '{}/'.format(self.url)

        payload = {
            'chapter': data.get('chapter_id'),
            'name': data.get('name'),
            'sequence': data.get('sequence'),
            'is_first_lesson': data.get('is_first_lesson'),
            'vimeo_video_id': data.get('vimeo_video_id'),
            'video_url': data.get('video_url'),
        }

        response = requests.post(url, headers=self.headers, data=payload)
        return response.json()


class LessonTestQuestionConsumer:

    def __init__(self):
        self.url = '{}/api/e-learning/lessons'.format(settings.DEBATNL_ONLINE_API)
        self.headers = {
            'Authorization': 'Token {}'.format(settings.DEBATNL_ONLINE_API_KEY)
        }

    def retrieve_all(self, lesson_id):
        url = '{}/{}/admin/questions/'.format(self.url, lesson_id)
        response = requests.get(url, headers=self.headers)
        return response.json()

    def retrieve_object(self, lesson_id, question_id):
        url = '{}/{}/admin/questions/{}/'.format(self.url, lesson_id, question_id)
        response = requests.get(url, headers=self.headers)
        return response.json()

    def update_object(self, lesson_id, question_id, data):
        url = '{}/{}/admin/questions/{}/'.format(self.url, lesson_id, question_id)

        payload = {
            'id': question_id,
            'question': data.get('question'),
            'sequence': data.get('sequence'),
            'is_deleted': data.get('is_deleted', False),
        }

        response = requests.put(url, headers=self.headers, data=payload)
        return response.json()

    def create_object(self, lesson_id, data):
        url = '{}/{}/questions/'.format(self.url, lesson_id)

        payload = {
            'lesson': lesson_id,
            'question': data.get('question'),
            'sequence': data.get('sequence'),
        }

        response = requests.post(url, headers=self.headers, data=payload)
        return response.json()


class LessonTestAnswerConsumer:
    def __init__(self):
        self.url = '{}/api/e-learning/questions'.format(settings.DEBATNL_ONLINE_API)
        self.headers = {
            'Authorization': 'Token {}'.format(settings.DEBATNL_ONLINE_API_KEY)
        }

    def update_object(self, question_id, data):
        url = '{}/{}/answers/'.format(self.url, question_id)
        response = requests.put(url, headers=self.headers, json=data)
        return response.json()

    def create_object(self, question_id, data):
        url = '{}/{}/answers/'.format(self.url, question_id)
        response = requests.post(url, headers=self.headers, json=data)
        return response.json()
