from backoffice.tests.bases import BaseEmailTestCase
from follow_ups.emails.notifications import FollowUpEventEmail, \
    NoFollowUpEventSetupEmail
from follow_ups.models import FollowUpEvent
from follow_ups.tests.factories import FollowUpFactory, FollowUpEventFactory


class FollowUpEmailTest(BaseEmailTestCase):
    template_dir = '../templates/follow_ups/emails'
    obj_context_name = 'follow_ups'

    def setUp(self):
        super().setUp()
        self.follow_up = FollowUpFactory()
        self.items = [
            {
                'class': NoFollowUpEventSetupEmail,
                'args': (self.follow_up, ),
                'template_name': 'email_5_no_follow_up_event_setup.html',
                'subject': 'Olaf wil graag je klanten iets sturen!',
                'recipients': [self.follow_up.opportunity.assigned_to.email],
            }
        ]


class FollowUpEventEmailTest(BaseEmailTestCase):
    template_dir = '../templates/follow_ups/emails'
    obj_context_name = 'follow_ups'

    def setUp(self):
        super().setUp()
        self.event = FollowUpEventFactory()

        self.items = [
            {
                'class': FollowUpEventEmail,
                'args': (self.event,),
                'template_name': 'email_4_follow_up_event.html',
                'subject': self.event.subject,
                'recipients': [self.event.follow_up.opportunity.person.email],
                'context': self.get_follow_up_event_assertions,
            },
        ]

    def get_follow_up_event_assertions(self, context):
        self.assertEqual(self.event.status, FollowUpEvent.SENT)
