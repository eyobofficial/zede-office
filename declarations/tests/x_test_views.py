import json
import uuid

from django.test import TestCase
from django.urls import reverse
from mock import patch

from backoffice.constants import GROUP_DECLARATIONS
from backoffice.tests.factories import UserFactory
from declarations.models import HubstaffItem
from users.models import AppPermission
from .factories import DeclarationFactory


class OverviewViewTests(TestCase):
    """
    Tests for the `DeclarationOverview` view
    """
    fixtures = ['app_permissions']

    def setUp(self):
        self.user = UserFactory()
        app_permission = AppPermission.objects.get(app_name=GROUP_DECLARATIONS)
        self.user.profile.app_permissions.add(app_permission)
        self.url = reverse('declarations:overview')

    def test_request_with_anonymous_user(self):
        """
        Ensure anonymous user cannot access the view with GET request.
        """
        response = self.client.get(self.url, follow=True)

        # Assertions
        expected_url = f'{reverse("login")}?next=/feedback/'
        self.assertRedirects(response, expected_url)

    def test_request_with_authenticated_user(self):
        """
        Ensure authenticated user can access the view with GET request.
        """
        self.client.force_login(self.user)
        response = self.client.get(self.url)

        # Assertions
        self.assertEqual(response.status_code, 200)

    # @patch('declarations.views.timezone.now')
    # def test_valid_request_with_2019_declarations(self, mock_obj):
    #     """
    #     Ensure authenticated user can only access their declaration for
    #     December month for 2019.
    #     """
    #     mock_obj.return_value = datetime.date(2019, 1, 30)
    #     self.client.force_login(self.user)
    #
    #     # Loggedin user declarations for 2019
    #     Declaration.objects.create(
    #         user=self.user,
    #         year=2019,
    #         month=Declaration.JANUARY
    #     )
    #     Declaration.objects.create(
    #         user=self.user,
    #         year=2019,
    #         month=Declaration.MAY
    #     )
    #     Declaration.objects.create(
    #         user=self.user, year=2019,
    #         month=Declaration.DECEMBER
    #     )
    #
    #     # Non-loggedin user declarations for 2019
    #     other_user = UserFactory()
    #     Declaration.objects.create(
    #         user=other_user,
    #         year=2019,
    #         month=Declaration.DECEMBER
    #     )
    #     response = self.client.get(self.url)
    #
    #     expected_declaration = Declaration.objects.filter(
    #         user=self.user,
    #         year=2019,
    #         month=Declaration.DECEMBER
    #     ).first()
    #
    #     # Assertions
    #     self.assertEqual(response.status_code, 200)
    #     self.assertEqual(response.context['object_list'][0],
    #                      expected_declaration)
    #
    # @patch('declarations.views.timezone.now')
    # def test_valid_request_with_non_2019_declarations(self, mock_obj):
    #     """
    #     Ensure authenticated user can access their declarations for every month
    #     of the that year.
    #     """
    #     mock_obj.return_value = datetime.date(2020, 1, 30)
    #     self.client.force_login(self.user)
    #
    #     # Loggedin user declarations for 2020 year
    #     Declaration.objects.create(
    #         user=self.user,
    #         year=2020,
    #         month=Declaration.JANUARY
    #     )
    #     Declaration.objects.create(
    #         user=self.user,
    #         year=2020,
    #         month=Declaration.MAY
    #     )
    #     Declaration.objects.create(
    #         user=self.user,
    #         year=2020,
    #         month=Declaration.DECEMBER
    #     )
    #
    #     # Loggedin user declaration for non 2020 year
    #     Declaration.objects.create(
    #         user=self.user,
    #         year=2022,
    #         month=Declaration.JANUARY
    #     )
    #     Declaration.objects.create(
    #         user=self.user,
    #         year=2019,
    #         month=Declaration.MAY
    #     )
    #     Declaration.objects.create(
    #         user=self.user,
    #         year=2030,
    #         month=Declaration.DECEMBER
    #     )
    #
    #     # Non-loggedin user declaration for 2020 year
    #     other_user = UserFactory()
    #     Declaration.objects.create(
    #         user=other_user,
    #         year=2020,
    #         month=Declaration.JANUARY
    #     )
    #     Declaration.objects.create(
    #         user=other_user,
    #         year=2020,
    #         month=Declaration.MAY
    #     )
    #     Declaration.objects.create(
    #         user=other_user,
    #         year=2020,
    #         month=Declaration.DECEMBER
    #     )
    #
    #     response = self.client.get(self.url)
    #     queryset = Declaration.objects.filter(user=self.user, year=2020)
    #
    #     # Assertions
    #     self.assertEqual(response.status_code, 200)
    #     self.assertEqual(response.context['object_list'], queryset)


# class DeclarationCreateViewTests(TestCase):
#     """
#     Tests for `DeclarationCreateView`
#     """
#     fixtures = ['app_permissions']
#
#     def setUp(self):
#         self.user = UserFactory()
#         group = Group.objects.get(name=GROUP_DECLARATIONS)
#         self.user.groups.add(group)
#         self.template = 'declarations/declaration_detail.html'
#         self.url = reverse('declarations:create')
#
#     def test_request_with_anonymous_user(self):
#         """
#         Ensure anonymous user cannot access the view with GET request
#         """
#         response = self.client.get(self.url, follow=True)
#
#         # Assertions
#         expected_url = f'{reverse("login")}?next=/feedback/'
#         self.assertRedirects(response, expected_url)
#
#     def test_request_with_authenticated_user(self):
#         """
#         Ensure authenticated users can access the view with GET request
#         """
#         self.client.force_login(self.user)
#         response = self.client.get(self.url)
#
#         # Assertions
#         self.assertEqual(response.status_code, 200)
#         self.assertTemplateUsed(response, self.template)
#
#     def test_POST_request_data_with_save_button(self):
#         """
#         When a user saves a declaration data, ensure a new declaration is
#         created with status 'OPEN'
#         """
#         self.client.force_login(self.user)
#         data = {
#             'user': self.user,
#             'year': '2019',
#             'month': '1',
#             'billable_hourly_rate': '100.00',
#             'internal_hourly_rate': '200.00',
#             'work_satisfaction': '5',
#             'company_satisfaction': '10',
#             'feedback': 'Test feedback',
#             'save': ''
#         }
#         response = self.client.post(self.url, data=data)
#         declaration = Declaration.objects.first()
#         expected_url = f"{reverse('declarations:overview')}?year=2019"
#
#         # Assertions
#         self.assertEqual(Declaration.objects.count(), 1)
#         self.assertEqual(declaration.status, Declaration.OPEN)
#         self.assertRedirects(response, expected_url)
#
#     def test_POST_request_data_with_submit_button(self):
#         """
#         When a user submits a declaration data, ensure a new declaration is
#         created with status 'IN_PROGRESS'.
#         """
#         self.client.force_login(self.user)
#         data = {
#             'user': self.user,
#             'year': '2019',
#             'month': '1',
#             'billable_hourly_rate': '100.00',
#             'internal_hourly_rate': '200.00',
#             'work_satisfaction': '5',
#             'company_satisfaction': '10',
#             'feedback': 'Test feedback',
#             'submit': ''
#         }
#         response = self.client.post(self.url, data=data)
#         declaration = Declaration.objects.first()
#         expected_url = f"{reverse('declarations:overview')}?year=2019"
#
#         # Assertions
#         self.assertEqual(Declaration.objects.count(), 1)
#         self.assertEqual(declaration.status, Declaration.IN_PROGRESS)
#         self.assertRedirects(response, expected_url)


# class DeclarationUpdateViewTests(TestCase):
#     """
#     Tests for `FeedbackUpdateView` tests
#     """
#     fixtures = ['app_permissions']
#
#     def setUp(self):
#         self.user = UserFactory()
#         group = Group.objects.get(name=GROUP_DECLARATIONS)
#         self.user.groups.add(group)
#         self.declaration = Declaration.objects.create(
#             user=self.user,
#             year=2019,
#             month=1
#         )
#         self.template = 'declarations/declaration_detail.html'
#         url = reverse(
#             'declarations:update',
#             args=[self.declaration.pk]
#         )
#         self.url = f'{url}?year=2019&month=Januari'
#
#     def test_GET_request_with_anonymous_user(self):
#         """
#         Ensure anonymous user cannot access the view with GET request.
#         """
#         response = self.client.get(self.url, follow=True)
#
#         # Assertions
#         expected_url = f'{reverse("login")}?next=/feedback/'
#         self.assertRedirects(response, expected_url)
#
#     def test_GET_request_with_authenticated_user(self):
#         """
#         Ensure authenticated user can access the view GET request.
#         """
#         self.client.force_login(self.user)
#         response = self.client.get(self.url)
#
#         # Assertions
#         self.assertEqual(response.status_code, 200)
#         self.assertTemplateUsed(response, self.template)
#
#     def test_valid_request_with_in_progress_declaration(self):
#         """
#         Ensure authenticated user cannot access a declaration with IN_PROGRESS
#         status using GET request.
#         """
#         self.client.force_login(self.user)
#         self.declaration.status = Declaration.IN_PROGRESS
#         self.declaration.save()
#         response = self.client.get(self.url)
#
#         # Assertions
#         self.assertEqual(response.status_code, 404)
#
#     def test_valid_request_with_submitted_declaration(self):
#         """
#         Ensure authenticated user cannont access a declaration with SUBMITTED
#         status using GET request.
#         """
#         self.client.force_login(self.user)
#         self.declaration.status = Declaration.SUBMITTED
#         self.declaration.save()
#         response = self.client.get(self.url)
#
#         # Assertions
#         self.assertEqual(response.status_code, 404)
#
#     def test_POST_request_with_save_data(self):
#         """
#         When a user saves a declaration data, ensure the declaration is updated
#         with feedback data and 'OPEN' status.
#         """
#         self.client.force_login(self.user)
#         data = {
#             'month': '1',
#             'year': '2019',
#             'billable_hourly_rate': '100.00',
#             'internal_hourly_rate': '200.00',
#             'work_satisfaction': '5',
#             'company_satisfaction': '10',
#             'feedback': 'Test feedback',
#             'save': ''
#         }
#         response = self.client.post(self.url, data=data)
#         self.declaration.refresh_from_db()
#
#         expected_url = f"{reverse('declarations:overview')}?year=2019"
#
#         # Assertions
#         self.assertRedirects(response, expected_url)
#         self.assertEqual(self.declaration.status, Declaration.OPEN)
#         self.assertEqual(self.declaration.billable_hourly_rate, 100.00)
#         self.assertEqual(self.declaration.internal_hourly_rate, 200.00)
#         self.assertEqual(self.declaration.work_satisfaction, 5)
#         self.assertEqual(self.declaration.company_satisfaction, 10)
#         self.assertEqual(self.declaration.feedback, 'Test feedback')
#
#     def test_POST_request_with_submit_data(self):
#         """
#         When a user saves a declaration data, ensure the declaration is updated
#         with the feedback data & 'IN_PROGRESS' status.
#         """
#         self.client.force_login(self.user)
#         data = {
#             'month': '1',
#             'year': '2019',
#             'billable_hourly_rate': '100.00',
#             'internal_hourly_rate': '200.00',
#             'work_satisfaction': '5',
#             'company_satisfaction': '10',
#             'feedback': 'Test feedback',
#             'submit': ''
#         }
#         response = self.client.post(self.url, data=data)
#         self.declaration.refresh_from_db()
#
#         expected_url = f"{reverse('declarations:overview')}?year=2019"
#
#         # Assertions
#         self.assertRedirects(response, expected_url)
#         self.assertEqual(self.declaration.status, Declaration.IN_PROGRESS)
#         self.assertEqual(self.declaration.billable_hourly_rate, 100.00)
#         self.assertEqual(self.declaration.internal_hourly_rate, 200.00)
#         self.assertEqual(self.declaration.work_satisfaction, 5)
#         self.assertEqual(self.declaration.company_satisfaction, 10)
#         self.assertEqual(self.declaration.feedback, 'Test feedback')

#
# class ELearningCreateViewTests(TestCase):
#     """
#     Tests for `ELearningCreateView` view
#     """
#     fixtures = ['app_permissions']
#
#     def setUp(self):
#         self.user = UserFactory()
#         group = Group.objects.get(name=GROUP_DECLARATIONS)
#         self.user.groups.add(group)
#         self.template = 'declarations/components/forms/form-elearning-create.html'
#         self.url = reverse('declarations:elearning-create')
#
#     def test_request_with_anonymous_user(self):
#         """
#         Ensure anonymous users cannot access the view with GET request
#         """
#         response = self.client.get(self.url, follow=True)
#
#         # Assertions
#         expected_url = f'{reverse("login")}?next=/feedback/'
#         self.assertRedirects(response, expected_url)
#
#     def test_request_with_authenticated_user(self):
#         """
#         Ensure anonymous users can access the view GET request
#         """
#         self.client.force_login(self.user)
#         response = self.client.get(self.url)
#
#         # Assertions
#         self.assertEqual(response.status_code, 200)
#         self.assertTemplateUsed(response, self.template)
#
#     def test_invalid_POST_request_without_a_certificate_file(self):
#         """
#         Ensure POST request without a certificate file does not create an new
#         ELearning item
#         """
#         self.client.force_login(self.user)
#         data = {'name': ELearningItem.BEWUST_BEINVLOEDEN}
#         response = self.client.post(self.url, data)
#
#         # Assertions
#         self.assertEqual(response.status_code, 400)
#
#     def test_valid_POST_request_without_related_declaration(self):
#         """
#         Ensure that a valid POST request without a related declaration instance
#         create an ELearningItem instance and store the id in a session.
#         """
#         self.client.force_login(self.user)
#         data = {
#             'name': ELearningItem.BEWUST_BEINVLOEDEN,
#             'certificate': SimpleUploadedFile('file.pdf', b'content')
#         }
#         response = self.client.post(self.url, data=data, follow=True)
#
#         expected_url = reverse('declarations:elearning-table')
#         session = self.client.session
#
#         # Assertions
#         self.assertRedirects(response, expected_url)
#         self.assertEqual(ELearningItem.objects.count(), 1)
#         self.assertTrue(ELearningItem.objects.first(), session['elearning_items'])
#
#     def test_valid_POST_request_with_related_declaration(self):
#         """
#         Ensure that a valid POST request with a related declation instance
#         creates an ELearningItem instance.
#         """
#         self.client.force_login(self.user)
#         data = {
#             'name': ELearningItem.BEWUST_BEINVLOEDEN,
#             'certificate': SimpleUploadedFile('file.pdf', b'content')
#         }
#         declaration = Declaration.objects.create(
#             user=self.user,
#             month=Declaration.JANUARY,
#             year=2019,
#             status=Declaration.OPEN
#         )
#         url = f'{self.url}?declaration={declaration.pk}'
#         response = self.client.post(url, data=data, follow=True)
#
#         expected_url = reverse('declarations:elearning-table')
#         expected_url = f'{expected_url}?declaration={declaration.pk}'
#
#         # Assertions
#         self.assertRedirects(response, expected_url)
#         self.assertEqual(ELearningItem.objects.count(), 1)
#
#
# class ELearningDeleteViewTests(TestCase):
#     """
#     Tests for `ELearningDeleteView` view
#     """
#     fixtures = ['app_permissions']
#
#     def setUp(self):
#         self.user = UserFactory()
#         group = Group.objects.get(name=GROUP_DECLARATIONS)
#         self.user.groups.add(group)
#         self.elearning_item = ELearningItem.objects.create(
#             name=ELearningItem.BEWUST_BEINVLOEDEN,
#             certificate=SimpleUploadedFile('file.pdf', b'content'),
#             hours=1
#         )
#         self.url = reverse(
#             'declarations:elearning-delete',
#             args=[self.elearning_item.pk]
#         )
#         self.template = 'declarations/components/forms/form-elearning-delete.html'
#
#     def test_request_with_anonymous_user(self):
#         """
#         Ensure anonymous users cannot access the view with GET request
#         """
#         response = self.client.get(self.url, follow=True)
#
#         # Assertions
#         expected_url = f'{reverse("login")}?next=/feedback/'
#         self.assertRedirects(response, expected_url)
#
#     def test_request_with_authenticated_user(self):
#         """
#         Ensure authenticated users can access the view with GET request
#         """
#         self.client.force_login(self.user)
#         response = self.client.get(self.url)
#
#         # Assertions
#         self.assertEqual(response.status_code, 200)
#         self.assertTemplateUsed(response, self.template)
#
#     def test_POST_request_with_authenticated_user(self):
#         """
#         Ensure authenticated users can delete an ELearningItem instance using
#         POST request.
#         """
#         self.client.force_login(self.user)
#         response = self.client.post(self.url)
#         expected_url = reverse('declarations:elearning-table')
#
#         # Assertions
#         self.assertRedirects(response, expected_url)
#         self.assertEqual(ELearningItem.objects.count(), 0)
#
#
# class ELearningTableViewTests(TestCase):
#     """
#     Tests for `ELearningTableView` view
#     """
#     fixtures = ['app_permissions']
#
#     def setUp(self):
#         self.user = UserFactory()
#         group = Group.objects.get(name=GROUP_DECLARATIONS)
#         self.user.groups.add(group)
#         self.declaration = Declaration.objects.create(
#             user=self.user,
#             year=2019,
#             month=Declaration.MAY
#         )
#         self.elearning_item_1 = ELearningItem.objects.create(
#             name=ELearningItem.BEWUST_BEINVLOEDEN,
#             certificate=SimpleUploadedFile('file_1.pdf', b'content'),
#             declaration=self.declaration,
#             hours=1
#         )
#         self.elearning_item_2 = ELearningItem.objects.create(
#             name=ELearningItem.BEWUST_BEINVLOEDEN_INTERN,
#             certificate=SimpleUploadedFile('file_2.pdf', b'content'),
#             declaration=self.declaration,
#             hours=2
#         )
#         self.elearning_item_3 = ELearningItem.objects.create(
#             name=ELearningItem.DEBATSCAN,
#             certificate=SimpleUploadedFile('file_3.pdf', b'content'),
#             hours=3
#         )
#         url = reverse('declarations:elearning-table')
#         self.url = f'{url}?declaration={self.declaration.pk}'
#         self.template = 'declarations/components/tables/elearning_item_table.html'
#
#     def test_request_with_anonymous_user(self):
#         """
#         Ensure that anonymous cannot access the view with GET request
#         """
#         response = self.client.get(self.url, follow=True)
#
#         # Assertions
#         expected_url = f'{reverse("login")}?next=/feedback/'
#         self.assertRedirects(response, expected_url)
#
#     def test_request_authenticated_user(self):
#         """
#         Ensure that authenticated can access the view GET request
#         """
#         self.client.force_login(self.user)
#         response = self.client.get(self.url)
#
#         # Assertions
#         self.assertEqual(response.status_code, 200)
#         self.assertTemplateUsed(response, self.template)
#         self.assertEqual(
#             response.context['elearning_items'],
#             [self.elearning_item_1, self.elearning_item_2]
#         )


class DirectionJsonViewTest(TestCase):
    """
    Tests for `DirectionJsonView` view
    """
    fixtures = ['app_permissions']

    def setUp(self):
        self.user = UserFactory()
        app_permission = AppPermission.objects.get(app_name=GROUP_DECLARATIONS)
        self.user.profile.app_permissions.add(app_permission)
        self.url = reverse('declarations:get-direction')
        routes = [
            {
                'location': 'loc 1',
                'mode': 'driving',
                'declarationType': 'HOUR',
                'tripType': 'ONE_WAY'
            },
            {
                'location': 'loc 2',
                'mode': 'driving',
                'declarationType': 'HOUR_KM',
                'tripType': 'TWO_WAY'
            },
            {
                'location': 'loc 3',
                'mode': 'walking',
                'declarationType': 'KM',
                'tripType': 'ONE_WAY'
            }
        ]
        self.post_data = json.dumps(routes)


    def test_GET_request_with_authenticated_user(self):
        """
        Ensure GET requested is not allowed
        """
        self.client.force_login(self.user)
        response = self.client.get(self.url)

        # Assertions
        self.assertEqual(response.status_code, 405)

    @patch('declarations.googlemap.directions.requests.get')
    def test_POST_request_with_anonymous_user(self, mock_get):
        """
        Ensure anonymous request cannot make POST requests.
        """
        mock_get.return_value.json.return_value = {'status': 'OK'}
        response = self.client.post(
            self.url,
            data=self.post_data,
            content_type='application/json',
            follow=True
        )
        expected_url = f'{reverse("login")}?next=/feedback/'

        # Assertions
        self.assertRedirects(response, expected_url)

    @patch('declarations.googlemap.directions.requests.get')
    def test_POST_request_with_valid_location(self, mock_get):
        """
        Ensure a POST request with an authenticated user & a valid location
        returns direction data & a status code of 200.
        """
        self.client.force_login(self.user)
        mock_get.return_value.json.return_value = {
            'status': 'OK',
            'routes': [
                {
                    'legs': [
                        {
                            'distance': {'text': '1 km', 'value': 1000},
                            'duration': {'text': '50 mins', 'value': 3000}
                        }
                    ]
                }
            ]
        }
        response = self.client.post(
            self.url,
            data=self.post_data,
            content_type='application/json'
        )
        expected_data = {'distance': 2.0, 'duration': 2.5}

        # Assertions
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), expected_data)

    @patch('declarations.googlemap.directions.requests.get')
    def test_POST_request_with_non_existing_location(self, mock_get):
        """
        Ensure a POST request with an authenticated user & a non-existing
        location returns data & a status code of 400.
        """
        self.client.force_login(self.user)
        mock_get.return_value.json.return_value = {'status': 'NOT_FOUND'}
        response = self.client.post(
            self.url,
            data=self.post_data,
            content_type='application/json'
        )

        # Assertions
        self.assertEqual(response.status_code, 400)

    @patch('declarations.googlemap.directions.requests.get')
    def test_POST_request_when_daily_API_call_limit_is_reached(self, mock_get):
        """
        When the daily allowed Google API call limit is reached, ensure a
        status code of 429 is returned by the view.
        """
        self.client.force_login(self.user)
        mock_get.return_value.json.return_value = {'status': 'OVER_DAILY_LIMIT'}
        response = self.client.post(
            self.url,
            data=self.post_data,
            content_type='application/json'
        )

        # Assertions
        self.assertEqual(response.status_code, 429)


class LinkHubstaffAccountViewTest(TestCase):
    """
    Tests for `LinkHubstaffAccountView` view
    """
    fixutures = ['groups']

    def setUp(self):
        self.declaration = DeclarationFactory()
        self.url = reverse('declarations:link-hubstaff-account')
        self.success_template = 'declarations/public/link_hubstaff_success.html'
        self.invalid_token_template = 'declarations/public/invalid_token.html'

    def test_request_with_invalid_token(self):
        """
        Ensure when an invalid token key is sent with the URL, no hubstaff
        account is created.
        """
        invalid_token = uuid.uuid4()
        url = f'{self.url}?token={invalid_token}'
        response = self.client.get(url)

        # Assertions
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, self.invalid_token_template)
        self.assertQuerysetEqual(HubstaffItem.objects.all(), [])

    # def test_request_with_valid_token(self):
    #     """
    #     Ensure when a valid token key is sent with the URL, a related hubstaff
    #     account is created & the token is deleted.
    #     """
    #     valid_token = self.token.key
    #     url = f'{self.url}?token={valid_token}'
    #     response = self.client.get(url)
    #
    #     # Assertions
    #     self.assertEqual(response.status_code, 200)
    #     self.assertTemplateUsed(response, self.success_template)
    #     self.assertIsNotNone(self.declaration.hubstaff_item)
