from django import forms


class UserForm(forms.Form):
    SUBSCRIPTIONS_CHOICES = (
        ('E-LEARNING', 'E-LEARNING'),
        ('LEARNING GOALS', 'LEARNING GOALS')
    )

    id = forms.CharField(required=False)
    first_name = forms.CharField()
    last_name = forms.CharField()
    email = forms.EmailField()
    subscriptions = forms.MultipleChoiceField(choices=SUBSCRIPTIONS_CHOICES,
                                              required=False)
