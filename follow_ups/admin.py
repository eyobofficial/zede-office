from django.contrib import admin

from backoffice.admin import CustomURLModelAdmin
from follow_ups.models import FollowUp, FollowUpEvent
from follow_ups.views import FollowUpEventEmailView, \
    NoFollowUpEventSetupEmailView, UpdateWebCRMOpportunityOrderDateView


class FollowUpEventInline(admin.StackedInline):
    model = FollowUpEvent
    extra = 0
    can_delete = False
    ordering = ('-date', )


@admin.register(FollowUp)
class FollowUpAdmin(CustomURLModelAdmin):
    list_display = ('opportunity', 'status')
    list_filter = ('status', )
    search_fields = ('opportunity__organization__name', )
    inlines = (FollowUpEventInline, )
    custom_urls = [
        {
            'regex': r'^(?P<pk>.+)/send_email_no_event_setup/$',
            'view': NoFollowUpEventSetupEmailView,
            'name': 'send_email_no_event_setup'
        },
        {
            'regex': r'^(?P<pk>.+)/update_webcrm_opportunity_order_date/$',
            'view': UpdateWebCRMOpportunityOrderDateView,
            'name': 'update_webcrm_opportunity_order_date'
        }
    ]


@admin.register(FollowUpEvent)
class FollowUpEventAdmin(CustomURLModelAdmin):
    list_display = ('date', 'type', 'subject', 'follow_up', 'status')
    list_filter = ('status', )
    custom_urls = [
        {
            'regex': r'^(?P<pk>.+)/send_email_follow_up_event/$',
            'view': FollowUpEventEmailView,
            'name': 'send_email_follow_up_event'
        }
    ]
