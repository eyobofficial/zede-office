import pendulum
from django.conf import settings
from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth.models import User
from django.db import transaction
from django.http import HttpResponse
from django.shortcuts import render
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views import View
from django.views.generic import TemplateView, CreateView, UpdateView, \
    DeleteView
from django.views.generic.edit import FormMixin

from backoffice.constants import GROUP_INTAKES
from backoffice.mixins import GroupAccessMixin
from backoffice.views import BaseEmailView
from intakes.emails.notifications import \
    IntakeTrainerDateChangeNotificationEmail, \
    IntakeTrainerDateNotificationEmail
from intakes.emails.reminders import IntakeReminderEmail, \
    IntakeOrganizationInsightReportEmail, IntakeTrainerInsightReportEmail, \
    IntakeCheckReminderEmail
from intakes.forms import IntakeForm, IntakeReportQuestionFormSet, \
    IntakeReminderFormSet
from intakes.models import Intake, IntakeReportFile
from jotform.client.client import JOTFormClient
from shared.enums import RoleName


class OverviewView(GroupAccessMixin, TemplateView):
    template_name = 'intakes/base.html'
    access_groups = [GROUP_INTAKES]

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        today = pendulum.today()
        updated_at = today.subtract(weeks=4)

        ongoing_intakes = Intake.objects.exclude(status=Intake.FINISHED)
        finished_intakes = Intake.objects.filter(status=Intake.FINISHED,
                                                 updated_at__gte=updated_at)

        context.update({
            'ongoing_intakes': ongoing_intakes.order_by('created_at'),
            'finished_intakes': finished_intakes.order_by(
                'trainers_report_date'),
        })

        return context


class JotFormView(TemplateView):
    template_name = 'intakes/components/modals/modal-intake.html'


class IntakeBaseView(GroupAccessMixin, FormMixin):
    model = Intake
    form_class = IntakeForm
    template_name = 'intakes/components/modals/sections/intake-form.html'
    success_url = reverse_lazy('intakes:overview')
    access_groups = [GROUP_INTAKES]
    object = None

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        current_year = pendulum.now().year
        api_key = settings.JOT_FORM_API_KEY
        form_id = self.get_form_id()

        client = JOTFormClient(api_key=api_key, form_id=form_id)
        context['questions'] = client.questions.list()

        formset_questions = self.init_formset(IntakeReportQuestionFormSet)
        formset_reminders = self.init_formset(IntakeReminderFormSet)

        context.update({
            'form_id': form_id,
            'formset_questions': formset_questions,
            'formset_reminders': formset_reminders,
            'days': range(1, 32),
            'years': range(current_year, current_year + 5),
            'trainers': User.objects.filter(
                profile__roles__name=RoleName.TRAINER.value,
                is_active=True
            )
        })
        return context

    def init_formset(self, formset_class):
        if self.request.POST:
            formset = formset_class(self.request.POST, instance=self.object)
        else:
            formset = formset_class(instance=self.object)

        if formset.initial_form_count() > 0:
            formset.extra = 0

        return formset

    def get_form_id(self):
        if self.object and self.object.form:
            return self.object.form.id

        return self.request.GET.get('form-id')

    @transaction.atomic
    def form_valid(self, form):
        formset_questions = self.init_formset(IntakeReportQuestionFormSet)
        formset_reminders = self.init_formset(IntakeReminderFormSet)

        self.object = form.save()
        formset_questions.instance = self.object
        formset_reminders.instance = self.object

        if formset_questions.is_valid() and formset_reminders.is_valid():
            formset_questions.save()
            formset_reminders.save()

            self.send_email(form)

            return super().form_valid(form)

        return super().form_invalid(form)

    def send_email(self, *args, **kwargs):
        raise NotImplemented


class IntakeCreate(IntakeBaseView, CreateView):
    object = None

    def send_email(self, *args, **kwargs):
        if self.object.trainers_report_date:
            IntakeTrainerDateNotificationEmail(self.object).send()


class IntakeUpdate(IntakeBaseView, UpdateView):
    def send_email(self, form):
        if 'trainers_report_date' in form.changed_data:
            IntakeTrainerDateChangeNotificationEmail(self.object).send()


class IntakeDelete(GroupAccessMixin, DeleteView):
    model = Intake
    template_name = 'intakes/components/modals/modal-delete.html'
    success_url = reverse_lazy('intakes:overview')
    access_groups = [GROUP_INTAKES]


@method_decorator(staff_member_required, name='dispatch')
class BaseIntakeReportEmailView(BaseEmailView):
    url = 'admin:intakes_intake_change'
    model = Intake


class SendParticipantReportEmail(BaseIntakeReportEmailView):
    email_class = IntakeReminderEmail
    email_name = 'participant report'


class SendOrganizationInsightReportEmail(BaseIntakeReportEmailView):
    email_class = IntakeOrganizationInsightReportEmail
    email_name = 'organization insight report'


class SendTrainerInsightReportEmail(BaseIntakeReportEmailView):
    email_class = IntakeTrainerInsightReportEmail
    email_name = 'trainer insight report'


class SendIntakeCheckEmail(BaseIntakeReportEmailView):
    email_class = IntakeCheckReminderEmail
    email_name = 'intake check'


class SendTrainerDateNotificationEmail(BaseIntakeReportEmailView):
    email_class = IntakeTrainerDateNotificationEmail
    email_name = 'trainer date notification'


class SendTrainerDateChangeNotificationEmail(BaseIntakeReportEmailView):
    email_class = IntakeTrainerDateChangeNotificationEmail
    email_name = 'trainer date change notification'


class IntakeReportDownload(View):
    template_name = 'intakes/public/expired_report.html'

    def get(self, request, report_id):
        try:
            report = IntakeReportFile.objects.get(report_id=report_id)

            today = pendulum.today()
            intake_updated_at = pendulum.instance(report.intake.updated_at)
            is_expired = today.diff(intake_updated_at).in_weeks() >= 4

            if not (report.intake.status == Intake.FINISHED and is_expired):
                disposition = 'attachment; filename="{}"'.format(
                    report.filename())
                response = HttpResponse(content=report.file.read())
                response['Content-Type'] = 'application/pdf'
                response['Content-Disposition'] = disposition
                return response

        except IntakeReportFile.DoesNotExist:
            pass

        return render(request, self.template_name, status=404)
