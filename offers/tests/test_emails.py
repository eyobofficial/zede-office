from django.conf import settings
from django.urls import reverse

from backoffice.tests.bases import BaseEmailTestCase
from offers.emails.notifications import CreatedOfferNotification, \
    ChangedOfferNotification, AcceptedOfferNotification, \
    RemovedOfferNotification, DeniedOfferNotification, ReviewOfferNotification
from offers.emails.reminders import ReviewOfferReminder, CreatedOfferReminder
from offers.models import OfferToken
from offers.tests.factories import OfferFactory, OfferInvestmentFactory, \
    OfferTokenFactory


class BaseOfferEmailTest(BaseEmailTestCase):
    template_dir = '../templates/offers/emails'
    obj_context_name = 'offer'

    def setUp(self):
        super().setUp()
        self.obj = OfferFactory()
        OfferInvestmentFactory(offer=self.obj)

    def get_created_offer_assertions(self, context):
        email_obj = context.get('email_obj')
        token = OfferToken.objects.get(email=email_obj.recipient.email,
                                       type=OfferToken.UPLOAD)

        self.assertEqual(context.get('url'), '{}{}?token={}&email={}'.format(
            settings.APP_HOSTNAME,
            reverse('offers:upload'),
            str(token.key),
            self.obj.editor.email
        ))

    def get_denied_offer_assertions(self, context):
        self.get_created_offer_assertions(context)
        self.assertEqual(context.get('review'), self.obj.reviews.last())

    def get_review_offer_assertions(self, context):
        email_obj = context.get('email_obj')
        token = OfferToken.objects.get(email=email_obj.recipient.email,
                                       type=OfferToken.REVIEW)

        self.assertEqual(context.get('url'), '{}{}?token={}&email={}'.format(
            settings.APP_HOSTNAME,
            reverse('offers:review'),
            str(token.key),
            self.obj.delivery_responsible_email
        ))


class OfferEmailTest(BaseOfferEmailTest):
    def setUp(self):
        super().setUp()

        self.items = [
            {
                'class': CreatedOfferNotification,
                'args': (self.obj,),
                'template_name': 'email_1_new_offer_notification.html',
                'subject': 'Aanvraag offerte {}'.format(self.obj.organization),
                'attachment_count': 1,
                'context': self.get_created_offer_assertions,
                'recipients': [self.obj.editor.email]
            },
            {
                'class': ChangedOfferNotification,
                'args': (self.obj,),
                'template_name': 'email_6_changed_offer.html',
                'subject': 'Offerte {} is gewijzigd'.format(
                    self.obj.organization),
                'attachment_count': 1,
                'context': self.get_created_offer_assertions,
                'recipients': [self.obj.editor.email]
            },
            {
                'class': RemovedOfferNotification,
                'args': (self.obj,),
                'template_name': 'email_7_removed_offer.html',
                'subject': 'Offerte {} is geannuleerd'.format(
                    self.obj.organization),
                'recipients': [self.obj.editor.email]
            },
            {
                'class': AcceptedOfferNotification,
                'args': (self.obj,),
                'template_name': 'email_4_accepted_offer_notification.html',
                'subject': 'Offerte goedgekeurd',
                'recipients': [self.obj.editor.email]
            },
            {
                'class': DeniedOfferNotification,
                'args': (self.obj,),
                'template_name': 'email_3_denied_offer_notification.html',
                'subject': 'Offerte {} is niet goedgekeurd'.format(
                    self.obj.organization),
                'context': self.get_denied_offer_assertions,
                'recipients': [self.obj.editor.email]
            },
            {
                'class': ReviewOfferNotification,
                'args': (self.obj,),
                'template_name': 'email_2_review_offer_notification.html',
                'recipients':  [self.obj.delivery_responsible_email],
                'subject': 'Offerte {} is binnen'.format(
                    self.obj.organization),
                'context': self.get_review_offer_assertions,
            },
        ]


class OfferReminderTest(BaseOfferEmailTest):
    def setUp(self):
        super().setUp()
        OfferTokenFactory(email=self.obj.editor.email, type=OfferToken.UPLOAD,
                          offer=self.obj)
        OfferTokenFactory(email=self.obj.delivery_responsible_email,
                          type=OfferToken.REVIEW, offer=self.obj)

        self.items = [
            {
                'class': CreatedOfferReminder,
                'args': (self.obj.tokens.get(type=OfferToken.UPLOAD),),
                'template_name': 'email_5_new_offer_reminder.html',
                'subject': 'Reminder offerte {}'.format(self.obj.organization),
                'attachment_count': 1,
                'context': self.get_created_offer_assertions,
                'recipients': [self.obj.editor.email]
            },
            {
                'class': ReviewOfferReminder,
                'args': (self.obj.tokens.get(type=OfferToken.REVIEW),),
                'template_name': 'email_2a_review_offer_reminder.html',
                'recipients': [self.obj.delivery_responsible_email],
                'subject': 'Bekijk offerte {}'.format(self.obj.organization),
                'context': self.get_review_offer_assertions
            },
        ]
