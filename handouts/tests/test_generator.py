import datetime

import mock
from django.test import TestCase
from django.utils import timezone

from jotform.tests.factories import SubmissionFactory, AnswerFactory
from jotform.models import Form

from webcrm.factories import DeliveryFactory

from handouts.generator import HandoutRequestGenerator
from handouts.models import HandoutRequest


class HandoutRequestGeneratorTests(TestCase):
    """
    Tests for `HandoutRequestGenerator` class
    """
    fixtures = ['app_permissions']

    def setUp(self):
        now = timezone.now()
        self.form = Form.objects.create(id='123-456')
        self.submission = SubmissionFactory(form=self.form, created_at=now)
        answers = [
            {'qid': 7, 'answer': 'Test Trainee'},
            {'qid': 10, 'answer': "{'year':'2019','month':'05','day':'19' }"},
            {'qid': 11, 'answer': 'trainee@test.email'},
            {'qid': 13, 'answer': "['Test Trainer']"},
            {'qid': 14, 'answer': "['Ja, ik wil een handout ontvangen']"},
        ]

        for answer in answers:
            AnswerFactory(**answer, submission=self.submission)

        self.generator = HandoutRequestGenerator()

    def test__get_submission_trainee_name_method(self):
        """
        Ensure `_get_submission_trainee_name` method returns trainee_name.
        """
        name = self.generator._get_submission_trainee_name(self.submission)
        self.assertEqual(name, 'Test Trainee')

    def test__get_submission_trainee_email_method(self):
        """
        Ensure `_get_submission_trainee_email` method returns trainee email.
        """
        email = self.generator._get_submission_trainee_email(self.submission)
        self.assertEqual(email, 'trainee@test.email')

    def test__get_submission_trainer_method(self):
        """
        Ensure `_get_submission_trainer` method returns trainer name.
        """
        trainer = self.generator._get_submission_trainer(self.submission)
        self.assertEqual(trainer, 'Test Trainer')

    def test__get_submission_training_date(self):
        """
        Ensure `_get_submission_training_date` method returns the
        right training date.
        """
        dt = self.generator._get_submission_training_date(self.submission)
        self.assertEqual(dt, datetime.date(2019, 5, 19))

    def test__get_corresponding_deliveries(self):
        """
        Ensure `_get_corresponding_deliveries` method returns the
        right deliveries.
        """
        dt = datetime.date(2019, 5, 19)
        expected_delivery = DeliveryFactory(order_date=dt, product='XYZ')

        DeliveryFactory(order_date=dt, product='Digitale Intake')
        DeliveryFactory(order_date=dt, product='Debat.NL Online')
        DeliveryFactory(order_date=dt, product='SkillsTracker')
        DeliveryFactory(order_date=datetime.date(2019, 5, 20), product='ABC')

        deliveries = self.generator._get_corresponding_deliveries(dt)

        self.assertEqual(deliveries.count(), 1)
        self.assertEqual(deliveries.first(), expected_delivery)

    @mock.patch('handouts.models.HandoutRequest.'
                'send_notification_based_on_status')
    def test_generate_with_existing_submission_id(self, mock_obj):
        """
        Ensure a new handout request is created, if no duplicate
        submission_id is already saved.
        """

        # Run 1
        self.generator._generate_submission_handout_request(self.submission)

        # Run 2
        self.generator._generate_submission_handout_request(self.submission)

        # Run 3
        self.generator._generate_submission_handout_request(self.submission)

        self.assertEqual(HandoutRequest.objects.count(), 1)
        self.assertEqual(mock_obj.call_count, 1)

    @mock.patch('handouts.models.HandoutRequest.'
                'send_notification_based_on_status')
    def test_generate_with_no_handout_values(self, mock_obj):
        """
        Ensure `generate` method does not create `HandoutRequest` instances,
        if there are no submissions (jotform app) that have an answer value
        of `Ja, ik wil een handout ontvangen`.
        """

        now = timezone.now()
        submission = SubmissionFactory(id=7891, form=self.form, created_at=now)
        answers = [
            {'qid': 7, 'answer': 'Test Trainee 2'},
            {'qid': 10, 'answer': "{'year':'2019','month':'05','day':'20' }"},
            {'qid': 11, 'answer': 'testtrainee2@test.email'},
            {'qid': 13, 'answer': "['Test Trainer 2']"},
            {'qid': 14, 'answer': "['Ja, ik wil elke maand vijf overtuigtips "
                                  "ontvangen']"},
        ]

        for answer in answers:
            AnswerFactory(**answer, submission=submission)

        self.generator.generate()
        self.assertEqual(HandoutRequest.objects.count(), 1)
        self.assertEqual(mock_obj.call_count, 1)

    @mock.patch('handouts.models.HandoutRequest.'
                'send_notification_based_on_status')
    def test_generate_with_handout_values(self, mock_obj):
        """
        Ensure `generate` method creates `HandoutRequest` instances,
        if there are submissions (jotform app) that have an answer
        value of `Ja, ik wil een handout ontvangen`.
        """
        now = timezone.now()
        submission = SubmissionFactory(id=7891, form=self.form, created_at=now)
        answers = [
            {'qid': 7, 'answer': 'Test Trainee 2'},
            {'qid': 10, 'answer': "{'year':'2019','month':'05','day':'20' }"},
            {'qid': 11, 'answer': 'testtrainee2@test.email'},
            {'qid': 13, 'answer': "['Test Trainer 2']"},
            {'qid': 14, 'answer': "['Ja, ik wil elke maand vijf overtuigtips "
                                  "ontvangen', 'Ja, ik wil een handout "
                                  "ontvangen']"},
        ]

        for answer in answers:
            AnswerFactory(**answer, submission=submission)

        self.generator.generate()
        self.assertEqual(HandoutRequest.objects.count(), 2)
        self.assertEqual(mock_obj.call_count, 2)
