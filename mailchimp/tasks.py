from requests import HTTPError

from DebatNL_BackOffice.celery import app

from mailchimp.models import MailChimpMember, FeedbackSubmission


@app.task
def import_audience_members_from_jotform():
    kwargs = {'mailchimp_member__isnull': True}
    submissions = FeedbackSubmission.objects.filter(**kwargs).all()
    for index, submission in enumerate(submissions):
        try:
            submission.add_to_mailing_list()
            status = MailChimpMember.SUCCESS
        except HTTPError:
            status = MailChimpMember.FAILED

        MailChimpMember.objects.create(submission=submission, status=status)
