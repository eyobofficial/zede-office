# -*- coding: utf-8 -*-
# Generated by Django 1.11.20 on 2019-06-19 13:40
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('webcrm', '0004_delivery_person'),
    ]

    operations = [
        migrations.CreateModel(
            name='Opportunity',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('order_date', models.DateTimeField()),
                ('product', models.CharField(blank=True, default='', max_length=120)),
                ('description', models.TextField(blank=True, default='')),
                ('level', models.IntegerField(blank=True, null=True)),
                ('level_text', models.CharField(blank=True, default='', max_length=120)),
                ('number', models.CharField(blank=True, default='', max_length=25)),
                ('created_at', models.DateTimeField()),
                ('updated_at', models.DateTimeField()),
                ('state', models.CharField(choices=[('AVAILABLE', 'Available'), ('DELETED', 'Deleted')], default='AVAILABLE', max_length=50)),
                ('assigned_to', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='opportunities_as_1', to='webcrm.User')),
                ('assigned_to2', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='opportunities_as_2', to='webcrm.User')),
                ('organization', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='webcrm.Organization')),
                ('person', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='opportunities', to='webcrm.Person')),
            ],
            options={
                'verbose_name_plural': 'opportunities',
                'ordering': ('order_date', 'organization'),
            },
        ),
    ]
