from backoffice.utilities.emails import ReminderEmailMixin
from briefings.emails.bases import BriefingEmail, DeliverableEmail


class BriefingNotAddedReminder(ReminderEmailMixin, DeliverableEmail):
    def __init__(self, deliverable):
        super().__init__(deliverable)
        self.template_name = 'email_2_briefing_not_added_reminder.html'
        self.subject = 'Maak Olaf en andere collega’s blij!'


class TwoWeeksForDeliveryReminder(ReminderEmailMixin, DeliverableEmail):
    def __init__(self, deliverable):
        super().__init__(deliverable)
        self.template_name = 'email_3_two_weeks_left_reminder.html'
        self.subject = 'Olaf en andere collega’s zijn zenuwachtig!'


class NewAddedBriefingReminder(ReminderEmailMixin, BriefingEmail):
    def __init__(self, briefing):
        super().__init__(briefing)
        self.template_name = 'email_5_last_week_added_briefing_reminder.html'
        self.subject = 'Er zijn nieuwe briefings voor je'

    def get_context_data(self):
        context = super().get_context_data()
        deliverables = self.briefing.deliveries.all().order_by('-created_at')
        context['deliverables'] = deliverables
        return context
