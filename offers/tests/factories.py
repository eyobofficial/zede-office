import factory
import pendulum
from django.conf import settings

from backoffice.tests.factories import EditorFactory
from offers.models import OfferPhoto, Offer, OfferInvestment, OfferToken, \
    OfferAbsenceSchedule
from shared.enums import RoleName
from users.models import Role
from users.tests.factories import UserFactory


class OfferPhotoFactory(factory.django.DjangoModelFactory):
    name = factory.Sequence(lambda n: 'Photo {}'.format(n))
    image = factory.django.ImageField(filename='{}.jpg'.format(name))

    class Meta:
        model = OfferPhoto


class OfferFactory(factory.django.DjangoModelFactory):
    organization = factory.Faker('company')
    contact_person = factory.Faker('name')
    photo = factory.SubFactory(OfferPhotoFactory)
    delivery_responsible_name = factory.Faker('name')
    delivery_responsible_email = factory.LazyAttribute(
        lambda t: '{}@example.com'.format(
            t.delivery_responsible_name
        ).replace(' ', '').lower()
    )
    question_background = factory.Faker('text')
    program_structure = factory.Faker('text')
    is_trainer_proposed = True
    old_editor = factory.SubFactory(EditorFactory)
    editor = factory.SubFactory(UserFactory)
    file_location_name = 'test_location'
    file_location_year = '2019'

    class Meta:
        model = Offer

    class Params:
        finished = factory.Trait(
            status='FINISHED',
            finished_at=factory.Sequence(
                lambda n: pendulum.now().subtract(days=n)
            )
        )

    @factory.post_generation
    def trainers(self, create, extracted, **kwargs):
        if create and not extracted:
            role, _ = Role.objects.get_or_create(name=RoleName.TRAINER.value)
            created_trainers = UserFactory.create_batch(2)
            for t in created_trainers:
                t.profile.roles.add(role)

            self.trainers.add(*created_trainers)


class OfferInvestmentFactory(factory.django.DjangoModelFactory):
    offer = factory.SubFactory(OfferFactory)
    subject = factory.Sequence(lambda n: 'Subject {}'.format(n))
    range = factory.Sequence(lambda n: '{}0000 - {}0000'.format(n, n+1))

    class Meta:
        model = OfferInvestment


class OfferTokenFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = OfferToken


class OfferAbsenceScheduleFactory(factory.django.DjangoModelFactory):
    start_date = pendulum.today(tz=settings.TIME_ZONE).date()
    end_date = pendulum.today(tz=settings.TIME_ZONE).add(days=5).date()
    old_employee = factory.SubFactory(EditorFactory)
    employee = factory.SubFactory(UserFactory)

    class Meta:
        model = OfferAbsenceSchedule
