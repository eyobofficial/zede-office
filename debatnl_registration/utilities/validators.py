import logging

from debatnl_registration.models import TrainingDate

logger = logging.getLogger('django')


def validate_registration(registration):
    submission_id = registration.submission_id

    if registration.duration != TrainingDate.objects.filter(
            attendance__participant__registration=registration
    ).distinct().count():
        message = f'Registration Submission ID: {submission_id} - Reason: ' \
            f'The defined duration is not equal to the amount of training ' \
            f'date objects.'
        logger.error(message)
        return False

    if registration.participant_count != registration.participants.count():
        message = f'Registration Submission ID: {submission_id} - Reason: ' \
            f'The defined participant count is not equal to ' \
            f'the amount of participant objects.'
        logger.error(message)
        return False

    return True
