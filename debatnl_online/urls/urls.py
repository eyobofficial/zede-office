from django.conf.urls import url, include
from django.urls import reverse_lazy
from django.views.generic import RedirectView

from debatnl_online.views.shared import SignS3DebatNLOnlineView

urlpatterns = [
    url(
        r'^users/',
        include('debatnl_online.urls.users', namespace='users')
    ),
    url(
        r'^e-learning/',
        include('debatnl_online.urls.e_learning.urls', namespace='e_learning')
    ),
    url(
        r'^learning-goals/',
        include(
            'debatnl_online.urls.learning_goals.urls',
            namespace='learning_goals'
        )
    ),
    url(
        r'^sign-s3-upload-url/',
        SignS3DebatNLOnlineView.as_view(),
        name='sign_s3_upload_url'
    ),
    url(
        r'^$',
        RedirectView.as_view(
            url=reverse_lazy('debatnl_online:users:overview')
        ),
        name='overview'
    ),
]
