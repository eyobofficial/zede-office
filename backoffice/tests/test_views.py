from django.test import TestCase, Client
from django.urls import reverse

from backoffice.constants import GROUP_BACKOFFICE
from backoffice.models import Training
from users.models import AppPermission
from .factories import UserFactory, TrainingFactory


class OverviewViewTest(TestCase):
    """
    Tests for the `OverviewView` view
    """
    fixtures = ['app_permissions']

    def setUp(self):
        self.client = Client()
        self.url = reverse('configuration:overview')

    def test_request_with_anonymous_user(self):
        """
        Test a request to `OverviewView` view from anonymous user
        """
        response = self.client.get(self.url, follow=True)

        # Assertions
        expected_url = f'{reverse("login")}?next=/feedback/'
        self.assertRedirects(response, expected_url)

    def test_request_with_authenticated_user(self):
        """
        Test a request to `OverviewView` view from an authenticated
        user
        """
        # Valid users.
        user_1 = UserFactory(is_active=True, is_superuser=False)
        UserFactory(is_active=True, is_superuser=True)

        # Invalid users.
        UserFactory(is_active=False, is_superuser=True)
        UserFactory(is_active=False, is_superuser=False)

        app_permission = AppPermission.objects.get(app_name=GROUP_BACKOFFICE)
        user_1.profile.app_permissions.add(app_permission)

        self.client.force_login(user_1)
        response = self.client.get(self.url)

        expected_users = response.context['object_list']

        # Assertions
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(expected_users), 2)
        self.assertIn(user_1, expected_users)
        self.assertIsNotNone(response.context['trainings'])


class TrainingCreateViewTest(TestCase):
    """
    Tests for `TrainingCreateView` view
    """
    fixtures = ['app_permissions']

    def setUp(self):
        self.user = UserFactory()
        app_permission = AppPermission.objects.get(app_name=GROUP_BACKOFFICE)
        self.user.profile.app_permissions.add(app_permission)
        self.client = Client()
        self.url = reverse('configuration:training-create')
        self.template = 'backoffice/components/modals/modal-training-form.html'

    def test_request_with_anonymous_user(self):
        """
        Test request to `TrainingCreateView` view from anonymous user
        """
        response = self.client.get(self.url, follow=True)

        # Assertions
        expected_url = f'{reverse("login")}?next=/feedback/'
        self.assertRedirects(response, expected_url)

    def test_request_with_authenticated_user(self):
        """
        Test request to `TrainingCreateView` view from authenticated user
        """
        self.client.force_login(self.user)
        response = self.client.get(self.url)

        # Assertions
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, self.template)

    def test_post_request_with_training_form_data(self):
        """
        Test a valid post request to `TrainingCreateView` with form
        data
        """
        self.client.force_login(self.user)
        payload = {
            'name': 'Test Training'
        }
        response = self.client.post(self.url, payload)
        training = Training.objects.last()

        # Assertions
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, '/configuration/')
        self.assertEqual(training.name, payload['name'])


class TrainingUpdateViewTest(TestCase):
    """
    Tests for `TrainingUpdateView` view
    """
    fixtures = ['app_permissions']

    def setUp(self):
        self.user = UserFactory()
        app_permission = AppPermission.objects.get(app_name=GROUP_BACKOFFICE)
        self.user.profile.app_permissions.add(app_permission)
        self.client = Client()
        self.training = TrainingFactory()
        self.url = reverse(
            'configuration:training-update',
            args=(self.training.pk, )
        )
        self.template = 'backoffice/components/modals/modal-training-form.html'

    def test_request_with_anonymous_user(self):
        """
        Test request to `TrainingUpdateView` view from anonymous user
        """
        response = self.client.get(self.url, follow=True)

        # Assertions
        expected_url = f'{reverse("login")}?next=/feedback/'
        self.assertRedirects(response, expected_url)

    def test_request_with_authenticated_user(self):
        """
        Test request to `TrainingUpdateView` view from authenticated user
        """
        self.client.force_login(self.user)
        response = self.client.get(self.url)

        # Assertions
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, self.template)

    def test_post_request_with_form_data(self):
        self.client.force_login(self.user)
        payload = {
            'name': 'Updated Training'
        }
        response = self.client.post(self.url, payload)
        self.training.refresh_from_db()

        # Assertions
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, '/configuration/')
        self.assertEqual(self.training.name, payload['name'])


class TrainingDeleteViewTest(TestCase):
    """
    Tests for `TrainingDeleteView` view
    """
    fixtures = ['app_permissions']

    def setUp(self):
        self.user = UserFactory()
        app_permission = AppPermission.objects.get(app_name=GROUP_BACKOFFICE)
        self.user.profile.app_permissions.add(app_permission)
        self.client = Client()
        training = TrainingFactory()
        self.url = reverse(
            'configuration:training-delete',
            args=(training.pk, )
        )
        self.template = 'backoffice/components/modals/modal-delete.html'

    def test_request_with_anonymous_user(self):
        """
        Test request to `TrainingDeleteView` view from anonymous user
        """
        response = self.client.get(self.url, follow=True)

        # Assertions
        expected_url = f'{reverse("login")}?next=/feedback/'
        self.assertRedirects(response, expected_url)

    def test_request_with_authenticated_user(self):
        """
        Test request to `TrainingDeleteView` view from authenticated
        user
        """
        self.client.force_login(self.user)
        response = self.client.get(self.url)

        # Assertions
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, self.template)

    def test_post_request_to_delete_training(self):
        """
        Test a valid POST request to `TrainingDeleteView`
        """
        self.client.force_login(self.user)
        response = self.client.post(self.url)
        training_list = Training.objects.all()

        # Assertions
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, '/configuration/')
        self.assertEqual(len(training_list), 0)


class UserModifyOptionViewTests(TestCase):
    """
    Tests for `UserModifyOptionView`
    """
    fixtures = ['app_permissions']

    def setUp(self):
        self.user = UserFactory()
        app_permission = AppPermission.objects.get(app_name=GROUP_BACKOFFICE)
        self.user.profile.app_permissions.add(app_permission)
        self.client = Client()
        self.url = reverse('configuration:user-modify')
        self.template = 'backoffice/components/modals/modal-user-form.html'

    def test_request_with_anonymous_user(self):
        """
        Test request to `UserModifyOptionView` from anonymous user
        """
        response = self.client.get(self.url, follow=True)

        # Assertions
        expected_url = f'{reverse("login")}?next=/feedback/'
        self.assertRedirects(response, expected_url)

    def test_request_with_authenticated_user(self):
        """
        Test request to `UserModifyOptionView` from authenticated
        user
        """
        self.client.force_login(self.user)
        response = self.client.get(self.url)

        # Assertions
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, self.template)
