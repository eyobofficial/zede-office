from django.test import TestCase
from django.urls import reverse

from planning.forms import DeliveryModelForm, ScheduleForm, ProcessTypeForm
from planning.models import Schedule
from shared.enums import RoleName
from users.models import Role
from .factories import DeliveryFactory, ScheduleFactory


class DeliveryModelFormTest(TestCase):
    """
    Tests for `DeliveryModelForm` form
    """

    def setUp(self):
        self.form = DeliveryModelForm()

    def test_form_field(self):
        """
        Test the existance of `date` field in DeliveryModelForm
        """
        self.assertTrue('date' in self.form.fields)

    def test_form_has_organization_field(self):
        """
        Test the existance of `organization` field in DeliveryModelForm
        """
        self.assertTrue('organization' in self.form.fields)

    def test_form_has_time_field(self):
        """
        Test the existance of `time` field in DeliveryModelForm
        """
        self.assertTrue('time' in self.form.fields)

    def test_form_has_location_field(self):
        """
        Test the existance of `location` field in DeliveryModelForm
        """
        self.assertTrue('location' in self.form.fields)

    def test_form_has_description_field(self):
        """
        Test the existance of `description` field in DeliveryModelForm
        """
        self.assertTrue('description' in self.form.fields)

    def test_form_has_trainer_amount_field(self):
        """
        Test the existance of `trainer_amount` field in
        DeliveryModelForm
        """
        self.assertTrue('trainer_amount' in self.form.fields)


class ScheduleFormTest(TestCase):
    """
    Tests for `ScheduleForm` form
    """
    fixtures = ['roles']

    def setUp(self):
        self.form = ScheduleForm()

    def test_form_has_delivery_field(self):
        """
        Test the existance of `delivery` field in `ScheduleForm`
        """
        self.assertTrue('delivery' in self.form.fields)

    def test_form_has_user_field(self):
        """
        Test the existance of `employee` field in `ScheduleForm`
        """
        self.assertTrue('employee' in self.form.fields)

    def test_save_method_with_removed_trainers(self):
        """
        When a trainer is removed from the selected trainers while saving the
        form, ensure the corresponding schedules are deleted.
        """
        delivery = DeliveryFactory()
        schedule1 = ScheduleFactory(delivery=delivery)
        schedule2 = ScheduleFactory(delivery=delivery)
        schedule3 = ScheduleFactory(delivery=delivery)

        role = Role.objects.get(name=RoleName.TRAINER.value)
        schedule1.user.profile.roles.add(role)
        schedule2.user.profile.roles.add(role)

        form_data = {
            'delivery': delivery.pk,
            'employee': [schedule1.user.pk, schedule2.user.pk]
        }

        form = ScheduleForm(data=form_data)
        form.is_valid()
        form.save()

        qs = Schedule.objects.all()
        self.assertTrue(schedule1 in qs)
        self.assertTrue(schedule2 in qs)
        self.assertTrue(schedule3 not in qs)


class ProcessTypeFormTest(TestCase):
    """
    Tests for `ProcessTypeForm` form
    """

    def test_get_redirect_url_with_definite_planning_choice(self):
        """
        Test the `get_redirect_url` method when `DEFINITE` choice
        is selected for `process` type radio field.
        """
        form_data = {'process': ProcessTypeForm.DEFINITE}
        form = ProcessTypeForm(data=form_data)
        form.is_valid()
        self.assertEqual(
            form.get_redirect_url(),
            reverse('planning:definite-request-create')
        )

    def test_get_redirect_url_with_option_planning_choices(self):
        """
        Test the `get_redirect_url` method when `OPTION` choice is
        selected for `process` type radio field.
        """
        form_data = {'process': ProcessTypeForm.OPTION}
        form = ProcessTypeForm(data=form_data)
        form.is_valid()
        self.assertEqual(
            form.get_redirect_url(),
            reverse('planning:option-request-create')
        )
