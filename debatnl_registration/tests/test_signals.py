from django.test import TestCase
from django.utils import timezone
from post_office.models import Email

from debatnl_registration.tests.factories import RegistrationFactory, \
    ParticipantFactory, TrainingDateFactory, AttendanceFactory
from feedback.models import JotFormTrainingReview
from feedback.tests.factories import JotFormTrainingReviewFactory
from shared.enums import RoleName
from users.models import Role
from users.tests.factories import UserFactory


class SendReviewTrainingSignal(TestCase):
    fixtures = ['roles']

    def setUp(self):
        role = Role.objects.get(name=RoleName.ADMIN.value)
        user = UserFactory()
        user.profile.roles.add(role)

        registration = RegistrationFactory()
        self.participant = ParticipantFactory(registration=registration)

        training_date = TrainingDateFactory(date=timezone.now())
        AttendanceFactory(
            training_date=training_date,
            participant=self.participant
        )

    def test_with_valid_scores(self):
        JotFormTrainingReviewFactory(
            reviewer_name=self.participant.name,
            score=9
        )

        self.participant.refresh_from_db()

        self.assertEqual(Email.objects.count(), 1)
        self.assertEqual(JotFormTrainingReview.objects.count(), 1)
        self.assertTrue(self.participant.has_feedback)

    def test_with_invalid_scores(self):
        JotFormTrainingReviewFactory(
            reviewer_name=self.participant.name,
            score=6
        )

        self.assertEqual(Email.objects.count(), 0)
        self.assertEqual(JotFormTrainingReview.objects.count(), 1)
        self.assertFalse(self.participant.has_feedback)
