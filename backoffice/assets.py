from django_assets import Bundle, register

js = Bundle('js/base.js', 'backoffice/js/configuration.js', filters='jsmin', output='gen/configuration.packed.js')
register('configuration_js', js)