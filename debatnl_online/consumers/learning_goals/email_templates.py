from debatnl_online.consumers.base import BaseConsumer


class EmailTemplateConsumer(BaseConsumer):
    def __init__(self):
        self.url = '{}/api/learning-goals/email-templates'.format(
            self.base_url)
