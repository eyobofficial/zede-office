from backoffice.utilities.emails import BaseEmail


class BaseFollowUpEmail(BaseEmail):
    template_location = '../templates/follow_ups/emails'
    email_type = ''
