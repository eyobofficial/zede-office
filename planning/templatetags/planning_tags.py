from django import template

from planning.models import PlanningRequest

register = template.Library()


@register.filter(name='type')
def filter_type(delivery_list, type_name):
    types = {v: k for k, v in PlanningRequest.TYPE_CHOICES}
    return delivery_list.filter(planning_request__type=types[type_name])


@register.filter(name='status')
def filter_status(delivery_list, status):
    statuses = {v: k for k, v in PlanningRequest.STATUS_CHOICES}
    return delivery_list.filter(planning_request__status=statuses[status])


@register.filter(name='exclude_type')
def exclude_type(delivery_list, type_name):
    """
    Exclude delivery instance based on it's type
    """
    types = {v: k for k, v in PlanningRequest.TYPE_CHOICES}
    return delivery_list.exclude(planning_request__type=types[type_name])
