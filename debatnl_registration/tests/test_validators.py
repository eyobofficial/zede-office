from django.test import TestCase

from debatnl_registration.tests.factories import RegistrationFactory, \
    TrainingDateFactory, ParticipantFactory, AttendanceFactory
from debatnl_registration.utilities.validators import validate_registration


class RegistrationValidatorTest(TestCase):
    """
    Test the validation of the registration object.
    """

    def setUp(self):
        self.registration = RegistrationFactory()
        participant = ParticipantFactory(registration=self.registration)
        training_date = TrainingDateFactory()
        AttendanceFactory(participant=participant, training_date=training_date)

    def test_valid_registration(self):
        is_valid = validate_registration(self.registration)
        self.assertTrue(is_valid)

    def test_incorrect_amount_of_training_dates(self):
        self.registration.duration = 3

        with self.assertLogs(level='ERROR') as log:
            is_valid = validate_registration(self.registration)
            self.assertFalse(is_valid)
            self.assertIn(
                'Reason: The defined duration is not equal',
                log.output[0]
            )

    def test_incorrect_amount_of_participants(self):
        self.registration.participant_count = 5
        ParticipantFactory.create_batch(2, registration=self.registration)

        with self.assertLogs(level='ERROR') as log:
            is_valid = validate_registration(self.registration)
            self.assertFalse(is_valid)
            self.assertIn(
                'Reason: The defined participant count is not equal',
                log.output[0]
            )
