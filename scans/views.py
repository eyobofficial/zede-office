from django.db.models import Q
from django.shortcuts import get_object_or_404, redirect
from django.urls import reverse_lazy
from django.utils import timezone
from django.views.generic import FormView, ListView, DetailView, DeleteView, \
    UpdateView, TemplateView

from backoffice.constants import GROUP_SCANS
from backoffice.mixins import GroupAccessMixin
from backoffice.views import BaseEmailView

from briefings.models import Deliverable

from scans.emails.notifications import NewScanEmail, UploadConfirmationEmail, \
    UploadDRNotificationEmail, ScanDeniedEmail, ScanApprovedEmail
from scans.emails.reminders import ScanSubmissionReminderEmail, \
    ScanReviewReminderEmail
from scans.forms import ScanFormSet, ScanApproveForm
from scans.mixins import ScanTrainersMixin
from scans.models import Scan


class OverviewView(GroupAccessMixin, ListView):
    template_name = 'scans/base.html'
    model = Scan
    access_groups = [GROUP_SCANS]

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'pending_scans': self.object_list.exclude(status=Scan.FINISHED),
            'finished_scans': self.object_list.filter(status=Scan.FINISHED)
        })
        return context


class ScanCreateView(GroupAccessMixin, ScanTrainersMixin, FormView):
    template_name = 'scans/components/modals/modal-create.html'
    form_class = ScanFormSet
    success_url = reverse_lazy('briefings:overview')
    access_groups = [GROUP_SCANS]

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['queryset'] = Scan.objects.none()
        return kwargs

    def get_initial(self):
        deliverable = get_object_or_404(
            Deliverable,
            pk=self.kwargs['pk']
        )
        initial = [
            {
                'organization': deliverable.delivery.organization.name,
                'dr_name': deliverable.delivery.responsible.name,
                'dr_email': deliverable.delivery.responsible.email
            }
        ]
        return initial

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        selected_deliverable = get_object_or_404(
            Deliverable,
            pk=self.kwargs['pk']
        )
        related_deliverables = selected_deliverable.get_pending_related()
        pending_deliverables = related_deliverables.filter(
            Q(delivery__product='Debatscan') |
            Q(delivery__product='Voorzittersscan')
        )
        context['selected_deliverable'] = selected_deliverable
        context['pending_deliverables'] = pending_deliverables
        context['formset'] = context.get('form')
        return context

    def form_valid(self, formset):
        data = dict(formset.data)
        deliverables = []
        deliverable_pks = data.get('deliverables')
        for key in deliverable_pks:
            deliverable = Deliverable.objects.get(pk=int(key))
            deliverables.append(deliverable)
        scans = formset.save(deliverables)
        self.send_emails(scans)
        return super().form_valid(formset)

    def form_invalid(self, formset):
        return redirect('briefings:overview')

    @staticmethod
    def send_emails(scans):
        trainer_ids = []
        for scan in scans:
            if scan.trainer_id not in trainer_ids:
                NewScanEmail(scan).send()
                trainer_ids.append(scan.trainer_id)


class ScanDetailView(GroupAccessMixin, ScanTrainersMixin, DetailView):
    template_name = 'scans/components/modals/modal-details.html'
    model = Scan
    access_groups = [GROUP_SCANS]


class ScanDeleteView(GroupAccessMixin, DeleteView):
    template_name = 'scans/components/modals/modal-delete.html'
    success_url = reverse_lazy('scans:overview')
    model = Scan
    access_groups = [GROUP_SCANS]


class ScanUploadView(GroupAccessMixin, UpdateView):
    template_name = 'scans/components/modals/modal-upload.html'
    success_url = reverse_lazy('scans:overview')
    model = Scan
    access_groups = [GROUP_SCANS]
    fields = ('client_name', 'client_filename', 'status')
    initial = {'status': Scan.SUBMITTED}

    def form_valid(self, form):
        self.object.submitted_at = timezone.now()
        self.object.save()

        UploadConfirmationEmail(self.object).send()
        UploadDRNotificationEmail(self.object).send()
        return super().form_valid(form)


class ScansDeliverableView(GroupAccessMixin, ScanTrainersMixin, DetailView):
    template_name = 'scans/components/modals/modal-deliverable-scans.html'
    model = Deliverable
    access_groups = [GROUP_SCANS]


class ScanUpdateView(GroupAccessMixin, ScanTrainersMixin, FormView):
    template_name = 'scans/components/modals/modal-update.html'
    form_class = ScanFormSet
    model = Scan
    success_url = reverse_lazy('briefings:overview')
    access_groups = [GROUP_SCANS]

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        deliverable = get_object_or_404(Deliverable, pk=self.kwargs['pk'])
        extra_count = self.request.GET.get('extra')
        initial = [
            {
                'organization': deliverable.delivery.organization.name,
                'dr_name': deliverable.delivery.responsible.name,
                'dr_email': deliverable.delivery.responsible.email
            }
        ]

        if extra_count and extra_count.isdigit():
            delta = int(extra_count) - deliverable.scans.count()
            kwargs['initial'] = initial * delta

        kwargs['queryset'] = deliverable.scans.all()
        return kwargs

    def get_form(self, form_class=None):
        formset = super().get_form(form_class)
        extra_count = self.request.GET.get('extra')

        if extra_count and extra_count.isdigit():
            formset.extra = int(extra_count) - formset.initial_form_count()
        else:
            formset.extra = 0

        return formset

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['deliverable'] = get_object_or_404(
            Deliverable,
            pk=self.kwargs['pk']
        )
        context['formset'] = context.get('form')
        return context

    def form_valid(self, formset):
        deliverable = get_object_or_404(Deliverable, pk=self.kwargs['pk'])
        scan = deliverable.scans.first()
        formset.save(list(scan.deliverables.all()))
        return super().form_valid(formset)

    def form_invalid(self, formset):
        return redirect('briefings:overview')


class ScanApproveView(UpdateView):
    template_name = 'scans/public/approval.html'
    form_class = ScanApproveForm
    model = Scan
    success_url = reverse_lazy('scans:confirm')

    def get_template_names(self):
        if not self.object or self.object.status != Scan.SUBMITTED:
            self.template_name = 'common/public/expired_page.html'
        return super().get_template_names()


class ScanConfirmationView(TemplateView):
    template_name = 'scans/public/approval_confirmation.html'


class BaseScanEmailView(BaseEmailView):
    url = 'admin:scans_scan_change'
    model = Scan


class NewScanEmailView(BaseScanEmailView):
    email_class = NewScanEmail
    email_name = 'new scan notification'


class ScanDeniedEmailView(BaseScanEmailView):
    email_class = ScanDeniedEmail
    email_name = 'scan denied notification'


class ScanApprovedEmailView(BaseScanEmailView):
    email_class = ScanApprovedEmail
    email_name = 'scan approved notification'


class UploadConfirmationEmailView(BaseScanEmailView):
    email_class = UploadConfirmationEmail
    email_name = 'upload confirmation'


class UploadDRNotificationEmailView(BaseScanEmailView):
    email_class = UploadDRNotificationEmail
    email_name = 'upload DR notification'


class ScanSubmissionReminderEmailView(BaseScanEmailView):
    email_class = ScanSubmissionReminderEmail
    email_name = 'scan submission reminder'


class ScanReviewReminderEmailView(BaseScanEmailView):
    email_class = ScanReviewReminderEmail
    email_name = 'scan review reminder'
