from django.contrib import admin

from backoffice.admin import CustomURLModelAdmin

from intakes.models import Intake, IntakeReportQuestion, IntakeReminder, \
    IntakeReportFile
from intakes.views import SendParticipantReportEmail, \
    SendTrainerInsightReportEmail, SendOrganizationInsightReportEmail, \
    SendIntakeCheckEmail, SendTrainerDateNotificationEmail, \
    SendTrainerDateChangeNotificationEmail


@admin.register(Intake)
class IntakeAdmin(CustomURLModelAdmin):
    list_display = ('organization', 'contact_person', 'report_date',)
    custom_urls = [
        {
            'regex': r'^(?P<pk>.+)/participant-report/$',
            'view': SendParticipantReportEmail,
            'name': 'send_participants_report'
        },
        {
            'regex': r'^(?P<pk>.+)/organization-insight-report/$',
            'view': SendOrganizationInsightReportEmail,
            'name': 'send_organization_insight_report'
        },
        {
            'regex': r'^(?P<pk>.+)/trainer-insight-report/$',
            'view': SendTrainerInsightReportEmail,
            'name': 'send_trainer_insight_report'
        },
        {
            'regex': r'^(?P<pk>.+)/intake-check/$',
            'view': SendIntakeCheckEmail,
            'name': 'send_intake_check'
        },
        {
            'regex': r'^(?P<pk>.+)/trainer-date-notification/$',
            'view': SendTrainerDateNotificationEmail,
            'name': 'send_trainer_date_notification'
        },
        {
            'regex': r'^(?P<pk>.+)/trainer-date-change-notification$',
            'view': SendTrainerDateChangeNotificationEmail,
            'name': 'send_trainer_date_change_notification'
        }
    ]


@admin.register(IntakeReportQuestion)
class IntakeReportQuestionAdmin(admin.ModelAdmin):
    list_display = ('intake', 'qid',)


@admin.register(IntakeReminder)
class IntakeReminderAdmin(admin.ModelAdmin):
    list_display = ('intake', 'date',)


@admin.register(IntakeReportFile)
class IntakeReminderAdmin(admin.ModelAdmin):
    list_display = ('intake', 'file',)
