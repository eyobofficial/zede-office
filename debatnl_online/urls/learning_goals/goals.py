from django.conf.urls import url
from django.contrib.auth.decorators import login_required

from debatnl_online.views.learning_goals.goals import GoalCreate, GoalUpdate, \
    GoalDelete

urlpatterns = [
    url(
        r'^create/$',
        login_required(GoalCreate.as_view()),
        name='create'
    ),
    url(
        r'^update/(?P<pk>[-\w]+)/$',
        login_required(GoalUpdate.as_view()),
        name='update'
    ),
    url(
        r'^delete/(?P<pk>[-\w]+)/$',
        login_required(GoalDelete.as_view()),
        name='delete'
    )
]
