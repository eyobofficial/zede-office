from django import forms
from django.forms import modelformset_factory
from django.forms.models import inlineformset_factory, BaseInlineFormSet, \
    BaseModelFormSet

from .models import Deliverable, Briefing, BriefingFile, BriefingQuestion


class DeliverableForm(forms.ModelForm):
    class Meta:
        model = Deliverable
        fields = ['id', 'delivery']


class BaseDeliverableFormSet(BaseInlineFormSet):
    """
    If a list of deliverable object is provided,
    update briefing field to the instance and
    status to submitted.
    """
    def save(self, deliverable_list=None, commit=False):
        if deliverable_list:
            for form in self.forms:
                for deliverable in deliverable_list:
                    deliverable.briefing = self.instance
                    deliverable.status = Deliverable.SUBMITTED
                    deliverable.save()
        return super().save(commit)


DeliverableFormSet = inlineformset_factory(
    Briefing,
    Deliverable,
    form=DeliverableForm,
    formset=BaseDeliverableFormSet,
    extra=1, can_delete=False
)


class BriefingForm(forms.ModelForm):
    deliverables = forms.ModelMultipleChoiceField(
        label='Zijn er nog andere deliveries waarvoor je een briefing '
              'wilt toevoegen?',
        queryset=Deliverable.objects.all(),
        required=False
    )

    class Meta:
        model = Briefing
        fields = ['time', 'address', 'reason', 'program']


class BriefingFileForm(forms.ModelForm):
    class Meta:
        model = BriefingFile
        fields = ['briefing', 'file']


class BaseBriefingFileFormSet(BaseInlineFormSet):
    def save(self, commit=False):
        """
        When saving the current briefing instance,
        create briefing files record.
        """

        if self.files:
            for file in self.files.getlist('files'):
                self.instance.files.create(file_name=file.name, file=file)

        for form in self.deleted_forms:
            form.instance.delete()

        return super().save(commit)


BriefingFileFormSet = inlineformset_factory(
    Briefing,
    BriefingFile,
    form=BriefingFileForm,
    formset=BaseBriefingFileFormSet,
    extra=0, can_delete=True
)


class BaseQuestionFormSet(BaseInlineFormSet):
    def save(self, user=None, commit=True):
        briefing_questions = []
        if user:
            for form in self.forms:
                form.instance.user = user
                question = form.save()
                briefing_questions.append(question)
            super().save(commit)
        return briefing_questions


class QuestionModelForm(forms.ModelForm):
    class Meta:
        model = BriefingQuestion
        fields = ('question', 'answer')


QuestionFormSet = inlineformset_factory(
    Briefing,
    BriefingQuestion,
    form=QuestionModelForm,
    formset=BaseQuestionFormSet,
    min_num=1, extra=0,
    can_delete=False
)


class BaseQuestionModelFormSet(BaseModelFormSet):
    def add_fields(self, form, index):
        super().add_fields(form, index)
        form.fields['files'] = forms.FileField(
            required=False,
            widget=forms.ClearableFileInput(attrs={'multiple': True})
        )

    def save(self, commit=False):
        briefing_questions = []
        for form in self.forms:
            question = form.save()
            briefing_questions.append(question)
            field_name = f'{form.prefix}-files'
            if form.files:
                for file in form.files.getlist(field_name):
                    question.files.create(filename=file.name, file=file)
        return briefing_questions


QuestionModelFormSet = modelformset_factory(
    BriefingQuestion,
    fields=('answer', ),
    formset=BaseQuestionModelFormSet,
    extra=0
)
