import ast
import os
from io import BytesIO

import pendulum
from PyPDF2 import PdfFileReader, PdfFileWriter
from django.core.files.base import ContentFile
from reportlab.lib.colors import HexColor
from reportlab.lib.styles import ParagraphStyle
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.pdfgen import canvas
from reportlab.platypus import SimpleDocTemplate


class BaseIntakeReport:
    base_template = 'template_content_page.pdf'
    title_template = 'template_title_page.pdf'
    title = None
    additional_styles = None
    additional_fonts = None
    _default_fonts = (
        ('Calibri', 'Calibri.ttf'),
        ('Calibri-Bold', 'Calibri-Bold.ttf')
    )
    _default_styles = {
        'header': ParagraphStyle(
            'Header',
            fontName='Calibri',
            fontSize=14,
            textColor=HexColor('#FF6600'),
            leading=20,
            spaceBefore=4,
            spaceAfter=15
        ),
        'body': ParagraphStyle(
            'Body',
            fontName='Calibri',
            fontSize=11,
            textColor=HexColor('#08304F'),
            leading=16
        ),
        'question': ParagraphStyle(
            'Question',
            fontName='Calibri-Bold',
            fontSize=11,
            textColor=HexColor('#08304F'),
            leading=16,
            spaceBefore=15
        )
    }

    def __init__(self, intake):
        self.base_path = os.path.dirname(os.path.realpath(__file__))
        self.styles = self.load_styles()
        self.intake = intake
        self._register_fonts()

    def generate(self):
        title_page = self._generate_title_page(self.title)

        writer = PdfFileWriter()
        writer.addPage(title_page)

        pages = self._generate_content_pages()

        for page in pages:
            writer.addPage(page)

        buffer = BytesIO()
        writer.write(buffer)
        return ContentFile(buffer.getvalue())

    def _generate_title_page(self, title):
        template = self._get_template(self.title_template)

        # Create canvas
        buffer = BytesIO()
        c = canvas.Canvas(buffer)

        # Set title
        c.setFillColor(HexColor('#FF6600'))
        c.setFont('Calibri-Bold', 20)
        c.drawCentredString(153, 707, title)

        # Set date
        c.setFillColor(HexColor('#08304F'))
        c.setFont('Calibri', 11)
        today = pendulum.today().format('DD-MM-YYYY')
        c.drawCentredString(203, 406, today)

        # Save canvas
        c.showPage()
        c.save()
        buffer.seek(0)

        # Generate page
        page = template.getPage(0)
        page.mergePage(PdfFileReader(buffer).getPage(0))

        return page

    def _generate_content_pages(self):
        buffer = BytesIO()
        template = self._get_template(self.base_template)
        document = self.get_document(buffer)

        # Build document
        document.build(self.get_content())
        buffer.seek(0)

        # Read content
        reader = PdfFileReader(buffer)

        # Combine content with base template
        pages = []
        template_page = template.getPage(0)
        for n in range(0, reader.getNumPages()):
            page = reader.getPage(n)
            page.mergePage(template_page)
            pages.append(page)

        return pages

    def _get_template(self, filename):
        path = os.path.join(self.base_path, 'templates', filename)
        file = open(path, 'rb')
        return PdfFileReader(file)

    def _register_fonts(self):
        if self.additional_fonts:
            self._default_fonts += self.additional_fonts

        for font_name, font_filename in self._default_fonts:
            path = os.path.join(self.base_path, 'fonts', font_filename)
            font = TTFont(font_name, path)
            pdfmetrics.registerFont(font)

    def get_content(self):
        raise NotImplementedError

    def load_styles(self):
        if self.additional_styles:
            self._default_styles.update(self.additional_styles)
        return self._default_styles

    @staticmethod
    def get_document(buffer):
        return SimpleDocTemplate(buffer, topMargin=100, leftMargin=58)

    @staticmethod
    def evaluate_value(value):
        text = value if value else '--'

        try:
            values = ast.literal_eval(value)

            if isinstance(values, dict):
                text = '<br/>'.join(values.values())

            elif isinstance(values, list):
                text = '<br/>'.join(values)

        except (ValueError, SyntaxError):
            pass

        return text
