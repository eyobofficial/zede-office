from debatnl_online.consumers.base import BaseConsumer


class ParticipantConsumer(BaseConsumer):
    def __init__(self):
        self.url = '{}/api/e-learning/participants'.format(self.base_url)
