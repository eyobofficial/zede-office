from django.contrib.auth.models import User

from DebatNL_BackOffice.celery import app
from backoffice.constants import GROUP_SCHEDULES
from planning.emails.reminders import RequestAvailabilityReminder
from shared.enums import RoleName


@app.task
def send_request_availability_email():
    """
    Send request availability e-mail in the morning
    (e.g. 10 am Europe/Amsterdam on the 15th of each month)
    """
    trainers = User.objects.filter(
        profile__roles__name=RoleName.TRAINER.value,
        profile__app_permissions__app_name=GROUP_SCHEDULES,
        is_active=True
    )

    for trainer in trainers:
        RequestAvailabilityReminder(trainer).send()
