import uuid

import pendulum
from django.conf import settings
from django.contrib.auth.models import User
from django.db import models
from django.utils import timezone


class VacationPeriod(models.Model):
    PAID = 'PAID'
    UNPAID = 'UNPAID'

    TYPE_CHOICES = (
        (PAID, 'Paid'),
        (UNPAID, 'Unpaid'),
    )

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    type = models.CharField(max_length=50, blank=True, default='',
                            choices=TYPE_CHOICES)
    start_date = models.DateField()
    end_date = models.DateField()
    amount_hours = models.IntegerField()
    reason = models.TextField()
    user = models.ForeignKey(User, related_name='vacations',
                             on_delete=models.CASCADE)

    def on_vacation(self, some_date):
        return self.start_date <= some_date < self.end_date

    def get_status(self):
        pending = VacationPeriodApproval.PENDING
        approved = VacationPeriodApproval.APPROVED

        if self.approvals.filter(status=pending).exists():
            return pending
        elif all(a.status == approved for a in self.approvals.all()):
            return approved
        else:
            return VacationPeriodApproval.DENIED

    def get_last_approval(self):
        approvals = [a.status == a.APPROVED for a in self.approvals.all()]
        if len(approvals) >= 2:
            return self.approvals.order_by('approved_at').last()
        return self.approvals.none()

    def is_approved_in_month(self, month, year):
        month = int(month)
        year = int(year)
        approval = self.get_last_approval()

        if not approval or not approval.approved_at:
            return False

        approved_at = pendulum.instance(approval.approved_at)
        approved_at = approved_at.in_tz(settings.TIME_ZONE)
        return month == approved_at.month and year == approved_at.year


class VacationPeriodApproval(models.Model):
    PENDING = 'PENDING'
    APPROVED = 'APPROVED'
    DENIED = 'DENIED'

    STATUS_CHOICES = (
        (PENDING, PENDING),
        (APPROVED, APPROVED),
        (DENIED, DENIED),
    )

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    email = models.CharField(max_length=255)
    reason = models.TextField(default='', blank=True)
    status = models.CharField(max_length=15, choices=STATUS_CHOICES,
                              default=PENDING)
    vacation_period = models.ForeignKey(VacationPeriod,
                                        related_name='approvals',
                                        on_delete=models.CASCADE)
    approved_at = models.DateTimeField(null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def approve(self):
        self.approved_at = timezone.now()
        self.status = self.APPROVED
        self.save()


class Timetable(models.Model):
    MORNING = 'MO'
    AFTERNOON = 'AF'
    EVENING = 'EV'

    AVAILABLE = 'AV'
    NOT_AVAILABLE = 'NA'
    PREFERRED_NOT_AVAILABLE = 'PN'

    DAYPART_CHOICES = (
        (MORNING, 'MORNING'),
        (AFTERNOON, 'AFTERNOON'),
        (EVENING, 'EVENING')
    )

    STATE_CHOICES = (
        (AVAILABLE, 'AVAILABLE'),
        (NOT_AVAILABLE, 'NOT AVAILABLE'),
        (PREFERRED_NOT_AVAILABLE, 'PREFERRED NOT AVAILABLE')
    )

    period_id = models.CharField(max_length=15, blank=True, default='',
                                 db_index=True)
    year = models.IntegerField()
    week = models.IntegerField()
    daypart = models.CharField(choices=DAYPART_CHOICES, max_length=2)
    monday = models.CharField(choices=STATE_CHOICES, max_length=2,
                              default=AVAILABLE)
    tuesday = models.CharField(choices=STATE_CHOICES, max_length=2,
                               default=AVAILABLE)
    wednesday = models.CharField(choices=STATE_CHOICES, max_length=2,
                                 default=AVAILABLE)
    thursday = models.CharField(choices=STATE_CHOICES, max_length=2,
                                default=AVAILABLE)
    friday = models.CharField(choices=STATE_CHOICES, max_length=2,
                              default=AVAILABLE)
    saturday = models.CharField(choices=STATE_CHOICES, max_length=2,
                                default=AVAILABLE)
    sunday = models.CharField(choices=STATE_CHOICES, max_length=2,
                              default=AVAILABLE)
    user = models.ForeignKey(User, related_name='timetables',
                             on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    @staticmethod
    def get_period_id(year, week):
        return f'{year}-W{week:02d}'

    @staticmethod
    def init_section(year, week, user):
        for key, value in Timetable.DAYPART_CHOICES:
            period_id = Timetable.get_period_id(year, week)
            Timetable.objects.get_or_create(year=year, week=week, daypart=key,
                                            period_id=period_id, user=user)
