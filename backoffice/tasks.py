from django.conf import settings
from django.core.management import call_command

from DebatNL_BackOffice.celery import app


@app.task
def send_queued_emails():
    if settings.ALLOW_EMAIL_SENDING:
        call_command('send_queued_mail')


@app.task
def clean_up_emails(**kwargs):
    call_command('cleanup_mail', days=kwargs.get('days', 90))


@app.task
def print_hello_world():
    print('Hello World - Message Approved by CIRX Software')
