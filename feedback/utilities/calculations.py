from feedback.models import JotFormTraining


def calculate_average_review_score(month, year, trainer=None):
    """
    Calculate the average review score based on trainer, month and year.

    :param trainer: (JotFormTrainer) The trainer.
    :param month: (int) The month number.
    :param year: (int) The year.
    :return: (dict)
    """

    # Initialize the values.
    total_score = amount_submissions = 0
    average_score = 0
    scores_list = []

    # Retrieve trainings based on the trainer, month and year.
    trainings = JotFormTraining.objects.filter(date__month=month,
                                               date__year=year)

    if trainer:
        trainings = trainings.filter(trainer=trainer)

    # Retrieve review score of each training and count the amount of reviews.
    for training in trainings:
        reviews = training.reviews.all()

        for review in reviews:
            if review.score:
                total_score += review.score
                amount_submissions += 1
                scores_list.append(review.score)

    # Calculate the average review score.
    if amount_submissions > 0:
        average_score = total_score / amount_submissions

    # Calculate the occurrences of each review score in percentages.
    occurrences = {}
    for index in range(5, 11):
        count = scores_list.count(index)
        percentage = '0%'

        if count > 0:
            percentage = '%s%%' % str((count / amount_submissions) * 100)

        occurrences[index] = {
            'count': count,
            'percentage':  percentage,
        }

    return {
        'amount_submissions': amount_submissions,
        'average_score': "%.1f" % round(average_score, 1),
        'month': month,
        'occurrences': occurrences
    }
