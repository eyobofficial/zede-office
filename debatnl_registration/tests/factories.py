from datetime import datetime, timedelta

import factory

from backoffice.tests.factories import TrainingFactory
from debatnl_registration.models import Registration, Participant, \
    PlannedTraining, TrainingDate, Attendance, BloomvilleFeedItem, \
    SpringestFeedItem, PlannedTrainingToken


class RegistrationFactory(factory.django.DjangoModelFactory):
    form_id = 1
    submission_id = factory.Sequence(lambda n: n + 1)
    training = factory.Faker('sentence', nb_words=3, variable_nb_words=True)
    duration = 1
    participant_count = 1
    registrant_first_name = factory.Faker('first_name')
    registrant_last_name = factory.Faker('last_name')
    registrant_phone = factory.Faker('phone_number')
    registrant_email = factory.LazyAttribute(
        lambda r: '{}.{}@example.com'.format(
            r.registrant_first_name,
            r.registrant_last_name
        ).lower()
    )

    class Meta:
        model = Registration


class ParticipantFactory(factory.django.DjangoModelFactory):
    name = factory.Faker('name')
    phone = factory.Faker('phone_number')
    email = factory.LazyAttribute(
        lambda p: '{}@example.com'.format(
            p.name.replace('.', '').replace(' ', '').lower())
    )
    dietary_wishes = factory.Faker('sentence', nb_words=5,
                                   variable_nb_words=True)
    registration = factory.SubFactory(RegistrationFactory)

    class Meta:
        model = Participant


class PlannedTrainingFactory(factory.django.DjangoModelFactory):
    training = factory.SubFactory(TrainingFactory)

    class Meta:
        model = PlannedTraining


class PlannedTrainingTokenFactory(factory.django.DjangoModelFactory):
    planned_training = factory.SubFactory(PlannedTrainingFactory)
    participant = factory.SubFactory(ParticipantFactory)

    class Meta:
        model = PlannedTrainingToken


class TrainingDateFactory(factory.django.DjangoModelFactory):
    date = factory.Sequence(
        lambda n: (datetime.now() + timedelta(days=1)).date()
    )
    sequence = factory.Sequence(lambda n: n)
    planned_training = factory.SubFactory(PlannedTrainingFactory)

    class Meta:
        model = TrainingDate


class AttendanceFactory(factory.django.DjangoModelFactory):
    participant = factory.SubFactory(ParticipantFactory)
    training_date = factory.SubFactory(TrainingDateFactory)

    class Meta:
        model = Attendance


class BloomvilleFeedItemFactory(factory.django.DjangoModelFactory):
    id = factory.Faker('uuid4')
    category = factory.Iterator(['1 dag', '3 dagen'])
    description = factory.Faker('paragraphs', nb=3)
    course_format = factory.Faker('paragraph', nb_sentences=3)
    price = 999.99

    class Meta:
        model = BloomvilleFeedItem


class SpringestFeedItemFactory(factory.django.DjangoModelFactory):
    id = factory.Faker('uuid4')
    category = factory.Iterator(['1 dag', '3 dagen'])
    description = factory.Faker('paragraphs', nb=3)
    website = factory.Faker('url')
    price = 999.99
    vat_percentage = 21

    class Meta:
        model = SpringestFeedItem
