from django.conf.urls import url

from .views import OverviewView, TrainingCreateView, TrainingUpdateView, \
    TrainingDeleteView, UserModifyOptionView

urlpatterns = [
    url(r'^$', OverviewView.as_view(), name='overview'),
    url(
        r'^trainings/create/$',
        TrainingCreateView.as_view(),
        name='training-create'
    ),
    url(
        r'^trainings/(?P<pk>[a-zA-Z0-9-]+)/update/$',
        TrainingUpdateView.as_view(),
        name='training-update'
    ),
    url(
        r'^trainings/(?P<pk>[a-zA-Z0-9-]+)/delete/$',
        TrainingDeleteView.as_view(),
        name='training-delete'
    ),
    url(
        r'^users/modify/$',
        UserModifyOptionView.as_view(),
        name='user-modify'
    ),
]
