from django.contrib.auth.models import User, Permission
from django.test import TestCase, Client
from django.urls import reverse
from mock import patch
from post_office.models import Email

from backoffice.constants import GROUP_BACKOFFICE
from shared.enums import RoleName, Position
from users.models import Role, AppPermission
from users.tests.factories import UserFactory


class UserCreateViewTest(TestCase):
    """
    Tests for `UserCreateView`
    """
    fixtures = ['roles', 'app_permissions']

    def setUp(self):
        role = Role.objects.get(name=RoleName.ADMIN.value)
        admin = UserFactory()
        admin.profile.roles.add(role)
        self.user = UserFactory()
        app_permission = AppPermission.objects.get(app_name=GROUP_BACKOFFICE)
        self.user.profile.app_permissions.add(app_permission)
        self.client = Client()
        self.url = reverse('users:create')

    def test_request_with_anonymous_user(self):
        """
        Test request to `UserCreateView` from anonymous user
        """
        response = self.client.get(self.url, follow=True)

        # Assertions
        expected_url = f'{reverse("login")}?next=/feedback/'
        self.assertRedirects(response, expected_url)

    def test_post_request_with_form_data(self):
        """
        Test a post request to `UserCreateView` view with a valid form data
        """
        self.client.force_login(self.user)
        roles = Role.objects.all()
        permissions = AppPermission.objects.all()

        payload = {
            'first_name': 'John',
            'last_name': 'Doe',
            'email': 'john.doe@test.email',
            'position': Position.TRAINER.value,
            'roles': [roles[0].pk, roles[1].pk],
            'app_permissions': [permissions[0].pk]
        }

        response = self.client.post(self.url, payload)

        user = User.objects.last()

        # Assertions
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, '/configuration/')
        self.assertEqual(user.first_name, payload['first_name'])
        self.assertEqual(user.last_name, payload['last_name'])
        self.assertEqual(user.email, payload['email'])
        self.assertEqual(user.username, payload['email'])
        self.assertEqual(user.profile.position, Position.TRAINER.value)
        self.assertEqual(user.profile.roles.count(), 2)
        self.assertEqual(user.profile.app_permissions.count(), 1)
        self.assertEqual(Email.objects.count(), 1)

    def test_post_request_with_no_roles_and_app_permissions(self):
        self.client.force_login(self.user)

        payload = {
            'first_name': 'John',
            'last_name': 'Doe',
            'email': 'john.doe@test.email',
            'position': Position.TRAINER.value,
            'roles': [],
            'app_permissions': []
        }

        response = self.client.post(self.url, payload)

        user = User.objects.last()

        # Assertions
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, '/configuration/')
        self.assertEqual(user.first_name, payload['first_name'])
        self.assertEqual(user.last_name, payload['last_name'])
        self.assertEqual(user.email, payload['email'])
        self.assertEqual(user.username, payload['email'])
        self.assertEqual(user.profile.position, Position.TRAINER.value)
        self.assertEqual(user.profile.roles.count(), 0)
        self.assertEqual(user.profile.app_permissions.count(), 0)
        self.assertEqual(Email.objects.count(), 1)


class UserUpdateViewTest(TestCase):
    """
    Tests for `UserUpdateView` view
    """
    fixtures = ['roles', 'app_permissions']

    def setUp(self):
        self.user_1 = UserFactory()
        self.user_2 = UserFactory()

        app_permission = AppPermission.objects.get(app_name=GROUP_BACKOFFICE)
        self.user_1.profile.app_permissions.add(app_permission)

        self.client = Client()
        self.url = reverse('users:update', args=(self.user_2.pk,))

    def test_GET_request_with_anonymous_user(self):
        """
        Test a request to `UserUpdateView` with anonymous user
        """
        response = self.client.get(self.url, follow=True)
        self.assertRedirects(response, f'{reverse("login")}?next=/feedback/')

    def test_GET_request_with_authenticated_user(self):
        """
        Test a request to `UserUpdateView` with authenticated user
        """
        self.client.force_login(self.user_1)
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)

    def test_POST_request_with_authenticated_user(self):
        """
        Test a valid post request to `UserUpdateView`
        """
        self.client.force_login(self.user_1)
        roles = Role.objects.all()
        permissions = AppPermission.objects.all()

        payload = {
            'first_name': 'John',
            'last_name': 'Doe',
            'position': Position.TRAINER.value,
            'roles': [roles[0].pk, roles[1].pk],
            'app_permissions': [permissions[0].pk]
        }

        response = self.client.post(self.url, payload)
        self.user_2.refresh_from_db()
        self.user_2.profile.refresh_from_db()

        # Assertions
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, '/configuration/')
        self.assertEqual(self.user_2.first_name, payload['first_name'])
        self.assertEqual(self.user_2.last_name, payload['last_name'])
        self.assertEqual(self.user_2.profile.position, Position.TRAINER.value)
        self.assertEqual(self.user_2.profile.roles.count(), 2)
        self.assertEqual(self.user_2.profile.app_permissions.count(), 1)


class UserProfileUpdateViewTest(TestCase):
    """
    Tests for `UserProfileUpdateView` view
    """
    fixtures = ['roles', 'app_permissions']

    def setUp(self):
        self.user_1 = UserFactory()
        self.user_2 = UserFactory()

        app_permission = AppPermission.objects.get(app_name=GROUP_BACKOFFICE)
        self.user_1.profile.app_permissions.add(app_permission)

        self.client = Client()
        kwargs = {'pk': self.user_2.profile.pk}
        self.url = reverse('users:profile-update', kwargs=kwargs)

    def test_GET_request_with_anonymous_user(self):
        """
        Test a request to `UserUpdateView` with anonymous user
        """
        response = self.client.get(self.url, follow=True)
        self.assertRedirects(response, f'{reverse("login")}?next=/feedback/')

    def test_POST_request_with_authenticated_user(self):
        """
        Test a valid post request to `UserUpdateView`
        """
        self.client.force_login(self.user_1)

        payload = {
            'briefings': True,
            'feedback_trainers': True,
            'follow_ups': True
        }

        response = self.client.post(self.url, payload)
        self.user_2.refresh_from_db()

        permission_1 = Permission.objects.get(codename='view_all_briefings')
        permission_2 = Permission.objects.get(codename='view_all_followups')
        permission_3 = Permission.objects.get(codename='view_all_trainers')

        # Assertions
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, '/configuration/')
        self.assertIn(permission_1, self.user_2.user_permissions.all())
        self.assertIn(permission_2, self.user_2.user_permissions.all())
        self.assertIn(permission_3, self.user_2.user_permissions.all())


class UserDeactivateFormSetViewTest(TestCase):
    """
    Tests for `UserDeactivateFormSetView`
    """
    fixtures = ['app_permissions']

    def setUp(self):
        self.user = UserFactory()
        app_permission = AppPermission.objects.get(app_name=GROUP_BACKOFFICE)
        self.user.profile.app_permissions.add(app_permission)
        self.client = Client()
        self.url = reverse('users:group-deactivate')

    def test_request_with_anonymous_user(self):
        """
        Test request to `UserDeactivateFormSetView` from anonymous user
        """
        response = self.client.get(self.url, follow=True)
        self.assertRedirects(response, f'{reverse("login")}?next=/feedback/')

    def test_request_with_authenticated_user(self):
        """
        Test request to `UserDeactivateFormSetView` from authenticated user
        """
        self.client.force_login(self.user)

        user_1 = UserFactory(is_active=True)
        user_2 = UserFactory(is_active=False)
        user_3 = UserFactory(is_active=True, is_superuser=True)

        response = self.client.get(self.url)

        # Assertions
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, user_1.get_full_name())
        self.assertNotContains(response, user_2.get_full_name())
        self.assertNotContains(response, user_3.get_full_name())
        self.assertNotContains(response, self.user.get_full_name())

    def test_post_request_with_valid_form_data(self):
        """
        Test POST request to `UserDeactivateFormSetView` view with valid form
        data
        """
        self.client.force_login(self.user)

        user_1 = UserFactory(is_active=True)
        user_2 = UserFactory(is_active=True)
        user_3 = UserFactory(is_active=True)

        payload = {
            'form-TOTAL_FORMS': '3',
            'form-INITIAL_FORMS': '3',
            'form-MIN_NUM_FORMS': '0',
            'form-MAX_NUM_FORMS': '1000',
            'form-0-id': user_1.pk,
            'form-0-DELETE': True,
            'form-1-id': user_2.pk,
            'form-1-DELETE': True,
            'form-2-id': user_3.pk,
            'form-2-DELETE': False
        }

        response = self.client.post(self.url, payload)
        user_1.refresh_from_db()
        user_2.refresh_from_db()
        user_3.refresh_from_db()

        # Assertions
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, '/configuration/')
        self.assertFalse(user_1.is_active)
        self.assertFalse(user_2.is_active)
        self.assertTrue(user_3.is_active)


class UserDeactivateViewTest(TestCase):
    """
    Tests for `UserDeactivateView`
    """
    fixtures = ['app_permissions']

    def setUp(self):
        self.user_1 = UserFactory()
        self.user_2 = UserFactory()

        app_permission = AppPermission.objects.get(app_name=GROUP_BACKOFFICE)
        self.user_1.profile.app_permissions.add(app_permission)

        self.client = Client()
        self.url = reverse('users:deactivate', args=(self.user_2.pk,))

    def test_request_with_anonymous_user(self):
        """
        Test request to `UserDeactivateView` from anonymous user
        """
        response = self.client.get(self.url, follow=True)
        self.assertRedirects(response, f'{reverse("login")}?next=/feedback/')

    def test_request_with_authenticated_user(self):
        """
        Test request to `UserDeactivateView` from authenticated user
        """
        self.client.force_login(self.user_1)
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)

    def test_post_request_with_valid_form_data(self):
        """
        Test POST request to `UserDeactivateView` view with valid form
        data
        """
        self.client.force_login(self.user_1)
        response = self.client.post(self.url, {})
        self.user_2.refresh_from_db()

        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, '/configuration/')
        self.assertFalse(self.user_2.is_active)


class UserProfileHubstaffUpdateViewTest(TestCase):
    """
    Tests for `UserProfileHubstaffUpdateView`
    """
    fixtures = ['app_permissions']

    def setUp(self):
        self.user_1 = UserFactory()
        self.user_2 = UserFactory()

        app_permission = AppPermission.objects.get(app_name=GROUP_BACKOFFICE)
        self.user_1.profile.app_permissions.add(app_permission)

        self.hubstaff_users = [{'id': 1, 'name': 'Test User'}]

        self.client = Client()
        kwargs = {'pk': self.user_2.profile.pk}
        self.url = reverse('users:profile-hubstaff-update', kwargs=kwargs)

    def test_request_with_anonymous_user(self):
        """
        Test request to `UserProfileHubstaffUpdateView` from anonymous user
        """
        response = self.client.get(self.url, follow=True)
        self.assertRedirects(response, f'{reverse("login")}?next=/feedback/')

    @patch('users.views.HubstaffAPIClient')
    def test_request_with_authenticated_user(self, mock_client):
        """
        Test request to `UserProfileHubstaffUpdateView` from authenticated user
        """
        users = [{'id': 1, 'name': 'Test User'}]
        mock_client().get_users.return_value = users

        self.client.force_login(self.user_1)
        response = self.client.get(self.url)
        template = 'users/userprofile_hubstaff_form.html'
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, template)
        self.assertEqual(response.context['hubstaff_users'], users)

    @patch('hubstaff.client.HubstaffAPIClient')
    def test_post_request_with_valid_form_data(self, _):
        """
        Test POST request to `UserProfileHubstaffUpdateView` view with valid
        form data
        """
        self.client.force_login(self.user_1)
        payload = {'hubstaff_user_id': self.hubstaff_users[0]['id']}
        response = self.client.post(self.url, payload)

        self.user_2.profile.refresh_from_db()
        hubstaff_user_id = self.user_2.profile.hubstaff_user_id

        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, '/configuration/')
        self.assertEqual(hubstaff_user_id, self.hubstaff_users[0]['id'])


class UserProfileRemoveHubstaffUpdateViewTest(TestCase):
    """
    Tests for `UserProfileHubstaffUpdateView`
    """
    fixtures = ['app_permissions']

    def setUp(self):
        self.user_1 = UserFactory()
        self.user_2 = UserFactory()
        self.user_2.profile.hubstaff_user_id = '00000'
        self.user_2.profile.save()

        app_permission = AppPermission.objects.get(app_name=GROUP_BACKOFFICE)
        self.user_1.profile.app_permissions.add(app_permission)

        self.client = Client()
        kwargs = {'pk': self.user_2.profile.pk}
        self.url = reverse('users:profile-hubstaff-remove', kwargs=kwargs)

    def test_request_with_anonymous_user(self):
        """
        Test request to `UserProfileRemoveHubstaffUpdateView` from anonymous
        user
        """
        response = self.client.get(self.url, follow=True)
        self.assertRedirects(response, f'{reverse("login")}?next=/feedback/')

    def test_request_with_authenticated_user(self):
        """
        Test request to `UserProfileRemoveHubstaffUpdateView` from
        authenticated user
        """
        self.client.force_login(self.user_1)
        response = self.client.get(self.url)
        template = 'users/userprofile_remove_hubstaff_form.html'
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, template)

    def test_post_request_with_valid_form_data(self):
        """
        Test POST request to `UserProfileHubstaffUpdateView` view with valid
        form data
        """
        self.client.force_login(self.user_1)
        response = self.client.post(self.url, {'hubstaff_user_id': ''})
        self.user_2.profile.refresh_from_db()

        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, '/configuration/')
        self.assertIsNone(self.user_2.profile.hubstaff_user_id)


class UserProfileDisableHubstaffUpdateViewTest(TestCase):
    """
    Tests for `UserProfileDisableHubstaffUpdateView`
    """
    fixtures = ['app_permissions']

    def setUp(self):
        self.user_1 = UserFactory()
        app_permission = AppPermission.objects.get(app_name=GROUP_BACKOFFICE)
        self.user_1.profile.app_permissions.add(app_permission)
        self.client = Client()

    def test_request_with_wrong_user_pk(self):
        self.client.force_login(self.user_1)
        url = reverse('users:profile-hubstaff-disable', kwargs={'pk': 999})
        response = self.client.get(url)
        template = 'users/public/hubstaff_disabled_error.html'
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, template)

    def test_post_request_with_valid_form_data(self):
        """
        Test POST request to `UserProfileDisableHubstaffUpdateView` view with
        valid form data
        """
        user_2 = UserFactory()
        self.client.force_login(self.user_1)
        kwargs = {'pk': user_2.pk}
        url = reverse('users:profile-hubstaff-disable', kwargs=kwargs)

        self.assertTrue(user_2.profile.do_hubstaff_check)

        response = self.client.get(url)
        user_2.profile.refresh_from_db()
        template = 'users/public/hubstaff_disabled_success.html'

        self.assertEqual(response.status_code, 200)
        self.assertFalse(user_2.profile.do_hubstaff_check)
        self.assertTemplateUsed(response, template)
