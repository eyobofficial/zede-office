from django.contrib import admin

from backoffice.admin import CustomURLModelAdmin
from schedules.models import VacationPeriod, Timetable, VacationPeriodApproval
from schedules.views import NewVacationPeriodEmailView, \
    ApprovedVacationPeriodEmailView, DeniedVacationPeriodEmailView, \
    PhoneCareVacationPeriodEmailView, AdminVacationPeriodEmailView


class VacationPeriodApprovalInline(admin.TabularInline):
    model = VacationPeriodApproval
    extra = 0


@admin.register(VacationPeriod)
class VacationPeriodAdmin(CustomURLModelAdmin):
    list_display = ['user', 'start_date', 'end_date', 'type', 'amount_hours']
    search_fields = ['user__email', 'user__first_name', 'user__last_name']
    inlines = [VacationPeriodApprovalInline]
    custom_urls = [
        {
            'regex': r'^(?P<pk>.+)/send_new_vacation_period_email/$',
            'view': NewVacationPeriodEmailView,
            'name': 'send_new_vacation_period_email'
        },
        {
            'regex': r'^(?P<pk>.+)/send_approved_vacation_period_email/$',
            'view': ApprovedVacationPeriodEmailView,
            'name': 'send_approved_vacation_period_email'
        },
        {
            'regex': r'^(?P<pk>.+)/send_denied_vacation_period_email/$',
            'view': DeniedVacationPeriodEmailView,
            'name': 'send_denied_vacation_period_email'
        },
        {
            'regex': r'^(?P<pk>.+)/send_phonecare_vacation_period_email/$',
            'view': PhoneCareVacationPeriodEmailView,
            'name': 'send_phonecare_vacation_period_email'
        },
        {
            'regex': r'^(?P<pk>.+)/send_admin_vacation_period_email/$',
            'view': AdminVacationPeriodEmailView,
            'name': 'send_admin_vacation_period_email'
        }
    ]


@admin.register(Timetable)
class TimetableAdmin(admin.ModelAdmin):
    list_display = ['user', 'year', 'week', 'daypart', 'created_at',
                    'updated_at']
    search_fields = ['user__email', 'user__first_name', 'user__last_name']
    list_filter = ['year', 'week', 'daypart', 'user']
