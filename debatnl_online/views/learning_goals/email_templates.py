from django.contrib.auth.models import User
from django.http import HttpResponseRedirect
from django.urls import reverse_lazy
from django.views.generic import TemplateView, FormView

from backoffice.constants import GROUP_DEBATNLONLINE
from backoffice.mixins import GroupAccessMixin
from debatnl_online.consumers.learning_goals.email_templates import \
    EmailTemplateConsumer
from debatnl_online.forms.learning_goals import EmailTemplateForm
from shared.enums import RoleName


class EmailTemplateBaseView(GroupAccessMixin, FormView):
    form_class = EmailTemplateForm
    base_template = 'debatnl_online/components/learning_goals/{}'
    base_template = base_template.format('email_templates/modals/{}')
    template_name = base_template.format('modal-email-template.html')
    consumer = EmailTemplateConsumer()
    access_groups = [GROUP_DEBATNLONLINE]

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        kwargs = {
            'profile__roles__name': RoleName.ADMIN.value,
            'is_active': True
        }
        context['sender'] = User.objects.filter(kwargs).first()
        return context

    def get_success_url(self):
        self.success_url = reverse_lazy(
            'debatnl_online:learning_goals:organizations:details',
            kwargs={'organization': self.kwargs['organization']}
        )
        return super().get_success_url()

    def get_initial(self):
        self.initial['organization'] = self.kwargs['organization']
        self.initial['type'] = self.request.GET.get('type')
        self.initial['role'] = self.request.GET.get('role', 'PARTICIPANT')
        return super().get_initial()


class EmailTemplateCreate(EmailTemplateBaseView):
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'modal_title': 'E-MAIL TOEVOEGEN',
            'button_text': 'Toevoegen'
        })
        return context

    def form_valid(self, form):
        self.consumer.create_object(form.cleaned_data)
        return super().form_valid(form)


class EmailTemplateUpdate(EmailTemplateBaseView):
    def get_context_data(self, **kwargs):
        self.initial = self.consumer.retrieve_object(self.kwargs['pk'])

        kwargs.update({
            'modal_title': 'E-MAIL WIJZIGEN',
            'button_text': 'Wijzigen'
        })

        return super().get_context_data(**kwargs)

    def form_valid(self, form):
        self.consumer.update_object(form.cleaned_data['id'], form.cleaned_data)
        return super().form_valid(form)


class EmailTemplateDelete(GroupAccessMixin, TemplateView):
    template_name = 'debatnl_online/modals/modal-delete.html'
    access_groups = [GROUP_DEBATNLONLINE]

    def get_context_data(self, **kwargs):
        kwargs.update({
            'modal_title': 'EMAIL',
            'modal_description': 'de e-mail',
        })

        return super().get_context_data(**kwargs)

    def post(self, request, organization, pk):
        consumer = EmailTemplateConsumer()
        consumer.delete_object(pk)

        success_url = reverse_lazy(
            'debatnl_online:learning_goals:organizations:details',
            kwargs={'organization': organization}
        )

        return HttpResponseRedirect(success_url)
