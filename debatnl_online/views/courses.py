from django.contrib.auth.decorators import login_required, user_passes_test
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse, reverse_lazy

from debatnl_online.consumers.courses import CourseConsumer, \
    CourseTestQuestionConsumer
from debatnl_online.permissions import group_check


@login_required
@user_passes_test(group_check, login_url=reverse_lazy('main_page'))
def view_course(request, course_id):
    course_details_consumer = CourseConsumer()
    course = course_details_consumer.retrieve_object(course_id)

    course_test_consumer = CourseTestQuestionConsumer()
    course_test = course_test_consumer.retrieve_all(course_id)

    context = {
        'course': course,
        'course_test': course_test,
    }
    return render(request, 'debatnl_online/course_details.html',
                  context=context)


@login_required
@user_passes_test(group_check, login_url=reverse_lazy('main_page'))
def view_add_course(request):
    if request.method == 'GET':
        return render(request,
                      'debatnl_online/modals/courses/modal-create.html')

    if request.method == 'POST':
        consumer = CourseConsumer()
        consumer.create_object(request.POST)

    return HttpResponseRedirect(reverse('debatnl_online:e_learning:overview'))


@login_required
@user_passes_test(group_check, login_url=reverse_lazy('main_page'))
def view_change_course(request, course_id):
    if request.method == 'GET':
        consumer = CourseConsumer()
        course = consumer.retrieve_object(course_id)

        context = {
            'course': course
        }
        return render(request, 'debatnl_online/modals/courses/modal-edit.html',
                      context=context)

    if request.method == 'POST':
        consumer = CourseConsumer()
        consumer.update_object(course_id, request.POST)

    return HttpResponseRedirect(reverse('debatnl_online:e_learning:overview'))


@login_required
@user_passes_test(group_check, login_url=reverse_lazy('main_page'))
def view_delete_course(request, course_id):
    if request.method == 'GET':
        consumer = CourseConsumer()
        course = consumer.retrieve_object(course_id)

        context = {
            'course': course
        }
        return render(request,
                      'debatnl_online/modals/courses/modal-delete.html',
                      context=context)

    if request.method == 'POST':
        consumer = CourseConsumer()
        payload = {
            "is_deleted": True
        }
        consumer.update_object(course_id, payload)

    return HttpResponseRedirect(reverse('debatnl_online:e_learning:overview'))
