import pendulum

from mock import patch

from django.conf import settings
from django.test import TestCase

from planning.models import PlanningRequest
from planning.tasks import send_reminder_for_pending_option_plans, \
    send_overdue_notification_after_1_day
from planning.tests.factories import PlanningRequestFactory, DeliveryFactory


class PendingOptionPlansReminderTask(TestCase):
    """
    Tests for `send_reminder_for_pending_option_plans`
    task
    """

    def setUp(self):
        self.today = pendulum.parse('2019-08-01', tz=settings.TIME_ZONE)
        self.d1 = [pendulum.MONDAY, pendulum.WEDNESDAY, pendulum.FRIDAY]
        self.d2 = self.d1 + [pendulum.TUESDAY, pendulum.THURSDAY]

    @patch('planning.emails.reminders.PendingPlanningOptionReminderEmail.send')
    def test_pending_option_after_60_days_every_14_days(self, mock_email):
        """
        Ensure task sends reminder email every 14 days if there
        are pending option planning requests due after 60 days.
        """
        planning_request = PlanningRequestFactory(type=PlanningRequest.OPTION)
        DeliveryFactory(
            planning_request=planning_request,
            date=self.today.add(days=100).date()
        )

        # Test dates which result a period of more than 60 days from the
        # due date and their difference from due date is divisible by 14
        td1 = pendulum.parse('2019-08-03', tz=settings.TIME_ZONE)
        td2 = pendulum.parse('2019-08-17', tz=settings.TIME_ZONE)
        td3 = pendulum.parse('2019-08-31', tz=settings.TIME_ZONE)
        test_dates = [td1, td2, td3]

        for dt in test_dates:
            with pendulum.test(dt), self.subTest(dt=dt):
                send_reminder_for_pending_option_plans()
                self.assertTrue(mock_email.called)
                mock_email.reset_mock()

    @patch('planning.emails.reminders.PendingPlanningOptionReminderEmail.send')
    def test_pending_option_after_60_days_every_non_14_days(self, mock_email):
        """
        Ensure task does not sends reminder email on non 14 days if
        there pending option planning requests due after 60 days.
        """
        planning_request = PlanningRequestFactory(type=PlanningRequest.OPTION)
        DeliveryFactory(
            planning_request=planning_request,
            date=self.today.add(days=100).date()
        )

        # Test dates which result a period of more than 60 days from the
        # due date and their difference from due date is not divisible by 14
        td1 = pendulum.parse('2019-08-11', tz=settings.TIME_ZONE)
        td2 = pendulum.parse('2019-08-21', tz=settings.TIME_ZONE)
        td3 = pendulum.parse('2019-09-12', tz=settings.TIME_ZONE)
        test_dates = [td1, td2, td3]

        for dt in test_dates:
            with pendulum.test(dt), self.subTest(dt=dt):
                send_reminder_for_pending_option_plans()
                self.assertFalse(mock_email.called)
                mock_email.reset_mock()

    @patch('planning.emails.reminders.PendingPlanningOptionReminderEmail.send')
    def test_pending_options_from_31_to_60_days_every_week(self, mock_email):
        """
        Ensure task sends reminder emails every 7 days if there are pending
        option planning requests due between 31 and 60 days.
        """
        planning_request = PlanningRequestFactory(type=PlanningRequest.OPTION)
        DeliveryFactory(
            planning_request=planning_request,
            date=self.today.add(days=60).date()
        )

        # Test dates which result a period of between 31 & 60 days from the
        # due date and their difference from due date is divisible by 7
        td1 = pendulum.parse('2019-08-05', tz=settings.TIME_ZONE)
        td2 = pendulum.parse('2019-08-12', tz=settings.TIME_ZONE)
        td3 = pendulum.parse('2019-08-19', tz=settings.TIME_ZONE)
        test_dates = [td1, td2, td3]

        for dt in test_dates:
            with pendulum.test(dt), self.subTest(dt=dt):
                send_reminder_for_pending_option_plans()
                self.assertTrue(mock_email.called)
                mock_email.reset_mock()

    @patch('planning.emails.reminders.PendingPlanningOptionReminderEmail.send')
    def test_pending_options_from_31_to_60_days_every_non_week_days(self, mock_email):
        """
        Ensure task does not sends reminder emails every non 7 days if there
        are pending option planning requests due between 31 and 60 days.
        """
        planning_request = PlanningRequestFactory(type=PlanningRequest.OPTION)
        DeliveryFactory(
            planning_request=planning_request,
            date=self.today.add(days=60).date()
        )

        # Test dates which result a period of between 31 & 60 days from the
        # due date and their difference from due date is not divisible by 7
        td1 = pendulum.parse('2019-08-11', tz=settings.TIME_ZONE)
        td2 = pendulum.parse('2019-08-21', tz=settings.TIME_ZONE)
        td3 = pendulum.parse('2019-09-01', tz=settings.TIME_ZONE)
        test_dates = [td1, td2, td3]

        for dt in test_dates:
            with pendulum.test(dt), self.subTest(dt=dt):
                send_reminder_for_pending_option_plans()
                self.assertFalse(mock_email.called)
                mock_email.reset_mock()

    @patch('planning.emails.reminders.PendingPlanningOptionReminderEmail.send')
    def test_pending_options_from_15_to_60_days_every_few_days(self, mock_email):
        """
        Ensure task sends reminder email every Monday, Wednesday & Friday
        if there are pending option planning request due between 15 & 30 days.
        """
        due_date = pendulum.parse('2019-08-31', tz=settings.TIME_ZONE)
        planning_request = PlanningRequestFactory(type=PlanningRequest.OPTION)
        DeliveryFactory(
            planning_request=planning_request,
            date=due_date.date()
        )

        # Test dates
        td1 = pendulum.parse('2019-08-12', tz=settings.TIME_ZONE)
        td2 = pendulum.parse('2019-08-14', tz=settings.TIME_ZONE)
        td3 = pendulum.parse('2019-08-16', tz=settings.TIME_ZONE)
        test_dates = [td1, td2, td3]

        for dt in test_dates:
            with pendulum.test(dt), self.subTest(dt=dt):
                send_reminder_for_pending_option_plans()
                self.assertTrue(mock_email.called)
                mock_email.reset_mock()

    @patch('planning.emails.reminders.PendingPlanningOptionReminderEmail.send')
    def test_pending_option_from_15_to_60_days_on_remaining_days(self, mock_email):
        """
        Ensure task does not sends reminder email on Tuesday, Thursday,
        Saturday and Sunday if there are pending option planning requests
        due between 15 and 30 days.
        """
        due_date = pendulum.parse('2019-08-31', tz=settings.TIME_ZONE)
        planning_request = PlanningRequestFactory(type=PlanningRequest.OPTION)
        DeliveryFactory(
            planning_request=planning_request,
            date=due_date.date()
        )

        # Test dates
        td1 = pendulum.parse('2019-08-13', tz=settings.TIME_ZONE)
        td2 = pendulum.parse('2019-08-15', tz=settings.TIME_ZONE)
        td3 = pendulum.parse('2019-08-17', tz=settings.TIME_ZONE)
        test_dates = [td1, td2, td3]

        for dt in test_dates:
            with pendulum.test(dt), self.subTest(dt=dt):
                send_reminder_for_pending_option_plans()
                self.assertFalse(mock_email.called)
                mock_email.reset_mock()

    @patch('planning.emails.reminders.PendingPlanningOptionReminderEmail.send')
    def test_pending_options_due_in_15_days_on_weekdays(self, mock_email):
        """
        Ensure task sends reminder email every weekday (i.e. Monday to Friday)
        if there are pending option planning requests due in less than 15 days.
        """
        due_date = pendulum.parse('2019-08-20', tz=settings.TIME_ZONE)
        planning_request = PlanningRequestFactory(type=PlanningRequest.OPTION)
        DeliveryFactory(
            planning_request=planning_request,
            date=due_date.date()
        )

        # Test dates
        td1 = pendulum.parse('2019-08-12', tz=settings.TIME_ZONE)
        td2 = pendulum.parse('2019-08-13', tz=settings.TIME_ZONE)
        td3 = pendulum.parse('2019-08-14', tz=settings.TIME_ZONE)
        td4 = pendulum.parse('2019-08-15', tz=settings.TIME_ZONE)
        td5 = pendulum.parse('2019-08-16', tz=settings.TIME_ZONE)
        test_dates = [td1, td2, td3, td4, td5]

        for dt in test_dates:
            with pendulum.test(dt), self.subTest(dt=dt):
                send_reminder_for_pending_option_plans()
                self.assertTrue(mock_email.called)
                mock_email.reset_mock()

    @patch('planning.emails.reminders.PendingPlanningOptionReminderEmail.send')
    def test_pending_option_due_in_15_days_on_weekends(self, mock_email):
        """
        Ensure task does not sends reminder email on weekends (i.e. Saturday &
        Sunday) if there are pending option planning requests dues in less than
        15 days.
        """
        due_date = pendulum.parse('2019-08-20', tz=settings.TIME_ZONE)
        planning_request = PlanningRequestFactory(type=PlanningRequest.OPTION)
        DeliveryFactory(
            planning_request=planning_request,
            date=due_date.date()
        )

        # Test dates
        td1 = pendulum.parse('2019-08-17', tz=settings.TIME_ZONE)
        td2 = pendulum.parse('2019-08-18', tz=settings.TIME_ZONE)
        test_dates = [td1, td2]

        for dt in test_dates:
            with pendulum.test(dt), self.subTest(dt=dt):
                send_reminder_for_pending_option_plans()
                self.assertFalse(mock_email.called)
                mock_email.reset_mock()


class OverdueNotificationTaskTests(TestCase):
    """
    Tests for `send_overdue_notification_after_1_day` task.
    """

    def setUp(self):
        self.today = pendulum.today(tz=settings.TIME_ZONE)

    @patch('planning.emails.notifications.OverdueNotificationEmail.send')
    def test_with_pending_option_plan_overdue_by_1_day(self, mock_email):
        """
        Ensure overdue notification email is sent when a pending
        option plan is overdue by 1 day.
        """
        planning_request = PlanningRequestFactory(type=PlanningRequest.OPTION)
        DeliveryFactory(
            planning_request=planning_request,
            date=self.today.subtract(days=1).date()
        )

        with pendulum.test(self.today):
            send_overdue_notification_after_1_day()
            self.assertTrue(mock_email.called)

    @patch('planning.emails.notifications.OverdueNotificationEmail.send')
    def test_with_pending_option_plan_overdue_by_more_than_1_day(self, mock_email):
        """
        Ensure overdue notification email is not sent when a pending
        option plan is overdue by more than 1 day.
        """
        planning_request1 = PlanningRequestFactory(type=PlanningRequest.OPTION)
        DeliveryFactory(
            planning_request=planning_request1,
            date=self.today.subtract(days=2).date()
        )

        planning_request2 = PlanningRequestFactory(type=PlanningRequest.OPTION)
        DeliveryFactory(
            planning_request=planning_request2,
            date=self.today.subtract(days=10).date()
        )

        planning_request3 = PlanningRequestFactory(type=PlanningRequest.OPTION)
        DeliveryFactory(
            planning_request=planning_request3,
            date=self.today.subtract(days=100).date()
        )

        with pendulum.test(self.today):
            send_overdue_notification_after_1_day()
            self.assertFalse(mock_email.called)
