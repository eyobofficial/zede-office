# -*- coding: utf-8 -*-
# Generated by Django 1.11.20 on 2019-06-11 10:20
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('webcrm', '0003_auto_20190418_1218'),
        ('handouts', '0003_auto_20190609_1926'),
    ]

    operations = [
        migrations.CreateModel(
            name='HandoutToken',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('delivery', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='webcrm.Delivery')),
            ],
        ),
        migrations.AlterField(
            model_name='handoutrequest',
            name='status',
            field=models.CharField(choices=[('NEW', 'NEW'), ('DNF', 'DELIVERY BY TRAINING DATE NOT FOUND'), ('TNF', 'TRAINER NOT FOUND IN DELIVERY'), ('MMD', 'MISMATCHING DELIVERIES FOUND'), ('HOS', 'HANDOUT SELECTION'), ('SNT', 'SENT')], default='NEW', max_length=3),
        ),
        migrations.AddField(
            model_name='handouttoken',
            name='handout_request',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='handouts.HandoutRequest'),
        ),
    ]
