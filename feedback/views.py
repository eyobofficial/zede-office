import pendulum
from dateutil.relativedelta import relativedelta
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.exceptions import ValidationError
from django.http import JsonResponse
from django.urls import reverse_lazy
from django.utils import timezone
from django.utils.datastructures import OrderedSet
from django.views.generic import ListView
from django.views.generic.base import View, RedirectView, TemplateView

from backoffice.views import BaseEmailView
from feedback.emails import OuttakeIntroEmail, OuttakeReminderEmail
from feedback.models import JotFormTrainer, JotFormTraining, \
    TrainingOuttakeSubscriber
from feedback.utilities.calculations import calculate_average_review_score
from feedback.utilities.training_outtakes import activate_outtake_notifications


class OverviewView(LoginRequiredMixin, ListView):
    template_name = 'feedback/base.html'

    def get_queryset(self):
        cutoff_date = timezone.now() - relativedelta(months=6)
        kwargs = {'jotformtraining__date__gte': cutoff_date}

        if not self.request.user.has_perm('feedback.view_all_trainers'):
            kwargs.update({
                'first_name': self.request.user.first_name,
                'last_name': self.request.user.last_name
            })

        self.queryset = JotFormTrainer.objects.filter(**kwargs).distinct()
        return super().get_queryset()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['years'] = self.get_years()
        return context

    @staticmethod
    def get_years():
        years = OrderedSet()
        for dt in JotFormTraining.objects.dates('date', 'year'):
            if dt.year > 2000:
                years.add(dt.year)

        # This will make sure that the current year is always available,
        # even when there are no reviews in the current year.
        years.add(pendulum.today().year)
        return sorted(years)


class FeedbackView(LoginRequiredMixin, ListView):
    template_name = 'feedback/components/tables-feedback.html'
    model = JotFormTraining

    def get_queryset(self):
        kwargs = {
            'trainer_id': self.request.GET.get('trainer_id'),
            'date__month': self.request.GET.get('month'),
            'date__year': self.request.GET.get('year'),
        }
        self.queryset = JotFormTraining.objects.none()

        if kwargs['trainer_id']:
            self.queryset = JotFormTraining.objects.filter(**kwargs)

        return super().get_queryset()


class TrainerAverageScoreView(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        month = request.GET.get('month')
        year = request.GET.get('year')
        trainer = self.get_trainer()
        calculation = calculate_average_review_score(month, year, trainer)
        return JsonResponse(calculation)

    def get_trainer(self):
        pk = self.request.GET.get('trainer_id')
        try:
            trainer = JotFormTrainer.objects.get(pk=pk)
        except (ValidationError, JotFormTrainer.DoesNotExist):
            trainer = None

        return trainer


class TrainingOuttakeActivation(LoginRequiredMixin, RedirectView):
    url = reverse_lazy('feedback:overview')

    def get(self, request, *args, **kwargs):
        try:
            training_id = kwargs.get('training_id')
            training = JotFormTraining.objects.get(id=training_id)
            activate_outtake_notifications(training)
        except JotFormTraining.DoesNotExist:
            pass

        return super().get(request, *args, **kwargs)


class TrainingOuttakeSubscriptionView(TemplateView):
    model = TrainingOuttakeSubscriber
    template_name = 'feedback/unsubscribed.html'

    def get(self, request, *args, **kwargs):
        try:
            pk = request.GET.get('subscriber')
            subscriber = TrainingOuttakeSubscriber.objects.get(pk=pk)
            subscriber.status = TrainingOuttakeSubscriber.UNSUBSCRIBED
            subscriber.save()

        except (TrainingOuttakeSubscriber.DoesNotExist, ValidationError):
            pass

        return super().get(request, *args, **kwargs)


class BaseOuttakeEmailView(BaseEmailView):
    url = 'admin:feedback_trainingouttakesubscriber_change'
    model = TrainingOuttakeSubscriber


class OuttakeIntroEmailView(BaseOuttakeEmailView):
    email_class = OuttakeIntroEmail
    email_name = 'outtake intro'


class OuttakeReminderEmailView(BaseOuttakeEmailView):
    email_class = OuttakeReminderEmail
    email_name = 'outtake intro'
