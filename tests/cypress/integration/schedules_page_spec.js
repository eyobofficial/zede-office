describe('Schedules App Modal Test', () => {
  beforeEach(() => {
    cy.visit('/login/')
    cy.get('#id_username').type(Cypress.env('username'))
    cy.get('#id_password').type(Cypress.env('password'))
    cy.get('form').submit()
    cy.visit('/schedules')
  })

  it('Visit schedules page', () => {
    // Visit the schedules app landing page
    cy.url().should('include', '/schedules/')
    cy.get('#btn-create-vacation-period').contains('Verlof aanvragen')
    cy.get('#create-vacation-period-modal').should('be', 'invisible')
  })

  it('Click the Verlof aanvragen button', () => {
    // Click `Verlof aanvragen` button to fire a modal for requesting
    // leave of absence
    cy.get('#btn-create-vacation-period').click()
    cy.get('#create-vacation-period-modal')
      .should('be.visible')
    cy.get('#step2-form, #step3-form, #step4-form')
      .should('be', 'invisible')

    cy.get('#step1-form')
      .should('be', 'visible')
      .get('input[name=is_paid]').should('be.not', 'checked')
      .get('#step1-form .step-btn').should('be', 'disabled')
  })

  it('Select `Betaald` radio input on step 1', () => {
    // Step 1 - Ensure when select the `Betaald` radio, step 2 is
    // displayed.
    cy.get('#btn-create-vacation-period').click()
    cy.get('#step1-form label[for=paid-yes]').click()
    cy.get('#step1-form #paid-yes').check({ force: true })
    cy.get('#step1-form .step-btn')
      .should('be.not', 'disabled')
      .click()
    cy.get('#step1-form, #step3-form, #step4-form')
      .should('be', 'invisible')
    cy.get('#step2-form').should('be', 'visible')
    cy.get('input[name=training], input[name=appointement], input[name=meeting]')
      .should('be.not', 'checked')
    cy.get('#step2-form .step-btn').should('be', 'disabled')
  })

  it('Select `Onbetaald` radio input on step 2', () => {
    // Step 1 - Ensure when select the `Onbetaald` radio,
    // step 5 form is displayed.
    cy.get('#btn-create-vacation-period').click()
    cy.get('#step1-form label[for=paid-yes]').click()
    cy.get('#step1-form #paid-no').check({ force: true })
    cy.get('#step1-form .step-btn')
      .should('be.not', 'disabled')
      .click()

    cy.get('#step1-form, #step2-form, #step3-form, #step4-form')
      .should('be', 'invisible')
    cy.get('#step5-form').should('be', 'visible')
    cy.get('#step5-form .step-btn').should('be', 'disabled')

    // Ensure back button in step 5 takes user to step 1
    cy.get('#step5-form .back-btn').click()
    cy.get('#step2-form, #step3-form, #step4-form, #step5-form')
      .should('be', 'invisible')
    cy.get('#step1-form').should('be', 'visible')
  })

  it('Select all `Nee` radio inputs on step 2', () => {
    // Step 2 - Ensure when selecting all `Nee` inputs, step 4 will
    // is displayed.
    cy.get('#btn-create-vacation-period').click()
    cy.get('#step1-form label[for=paid-yes]').click()
    cy.get('#step1-form #paid-yes').check({ force: true })
    cy.get('#step1-form .step-btn').click()
    cy.get('#step2-form .no-inputs').check({ force: true })

    cy.get('#step2-form .step-btn')
      .should('be.not', 'disabled')
      .click()

    cy.get('#step1-form, #step2-form, #step3-form')
      .should('be', 'invisible')

    cy.get('#step4-form').should('be', 'visible')

    cy.get('#step4-form input[name=leave-balance]')
      .should('be.not', 'checked')
    cy.get('#step4-form .step-btn').should('be', 'disabled')
  })

  it('Select `Ja` radio input on step 2', () => {
    // Step 2 - Ensure when selecting a `Ja` input, step 3 is
    // displayed
    cy.get('#btn-create-vacation-period').click()
    cy.get('#step1-form label[for=paid-yes]').click()
    cy.get('#step1-form #paid-yes').check({ force: true })
    cy.get('#step1-form .step-btn').click()
    cy.get('#step2-form .yes-inputs').check({ force: true })
    cy.get('#step2-form .step-btn')
      .should('be.not', 'disabled')
      .click()

    cy.get('#step1-form, #step2-form, #step4-form')
      .should('be', 'invisible')

    cy.get('#step3-form').should('be', 'visible')
  })

  it('Check step 3 of the multistep form', () => {
    // Step 3
    cy.get('#btn-create-vacation-period').click()
    cy.get('#step1-form label[for=paid-yes]').click()
    cy.get('#step1-form #paid-yes').check({ force: true })
    cy.get('#step1-form .step-btn').click()
    cy.get('#step2-form .yes-inputs').check({ force: true })
    cy.get('#step2-form .step-btn').click()

    cy.get('#step3-form .button').contains('Afronden')
      .click()
    cy.get('#create-vacation-period-modal').should('be', 'invisible')
  })

  it('Click `Vorgie` button on step 4', () => {
    // Step 4 - Ensure clicking the `Vorgie` button diplays back
    // step 2 of the multi-form.
    cy.get('#btn-create-vacation-period').click()
    cy.get('#step1-form label[for=paid-yes]').click()
    cy.get('#step1-form #paid-yes').check({ force: true })
    cy.get('#step1-form .step-btn').click()
    cy.get('#step2-form .no-inputs').check({ force: true })
    cy.get('#step2-form .step-btn').click()
    cy.get('#step4-form .back-btn').click()

    cy.get('#step1-form, #step3-form, #step4-form').should('be', 'invisible')
    cy.get('#step2-form').should('be', 'visible')
  })

  it('Click `Ja` radio input on step 4', () => {
    // Step 4 - Ensure selecting `Ja` radio input displays the step 5
    // of the multi-form
    cy.get('#btn-create-vacation-period').click()
    cy.get('#step1-form label[for=paid-yes]').click()
    cy.get('#step1-form #paid-yes').check({ force: true })
    cy.get('#step1-form .step-btn').click()
    cy.get('#step2-form .no-inputs').check({ force: true })
    cy.get('#step2-form .step-btn').click()
    cy.get('#step4-form label[for=leave-balance-yes]').click()
    cy.get('#step4-form .yes-inputs').check({ force: true })
    cy.get('#step4-form .step-btn').click()

    cy.get('#step1-form, #step2-form, #step3-form, #step4-form')
      .should('be', 'invisible')
    cy.get('#step5-form').should('be', 'visible')

    // Ensure back button in step 5 takes user to step 4
    cy.get('#step5-form .back-btn').click()
    cy.get('#step1-form, #step2-form, #step3-form, #step5-form')
      .should('be', 'invisible')
    cy.get('#step4-form').should('be', 'visible')
  })

  it('Click `Nee` radio input on step 4', () => {
    // Step 4 - Ensure selecting `Nee` radio input displays step 7
    cy.get('#btn-create-vacation-period').click()
    cy.get('#step1-form label[for=paid-yes]').click()
    cy.get('#step1-form #paid-yes').check({ force: true })
    cy.get('#step1-form .step-btn').click()
    cy.get('#step2-form .no-inputs').check({ force: true })
    cy.get('#step2-form .step-btn').click()
    cy.get('#step4-form label[for=leave-balance-no]').click()
    cy.get('#step4-form .step-btn').click()
    cy.get('#step4-form').should('be', 'invisible')
    cy.get('#step7-form').should('be', 'visible')

    // Check `Vorgie` button displays step 4 again
    cy.get('#step7-form .back-btn').click()
    cy.get('#step7-form').should('be', 'invisible')
    cy.get('#step4-form').should('be', 'visible')
  })

  it('Click `Nulurencontract` radio input in step 7', () => {
    // Step 7 - Ensure selecting `Nulurencontract` radio input
    // displays step 8.
    cy.get('#btn-create-vacation-period').click()
    cy.get('#step1-form label[for=paid-yes]').click()
    cy.get('#step1-form .step-btn').click()
    cy.get('#step2-form .no-inputs').check({ force: true })
    cy.get('#step2-form .step-btn').click()
    cy.get('#step4-form label[for=leave-balance-no]').click()
    cy.get('#step4-form .step-btn').click()
    cy.get('#step7-form label[for=zero-hour]').click()
    cy.get('#step7-form .step-btn').click()

    // Assertions
    cy.get('#step7-form').should('be', 'invisible')
    cy.get('#step8-form').should('be', 'visible')
    cy.get('#step8-form button[data-close]').click()
    cy.get('#step8-form').should('be', 'invisible')
    cy.get('#create-vacation-period-modal').should('be', 'invisible')
  })

  it('Click `Full-time contract` radio input in step 7', () => {
    // Step 7 - Ensure selecting `Full-time contract` radio input
    // displays step 8
    cy.get('#btn-create-vacation-period').click()
    cy.get('#step1-form label[for=paid-yes]').click()
    cy.get('#step1-form .step-btn').click()
    cy.get('#step2-form .no-inputs').check({ force: true })
    cy.get('#step2-form .step-btn').click()
    cy.get('#step4-form label[for=leave-balance-no').click()
    cy.get('#step4-form .step-btn').click()
    cy.get('#step7-form label[for=zero-hour]').click()
    cy.get('#step7-form .step-btn').click()

    // Assertions
    cy.get('#step7-form').should('be', 'invisible')
    cy.get('#step8-form').should('be', 'visible')
    cy.get('#step8-form button[data-close]').click()
    cy.get('#step8-form').should('be', 'invisible')
    cy.get('#create-vacation-period-modal').should('be', 'invisible')
  })

  it('Click `Min-max contract` radio input in step 7', () => {
    // Step 7 - Ensure selecting `Min-max contract` radio
    // input displays step 9
    cy.get('#btn-create-vacation-period').click()
    cy.get('#step1-form label[for=paid-yes]').click()
    cy.get('#step1-form .step-btn').click()
    cy.get('#step2-form .no-inputs').check({ force: true })
    cy.get('#step2-form .step-btn').click()
    cy.get('#step4-form label[for=leave-balance-no]').click()
    cy.get('#step4-form .step-btn').click()
    cy.get('#step7-form label[for=min-max]').click()
    cy.get('#step7-form .step-btn').click()

    // Assertions
    cy.get('#step7-form').should('be', 'invisible')
    cy.get('#step9-form').should('be', 'visible')
    cy.get('#step9-form .step-btn').should('be', 'disabled')
  })

  it('Request more leave hours than balance in step 9, 10 & 11', () => {
    // Step 9, 10, 11 - Ensure requesting leave hours more than available
    // leave balance displays step 11
    cy.get('#btn-create-vacation-period').click()
    cy.get('#step1-form label[for=paid-yes]').click()
    cy.get('#step1-form .step-btn').click()
    cy.get('#step2-form .no-inputs').check({ force: true })
    cy.get('#step2-form .step-btn').click()
    cy.get('#step4-form label[for=leave-balance-no]').click()
    cy.get('#step4-form .step-btn').click()
    cy.get('#step7-form label[for=min-max]').click()
    cy.get('#step7-form .step-btn').click()
    cy.get('#step9-form #id_hours').type('10')
    cy.get('#step9-form .step-btn').click()

    // Assertions
    cy.get('#step9-form').should('be', 'invisible')
    cy.get('#step10-form').should('be', 'visible')
    cy.get('#step10-form .mark').contains(Math.round(10 * 12 * 0.8, 1))
    cy.get('#step10-form #id_requested_hours')
      .type(Math.round(10 * 12 * 0.8) + 1)
    cy.get('#step10-form .step-btn').click()
    cy.get('#step10-form').should('be', 'invisible')
    cy.get('#step11-form').should('be', 'visible')
    cy.get('#step11-form button[data-close]').click()
    cy.get('#step11-form').should('be', 'invisible')
    cy.get('#create-vacation-period-modal').should('be', 'invisible')
  })

  it('Request less leave hours than balance in step 9, 10 & 5', () => {
    // Step 9, 10, 5 - Ensure requesting leave hours less than
    // available leave balance displays step 5
    cy.get('#btn-create-vacation-period').click()
    cy.get('#step1-form label[for=paid-yes]').click()
    cy.get('#step1-form .step-btn').click()
    cy.get('#step2-form .no-inputs').check({ force: true })
    cy.get('#step2-form .step-btn').click()
    cy.get('#step4-form label[for=leave-balance-no]').click()
    cy.get('#step4-form .step-btn').click()
    cy.get('#step7-form label[for=min-max]').click()
    cy.get('#step7-form .step-btn').click()
    cy.get('#step9-form #id_hours').type('10')
    cy.get('#step9-form .step-btn').click()

    // Assertions
    cy.get('#step9-form').should('be', 'invisible')
    cy.get('#step10-form').should('be', 'visible')
    cy.get('#step10-form .mark').contains(Math.round(10 * 12 * 0.8, 1))
    cy.get('#step10-form #id_requested_hours')
      .type(Math.round(10 * 12 * 0.8) - 1)
    cy.get('#step10-form .step-btn').click()
    cy.get('#step10-form').should('be', 'invisible')
    cy.get('#step5-form').should('be', 'visible')
  })

  it('Submit leave of absence form on step 5', () => {
    // Step 5 - Ensure leave of absence form creates a vacation period
    cy.get('#btn-create-vacation-period').click()
    cy.get('#step1-form label[for=paid-no]').click()
    cy.get('#step1-form #paid-no').check({ force: true })
    cy.get('#step1-form .step-btn').click()

    // Initial state
    cy.get('#step5-form .step-btn').should('be', 'disabled')
    cy.get('.error-message').should('be', 'invisible')

    // Fill form non-date fields
    cy.get('#step5-form #id_amount_hours').type('10')
    cy.get('#step5-form #id_reason').type('Test reason')

    // Add dates where start_date > end_date
    var startDate = new Date();
    var endDate = new Date();
    endDate.setDate(startDate.getDate() - 10)

    // Ensure error message is displayed
    cy.get('#step5-form #id_start_date')
      .type(`${startDate.getDate()}/${startDate.getMonth()}/${startDate.getFullYear()}`, { force: true })
    cy.get('#step5-form #id_end_date')
      .type(`${endDate.getDate()}/${endDate.getMonth()}/${endDate.getFullYear()}`, { force: true })
    cy.get('.error-message').should('be', 'visible')
    cy.get('#step5-form .step-btn').should('be', 'disabled')

    // Add dates where end_date >= start_date
    endDate.setDate(startDate.getDate() + 10)
    cy.get('#step5-form #id_start_date')
      .type(`${startDate.getDate()}/${startDate.getMonth()}/${startDate.getFullYear()}`, { force: true })
    cy.get('#step5-form #id_end_date')
      .type(`${endDate.getDate()}/${endDate.getMonth()}/${endDate.getFullYear()}`, { force: true })
    cy.get('.error-message').should('be', 'invisible')
    cy.get('#step5-form .step-btn')
      .should('be.not', 'disabled')
      .click()

    cy.get('#step1-form, #step2-form, #step3-form, #step4-form, #step5-form')
      .should('be', 'innvisible')
    cy.get('#create-vacation-period-modal button[data-close]').click()
    cy.get('#create-vacation-period-modal').should('be', 'invisible')
  })
})
