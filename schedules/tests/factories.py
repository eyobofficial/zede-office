import factory
import pendulum
from django.conf import settings

from schedules.models import Timetable, VacationPeriod
from users.tests.factories import UserFactory


class TimetableFactory(factory.django.DjangoModelFactory):
    year = pendulum.today(settings.TIME_ZONE).year
    daypart = factory.Iterator([Timetable.MORNING, Timetable.AFTERNOON,
                                Timetable.EVENING])

    class Meta:
        model = Timetable


class VacationPeriodFactory(factory.django.DjangoModelFactory):
    start_date = factory.Faker('date_this_year')
    end_date = factory.Faker('date_this_year')
    amount_hours = 1
    reason = factory.Faker('text')
    user = factory.SubFactory(UserFactory)

    class Meta:
        model = VacationPeriod
