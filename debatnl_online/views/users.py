from django.http import HttpResponseRedirect
from django.urls import reverse_lazy
from django.views.generic import FormView, TemplateView

from backoffice.constants import GROUP_DEBATNLONLINE
from backoffice.mixins import GroupAccessMixin
from debatnl_online.consumers.users import UserConsumer
from debatnl_online.forms.users import UserForm


class UserBaseView(GroupAccessMixin, FormView):
    form_class = UserForm
    template_name = 'debatnl_online/components/users/{}'
    template_name = template_name.format('modal-user.html')
    consumer = UserConsumer()
    success_url = reverse_lazy('debatnl_online:overview')
    access_groups = [GROUP_DEBATNLONLINE]


class UserCreate(UserBaseView):
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'modal_title': 'GEBRUIKER TOEVOEGEN',
            'button_text': 'Toevoegen'
        })
        return context

    def form_valid(self, form):
        self.consumer.create_object(form.cleaned_data)
        return super().form_valid(form)


class UserUpdate(UserBaseView):
    def get_context_data(self, **kwargs):
        self.initial = self.consumer.retrieve_object(self.kwargs['pk'])

        kwargs.update({
            'modal_title': 'GEBRUIKER WIJZIGEN',
            'button_text': 'Wijzigen'
        })

        return super().get_context_data(**kwargs)

    def form_valid(self, form):
        self.consumer.update_object(form.cleaned_data['id'], form.cleaned_data)
        return super().form_valid(form)


class UserDelete(GroupAccessMixin, TemplateView):
    template_name = 'debatnl_online/modals/modal-delete.html'
    access_groups = [GROUP_DEBATNLONLINE]

    def get_context_data(self, **kwargs):
        kwargs.update({
            'modal_title': 'GEBRUIKER',
            'modal_description': 'de gebruiker',
        })

        return super().get_context_data(**kwargs)

    def post(self, request, pk):
        UserConsumer().delete_object(pk)
        return HttpResponseRedirect(reverse_lazy('debatnl_online:overview'))
