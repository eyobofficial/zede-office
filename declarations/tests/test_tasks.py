import pendulum

from mock import patch

from django.conf import settings
from django.test import TestCase

from declarations.models import Declaration
from declarations.tasks import send_declaration_reminder_email_every_month
from .factories import DeclarationFactory


class DeclarationReminderEmailTasks(TestCase):
    """
    Tests for `send_declaration_reminder_email_every_month` task
    """

    @patch('declarations.tasks.DeclarationReminder.send')
    def test_valid_declarations_on_2nd_days(self, mock_send):
        """
        Ensure task sends `DeclarationReminder` every 2nd days of the month
        when the declaration is not submitted.
        """
        kwargs = {'year': 2019, 'month': 12}
        DeclarationFactory(status=Declaration.OPEN, **kwargs)
        DeclarationFactory(status=Declaration.NEED_CHANGES, **kwargs)
        DeclarationFactory(status=Declaration.IN_PROGRESS, **kwargs)

        fake_today = pendulum.parse('2020-01-02', tz=settings.TIME_ZONE)
        with pendulum.test(fake_today):
            send_declaration_reminder_email_every_month()
            self.assertTrue(mock_send.called)
            self.assertEqual(mock_send.call_count, 3)

    @patch('declarations.tasks.DeclarationReminder.send')
    def test_open_declarations_on_non_2nd_days(self, mock_send):
        """
        Ensure task does not sends `DeclarationReminder` on non-second days of
        the month when the declaration is not submitted.
        """
        kwargs = {'year': 2019, 'month': 12}
        DeclarationFactory(status=Declaration.SUBMITTED, **kwargs)

        test_dates = [
            pendulum.parse('2020-01-02', tz=settings.TIME_ZONE),
            pendulum.parse('2020-02-04', tz=settings.TIME_ZONE),
            pendulum.parse('2020-03-30', tz=settings.TIME_ZONE)
        ]

        for dt in test_dates:
            with pendulum.test(dt), self.subTest(dt=dt):
                send_declaration_reminder_email_every_month()
                self.assertFalse(mock_send.called)
                mock_send.reset_mock()
