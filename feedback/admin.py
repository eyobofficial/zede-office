from django.contrib import admin

from backoffice.admin import CustomURLModelAdmin

from feedback.models import JotFormTrainer, JotFormTraining, \
    JotFormTrainingReview, TrainingOuttake, TrainingOuttakeSubscriber, \
    TrainingOuttakeNotification
from feedback.views import OuttakeIntroEmailView, OuttakeReminderEmailView


@admin.register(JotFormTrainer)
class JotFormTrainerAdmin(admin.ModelAdmin):
    list_display = ('first_name', 'last_name',)


@admin.register(JotFormTraining)
class JotFormTrainingAdmin(admin.ModelAdmin):
    list_display = ('date', 'trainer',)
    list_filter = ('trainer',)


@admin.register(JotFormTrainingReview)
class JotFormTrainingReviewAdmin(admin.ModelAdmin):
    list_display = ('training', 'reviewer_name', 'score', 'comment',)


@admin.register(TrainingOuttake)
class TrainingOuttakeAdmin(admin.ModelAdmin):
    list_display = ('training_date', 'trainer_name', 'status',)
    list_filter = ('trainer_name',)


@admin.register(TrainingOuttakeSubscriber)
class TrainingOuttakeSubscriberAdmin(CustomURLModelAdmin):
    list_display = ('email', 'name', 'training_date', 'subscription_date',
                    'status',)
    list_filter = ('subscription_date', 'status')
    custom_urls = [
        {
            'regex': r'^(?P<pk>.+)/send_outtake_intro/$',
            'view': OuttakeIntroEmailView,
            'name': 'send_outtake_intro'
        },
        {
            'regex': r'^(?P<pk>.+)/send_outtake_reminder/$',
            'view': OuttakeReminderEmailView,
            'name': 'send_outtake_reminder'
        }
    ]


@admin.register(TrainingOuttakeNotification)
class TrainingOuttakeNotificationAdmin(admin.ModelAdmin):
    list_display = ('subscriber', 'notification_date', 'notification_type',)
