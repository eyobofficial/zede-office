from django.conf import settings
from django.contrib.auth.models import User
from django.urls import reverse

from debatnl_registration.emails.bases import PlannedTrainingBaseEmail
from debatnl_registration.models import TrainingDate, PlannedTrainingToken, \
    Participant
from shared.enums import RoleName


class BookHallNotificationEmail(PlannedTrainingBaseEmail):
    template_name = 'email_8_book_hall_notification.html'

    def __init__(self, planned_training):
        super().__init__(planned_training)
        training_name = planned_training.training.name
        self.subject = f'Verzoek boeking zaal training {training_name}'
        self.recipient = User.objects.filter(
            profile__roles__name=RoleName.HALL.value,
            is_active=True
        ).first()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        token = PlannedTrainingToken.objects.create(
            type=PlannedTrainingToken.BOOK_HALL,
            planned_training=self.planned_training
        )
        uri = reverse('debatnl_registration:hall_availability')
        base_url = f'{settings.APP_HOSTNAME}{uri}?token={str(token.key)}'
        training_dates = self.planned_training.training_dates.order_by('date')
        context.update({
            'training_dates': training_dates,
            'accept_url': '{}&rsvp=ACCEPT'.format(base_url),
            'deny_url': '{}&rsvp=DENY'.format(base_url),
        })
        return context


class CancelHallNotificationEmail(PlannedTrainingBaseEmail):
    template_name = 'email_15a_cancel_hall_notification.html'

    def __init__(self, planned_training):
        super().__init__(planned_training)
        training_name = planned_training.training.name
        self.subject = f'Annulering zaal training {training_name}'
        self.recipient = User.objects.filter(
            profile__roles__name=RoleName.HALL.value,
            is_active=True
        ).first()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        token = PlannedTrainingToken.objects.create(
            type=PlannedTrainingToken.CANCEL_HALL,
            planned_training=self.planned_training
        )
        training_dates = self.planned_training.training_dates.filter(
            status=TrainingDate.CANCELLED).order_by('sequence')
        uri = reverse('debatnl_registration:hall_cancellation')
        url = f'{settings.APP_HOSTNAME}{uri}?token={str(token.key)}'
        context.update({
            'training_dates': training_dates,
            'button_url': url
        })
        return context


class HallNotAvailableToAdminEmail(PlannedTrainingBaseEmail):
    template_name = 'email_10_hall_not_available_admin.html'

    def __init__(self, planned_training):
        super().__init__(planned_training)
        training_name = planned_training.training.name
        self.subject = f'Zaal voor training {training_name} NIET beschikbaar'
        self.recipient = User.objects.filter(
            profile__roles__name=RoleName.ADMIN.value,
            is_active=True
        ).first()


class HallNotAvailableToTrainersEmail(PlannedTrainingBaseEmail):
    template_name = 'email_11_hall_not_available_trainer.html'

    def __init__(self, planned_training):
        super().__init__(planned_training)
        training_name = planned_training.training.name
        self.subject = f'Annulering training {training_name}'

    def send(self):
        trainers = User.objects.filter(
            profile__roles__name=RoleName.TRAINER.value,
            trainingdate__planned_training=self.planned_training
        ).distinct().all()

        for trainer in trainers:
            self.recipient = trainer
            super().send()


class HallAvailableNotificationEmail(PlannedTrainingBaseEmail):
    template_name = 'email_12_hall_available_notification.html'

    def __init__(self, planned_training):
        super().__init__(planned_training)
        training_name = planned_training.training.name
        self.subject = f'Zaal voor training {training_name}'
        self.recipient = User.objects.filter(
            profile__roles__name=RoleName.ADMIN.value,
            is_active=True
        ).first()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        token = PlannedTrainingToken.objects.create(
            type=PlannedTrainingToken.HALL_AVAILABLE,
            planned_training=self.planned_training
        )
        uri = reverse('debatnl_registration:confirm_jot_form_update')
        url = f'{settings.APP_HOSTNAME}{uri}?token={str(token.key)}'
        context.update({'button_url': url})
        return context


class UpdateWebsiteNotificationEmail(PlannedTrainingBaseEmail):
    template_name = 'email_14a_change_website_notification.html'

    def __init__(self, planned_training):
        super().__init__(planned_training)
        training_name = planned_training.training.name
        self.subject = f'Verzoek tot aanpassing data training {training_name}'
        self.recipient = User.objects.filter(
            profile__roles__name=RoleName.WEBSITE.value,
            is_active=True
        ).first()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        token = PlannedTrainingToken.objects.create(
            type=PlannedTrainingToken.UPDATE_WEBSITE,
            planned_training=self.planned_training
        )
        uri = reverse('debatnl_registration:confirm_website_update')
        url = f'{settings.APP_HOSTNAME}{uri}?token={str(token.key)}'
        context.update({'button_url': url})
        return context


class CancelTrainingToParticipantsEmail(PlannedTrainingBaseEmail):
    def __init__(self, planned_training, exclude_first_date=False):
        super().__init__(planned_training)
        self.subject = self.get_subject(exclude_first_date)
        self.template_name = self.get_template_name(exclude_first_date)

    def get_subject(self, exclude_first_date):
        subject = 'Annulering training'
        if exclude_first_date:
            subject = 'Annulering dag 2 en 3 van training'
        return f'{subject} {self.planned_training.training.name}'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        training_dates = self.planned_training.training_dates.filter(
            status='CANCELLED').order_by('sequence').all()
        token = PlannedTrainingToken.objects.create(
            planned_training=self.planned_training, participant=self.recipient,
            type=PlannedTrainingToken.CANCEL_TRAINING_PARTICIPANT)
        uri = reverse('debatnl_registration:confirm_training_update')
        url = f'{settings.APP_HOSTNAME}{uri}?token={str(token.key)}'
        context.update({
            'button_url': url,
            'training_dates': list(training_dates)
        })
        return context

    @staticmethod
    def get_template_name(exclude_first_date):
        template = 'email_19_cancel_training_3days_to_participant.html'
        if exclude_first_date:
            template = 'email_18a_cancel_training_2days_to_participant.html'
        return template

    def send(self):
        kwargs = {'trainingdate__planned_training': self.planned_training}
        participants = Participant.objects.filter(**kwargs).distinct().all()

        for participant in participants:
            self.recipient = participant
            super().send()


class CancelTrainingToTrainersEmail(PlannedTrainingBaseEmail):
    template_name = 'email_16_17_cancel_training_to_trainer.html'

    def __init__(self, planned_training, exclude_first_date=False):
        super().__init__(planned_training)
        self.subject = self.get_subject(exclude_first_date)
        self.exclude_first_date = exclude_first_date

    def get_subject(self, exclude_first_date):
        subject = 'Annulering training'
        if exclude_first_date:
            subject = 'Annulering dag 2 en 3 van training'
        return f'{subject} {self.planned_training.training.name}'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        training_dates = self.planned_training.training_dates.order_by('date')

        if self.exclude_first_date:
            training_dates = training_dates[1:]

        context.update({
            'training_dates': list(training_dates.all()),
        })
        return context

    def send(self):
        trainers = User.objects.filter(
            profile__roles__name=RoleName.TRAINER.value,
            trainingdate__planned_training=self.planned_training
        ).distinct().all()

        for trainer in trainers:
            self.recipient = trainer
            super().send()
