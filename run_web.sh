#!/bin/bash

# Check if environment variables are set
: "${ENVIRONMENT:? You must set ENVIRONMENT (STAGING, PRODUCTION)}"
: "${DATABASE_URL:? You must set DATABASE_URL}"
: "${SECRET_KEY:? You must set SECRET_KEY}"
: "${ADMIN_NAME:? You must set ADMIN_NAME}"
: "${ADMIN_EMAIL_ADDRESS:? You must set ADMIN_EMAIL_ADDRESS}"
: "${JOT_FORM_API_KEY:? You must set JOT_FORM_API_KEY}"
: "${MANDRILL_API_KEY:? You must set MANDRILL_API_KEY}"

# Install project dependencies
pip install -r requirements.txt

# Run Django migrations
python manage.py migrate --noinput

# Collecting Static Files
python manage.py collectstatic --clear --noinput

# Create default super user
python manage.py create_default_superuser

# Enable cron jobs
source prepare_start.sh test

# Start Gunicorn
gunicorn DebatNL_BackOffice.wsgi:application -w 5 -b 0.0.0.0:8080 --log-file /logs/gunicorn-error.log --log-level error