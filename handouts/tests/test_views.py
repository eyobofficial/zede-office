import datetime
import mock

from django.core.files.uploadedfile import SimpleUploadedFile
from django.test import TestCase
from django.urls import reverse
from post_office.models import Email

from shared.enums import RoleName
from shared.tests.factories import TrainingFactory
from shared.models import Training
from handouts.models import HandoutToken, HandoutRequest
from users.models import Role
from users.tests.factories import UserFactory
from webcrm.factories import DeliveryFactory
from .factories import HandoutRequestFactory, HandoutFactory


class HandoutOverviewTest(TestCase):
    """
    Tests for `HandoutOverviewView`
    """

    def test_request_with_anonymous_user(self):
        handout_request = HandoutRequestFactory()
        url = reverse('handouts:handout-overview', args=[handout_request.pk])
        token = HandoutToken.objects.create(handout_request=handout_request)
        url = f'{url}?token={token.id}'
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(
            response,
            'handouts/public/handout_overview.html'
        )
        self.assertTrue('pages' in response.context)
        self.assertTrue('form' in response.context)


class HandoutCreateSuccessViewTest(TestCase):
    """
    Tests for `HandoutCreateSuccessView`
    """

    def test_request_with_anonymous_user(self):
        url = reverse('handouts:handout-create-success')
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(
            response,
            'handouts/public/create_success.html'
        )


class HandoutUpdateViewTest(TestCase):
    """
    Tests for `HandoutUpdateView`
    """
    fixtures = ['roles']

    def setUp(self):
        self.handout_request = HandoutRequestFactory()
        self.url = reverse(
            'handouts:handout-update',
            args=[self.handout_request.pk]
        )

        role = Role.objects.get(name=RoleName.ADMIN.value)
        user = UserFactory()
        user.profile.roles.add(role)

    def test_request_with_anonymous_user(self):
        """
        Ensure unauthenticated users can access the view.
        """
        token = HandoutToken.objects.create(
            handout_request=self.handout_request
        )
        url = f'{self.url}?token={token.id}'
        response = self.client.get(url)
        template = 'handouts/public/handout_request_update.html'

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, template)
        self.assertTrue('trainers' in response.context)
        self.assertTrue('form' in response.context)

    def test_valid_post_request_with_a_corresponding_delivery(self):
        """
        When submitting a valid POST request with a corresponding
        delivery, ensure that handout request instance is updated
        and no other notification e-mail is sent.
        """
        product = 'Bewust Beinvloeden'
        TrainingFactory(name=product)
        DeliveryFactory(order_date='2019-06-30', custom5='John Doe',
                        product=product)
        form_data = {
            'training_date': datetime.date(2019, 6, 30),
            'trainer': 'John Doe'
        }
        response = self.client.post(self.url, form_data)
        self.handout_request.refresh_from_db()

        self.assertRedirects(response, '/handouts/handout-update-done/')
        self.assertEqual(
            self.handout_request.training_date,
            datetime.date(2019, 6, 30)
        )
        self.assertEqual(self.handout_request.trainer, 'John Doe')
        self.assertEqual(Email.objects.count(), 1)

    def test_valid_post_request_with_no_corresponding_delivery(self):
        """
        When submitting a valid POST request without a corresponding
        delivery, ensure that handout request instance is updated and
        a notification e-mail is sent.
        """
        form_data = {
            'training_date': datetime.date(2019, 6, 30),
            'trainer': 'John Doe'
        }
        response = self.client.post(self.url, form_data)
        self.handout_request.refresh_from_db()

        self.assertRedirects(response, '/handouts/handout-update-done/')
        self.assertEqual(
            self.handout_request.training_date,
            datetime.date(2019, 6, 30)
        )
        self.assertEqual(self.handout_request.trainer, 'John Doe')
        self.assertEqual(Email.objects.count(), 1)


class HandoutUpdateSuccessViewTest(TestCase):
    """
    Tests for `HandoutUpdateSuccessView`
    """

    def test_request_with_anonymous_user(self):
        url = reverse('handouts:handout-update-success')
        response = self.client.get(url)
        template = 'handouts/public/update_success.html'

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, template)


class MismatchedDeliveryUpdateViewTest(TestCase):
    """
    Tests for `MismatchedDeliveryUpdateView`
    """

    def setUp(self):
        product1 = 'ONTSPANNEN_PRESENTEREN'
        product2 = 'OVERTUIGEND_DEBATTERE'
        self.training1 = TrainingFactory(name=product1)
        self.training2 = TrainingFactory(name=product2)

    def test_GET_request_with_valid_token(self):
        delivery = DeliveryFactory()
        mismatch_status = HandoutRequest.MISMATCHING_DELIVERIES
        handout_request = HandoutRequestFactory(status=mismatch_status)
        handout_request.deliveries.add(delivery)
        token = HandoutToken.objects.create(handout_request=handout_request)

        path = reverse('handouts:correct-mismatched-delivery')
        path = f'{path}?token={token.id}'
        response = self.client.get(path)
        template = 'handouts/public/mismatched_delivery_overview.html'
        ctx = response.context
        expected_handouts = HandoutRequest.objects.all()

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, template)
        self.assertEqual(ctx['is_valid_token'], True)
        self.assertQuerysetEqual(
            ctx['trainings'],
            Training.objects.all(),
            lambda x: x
        )
        self.assertEqual(ctx['token'], str(token.id))
        self.assertQuerysetEqual(ctx['handout_requests'], expected_handouts,
                                 transform=lambda x: x)

    def test_GET_request_with_invalid_token(self):
        path = reverse('handouts:correct-mismatched-delivery')
        path = f'{path}?token=123'
        response = self.client.get(path)
        template = 'handouts/public/mismatched_delivery_overview.html'
        ctx = response.context

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, template)
        self.assertEqual(ctx['is_valid_token'], False)
        self.assertQuerysetEqual(
            ctx['trainings'],
            Training.objects.all(),
            transform=lambda x: x
        )
        self.assertEqual(ctx['token'], '123')
        self.assertEqual(ctx['handout_requests'], None)

    @mock.patch('handouts.views.PageSelectionNotification')
    def test_POST_request(self, mock_obj):
        delivery_1 = DeliveryFactory()
        delivery_2 = DeliveryFactory()

        h_request_1 = HandoutRequestFactory()
        h_request_1.deliveries.add(delivery_1)
        h_request_1.deliveries.add(delivery_2)

        h_request_2 = HandoutRequestFactory()
        h_request_2.deliveries.add(delivery_1)

        token = HandoutToken.objects.create(delivery=delivery_1)

        form_data = {
            'token': str(token.id),
            f'handout-{h_request_1.id}': self.training1.pk,
            f'handout-{h_request_2.id}': self.training2.pk,
        }

        template = 'handouts/public/create_success.html'
        path = reverse('handouts:correct-mismatched-delivery')
        response = self.client.post(path, data=form_data, follow=True)
        h_request_1.refresh_from_db()
        h_request_2.refresh_from_db()

        expected_status = HandoutRequest.HANDOUT_SELECTION
        expected_status_op = self.training1
        expected_status_od = self.training2
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, template)
        self.assertEqual(h_request_1.status, expected_status)
        self.assertEqual(h_request_2.status, expected_status)
        self.assertEqual(HandoutToken.objects.count(), 0)
        mock_obj.assert_any_call(h_request_1, training=expected_status_op)
        mock_obj.assert_any_call(h_request_2, training=expected_status_od)


class HandoutDownloadViewTest(TestCase):
    """
    Tests for the `HandoutDownloadView` class
    """

    def test_GET_request(self):
        handout_file = SimpleUploadedFile('handout.pdf', b'content')
        handout = HandoutFactory()
        handout.file = handout_file
        handout.save()
        url = reverse('handouts:handout-download', args=[handout.pk])
        template = 'handouts/public/handout_download.html'
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, template)


class UnknownDeliveryUpdateViewTest(TestCase):
    """
    Tests for `UnknownDeliveryUpdateView`
    """

    def setUp(self):
        self.training = TrainingFactory(name='OVERTUIGEND_DEBATTERE')

    @mock.patch('handouts.views.PageSelectionNotification')
    def test_GET_request_with_valid_token(self, mock_obj):
        delivery = DeliveryFactory()
        unknown_status = HandoutRequest.UNKNOWN_DELIVERIES
        handout_request = HandoutRequestFactory(status=unknown_status)
        handout_request.deliveries.add(delivery)
        token = HandoutToken.objects.create(delivery=delivery)

        path = reverse('handouts:correct-unknown-delivery')
        path = f'{path}?token={token.id}&training={self.training}'
        response = self.client.get(path, follow=True)
        template = 'handouts/public/create_success.html'

        handout_request.refresh_from_db()
        expected_status = HandoutRequest.HANDOUT_SELECTION
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, template)
        self.assertEqual(handout_request.status, expected_status)
        self.assertEqual(HandoutToken.objects.count(), 0)
        mock_obj.assert_called_once_with(
            handout_request, training=self.training)

    @mock.patch('handouts.views.PageSelectionNotification')
    def test_GET_request_with_multiple_hand_requests(self, mock_obj):
        unknown_status = HandoutRequest.UNKNOWN_DELIVERIES
        delivery_1 = DeliveryFactory()
        delivery_2 = DeliveryFactory()

        h_request_1 = HandoutRequestFactory(status=unknown_status)
        h_request_1.deliveries.add(delivery_1)
        h_request_1.deliveries.add(delivery_2)

        h_request_2 = HandoutRequestFactory(status=unknown_status)
        h_request_2.deliveries.add(delivery_1)

        token = HandoutToken.objects.create(delivery=delivery_1)
        path = reverse('handouts:correct-unknown-delivery')
        path = f'{path}?token={token.id}&training={self.training}'
        response = self.client.get(path, follow=True)
        template = 'handouts/public/create_success.html'

        h_request_1.refresh_from_db()
        h_request_2.refresh_from_db()

        expected_status = HandoutRequest.HANDOUT_SELECTION
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, template)
        self.assertEqual(h_request_1.status, expected_status)
        self.assertEqual(h_request_2.status, expected_status)
        self.assertEqual(HandoutToken.objects.count(), 0)
        mock_obj.assert_any_call(h_request_1, training=self.training)
        mock_obj.assert_any_call(h_request_2, training=self.training)

    @mock.patch('handouts.views.PageSelectionNotification')
    def test_GET_request_with_invalid_token(self, mock_obj):
        path = reverse('handouts:correct-unknown-delivery')
        path = f'{path}?token=123&training={self.training}'
        response = self.client.get(path, follow=True)
        template = 'handouts/public/invalid_token.html'
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, template)
        mock_obj.assert_not_called()

    @mock.patch('handouts.views.PageSelectionNotification')
    def test_GET_request_with_invalid_training(self, mock_obj):
        delivery = DeliveryFactory()
        unknown_status = HandoutRequest.UNKNOWN_DELIVERIES
        handout_request = HandoutRequestFactory(status=unknown_status)
        handout_request.deliveries.add(delivery)
        token = HandoutToken.objects.create(delivery=delivery)

        training = 'Unknown Training'
        path = reverse('handouts:correct-unknown-delivery')
        path = f'{path}?token={token.id}&training={training}'
        response = self.client.get(path, follow=True)
        template = 'handouts/public/invalid_token.html'
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, template)
        mock_obj.assert_not_called()

