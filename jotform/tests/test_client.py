import json
import os
from datetime import datetime

import pytz
import requests_mock
from django.test import SimpleTestCase
from requests import HTTPError

from jotform.client.client import JOTFormClient


def setup_mock_object(mock, url, file_name, status_code):
    base_dir = os.path.dirname(__file__)
    test_case_file_name = '{}.json'.format(file_name)
    expectation_file_name = '{}_expectation.json'.format(file_name)

    test_case_location = os.path.join(base_dir, 'mock_data',
                                      test_case_file_name)
    expectation_location = os.path.join(base_dir, 'mock_data',
                                        expectation_file_name)

    with open(test_case_location) as file:
        text = file.read()
        mock.get(url, text=text, status_code=status_code)
        mock.post(url, text=text, status_code=status_code)
        mock.delete(url, text=text, status_code=status_code)

    expectation = None
    if os.path.isfile(expectation_location):
        with open(expectation_location) as file:
            expectation = json.loads(file.read())

    return expectation


class BadRequestTest(SimpleTestCase):
    """
    Test the processing of a bad request to JOTForm API.
    """

    def setUp(self):
        self.client = JOTFormClient(api_key='XYZ123', form_id='1')
        self.scenarios = [
            {'file_name': 'unauthorized', 'status_code': 401},
            {'file_name': 'not_found', 'status_code': 404},
        ]

    @requests_mock.mock()
    def test_bad_requests(self, m):
        """
        Test the response when a request is made with an invalid API key.
        """

        for scenario in self.scenarios:
            url = 'https://api.jotform.com/form/1'

            with self.subTest(status=scenario['status_code']):
                setup_mock_object(m, url, **scenario)

                with self.assertRaises(HTTPError) as cm:
                    self.client.forms.get()

                msg = '{} Client Error'.format(scenario['status_code'])
                self.assertIn(msg, cm.exception.args[0])


class FormTest(SimpleTestCase):
    """
    Test the successful responses of the form endpoints.
    """

    def setUp(self):
        self.client = JOTFormClient(api_key='XYZ123', form_id='1')

    @requests_mock.mock()
    def test_get(self, mock):
        url = 'https://api.jotform.com/form/1'
        setup_mock_object(mock, url, 'form', 200)

        form = self.client.forms.get()

        tz = pytz.utc
        expected_created_at = datetime(2017, 2, 9, 12, 0, 26, tzinfo=tz)
        expected_updated_at = datetime(2017, 9, 21, 16, 42, 32, tzinfo=tz)
        expected_last_submission = datetime(2017, 9, 23, 11, 1, 51, tzinfo=tz)
        expected_url = 'https://form.jotformpro.com/70395163921962'

        self.assertEqual(form.id, '70395163921962')
        self.assertEqual(form.username, 'debatnl')
        self.assertEqual(form.title, 'Feedbackformulier')
        self.assertEqual(form.height, 1447)
        self.assertEqual(form.url, expected_url)
        self.assertEqual(form.status, 'ENABLED')
        self.assertEqual(form.created_at, expected_created_at)
        self.assertEqual(form.updated_at, expected_updated_at)
        self.assertEqual(form.last_submission, expected_last_submission)
        self.assertEqual(form.new, 562)
        self.assertEqual(form.count, 932)


class QuestionsTest(SimpleTestCase):
    """
    Test the successful responses of the form endpoints.
    """

    def setUp(self):
        self.client = JOTFormClient(api_key='XYZ123', form_id='1')

    @requests_mock.mock()
    def test_list(self, mock):
        url = 'https://api.jotform.com/form/1/questions'
        expected_questions = setup_mock_object(mock, url, 'questions', 200)

        questions = self.client.questions.list()

        self.assertEqual(len(questions), 3)
        for index, expected in enumerate(expected_questions):
            with self.subTest(qid=expected['qid']):
                question = questions[index]
                self.assertEqual(question.qid, expected['qid'])
                self.assertEqual(question.name, expected['name'])
                self.assertEqual(question.text, expected['text'])
                self.assertEqual(question.type, expected['type'])
                self.assertEqual(question.order, expected['order'])


@requests_mock.Mocker()
class SubmissionsTest(SimpleTestCase):
    """
    Test the successful responses of the form endpoints.
    """

    def setUp(self):
        self.client = JOTFormClient(api_key='XYZ123', form_id='1')

    def test_list(self, mock):
        url = 'https://api.jotform.com/form/1/submissions'
        expected_submissions = setup_mock_object(mock, url, 'submissions', 200)

        submissions = self.client.submissions.list()

        self.assertEqual(len(submissions), 2)
        for index, expected in enumerate(expected_submissions):
            with self.subTest(id=expected['id']):
                submission = submissions[index]
                created_at = str(submission.created_at)
                updated_at = str(submission.updated_at)

                self.assertEqual(submission.id, expected['id'])
                self.assertEqual(submission.form_id, expected['form_id'])
                self.assertEqual(created_at, str(expected['created_at']))
                self.assertEqual(updated_at, str(expected['updated_at']))
                self.assertEqual(submission.status, expected['status'])
                self.assertEqual(submission.new, expected['new'])
                self.assertListEqual(submission.answers, expected['answers'])

    def test_get(self, mock):
        url = 'https://api.jotform.com/submission/1'
        expected = setup_mock_object(mock, url, 'submission', 200)

        submissions = self.client.submissions.get(1)
        submission = submissions[0]
        created_at = str(submission.created_at)
        updated_at = str(submission.updated_at)

        self.assertEqual(submission.id, expected['id'])
        self.assertEqual(submission.form_id, expected['form_id'])
        self.assertEqual(created_at, str(expected['created_at']))
        self.assertEqual(updated_at, str(expected['updated_at']))
        self.assertEqual(submission.status, expected['status'])
        self.assertEqual(submission.new, expected['new'])
        self.assertListEqual(submission.answers, expected['answers'])


@requests_mock.Mocker()
class WebhooksTest(SimpleTestCase):
    """
    Test the successful responses of the form endpoints.
    """

    def setUp(self):
        self.url = 'https://api.jotform.com/form/1/webhooks'
        self.client = JOTFormClient(api_key='XYZ123', form_id='1')

    def test_list(self, mock):
        expected_webhooks = setup_mock_object(mock, self.url, 'webhooks', 200)
        webhooks = self.client.webhooks.list()
        self.run_assertions(webhooks, expected_webhooks)

    def test_create(self, mock):
        expected_webhooks = setup_mock_object(mock, self.url, 'webhooks', 200)
        data = {'webhookURL': 'http://www.example.com/webhook'}
        webhooks = self.client.webhooks.create(data=data)
        self.run_assertions(webhooks, expected_webhooks)

    def test_delete(self, mock):
        url = '{}/0'.format(self.url)
        expected_webhooks = setup_mock_object(mock, url, 'webhooks', 200)
        webhooks = self.client.webhooks.delete(hook_id=0)
        self.run_assertions(webhooks, expected_webhooks)

    def run_assertions(self, webhooks, expected_webhooks):
        self.assertEqual(len(webhooks), 2)
        for index, expected in enumerate(expected_webhooks):
            with self.subTest(wid=expected['wid']):
                webhook = webhooks[index]
                self.assertEqual(webhook.wid, expected['wid'])
                self.assertEqual(webhook.url, expected['url'])
