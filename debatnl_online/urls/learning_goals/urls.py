from django.conf.urls import url, include
from django.contrib.auth.decorators import login_required

from debatnl_online.views.learning_goals.overview import OverviewView

urlpatterns = [
    url(
        r'^organizations/',
        include(
            'debatnl_online.urls.learning_goals.organizations',
            namespace='organizations'
        )
    ),
    url(
        r'^organizations/(?P<organization>[-\w]+)/goals/',
        include(
            'debatnl_online.urls.learning_goals.goals',
            namespace='goals'
        )
    ),
    url(
        r'^organizations/(?P<organization>[-\w]+)/roles/',
        include(
            'debatnl_online.urls.learning_goals.roles',
            namespace='roles'
        )
    ),
    url(
        r'^organizations/(?P<organization>[-\w]+)/email_templates/',
        include(
            'debatnl_online.urls.learning_goals.email_templates',
            namespace='email_templates'
        )
    ),
    url(r'^$', login_required(OverviewView.as_view()), name='overview'),
]
