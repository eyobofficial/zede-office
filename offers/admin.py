from django.contrib import admin
from ordered_model.admin import OrderedModelAdmin

from backoffice.admin import CustomURLModelAdmin

from backoffice.admin import EmailPreviewAdmin
from offers.models import OfferPhoto, Offer, OfferToken, OfferReview, \
    OfferFile, OfferEmailItem, OfferAbsenceSchedule
from offers.views import CreatedOfferNotificationEmail, \
    CreatedOfferReminderEmail, ChangedOfferNotificationEmail, \
    RemovedOfferNotificationEmail, ReviewOfferNotificationEmail, \
    AcceptedOfferNotificationEmail, DeniedOfferNotificationEmail, \
    MarketingOfferNotificationEmail


@admin.register(Offer)
class OfferAdmin(CustomURLModelAdmin):
    list_display = ('organization', 'delivery_responsible_name', 'status',)
    custom_urls = [
        {
            'regex': r'^(?P<pk>.+)/create-offer-notification/$',
            'view': CreatedOfferNotificationEmail,
            'name': 'send_create_offer_notification'
        },
        {
            'regex': r'^(?P<pk>.+)/changed-offer-notification/$',
            'view': ChangedOfferNotificationEmail,
            'name': 'send_changed_offer_notification'
        },
        {
            'regex': r'^(?P<pk>.+)/removed-offer-notification/$',
            'view': RemovedOfferNotificationEmail,
            'name': 'send_removed_offer_notification'
        },
        {
            'regex': r'^(?P<pk>.+)/review-offer-notification/$',
            'view': ReviewOfferNotificationEmail,
            'name': 'send_review_offer_notification'
        },
        {
            'regex': r'^(?P<pk>.+)/accepted-offer-notification/$',
            'view': AcceptedOfferNotificationEmail,
            'name': 'send_accepted_offer_notification'
        },
        {
            'regex': r'^(?P<pk>.+)/denied-offer-notification/$',
            'view': DeniedOfferNotificationEmail,
            'name': 'send_denied_offer_notification'
        },
        {
            'regex': r'^(?P<pk>.+)/marketing-offer-notification/$',
            'view': MarketingOfferNotificationEmail,
            'name': 'send_marketing_offer_notification'
        }
    ]


@admin.register(OfferPhoto)
class OfferPhotoAdmin(OrderedModelAdmin):
    list_display = ('name', 'image', 'order', 'move_up_down_links',)
    ordering = ('order',)


@admin.register(OfferFile)
class OfferFileAdmin(admin.ModelAdmin):
    list_display = ('offer', 'file', 'is_denied', 'updated_at',)


@admin.register(OfferToken)
class OfferTokenAdmin(CustomURLModelAdmin):
    list_display = ('email', 'offer', 'type', 'created_at',)
    custom_urls = [
        {
            'regex': r'^(?P<pk>.+)/create-offer-reminder/$',
            'view': CreatedOfferReminderEmail,
            'name': 'send_create_offer_reminder'
        },
        {
            'regex': r'^(?P<pk>.+)/review-offer-reminder/$',
            'view': ReviewOfferNotificationEmail,
            'name': 'send_review_offer_reminder'
        }
    ]


@admin.register(OfferReview)
class OfferReviewAdmin(admin.ModelAdmin):
    list_display = ('offer', 'review',)


@admin.register(OfferEmailItem)
class OfferEmailAdmin(EmailPreviewAdmin):
    list_filter = ('status', 'offer',)


@admin.register(OfferAbsenceSchedule)
class OfferAbsenceScheduleAdmin(admin.ModelAdmin):
    list_display = ('start_date', 'end_date', 'employee')
    list_filter = ('employee', )
    search_fields = ('employee__first_name', 'employee__last_name',
                     'employee__email')
