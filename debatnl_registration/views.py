import pendulum
from django.conf import settings
from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib.auth.models import User
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse_lazy
from django.utils import timezone
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import DeleteView, TemplateView, ListView, CreateView
from extra_views import InlineFormSetFactory, NamedFormsetsMixin, \
    CreateWithInlinesView

from backoffice.constants import GROUP_DEBATNLREGISTRATION
from backoffice.mixins import GroupAccessMixin
from backoffice.models import Training
from backoffice.views import BaseEmailView
from debatnl_registration.emails.attendance.notifications import \
    ExtendInvitationNotificationEmail, CertificateEmail
from debatnl_registration.emails.participant.notifications import \
    ReviewTrainingNotificationEmail, RegistrationConfirmationEmail, \
    TrainingContinuationNotificationEmail, ParticipantCancellationChoiceEmail
from debatnl_registration.emails.planned_training.notifications import \
    CancelTrainingToTrainersEmail, CancelTrainingToParticipantsEmail, \
    BookHallNotificationEmail, CancelHallNotificationEmail, \
    HallNotAvailableToAdminEmail, HallNotAvailableToTrainersEmail, \
    HallAvailableNotificationEmail, UpdateWebsiteNotificationEmail
from debatnl_registration.emails.reminders import \
    TravelDirectionReminderEmail, BookHallReminderEmail, \
    CancelHallReminderEmail, HallAvailableReminderEmail, \
    UpdateWebsiteReminderEmail, AttendeesListReminderEmail, \
    AttendeesAmountReminderEmail
from debatnl_registration.emails.training_date.notifications import \
    TrainingConfirmationHallEmail, AttendeesListNotificationEmail, \
    TrainingInformationNotificationEmail, AttendeesAmountNotificationEmail
from debatnl_registration.forms import TrainingDateModelForm, \
    PlannedTrainingModelForm
from debatnl_registration.models import PlannedTrainingToken, TrainingDate, \
    Participant, Attendance, PlannedTraining, BloomvilleFeedItem, \
    SpringestFeedItem
from debatnl_registration.tasks import add_training_dates_to_gravity_form, \
    add_2_day_additional_training_dates_to_gravity_form, \
    remove_2_day_additional_training_dates_from_gravity_form, \
    remove_training_dates_from_gravity_form
from debatnl_registration.utilities.dispatcher import gravity_form_dispatcher
from debatnl_registration.utilities.helpers import persist_planned_training, \
    update_training_date_trainers, change_attendance, delete_attendance
from debatnl_registration.utilities.parsers import \
    parse_planned_training_data, \
    parse_training_update
from shared.enums import RoleName
from users.models import UserProfile


def group_check(user):
    try:
        profile = user.profile
        kwargs = {'app_name__in': [GROUP_DEBATNLREGISTRATION]}
        return profile.app_permissions.filter(**kwargs).exists()

    except (UserProfile.DoesNotExist, AttributeError):
        return user.is_superuser


@login_required
@user_passes_test(group_check, login_url=reverse_lazy('main_page'))
def overview(request):
    year = timezone.now().year

    context = {
        'trainings': Training.objects.order_by('name').all(),
        'date_count': range(3),
        'days': range(1, 32),
        'months': range(1, 13),
        'years': range(year, year + 5),
        'trainers': User.objects.filter(
            profile__roles__name=RoleName.TRAINER.value,
            is_active=True
        ).order_by('first_name', 'last_name').all(),
        'training_dates': TrainingDate.objects.filter(
            status='PENDING'
        ).order_by('date').all()
    }

    return render(request, 'debatnl_registration/base.html', context)


class TrainingDateInline(InlineFormSetFactory):
    model = TrainingDate
    form_class = TrainingDateModelForm
    factory_kwargs = {'min_num': 1, 'max_num': 3, 'extra': 2}


class PlannedTrainingCreateView(GroupAccessMixin, NamedFormsetsMixin,
                                CreateWithInlinesView):
    model = PlannedTraining
    success_url = reverse_lazy('debatnl_registration:overview')
    access_groups = [GROUP_DEBATNLREGISTRATION]
    form_class = PlannedTrainingModelForm
    inlines = [TrainingDateInline]
    inlines_names = ['training_date_formset']

    def forms_valid(self, form, inlines):
        response = super().forms_valid(form, inlines)
        training_dates = self.object.training_dates.order_by('date')
        for index, training_date in enumerate(training_dates):
            training_date.sequence = index
            training_date.save()

        kwargs = {
            'profile__roles__name': RoleName.HALL.value,
            'is_active': True
        }
        if User.objects.filter(**kwargs).exists():
            BookHallNotificationEmail(self.object).send()

        return response


@login_required
@user_passes_test(group_check, login_url=reverse_lazy('main_page'))
def add_trainers(request, pk):
    """
    A view to add trainers to an ongoing training.

    :param request: (Request) A HTTP Request.
    :param pk: (Int) PK of a training_date object.
    :return: (HttpResponse)
    """

    if request.method == 'POST':
        training_date = get_object_or_404(TrainingDate, id=pk)
        update_training_date_trainers(training_date, request.POST)

    return HttpResponse(status=200)


class TrainingDateDelete(GroupAccessMixin, DeleteView):
    model = TrainingDate
    success_url = reverse_lazy('debatnl_registration:overview')
    base_path = 'debatnl_registration/components/modals'
    template_name = f'{base_path}/modal-cancel-training-date.html'
    planned_training = None
    access_groups = [GROUP_DEBATNLREGISTRATION]
    object = None

    def delete(self, request, *args, **kwargs):
        self.object = self.get_object()
        self.planned_training = self.object.planned_training

        if self.object.sequence == 0:
            self.cancel_all_training_dates()
        else:
            self.object.cancel()

        success_url = self.get_success_url()
        return HttpResponseRedirect(success_url)

    def cancel_all_training_dates(self):
        for training_date in self.planned_training.training_dates.all():
            training_date.cancel()

        # Send notifications
        CancelHallNotificationEmail(self.planned_training).send()
        CancelTrainingToParticipantsEmail(self.planned_training).send()
        CancelTrainingToTrainersEmail(self.planned_training).send()

        # Remove dates from the Gravity forms on Debat.NL
        self.remove_training_dates_from_gravity_forms()

    def remove_training_dates_from_gravity_forms(self):
        t_id = self.planned_training.id
        remove_training_dates_from_gravity_form.delay(t_id)
        remove_2_day_additional_training_dates_from_gravity_form.delay(t_id)


class AvailableTrainingDatesListView(GroupAccessMixin, ListView):
    model = TrainingDate
    access_groups = [GROUP_DEBATNLREGISTRATION]
    template_name = 'debatnl_registration/components/widgets/' \
                    'training_dates_selector.html'

    def get_queryset(self):
        training_pk = self.kwargs.get('pk')
        if training_pk:
            qs = super().get_queryset()
            today = pendulum.today(tz=settings.TIME_ZONE).date()
            training = Training.objects.get(pk=training_pk)
            kwargs = {
                'date__gt': today,
                'planned_training__training': training,
                'status': TrainingDate.PENDING
            }
            return qs.filter(**kwargs).order_by('date')
        return TrainingDate.objects.none()


@login_required
@user_passes_test(group_check, login_url=reverse_lazy('main_page'))
def cancel_attendance(request, pk, attendee_id):
    if request.method == 'POST':
        try:
            attendance = Attendance.objects.get(
                training_date_id=pk,
                participant_id=attendee_id
            )
            attendance.delete()

            # Delete the participant if it is not attending any training dates.
            if Attendance.objects.filter(
                participant_id=attendee_id
            ).count() == 0:
                participant = Participant.objects.get(id=attendee_id)
                participant.delete()
        except (Attendance.DoesNotExist, Participant.DoesNotExist):
            pass

    return redirect('debatnl_registration:overview')


@login_required
@user_passes_test(group_check, login_url=reverse_lazy('main_page'))
def change_attendee_training(request, pk, attendee_id):
    if request.method == 'POST':

        try:
            new_training_date_id = request.POST.get('training_date')
            new_training_date = TrainingDate.objects.get(
                id=new_training_date_id
            )

            attendance = Attendance.objects.get(
                training_date_id=pk,
                participant_id=attendee_id
            )
            attendance.training_date = new_training_date
            attendance.save()

        except (Attendance.DoesNotExist, TrainingDate.DoesNotExist):
            pass

    return redirect('debatnl_registration:overview')


def hall_availability(request):
    rsvp = request.GET.get('rsvp')
    token = request.GET.get('token')

    try:
        # Verify if token exists.
        token = PlannedTrainingToken.objects.get(key=token, type='BOOK HALL')

        # Update hall status based on the choice from hall contact.
        planned_training = token.planned_training
        planned_training.hall_status = 'AVAILABLE' if rsvp == 'ACCEPT' \
            else 'NOT AVAILABLE'
        planned_training.save()

        # Delete token.
        token.delete()

        # Send e-mails based on the hall status.
        if planned_training.hall_status == 'AVAILABLE':
            HallAvailableNotificationEmail(planned_training).send()
            UpdateWebsiteNotificationEmail(planned_training).send()

            add_training_dates_to_gravity_form.delay(planned_training.id)
            add_2_day_additional_training_dates_to_gravity_form.delay(
                planned_training.id
            )
        else:
            HallNotAvailableToTrainersEmail(planned_training).send()
            HallNotAvailableToAdminEmail(planned_training).send()

            # Delete planned training.
            planned_training.delete()

        context = {
            'messages': [
                'Bedankt voor uw keuze.',
                'We zullen deze intern verwerken.'
            ]
        }

        template = 'debatnl_registration/public/confirmation.html'
        return render(request, template, context)

    except PlannedTrainingToken.DoesNotExist:
        pass

    return render(request, 'debatnl_registration/public/expired_token.html')


def hall_cancellation(request):
    token = request.GET.get('token')

    try:
        token = PlannedTrainingToken.objects.get(
            key=token,
            type='CANCEL HALL'
        )
        token.delete()

        context = {
            'messages': [
                'Bedank voor het bevestigen van de annulering.'
            ]
        }

        template = 'debatnl_registration/public/confirmation.html'
        return render(request, template, context)

    except PlannedTrainingToken.DoesNotExist:
        pass

    return render(request, 'debatnl_registration/public/expired_token.html')


def confirm_jot_form_update(request):
    token = request.GET.get('token')

    try:
        token = PlannedTrainingToken.objects.get(key=token)
        token.delete()

        context = {'messages': ['Dank voor het updaten!']}
        template = 'debatnl_registration/public/confirmation.html'
        return render(request, template, context)

    except PlannedTrainingToken.DoesNotExist:
        pass

    return render(request, 'debatnl_registration/public/expired_token.html')


def confirm_website_update(request):
    token = request.GET.get('token')

    try:
        token = PlannedTrainingToken.objects.get(key=token)
        token.delete()

        context = {'messages': ['Dank voor het updaten van de data!']}
        template = 'debatnl_registration/public/confirmation.html'
        return render(request, template, context)

    except PlannedTrainingToken.DoesNotExist:
        pass

    return render(request, 'debatnl_registration/public/expired_token.html')


def confirm_training_update(request):
    token = request.GET.get('token')

    try:
        year = timezone.now().year
        token = PlannedTrainingToken.objects.get(
            key=token,
            type='CANCEL TRAINING PARTICIPANT'
        )

        exclude_first_day = not token.planned_training.training_dates.filter(
            status='PENDING'
        ).exists()

        planned_trainings = PlannedTraining.objects.filter(
            training=token.planned_training.training,
            hall_status='AVAILABLE',
            training_dates__status='PENDING'
        ).exclude(id=token.planned_training_id).distinct().all()

        context = {
            'token': token,
            'exclude_first_option': exclude_first_day,
            'planned_trainings': planned_trainings,
            'participant': token.participant,
            'days': range(1, 32),
            'months': range(1, 13),
            'years': range(year, year + 5),
        }

        if request.method == 'POST':
            data = parse_training_update(request.POST)

            if data.get('option') == '1':
                change_attendance(
                    token,
                    data,
                    exclude_first_day=exclude_first_day
                )
                RegistrationConfirmationEmail(token.participant).send()

            if data.get('option') == '2':
                change_attendance(
                    token,
                    data,
                    exclude_first_day=exclude_first_day
                )
                RegistrationConfirmationEmail(token.participant).send()

            if data.get('option') == '3':
                delete_attendance(token)

            ParticipantCancellationChoiceEmail(token, data).send()

            context['messages'] = ['Bedankt voor het maken van uw keuze!']
            token.delete()

            template = 'debatnl_registration/public/confirmation.html'
            return render(request, template, context)

        template = 'debatnl_registration/public/change_training.html'
        return render(request, template, context)

    except PlannedTrainingToken.DoesNotExist:
        pass

    return render(request, 'debatnl_registration/public/expired_token.html')


def confirm_attendees_amount(request):
    token = request.GET.get('token')

    try:
        token = PlannedTrainingToken.objects.get(
            key=token,
            type='ATTENDEES AMOUNT'
        )
        token.delete()

        context = {'messages': ['Dank voor het doorgeven!']}
        template = 'debatnl_registration/public/confirmation.html'
        return render(request, template, context)

    except PlannedTrainingToken.DoesNotExist:
        pass

    return render(request, 'debatnl_registration/public/expired_token.html')


def confirm_attendance_list(request):
    token = request.GET.get('token')

    try:
        token = PlannedTrainingToken.objects.get(
            key=token,
            type='ATTENDANCE LIST'
        )
        attendance_list = token.training_date.attendance_set.all()
        context = {
            'token': str(token.key),
            'attendance_list': attendance_list
        }

        if request.method == 'POST':
            for attendance in attendance_list:
                key = '{}-has_attended'.format(attendance.id)
                attendance.has_attended = request.POST.get(key) == 'true'
                attendance.save()

            context['messages'] = ['Dank voor het doorgeven!']
            token.delete()

            template = 'debatnl_registration/public/confirmation.html'
            return render(request, template, context)

        template = 'debatnl_registration/public/attendance_list.html'
        return render(request, template, context)

    except PlannedTrainingToken.DoesNotExist:
        pass

    return render(request, 'debatnl_registration/public/expired_token.html')


@csrf_exempt
def hooks(request):
    gravity_form_dispatcher(request.POST)
    return HttpResponse(status=200)


class SpringestFeedView(TemplateView):
    template_name = 'debatnl_registration/feeds/springest/courses.xml'
    content_type = 'text/xml'

    def get_context_data(self, **kwargs):
        qs_kwargs = {'profile__roles__name': RoleName.ADMIN.value}
        kwargs['feed_items'] = SpringestFeedItem.objects.all()
        kwargs['contact_person'] = User.objects.filter(**qs_kwargs).first()
        return super().get_context_data(**kwargs)


class BloomvilleFeedView(TemplateView):
    template_name = 'debatnl_registration/feeds/bloomville/courses_v1.xml'
    content_type = 'text/xml'

    def get_context_data(self, **kwargs):
        kwargs['feed_items'] = BloomvilleFeedItem.objects.all()
        return super().get_context_data(**kwargs)


def feed_bloomville_courses(request):
    """
    NOTE: THIS VIEW WILL BECOME DEPRECATED!
    """
    context = {
        'feed_items': BloomvilleFeedItem.objects.all()
    }

    template = 'debatnl_registration/feeds/bloomville/courses.xml'
    return render(request, template, context=context, content_type='text/xml')


def feed_bloomville_classes(request):
    """
    NOTE: THIS VIEW WILL BECOME DEPRECATED!
    """
    context = {
        'training_dates': TrainingDate.objects.filter(
            planned_training__hall_status='AVAILABLE'
        ).exclude(
            status='CANCELLED'
        ).all()
    }

    template = 'debatnl_registration/feeds/bloomville/classes.xml'
    return render(request, template, context=context, content_type='text/xml')


class BaseParticipantEmailView(BaseEmailView):
    url = 'admin:debatnl_registration_participant_change'
    model = Participant


class BaseAttendanceEmailView(BaseEmailView):
    url = 'admin:debatnl_registration_attendance_change'
    model = Attendance


class BasePlannedTrainingEmailView(BaseEmailView):
    url = 'admin:debatnl_registration_plannedtraining_change'
    model = PlannedTraining


class BasePlannedTrainingTokenEmailView(BaseEmailView):
    url = 'admin:debatnl_registration_plannedtrainingtoken_change'
    model = PlannedTrainingToken


class BaseTrainingDateEmailView(BaseEmailView):
    url = 'admin:debatnl_registration_trainingdate_change'
    model = TrainingDate


class RegistrationConfirmationEmailView(BaseParticipantEmailView):
    email_class = RegistrationConfirmationEmail


class TravelDirectionReminderEmailView(BaseParticipantEmailView):
    email_class = TravelDirectionReminderEmail


class ReviewTrainingNotificationEmailView(BaseParticipantEmailView):
    email_class = ReviewTrainingNotificationEmail


class TrainingContinuationNotificationEmailView(BaseParticipantEmailView):
    email_class = TrainingContinuationNotificationEmail


class ExtendInvitationNotificationEmailView(BaseAttendanceEmailView):
    email_class = ExtendInvitationNotificationEmail


class CertificateEmailView(BaseAttendanceEmailView):
    email_class = CertificateEmail


class CancelTrainingToTrainersEmailView(BasePlannedTrainingEmailView):
    email_class = CancelTrainingToTrainersEmail


class CancelTrainingToParticipantEmailView(BasePlannedTrainingEmailView):
    email_class = CancelTrainingToParticipantsEmail


class BookHallNotificationEmailView(BasePlannedTrainingEmailView):
    email_class = BookHallNotificationEmail


class CancelHallNotificationEmailView(BasePlannedTrainingEmailView):
    email_class = CancelHallNotificationEmail


class HallNotAvailableToAdminEmailView(BasePlannedTrainingEmailView):
    email_class = HallNotAvailableToAdminEmail


class HallNotAvailableToTrainersEmailView(BasePlannedTrainingEmailView):
    email_class = HallNotAvailableToTrainersEmail


class HallAvailableNotificationEmailView(BasePlannedTrainingEmailView):
    email_class = HallAvailableNotificationEmail


class UpdateWebsiteNotificationEmailView(BasePlannedTrainingEmailView):
    email_class = UpdateWebsiteNotificationEmail


class BookHallReminderEmailView(BasePlannedTrainingTokenEmailView):
    email_class = BookHallReminderEmail


class CancelHallReminderEmailView(BasePlannedTrainingTokenEmailView):
    email_class = CancelHallReminderEmail


class HallAvailableReminderEmailView(BasePlannedTrainingTokenEmailView):
    email_class = HallAvailableReminderEmail


class UpdateWebsiteReminderEmailView(BasePlannedTrainingTokenEmailView):
    email_class = UpdateWebsiteReminderEmail


class AttendeesListReminderEmailView(BasePlannedTrainingTokenEmailView):
    email_class = AttendeesListReminderEmail


class AttendeesAmountReminderEmailView(BasePlannedTrainingTokenEmailView):
    email_class = AttendeesAmountReminderEmail


class TrainingConfirmationHallEmailView(BaseTrainingDateEmailView):
    email_class = TrainingConfirmationHallEmail


class AttendeesListNotificationEmailView(BaseTrainingDateEmailView):
    email_class = AttendeesListNotificationEmail


class AttendeesAmountNotificationEmailView(BaseTrainingDateEmailView):
    email_class = AttendeesAmountNotificationEmail


class TrainingInformationNotificationEmailView(BaseTrainingDateEmailView):
    email_class = TrainingInformationNotificationEmail
