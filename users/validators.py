from django.core.exceptions import ValidationError
from django.utils.translation import gettext as _


class CapitalLetterValidator:
    def validate(self, password, user=None):
        if not any(char.isupper() for char in password):
            msg = _('Wachtwoord moet minimaal 1 hoofdletter bevatten.')
            raise ValidationError(msg)

    def get_help_text(self):
        return _("Wachtwoord moet minimaal 1 hoofdletter bevatten.")
