from contextlib import contextmanager

from django.dispatch import Signal


@contextmanager
def disable_signal(signal, sender, receiver):
    Signal.disconnect(signal, sender=sender, receiver=receiver)
    yield
    Signal.connect(signal, sender=sender, receiver=receiver)
