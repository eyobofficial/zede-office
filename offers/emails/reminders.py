from backoffice.utilities.emails import ReminderEmailMixin
from offers.emails.bases import OfferEmail, ReviewOfferEmail


class CreatedOfferReminder(ReminderEmailMixin, OfferEmail):
    def __init__(self, token):
        super().__init__(token.offer)
        self.template_name = 'email_5_new_offer_reminder.html'
        self.subject = 'Reminder offerte {}'.format(token.offer.organization)
        self.token = token


class ReviewOfferReminder(ReminderEmailMixin, ReviewOfferEmail):
    def __init__(self, token):
        super().__init__(token.offer)
        self.template_name = 'email_2a_review_offer_reminder.html'
        self.subject = 'Bekijk offerte {}'.format(token.offer.organization)
        self.token = token
