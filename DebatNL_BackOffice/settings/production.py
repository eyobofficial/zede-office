from .base import *

DEBUG = False

SECRET_KEY = env('SECRET_KEY')

INSTALLED_APPS += []

# Cache Configurations

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
    }
}

# Database

DATABASES['default']['OPTIONS'] = {'charset': 'utf8mb4'}

# Force HTTPS redirection

SECURE_SSL_REDIRECT = True

# Force Sub-path Setting

FORCE_SCRIPT_NAME = '/olaf/'

# Login Redirects

LOGIN_URL = '/olaf/login/'
LOGIN_REDIRECT_URL = '/olaf/'
LOGOUT_REDIRECT_URL = '/olaf/login/'

# Static Settings

STATIC_ROOT = '/home/debatnl/webapps/debatnl_backoffice_static/'
STATIC_URL = '/olaf/static/'

# Media Settings

MEDIA_ROOT = '/home/debatnl/webapps/debatnl_backoffice_media/'
MEDIA_URL = 'https://www.debat.nl/olaf/media/'

# Host Settings

APP_HOSTNAME = 'https://www.debat.nl'
DEBATNL_ONLINE_API = 'https://api.debatonline.nl'

# E-mail Settings

ALLOW_EMAIL_SENDING = True

# Frontend Settings

FRONTEND_URL = 'https://www.debat.nl/online'

# Logging Configurations

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'file': {
            'level': env('DJANGO_LOG_LEVEL', default='ERROR'),
            'class': 'logging.FileHandler',
            'filename': os.path.join(
                BASE_DIR, 'logs', 'django-debug.log'
            )
        },
        'sentry': {
            'level': 'ERROR',
            'class': 'raven.contrib.django.raven_compat.'
                     'handlers.SentryHandler',
        },
    },
    'loggers': {
        'django': {
            'handlers': ['file', 'sentry'],
            'level': env('DJANGO_LOG_LEVEL', default='ERROR'),
            'propagate': True,
        },
    },
}

# SASS Processor Configurations

SASS_PROCESSOR_INCLUDE_DIRS = [
    os.path.join(os.sep, 'home', 'debatnl', 'webapps',
                 'debatnl_backoffice_static'),
]

SASS_PROCESSOR_ENABLED = True

# Debat.NL Online Settings

DEBATNL_ONLINE_API_KEY = env('DEBATNL_ONLINE_API_KEY')
# This is for the readonly access to a participant dashboard.
DEBATNL_ONLINE_ADMIN_KEY = env('DEBATNL_ONLINE_ADMIN_KEY', default='')


# Gravity Form API Settings

ALLOW_GRAVITY_FORM_REQUESTS = True
GRAVITY_FORM_ENDPOINT = env('GRAVITY_FORM_ENDPOINT')
GRAVITY_FORM_PUBLIC_KEY = env('GRAVITY_FORM_PUBLIC_KEY')
GRAVITY_FORM_PRIVATE_KEY = env('GRAVITY_FORM_PRIVATE_KEY')

# JotForm API

JOT_FORM_API_KEY = env('JOT_FORM_API_KEY')
JOT_FORM_AWS_QUEUE = 'DebatNLBackoffice-JOTForm-Production'
JOT_FORM_AWS_SUBMISSION_WEBHOOK = env('JOT_FORM_AWS_SUBMISSION_WEBHOOK')

# Mandrill Settings

MANDRILL_API_KEY = env('MANDRILL_API_KEY')

# AWS S3 Configurations

AWS_ACCESS_KEY = env('AWS_ACCESS_KEY')
AWS_SECRET_KEY = env('AWS_SECRET_KEY')
AWS_S3_BUCKET = 'debatnl-online'
AWS_S3_BUCKET_SUB_PATH = 'production'

# Vimeo API Settings

VIMEO_ACCESS_TOKEN = env('VIMEO_ACCESS_TOKEN')
VIMEO_PRESET_ID = 120402792
VIMEO_ALLOWED_DOMAIN = env('VIMEO_ALLOWED_DOMAIN')

# MS Graph/Sharepoint Settings

MS_GRAPH_CLIENT_ID = env('MS_GRAPH_CLIENT_ID')
MS_GRAPH_CLIENT_SECRET = env('MS_GRAPH_CLIENT_SECRET')
