from django.apps import AppConfig


class DebatnlRegistrationConfig(AppConfig):
    name = 'debatnl_registration'

    def ready(self):
        import debatnl_registration.signals # noqa
