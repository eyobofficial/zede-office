import pendulum
from django.conf import settings

from DebatNL_BackOffice.celery import app
from users.emails.notifications import NoHubstaffAccountNotificationEmail
from users.models import UserProfile


@app.task
def send_no_hubstaff_account_email_every_10_days():
    """
    Send `NoHubstaffAccountEmail` every 10 days
    """
    tz = settings.TIME_ZONE
    today = pendulum.today(tz=tz)
    kwargs = {
        'user__is_active': True,
        'roles__name__iexact': 'Trainer',
        'hubstaff_user_id__isnull': True,
        'do_hubstaff_check': True
    }

    for profile in UserProfile.objects.filter(**kwargs):
        date_joined = pendulum.instance(profile.user.date_joined).in_tz(tz)
        diff = today.diff(date_joined).in_days()

        if diff % 10 == 0:
            NoHubstaffAccountNotificationEmail(profile).send()
