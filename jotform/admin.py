from django.contrib import admin

from backoffice.admin import CustomURLModelAdmin

from jotform.models import Form, Webhook, Submission, Answer, Question
from jotform.views import SyncFormView, SyncWebhooksView, SyncQuestionsView, \
    SyncSubmissionsView


class WebhookInline(admin.TabularInline):
    model = Webhook
    extra = 0


class QuestionInline(admin.TabularInline):
    model = Question
    can_delete = False
    extra = 0
    readonly_fields = [i.name for i in Question._meta.get_fields()]
    ordering = ('order',)

    def has_add_permission(self, request):
        return False


@admin.register(Form)
class FormAdmin(CustomURLModelAdmin):
    inlines = (WebhookInline, QuestionInline,)
    list_display = ('id', 'title', 'status', 'count', 'updated_at',)
    readonly_fields = ('username', 'title', 'height', 'url', 'status',
                       'created_at', 'updated_at', 'new', 'count',
                       'last_submission', 'url',)
    search_fields = ('id', 'title')
    custom_urls = [
        {
            'regex': r'^(?P<pk>.+)/sync/form/$',
            'view': SyncFormView,
            'name': 'sync_form'
        },
        {
            'regex': r'^(?P<pk>.+)/sync/webhooks/$',
            'view': SyncWebhooksView,
            'name': 'sync_webhooks'
        },
        {
            'regex': r'^(?P<pk>.+)/sync/questions/$',
            'view': SyncQuestionsView,
            'name': 'sync_questions'
        },
        {
            'regex': r'^(?P<pk>.+)/sync/submissions/$',
            'view': SyncSubmissionsView,
            'name': 'sync_submissions'
        }
    ]


class AnswerInline(admin.TabularInline):
    model = Answer
    readonly_fields = [i.name for i in Answer._meta.get_fields()]
    can_delete = False
    extra = 0

    def has_add_permission(self, request):
        return False  # pragma: no cover


@admin.register(Submission)
class SubmissionAdmin(admin.ModelAdmin):
    inlines = (AnswerInline,)
    list_display = ('id', 'form', 'status', 'created_at', 'updated_at',)
    list_filter = ('form',)
    readonly_fields = ('id', 'status', 'new', 'flag', 'created_at',
                       'updated_at', 'form',)
    search_fields = ('answers__answer',)
