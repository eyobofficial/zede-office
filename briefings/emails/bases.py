from django.conf import settings
from django.urls import reverse

from backoffice.utilities.emails import BaseEmail


class BaseBriefingEmail(BaseEmail):
    template_location = '../templates/briefings/emails'
    email_type = ''


class BriefingEmail(BaseBriefingEmail):
    def __init__(self, briefing):
        self.briefing = briefing

    def send(self):
        """
        Send email to all trainers of the briefing's deliverables
        Return list of trainers which emails are sent to
        """
        if self.briefing.deliveries:
            sent_trainers = []
            for deliverable in self.briefing.deliveries.all():
                for trainer in deliverable.delivery.trainers.all():
                    if trainer in sent_trainers:
                        continue
                    self.recipient = trainer
                    super().send()
                    sent_trainers.append(trainer)
            return sent_trainers
        return

    def get_context_data(self):
        context = {
            'briefing': self.briefing,
            'trainer': self.recipient,
            'url': '{}{}'.format(
                settings.APP_HOSTNAME,
                reverse('briefings:overview')
            )
        }
        return super().get_context_data(**context)


class DeliverableEmail(BaseBriefingEmail):
    def __init__(self, deliverable):
        self.deliverable = deliverable
        self.recipient = self.deliverable.delivery.responsible

    def get_context_data(self):
        context = {
            'deliverable': self.deliverable,
            'url': f'{ settings.APP_HOSTNAME}{reverse("briefings:overview")}'
        }
        return super().get_context_data(**context)


class BriefingQuestionEmail(BaseBriefingEmail):
    def __init__(self, briefing_token):
        self.token = briefing_token

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['recipient'] = self.recipient
        context['token'] = self.token
        return context
