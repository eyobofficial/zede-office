import pendulum
from django.conf import settings

from DebatNL_BackOffice.celery import app
from planning.models import PlanningRequest, Schedule
from planning.emails.notifications import OptionTrainerConfirmEmail, \
    OverdueNotificationEmail
from planning.emails.reminders import PendingPlanningOptionReminderEmail, \
    FinalizeConfirmationReminderEmail, TrainerConfirmationReminderEmail, \
    AdminConfirmationReminderEmail


@app.task
def send_trainer_confirmation_email_every_24_hours():
    """
    Check every 24 hours if trainer e-mail has not been confirmed
    and re-send again
    """
    today = pendulum.today(settings.TIME_ZONE)
    unconfirmed_schedules = Schedule.objects.filter(is_confirmed=False)

    planning_requests = []

    for schedule in unconfirmed_schedules:
        created_at = pendulum.instance(schedule.created_at)
        created_at = created_at.in_tz(settings.TIME_ZONE)
        planning_request = schedule.delivery.planning_request

        is_new = planning_request not in planning_requests

        if today.diff(created_at).in_days() >= 1 and is_new:
            TrainerConfirmationReminderEmail(schedule).send()
            planning_requests.append(planning_request)


@app.task
def send_admin_notification_email_every_48_hours():
    """
    Check every  48 hours if admin e-mail has not been confirmed
    and re-send again
    """
    unconfirmed_planning_requests = PlanningRequest.objects.filter(
        is_confirmed_by_admin=False,
        request_approval_date__isnull=False,
        status=PlanningRequest.WAITING_FOR_APPROVAL
    )
    today = pendulum.today(tz=settings.TIME_ZONE)

    for planning_request in unconfirmed_planning_requests:
        request_at = pendulum.instance(planning_request.request_approval_date)
        request_at = request_at.in_tz(settings.TIME_ZONE)
        amount_days = today.diff(request_at).in_days()

        schedules = Schedule.objects.filter(
            delivery__planning_request=planning_request)
        confirmations = list(schedules.values_list('is_confirmed',
                                                   flat=True).all())

        if all(confirmations) and amount_days > 0 and amount_days % 2 == 0:
            AdminConfirmationReminderEmail(planning_request).send()


@app.task
def send_trainer_notification_email_every_48_hours():
    """
    Check every 48 hours if assigned trainer email has not be confirmed
    and re-send it again.
    """
    unconfirmed_schedules = Schedule.objects.filter(
        delivery__planning_request__type=PlanningRequest.OPTION,
        is_confirmed=False
    )
    today = pendulum.today(tz=settings.TIME_ZONE)

    for schedule in unconfirmed_schedules:
        assigned_date = pendulum.instance(schedule.created_at)
        assigned_date = assigned_date.in_tz(settings.TIME_ZONE)
        amount_days = today.diff(assigned_date).in_days()

        if amount_days > 0 and amount_days % 2 == 0:
            OptionTrainerConfirmEmail(schedule).send()


@app.task
def send_finalize_option_notification_email_every_48_hours():
    """
    Check every 48 hours if a trainer assigned to finalize an OPTION
    planning request has not confirmed the schedule and re-send it again.
    """
    unconfirmed_schedules = Schedule.objects.filter(
        delivery__planning_request__finalized_at__isnull=False,
        is_confirmed=False
    )
    today = pendulum.today(tz=settings.TIME_ZONE)

    for schedule in unconfirmed_schedules:
        assigned_date = pendulum.instance(schedule.updated_at)
        assigned_date = assigned_date.in_tz(settings.TIME_ZONE)
        amount_days = today.diff(assigned_date).in_days()

        if amount_days > 0 and amount_days % 2 == 0:
            FinalizeConfirmationReminderEmail(schedule).send()


@app.task
def send_reminder_for_pending_option_plans():
    """
    Send pending option planning request reminder e-mail every:
     - 14 days if more than 60 days are left for the pending option
       planning request.
     - 7 days if more than 30 days & less than 61 days are left for the
       pending planning request.
     - Monday, Wednesday & Friday if more than 14 days & less than 31 days
       are left for the pending planning request.
     - Monday, Tuesdays, Wednesday, Thursday & Friday if more than 0 days &
       less 15 days are left the pending planning request.
    """
    pending_option_plans = PlanningRequest.objects.filter(
        type=PlanningRequest.OPTION
    )
    today = pendulum.today(tz=settings.TIME_ZONE)
    d1 = [pendulum.MONDAY, pendulum.WEDNESDAY, pendulum.FRIDAY]
    d2 = d1 + [pendulum.TUESDAY, pendulum.THURSDAY]
    for planning_request in pending_option_plans:
        if not planning_request.get_date():  # No Related Deliveries
            return

        date = planning_request.get_date()
        date = pendulum.parse(date.isoformat(), tz=settings.TIME_ZONE)
        amount_days = today.diff(date).in_days()

        if amount_days > 60 and amount_days % 14 == 0:
            PendingPlanningOptionReminderEmail(planning_request).send()

        elif amount_days > 30 and amount_days < 61 and amount_days % 7 == 0:
            PendingPlanningOptionReminderEmail(planning_request).send()

        elif amount_days > 14 and amount_days < 31 and today.day_of_week in d1:
            PendingPlanningOptionReminderEmail(planning_request).send()

        elif amount_days > 0 and amount_days < 15 and today.day_of_week in d2:
            PendingPlanningOptionReminderEmail(planning_request).send()


@app.task
def send_overdue_notification_after_1_day():
    """
    Once OPTION planning requests is still open a day after the
    planning request date, send a overdue notification.
    """
    pending_option_plans = PlanningRequest.objects.filter(
        type=PlanningRequest.OPTION
    )
    today = pendulum.today(tz=settings.TIME_ZONE)

    for planning_request in pending_option_plans:
        if not planning_request.get_date():  # No Related Deliveries
            return

        date = planning_request.get_date()
        date = pendulum.parse(date.isoformat(), tz=settings.TIME_ZONE)
        amount_days = today.diff(date, False).in_days()

        if amount_days == -1:  # 1 day is passed
            OverdueNotificationEmail(planning_request).send()
