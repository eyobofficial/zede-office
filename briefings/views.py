import pendulum
from django.conf import settings
from django.contrib import messages
from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth.models import User
from django.db.models import Q
from django.shortcuts import redirect, render, get_object_or_404
from django.urls import reverse_lazy, reverse
from django.utils.decorators import method_decorator
from django.views import View
from django.views.generic import ListView, CreateView, DetailView, \
    UpdateView, TemplateView

from backoffice.constants import GROUP_BRIEFINGS
from backoffice.mixins import GroupAccessMixin
from backoffice.views import BaseEmailView
from briefings.emails.notifications import ChangedBriefingNotification, \
    MaxTrainersNotification, BriefingQuestionNotification, \
    AnswerNotification
from briefings.emails.reminders import BriefingNotAddedReminder, \
    TwoWeeksForDeliveryReminder, NewAddedBriefingReminder
from briefings.tasks import create_briefing_deliveries
from shared.enums import RoleName
from .forms import BriefingFileFormSet, QuestionFormSet, QuestionModelFormSet
from .mixins import BaseBriefingMixin
from .models import Deliverable, Briefing, DeliverableTrainer, \
    DeliverableMembership, DeliverableToken, BriefingQuestion, \
    BriefingQuestionToken


class OverviewView(GroupAccessMixin, ListView):
    """
    List all eligible deliveries for briefings and scans
    """
    template_name = 'briefings/deliverable_list.html'
    access_groups = [GROUP_BRIEFINGS]

    @staticmethod
    def calculate_cutoff_date(page):
        today = pendulum.today(tz=settings.TIME_ZONE)
        cutoff_week = page * 2
        return today.subtract(weeks=cutoff_week)

    def get_current_page(self, page):
        qs = Deliverable.objects.exclude(
            status=Deliverable.WAITING_FOR_APPROVAL
        ).exclude(status=Deliverable.TO_BE_DELETED)
        cutoff_date = self.calculate_cutoff_date(page)
        kwargs = {'delivery__order_date__gte': cutoff_date}
        return qs.filter(**kwargs)

    def has_previous_page(self, page):
        qs = Deliverable.objects.exclude(
            status=Deliverable.WAITING_FOR_APPROVAL
        ).exclude(status=Deliverable.TO_BE_DELETED)
        cutoff_date = self.calculate_cutoff_date(page - 1)
        kwargs = {'delivery__order_date__lte': cutoff_date}
        return qs.filter(**kwargs).exists()

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        page = int(self.request.GET.get('page', 0))
        next_page = page + 1
        context['page'] = next_page
        context['is_prev'] = self.has_previous_page(next_page)
        return context

    def get_queryset(self):
        page = int(self.request.GET.get('page', 0))
        self.queryset = self.get_current_page(page)

        if not self.request.user.has_perm('briefings.view_all_briefings'):
            self.queryset = self.queryset.filter(
                Q(delivery__responsible__email=self.request.user.email) |
                Q(delivery__trainers__email=self.request.user.email)
            ).distinct()

        return super().get_queryset()


class DeliverableDetail(GroupAccessMixin, DetailView):
    model = Deliverable
    template_name = 'briefings/components/modals/deliverable_detail.html'
    access_groups = [GROUP_BRIEFINGS]


class BaseDeliverableMaxTrainerView(DetailView):
    model = Deliverable
    token = None

    def dispatch(self, request, *args, **kwargs):
        try:
            token_key = self.request.GET.get('token')
            self.token = DeliverableToken.objects.get(key=token_key)
        except DeliverableToken.DoesNotExist:
            return render(request, 'briefings/public/expired_token.html')

        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(**kwargs)
        context['token'] = str(self.token.key)
        return context


class DeliverableMaxTrainerDetail(BaseDeliverableMaxTrainerView):
    template_name = 'briefings/public/max-trainers/base.html'


class DeliverableMaxTrainerUpdate(BaseDeliverableMaxTrainerView):
    template_name = 'briefings/public/max-trainers/update_trainers.html'

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(**kwargs)
        kwargs = {
            'profile__roles__name': RoleName.TRAINER.value,
            'is_active': True
        }
        trainers = User.objects.filter(**kwargs).all()
        context['trainers'] = trainers.order_by('first_name', 'last_name')
        return context

    def post(self, *args, **kwargs):
        deliverable = self.get_object()

        for key, trainer_id in self.request.POST.items():
            if 'trainers-' in key and trainer_id:
                user = User.objects.get(pk=trainer_id)

                try:
                    trainer = DeliverableTrainer.objects.get(email=user.email)
                except DeliverableTrainer.DoesNotExist:
                    trainer = DeliverableTrainer.objects.create(
                        name=user.get_full_name(),
                        email=user.email
                    )

                DeliverableMembership.objects.create(
                    trainer=trainer, deliverable=deliverable,
                    is_manually_added=True)

        url = reverse('briefings:deliverable-max-trainers-confirm',
                      args=(deliverable.pk,))
        return redirect(f'{url}?token={self.token.key}')


class DeliverableMaxTrainerConfirm(BaseDeliverableMaxTrainerView):
    template_name = 'briefings/public/max-trainers/confirm_update.html'

    def get(self, *args, **kwargs):
        deliverable = self.get_object()
        deliverable.status = deliverable.PENDING
        deliverable.save()

        self.token.delete()
        return super().get(*args, *kwargs)


class BriefingCreate(BaseBriefingMixin, CreateView):
    """
    Create briefings for selected deliverables
    """
    template_name = 'briefings/components/modals/modal_add_briefing.html'

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(**kwargs)
        if self.request.POST:
            context['briefing_formset'] = BriefingFileFormSet(
                self.request.POST, self.request.FILES)
        else:
            context['briefing_formset'] = BriefingFileFormSet()
        return context

    def form_valid(self, form):
        context = self.get_context_data()
        briefing_formset = context['briefing_formset']
        deliverable_formset = context['deliverable_formset']

        if briefing_formset.is_valid():
            briefing = form.save()
            briefing_formset.instance = briefing
            briefing_formset.save()

            if deliverable_formset.is_valid():
                deliverable_list = form.cleaned_data.get('deliverables')
                deliverable_formset.instance = briefing
                deliverable_formset.save(deliverable_list)

            return super().form_valid(form)

        return self.form_invalid(form)


class BriefingUpdate(BaseBriefingMixin, UpdateView):
    template_name = 'briefings/components/modals/modal_update_briefing.html'

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(**kwargs)
        if self.request.POST:
            context['briefing_formset'] = BriefingFileFormSet(
                self.request.POST, self.request.FILES, instance=self.object)
        else:
            context['briefing_formset'] = BriefingFileFormSet(
                instance=self.object)
        return context

    def form_valid(self, form):
        context = self.get_context_data()
        briefing_formset = context['briefing_formset']
        deliverable_formset = context['deliverable_formset']
        if briefing_formset.is_valid():
            briefing_formset.save()

            if deliverable_formset.is_valid():
                deliverable_list = form.cleaned_data.get('deliverables')
                deliverable_formset.instance = self.object
                deliverable_formset.save(deliverable_list)
            ChangedBriefingNotification(self.object).send()
        return super().form_valid(form)


class BriefingQuestionCreateView(GroupAccessMixin, CreateView):
    template_name = 'briefings/components/modals/question_create.html'
    model = BriefingQuestion
    form_class = QuestionFormSet
    success_url = reverse_lazy('briefings:overview')
    access_groups = [GROUP_BRIEFINGS]

    def get_form(self, form_class=None):
        deliverable = get_object_or_404(Deliverable, pk=self.kwargs.get('pk'))
        formset = self.get_form_class()
        data = self.request.POST
        if data:
            return formset(data, instance=deliverable.briefing)
        return formset(
            queryset=BriefingQuestion.objects.none(),
            instance=deliverable.briefing
        )

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['deliverable'] = get_object_or_404(Deliverable,
                                                   pk=self.kwargs['pk'])
        context['formset'] = context['form']
        return context

    def form_valid(self, formset):
        briefing_questions = formset.save(self.request.user)
        context = self.get_context_data()
        token = BriefingQuestionToken.objects.create(
            deliverable=context['deliverable'],
        )
        token.questions.add(*briefing_questions)
        BriefingQuestionNotification(token).send()
        return redirect(self.success_url)


class BriefingQuestionUpdateView(UpdateView):
    """
    View handles adding answers to briefing questions
    """
    template_name = 'briefings/public/add_answers.html'
    model = BriefingQuestionToken
    form_class = QuestionModelFormSet
    success_url = reverse_lazy('briefings:question-update-success')

    def get_queryset(self):
        return BriefingQuestionToken.objects.filter(is_expired=False)

    def get_form(self, form_class=None):
        formset = self.get_form_class()
        token = self.get_object()
        data = self.request.POST
        if data:
            return formset(data, self.request.FILES)
        return formset(queryset=token.questions.all())

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['formset'] = context['form']
        return context

    def form_valid(self, formset):
        formset.save()
        self.object.is_expired = True
        self.object.save()
        AnswerNotification(self.object).send()
        return redirect(self.success_url)


class QuestionUpdateSuccessView(TemplateView):
    template_name = 'briefings/public/question_success.html'


@method_decorator(staff_member_required, name='dispatch')
class SyncDeliverablesView(View):
    def get(self, request, *args, **kwargs):
        create_briefing_deliveries()
        messages.success(request, 'Deliverables have been synced.')
        return redirect(reverse('admin:briefings_deliverable_changelist'))


@method_decorator(staff_member_required, name='dispatch')
class BaseDeliverableEmailView(BaseEmailView):
    url = 'admin:briefings_deliverable_change'
    model = Deliverable


@method_decorator(staff_member_required, name='dispatch')
class BaseBriefingEmailView(BaseEmailView):
    url = 'admin:briefings_briefing_change'
    model = Briefing


@method_decorator(staff_member_required, name='dispatch')
class BaseBriefingTokenEmailView(BaseEmailView):
    url = 'admin:briefings_briefingquestiontoken_change'
    model = BriefingQuestionToken


class MaxTrainerNotificationEmail(BaseDeliverableEmailView):
    email_class = MaxTrainersNotification
    email_name = 'max trainer notification'


class BriefingNotAddedReminderEmail(BaseDeliverableEmailView):
    email_class = BriefingNotAddedReminder
    email_name = 'add briefing reminder'


class TwoWeeksForDeliveryReminderEmail(BaseDeliverableEmailView):
    email_class = TwoWeeksForDeliveryReminder
    email_name = 'two weeks left for delivery reminder'


class ChangedBriefingNotificationEmail(BaseBriefingEmailView):
    email_class = ChangedBriefingNotification
    email_name = 'briefing changed notification'


class NewAddedBriefingReminderEmail(BaseBriefingEmailView):
    email_class = NewAddedBriefingReminder
    email_name = 'new briefing added reminder'


class BriefingQuestionNotificationEmail(BaseBriefingTokenEmailView):
    email_class = BriefingQuestionNotification
    email_name = 'new briefing question added'


class AnswerNotificationEmail(BaseBriefingTokenEmailView):
    email_class = AnswerNotification
    email_name = 'briefing question answers added'
