from django.contrib import messages
from django.contrib.admin.views.decorators import staff_member_required
from django.shortcuts import redirect
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.views.generic import DetailView

from mailchimp.models import FeedbackSubmission


@method_decorator(staff_member_required, name='dispatch')
class AddToMailingListView(DetailView):
    url = 'admin:mailchimp_feedbacksubmission_change'
    model = FeedbackSubmission

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        list_id = self.request.GET.get('list_id')
        self.object.add_to_mailing_list(list_id)
        message = 'The submission has been added to the mailing list.'
        messages.success(request, message)
        return redirect(reverse(self.url, args=(self.object.pk,)))
