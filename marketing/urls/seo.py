from django.conf.urls import url

from marketing.views.seo import OverviewView, PageCreateView, PageDeleteView, \
    PageScoreCreateView, PageScoreDeleteView, PageScoreDetailView

urlpatterns = [
    url(r'^$', OverviewView.as_view(), name='overview'),
    url(r'^page/create/$', PageCreateView.as_view(), name='page-create'),
    url(
        r'^page/(?P<pk>[a-zA-Z0-9-]+)/delete/$',
        PageDeleteView.as_view(),
        name='page-delete'
    ),
    url(
        r'^page/(?P<pk>[a-zA-Z0-9-]+)/score/create/$',
        PageScoreCreateView.as_view(),
        name='page-score-create'
    ),
    url(
        r'^page/score/(?P<pk>[a-zA-Z0-9-]+)/delete/$',
        PageScoreDeleteView.as_view(),
        name='page-score-delete'
    ),
    url(
        r'^page/score/(?P<pk>[a-zA-Z0-9-]+)/detail/$',
        PageScoreDetailView.as_view(),
        name='page-score-detail'
    ),
]
