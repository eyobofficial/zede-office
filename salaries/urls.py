from django.conf.urls import url

from salaries.views import SalaryOverview, DeclarationApprovalRedirectView, \
    DeclarationFeedbackCreateView, TabExternalMeetingDetailView, \
    TabInternalMeetingDetailView, TabELearningDetailView, \
    TabHubstaffDetailView, TabAbsenceDetailView, TabExpenseDetailView, \
    SalaryRedirectView

urlpatterns = [
    url(r'^$', SalaryOverview.as_view(), name='overview'),
    url(
        r'^(?P<pk>[0-9]+)/$',
        SalaryRedirectView.as_view(),
        name='detail'
    ),
    url(
        r'^(?P<pk>[0-9]+)/external-meetings/$',
        TabExternalMeetingDetailView.as_view(),
        name='external-meeting-detail'
    ),
    url(
        r'^(?P<pk>[0-9]+)/internal-meetings/$',
        TabInternalMeetingDetailView.as_view(),
        name='internal-meeting-detail'
    ),
    url(
        r'^(?P<pk>[0-9]+)/e-learning/$',
        TabELearningDetailView.as_view(),
        name='e-learning-detail'
    ),
    url(
        r'^(?P<pk>[0-9]+)/hubstaff/$',
        TabHubstaffDetailView.as_view(),
        name='hubstaff-detail'
    ),
    url(
        r'^(?P<pk>[0-9]+)/absences/$',
        TabAbsenceDetailView.as_view(),
        name='absence-detail'
    ),
    url(
        r'^(?P<pk>[0-9]+)/expenses/$',
        TabExpenseDetailView.as_view(),
        name='expense-detail'
    ),
    url(
        r'^(?P<pk>[0-9]+)/approve/$',
        DeclarationApprovalRedirectView.as_view(),
        name='approve'
    ),
    url(
        r'^(?P<pk>[0-9]+)/deny/$',
        DeclarationFeedbackCreateView.as_view(),
        name='deny'
    ),
]
