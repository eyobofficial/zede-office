import requests

from django.conf import settings


class BaseConsumer:
    url = None
    base_url = settings.DEBATNL_ONLINE_API
    headers = {
        'Authorization': 'Token {}'.format(settings.DEBATNL_ONLINE_API_KEY)
    }

    def retrieve_all(self, **kwargs):
        url = '{}/'.format(self.url)
        response = requests.get(url, headers=self.headers, params=kwargs)
        response.raise_for_status()
        return response.json()

    def retrieve_object(self, object_id):
        url = '{}/{}/'.format(self.url, object_id)
        response = requests.get(url, headers=self.headers)
        response.raise_for_status()
        return response.json()

    def update_object(self, object_id, data):
        url = '{}/{}/'.format(self.url, object_id)
        response = requests.patch(url, headers=self.headers, json=data)
        response.raise_for_status()
        return response.json()

    def create_object(self, data):
        url = '{}/'.format(self.url)
        response = requests.post(url, headers=self.headers, json=data)
        response.raise_for_status()
        return response.json()

    def delete_object(self, object_id):
        url = '{}/{}/'.format(self.url, object_id)
        response = requests.delete(url, headers=self.headers)
        response.raise_for_status()
