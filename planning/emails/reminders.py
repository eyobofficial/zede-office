from django.conf import settings
from django.urls import reverse

from backoffice.utilities.emails import ReminderEmailMixin

from planning.emails.bases import BasePlanningEmail, BaseScheduleEmail, \
    BasePlanningRequestEmail
from planning.emails.notifications import TrainerScheduleConfirmEmail, \
    PlanningRequestStatusChangeEmail


class RequestAvailabilityReminder(ReminderEmailMixin, BasePlanningEmail):
    def __init__(self, trainer):
        self.template_name = 'email_6_request_availability.html'
        self.subject = 'Geef beschikbaarheid door'
        self.recipient = trainer

    def get_context_data(self):
        url = reverse('schedules:overview')
        context = {
            'url': f'{settings.APP_HOSTNAME}{url}'
        }
        return super().get_context_data(**context)


class PendingPlanningOptionReminderEmail(
        ReminderEmailMixin,
        BasePlanningRequestEmail):
    template_name = 'pending_planning_option_reminder_email.html'

    def __init__(self, planning_request):
        super().__init__(planning_request)
        self.recipient = planning_request.created_by
        organization = planning_request.deliveries.first().organization
        self.subject = f'Optie {organization}'


class FinalizeConfirmationReminderEmail(ReminderEmailMixin, BaseScheduleEmail):
    """
    Reminder e-mail sent to remind a trainer to confirm finalizing of an
    OPTION planning request.
    """
    template_name = 'finalize_schedule_reminder_email.html'

    def __init__(self, schedule):
        super().__init__(schedule)
        self.recipient = schedule.user
        delivery = self.schedule.delivery
        organization = delivery.organization
        date = delivery.date.strftime('%d-%m-%Y')
        self.subject = f'REMINDER: Optie {organization} op {date} bevestigd'

    def get_context_data(self, **kwargs):
        planning_request = self.schedule.delivery.planning_request
        trainer = self.schedule.user
        kwargs['schedule_list'] = trainer.schedules.filter(
            is_confirmed=False,
            delivery__planning_request=planning_request
        )
        uri = reverse(
            'planning:planning-request-confirm',
            args=[planning_request.pk, trainer.pk]
        )
        kwargs['url'] = f'{settings.APP_HOSTNAME}{uri}'
        return super().get_context_data(**kwargs)


class TrainerConfirmationReminderEmail(TrainerScheduleConfirmEmail):
    template_name = 'email_12_trainer_confirmation_reminder.html'

    def __init__(self, schedule):
        super().__init__(schedule)
        self.subject = f'HERINNERING sessie {schedule.delivery.organization}'


class AdminConfirmationReminderEmail(PlanningRequestStatusChangeEmail):
    template_name = 'email_13_admin_confirmation_reminder.html'

    @property
    def subject(self):
        first_delivery = self.planning_request.deliveries.first()
        organization = first_delivery.organization
        return f'HERINNERING pas CRM aan voor {organization}'
