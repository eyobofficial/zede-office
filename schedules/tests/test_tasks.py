import mock
from django.test import TestCase

from backoffice.constants import GROUP_SCHEDULES
from schedules.tasks import send_request_availability_email
from shared.enums import RoleName
from users.models import Role, AppPermission
from users.tests.factories import UserFactory


@mock.patch('schedules.tasks.RequestAvailabilityReminder')
class TaskRequestAvailabilityEmailTest(TestCase):
    fixtures = ['app_permissions', 'roles']

    def setUp(self):
        role = Role.objects.get(name=RoleName.ADMIN.value)
        admin = UserFactory()
        admin.profile.roles.add(role)

        # Active Trainer
        role = Role.objects.get(name=RoleName.TRAINER.value)
        permission = AppPermission.objects.get(app_name=GROUP_SCHEDULES)
        trainer_1 = UserFactory()
        trainer_1.profile.roles.add(role)
        trainer_1.profile.app_permissions.add(permission)

        # Inactive Trainer
        role = Role.objects.get(name=RoleName.TRAINER.value)
        trainer_2 = UserFactory(is_active=False)
        trainer_2.profile.roles.add(role)
        trainer_2.profile.app_permissions.add(permission)

        # No Permission Trainer
        role = Role.objects.get(name=RoleName.TRAINER.value)
        trainer_3 = UserFactory()
        trainer_3.profile.roles.add(role)

        self.trainers = {
            'active': trainer_1,
            'no_permission': trainer_2,
            'inactive': trainer_3
        }

    def test_on_the_15th_last_month(self, mock_class):
        """
        Test if an e-mail is send for the trainers which the `has_availability`
        and `is_active` is set to True.
        """
        send_request_availability_email()
        mock_class.assert_called_once_with(self.trainers['active'])
        mock_class().send.assert_called_once()
        mock_class.reset_mock()
