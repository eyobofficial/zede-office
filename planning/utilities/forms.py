import pendulum

from django.conf import settings

from planning.models import PlanningRequest


def calculate_delivery_type(date_list, reference_date=None):
    """
    Return the delivery type of a planning request based
    on the earliest date of the deliveries.
    """
    if reference_date is None:
        reference_date = pendulum.today(settings.TIME_ZONE)

    if len(date_list) == 0:
        return

    earliest_date = min(date_list)
    delta = reference_date.diff(earliest_date).in_days()

    if delta <= 7:
        request_type = PlanningRequest.IMMEDIATE
    elif 7 < delta < 120:
        request_type = PlanningRequest.SHORT_TERM
    else:
        request_type = PlanningRequest.LONG_TERM

    return request_type
