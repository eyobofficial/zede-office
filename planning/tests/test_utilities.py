import datetime

import pendulum
from django.conf import settings
from django.test import TestCase
from django.utils import timezone

from planning.models import PlanningRequest
from planning.utilities.forms import calculate_delivery_type
from planning.utilities.timetables import get_daily_timetable
from schedules.tests.factories import VacationPeriodFactory
from shared.enums import RoleName
from users.models import Role
from users.tests.factories import UserFactory


class CalculateDeliveryStatusTest(TestCase):
    """
    Tests for calculate_delivery_type() form utility function
    """

    def test_with_earliest_date_within_7_days_of_today(self):
        """
        Test function with a list of dates with the earliest
        date being within the 7 days of today (date <= 7)
        """
        today = pendulum.today(settings.TIME_ZONE)
        d1 = today.add(days=7)
        d2 = today.add(days=10)
        d3 = today.add(days=120)

        date_list = [d1, d2, d3]
        request_type = calculate_delivery_type(date_list)

        # Assertions
        self.assertEqual(request_type, PlanningRequest.IMMEDIATE)

    def test_with_earliest_date_between_8_and_120_days_of_today(self):
        """
        Test function with list of dates with the earliest
        date being between 8 and 120 days of today (8 <= date <= 7)
        """
        today = pendulum.today(settings.TIME_ZONE)
        d1 = today.add(days=8)
        d2 = today.add(days=120)
        d3 = today.add(days=200)

        date_list = [d1, d2, d3]
        request_type = calculate_delivery_type(date_list)

        # Assertions
        self.assertEqual(request_type, PlanningRequest.SHORT_TERM)

    def test_with_earliest_date_greater_than_120_days_from_today(self):
        """
        Test function with list of dates with the earliest
        date being greater than 120 days from today (date >= 120)
        """
        today = pendulum.today(settings.TIME_ZONE)
        d1 = today.add(days=121)
        d2 = today.add(days=200)
        d3 = today.add(days=500)

        date_list = [d1, d2, d3]
        request_type = calculate_delivery_type(date_list)

        # Assertions
        self.assertEqual(request_type, PlanningRequest.LONG_TERM)


class GetDailyTimetableTest(TestCase):
    """
    Tests for `get_daily_timetable()` function
    """
    fixtures = ['roles']

    def setUp(self):
        role = Role.objects.get(name=RoleName.TRAINER.value)
        self.user = UserFactory()
        self.user.profile.roles.add(role)
        self.today = timezone.now().date()

    def test_with_trainer_in_vacation(self):
        VacationPeriodFactory(
            start_date=self.today - datetime.timedelta(days=10),
            end_date=self.today + datetime.timedelta(days=10),
            user=self.user
        )

        timetable = get_daily_timetable(self.user, self.today)
        self.assertTrue(timetable['vacation'])

    def test_with_trainer_in_no_vacation(self):
        VacationPeriodFactory(
            start_date=self.today + datetime.timedelta(days=10),
            end_date=self.today + datetime.timedelta(days=20),
            user=self.user
        )

        timetable = get_daily_timetable(self.user, self.today)
        self.assertFalse(timetable['vacation'])

    def test_with_trainer_with_no_user(self):
        expected_timetable = {
            'date': self.today,
            'vacation': False,
            'morning': None,
            'afternoon': None,
            'evening': None
        }

        role = Role.objects.get(name=RoleName.TRAINER.value)
        trainer = UserFactory()
        trainer.profile.roles.add(role)

        timetable = get_daily_timetable(trainer, self.today)
        self.assertDictEqual(timetable, expected_timetable)
