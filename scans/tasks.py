import pendulum
from django.conf import settings

from DebatNL_BackOffice.celery import app
from scans.emails.reminders import ScanSubmissionReminderEmail, \
    ScanReviewReminderEmail
from scans.models import Scan


@app.task
def clean_up_outdated_scans(**kwargs):
    """
    Clean up scans that are FINISHED for longer than 6 weeks.
    """
    today = pendulum.today(settings.TIME_ZONE)
    finished_at = today.subtract(weeks=kwargs.get('weeks', 6))
    Scan.objects.filter(status=Scan.FINISHED,
                        finished_at__lt=finished_at).delete()


@app.task
def send_trainer_scan_submission_reminder():
    """
    Send the scan submission reminder to the trainers.
    """
    deadline_cutoff = pendulum.yesterday(settings.TIME_ZONE).date()
    kwargs = {'status': Scan.PENDING, 'deadline_at__lte': deadline_cutoff}
    scans = Scan.objects.filter(**kwargs).all()
    for scan in scans:
        ScanSubmissionReminderEmail(scan).send()


@app.task
def send_trainer_scan_review_reminder():
    """
    Send the scan review reminder to the trainers.
    """
    kwargs = {'status': Scan.PENDING, 'submitted_at__isnull': False}
    scans = Scan.objects.filter(**kwargs).all()
    now = pendulum.now(tz=settings.TIME_ZONE)

    for scan in scans:
        submitted_at = pendulum.instance(scan.submitted_at)
        submitted_at = submitted_at.in_tz(settings.TIME_ZONE)
        difference = submitted_at.diff(now, False).in_hours()

        if difference % 24 == 0:
            ScanReviewReminderEmail(scan).send()
