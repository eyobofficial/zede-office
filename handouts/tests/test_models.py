import datetime

import mock
import pendulum
from django.conf import settings

from django.core.files.uploadedfile import SimpleUploadedFile
from django.test import TestCase
from django.utils import timezone

from shared.tests.factories import TrainingFactory, InfographicFactory
from handouts.models import HandoutRequest
from webcrm.factories import DeliveryFactory

from .factories import HandoutRequestFactory, HandoutFactory


class HandoutRequestModelTests(TestCase):
    """
    Tests for `HandoutRequest` model
    """

    def test_get_training_method_with_no_related_delivery(self):
        """
        If a handout_request does not have a related delivery,
        ensure `get_training` method returns None.
        """
        training_date = timezone.now()
        handout_request = HandoutRequestFactory(
            training_date=training_date.date(),
        )
        self.assertIsNone(handout_request.get_training())

    def test_get_training_method_with_invalid_related_delivery(self):
        """
        If a handout_request have a related delivery, ensure
        `get_training` method returns the related delivery's product.
        """
        delivery = DeliveryFactory(product='Test Product')
        handout_request = HandoutRequestFactory()
        handout_request.deliveries.add(delivery)
        self.assertEqual(handout_request.get_training(), None)

    def test_get_training_method_with_valid_related_delivery(self):
        """
        If a handout_request have a related delivery, ensure
        `get_training` method returns the related delivery's product.
        """
        TrainingFactory(name='Test Training')
        delivery = DeliveryFactory(product='Test Training')
        handout_request = HandoutRequestFactory()
        handout_request.deliveries.add(delivery)
        self.assertEqual(handout_request.get_training(), 'Test Training')

    def test_set_corresponding_deliveries(self):
        dt = pendulum.datetime(2019, 5, 19)
        dt_in_tz = dt.in_tz(settings.TIME_ZONE)
        expected_delivery = DeliveryFactory(order_date=dt_in_tz, product='XYZ')
        handout_request = HandoutRequestFactory(training_date=dt.date())
        DeliveryFactory(order_date=dt, product='Digitale Intake')
        DeliveryFactory(order_date=dt, product='Debat.NL Online')
        DeliveryFactory(order_date=dt, product='SkillsTracker')
        DeliveryFactory(order_date=datetime.date(2019, 5, 20), product='ABC')
        deliveries = handout_request.set_corresponding_deliveries()
        self.assertEqual(handout_request.deliveries.count(), 1)
        self.assertEqual(handout_request.deliveries.first(), expected_delivery)
        self.assertListEqual(list(handout_request.deliveries.all()),
                             list(deliveries))

    @mock.patch('handouts.emails.notifications.PageSelectionNotification.send')
    def test_notification_based_on_HANDOUT_SELECTION(self, mock_obj):
        status = HandoutRequest.HANDOUT_SELECTION
        handout_request = HandoutRequestFactory(status=status)
        handout_request.send_notification_based_on_status()
        self.assertEqual(mock_obj.call_count, 1)

    @mock.patch('handouts.emails.notifications.TrainerNotAssignedNotification.'
                'send')
    def test_notification_based_on_TRAINER_NOT_FOUND(self, mock_obj):
        status = HandoutRequest.TRAINER_NOT_FOUND
        handout_request = HandoutRequestFactory(status=status)
        handout_request.send_notification_based_on_status()
        self.assertEqual(mock_obj.call_count, 1)

    @mock.patch('handouts.emails.notifications.NoDeliveryNotification.send')
    def test_notification_based_on_DELIVERY_NOT_FOUND(self, mock_obj):
        status = HandoutRequest.DELIVERY_NOT_FOUND
        handout_request = HandoutRequestFactory(status=status)
        handout_request.send_notification_based_on_status()
        self.assertEqual(mock_obj.call_count, 1)


class HandoutRequestStatusMethodTests(TestCase):
    """
    Tests for the `determine_status` method of the `HandoutRequest` model.
    """
    def setUp(self):
        self.product_1 = 'Overtuigend Debatteren'
        self.product_2 = 'Bewust Beinvloeden'
        TrainingFactory(name=self.product_1)
        TrainingFactory(name=self.product_2)

    def test_determine_status_with_0_deliveries(self):
        handout_request = HandoutRequestFactory()
        status = handout_request.determine_status()
        self.assertEqual(status, HandoutRequest.DELIVERY_NOT_FOUND)
        self.assertEqual(handout_request.status, status)

    def test_determine_status_without_trainer(self):
        delivery = DeliveryFactory()
        handout_request = HandoutRequestFactory(trainer='Trainer Bob')
        handout_request.deliveries.add(delivery)
        status = handout_request.determine_status()
        self.assertEqual(status, HandoutRequest.TRAINER_NOT_FOUND)
        self.assertEqual(handout_request.status, status)

    def test_determine_status_with_mismatching_deliveries(self):
        delivery_1 = DeliveryFactory(custom7='Bob', product=self.product_1)
        delivery_2 = DeliveryFactory(custom7='Bob', product=self.product_2)
        handout_request = HandoutRequestFactory(trainer='Bob')
        handout_request.deliveries.add(delivery_1)
        handout_request.deliveries.add(delivery_2)
        status = handout_request.determine_status()
        self.assertEqual(status, HandoutRequest.MISMATCHING_DELIVERIES)
        self.assertEqual(handout_request.status, status)

    def test_determine_status_with_matching_deliveries(self):
        delivery_1 = DeliveryFactory(custom7='Joe', product=self.product_1)
        delivery_2 = DeliveryFactory(custom7='Joe', product=self.product_1)
        handout_request = HandoutRequestFactory(trainer='Joe')
        handout_request.deliveries.add(delivery_1)
        handout_request.deliveries.add(delivery_2)
        status = handout_request.determine_status()
        self.assertEqual(status, HandoutRequest.HANDOUT_SELECTION)
        self.assertEqual(handout_request.status, status)

    def test_determine_status(self):
        delivery = DeliveryFactory(custom7='Joe', product=self.product_1)
        handout_request = HandoutRequestFactory(trainer='Joe')
        handout_request.deliveries.add(delivery)
        status = handout_request.determine_status()
        self.assertEqual(status, HandoutRequest.HANDOUT_SELECTION)
        self.assertEqual(handout_request.status, status)


class HandoutRequestTrainerCheckMethodTests(TestCase):
    """
    Tests for the `_check_for_trainer` method of the `HandoutRequest` model.
    """

    def test_no_trainers(self):
        handout_request = HandoutRequestFactory()
        delivery = DeliveryFactory()
        handout_request.deliveries.add(delivery)
        self.assertFalse(handout_request._check_for_trainer())

    def test_with_mismatching_trainer(self):
        handout_request = HandoutRequestFactory(trainer='Joe')
        delivery = DeliveryFactory(custom7='Trainer Joe')
        handout_request.deliveries.add(delivery)
        self.assertFalse(handout_request._check_for_trainer())

    def test_with_matching_trainer(self):
        handout_request = HandoutRequestFactory(trainer='Trainer Joe')
        delivery = DeliveryFactory(custom7='Trainer Joe')
        handout_request.deliveries.add(delivery)
        self.assertTrue(handout_request._check_for_trainer())


class HandoutRequestMatchingDeliveriesCheckMethodTests(TestCase):
    """
    Tests for the `_check_for_matching_deliveries` method of the
    `HandoutRequest` model.
    """

    def test_no_deliveries(self):
        handout_request = HandoutRequestFactory(trainer='Joe')
        delivery = DeliveryFactory(custom8='Joe')
        handout_request.deliveries.add(delivery)
        self.assertFalse(handout_request._check_for_matching_deliveries())

    def test_with_mismatching_delivery(self):
        handout_request = HandoutRequestFactory(trainer='Joe')
        delivery_1 = DeliveryFactory(custom8='Joe', product='Product A')
        delivery_2 = DeliveryFactory(custom8='Joe', product='Product B')
        handout_request.deliveries.add(delivery_1)
        handout_request.deliveries.add(delivery_2)
        self.assertFalse(handout_request._check_for_matching_deliveries())

    def test_with_one_delivery(self):
        handout_request = HandoutRequestFactory(trainer='Joe')
        delivery = DeliveryFactory(custom8='Joe', product='Product A')
        handout_request.deliveries.add(delivery)
        self.assertTrue(handout_request._check_for_matching_deliveries())

    def test_with_matching_deliveries(self):
        handout_request = HandoutRequestFactory(trainer='Joe')
        delivery_1 = DeliveryFactory(custom8='Joe', product='Product A')
        delivery_2 = DeliveryFactory(custom8='Joe', product='Product A')
        handout_request.deliveries.add(delivery_1)
        handout_request.deliveries.add(delivery_2)
        self.assertTrue(handout_request._check_for_matching_deliveries())


class HandoutRequestKnownDeliveriesCheckMethodTests(TestCase):
    """
    Tests for the `_check_for_known_deliveries` method of the `HandoutRequest`
    model.
    """

    def test_one_known_deliveries(self):
        TrainingFactory(name='Bewust Beinvloeden')
        handout_request = HandoutRequestFactory(trainer='Joe')
        delivery = DeliveryFactory(custom8='Joe', product='Bewust Beinvloeden')
        handout_request.deliveries.add(delivery)
        self.assertTrue(handout_request._check_for_known_deliveries())

    def test_multiple_known_deliveries(self):
        product_1 = 'Bewust Beinvloeden'
        product_2 = f'{product_1} (Open training)'
        TrainingFactory(name=product_1)
        TrainingFactory(name=product_2)
        handout_request = HandoutRequestFactory(trainer='Joe')
        delivery_1 = DeliveryFactory(custom8='Joe', product=product_1)
        delivery_2 = DeliveryFactory(custom8='Joe', product=product_2)
        handout_request.deliveries.add(delivery_1)
        handout_request.deliveries.add(delivery_2)
        self.assertTrue(handout_request._check_for_known_deliveries())

    def test_one_unknown_delivery(self):
        handout_request = HandoutRequestFactory(trainer='Joe')
        delivery = DeliveryFactory(custom8='Joe', product='Unknown Training')
        handout_request.deliveries.add(delivery)
        self.assertFalse(handout_request._check_for_known_deliveries())

    def test_multiple_unknown_deliveries(self):
        product_1 = 'Unknown Training 1'
        product_2 = 'Unknown Training 2'
        handout_request = HandoutRequestFactory(trainer='Joe')
        delivery_1 = DeliveryFactory(custom8='Joe', product=product_1)
        delivery_2 = DeliveryFactory(custom8='Joe', product=product_2)
        handout_request.deliveries.add(delivery_1)
        handout_request.deliveries.add(delivery_2)
        self.assertFalse(handout_request._check_for_known_deliveries())

    def test_with_both_known_and_unknown_deliveries(self):
        product_1 = 'Bewust Beinvloeden'
        product_2 = 'Unknown Training'
        handout_request = HandoutRequestFactory(trainer='Joe')
        delivery_1 = DeliveryFactory(custom8='Joe', product=product_1)
        delivery_2 = DeliveryFactory(custom8='Joe', product=product_2)
        handout_request.deliveries.add(delivery_1)
        handout_request.deliveries.add(delivery_2)
        self.assertFalse(handout_request._check_for_known_deliveries())


class HandoutModelTests(TestCase):
    """
    Tests for `Handout` model.
    """

    @mock.patch('handouts.models.Handout.combine_pdfs')
    def test_generate_pdf_method_without_related_pages(self, mock_obj):
        """
        If a handout instance have no related pages, ensure
        `generate_pdf` method does not generate PDF.
        """
        mock_obj.return_value = SimpleUploadedFile('handout.pdf', b'content')
        handout = HandoutFactory()
        handout.generate_pdf()
        self.assertEqual(mock_obj.call_count, 0)
        self.assertEqual(handout.file.name, '')

    @mock.patch('handouts.models.Handout.combine_pdfs')
    def test_generate_pdf_method_with_related_pages(self, mock_obj):
        """
        If a handout instance have related pages, ensure
        `generate_pdf` method generate PDF.
        """
        mock_obj.return_value = SimpleUploadedFile('handout.pdf', b'content')
        page1 = InfographicFactory()
        page2 = InfographicFactory()

        handout = HandoutFactory()
        handout.pages.add(page1, page2)
        handout.generate_pdf()

        self.assertEqual(mock_obj.call_count, 1)
        self.assertTrue(handout.file.name.endswith('handout.pdf'))
