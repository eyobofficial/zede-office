from django.conf.urls import url
from django.contrib.auth.decorators import login_required

from debatnl_online.views.e_learning.participants import ParticipantCreate, \
    ParticipantUpdate, ParticipantDelete

urlpatterns = [
    url(
        r'^create/$',
        login_required(ParticipantCreate.as_view()),
        name='create'
    ),
    url(
        r'^update/(?P<pk>[-\w]+)/$',
        login_required(ParticipantUpdate.as_view()),
        name='update'
    ),
    url(
        r'^delete/(?P<pk>[-\w]+)/$',
        login_required(ParticipantDelete.as_view()),
        name='delete'
    )
]
