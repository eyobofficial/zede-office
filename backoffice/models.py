import uuid

from django.contrib.auth.models import User
from django.db import models


class Training(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=200)

    class Meta:
        ordering = ('name',)

    def __str__(self):
        return self.name


class Employee(models.Model):
    TRAINER = 'TRAINER'
    ADMINISTRATOR = 'ADMINISTRATOR'
    EDITOR = 'EDITOR'

    CATEGORY_CHOICES = (
        (TRAINER, 'Trainer'),
        (ADMINISTRATOR, 'Administrator'),
        (EDITOR, 'Editor'),
    )

    employee_id = models.UUIDField(primary_key=True, default=uuid.uuid4,
                                   editable=False)
    last_name = models.CharField(max_length=50, blank=True, default='')
    first_name = models.CharField(max_length=50, blank=True, default='')
    phone = models.CharField(max_length=30, blank=True, default='')
    email = models.CharField(max_length=50, blank=True, default='')
    category = models.CharField(max_length=30, blank=True, default='',
                                choices=CATEGORY_CHOICES)
    hubstaff_user_id = models.IntegerField(blank=True, null=True)
    do_hubstaff_check = models.BooleanField(default=True)
    is_active = models.BooleanField(default=True)
    has_availability = models.BooleanField(default=False)
    has_planning = models.BooleanField(default=False)

    class Meta:
        ordering = ('first_name', 'last_name')

    def __str__(self):
        return '{} {}'.format(self.first_name, self.last_name)

    def get_full_name(self):
        return '{} {}'.format(self.first_name, self.last_name).strip()

    def get_user(self, is_active=True, **kwargs):
        try:
            return User.objects.get(email=self.email, is_active=is_active,
                                    **kwargs)
        except (User.DoesNotExist, User.MultipleObjectsReturned):
            return None

    @staticmethod
    def get_trainer(user, is_active=True):
        try:
            kwargs = {
                'email': user.email,
                'is_active': is_active,
                'category': Employee.TRAINER
            }
            return Employee.objects.get(**kwargs)
        except Employee.DoesNotExist:
            return None


class ContactPerson(models.Model):
    HALL = 'HALL'
    WEBSITE = 'WEBSITE'

    CATEGORY_CHOICES = (
        (HALL, 'Hall'),
        (WEBSITE, 'Website')
    )

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    last_name = models.CharField(max_length=50, null=True, blank=True)
    first_name = models.CharField(max_length=50, null=True, blank=True)
    phone = models.CharField(max_length=30, null=True, blank=True)
    email = models.CharField(max_length=50, null=True)
    category = models.CharField(max_length=30, null=True, blank=True,
                                choices=CATEGORY_CHOICES)
    is_active = models.BooleanField(default=True)

    class Meta:
        ordering = ('first_name', 'last_name')

    def __str__(self):
        return '{} {}'.format(self.first_name, self.last_name)
