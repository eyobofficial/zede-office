import re
from unicodedata import normalize

import boto3
from django.conf import settings
from django.contrib import messages
from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User
from django.http import JsonResponse
from django.shortcuts import redirect
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.utils.text import camel_case_to_spaces, slugify
from django.views import View
from django.views.generic import DetailView, TemplateView, UpdateView, \
    CreateView, DeleteView, ListView
from post_office.models import Email

from backoffice.constants import GROUP_BACKOFFICE
from backoffice.mixins import GroupAccessMixin, BaseTrainingMixin
from backoffice.models import Employee, Training
from planning.emails.reminders import RequestAvailabilityReminder


class OverviewView(GroupAccessMixin, ListView):
    access_groups = [GROUP_BACKOFFICE]
    queryset = User.objects.filter(is_active=True)
    ordering = ('first_name', 'last_name')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({'trainings': Training.objects.all()})
        return context


class TrainingCreateView(BaseTrainingMixin, CreateView):
    pass


class TrainingUpdateView(BaseTrainingMixin, UpdateView):
    pass


class TrainingDeleteView(BaseTrainingMixin, DeleteView):
    template_name = 'backoffice/components/modals/modal-delete.html'


class UserModifyOptionView(GroupAccessMixin, TemplateView):
    template_name = 'backoffice/components/modals/modal-user-form.html'
    access_groups = [GROUP_BACKOFFICE]


@method_decorator(staff_member_required, name='dispatch')
class EmailPreview(DetailView):
    model = Email
    template_name = 'backoffice/email-preview.html'


@method_decorator(staff_member_required, name='dispatch')
class BaseEmailView(DetailView):
    url = None
    object = None
    email_class = None

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        self.email_class(self.object).send()
        message = f'The {self.get_email_name()} e-mail has been sent.'
        messages.success(request, message)
        return redirect(reverse(self.url, args=(self.object.pk,)))

    @classmethod
    def get_admin_url_name(cls) -> str:
        class_name = camel_case_to_spaces(cls.email_class.__name__)
        return slugify(f'send-{class_name}')

    @classmethod
    def get_admin_url_regex(cls):
        return r'^(?P<pk>.+)/' + cls.get_admin_url_name()

    @classmethod
    def get_email_name(cls):
        class_name = camel_case_to_spaces(cls.email_class.__name__)
        return re.sub('view$', '', class_name)


class RequestAvailabilityEmailView(BaseEmailView):
    url = 'admin:backoffice_employee_change'
    model = Employee
    email_class = RequestAvailabilityReminder
    email_name = 'request availability reminder'


class BaseSignS3UploadURLView(LoginRequiredMixin, View):
    aws_s3_bucket = None

    def get(self, request, **kwargs):
        filename = request.GET.get('file_name')
        filename = normalize('NFKD', filename).encode('ascii', 'ignore')
        filename = filename.decode('UTF-8')
        filename = filename.replace(' ', '_')

        key = 'temp/videos/{}/{}'.format(settings.AWS_S3_BUCKET_SUB_PATH,
                                         filename)
        client = boto3.client(
            's3',
            aws_access_key_id=settings.AWS_ACCESS_KEY,
            aws_secret_access_key=settings.AWS_SECRET_KEY,
            region_name='eu-central-1'
        )

        pre_signed_post = client.generate_presigned_post(
            Bucket=self.aws_s3_bucket,
            Key=key,
            Fields={'acl': 'public-read'},
            Conditions=[{'acl': 'public-read'}]
        )

        return JsonResponse(pre_signed_post)
