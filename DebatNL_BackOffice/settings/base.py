import os

import environ

from django.db import models

from DebatNL_BackOffice.utilities.settings_loader import env, project_name

root = environ.Path(__file__) - 3

BASE_DIR = root()

INSTALLED_APPS = [
    'users',                        # This app should be before admin and auth!
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.humanize',
    'raven.contrib.django.raven_compat',
    'post_office',
    'djrill',
    'compressor',
    'sass_processor',
    'django_assets',
    'ordered_model',
    'django_celery_beat',
    'django_celery_results',
    'tinymce',
    'field_history',
    'extra_views'
]

# Debat.NL Specific Apps

INSTALLED_APPS += [
    'feedback',
    'backoffice',
    'scans',
    'debatnl_online',
    'debatnl_registration',
    'offers',
    'intakes',
    'jotform',
    'briefings',
    'follow_ups',
    'planning',
    'schedules',
    'ccd',
    'debatnl_mobile',
    'marketing',
    'webcrm',
    'handouts',
    'mailchimp',
    'shared',
    'declarations',
    'salaries',
    'hubstaff',
    'education'
]


MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = '%s.urls' % project_name

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = '%s.wsgi.application' % project_name
ENVIRONMENT = env('ENVIRONMENT')


# Password validation
# https://docs.djangoproject.com/en/1.10/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.'
                'MinimumLengthValidator',
        'OPTIONS': {
            'min_length': 8,
        }
    },
    {
        'NAME': 'users.validators.CapitalLetterValidator',
    },
]

# Database
# https://docs.djangoproject.com/en/1.10/ref/settings/#databases

DATABASES = {
    'default': env.db()
}

# Internationalization
# https://docs.djangoproject.com/en/1.10/topics/i18n/

LANGUAGE_CODE = 'en'
TIME_ZONE = 'Europe/Amsterdam'
USE_I18N = True
USE_L10N = True
USE_TZ = True

# Sentry Settings
RAVEN_CONFIG = {
    'dsn': env('SENTRY_DSN', default=''),
    'environment': ENVIRONMENT
}

# E-mail Settings

EMAIL_BACKEND = 'post_office.EmailBackend'
POST_OFFICE = {
    'BACKENDS': {
        'default': 'djrill.mail.backends.djrill.DjrillBackend'
    }
}

ALLOW_EMAIL_SENDING = False

# Login Redirects

LOGIN_URL = '/login/'
LOGIN_REDIRECT_URL = '/'
LOGOUT_REDIRECT_URL = '/login/'

# Allowed Hosts

ALLOWED_HOSTS = ['*']

# SASS Settings

SASS_PRECISION = 8
SASS_OUTPUT_STYLE = 'compact'

# Static Settings

STATICFILES_FINDERS = [
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    'sass_processor.finders.CssFinder',
    'compressor.finders.CompressorFinder',
]

STATIC_ROOT = 'static'
STATIC_URL = '/static/'

STATICFILES_DIRS = [
    os.path.join(BASE_DIR, 'DebatNL_BackOffice', 'static'),
    os.path.join(BASE_DIR, 'node_modules')
]

# Media Settings

MEDIA_URL = '/media/'
MEDIA_ROOT = 'media'

# Gravity Form API Settings

ALLOW_GRAVITY_FORM_REQUESTS = False
GRAVITY_FORM_ENDPOINT = env('GRAVITY_FORM_ENDPOINT', default='')
GRAVITY_FORM_PUBLIC_KEY = env('GRAVITY_FORM_PUBLIC_KEY', default='')
GRAVITY_FORM_PRIVATE_KEY = env('GRAVITY_FORM_PRIVATE_KEY', default='')

# MS Graph/Sharepoint Settings

MS_GRAPH_TENANT = 'debatnl.onmicrosoft.com'
MS_GRAPH_CLIENT_ID = env('MS_GRAPH_CLIENT_ID', default='')
MS_GRAPH_CLIENT_SECRET = env('MS_GRAPH_CLIENT_SECRET', default='')
MS_SHAREPOINT_HOST = 'debatnl.sharepoint.com'

# Celery Settings

CELERY_ENABLE_UTC = False
CELERY_RESULT_BACKEND = 'django-db'
CELERY_BEAT_SCHEDULER = 'django_celery_beat.schedulers:DatabaseScheduler'
CELERY_BROKER_URL = 'amqp://{}:{}@{}/debatnl_backoffice'.format(
    env('RABBITMQ_USERNAME'), env('RABBITMQ_PASSWORD'), env('RABBITMQ_HOST')
)

# WebCRM Settings

WEBCRM_AUTH_CODE = env('WEBCRM_AUTH_CODE', default='')


# Field History Configuration for MYSQL
FIELD_HISTORY_OBJECT_ID_TYPE = (models.CharField, {'max_length': 100})


# Debat.NL Mobile API URL

DEBATNL_MOBILE_API = env(
    'DEBATNL_MOBILE_API',
    default='http://localhost:8080/admins/'
)
DEBATNL_MOBILE_API_KEY = env(
    'DEBATNL_MOBILE_API_KEY',
    default=''
)

# MailChimp Settings

MAILCHIMP_API_KEY = env('MAILCHIMP_API_KEY', default='')

# Google Maps

GOOGLE_MAP_API_KEY = env('GOOGLE_MAP_API_KEY', default='')

# Hubstaff Settings

HUBSTAFF_APP_TOKEN = env('HUBSTAFF_APP_TOKEN', default='')
HUBSTAFF_EMAIL = env('HUBSTAFF_EMAIL', default='')
HUBSTAFF_PASSWORD = env('HUBSTAFF_PASSWORD', default='')
