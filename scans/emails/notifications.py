from django.conf import settings
from django.urls import reverse

from backoffice.utilities.emails import NotificationEmailMixin
from scans.emails.bases import BaseScanDREmail, BaseScanTrainerEmail


class NewScanEmail(NotificationEmailMixin, BaseScanTrainerEmail):
    template_name = 'email_1_new_scan.html'

    def __init__(self, scan):
        super().__init__(scan)
        self.subject = f'Deadline {scan.organization}'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['url'] = f"{settings.APP_HOSTNAME}{reverse('scans:overview')}"
        return context


class UploadConfirmationEmail(NotificationEmailMixin, BaseScanTrainerEmail):
    template_name = 'email_2_upload_confirmation.html'
    subject = 'Upload succesvol'


class UploadDRNotificationEmail(NotificationEmailMixin, BaseScanDREmail):
    template_name = 'email_3_dr_upload_notification.html'

    def __init__(self, scan):
        super().__init__(scan)
        self.subject = f'Scan {scan.organization} met de bestandsnaam ' \
                       f'{scan.client_filename} ingediend'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        uri = reverse('scans:approve', args=(self.scan.pk,))
        context['url'] = f"{settings.APP_HOSTNAME}{uri}"
        return context


class ScanDeniedEmail(NotificationEmailMixin, BaseScanTrainerEmail):
    template_name = 'email_4_scan_denied.html'

    def __init__(self, scan):
        super().__init__(scan)
        self.subject = f'Scan {scan.organization} moet aangepast worden'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['scan'] = self.scan
        context['feedback'] = self.scan.reviews.last().feedback
        return context


class ScanApprovedEmail(NotificationEmailMixin, BaseScanTrainerEmail):
    template_name = 'email_5_scan_approved.html'

    def __init__(self, scan):
        super().__init__(scan)
        self.subject = f'Scan {scan.organization} akkoord'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['scan'] = self.scan
        return context
