from django.conf.urls import url

from schedules.views import OverviewView, VacationFormStep1View, \
    VacationFormStep2View, VacationFormStep3View, VacationFormStep4View, \
    VacationFormStep5View, VacationFormStep6View, VacationFormStep7View, \
    VacationFormStep8View, VacationFormStep9View, VacationFormStep10View, \
    VacationFormStep11View, VacationApprovalView

urlpatterns = [
    url(r'^$', OverviewView.as_view(), name='overview'),
    url(
        r'^vacation-approvals/(?P<pk>[a-zA-Z0-9-]+)',
        VacationApprovalView.as_view(),
        name='vacation-approval'
    ),
    url(
        r'^vacation/steps/1/$',
        VacationFormStep1View.as_view(),
        name='step-1'
    ),
    url(
        r'^vacation/steps/2/$',
        VacationFormStep2View.as_view(),
        name='step-2'
    ),
    url(
        r'^vacation/steps/3/$',
        VacationFormStep3View.as_view(),
        name='step-3'
    ),
    url(
        r'^vacation/steps/4/$',
        VacationFormStep4View.as_view(),
        name='step-4'
    ),
    url(
        r'^vacation/steps/5/$',
        VacationFormStep5View.as_view(),
        name='step-5'
    ),
    url(
        r'^vacation/steps/6/$',
        VacationFormStep6View.as_view(),
        name='step-6'
    ),
    url(
        r'^vacation/steps/7/$',
        VacationFormStep7View.as_view(),
        name='step-7'
    ),
    url(
        r'^vacation/steps/8/$',
        VacationFormStep8View.as_view(),
        name='step-8'
    ),
    url(
        r'^vacation/steps/9/$',
        VacationFormStep9View.as_view(),
        name='step-9'
    ),
    url(
        r'^vacation/steps/10/$',
        VacationFormStep10View.as_view(),
        name='step-10'
    ),
    url(
        r'^vacation/steps/11/$',
        VacationFormStep11View.as_view(),
        name='step-11'
    )
]
