import requests
from django.conf import settings

from hubstaff.models import HubstaffAuthToken


class HubstaffAPIClient:
    host = 'https://api.hubstaff.com/v1'
    app_token = settings.HUBSTAFF_APP_TOKEN
    email = settings.HUBSTAFF_EMAIL
    password = settings.HUBSTAFF_PASSWORD
    auth_token = None
    retry_mode = False

    def __init__(self):
        if HubstaffAuthToken.objects.exists():
            token = HubstaffAuthToken.objects.first()
        else:
            token = self.obtain_token_from_api()

        self.auth_token = token.token

    def _get_headers(self):
        return {'App-Token': self.app_token, 'Auth-Token': self.auth_token}

    def _get(self, path, params=None):
        headers = self._get_headers()
        response = requests.get(path, params=params, headers=headers)
        unauthorized_code = requests.codes['unauthorized']
        is_unauthorized = response.status_code == unauthorized_code

        if is_unauthorized and not self.retry_mode:
            HubstaffAuthToken.objects.all().delete()
            token = self.obtain_token_from_api()
            self.auth_token = token.token
            self.retry_mode = True
            return self._get(path, params=params)
        else:
            response.raise_for_status()
            return response.json()

    def obtain_token_from_api(self):
        path = f'{self.host}/auth'
        headers = {'App-Token': self.app_token}
        data = {'email': self.email, 'password': self.password}
        response = requests.post(path, data=data, headers=headers)
        response.raise_for_status()
        json = response.json()
        token = json['user']['auth_token']
        last_activity = json['user']['last_activity']
        kwargs = {'token': token, 'last_activity': last_activity}
        return HubstaffAuthToken.objects.create(**kwargs)

    def get_users(self):
        json = self._get(f'{self.host}/users')
        return json['users']

    def get_total_time_per_user(self, user_id, start_date, end_date):
        params = {
            'start_date': start_date,
            'end_date': end_date,
            'users': user_id
        }
        json = self._get(f'{self.host}/custom/by_member/team', params=params)

        try:
            duration = json['organizations'][0]['duration']
        except (IndexError, KeyError):
            duration = 0

        return duration
