import pendulum
from django.conf import settings
from django.contrib.auth.models import Permission
from django.test import TestCase
from django.urls import reverse

from backoffice.constants import GROUP_FOLLOWUPS
from backoffice.tests.factories import UserFactory
from follow_ups.models import FollowUp, FollowUpEvent
from follow_ups.tests.factories import FollowUpFactory, FollowUpEventFactory
from follow_ups.views import FollowUpSetupStep2View
from shared.tests.factories import InfographicFactory
from users.models import AppPermission
from webcrm.factories import UserFactory as WebCRMUserFactory, \
    OpportunityFactory


class FollowUpOverviewTest(TestCase):
    """
    Test the Overview view
    """
    fixtures = ['app_permissions']

    def setUp(self):
        self.template = 'follow_ups/overview.html'
        self.url = reverse('follow_ups:overview')
        self.user = UserFactory()
        app_permission = AppPermission.objects.get(app_name=GROUP_FOLLOWUPS)
        self.user.profile.app_permissions.add(app_permission)

    def test_request_as_anonymous_user(self):
        """
        Test a request to the Overview view from anonymous user
        """
        response = self.client.get(self.url, follow=True)
        expected_url = f'{reverse("login")}?next=/feedback/'
        self.assertRedirects(response, expected_url)

    def test_request_as_authenticated_user_with_the_right_permission(self):
        """
        Ensure an authenticated users with `Can see all follow_ups` permission
        can see all (non-deleted) follow_up instances by issueing a GET
        request.
        """
        permission = Permission.objects.get(name='Can see all follow_ups')
        self.user.user_permissions.add(permission)
        follow_ups = [
            FollowUpFactory(status=FollowUp.NEW),
            FollowUpFactory(status=FollowUp.NO_CUSTOMER_CONTACT),
            FollowUpFactory(status=FollowUp.NO_SALES_RESPONSIBLE),
            FollowUpFactory(status=FollowUp.READY),
            FollowUpFactory(status=FollowUp.READY),
        ]

        FollowUpFactory(status=FollowUp.DELETED)

        self.client.force_login(self.user)
        response = self.client.get(self.url)
        object_list = list(response.context['object_list'])

        follow_ups[0].product = 'Johnny'
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'follow_ups/overview.html')
        self.assertCountEqual(object_list, follow_ups)
        self.assertSetEqual(set(object_list), set(follow_ups))

    def test_request_as_authenticated_user_without_the_right_permission(self):
        """
        Ensure an authenticated users without `Can see all follow_ups`
        permission can only see those follow_ups which he/she is assigned
        to as sales responsible.
        """

        # Follow-ups which the user is NOT assigned to as a sales representative
        follow_ups1 = [FollowUpFactory(), FollowUpFactory()]

        user = WebCRMUserFactory(email=self.user.email)
        opportunity1 = OpportunityFactory(assigned_to=user)
        opportunity2 = OpportunityFactory(assigned_to=user)

        # Follow-ups which the user is assigned as a saltes representative
        follow_ups2 = [
            FollowUpFactory(opportunity=opportunity1),
            FollowUpFactory(opportunity=opportunity2)
        ]

        self.client.force_login(self.user)
        response = self.client.get(self.url)
        object_list = list(response.context['object_list'])

        # Assertions
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'follow_ups/overview.html')
        self.assertCountEqual(object_list, follow_ups2)
        self.assertSetEqual(set(object_list), set(follow_ups2))


class FollowUpDetailViewTest(TestCase):
    """
    Test the followup-detail view
    """
    fixtures = ['app_permissions']

    def setUp(self):
        self.user = UserFactory()
        app_permission = AppPermission.objects.get(app_name=GROUP_FOLLOWUPS)
        self.user.profile.app_permissions.add(app_permission)
        self.follow_up = FollowUpFactory()
        kwargs = {'pk': self.follow_up.pk}
        self.url = reverse('follow_ups:followup-detail', kwargs=kwargs)

    def test_GET_request_as_anonymous_user(self):
        """
        Test request to the followup-detail view from anonymous user
        """
        response = self.client.get(self.url, follow=True)
        expected_url = f'{reverse("login")}?next=/feedback/'
        self.assertRedirects(response, expected_url)

    def test_GET_request_as_authenticated_user(self):
        """
        Test GET request to the followup-detail view from authenticated
        user
        """
        template = 'follow_ups/components/modals/detail_followup.html'
        self.client.force_login(self.user)
        response = self.client.get(self.url)

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, template)
        self.assertEqual(response.context['object'], self.follow_up)


class FollowUpDeleteViewTest(TestCase):
    """
    Test followup-detail view
    """
    fixtures = ['app_permissions']

    def setUp(self):
        self.user = UserFactory()
        app_permission = AppPermission.objects.get(app_name=GROUP_FOLLOWUPS)
        self.user.profile.app_permissions.add(app_permission)
        self.follow_up = FollowUpFactory()

    def test_GET_request_as_anonymous_user(self):
        """
        Test GET request to the followup-delete view from anonymous user
        """
        kwargs = {'pk': str(self.follow_up.pk)}
        url = reverse('follow_ups:followup-delete', kwargs=kwargs)
        response = self.client.get(url, follow=True)
        expected_url = f'{reverse("login")}?next=/feedback/'
        self.assertRedirects(response, expected_url)

    def test_POST_request_as_anonymous_user(self):
        """
        Test POST request to the followup-delete view from anonymous user
        """
        kwargs = {'pk': str(self.follow_up.pk)}
        url = reverse('follow_ups:followup-delete', kwargs=kwargs)
        response = self.client.post(url, follow=True)
        expected_url = f'{reverse("login")}?next=/feedback/'
        self.assertRedirects(response, expected_url)

    def test_GET_request_as_authenticated_user(self):
        """
        Test GET request to the followup-delete view from authenticated
        user
        """
        self.client.force_login(self.user)
        kwargs = {'pk': str(self.follow_up.pk)}
        url = reverse('follow_ups:followup-delete', kwargs=kwargs)
        response = self.client.get(url)
        template = 'follow_ups/components/modals/delete_followup.html'
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, template)

    def test_POST_request_as_authenticated_user(self):
        """
        Test POST request to the followup-delete view from authenticated
        user
        """

        FollowUpEventFactory(follow_up=self.follow_up)

        self.client.force_login(self.user)
        kwargs = {'pk': str(self.follow_up.pk)}
        url = reverse('follow_ups:followup-delete', kwargs=kwargs)
        response = self.client.post(url, follow=True)
        expected_url = reverse('follow_ups:overview')

        self.follow_up.refresh_from_db()

        self.assertRedirects(response, expected_url=expected_url)
        self.assertEqual(self.follow_up.status, FollowUp.DELETED)
        self.assertEqual(self.follow_up.events.count(), 0)


class FollowUpSetupViewTest(TestCase):
    """
    Test followup-setup view
    """
    fixtures = ['app_permissions']

    def setUp(self):
        self.user = UserFactory()
        app_permission = AppPermission.objects.get(app_name=GROUP_FOLLOWUPS)
        self.user.profile.app_permissions.add(app_permission)
        self.follow_up = FollowUpFactory()

    def test_GET_request_as_anonymous_user(self):
        """
        Test GET request to the followup-setup view from anonymous user
        """
        kwargs = {'pk': str(self.follow_up.pk)}
        url = reverse('follow_ups:followup-setup', kwargs=kwargs)
        response = self.client.get(url, follow=True)
        expected_url = f'{reverse("login")}?next={url}'
        self.assertRedirects(response, expected_url)

    def test_GET_request_as_authenticated_user(self):
        """
        Test GET request to the followup-setup view from authenticated
        user
        """
        self.client.force_login(self.user)
        kwargs = {'pk': str(self.follow_up.pk)}
        url = reverse('follow_ups:followup-setup', kwargs=kwargs)
        response = self.client.get(url)
        template = 'follow_ups/public/setup_followup.html'
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, template)


class FollowUpEventCreateViewTest(TestCase):
    """
    Test followup-event-create view
    """
    fixtures = ['app_permissions']

    def setUp(self):
        self.user = UserFactory()
        app_permission = AppPermission.objects.get(app_name=GROUP_FOLLOWUPS)
        self.user.profile.app_permissions.add(app_permission)
        self.follow_up = FollowUpFactory()

    def test_GET_request_as_anonymous_user(self):
        """
        Test GET request to the followup-event-create view from anonymous user
        """
        kwargs = {'pk': str(self.follow_up.pk)}
        url = reverse('follow_ups:followup-event-create', kwargs=kwargs)
        response = self.client.get(url, follow=True)
        expected_url = f'{reverse("login")}?next=/feedback/'
        self.assertRedirects(response, expected_url)

    def test_GET_request_as_authenticated_user(self):
        """
        Test GET request to the followup-event-create view from authenticated
        user
        """
        self.client.force_login(self.user)
        kwargs = {'pk': str(self.follow_up.pk)}
        url = reverse('follow_ups:followup-event-create', kwargs=kwargs)
        response = self.client.get(url)
        template = 'follow_ups/components/modals/create_followup_event.html'
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, template)

    def test_POST_request_as_anonymous_user(self):
        """
        Test POST request to the followup-event-create view from anonymous user
        """
        kwargs = {'pk': str(self.follow_up.pk)}
        url = reverse('follow_ups:followup-event-create', kwargs=kwargs)
        response = self.client.post(url, follow=True)
        expected_url = f'{reverse("login")}?next=/feedback/'
        self.assertRedirects(response, expected_url)

    def test_POST_request_as_authenticated_user(self):
        """
        Test POST request to the followup-event-create from authenticated
        user
        """
        payload = {
            'date': '2019-05-05',
            'type': FollowUpEvent.TYPE_INFOGRAPHIC,
            'subject': 'Test Subject',
            'message': 'Hello World Message',
            'button_text': 'Button Text',
            'button_url': 'www.test.test/test',
            'follow_up': str(self.follow_up.pk),
        }

        self.client.force_login(self.user)
        pk = str(self.follow_up.pk)
        url = reverse('follow_ups:followup-event-create', args=(pk,))
        response = self.client.post(url, payload, follow=True)
        expected_url = reverse('follow_ups:followup-setup', args=(pk,))

        self.follow_up.refresh_from_db()

        self.assertRedirects(response, expected_url=expected_url)
        self.assertEqual(FollowUpEvent.objects.count(), 1)
        self.assertEqual(self.follow_up.events.count(), 1)


class FollowUpSetupStep1ViewTest(TestCase):
    """
    Tests for `followup-setup-step1` view
    """
    fixtures = ['app_permissions']

    def setUp(self):
        self.user = UserFactory()
        app_permission = AppPermission.objects.get(app_name=GROUP_FOLLOWUPS)
        self.user.profile.app_permissions.add(app_permission)
        self.follow_up = FollowUpFactory()
        self.url = reverse(
            'follow_ups:followup-setup-step1',
            args=[self.follow_up.pk]
        )

    def test_GET_request_with_anonymous_user(self):
        """
        Ensure GET request from anonymous user cannot access the
        `FollowUpSetupStep1View` view.
        """
        response = self.client.get(self.url, follow=True)
        expected_url = f'{reverse("login")}?next=/feedback/'
        self.assertRedirects(response, expected_url)

    def test_GET_request_with_authenticated_user(self):
        """
        Ensure GET request from authenticated user can access the
        `FollowUpSetupStep1View` view.
        """
        self.client.force_login(self.user)
        response = self.client.get(self.url)
        template = 'follow_ups/components/modals/setup/step_1.html'

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, template)
        self.assertContains(response, 'form')

    def test_POST_request_with_authenticated_user(self):
        """
        Ensure POST request from authenticated users redirects
        step 2 of the form.
        """
        self.client.force_login(self.user)
        infographic_1 = InfographicFactory()
        infographic_2 = InfographicFactory()
        infographic_3 = InfographicFactory()

        payload = {
            'infographics': [infographic_1.pk, infographic_2.pk]
        }
        response = self.client.post(self.url, payload, follow=True)
        infographic_pks = self.client.session.get('infographic_pks')
        expected_url = reverse(
            'follow_ups:followup-setup-step2',
            args=[self.follow_up.pk]
        )

        # Assertions
        self.assertEqual(response.status_code, 200)
        self.assertIsNotNone(infographic_pks)
        self.assertTrue(infographic_1.pk.__str__() in infographic_pks)
        self.assertTrue(infographic_2.pk.__str__() in infographic_pks)
        self.assertFalse(infographic_3.pk.__str__() in infographic_pks)
        self.assertRedirects(response, expected_url)


class FollowUpSetupStep2ViewTest(TestCase):
    """
    Tests for `followup-setup-step1` view
    """
    fixtures = ['app_permissions']

    def setUp(self):
        self.user = UserFactory()
        app_permission = AppPermission.objects.get(app_name=GROUP_FOLLOWUPS)
        self.user.profile.app_permissions.add(app_permission)
        self.follow_up = FollowUpFactory()
        self.url = reverse(
            'follow_ups:followup-setup-step2',
            args=[self.follow_up.pk]
        )

    def test_GET_request_as_anonymous_user(self):
        """
        Ensure GET request from non-authenticated user cannot access the
        `FollowUpSetupStep2View` view.
        """
        response = self.client.get(self.url, follow=True)
        expected_url = f'{reverse("login")}?next=/feedback/'
        self.assertRedirects(response, expected_url)

    def test_GET_request_as_authenticated_user(self):
        """
        Ensure GET request from authenticated user can access the
        `FollowUpSetupStep2View` view.
        """
        self.client.force_login(self.user)
        template = 'follow_ups/components/modals/setup/step_2.html'
        response = self.client.get(self.url)

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, template)
        self.assertTrue('formset' in response.context)

    def test_calculate_date_method_with_no_initial_date(self):
        """
        Ensure `calculate_date` static method returns the next Tuesday
        if no initial date is provided as a parameter.
        """
        dt = FollowUpSetupStep2View.calculate_date()
        self.assertEqual(dt.day_of_week, pendulum.TUESDAY)

    def test_calculate_date_method_with_initial_date(self):
        """
        If initial date is provided, ensure `calculate_date` static method
        returns a date that is 12 weeks after the initial date.
        """
        initial_date = pendulum.today(tz=settings.TIME_ZONE)
        dt = FollowUpSetupStep2View.calculate_date(initial_date)
        self.assertEqual(dt, initial_date.add(weeks=12))

    def test_POST_request_with_authenticated_user(self):
        """
        Ensure POST request from authenticated users redirects
        to step 3.
        """
        self.client.force_login(self.user)
        infographic_1 = InfographicFactory()
        dt_1 = '2019-10-30'

        infographic_2 = InfographicFactory()
        dt_2 = '2019-11-30'

        payload = {
            'form-INITIAL_FORMS': '2',
            'form-TOTAL_FORMS': '2',
            'form-MIN_NUM_FORMS': '0',
            'form-MAX-NUM-FORMS': '1000',
            'form-0-infographic': infographic_1.pk.__str__(),
            'form-0-date': '2019-10-30',
            'form-1-infographic': infographic_2.pk.__str__(),
            'form-1-date': '2019-11-30'
        }
        response = self.client.post(self.url, payload, follow=True)
        session_data = self.client.session.get('infographics')
        infographic_pks = [pk for pk, _ in session_data]
        infographic_dts = [dt for _, dt in session_data]
        expected_url = reverse(
            'follow_ups:followup-setup-step3',
            args=[self.follow_up.pk]
        )

        self.assertEqual(response.status_code, 200)
        self.assertRedirects(response, expected_url)
        self.assertEqual(
            infographic_pks,
            [infographic_1.pk.__str__(), infographic_2.pk.__str__()]
        )
        self.assertEqual(infographic_dts, [dt_1, dt_2])


class FollowUpSetupStep3ViewTest(TestCase):
    """
    Tests for `FollowUpSetupStep3View`
    """
    fixtures = ['app_permissions']

    def setUp(self):
        self.user = UserFactory()
        app_permission = AppPermission.objects.get(app_name=GROUP_FOLLOWUPS)
        self.user.profile.app_permissions.add(app_permission)
        self.follow_up = FollowUpFactory()
        self.url = reverse(
            'follow_ups:followup-setup-step3',
            args=[self.follow_up.pk]
        )

    def test_GET_request_with_anonymous_user(self):
        """
        Ensure non-authenticated users cannot access the
        `FollowUpSetupStep3View` view.
        """
        response = self.client.get(self.url, follow=True)
        expected_url = f'{reverse("login")}?next=/feedback/'
        self.assertRedirects(response, expected_url)

    def test_GET_request_as_authenticated_user(self):
        """
        Ensure GET request from authenticated user can access the
        `FollowUpSetupStep3View` view.
        """
        self.client.force_login(self.user)
        template = 'follow_ups/components/modals/setup/step_3.html'
        response = self.client.get(self.url)

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, template)
        self.assertTrue('formset' in response.context)

    def test_POST_request_as_authenticated_user(self):
        """
        Ensure a POST request with authenticated user creates a new
        `FollowUpEvent` instances.
        """
        self.client.force_login(self.user)
        payload = {
            'form-INITIAL_FORMS': '0',
            'form-TOTAL_FORMS': '2',
            'form-MIN_NUM_FORMS': '0',
            'form-MAX-NUM-FORMS': '1000',

            'form-0-follow_up': self.follow_up.pk,
            'form-0-date': '2019-10-30',
            'form-0-type': FollowUpEvent.TYPE_INFOGRAPHIC,
            'form-0-subject': 'Test subject 0',
            'form-0-message': 'Test message 0',

            'form-1-follow_up': self.follow_up.pk,
            'form-1-date': '2019-11-30',
            'form-1-type': FollowUpEvent.TYPE_INFOGRAPHIC,
            'form-1-subject': 'Test subject 1',
            'form-1-message': 'Test message 1'
        }
        response = self.client.post(self.url, payload, follow=True)
        expected_url = reverse('follow_ups:overview')

        self.assertRedirects(response, expected_url)
        self.assertEqual(FollowUpEvent.objects.count(), 2)
        self.assertTrue(FollowUpEvent.objects.filter(subject='Test subject 0'))
        self.assertTrue(FollowUpEvent.objects.filter(subject='Test subject 1'))
