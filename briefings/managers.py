from django.db import models


class BriefingQuestionManager(models.Manager):
    """
    Custom manager to extend the `BriefingQuestion` model default
    manager
    """
    def answered(self):
        return self.exclude(answer='')

    def unanswered(self):
        return self.filter(answer='')
