from django.shortcuts import redirect
from django.views.generic import TemplateView, FormView, DeleteView
from django.urls import reverse_lazy

from debatnl_mobile.forms import OrganizationForm
from debatnl_mobile.mixins import BaseOrganizationView


class OrganizationOverviewView(BaseOrganizationView, TemplateView):
    template_name = 'debatnl_mobile/organization_overview.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if self.connection:
            context['organization_list'] = self.consumer().retrieve_all()
        return context


class OrganizationCreateView(BaseOrganizationView, FormView):
    template_name = 'debatnl_mobile/components/modals/organization_form.html'
    form_class = OrganizationForm
    success_url = reverse_lazy('debatnl_mobile:organization-overview')

    def form_valid(self, form):
        form.save(self.consumer())
        return super().form_valid(form)


class OrganizationUpdateView(BaseOrganizationView, FormView):
    template_name = 'debatnl_mobile/components/modals/organization_form.html'
    form_class = OrganizationForm
    success_url = reverse_lazy('debatnl_mobile:organization-overview')

    def form_valid(self, form):
        organization = self.get_object()
        object_id = organization.get('id')
        form.save(self.consumer(), object_id)
        return super().form_valid(form)

    def get_initial(self):
        return self.get_object()


class OrganizationDeleteView(BaseOrganizationView, DeleteView):
    template_name = 'debatnl_mobile/components/modals/delete.html'
    success_url = reverse_lazy('debatnl_mobile:organization-overview')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['warning_message'] = 'Weet je het zeker dat je deze '\
                                     'organisatie wilt verwijderen?'
        return context

    def delete(self, request, *args, **kwargs):
        pk = self.kwargs.get('pk')
        self.consumer().delete_object(pk)
        return redirect('debatnl_mobile:organization-overview')
