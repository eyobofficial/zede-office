# Google MAP API call status exceptions


class LocationNotFoundError(Exception):
    """
    The requested address could not be found
    """
    pass


class APILimitError(Exception):
    """
    Daily API call limit is reached
    """
    pass
