from django.contrib import messages
from django.contrib.admin.views.decorators import staff_member_required
from django.core.exceptions import ValidationError
from django.shortcuts import redirect
from django.urls import reverse_lazy, reverse
from django.utils.decorators import method_decorator
from django.views import View
from django.views.generic import TemplateView, UpdateView, DetailView, \
    RedirectView

from django.views.generic.detail import SingleObjectMixin

from backoffice.views import BaseEmailView
from shared.models import Infographic, Training
from handouts.emails.reminders import MismatchingDeliveriesNotification, \
    UnknownDeliveriesNotification
from webcrm.models import Person
from .emails.notifications import PageSelectionNotification, \
    NoDeliveryNotification, SecondNoDeliveryNotification, \
    TrainerNotAssignedNotification, HandoutDownloadNotification
from .forms import HandoutPageFormSet, HandoutRequestModelForm
from .mixins import HandoutRequestTokenMixin
from .models import HandoutRequest, HandoutToken, Handout
from .tasks import generate_handout_requests


class HandoutOverviewView(HandoutRequestTokenMixin, UpdateView):
    success_url = reverse_lazy('handouts:handout-create-success')
    template_name = 'handouts/public/handout_overview.html'
    fields = []

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['pages'] = self.get_pages()
        context['is_valid_token'] = self.verify_token()

        if self.request.POST:
            context['formset'] = HandoutPageFormSet(self.request.POST)
        else:
            context['formset'] = HandoutPageFormSet(self.get_object())
        return context

    def get_pages(self):
        obj = self.get_object()  # Handout request
        training = self.request.GET.get('training') or obj.get_training()
        return Infographic.objects.filter(training__name=training)

    def form_valid(self, form):
        handout_request = self.get_object()
        context = self.get_context_data()
        formset = context['formset']

        if formset.is_valid():
            handout = formset.save(handout_request)
            HandoutDownloadNotification(handout).send()
            self.delete_token()
            return super().form_valid(form)

        return super().form_invalid(form)


class HandoutCreateSuccessView(TemplateView):
    template_name = 'handouts/public/create_success.html'


class HandoutUpdateView(HandoutRequestTokenMixin, UpdateView):
    template_name = 'handouts/public/handout_request_update.html'
    form_class = HandoutRequestModelForm
    success_url = reverse_lazy('handouts:handout-update-success')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        qs_kwargs = {'training_deliveries__isnull': False}
        context['trainers'] = Person.objects.filter(**qs_kwargs).distinct()
        context['is_valid_token'] = self.verify_token()
        return context

    def form_valid(self, form):
        redirect_url = super().form_valid(form)
        self.delete_token()
        handout_request = self.get_object()

        # if no related delivery, send notification email
        if not handout_request.set_corresponding_deliveries().exists():
            SecondNoDeliveryNotification(handout_request).send()
        else:
            handout_request.determine_status()
            handout_request.send_notification_based_on_status()

        return redirect_url


class HandoutUpdateSuccessView(TemplateView):
    template_name = 'handouts/public/update_success.html'


class AssignHandoutView(SingleObjectMixin, View):
    """
    Send page selection e-mail for trainees in handout requests
    with unassigned trainer.
    """
    model = HandoutRequest

    def get(self, request, *args, **kwargs):
        handout_request = self.get_object()
        training_name = request.GET.get('training')
        training = Training.objects.filter(name=training_name).first()
        email = PageSelectionNotification(handout_request, training=training)
        email.send()
        return redirect('handouts:handout-create-success')


class MismatchedDeliveryUpdateView(TemplateView):
    template_name = 'handouts/public/mismatched_delivery_overview.html'
    success_url = reverse_lazy('handouts:handout-create-success')

    def get_handout_requests(self):
        token_id = self.request.GET.get('token')
        try:
            token = HandoutToken.objects.get(id=token_id)
            deliveries = token.handout_request.deliveries.all()
            kwargs = {
                'deliveries__in': deliveries,
                'status': HandoutRequest.MISMATCHING_DELIVERIES
            }
            return HandoutRequest.objects.filter(**kwargs).distinct().all()
        except (HandoutToken.DoesNotExist, ValidationError):
            return None

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        handout_requests = self.get_handout_requests()
        context['is_valid_token'] = handout_requests is not None
        context['handout_requests'] = handout_requests
        context['trainings'] = Training.objects.all()
        context['token'] = self.request.GET.get('token')
        return context

    def post(self, request, **kwargs):
        token_id = request.POST.get('token')
        post_data_items = request.POST.items()
        selections = [(k, v) for k, v in post_data_items if 'handout-' in k]
        HandoutToken.objects.filter(id=token_id).delete()

        for key, value in selections:
            _, pk = key.split('handout-')
            h_request = HandoutRequest.objects.get(pk=pk)
            training = Training.objects.get(pk=value)
            PageSelectionNotification(h_request, training=training).send()
            h_request.status = HandoutRequest.HANDOUT_SELECTION
            h_request.save()

        return redirect(self.success_url)


class HandoutDownloadView(DetailView):
    model = Handout
    template_name = 'handouts/public/handout_download.html'


class UnknownDeliveryUpdateView(RedirectView):
    template_name = 'handouts/public/unknown_delivery_overview.html'
    success_url = reverse_lazy('handouts:handout-create-success')

    def get_redirect_url(self):
        token_id = self.request.GET.get('token')
        training_name = self.request.GET.get('training')
        training = Training.objects.filter(name=training_name).first()

        try:
            token = HandoutToken.objects.get(id=token_id)
        except (HandoutToken.DoesNotExist, ValidationError):
            token = None

        if token and training:
            kwargs = {
                'deliveries': token.delivery,
                'status': HandoutRequest.UNKNOWN_DELIVERIES
            }
            h_requests = HandoutRequest.objects.filter(**kwargs).all()
            self.send_page_selection_notifications(h_requests, training)
            token.delete()
            return reverse('handouts:handout-create-success')

        return reverse('handouts:invalid-token')

    @staticmethod
    def send_page_selection_notifications(handout_requests, training):
        for h_request in handout_requests:
            PageSelectionNotification(h_request, training=training).send()
            h_request.status = HandoutRequest.HANDOUT_SELECTION
            h_request.save()


class InvalidTokenView(TemplateView):
    template_name = 'handouts/public/invalid_token.html'


@method_decorator(staff_member_required, name='dispatch')
class GenerateHandoutRequests(View):
    def get(self, request, *args, **kwargs):
        generate_handout_requests()
        messages.success(request, 'Handout requests have been generated')
        return redirect('admin:handouts_handoutrequest_changelist')


class BaseHandoutRequestEmailView(BaseEmailView):
    url = 'admin:handouts_handoutrequest_change'
    model = HandoutRequest


class BaseHandoutEmailView(BaseEmailView):
    url = 'admin:handouts_handoutrequest_change'
    model = Handout


class PageSelectionEmailView(BaseHandoutRequestEmailView):
    email_class = PageSelectionNotification
    email_name = 'handout page selection notification'


class NoDeliveryByTrainingEmailView(BaseHandoutRequestEmailView):
    email_class = NoDeliveryNotification
    email_name = 'no corresponding delivery by training notification'


class SecondNoDeliveryByTrainingEmailView(BaseHandoutRequestEmailView):
    email_class = SecondNoDeliveryNotification
    email_name = 'second no corresponding delivery by training notification'


class TrainerNotAssignedEmailView(BaseHandoutRequestEmailView):
    email_class = TrainerNotAssignedNotification
    email_name = 'trainer not assigned notification'


class MismatchedDeliveriesEmailView(BaseHandoutRequestEmailView):
    email_class = MismatchingDeliveriesNotification
    email_name = 'mismatched deliveries notification'


class HandoutDownloadEmailView(BaseHandoutEmailView):
    email_class = HandoutDownloadNotification
    email_name = 'handout download notification'

    def get(self, request, *args, **kwargs):
        handout = self.get_object()
        self.email_class(handout).send()
        message = f'The {self.email_name} e-mail has been sent.'
        messages.success(request, message)
        return redirect(reverse(self.url, args=[handout.handout_request.pk]))


class UnknownDeliveriesEmailView(BaseHandoutRequestEmailView):
    email_class = UnknownDeliveriesNotification
    email_name = 'unknown deliveries notification'

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()

        for delivery in self.object.deliveries.all():
            self.email_class(delivery).send()

        message = f'The {self.email_name} e-mails have been sent.'
        messages.success(request, message)
        return redirect(reverse(self.url, args=(self.object.pk,)))
