import base64

from django.conf import settings
from django.contrib.auth.models import User
from django.http import HttpResponseRedirect
from django.urls import reverse_lazy
from django.utils.dateparse import parse_datetime
from django.views.generic import TemplateView, FormView

from backoffice.constants import GROUP_DEBATNLONLINE
from backoffice.mixins import GroupAccessMixin
from debatnl_online.consumers.learning_goals.organizations import \
    OrganizationConsumer
from debatnl_online.forms.learning_goals import OrganizationForm, \
    WelcomeMessageForm
from shared.enums import RoleName


class OrganizationBaseView(GroupAccessMixin, FormView):
    form_class = OrganizationForm
    base_template = 'debatnl_online/components/learning_goals/organizations/{}'
    template_name = base_template.format('modals/modal-organization.html')
    success_url = reverse_lazy('debatnl_online:learning_goals:overview')
    consumer = OrganizationConsumer()
    access_groups = [GROUP_DEBATNLONLINE]


class OrganizationCreate(OrganizationBaseView):
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'modal_title': 'OPDRACHTGEVER TOEVOEGEN',
            'button_text': 'Toevoegen'
        })
        return context

    def form_valid(self, form):
        self.consumer.create_object(form.cleaned_data)
        return super().form_valid(form)


class OrganizationUpdate(OrganizationBaseView):
    def get_context_data(self, **kwargs):
        pk = self.kwargs['organization']
        self.initial = self.consumer.retrieve_object(pk)

        kwargs.update({
            'modal_title': 'OPDRACHTGEVER WIJZIGEN',
            'button_text': 'Wijzigen'
        })

        return super().get_context_data(**kwargs)

    def form_valid(self, form):
        self.consumer.update_object(form.cleaned_data['id'], form.cleaned_data)
        return super().form_valid(form)


class OrganizationDelete(GroupAccessMixin, TemplateView):
    base_template = 'debatnl_online/components/learning_goals/modals/{}'
    template_name = base_template.format('modal-delete.html')
    success_url = reverse_lazy('debatnl_online:learning_goals:overview')
    access_groups = [GROUP_DEBATNLONLINE]

    def get_context_data(self, **kwargs):
        kwargs.update({
            'modal_title': 'OPDRACHTGEVER',
            'modal_description': 'de opdrachtgever',
        })

        return super().get_context_data(**kwargs)

    def post(self, request, organization):
        consumer = OrganizationConsumer()
        consumer.delete_object(organization)
        return HttpResponseRedirect(self.success_url)


class OrganizationDetails(GroupAccessMixin, TemplateView):
    base_template = 'debatnl_online/components/learning_goals/organizations/{}'
    template_name = base_template.format('overview.html')
    access_groups = [GROUP_DEBATNLONLINE]

    def get_context_data(self, **kwargs):
        consumer = OrganizationConsumer()
        organization = consumer.retrieve_object(self.kwargs['organization'])
        organization['goals'] = self.remove_user_own_goals(
            organization['goals'])

        kwargs['organization'] = organization
        kwargs['sender'] = User.objects.filter(
            profile__roles__name=RoleName.ADMIN.value,
            is_active=True).first()

        for template in organization['email_templates']:
            template_type = template['type'].lower()
            template_role = template['role'].lower()
            key = f'{template_type}_{template_role}_template'
            kwargs[key] = template

        participants = organization['participants']
        self.convert_to_datetime('last_retrieved_at', participants)
        self.convert_to_datetime('last_retrieved_at', organization['managers'])
        self.add_dashboard_url(participants)

        return super().get_context_data(**kwargs)

    @staticmethod
    def convert_to_datetime(key, persons):
        for person in persons:
            if key in person and person[key]:
                dt = person[key]
                person[key] = parse_datetime(dt)

    @staticmethod
    def add_dashboard_url(persons):
        token = settings.DEBATNL_ONLINE_ADMIN_KEY.encode('utf-8')
        token = base64.urlsafe_b64encode(token).decode()
        host = settings.FRONTEND_URL

        for person in persons:
            p_id = person['id']
            url = f'{host}/token-redirect?token={token}'
            url = f'{url}&next=/learning-goals/admin/participants/{ p_id }'
            person['dashboard_url'] = url

    @staticmethod
    def remove_user_own_goals(goals):
        new_goals = []

        for goal in goals:
            if not goal['user_created']:
                new_goals.append(goal)

        return new_goals


class OrganizationMessageBaseView(GroupAccessMixin, FormView):
    form_class = WelcomeMessageForm
    base_template = 'debatnl_online/components/learning_goals/organizations/{}'
    template_name = base_template.format('modals/modal-message.html')
    consumer = OrganizationConsumer()
    access_groups = [GROUP_DEBATNLONLINE]

    def get_success_url(self):
        self.success_url = reverse_lazy(
            'debatnl_online:learning_goals:organizations:details',
            kwargs={'organization': self.kwargs['organization']}
        )
        return super().get_success_url()

    def form_valid(self, form):
        data = {'welcome_message': form.cleaned_data['welcome_message']}
        self.consumer.update_object(self.kwargs['organization'], data)
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        pk = self.kwargs['organization']
        self.initial = self.consumer.retrieve_object(pk)
        return super().get_context_data(**kwargs)


class OrganizationMessageCreate(OrganizationMessageBaseView):
    def get_context_data(self, **kwargs):
        kwargs.update({
            'modal_title': 'MELDING TOEVOEGEN',
            'button_text': 'Toevoegen'
        })
        return super().get_context_data(**kwargs)


class OrganizationMessageUpdate(OrganizationMessageBaseView):
    def get_context_data(self, **kwargs):
        kwargs.update({
            'modal_title': 'MELDING WIJZIGEN',
            'button_text': 'Wijzigen'
        })
        return super().get_context_data(**kwargs)


class OrganizationMessageDelete(GroupAccessMixin, TemplateView):
    template_name = 'debatnl_online/modals/modal-delete.html'
    access_groups = [GROUP_DEBATNLONLINE]

    def get_context_data(self, **kwargs):
        kwargs.update({
            'modal_title': 'MELDING',
            'modal_description': 'de melding',
        })

        return super().get_context_data(**kwargs)

    def post(self, request, organization):
        consumer = OrganizationConsumer()
        consumer.update_object(organization, {'welcome_message': ''})

        success_url = reverse_lazy(
            'debatnl_online:learning_goals:organizations:details',
            kwargs={'organization': self.kwargs['organization']}
        )
        return HttpResponseRedirect(success_url)
