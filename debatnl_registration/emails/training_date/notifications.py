from django.conf import settings
from django.contrib.auth.models import User
from django.urls import reverse

from debatnl_registration.emails.bases import TrainingDateBaseEmail
from debatnl_registration.models import PlannedTrainingToken
from shared.enums import RoleName


class TrainingConfirmationHallEmail(TrainingDateBaseEmail):
    template_name = 'email_21_training_confirmation_hall_contact.html'

    def __init__(self, training_date):
        super().__init__(training_date)
        name = training_date.planned_training.training.name
        date = training_date.date
        self.subject = f'Details training {name} van {date:%d-%m-%Y}'
        self.recipient = User.objects.filter(
            profile__roles__name=RoleName.HALL.value,
            is_active=True
        ).first()


class AttendeesListNotificationEmail(TrainingDateBaseEmail):
    template_name = 'email_22_attendees_list_notification.html'

    def __init__(self, training_date):
        super().__init__(training_date)
        name = training_date.planned_training.training.name
        date = training_date.date
        self.subject = f'Details training {name} van {date:%d-%m-%Y}'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        token = PlannedTrainingToken.objects.create(
            planned_training=self.training_date.planned_training,
            training_date=self.training_date,
            type=PlannedTrainingToken.ATTENDANCE_LIST
        )
        uri = reverse('debatnl_registration:confirm_attendance_list')
        url = f'{settings.APP_HOSTNAME}{uri}?token={str(token.key)}'
        context.update({'button_url': url})
        return context

    def send(self):
        for trainer in self.training_date.trainers.all():
            self.recipient = trainer
            super().send()


class AttendeesAmountNotificationEmail(TrainingDateBaseEmail):
    template_name = 'email_24_25_attendees_amount.html'

    def __init__(self, training_date):
        super().__init__(training_date)
        name = training_date.planned_training.training.name
        count = training_date.attendees.count()
        self.subject = f'{count} deelnemers voor {name}'
        self.recipient = User.objects.filter(
            profile__roles__name=RoleName.ADMIN.value,
            is_active=True
        ).first()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        token = PlannedTrainingToken.objects.create(
            planned_training=self.training_date.planned_training,
            training_date=self.training_date,
            type=PlannedTrainingToken.ATTENDEES_AMOUNT
        )
        uri = reverse('debatnl_registration:confirm_attendees_amount')
        url = f'{settings.APP_HOSTNAME}{uri}?token={str(token.key)}'
        context.update({'button_url': url})
        return context


class TrainingInformationNotificationEmail(TrainingDateBaseEmail):
    template_name = 'email_26_training_information.html'

    def __init__(self, training_date):
        super().__init__(training_date)
        name = training_date.planned_training.training.name
        date = training_date.date
        self.subject = f'Aantal deelnemers training {name} {date:%d-%m-%Y}'
        self.recipient = User.objects.filter(
            profile__roles__name=RoleName.ADMIN.value,
            is_active=True
        ).first()
