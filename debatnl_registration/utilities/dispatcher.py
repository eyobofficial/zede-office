import datetime

import pytz

from debatnl_registration.emails.participant.notifications import \
    RegistrationConfirmationEmail
from debatnl_registration.utilities.helpers import persist_registration
from debatnl_registration.utilities.parsers import \
    parse_training_registration, \
    parse_2day_additional_training_registration
from debatnl_registration.utilities.validators import validate_registration


def gravity_form_dispatcher(post_data):
    data = None
    form_id = int(post_data.get('entry[form_id]'))

    if form_id == 39:
        # Parse the registration for 1 day or 3 days training.
        data = parse_training_registration(post_data)

    if form_id == 45:
        # Parse the registration for the additional 2 day training.
        data = parse_2day_additional_training_registration(post_data)

    if data:
        # Persist the registration data.
        registration = persist_registration(data)

        # Validate the registration.
        if validate_registration(registration):
            registration.is_valid = True
            registration.save()

            registration.refresh_from_db()

            # Send the confirmation to the registrant.
            for participant in registration.participants.all():
                attendance = participant.attendance_set.order_by(
                    'training_date__date'
                ).first()

                now = datetime.datetime.now(
                    tz=pytz.timezone('Europe/Amsterdam')
                )
                delta = now.date() - attendance.training_date.date

                email = RegistrationConfirmationEmail(
                    participant,
                    with_travel_directions=delta.days < 7
                )
                email.send()
