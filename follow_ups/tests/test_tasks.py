from datetime import date

import mock
import pendulum
from django.conf import settings
from django.test import TestCase
from post_office.models import Email

from follow_ups.models import FollowUp, FollowUpEvent
from follow_ups.tasks import send_opportunity_emails, \
    send_follow_up_event_emails, send_no_follow_up_event_setup
from follow_ups.tests.factories import FollowUpFactory, FollowUpEventFactory
from shared.enums import RoleName
from users.models import Role
from users.tests.factories import UserFactory
from webcrm.factories import OpportunityFactory
from webcrm.models import Opportunity


class SendOpportunityEmailsTests(TestCase):
    """
    Tests for the `send_opportunity_emails` tasks
    """
    fixtures = ['roles']

    def setUp(self):
        kwargs = {'level_text': '[1] Automatische opvolging'}
        self.opportunity = OpportunityFactory(**kwargs)

        role = Role.objects.get(name=RoleName.ADMIN.value)
        user = UserFactory()
        user.profile.roles.add(role)

    def test_task_with_opportunity_and_related_follow_up(self):
        """
        If an opportunity instance with a related follow_up exists,
        ensure task does not create a new instance follow_up for that
        opportunity.
        """
        follow_up = FollowUpFactory(opportunity=self.opportunity)

        # Run the task
        send_opportunity_emails()

        # Assertions
        self.assertEqual(FollowUp.objects.count(), 1)
        self.assertEqual(follow_up.status, FollowUp.NEW)

    def test_task_with_an_existing_ready_follow_up(self):
        """
        If an opportunity instance with both `assigned_to` & `person`
        values exists, ensure e-mail does not get send on the next run.
        """
        kwargs = {'opportunity': self.opportunity, 'status': FollowUp.READY}
        FollowUpFactory(**kwargs)

        # Run the task
        send_opportunity_emails()
        follow_up = FollowUp.objects.first()

        # Assertions
        self.assertEqual(FollowUp.objects.count(), 1)
        self.assertEqual(follow_up.status, FollowUp.READY)

    def test_task_with_an_existing_deleted_follow_up(self):
        """
        If an opportunity instance with both `assigned_to` & `person`
        values exists, ensure e-mail does not get send on the next run.
        """
        kwargs = {'opportunity': self.opportunity, 'status': FollowUp.DELETED}
        FollowUpFactory(**kwargs)

        # Run the task
        send_opportunity_emails()
        follow_up = FollowUp.objects.first()

        # Assertions
        self.assertEqual(FollowUp.objects.count(), 1)
        self.assertEqual(follow_up.status, FollowUp.DELETED)

    def test_task_with_opportunity_with_incorrect_level_text(self):
        """
        If an opportunity instance with no related follow_up exists,
        ensure task create a new instance follow_up for that
        opportunity.
        """
        OpportunityFactory(level_text='abcde')

        # Run the task
        send_opportunity_emails()

        # Assertions
        self.assertEqual(FollowUp.objects.count(), 1)

    def test_task_with_related_opportunity_and_no_assigned_to(self):
        """
        If an opportunity instance with no `assigned_to` value
        exists, ensure task updates the related follow_up status
        to `NO_SALES_RESPONSIBLE`.
        """
        self.opportunity.assigned_to = None
        self.opportunity.save()

        # Run the task
        send_opportunity_emails()
        follow_up = FollowUp.objects.first()

        # Assertions
        self.assertEqual(FollowUp.objects.count(), 1)
        self.assertEqual(follow_up.status, FollowUp.NO_SALES_RESPONSIBLE)

    def test_task_with_an_existing_no_sales_resp_follow_up(self):
        """
        If an opportunity instance with a NO_SALES_RESPONSIBLE follow up
        exists, ensure e-mail does send on the next run.
        """
        self.opportunity.assigned_to = None
        self.opportunity.save()

        kwargs = {
            'opportunity': self.opportunity,
            'status': FollowUp.NO_SALES_RESPONSIBLE
        }
        FollowUpFactory(**kwargs)

        # Run the task
        send_opportunity_emails()
        follow_up = FollowUp.objects.first()

        # Assertions
        self.assertEqual(FollowUp.objects.count(), 1)
        self.assertEqual(follow_up.status, FollowUp.NO_SALES_RESPONSIBLE)

    def test_task_with_related_opportunity_and_no_person(self):
        """
        If an opportunity instance with no `person` value
        exists, ensure task updates the related follow_up status
        to `NO_CUSTOMER_CONTACT`.
        """
        self.opportunity.person = None
        self.opportunity.save()

        # Run the task
        send_opportunity_emails()
        follow_up = FollowUp.objects.first()

        # Assertions
        self.assertEqual(FollowUp.objects.count(), 1)
        self.assertEqual(follow_up.status, FollowUp.NO_CUSTOMER_CONTACT)

    def test_task_with_an_existing_no_person_follow_up(self):
        """
        If an opportunity instance with a NO_SALES_RESPONSIBLE follow up
        exists, ensure e-mail does send on the next run.
        """
        self.opportunity.person = None
        self.opportunity.save()

        kwargs = {
            'opportunity': self.opportunity,
            'status': FollowUp.NO_CUSTOMER_CONTACT
        }
        FollowUpFactory(**kwargs)

        # Run the task
        send_opportunity_emails()
        follow_up = FollowUp.objects.first()

        # Assertions
        self.assertEqual(FollowUp.objects.count(), 1)
        self.assertEqual(follow_up.status, FollowUp.NO_CUSTOMER_CONTACT)

    def test_task_with_opportunity_with_assigned_to_and_person(self):
        """
        If an opportunity instance with both `assigned_to` & `person`
        values exists, ensure task updates the related follow_up status
        to `READY`.
        """
        # Run the task
        send_opportunity_emails()
        follow_up = FollowUp.objects.first()

        # Assertions
        self.assertEqual(FollowUp.objects.count(), 1)
        self.assertEqual(follow_up.status, FollowUp.READY)

    def test_task_with_changed_deleted_opportunity(self):
        """
        If an opportunity has the state DELETED, ensure the follow up gets
        deleted.
        """
        self.opportunity.state = Opportunity.STATE_DELETED
        self.opportunity.save()

        kwargs = {
            'opportunity': self.opportunity,
            'status': FollowUp.READY
        }
        FollowUpFactory(**kwargs)

        self.assertEqual(FollowUp.objects.count(), 1)

        # Run the task
        send_opportunity_emails()

        # Assertions
        self.assertEqual(FollowUp.objects.count(), 0)

    def test_task_with_changed_opportunity_level(self):
        """
        If an opportunity has a changed level, ensure the follow up gets
        deleted.
        """
        FollowUpFactory(opportunity=self.opportunity)
        self.opportunity.level_text = '[2] Random level'
        self.opportunity.save()

        self.assertEqual(FollowUp.objects.count(), 1)

        # Run the task
        send_opportunity_emails()

        # Assertions
        self.assertEqual(FollowUp.objects.count(), 0)


class SendFollowUpEventEmailsTests(TestCase):
    """
    Tests for the `send_follow_up_event_emails` tasks
    """
    fixtures = ['roles']

    def test_task_with_multiple_events(self):
        role = Role.objects.get(name=RoleName.ADMIN.value)
        user = UserFactory()
        user.profile.roles.add(role)

        FollowUpEventFactory(status=FollowUpEvent.SENT)
        FollowUpEventFactory(date=date(2019, 6, 1))
        FollowUpEventFactory(date=date(2019, 6, 30))
        FollowUpEventFactory(date=date(2019, 6, 24), status=FollowUpEvent.SENT)
        event = FollowUpEventFactory(date=date(2019, 6, 24))

        mock_dt = pendulum.datetime(2019, 6, 24, tz=settings.TIME_ZONE)
        with pendulum.test(mock_dt):
            send_follow_up_event_emails()

        event.refresh_from_db()
        self.assertEqual(Email.objects.count(), 1)
        self.assertEqual(event.status, FollowUpEvent.SENT)


class SendNoEventSetupEmailTaskTests(TestCase):
    """
    Tests for `send_no_follow_up_event_setup` task
    """

    @mock.patch('follow_ups.emails.notifications.NoFollowUpEventSetupEmail.'
                'send')
    def test_task_with_follow_ups_with_follow_events(self, mock_obj):
        """
        Ensure task does not send notification e-mail if the follow_ups have
        event setups.
        """
        follow_up_1 = FollowUpFactory(opportunity=None)
        FollowUpEventFactory(follow_up=follow_up_1)

        follow_up_2 = FollowUpFactory(opportunity=None)
        FollowUpEventFactory(follow_up=follow_up_2)

        send_no_follow_up_event_setup()
        self.assertEqual(mock_obj.call_count, 0)

    @mock.patch('follow_ups.emails.notifications.NoFollowUpEventSetupEmail.'
                'send')
    def test_task_with_no_follow_up_event_and_no_sales_responsible(self,
                                                                   mock_obj):
        """
        Ensure task does not send notification e-mail if the follow_ups have
        no event setups and no sales responsilbe person assigned to.
        """
        FollowUpFactory(opportunity=None)
        FollowUpFactory(opportunity=None)

        send_no_follow_up_event_setup()
        self.assertEqual(mock_obj.call_count, 0)

    @mock.patch('follow_ups.emails.notifications.NoFollowUpEventSetupEmail.'
                'send')
    def test_task_with_follow_up_event_and_sales_responsible(self, mock_obj):
        """
        Ensure task sends notification e-mail if the follow_ups have a sales
        responsible assigned and have no related follow_up events.
        """
        FollowUpFactory()
        FollowUpFactory()
        send_no_follow_up_event_setup()

        self.assertEqual(mock_obj.call_count, 2)
