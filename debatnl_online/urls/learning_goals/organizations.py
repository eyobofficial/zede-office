from django.conf.urls import url
from django.contrib.auth.decorators import login_required

from debatnl_online.views.learning_goals.organizations import \
    OrganizationCreate, OrganizationUpdate, OrganizationDelete, \
    OrganizationDetails, OrganizationMessageCreate, OrganizationMessageUpdate, \
    OrganizationMessageDelete

urlpatterns = [
    url(
        r'^create/$',
        login_required(OrganizationCreate.as_view()),
        name='create'
    ),
    url(
        r'^update/(?P<organization>[-\w]+)/$',
        login_required(OrganizationUpdate.as_view()),
        name='update'
    ),
    url(
        r'^delete/(?P<organization>[-\w]+)/$',
        login_required(OrganizationDelete.as_view()),
        name='delete'
    ),
    url(
        r'^(?P<organization>[-\w]+)/$',
        login_required(OrganizationDetails.as_view()),
        name='details'
    ),
    url(
        r'^(?P<organization>[-\w]+)/message/create/$',
        login_required(OrganizationMessageCreate.as_view()),
        name='create_message'
    ),
    url(
        r'^(?P<organization>[-\w]+)/message/update/$',
        login_required(OrganizationMessageUpdate.as_view()),
        name='update_message'
    ),
    url(
        r'^(?P<organization>[-\w]+)/message/delete/$',
        login_required(OrganizationMessageDelete.as_view()),
        name='delete_message'
    ),
]
