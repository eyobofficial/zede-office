from django.conf import settings
from django.http import HttpResponseRedirect
from django.urls import reverse_lazy
from django.views.generic import TemplateView, FormView

from backoffice.constants import GROUP_DEBATNLONLINE
from backoffice.mixins import GroupAccessMixin
from debatnl_online.consumers.learning_goals.organizations import \
    OrganizationGoalConsumer
from debatnl_online.consumers.vimeo import VimeoConsumer
from debatnl_online.forms.learning_goals import GoalForm


class GoalBaseView(GroupAccessMixin, FormView):
    form_class = GoalForm
    base_template = 'debatnl_online/components/learning_goals/goals/{}'
    template_name = base_template.format('modals/modal-goal.html')
    consumer = None
    access_groups = [GROUP_DEBATNLONLINE]

    def dispatch(self, request, *args, **kwargs):
        self.consumer = OrganizationGoalConsumer(self.kwargs['organization'])
        return super().dispatch(request, *args, **kwargs)

    def get_success_url(self):
        self.success_url = reverse_lazy(
            'debatnl_online:learning_goals:organizations:details',
            kwargs={'organization': self.kwargs['organization']}
        )
        return super().get_success_url()

    def get_initial(self):
        self.initial['organization'] = self.kwargs['organization']
        return super().get_initial()

    @staticmethod
    def request_vimeo_pull(video_url):
        url = 'https://s3.eu-central-1.amazonaws.com/{}/{}'.format(
            settings.AWS_S3_BUCKET, video_url
        )
        url = url.replace(' ', '+')

        vimeo_consumer = VimeoConsumer()
        video = vimeo_consumer.request_pull(url)
        vimeo_consumer.assign_preset_to_video(video.get('uri'))
        vimeo_consumer.set_allowed_domain(video.get('uri'),
                                          settings.VIMEO_ALLOWED_DOMAIN)
        return video['uri'].split('/')[-1]


class GoalCreate(GoalBaseView):
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'modal_title': 'LEERDOEL TOEVOEGEN',
            'button_text': 'Toevoegen'
        })
        return context

    def form_valid(self, form):
        data = form.cleaned_data.copy()
        video_url = data.get('video_url')

        if video_url:
            data['vimeo_video_id'] = self.request_vimeo_pull(video_url)

        self.consumer.create_object(data)
        return super().form_valid(form)


class GoalUpdate(GoalBaseView):
    def get_context_data(self, **kwargs):
        self.initial = self.consumer.retrieve_object(self.kwargs['pk'])

        kwargs.update({
            'modal_title': 'LEERDOEL WIJZIGEN',
            'button_text': 'Wijzigen'
        })

        return super().get_context_data(**kwargs)

    def form_valid(self, form):
        data = form.cleaned_data.copy()
        video_url = data.get('video_url')
        video_id = data.get('vimeo_video_id')

        if (not video_id and video_url) or \
                (video_id and video_url != form.initial.get('video_url')):
            data['vimeo_video_id'] = self.request_vimeo_pull(video_url)

        self.consumer.update_object(data['id'], data)
        return super().form_valid(form)


class GoalDelete(GroupAccessMixin, TemplateView):
    template_name = 'debatnl_online/modals/modal-delete.html'
    access_groups = [GROUP_DEBATNLONLINE]

    def get_context_data(self, **kwargs):
        kwargs.update({
            'modal_title': 'LEERDOEL',
            'modal_description': 'het leerdoel',
        })

        return super().get_context_data(**kwargs)

    def post(self, request, organization, pk):
        consumer = OrganizationGoalConsumer(organization)
        consumer.delete_object(pk)

        success_url = reverse_lazy(
            'debatnl_online:learning_goals:organizations:details',
            kwargs={'organization': organization}
        )

        return HttpResponseRedirect(success_url)
