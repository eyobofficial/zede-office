# -*- coding: utf-8 -*-
# Generated by Django 1.11.20 on 2019-04-18 07:38
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ccd', '0007_auto_20181028_1312'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='persondeliverable',
            name='person',
        ),
        migrations.RemoveField(
            model_name='person',
            name='webcrm_id',
        ),
        migrations.DeleteModel(
            name='PersonDeliverable',
        ),
    ]
