import uuid
from django.db import models
from django.utils.translation import ugettext_lazy as _


class JotFormTrainer(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    first_name = models.CharField(max_length=200, blank=True, default='')
    last_name = models.TextField(max_length=200, blank=True, default='')

    class Meta:
        verbose_name = _('Trainer')
        verbose_name_plural = _('Trainers')
        ordering = ('first_name', 'last_name')
        permissions = (
            ('view_all_trainers', 'Can see all trainers'),
        )

    def __str__(self):
        return self.get_full_name()

    def get_full_name(self):
        return f'{self.first_name} {self.last_name}'.strip()


class JotFormTraining(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    date = models.DateField(null=True)
    trainer = models.ForeignKey(JotFormTrainer, on_delete=models.CASCADE)

    class Meta:
        verbose_name = _('Training')
        verbose_name_plural = _('Trainings')
        ordering = ('date',)

    def has_outtake(self):
        name = self.trainer.get_full_name()
        return TrainingOuttake.objects.filter(training_date=self.date,
                                              trainer_name=name).exists()

    def get_average_score(self):
        total_score = 0
        amount_submissions = 0

        for values in self.reviews.values('score'):
            score = values.get('score')
            if score:
                total_score += score
                amount_submissions += 1

        if amount_submissions > 0:
            return total_score / amount_submissions

        return 0


class JotFormTrainingReview(models.Model):
    training_review_id = models.UUIDField(primary_key=True, default=uuid.uuid4,
                                          editable=False)
    training = models.ForeignKey(JotFormTraining, null=True,
                                 on_delete=models.CASCADE,
                                 related_name='reviews')
    reviewer_name = models.CharField(max_length=200, blank=True, default='')
    reviewer_email = models.CharField(max_length=200, blank=True, default='')
    score = models.IntegerField(null=True)
    comment = models.TextField(blank=True, default='')
    resolution = models.TextField(blank=True, default='')
    submission_date = models.DateField(null=True)

    class Meta:
        verbose_name = _('Training review')
        verbose_name_plural = _('Training reviews')


class TrainingOuttake(models.Model):
    STARTED = 'STARTED'
    STOPPED = 'STOPPED'

    STATUS_CHOICES = (
        (STARTED, _('Started')),
        (STOPPED, _('Stopped')),
    )

    training_date = models.DateField(null=True, db_index=True)
    trainer_name = models.CharField(max_length=200, blank=True, default='')
    status = models.CharField(max_length=50, blank=True, default=STARTED,
                              choices=STATUS_CHOICES)

    class Meta:
        index_together = ['training_date', 'trainer_name']


class TrainingOuttakeSubscriber(models.Model):
    SUBSCRIBED = 'SUBSCRIBED'
    UNSUBSCRIBED = 'UNSUBSCRIBED'

    STATUS_CHOICES = (
        (SUBSCRIBED, _('Subscribed')),
        (UNSUBSCRIBED, _('Unsubscribed')),
    )

    subscriber_id = models.UUIDField(primary_key=True, default=uuid.uuid4,
                                     editable=False)
    outtake = models.ForeignKey(TrainingOuttake, on_delete=models.CASCADE,
                                null=True, related_name='subscribers')
    name = models.CharField(max_length=200, blank=True, default='')
    email = models.CharField(max_length=200, blank=True, default='')
    resolution = models.TextField(blank=True, default='')
    training_date = models.DateField(null=True)
    subscription_date = models.DateTimeField(null=True)
    status = models.CharField(max_length=50, blank=True, default=SUBSCRIBED,
                              choices=STATUS_CHOICES)

    class Meta:
        index_together = ['name', 'email', 'training_date']

    def __str__(self):
        return '{} (id: {})'.format(self.name, self.subscriber_id)


class TrainingOuttakeNotification(models.Model):
    INTRO = 'INTRO'
    REMINDER = 'REMINDER'

    NOTIFICATION_TYPE_CHOICES = (
        (INTRO, _('Intro')),
        (REMINDER, _('Reminder')),
    )

    notification_type = models.CharField(max_length=50, blank=True, default='',
                                         choices=NOTIFICATION_TYPE_CHOICES)
    notification_date = models.DateField(db_index=True)
    subscriber = models.ForeignKey(TrainingOuttakeSubscriber,
                                   on_delete=models.CASCADE,
                                   related_name='notifications')
