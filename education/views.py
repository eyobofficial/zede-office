from django.contrib.auth.models import User
from django.http import JsonResponse
from django.urls import reverse_lazy
from django.views.generic import ListView, CreateView, UpdateView

from backoffice.constants import GROUP_EDUCATION
from education.forms import LearningGoalForm, LearningGoalFinishedBy
from education.models import LearningGoal
from shared.enums import RoleName


class OverviewView(ListView):
    model = LearningGoal
    access_groups = [GROUP_EDUCATION]
    users = User.objects.filter(
        profile__roles__name=RoleName.TRAINER.value,
        is_active=True
    ).order_by('first_name', 'last_name')

    def get_context_data(self, **kwargs):
        kwargs['users'] = self.users
        kwargs['selected_user'] = self.get_user()
        return super().get_context_data(**kwargs)

    def get_queryset(self):
        queryset = super().get_queryset()
        user = self.get_user()
        return queryset.filter(position=user.profile.position)

    def get_user(self):
        user_id = self.request.GET.get('user')
        user = self.request.user

        permission_name = 'education.view_all_learning_goals'
        has_view_permission = user.has_perm(permission_name)

        if has_view_permission and user_id:
            user = User.objects.get(pk=user_id)

        elif has_view_permission and self.users.exists():
            user = self.users.first()

        return user


class LearningGoalCreateView(CreateView):
    model = LearningGoal
    form_class = LearningGoalForm
    access_groups = [GROUP_EDUCATION]
    success_url = reverse_lazy('education:overview')

    def get_success_url(self):
        success_url = super().get_success_url()
        success_url = f'{success_url}?{self.request.GET.urlencode()}'
        return success_url


class LearningGoalUpdateView(LearningGoalCreateView, UpdateView):
    pass


class LearningGoalFinishedByUpdateView(UpdateView):
    model = LearningGoal
    form_class = LearningGoalFinishedBy
    access_groups = [GROUP_EDUCATION]
    success_url = reverse_lazy('education:overview')

    def form_valid(self, form):
        super().form_valid(form)
        return JsonResponse(data={})

    def form_invalid(self, form):
        super().form_valid(form)
        return JsonResponse(data={}, status=400)
