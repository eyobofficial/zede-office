from backoffice.tests.bases import BaseEmailTestCase
from backoffice.tests.factories import TrainingFactory
from debatnl_registration.emails.attendance.notifications import \
    ExtendInvitationNotificationEmail, CertificateEmail
from debatnl_registration.emails.participant.notifications import \
    RegistrationConfirmationEmail, ReviewTrainingNotificationEmail, \
    TrainingContinuationNotificationEmail, ParticipantCancellationChoiceEmail
from debatnl_registration.emails.planned_training.notifications import \
    BookHallNotificationEmail, CancelHallNotificationEmail, \
    HallNotAvailableToAdminEmail, HallNotAvailableToTrainersEmail, \
    HallAvailableNotificationEmail, UpdateWebsiteNotificationEmail, \
    CancelTrainingToParticipantsEmail, CancelTrainingToTrainersEmail
from debatnl_registration.emails.reminders import TravelDirectionReminderEmail, \
    BookHallReminderEmail, CancelHallReminderEmail, HallAvailableReminderEmail, \
    UpdateWebsiteReminderEmail, AttendeesListReminderEmail, \
    AttendeesAmountReminderEmail
from debatnl_registration.emails.training_date.notifications import \
    TrainingConfirmationHallEmail, AttendeesListNotificationEmail, \
    AttendeesAmountNotificationEmail, TrainingInformationNotificationEmail
from debatnl_registration.models import TrainingDate
from debatnl_registration.tests.factories import AttendanceFactory, \
    TrainingDateFactory, ParticipantFactory, RegistrationFactory, \
    PlannedTrainingTokenFactory, PlannedTrainingFactory
from shared.enums import RoleName
from users.models import Role
from users.tests.factories import UserFactory


class AttendanceEmailTest(BaseEmailTestCase):
    template_dir = '../templates/debatnl_registration/emails/'
    obj_context_name = 'attendance'

    fixtures = ['roles']

    def setUp(self):
        super().setUp()
        self.training = TrainingFactory(name='Bewust Beinvloeden')
        kwargs = {'planned_training__training': self.training}
        training_date = TrainingDateFactory(sequence=0, **kwargs)
        self.attendance = AttendanceFactory(training_date=training_date)

        self.items = [
            {
                'class': ExtendInvitationNotificationEmail,
                'args': (self.attendance,),
                'template_name': 'email_3_extend_invitation_notification.html',
                'subject': 'Doet u ook weer mee?',
                'context': self.get_extend_invitation_assertions,
                'recipients': [self.attendance.participant.email]
            },
            {
                'class': CertificateEmail,
                'args': (self.attendance,),
                'template_name': 'email_4_certificate_email.html',
                'subject': 'Alstublieft! Uw certificaat',
                'attachment_count': 1,
                'context': self.get_extend_invitation_assertions,
                'recipients': [self.attendance.participant.email]
            }
        ]

    def get_extend_invitation_assertions(self, context):
        self.assertEqual(context['training'], self.training)
        self.assertIn('schedule_url', context)
        self.assertIn('registration_url', context)

    def get_certificate_assertions(self, context):
        self.assertEqual(context['training'], self.training)
        self.assertEqual(context['amount_training_days'], 1)
        self.assertIn('schedule_url', context)
        self.assertIn('registration_url', context)


class ParticipantEmailTest(BaseEmailTestCase):
    template_dir = '../templates/debatnl_registration/emails/'
    obj_context_name = 'participant'

    def setUp(self):
        super().setUp()
        training = TrainingFactory(name='Bewust Beinvloeden')
        kwargs = {'planned_training__training': training}
        self.training_date = TrainingDateFactory(sequence=0, **kwargs)
        registration = RegistrationFactory(training=training)
        self.participant = ParticipantFactory(registration=registration)

        attendance_kwargs = {
            'participant': self.participant,
            'training_date': self.training_date
        }
        AttendanceFactory(**attendance_kwargs)

        self.items = [
            {
                'class': RegistrationConfirmationEmail,
                'args': (self.participant,),
                'template_name': 'email_1_registration_confirmation.html',
                'subject': f'Uw inschrijving voor de training {training.name}',
                'context': self.get_registration_confirmation_1_assertions,
                'recipients': [self.participant.email]
            },
            {
                'class': RegistrationConfirmationEmail,
                'args': (self.participant, True),
                'template_name': 'email_1_registration_confirmation.html',
                'subject': f'Uw inschrijving voor de training {training.name}',
                'context': self.get_registration_confirmation_2_assertions,
                'recipients': [self.participant.email]
            },
            {
                'class': TravelDirectionReminderEmail,
                'args': (self.participant,),
                'template_name': 'email_2_travel_direction_reminder.html',
                'subject': f'Uw training {registration.training}',
                'context': self.get_travel_direction_assertions,
                'recipients': [self.participant.email]
            },
            {
                'class': ReviewTrainingNotificationEmail,
                'args': (self.participant,),
                'template_name': 'email_5_review_training_notification.html',
                'subject': 'Uw feedback',
                'context': self.get_review_training_assertions,
                'recipients': [self.participant.email]
            },
            {
                'class': TrainingContinuationNotificationEmail,
                'args': (self.participant,),
                'template_name': 'email_6_training_continuation_notification.'
                                 'html',
                'subject': 'Wat is de volgende?',
                'context': self.get_review_training_assertions,
                'recipients': [self.participant.email]
            },
        ]

    def get_registration_confirmation_1_assertions(self, context):
        self.assertFalse(context['with_travel_directions'])
        self.assertIn('button_url', context)
        self.assertIn(self.training_date, context['training_dates'])

    def get_registration_confirmation_2_assertions(self, context):
        self.assertTrue(context['with_travel_directions'])
        self.assertIn('button_url', context)
        self.assertIn(self.training_date, context['training_dates'])

    def get_travel_direction_assertions(self, context):
        self.assertIn(self.training_date, context['training_dates'])

    def get_review_training_assertions(self, context):
        self.assertIn('button_url', context)

    def get_training_continuation_assertions(self, context):
        self.assertIn('button_url', context)
        self.assertIn('button_text', context)


class PlannedTrainingTokenEmailTest(BaseEmailTestCase):
    template_dir = '../templates/debatnl_registration/emails/'
    obj_context_name = 'token'

    fixtures = ['roles']

    def setUp(self):
        super().setUp()

        role = Role.objects.get(name=RoleName.HALL.value)
        hall_cp = UserFactory()
        hall_cp.profile.roles.add(role)

        role = Role.objects.get(name=RoleName.WEBSITE.value)
        website_cp = UserFactory()
        website_cp.profile.roles.add(role)

        role = Role.objects.get(name=RoleName.TRAINER.value)
        trainer = UserFactory()
        trainer.profile.roles.add(role)

        self.data = {'hello': 'world'}
        self.token = PlannedTrainingTokenFactory()
        kwargs = {'planned_training': self.token.planned_training}
        self.training_date = TrainingDateFactory(sequence=0, **kwargs)
        AttendanceFactory(training_date=self.training_date)
        training = self.token.planned_training.training
        self.training_date.trainers.add(trainer)
        self.token.training_date = self.training_date
        self.token.save()

        kwargs = {
            'sequence': 1,
            'planned_training': self.token.planned_training,
            'status': TrainingDate.CANCELLED
        }
        self.cancelled_training_date = TrainingDateFactory(**kwargs)

        attendance_kwargs = {
            'training_date': self.cancelled_training_date
        }
        AttendanceFactory(**attendance_kwargs)

        self.items = [
            {
                'class': ParticipantCancellationChoiceEmail,
                'args': (self.token, self.data),
                'template_name': 'email_18b_participant_choice.html',
                'subject': f'Wijziging deelnemer {training.name}',
                'context': self.get_participant_cancellation_assertions,
                'recipients': [self.admin.email]
            },
            {
                'class': BookHallReminderEmail,
                'args': (self.token,),
                'template_name': 'email_9_book_hall_reminder.html',
                'subject': f'REMINDER: Verzoek boeking zaal training '
                           f'{training.name}',
                'context': self.get_book_hall_assertions,
                'recipients': [hall_cp.email]
            },
            {
                'class': CancelHallReminderEmail,
                'args': (self.token,),
                'template_name': 'email_15b_cancel_hall_reminder.html',
                'subject': f'REMINDER: Annulering zaal training '
                           f'{training.name}',
                'context': self.get_cancel_hall_assertions,
                'recipients': [hall_cp.email]
            },
            {
                'class': HallAvailableReminderEmail,
                'args': (self.token,),
                'template_name': 'email_13_hall_available_reminder.html',
                'subject': 'Reminder, update Jotform',
                'context': self.get_button_url_assertions,
                'recipients': [self.admin.email]
            },
            {
                'class': UpdateWebsiteReminderEmail,
                'args': (self.token,),
                'template_name': 'email_14b_change_website_reminder.html',
                'subject': f'REMINDER: Verzoek tot aanpassing data training '
                           f'{training.name}',
                'context': self.get_button_url_assertions,
                'recipients': [website_cp.email]
            },
            {
                'class': AttendeesListReminderEmail,
                'args': (self.token,),
                'template_name': 'email_23_attendees_list_reminder.html',
                'subject': f'Reminder aanwezigheid deelnemers {training.name} '
                           f'van {self.training_date.date:%d-%m-%Y}',
                'context': self.get_button_url_assertions,
                'recipients': [trainer.email]
            },
            {
                'class': AttendeesAmountReminderEmail,
                'args': (self.token,),
                'template_name': 'email_24_25_attendees_amount.html',
                'subject': f'REMINDER - {self.training_date.attendees.count()}'
                           f' deelnemers voor {training.name}',
                'context': self.get_button_url_assertions,
                'recipients': [self.admin.email]
            },
        ]

    def get_participant_cancellation_assertions(self, context):
        self.assertEqual(context['recipient'], self.admin)
        self.assertEqual(context['participant'], self.token.participant)
        self.assertEqual(context['data'], self.data)

    def get_book_hall_assertions(self, context):
        self.training_date.refresh_from_db()
        self.assertIn(self.training_date, context['training_dates'])
        self.assertIn('accept_url', context)
        self.assertIn('deny_url', context)

    def get_cancel_hall_assertions(self, context):
        self.cancelled_training_date.refresh_from_db()
        self.assertIn(self.cancelled_training_date, context['training_dates'])
        self.assertIn('button_url', context)

    def get_button_url_assertions(self, context):
        self.assertIn('button_url', context)


class PlannedTrainingEmailTest(BaseEmailTestCase):
    template_dir = '../templates/debatnl_registration/emails/'
    obj_context_name = 'planned_training'

    fixtures = ['roles']

    def setUp(self):
        super().setUp()

        role = Role.objects.get(name=RoleName.HALL.value)
        hall_cp = UserFactory()
        hall_cp.profile.roles.add(role)

        role = Role.objects.get(name=RoleName.WEBSITE.value)
        website_cp = UserFactory()
        website_cp.profile.roles.add(role)

        role = Role.objects.get(name=RoleName.TRAINER.value)
        trainer = UserFactory()
        trainer.profile.roles.add(role)

        participant = ParticipantFactory()
        training = TrainingFactory(name='Bewust Beinvloeden')
        self.obj = PlannedTrainingFactory(training=training)
        kwargs = {'planned_training': self.obj}

        self.training_date = TrainingDateFactory(sequence=0, **kwargs)
        self.training_date.trainers.add(trainer)

        attendance_kwargs = {
            'training_date': self.training_date,
            'participant': participant
        }
        AttendanceFactory(**attendance_kwargs)

        kwargs = {
            'sequence': 1,
            'planned_training': self.obj,
            'status': TrainingDate.CANCELLED
        }
        self.cancelled_training_date = TrainingDateFactory(**kwargs)
        attendance_kwargs = {
            'training_date': self.cancelled_training_date,
            'participant': participant
        }
        AttendanceFactory(**attendance_kwargs)

        self.items = [
            {
                'class': BookHallNotificationEmail,
                'args': (self.obj,),
                'template_name': 'email_8_book_hall_notification.html',
                'subject': f'Verzoek boeking zaal training {training.name}',
                'context': self.get_book_hall_assertions,
                'recipients': [hall_cp.email]
            },
            {
                'class': CancelHallNotificationEmail,
                'args': (self.obj,),
                'template_name': 'email_15a_cancel_hall_notification.html',
                'subject': f'Annulering zaal training {training.name}',
                'context': self.get_cancel_hall_assertions,
                'recipients': [hall_cp.email]
            },
            {
                'class': HallNotAvailableToAdminEmail,
                'args': (self.obj,),
                'template_name': 'email_10_hall_not_available_admin.html',
                'subject': f'Zaal voor training {training.name} NIET '
                           f'beschikbaar',
                'recipients': [self.admin.email]
            },
            {
                'class': HallNotAvailableToTrainersEmail,
                'args': (self.obj,),
                'template_name': 'email_11_hall_not_available_trainer.html',
                'subject': f'Annulering training {training.name}',
                'recipients': [trainer.email]
            },
            {
                'class': HallAvailableNotificationEmail,
                'args': (self.obj,),
                'template_name': 'email_12_hall_available_notification.html',
                'subject': f'Zaal voor training {training.name}',
                'context': self.get_hall_available_assertions,
                'recipients': [self.admin.email]
            },
            {
                'class': UpdateWebsiteNotificationEmail,
                'args': (self.obj,),
                'template_name': 'email_14a_change_website_notification.html',
                'subject': f'Verzoek tot aanpassing data training '
                           f'{training.name}',
                'context': self.get_update_website_assertions,
                'recipients': [website_cp.email]
            },
            {
                'class': CancelTrainingToParticipantsEmail,
                'args': (self.obj,),
                'template_name': 'email_19_cancel_training_3days_to_'
                                 'participant.html',
                'subject': f'Annulering training {training.name}',
                'context': self.get_cancel_training_participant_assertions,
                'recipients': [participant.email]
            },
            {
                'class': CancelTrainingToParticipantsEmail,
                'args': (self.obj, True),
                'template_name': 'email_18a_cancel_training_2days_to_'
                                 'participant.html',
                'subject': f'Annulering dag 2 en 3 van training '
                           f'{training.name}',
                'context': self.get_cancel_training_participant_assertions,
                'recipients': [participant.email]
            },
            {
                'class': CancelTrainingToTrainersEmail,
                'args': (self.obj,),
                'template_name': 'email_16_17_cancel_training_to_trainer.html',
                'subject': f'Annulering training {training.name}',
                'context': self.get_cancel_training_trainer_1_assertions,
                'recipients': [trainer.email]
            },
            {
                'class': CancelTrainingToTrainersEmail,
                'args': (self.obj, True),
                'template_name': 'email_16_17_cancel_training_to_trainer.html',
                'subject': f'Annulering dag 2 en 3 van training '
                           f'{training.name}',
                'context': self.get_cancel_training_trainer_2_assertions,
                'recipients': [trainer.email]
            },
        ]

    def get_book_hall_assertions(self, context):
        self.assertIn(self.training_date, context['training_dates'])
        self.assertIn('accept_url', context)
        self.assertIn('deny_url', context)

    def get_cancel_hall_assertions(self, context):
        self.assertIn(self.cancelled_training_date, context['training_dates'])
        self.assertIn('button_url', context)

    def get_hall_available_assertions(self, context):
        self.assertIn('button_url', context)

    def get_update_website_assertions(self, context):
        self.assertIn('button_url', context)

    def get_cancel_training_participant_assertions(self, context):
        self.assertIn(self.cancelled_training_date, context['training_dates'])
        self.assertIn('button_url', context)

    def get_cancel_training_trainer_1_assertions(self, context):
        training_dates = self.obj.training_dates.order_by('date').all()
        self.assertListEqual(list(training_dates), context['training_dates'])

    def get_cancel_training_trainer_2_assertions(self, context):
        training_dates = list(self.obj.training_dates.order_by('date').all())
        self.assertListEqual(training_dates[1:], context['training_dates'])


class TrainingDateEmailTest(BaseEmailTestCase):
    template_dir = '../templates/debatnl_registration/emails/'
    obj_context_name = 'training_date'

    fixtures = ['roles']

    def setUp(self):
        super().setUp()

        role = Role.objects.get(name=RoleName.HALL.value)
        hall_cp = UserFactory()
        hall_cp.profile.roles.add(role)

        role = Role.objects.get(name=RoleName.TRAINER.value)
        trainer = UserFactory()
        trainer.profile.roles.add(role)

        participant = ParticipantFactory()
        training = TrainingFactory(name='Bewust Beinvloeden')
        planned_training = PlannedTrainingFactory(training=training)
        kwargs = {'planned_training': planned_training}

        self.obj = TrainingDateFactory(sequence=0, **kwargs)
        self.obj.trainers.add(trainer)

        attendance_kwargs = {
            'training_date': self.obj,
            'participant': participant
        }
        AttendanceFactory(**attendance_kwargs)

        self.items = [
            {
                'class': TrainingConfirmationHallEmail,
                'args': (self.obj,),
                'template_name': 'email_21_training_confirmation_hall_contact.'
                                 'html',
                'subject': f'Details training {training.name} van '
                           f'{self.obj.date:%d-%m-%Y}',
                'recipients': [hall_cp.email]
            },
            {
                'class': AttendeesListNotificationEmail,
                'args': (self.obj,),
                'template_name': 'email_22_attendees_list_notification.html',
                'subject': f'Details training {training.name} van '
                           f'{self.obj.date:%d-%m-%Y}',
                'context': self.get_attendees_list_assertions,
                'recipients': [trainer.email]
            },
            {
                'class': AttendeesAmountNotificationEmail,
                'args': (self.obj,),
                'template_name': 'email_24_25_attendees_amount.html',
                'subject': f'{self.obj.attendees.count()} deelnemers voor '
                           f'{training.name}',
                'context': self.get_attendees_amount_assertions,
                'recipients': [self.admin.email]
            },
            {
                'class': TrainingInformationNotificationEmail,
                'args': (self.obj,),
                'template_name': 'email_26_training_information.html',
                'subject': f'Aantal deelnemers training {training.name} '
                           f'{self.obj.date:%d-%m-%Y}',
                'recipients': [self.admin.email]
            },
        ]

    def get_attendees_list_assertions(self, context):
        self.assertIn('button_url', context)

    def get_attendees_amount_assertions(self, context):
        self.assertIn('button_url', context)
