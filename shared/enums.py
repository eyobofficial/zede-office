from enum import Enum


class RoleName(Enum):
    TRAINER = 'Trainer'
    EDITOR = 'Redacteur'
    ADMIN = 'Administratief contact'
    HALL = 'Zaal contactpersoon'
    WEBSITE = 'Website contactpersoon'


class Position(Enum):
    JUNIOR_TRAINER = 'Junior Trainer'
    TRAINER = 'Trainer'
    SENIOR_TRAINER = 'Senior Trainer'
    SALARY_PARTNER = 'Salary Partner'
