import factory

from django.utils import timezone

from .models import Organization, Person, User, Delivery, Opportunity


class OrganizationFactory(factory.django.DjangoModelFactory):
    name = factory.Faker('company')
    division_name = factory.Sequence(lambda n: f'Division {n}')
    industry = factory.Sequence(lambda n: f'Industry {n}')
    created_at = factory.LazyFunction(timezone.now)
    updated_at = factory.LazyFunction(timezone.now)
    state = Organization.STATE_AVAILABLE

    class Meta:
        model = Organization


class PersonFactory(factory.django.DjangoModelFactory):
    first_name = factory.Faker('first_name')
    last_name = factory.Faker('last_name')
    name = factory.LazyAttribute(lambda o: f'{o.first_name} {o.last_name}')
    email = factory.LazyAttribute(
        lambda o: f'{o.first_name.lower()}{o.last_name.lower()}@test.email'
    )
    created_at = factory.LazyFunction(timezone.now)
    updated_at = factory.LazyFunction(timezone.now)
    state = Person.STATE_AVAILABLE

    class Meta:
        model = Person


class UserFactory(factory.django.DjangoModelFactory):
    name = factory.Faker('name')
    email = factory.Faker('email')
    created_at = factory.LazyFunction(timezone.now)
    updated_at = factory.LazyFunction(timezone.now)
    state = User.STATE_AVAILABLE

    class Meta:
        model = User


class DeliveryFactory(factory.django.DjangoModelFactory):
    organization = factory.SubFactory(OrganizationFactory)
    order_date = factory.LazyFunction(timezone.now)
    responsible = factory.SubFactory(UserFactory)
    created_at = factory.LazyFunction(timezone.now)
    updated_at = factory.LazyFunction(timezone.now)
    state = Delivery.STATE_AVAILABLE

    class Meta:
        model = Delivery


class OpportunityFactory(factory.django.DjangoModelFactory):
    organization = factory.SubFactory(OrganizationFactory)
    person = factory.SubFactory(PersonFactory)
    assigned_to = factory.SubFactory(UserFactory)
    order_date = factory.LazyFunction(timezone.now)
    created_at = factory.LazyFunction(timezone.now)
    updated_at = factory.LazyFunction(timezone.now)

    class Meta:
        model = Opportunity
