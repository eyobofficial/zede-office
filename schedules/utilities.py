import pendulum

from schedules.models import Timetable


def generate_period_ids(date, user):
    first_day = date.start_of('week')
    last_week = first_day.add(weeks=12).add(days=1)
    last_day = last_week.end_of('week')
    period = pendulum.period(first_day, last_day)

    period_ids = []
    for week in period.range('weeks'):
        year, week, day = week.isocalendar()
        period_id = Timetable.get_period_id(year, week)
        period_ids.append(period_id)
        Timetable.init_section(year, week, user)

    return period_ids
