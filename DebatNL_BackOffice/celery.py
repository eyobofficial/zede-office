from celery import Celery

from DebatNL_BackOffice.utilities.settings_loader import load_settings

load_settings()

app = Celery('DebatNL_BackOffice')
app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()
