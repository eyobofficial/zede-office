from django import forms
from django.contrib.auth.models import User

from education.models import LearningGoal
from shared.enums import Position


class LearningGoalForm(forms.ModelForm):
    position = forms.CharField(widget=forms.Select(
        choices=[(p.name, p.value) for p in Position])
    )

    class Meta:
        model = LearningGoal
        fields = ('position', 'name', 'description', 'order')


class LearningGoalFinishedBy(forms.ModelForm):
    ACTION_ADD = 'ADD'
    ACTION_REMOVE = 'REMOVE'

    action = forms.CharField(widget=forms.Select(
        choices=[(ACTION_ADD, 'ADD'), (ACTION_REMOVE, 'REMOVE')]
    ))
    user = forms.ModelChoiceField(queryset=User.objects.all())

    class Meta:
        model = LearningGoal
        fields = ('user', 'action')

    def save(self, commit=True):
        user = self.cleaned_data['user']
        if self.cleaned_data['action'] == self.ACTION_ADD:
            self.instance.finished_by.add(user)
        else:
            self.instance.finished_by.remove(user)
        return self.instance
