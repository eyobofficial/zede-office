from datetime import datetime

import pytz

from debatnl_registration.emails.bases import AttendanceBaseEmail
from debatnl_registration.utilities.helpers import get_training_urls, \
    generate_certificate


class ExtendInvitationNotificationEmail(AttendanceBaseEmail):
    template_name = 'email_3_extend_invitation_notification.html'
    subject = 'Doet u ook weer mee?'

    def __init__(self, attendance):
        super().__init__(attendance)
        self.training = attendance.training_date.planned_training.training

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        urls = get_training_urls(self.training.name)
        context.update({
            'training': self.training,
            'schedule_url': urls['schedule_url'],
            'registration_url': urls['registration_url']
        })
        return context


class CertificateEmail(AttendanceBaseEmail):
    template_name = 'email_4_certificate_email.html'
    subject = 'Alstublieft! Uw certificaat'

    def __init__(self, attendance):
        super().__init__(attendance)
        self.training_day = attendance.training_date
        self.training = self.training_day.planned_training.training

    def get_attachments(self, **kwargs):
        kwargs = super().get_attachments(**kwargs)
        tz = pytz.timezone('Europe/Amsterdam')
        today = '{:%d-%m-%Y}'.format(datetime.now(tz).date())
        file_name = f'{self.recipient.name} - {self.training.name}.pdf'
        amount_training_days = self.training_day.sequence + 1
        kwargs[file_name] = generate_certificate(self.recipient.name,
                                                 self.training.name,
                                                 amount_training_days, today)
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        urls = get_training_urls(self.training.name)
        context.update({
            'training': self.training,
            'amount_training_days': self.training_day.sequence + 1,
            'schedule_url': urls['schedule_url'],
            'registration_url': urls['registration_url']
        })
        return context
