from django.contrib.auth.models import User

from backoffice.utilities.emails import BaseEmail


class BaseScanEmail(BaseEmail):
    template_location = '../templates/scans/emails'

    def __init__(self, scan):
        self.scan = scan


class BaseScanTrainerEmail(BaseScanEmail):
    def __init__(self, scan):
        super().__init__(scan)
        self.recipient = scan.trainer


class BaseScanDREmail(BaseScanEmail):
    def __init__(self, scan):
        super().__init__(scan)
        self.recipient = User(first_name=scan.dr_name, email=scan.dr_email)
