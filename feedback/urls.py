from django.conf.urls import url
from feedback.views import OverviewView, FeedbackView, \
    TrainerAverageScoreView, TrainingOuttakeActivation,\
    TrainingOuttakeSubscriptionView

urlpatterns = [
    url(
        r'^training/outtakes/unsubscribe/$',
        TrainingOuttakeSubscriptionView.as_view(),
        name='unsubscribe-training-outtake'
    ),
    url(
        r'^training/outtakes/(?P<training_id>[a-zA-Z0-9-]+)/$',
        TrainingOuttakeActivation.as_view(),
        name='activate-training-outtake'
    ),
    url(
        r'^monthly-average-overview-review-score/$',
        TrainerAverageScoreView.as_view(),
        name='overview_monthly_average_review_score'
    ),
    url(r'^tables-feedback/$', FeedbackView.as_view(), name='tables_feedback'),
    url(r'^$', OverviewView.as_view(), name='overview'),
]
