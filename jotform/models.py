from django.db import models


class Form(models.Model):
    id = models.CharField(primary_key=True, max_length=150)
    username = models.CharField(max_length=100, blank=True, default='')
    title = models.CharField(max_length=200, blank=True, default='')
    height = models.IntegerField(null=True)
    url = models.URLField(blank=True, default='')
    status = models.CharField(max_length=30, blank=True, default='')
    created_at = models.DateTimeField(null=True)
    updated_at = models.DateTimeField(null=True)
    new = models.IntegerField(null=True)
    count = models.IntegerField(null=True)
    last_submission = models.DateTimeField(null=True)

    def __str__(self):
        return '{} ({})'.format(self.title, self.id)


class Question(models.Model):
    qid = models.IntegerField(null=True)
    name = models.CharField(max_length=100, blank=True, default='')
    text = models.TextField(blank=True, default='')
    type = models.CharField(max_length=50, blank=True, default='')
    options = models.TextField(blank=True, default='')
    order = models.IntegerField(blank=True, null=True)
    form = models.ForeignKey(Form, on_delete=models.CASCADE,
                             related_name='questions')

    def get_options_list(self):
        if self.options:
            return self.options.split('|')
        return []


class Submission(models.Model):
    id = models.CharField(primary_key=True, max_length=150)
    status = models.CharField(max_length=50, blank=True, default='')
    new = models.BooleanField(default=True)
    flag = models.BooleanField(default=False)
    created_at = models.DateTimeField(null=True)
    updated_at = models.DateTimeField(null=True)
    form = models.ForeignKey(Form, on_delete=models.CASCADE,
                             related_name='submissions')

    def __str__(self):
        return '{} - {}'.format(self.created_at, self.form)


class Answer(models.Model):
    qid = models.IntegerField(null=True)
    text = models.TextField(blank=True, default='')
    type = models.CharField(max_length=50, blank=True, default='')
    answer = models.TextField(blank=True, default='')
    submission = models.ForeignKey(Submission, on_delete=models.CASCADE,
                                   related_name='answers')

    def get_question(self):
        question = None

        try:
            question = self.submission.form.questions.get(qid=self.qid)
        except Question.DoesNotExist:
            pass

        return question


class Webhook(models.Model):
    url = models.URLField(blank=True, default='')
    form = models.ForeignKey(Form, on_delete=models.CASCADE,
                             related_name='webhooks')
