from unittest import mock

from django.db.models.signals import pre_save, pre_delete
from django.test import TestCase, override_settings

from jotform.client.api.webhooks import Webhooks
from jotform.context_managers import disable_signal
from jotform.models import Webhook
from jotform.signals import create_webhook_on_api, remove_webhook_from_api
from jotform.tests.factories import WebhookFactory


@override_settings(JOT_FORM_API_KEY='12345')
class DisableSignalTest(TestCase):

    @mock.patch.object(Webhooks, 'create')
    def test_webhook_creation(self, mock_create):
        with disable_signal(pre_save, Webhook, create_webhook_on_api):
            WebhookFactory()
        self.assertEqual(mock_create.call_count, 0)

    @mock.patch.object(Webhooks, 'list')
    @mock.patch.object(Webhooks, 'delete')
    @mock.patch.object(Webhooks, 'create')
    def test_webhook_deletion(self, mock_create, mock_delete, mock_list):
        webhooks = WebhookFactory.create_batch(5)

        for webhook in webhooks:
            webhook.__setattr__('wid', 0)

        mock_list.return_value = webhooks

        with disable_signal(pre_delete, Webhook, remove_webhook_from_api):
            webhooks[0].delete()
            webhooks[1].delete()

        self.assertEqual(mock_list.call_count, 0)
        self.assertEqual(mock_create.call_count, 5)
        self.assertEqual(mock_delete.call_count, 0)
