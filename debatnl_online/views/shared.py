from backoffice.views import BaseSignS3UploadURLView


class SignS3DebatNLOnlineView(BaseSignS3UploadURLView):
    aws_s3_bucket = 'debatnl-online'
