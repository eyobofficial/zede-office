from django.conf import settings
from django.contrib.auth.models import User
from django.urls import reverse
from django.utils.text import slugify

from intakes.emails.bases import BaseIntakeEmail
from intakes.models import IntakeReportFile
from intakes.reports.insights import OrganizationInsightsReport, \
    TrainerInsightsReport
from intakes.reports.participants import ParticipantsReport
from intakes.reports.submissions import SubmissionReport
from shared.enums import RoleName


class BaseContactPersonIntakeEmail(BaseIntakeEmail):
    report_class = None
    report_filename = None

    def __init__(self, intake):
        super().__init__(intake)
        self.recipient = User(email=intake.cp_email)

    def get_attachments(self):
        filename = '{}.pdf'.format(self.report_filename)
        return {
            filename: self.report_class(self.intake).generate()
        }


class IntakeReminderEmail(BaseContactPersonIntakeEmail):
    report_class = ParticipantsReport
    report_filename = 'Overzicht deelnemers'
    template_name = 'email_1_intake_reminder.html'
    subject = 'Overzicht deelnemers digitale intake'


class IntakeOrganizationInsightReportEmail(BaseContactPersonIntakeEmail):
    report_class = OrganizationInsightsReport
    report_filename = 'Overzicht antwoorden'
    template_name = 'email_2a_intake_organization_insight_report.html'
    subject = 'Overzicht antwoorden digitale intake'


class IntakeTrainerInsightReportEmail(BaseIntakeEmail):
    template_name = 'email_2b_intake_trainer_insight_report.html'
    subject = 'Overzicht antwoorden/inzendingen digitale intake'
    report_files = {}

    def __init__(self, intake):
        super().__init__(intake)
        self.subject = f'{self.subject} - {intake.organization}'

    def get_context_data(self, **kwargs):
        insight_report = self.report_files['Overzicht antwoorden']
        submission_report = self.report_files['Overzicht inzendingen']
        insight_url = reverse('intakes:download-report', kwargs={
            'report_id': insight_report.report_id
        })
        submission_url = reverse('intakes:download-report', kwargs={
            'report_id': submission_report.report_id
        })
        kwargs['insight_url'] = f'{settings.APP_HOSTNAME}{insight_url}'
        kwargs['submission_url'] = f'{settings.APP_HOSTNAME}{submission_url}'
        return super().get_context_data(**kwargs)

    def generate_report(self, filename, report_class):
        report = report_class(self.intake).generate()
        report.name = f'{filename} - {self.intake.organization}'
        report.name = slugify(report.name)[:55]
        report.name = f'{report.name}.pdf'
        file = IntakeReportFile.objects.create(intake=self.intake, file=report)
        self.report_files[filename] = file

    def send(self):
        self.generate_report('Overzicht antwoorden', TrainerInsightsReport)
        self.generate_report('Overzicht inzendingen', SubmissionReport)

        for trainer in self.intake.trainers.all():
            self.recipient = trainer
            super().send()


class IntakeCheckReminderEmail(BaseIntakeEmail):
    def __init__(self, intake):
        super().__init__(intake)
        self.template_name = 'email_4_intake_check.html'
        self.subject = 'Waarschuwing. Intake {} niet ingevuld'
        self.subject = self.subject.format(intake.organization)
        self.recipient = User.objects.filter(
            profile__roles__name=RoleName.ADMIN.value,
            is_active=True
        ).first()
