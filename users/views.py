from django.contrib.auth.models import User
from django.http import HttpResponseRedirect
from django.urls import reverse_lazy
from django.views.generic import CreateView, UpdateView, DeleteView, \
    TemplateView
from extra_views import ModelFormSetView, FormSetSuccessMessageMixin

from backoffice.constants import GROUP_BACKOFFICE
from backoffice.mixins import GroupAccessMixin
from backoffice.views import BaseEmailView
from hubstaff.client import HubstaffAPIClient
from users.emails.notifications import UserInvitationEmail, \
    NoHubstaffAccountNotificationEmail
from users.forms import UserForm, UserProfileForm
from users.models import UserProfile


class UserCreateView(GroupAccessMixin, CreateView):
    model = User
    form_class = UserForm
    success_url = reverse_lazy('configuration:overview')
    access_groups = [GROUP_BACKOFFICE]

    def form_valid(self, form):
        response = super().form_valid(form)
        UserInvitationEmail(self.object).send()
        return response


class UserUpdateView(GroupAccessMixin, UpdateView):
    model = User
    form_class = UserForm
    success_url = reverse_lazy('configuration:overview')
    access_groups = [GROUP_BACKOFFICE]


class UserProfileUpdateView(GroupAccessMixin, UpdateView):
    model = UserProfile
    form_class = UserProfileForm
    success_url = reverse_lazy('configuration:overview')
    access_groups = [GROUP_BACKOFFICE]


class UserDeactivateFormSetView(GroupAccessMixin, FormSetSuccessMessageMixin,
                                ModelFormSetView):
    model = User
    fields = ('id', )
    template_name_suffix = '_confirm_group_deactivate'
    access_groups = [GROUP_BACKOFFICE]
    ordering = ('first_name', 'last_name')
    factory_kwargs = {'min_num': 0, 'extra': 0, 'can_delete': True}
    success_url = reverse_lazy('configuration:overview')
    success_message = 'Geselecteerde gebruikers zijn verwijderd.'

    def get_queryset(self):
        queryset = super().get_queryset()
        queryset = queryset.filter(is_active=True, is_superuser=False)
        return queryset.exclude(pk=self.request.user.pk)

    def formset_valid(self, formset):
        for form in formset.deleted_forms:
            form.instance.is_active = False
            form.instance.save()

        formset.can_delete = False
        return super().formset_valid(formset)


class UserDeactivateView(GroupAccessMixin, DeleteView):
    model = User
    template_name_suffix = '_confirm_deactivate'
    success_url = reverse_lazy('configuration:overview')
    access_groups = [GROUP_BACKOFFICE]

    def delete(self, request, *args, **kwargs):
        self.object = self.get_object()
        self.object.is_active = False
        self.object.save()
        return HttpResponseRedirect(self.get_success_url())


class UserProfileHubstaffUpdateView(GroupAccessMixin, UpdateView):
    model = UserProfile
    fields = ('hubstaff_user_id', )
    template_name_suffix = '_hubstaff_form'
    success_url = reverse_lazy('configuration:overview')
    access_groups = [GROUP_BACKOFFICE]

    def get_context_data(self, **kwargs):
        client = HubstaffAPIClient()
        kwargs['hubstaff_users'] = client.get_users()
        return super().get_context_data(**kwargs)


class UserProfileRemoveHubstaffUpdateView(GroupAccessMixin, UpdateView):
    model = UserProfile
    fields = ('hubstaff_user_id', )
    template_name_suffix = '_remove_hubstaff_form'
    success_url = reverse_lazy('configuration:overview')
    access_groups = [GROUP_BACKOFFICE]


class UserProfileDisableHubstaffUpdateView(TemplateView):
    template_name = 'users/public/hubstaff_disabled_success.html'

    def get_template_names(self):
        error_template = 'users/public/hubstaff_disabled_error.html'

        try:
            profile = UserProfile.objects.get(pk=self.kwargs.get('pk'))
            profile.do_hubstaff_check = False
            profile.save()
            return self.template_name

        except UserProfile.DoesNotExist:
            return error_template


class UserInvitationEmailView(BaseEmailView):
    url = 'admin:auth_user_change'
    model = User
    email_class = UserInvitationEmail
    email_name = 'user invitation'


class NoHubstaffAccountNotificationEmailView(BaseEmailView):
    url = 'admin:users_userprofile_change'
    model = UserProfile
    email_class = NoHubstaffAccountNotificationEmail
    email_name = 'No Hubstaff account notification'
