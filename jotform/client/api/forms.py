from jotform.client.api.base import JOTFormAPI
from jotform.client.objects import Form


class Forms(JOTFormAPI):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.url = 'form/{}'.format(self._form_id)

    def get(self):
        response = self._get(self.url)
        return Form.construct_from(response['content'])
