from django.conf import settings
from django.contrib.auth.models import User
from django.urls import reverse

from backoffice.utilities.emails import NotificationEmailMixin
from briefings.emails.bases import BriefingEmail, DeliverableEmail, \
    BriefingQuestionEmail


class ChangedBriefingNotification(NotificationEmailMixin, BriefingEmail):
    def __init__(self, briefing):
        super().__init__(briefing)
        self.template_name = 'email_1_changed_briefing.html'
        self.subject = 'Olaf heeft een update van je collega ontvangen'


class MaxTrainersNotification(NotificationEmailMixin, DeliverableEmail):
    def __init__(self, deliverable):
        super().__init__(deliverable)
        self.template_name = 'email_4_max_trainers.html'
        self.subject = 'Olaf twijfelt en heeft je hulp nodig'
        self.recipient = User(first_name='Ilona', last_name='Eichhorn',
                              email='eichhorn@debat.nl')

    def get_context_data(self):
        context = super().get_context_data()
        token = self.obtain_token()
        uri = reverse('briefings:deliverable-max-trainers',
                      args=(self.deliverable.pk,))
        context['url'] = f'{settings.APP_HOSTNAME}{uri}?token={token.key}'
        return context

    def obtain_token(self):
        from briefings.models import DeliverableToken
        DeliverableToken.objects.filter(deliverable=self.deliverable).delete()
        return DeliverableToken.objects.create(
            deliverable=self.deliverable, type=DeliverableToken.MAX_TRAINER)


class BriefingQuestionNotification(NotificationEmailMixin,
                                   BriefingQuestionEmail):
    def __init__(self, token):
        super().__init__(token)
        self.template_name = 'email_7_briefing_question.html'
        delivery = self.token.deliverable.delivery
        self.recipient = delivery.responsible
        organization = delivery.organization.name
        order_date = delivery.order_date.strftime('%d-%m-%Y')
        self.subject = f'Vraag gesteld over {organization} op {order_date}'

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        uri = reverse('briefings:briefing-question-update',
                      args=(self.token.pk, ))
        context['url'] = f'{settings.APP_HOSTNAME}{uri}'
        return context


class AnswerNotification(NotificationEmailMixin, BriefingQuestionEmail):
    def __init__(self, token):
        super().__init__(token)
        self.template_name = 'email_8_question_answered.html'
        self.organization = token.deliverable.delivery.organization
        self.subject = f'Update briefing {self.organization.name}'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['organization'] = self.organization
        uri = reverse('briefings:overview')
        context['url'] = f'{settings.APP_HOSTNAME}{uri}'
        return context

    def send(self):
        for trainer in self.token.deliverable.delivery.trainers.all():
            self.recipient = trainer
            super().send()
