from django.conf import settings
from django.urls import reverse

from scans.emails.bases import BaseScanTrainerEmail, BaseScanDREmail


class ScanSubmissionReminderEmail(BaseScanTrainerEmail):
    template_name = 'email_6_trainer_scan_submission_reminder.html'

    def __init__(self, scan):
        super().__init__(scan)
        self.subject = f'HERINNERING verslag {scan.organization}'


class ScanReviewReminderEmail(BaseScanDREmail):
    template_name = 'email_7_dr_scan_review_reminder.html'

    def __init__(self, scan):
        super().__init__(scan)
        self.subject = f'HERINNERING verslag {scan.organization}'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        uri = reverse('scans:approve', args=(self.scan.pk,))
        context['url'] = f"{settings.APP_HOSTNAME}{uri}"
        return context
