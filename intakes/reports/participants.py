from reportlab.platypus import Paragraph

from intakes.reports.bases import BaseIntakeReport
from jotform.models import Answer


class ParticipantsReport(BaseIntakeReport):
    title = 'Overzicht deelnemers'

    def get_content(self):
        answers = Answer.objects.filter(submission__form=self.intake.form,
                                        qid=self.intake.qid_name)

        content = [Paragraph('Digitale intake', self.styles['header'])]
        content += [Paragraph("""
           In deze rapportage vindt u een lijst van alle deelnemers die de
           intake al ingevuld hebben. Op basis van de intake maken wij een
           maatwerk training. Dat betekent dat wij bij meer inzendingen meer
           mogelijkheden hebben om de training goed vorm te geven.
           """, self.styles['body'])]

        content += [Paragraph('Overzicht deelnemers', self.styles['header'])]

        for answer in answers.order_by('answer').all():
            name = answer.answer
            content += [Paragraph(name, self.styles['body'])]

        if answers.count() == 0:
            content += [Paragraph('Er zijn nog geen intakes ingevuld.',
                                  self.styles['body'])]

        return content
