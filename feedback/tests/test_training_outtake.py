from datetime import datetime

from dateutil.parser import parse
from django.test import TestCase
from django.utils import timezone
from post_office.models import Email

from backoffice.tests.factories import AdministratorFactory
from feedback.models import TrainingOuttakeNotification, \
    TrainingOuttakeSubscriber, TrainingOuttake
from feedback.tasks import send_outtake_reminders
from feedback.tests.factories import JotFormTrainerFactory, \
    JotFormTrainingFactory, JotFormTrainingReviewFactory
from feedback.utilities.training_outtakes import \
    activate_outtake_notifications, calculate_notification_date
from shared.enums import RoleName
from users.models import Role
from users.tests.factories import UserFactory


class TrainingOuttakeTest(TestCase):
    """
    Test the creation, deletion and execution of training outtakes.
    """
    fixtures = ['roles']

    def setUp(self):
        role = Role.objects.get(name=RoleName.ADMIN.value)
        admin = UserFactory()
        admin.profile.roles.add(role)

        # Create timezone aware date
        test_date = parse('2017-07-01')
        test_date = timezone.make_aware(test_date,
                                        timezone.get_current_timezone())

        trainers = JotFormTrainerFactory.create_batch(2)
        self.trainings = [
            JotFormTrainingFactory(date=test_date, trainer=trainers[0]),
            JotFormTrainingFactory(date=test_date, trainer=trainers[1])
        ]

        # Create reviews for training 1.
        reviews = JotFormTrainingReviewFactory.create_batch(
            2, training=self.trainings[0], submission_date=test_date)

        # Create reviews for training 2.
        # This review represents a duplicate review when a reviewer has a
        # training for two or more trainers.
        JotFormTrainingReviewFactory(training=self.trainings[1],
                                     reviewer_name=reviews[0].reviewer_name,
                                     reviewer_email=reviews[0].reviewer_email,
                                     submission_date=test_date)

    def tearDown(self):
        TrainingOuttake.objects.all().delete()
        Email.objects.all().delete()

    def test_calculation_of_notification_dates(self):
        """
        Test the calculation of the notification dates.
        """

        amount_days = [1, 30, 60, 90, 120, 150, 180]
        initial_date = datetime(2017, 1, 1)
        expected_notification_dates = [
            '2017-01-02',
            '2017-01-31',
            '2017-03-02',
            '2017-04-03',
            '2017-05-01',
            '2017-05-31',
            '2017-06-30'
        ]

        for index, amount in enumerate(amount_days):
            notification_date = calculate_notification_date(initial_date,
                                                            amount)
            expected_notification_date = parse(
                expected_notification_dates[index])
            self.assertEqual(notification_date, expected_notification_date)

    def test_activation_of_training_outtake(self):
        """
        Test the activation of the training outtakes.
        """
        self.assertEqual(TrainingOuttake.objects.count(), 0)
        self.assertEqual(TrainingOuttakeSubscriber.objects.count(), 0)
        self.assertEqual(TrainingOuttakeNotification.objects.count(), 0)

        activate_outtake_notifications(self.trainings[0])

        self.assertEqual(TrainingOuttake.objects.count(), 1)
        self.assertEqual(TrainingOuttakeSubscriber.objects.count(), 2)
        self.assertEqual(TrainingOuttakeNotification.objects.count(), 14)
        self.assertEqual(TrainingOuttakeNotification.objects.filter(
            notification_type='INTRO').count(), 2)
        self.assertEqual(TrainingOuttakeNotification.objects.filter(
            notification_type='REMINDER').count(), 12)

    def test_sending_of_intro_notification(self):
        """
        Test the creation of the 'INTRO' notification.
        """

        activate_outtake_notifications(self.trainings[0])

        notifications = TrainingOuttakeNotification.objects.filter(
            notification_type='INTRO').all()
        for notification in notifications:
            notification.notification_date = timezone.now()
            notification.save()

        send_outtake_reminders()
        self.assertEqual(Email.objects.count(), 2)

        for email in Email.objects.all():
            self.assertEqual(email.subject, 'Aan de slag!')

    def test_sending_of_reminder_notification(self):
        """
        Test the creation of the 'REMINDER' notification.
        """

        activate_outtake_notifications(self.trainings[0])

        base_notification = TrainingOuttakeNotification.objects.filter(
            notification_type='REMINDER').first()
        notifications = TrainingOuttakeNotification.objects.filter(
            notification_type='REMINDER',
            notification_date=base_notification.notification_date
        ).all()

        for notification in notifications:
            notification.notification_date = timezone.now()
            notification.save()

        send_outtake_reminders()

        self.assertEqual(Email.objects.count(), 2)

        for email in Email.objects.all():
            self.assertEqual(email.subject, 'Weet u het nog?')

    def test_unsubscribing_from_notifications(self):
        """
        Test the unsubscribing from the notification schedule.
        """

        activate_outtake_notifications(self.trainings[0])

        notifications = TrainingOuttakeNotification.objects.filter(
            notification_type='INTRO').all()
        for notification in notifications:
            notification.notification_date = timezone.now()
            notification.save()

        subscriber = TrainingOuttakeSubscriber.objects.first()
        subscriber.status = 'UNSUBSCRIBED'
        subscriber.save()

        send_outtake_reminders()
        self.assertEqual(Email.objects.count(), 1)

        for email in Email.objects.all():
            self.assertEqual(email.subject, 'Aan de slag!')

    def test_receiving_of_one_email_per_training(self):
        """
        Test the creation of ONE e-mail when a subscriber submitted two or more
        reviews for the same training.
        """

        # Activate Training 1
        activate_outtake_notifications(self.trainings[0])
        self.assertEqual(TrainingOuttake.objects.count(), 1)
        self.assertEqual(TrainingOuttakeSubscriber.objects.count(), 2)
        self.assertEqual(TrainingOuttakeNotification.objects.count(), 14)
        self.assertEqual(TrainingOuttakeNotification.objects.filter(
            notification_type='INTRO').count(), 2)
        self.assertEqual(TrainingOuttakeNotification.objects.filter(
            notification_type='REMINDER').count(), 12)

        # Activate Training 2
        activate_outtake_notifications(self.trainings[1])
        self.assertEqual(TrainingOuttake.objects.count(), 2)
        self.assertEqual(TrainingOuttakeSubscriber.objects.count(), 2)
        self.assertEqual(TrainingOuttakeNotification.objects.count(), 14)
        self.assertEqual(TrainingOuttakeNotification.objects.filter(
            notification_type='INTRO').count(), 2)
        self.assertEqual(TrainingOuttakeNotification.objects.filter(
            notification_type='REMINDER').count(), 12)

        # Send Outtake Reminders.
        send_outtake_reminders()
        notifications = TrainingOuttakeNotification.objects.filter(
            notification_type='INTRO').all()
        for notification in notifications:
            notification.notification_date = timezone.now()
            notification.save()

        # Check to see if reminders are only send once.
        send_outtake_reminders()
        self.assertEqual(Email.objects.count(), 2)
