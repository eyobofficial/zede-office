import pendulum
from django.conf import settings
from django.contrib import messages
from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth.models import User
from django.http import FileResponse
from django.shortcuts import redirect
from django.urls import reverse_lazy, reverse
from django.utils.decorators import method_decorator
from django.views.generic import ListView, DetailView, DeleteView, \
    CreateView, FormView
from django.views.generic.detail import SingleObjectMixin
from backoffice.constants import GROUP_FOLLOWUPS
from backoffice.mixins import GroupAccessMixin
from backoffice.views import BaseEmailView
from shared.enums import RoleName
from shared.models import Infographic, Training
from follow_ups.emails.notifications import FollowUpEventEmail, \
    NoFollowUpEventSetupEmail
from follow_ups.forms import FollowUpEventForm, InfographicsForm, \
    InfographicDateFormSet, FollowUpEventFormSet
from follow_ups.models import FollowUp, FollowUpEvent


class OverviewView(GroupAccessMixin, ListView):
    template_name = 'follow_ups/overview.html'
    queryset = FollowUp.objects.exclude(status=FollowUp.DELETED)
    access_groups = [GROUP_FOLLOWUPS]

    def get_queryset(self):
        qs = super().get_queryset()
        user = self.request.user
        if not user.has_perm('follow_ups.view_all_followups'):
            qs = qs.filter(opportunity__assigned_to__email=user.email)
        return qs


class FollowUpDetailView(GroupAccessMixin, DetailView):
    template_name = 'follow_ups/components/modals/detail_followup.html'
    model = FollowUp
    access_groups = [GROUP_FOLLOWUPS]


class FollowUpDeleteView(GroupAccessMixin, DeleteView):
    template_name = 'follow_ups/components/modals/delete_followup.html'
    model = FollowUp
    success_url = reverse_lazy('follow_ups:overview')
    access_groups = [GROUP_FOLLOWUPS]
    object = None

    def delete(self, request, *args, **kwargs):
        self.object = self.get_object()
        success_url = self.get_success_url()
        self.object.events.all().delete()
        self.object.status = FollowUp.DELETED
        self.object.save()
        return redirect(success_url)


class FollowUpEventCreateView(GroupAccessMixin, CreateView):
    template_name = 'follow_ups/components/modals/create_followup_event.html'
    model = FollowUpEvent
    form_class = FollowUpEventForm
    access_groups = [GROUP_FOLLOWUPS]

    def get_initial(self):
        return {'follow_up': self.kwargs.get('pk')}

    def get_success_url(self):
        pk = self.kwargs.get('pk')
        return reverse_lazy('follow_ups:followup-setup', args=(pk,))

    def get_context_data(self, **kwargs):
        kwargs.update({
            'follow_up': FollowUp.objects.get(pk=self.kwargs.get('pk')),
            'admin': User.objects.filter(
                profile__roles__name=RoleName.ADMIN.value,
                is_active=True
            ).first()
        })
        return super().get_context_data(**kwargs)

    def form_valid(self, form):
        response = super().form_valid(form)
        opportunity = self.object.follow_up.opportunity
        upcoming_event = self.object.follow_up.get_upcoming_events().first()

        if upcoming_event:
            dt = upcoming_event.date
            dt = pendulum.datetime(year=dt.year, month=dt.month, day=dt.day)
            payload = {
                'OpportunityId': opportunity.pk,
                'OpportunityOrderDate': dt.isoformat(),
                'OpportunityNextFollowUp': dt.isoformat(),
            }
            opportunity.update_on_webcrm(payload)
        return response


class FollowUpSetupView(GroupAccessMixin, DetailView):
    template_name = 'follow_ups/public/setup_followup.html'
    model = FollowUp
    access_groups = [GROUP_FOLLOWUPS]
    login_url = None
    redirect_field_name = 'next'


class BaseFollowUpSetupView(GroupAccessMixin, SingleObjectMixin, FormView):
    model = FollowUp
    access_groups = [GROUP_FOLLOWUPS]

    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)


class FollowUpSetupStep1View(BaseFollowUpSetupView):
    template_name = 'follow_ups/components/modals/setup/step_1.html'
    form_class = InfographicsForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        trainings = Training.objects.exclude(name='Professioneel Faciliteren')
        context['trainings'] = trainings.all()
        return context

    def get_success_url(self):
        obj = self.get_object()
        return reverse_lazy('follow_ups:followup-setup-step2', args=[obj.pk])

    def form_valid(self, form):
        infographics = form.cleaned_data['infographics'].order_by('label')
        infographic_pks = [i.pk.__str__() for i in infographics]
        self.request.session['infographic_pks'] = infographic_pks
        return super().form_valid(form)


class FollowUpSetupStep2View(BaseFollowUpSetupView):
    template_name = 'follow_ups/components/modals/setup/step_2.html'
    form_class = InfographicDateFormSet

    def form_valid(self, formset):
        data = []
        for form in formset:
            infographic = form.cleaned_data['infographic']
            dt = form.cleaned_data['date']
            data.append((infographic.pk.__str__(), dt.strftime('%Y-%m-%d')))

        self.request.session['infographics'] = data
        return super().form_valid(formset)

    def get_initial(self):
        infographic_pks = self.request.session.get('infographic_pks', [])
        data = []
        dt = self.calculate_date()
        for pk in infographic_pks:
            data.append({
                'infographic': Infographic.objects.get(pk=pk),
                'date': dt
            })
            dt = self.calculate_date(dt)
        return data

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['formset'] = context['form']
        return context

    def get_success_url(self):
        obj = self.get_object()
        return reverse_lazy('follow_ups:followup-setup-step3', args=[obj.pk])

    @staticmethod
    def calculate_date(dt=None):
        """
        If no initial date is provided, return the next Tuesday. But if initial
        date is provided, return the date after a 12 weeks interval.
        """
        if dt:
            return dt.add(weeks=12)
        dt = pendulum.today(tz=settings.TIME_ZONE)
        while dt.day_of_week is not pendulum.TUESDAY:
            dt = dt.add(days=1)
        return dt


class FollowUpSetupStep3View(BaseFollowUpSetupView):
    template_name = 'follow_ups/components/modals/setup/step_3.html'
    form_class = FollowUpEventFormSet
    success_url = reverse_lazy('follow_ups:overview')

    def form_valid(self, formset):
        formset.save()
        self.request.session['infographic_pks'] = None
        self.request.session['infographics'] = None
        return redirect(self.get_success_url())

    def get_form_class(self):
        form_class = super().get_form_class()
        infgraphics = self.request.session.get('infographics', [])
        form_class.extra = len(infgraphics)
        return form_class

    def get_form_kwargs(self, **kwargs):
        kwargs = super().get_form_kwargs()
        kwargs['queryset'] = FollowUpEvent.objects.none()
        return kwargs

    def get_initial(self):
        data = []
        follow_up = self.get_object()
        infographics = self.request.session.get('infographics', [])
        for pk, dt in infographics:
            infographic = Infographic.objects.get(pk=pk)
            path = reverse('follow_ups:infographic-download', args=(pk,))
            data.append({
                'follow_up': follow_up,
                'subject': infographic.label,
                'date': pendulum.parse(dt).date(),
                'type': FollowUpEvent.TYPE_INFOGRAPHIC,
                'button_text': 'Download Infographic',
                'button_url': f'{settings.APP_HOSTNAME}{path}'
            })
        return data

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['formset'] = context['form']
        context['admin'] = User.objects.filter(
            profile__roles__name=RoleName.ADMIN.value,
            is_active=True
        ).first()
        return context


class InfographicDownloadView(DetailView):
    model = Infographic

    def get(self, request, *args, **kwargs):
        super().get(request, *args, **kwargs)
        return FileResponse(self.object.file, content_type='application/pdf')


@method_decorator(staff_member_required, name='dispatch')
class UpdateWebCRMOpportunityOrderDateView(DetailView):
    url = 'admin:follow_ups_followup_change'
    model = FollowUp

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        next_event = self.object.get_upcoming_events().first()

        if next_event:
            dt = next_event.date
            dt = pendulum.datetime(year=dt.year, month=dt.month, day=dt.day)
            payload = {
                'OpportunityId': self.object.opportunity.pk,
                'OpportunityOrderDate': dt.isoformat(),
                'OpportunityNextFollowUp': dt.isoformat(),
            }
            self.object.opportunity.update_on_webcrm(payload)
            message = f'The opportunity order date is updated on WebCRM'
            messages.success(request, message)
        else:
            message = f'There are no future events planned for this follow up.'
            messages.warning(request, message)

        return redirect(reverse(self.url, args=(self.object.pk,)))


class BaseFollowUpEmailView(BaseEmailView):
    url = 'admin:follow_ups_followup_change'
    model = FollowUp


class FollowUpEventEmailView(BaseEmailView):
    url = 'admin:follow_ups_followupevent_change'
    model = FollowUpEvent
    email_class = FollowUpEventEmail
    email_name = 'follow up event notification'


class NoFollowUpEventSetupEmailView(BaseEmailView):
    url = 'admin:follow_ups_followup_change'
    model = FollowUp
    email_class = NoFollowUpEventSetupEmail
    email_name = 'no follow up event setup notification'
