from django.apps import AppConfig


class JotformConfig(AppConfig):
    name = 'jotform'

    def ready(self):
        import jotform.signals  # noqa
