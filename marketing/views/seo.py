from django.urls import reverse_lazy
from django.views.generic import ListView, CreateView, DeleteView, DetailView

from backoffice.constants import GROUP_MARKETING
from backoffice.mixins import GroupAccessMixin
from marketing.forms import PageScoreReportFormSet
from marketing.models import Page, PageScore


class OverviewView(GroupAccessMixin, ListView):
    template_name = 'marketing/seo/overview.html'
    model = Page
    access_groups = [GROUP_MARKETING]


class PageCreateView(GroupAccessMixin, CreateView):
    template_name = 'marketing/seo/components/modals/modal-page-create.html'
    access_groups = [GROUP_MARKETING]
    model = Page
    fields = ('title', 'url')
    success_url = reverse_lazy('marketing:seo:overview')


class PageDeleteView(GroupAccessMixin, DeleteView):
    template_name = 'marketing/seo/components/modals/modal-page-delete.html'
    access_groups = [GROUP_MARKETING]
    model = Page
    success_url = reverse_lazy('marketing:seo:overview')


class PageScoreCreateView(GroupAccessMixin, CreateView):
    template_name = 'marketing/seo/components/modals/' \
                    'modal-page-score-create.html'
    access_groups = [GROUP_MARKETING]
    model = PageScore
    fields = ('performance', 'accessibility', 'best_practice', 'seo', 'page')
    success_url = reverse_lazy('marketing:seo:overview')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['page_id'] = self.kwargs.get('pk')
        context['formset'] = PageScoreReportFormSet()
        return context

    def form_valid(self, form):
        formset = PageScoreReportFormSet(self.request.POST, self.request.FILES)
        if formset.is_valid():
            page_score = form.save()
            formset.instance = page_score
            formset.save()
            return super().form_valid(form)
        return self.form_invalid(form)


class PageScoreDetailView(GroupAccessMixin, DetailView):
    base_template = 'marketing/seo/components/modals'
    template_name = f'{base_template}/modal-page-score-report-list.html'
    access_groups = [GROUP_MARKETING]
    model = PageScore


class PageScoreDeleteView(GroupAccessMixin, DeleteView):
    template_name = 'marketing/seo/components/modals/' \
                    'modal-page-score-delete.html'
    access_groups = [GROUP_MARKETING]
    model = PageScore
    success_url = reverse_lazy('marketing:seo:overview')
