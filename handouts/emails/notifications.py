from django.conf import settings
from django.contrib.auth.models import User
from django.urls import reverse

from backoffice.utilities.emails import NotificationEmailMixin
from shared.models import Training
from handouts import models

from .bases import BaseHandoutEmail, BaseHandoutRequestEmail


class PageSelectionNotification(NotificationEmailMixin,
                                BaseHandoutRequestEmail):
    template_name = 'email_3_handout_pages_notification.html'
    subject = 'Iets om rustig na te lezen'

    def __init__(self, handout_request, training=None):
        super().__init__(handout_request)
        first_name = handout_request.trainee_name
        email = handout_request.trainee_email
        self.recipient = User(first_name=first_name, email=email)
        self.training = training

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        token = self.obtain_token()
        pk = self.handout_request.pk
        path = reverse('handouts:handout-overview', args=[pk])
        url = f'{settings.APP_HOSTNAME}{path}?token={token.id}'

        if Training.objects.filter(name__iexact=self.training).exists():
            url = f'{url}&training={self.training}'

        context['url'] = url
        return context

    def obtain_token(self):
        kwargs = {'handout_request': self.handout_request}
        token, _ = models.HandoutToken.objects.get_or_create(**kwargs)
        return token


class NoDeliveryNotification(NotificationEmailMixin, BaseHandoutRequestEmail):
    """
    If handout_request has no corresponding `delivery`, send a link
    to the trainee to update the `trainer` & the `training_date`.
    """
    template_name = 'email_4_no_delivery_notification.html'
    subject = 'Welke training heeft u gevolgd?'

    def __init__(self, handout_request):
        super().__init__(handout_request)
        first_name = handout_request.trainee_name
        email = handout_request.trainee_email
        self.recipient = User(first_name=first_name, email=email)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        token = self.obtain_token()
        pk = self.handout_request.pk
        path = reverse('handouts:handout-update', args=[pk])
        context['url'] = f'{settings.APP_HOSTNAME}{path}?token={token.id}'
        return context

    def obtain_token(self):
        kwargs = {'handout_request': self.handout_request}
        token, _ = models.HandoutToken.objects.get_or_create(**kwargs)
        return token


class SecondNoDeliveryNotification(NoDeliveryNotification):
    """
    If a corresponding `delivery` is still not found after the user
    updated the handout request, this e-mail is sent to the trainee.
    """
    template_name = 'email_5_second_no_delivery_notification.html'
    subject = 'Training niet bekend'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.pop('url')
        return context


class TrainerNotAssignedNotification(NotificationEmailMixin,
                                     BaseHandoutRequestEmail):
    """
    If a corresponding `delivery` does not have a trainer assigned to
    it (i.e. have a status of: TRAINER NOT FOUND IN DELIVERY), send
    this e-mail.
    """
    template_name = 'email_6_no_trainer_assigned_notification.html'

    def __init__(self, handout_request):
        super().__init__(handout_request)
        self.delivery = handout_request.deliveries.first()
        self.recipient = User(
            first_name='Ilona',
            last_name='Eichhorn',
            email='eichhorn@debat.nl'
        )

    @property
    def subject(self):
        product = self.delivery.product
        date = self.delivery.order_date
        return f"Trainer niet bekend - {product} ({date.strftime('%d-%m-%Y')})"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        uri = reverse(
            'handouts:assign-handout',
            args=[self.handout_request.pk]
        )
        context['url'] = f'{settings.APP_HOSTNAME}{uri}'
        context['trainings'] = Training.objects.all()
        return context


class HandoutDownloadNotification(NotificationEmailMixin,
                                  BaseHandoutEmail):
    template_name = 'email_8_handout_download_notification.html'
    subject = 'Uw naslagwerk'

    def __init__(self, handout):
        self.handout = handout
        first_name = self.handout.handout_request.trainee_name
        email = self.handout.handout_request.trainee_email
        self.recipient = User(first_name=first_name, email=email)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        path = reverse('handouts:handout-download', args=[self.handout.pk])
        context['url'] = f'{settings.APP_HOSTNAME}{path}'
        return context
