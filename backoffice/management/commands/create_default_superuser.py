import logging
import os

from django.contrib.auth.models import User
from django.core.management import BaseCommand
from django.db import IntegrityError


class Command(BaseCommand):
    help = 'Create a default super user (username: superuser@debat.nl password: admin)'

    def handle(self, *args, **options):
        logger = logging.getLogger('django')

        try:
            if not User.objects.filter(username='superuser@debat.nl').exists():
                User.objects.create_superuser(
                    username='superuser@debat.nl',
                    email='superuser@debat.nl',
                    password='admin'
                )
        except IntegrityError as error:
            logger.warning("DB Error Thrown %s" % error)
