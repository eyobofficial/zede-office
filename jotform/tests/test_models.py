from django.test import SimpleTestCase
from django.utils import timezone

from jotform.models import Question, Submission, Form


class QuestionModelTest(SimpleTestCase):
    def test_get_options_list_with_value(self):
        question = Question(options='a|b|c')
        self.assertListEqual(question.get_options_list(), ['a', 'b', 'c'])

    def test_get_options_list_without_value(self):
        question = Question(qid=1)
        self.assertListEqual(question.get_options_list(), [])


class SubmissionModelTest(SimpleTestCase):
    def test_str_representation(self):
        now = timezone.now()
        form = Form(id=1, title='Test')
        submission = Submission(form=form, created_at=now)
        self.assertEqual(str(submission), '{} - {}'.format(now, form))
