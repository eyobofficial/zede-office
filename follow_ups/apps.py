from django.apps import AppConfig


class FollowUpsConfig(AppConfig):
    name = 'follow_ups'
