from datetime import timedelta

from django.utils import timezone

from feedback.models import TrainingOuttake, TrainingOuttakeSubscriber, \
    TrainingOuttakeNotification


def activate_outtake_notifications(training):
    """
    Create outtake notifications starting from today.

    The schedule is as followed (Today is N):
    - Intro is on N + 1 day
    - Notification is on N + 30 days
    - Notification is on N + 60 days
    - Notification is on N + 90 days
    - Notification is on N + 120 days
    - Notification is on N + 150 days
    - Notification is on N + 180 days

    :param training: (JotFormTraining) A JOTForm training object.
    """
    today = timezone.now()
    trainer = training.trainer

    # Check if an outtake already exists or create one.
    outtake, _ = TrainingOuttake.objects.get_or_create(
        training_date=training.date,
        trainer_name=trainer.get_full_name()
    )

    for review in training.reviews.all():
        if review.reviewer_email and review.resolution:
            # Check if a subscriber already exists or create one.
            sub, created = TrainingOuttakeSubscriber.objects.get_or_create(
                training_date=training.date, name=review.reviewer_name,
                email=review.reviewer_email
            )

            if created:
                sub.outtake = outtake
                sub.resolution = review.resolution
                sub.subscription_date = today
                sub.save()

                create_outtake_notifications(today, sub)


def create_outtake_notifications(intro_date, subscriber):
    # Create 'INTRO' notification
    notify_date = calculate_notification_date(intro_date, amount_days=1)
    TrainingOuttakeNotification.objects.create(notification_type='INTRO',
                                               notification_date=notify_date,
                                               subscriber=subscriber)

    # Create 'REMINDER' notification
    amount_days = [30, 60, 90, 120, 150, 180]
    for amount in amount_days:
        n_date = calculate_notification_date(intro_date, amount_days=amount)

        TrainingOuttakeNotification.objects.create(
            notification_type='REMINDER', notification_date=n_date,
            subscriber=subscriber)


def calculate_notification_date(initial_date, amount_days):
    """
    Calculate the notification date by adding the amount of days to the initial
    date.If the notification date is a day in the weekend, the notification
    date will be set to the next working day (Monday).

    :param initial_date: (datetime) The initial date.
    :param amount_days: (int) The amount of days to add.
    """

    notification_date = initial_date + timedelta(days=amount_days)

    # Check if notification date is a Saturday.
    if notification_date.weekday() == 5:
        notification_date = initial_date + timedelta(days=amount_days + 2)

    # Check if notification date is a Sunday.
    if notification_date.weekday() == 6:
        notification_date = initial_date + timedelta(days=amount_days + 1)

    return notification_date
