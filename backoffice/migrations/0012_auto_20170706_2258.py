# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-07-06 20:58
from __future__ import unicode_literals

from django.db import migrations, models
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('backoffice', '0011_person'),
    ]

    operations = [
        migrations.CreateModel(
            name='Employee',
            fields=[
                ('employee_id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('last_name', models.CharField(blank=True, max_length=50, null=True)),
                ('first_name', models.CharField(blank=True, max_length=50, null=True)),
                ('position', models.CharField(blank=True, max_length=50, null=True)),
                ('phone', models.CharField(blank=True, max_length=30, null=True)),
                ('email', models.CharField(blank=True, max_length=50, null=True)),
                ('category', models.CharField(blank=True, choices=[('TRAINER', 'TRAINER'), ('ADMINISTRATOR', 'ADMINISTRATOR'), ('EDITOR', 'EDITOR')], max_length=30, null=True)),
            ],
        ),
        migrations.DeleteModel(
            name='Person',
        ),
    ]
