import pendulum

from django.conf import settings
from django.contrib import messages
from django.http import JsonResponse, HttpResponse
from django.shortcuts import redirect
from django.views.generic import ListView, CreateView, DeleteView, \
    DetailView, RedirectView
from django.utils import timezone
from django.urls import reverse_lazy, reverse
from extra_views import InlineFormSetFactory, UpdateWithInlinesView, \
    NamedFormsetsMixin, CreateWithInlinesView

from backoffice.constants import GROUP_DECLARATIONS
from backoffice.mixins import GroupAccessMixin
from backoffice.views import BaseEmailView
from backoffice.models import Employee
from hubstaff.client import HubstaffAPIClient
from schedules.models import VacationPeriod

from .emails.notifications import NewClientBonusNotification
from .emails.reminders import DeclarationReminder
from .forms import DeclarationModelForm, ExternalMeetingModelForm, \
    InternalMeetingModelForm, EmployeeBonusModelForm, ClientBonusModelForm, \
    ExpenseModelForm, ELearningItemModelForm, HubstaffItemModelForm, \
    TravelExpenseRouteModelForm, TravelExpenseModelForm, AllowanceModelForm
from .models import Declaration, ExternalMeeting, InternalMeeting, \
    ClientBonus, EmployeeBonus, Expense, ELearningItem, HubstaffItem, \
    TravelExpense, TravelExpenseRoute


class DeclarationOverview(GroupAccessMixin, ListView):
    access_groups = [GROUP_DECLARATIONS]
    model = Declaration
    start_year = 2019

    def get_queryset(self):
        user = self.request.user
        selected_year = self.get_selected_year()
        months = range(1, 13)
        kwargs = {'user': user, 'year': selected_year}
        for month in months:
            if selected_year == self.start_year and month not in (11, 12):
                continue

            Declaration.objects.get_or_create(month=month, **kwargs)

        return Declaration.objects.filter(**kwargs)

    def get_context_data(self, **kwargs):
        kwargs['year_list'] = self.get_year_list()
        kwargs['selected_year'] = self.get_selected_year()
        return super().get_context_data(**kwargs)

    def get_selected_year(self):
        current_year = timezone.now().year
        return int(self.request.GET.get('year', current_year))

    def get_year_list(self):
        """
        Return the list of years in the dropdown
        """
        year_list = []
        end_year = timezone.now().year
        current_year = self.start_year
        while current_year <= end_year:
            year_list.append(current_year)
            current_year += 1
        return year_list


class ExternalMeetingInline(InlineFormSetFactory):
    model = ExternalMeeting
    form_class = ExternalMeetingModelForm
    factory_kwargs = {'min_num': 0, 'extra': 0, 'can_delete': True}


class InternalMeetingInline(InlineFormSetFactory):
    model = InternalMeeting
    form_class = InternalMeetingModelForm
    factory_kwargs = {'min_num': 0, 'extra': 0, 'can_delete': True}


class HubstaffItemInline(InlineFormSetFactory):
    model = HubstaffItem
    form_class = HubstaffItemModelForm
    factory_kwargs = {'max_num': 1, 'extra': 1, 'can_delete': False}
    initial = [{'hours': 0, 'minutes': 0}]


class DeclarationRedirectView(GroupAccessMixin, RedirectView):
    access_groups = [GROUP_DECLARATIONS]
    pattern_name = 'declarations:external-meeting-update'


class BaseDeclarationUpdateView(GroupAccessMixin, NamedFormsetsMixin,
                                UpdateWithInlinesView):
    access_groups = [GROUP_DECLARATIONS]
    form_class = DeclarationModelForm
    success_url_name = None
    model = Declaration

    def get_object(self, queryset=None):
        declaration = super().get_object(queryset)
        ExternalMeeting.objects.create_from_webcrm(declaration)
        InternalMeeting.objects.create_from_webcrm(declaration)
        return declaration

    def get_success_url(self, args=None):
        if self.success_url:
            return super().get_success_url()

        return reverse(self.success_url_name, args=(self.object.pk,))

    def get_queryset(self):
        qs = super().get_queryset()
        qs = qs.exclude(status=Declaration.IN_PROGRESS)
        qs = qs.exclude(status=Declaration.SUBMITTED)
        qs = qs.filter(user=self.request.user)
        return qs

    def forms_valid(self, form, inlines):
        if 'next' in form.data:
            self.success_url = form.data['next']

        if 'save' in form.data:
            messages.success(self.request, 'Alle gegevens zijn opgeslagen.')

        if 'submit' in form.data:
            self.success_url = reverse('declarations:overview')

        return super().forms_valid(form, inlines)


class TabExternalMeetingUpdateView(BaseDeclarationUpdateView):
    inlines = [ExternalMeetingInline]
    inlines_names = ['external_meeting_formset']
    template_name_suffix = '_external_meeting_form'
    success_url_name = 'declarations:external-meeting-update'


class TabInternalMeetingUpdateView(BaseDeclarationUpdateView):
    inlines = [InternalMeetingInline]
    inlines_names = ['internal_meeting_formset']
    template_name_suffix = '_internal_meeting_form'
    success_url_name = 'declarations:internal-meeting-update'


class TabELearningUpdateView(BaseDeclarationUpdateView):
    template_name_suffix = '_elearning_form'
    success_url_name = 'declarations:e-learning-update'


class TabHubstaffUpdateView(BaseDeclarationUpdateView):
    inlines = [HubstaffItemInline]
    inlines_names = ['hubstaff_item_formset']
    template_name_suffix = '_hubstaff_form'
    success_url_name = 'declarations:hubstaff-update'

    def get_context_data(self, **kwargs):
        kwargs['hubstaff_work_time'] = self.get_hubstaff_work_time()
        return super().get_context_data(**kwargs)

    def get_hubstaff_work_time(self):
        start_date = pendulum.date(self.object.year, self.object.month, 1)
        end_date = start_date.end_of('month')
        trainer = Employee.get_trainer(self.request.user)

        if trainer and trainer.hubstaff_user_id:
            client = HubstaffAPIClient()
            kwargs = {
                'user_id': trainer.hubstaff_user_id,
                'start_date': start_date,
                'end_date': end_date
            }
            work_time = client.get_total_time_per_user(**kwargs)
            hours, minutes = divmod(work_time, 3600)
            minutes, seconds = divmod(minutes, 60)
            return {'hours': hours, 'minutes': minutes}

        return None


class TabAbsenceUpdateView(BaseDeclarationUpdateView):
    template_name_suffix = '_absence_form'
    success_url_name = 'declarations:absence-update'

    def get_context_data(self, **kwargs):
        kwargs['absences'] = self.get_related_vacation_periods()
        return super().get_context_data(**kwargs)

    def get_related_vacation_periods(self):
        month = self.object.month
        year = self.object.year
        periods = VacationPeriod.objects.filter(user=self.request.user).all()
        return [p for p in periods if p.is_approved_in_month(month, year)]


class TabExpenseUpdateView(BaseDeclarationUpdateView):
    template_name_suffix = '_expense_form'
    success_url_name = 'declarations:expense-update'


class BaseDeclarationDetailView(GroupAccessMixin, DetailView):
    model = Declaration
    access_groups = [GROUP_DECLARATIONS]

    def get_queryset(self):
        qs = super().get_queryset()
        qs = qs.filter(user=self.request.user)
        return qs

    def get_context_data(self, **kwargs):
        kwargs['readonly'] = True
        return super().get_context_data(**kwargs)


class TabExternalMeetingDetailView(BaseDeclarationDetailView):
    template_name_suffix = '_external_meeting_detail'


class TabInternalMeetingDetailView(BaseDeclarationDetailView):
    template_name_suffix = '_internal_meeting_detail'


class TabELearningDetailView(BaseDeclarationDetailView):
    template_name_suffix = '_elearning_detail'


class TabHubstaffDetailView(BaseDeclarationDetailView):
    template_name_suffix = '_hubstaff_detail'


class TabAbsenceDetailView(BaseDeclarationDetailView):
    template_name_suffix = '_absence_detail'


class TabExpenseDetailView(BaseDeclarationDetailView):
    template_name_suffix = '_expense_detail'


class TravelExpenseRouteInline(InlineFormSetFactory):
    model = TravelExpenseRoute
    form_class = TravelExpenseRouteModelForm
    factory_kwargs = {'min_num': 2, 'extra': 0, 'can_delete': False}


class TravelExpenseCreateView(GroupAccessMixin, CreateWithInlinesView):
    model = TravelExpense
    access_groups = [GROUP_DECLARATIONS]
    form_class = TravelExpenseModelForm
    inlines = [TravelExpenseRouteInline]
    success_url = reverse_lazy('declarations:overview')

    def get_initial(self):
        self.initial = {'date': self.request.GET.get('date', timezone.now())}
        return super().get_initial()

    def forms_valid(self, form, inlines):
        super().forms_valid(form, inlines)
        url_name = 'declarations:travel-expenses-update'
        path = reverse_lazy(url_name, args=(self.object.pk,))
        form_prefix = self.request.GET.get('form_prefix')
        data = {
            'form_prefix': form_prefix,
            'update_url': f'{path}?form_prefix={form_prefix}',
            'object_id': self.object.pk,
            'origin': self.object.routes.first().location,
            'destination': self.object.routes.last().location,
            'distance': self.object.distance,
            'duration': self.object.duration
        }
        return JsonResponse(data)


class TravelExpenseUpdateView(GroupAccessMixin, UpdateWithInlinesView):
    model = TravelExpense
    access_groups = [GROUP_DECLARATIONS]
    form_class = TravelExpenseModelForm
    inlines = [TravelExpenseRouteInline]
    success_url = reverse_lazy('declarations:overview')

    def get_initial(self):
        self.initial = {'date': self.request.GET.get('date', timezone.now())}
        return super().get_initial()

    def forms_valid(self, form, inlines):
        super().forms_valid(form, inlines)
        url_name = 'declarations:travel-expenses-update'
        path = reverse_lazy(url_name, args=(self.object.pk,))
        form_prefix = self.request.GET.get('form_prefix')
        data = {
            'form_prefix': form_prefix,
            'update_url': f'{path}?form_prefix={form_prefix}',
            'object_id': self.object.pk,
            'origin': self.object.routes.first().location,
            'destination': self.object.routes.last().location,
            'distance': self.object.distance,
            'duration': self.object.duration
        }
        return JsonResponse(data)


class TravelExpenseDeleteView(GroupAccessMixin, DeleteView):
    model = TravelExpense
    access_groups = [GROUP_DECLARATIONS]
    success_url = reverse_lazy('declarations:overview')


class TravelExpenseCalculationView(TravelExpenseCreateView):
    def forms_valid(self, form, inlines):
        inlines[0].save(commit=False)
        routes = [form.instance for form in inlines[0].forms]
        routes += [routes.pop(1)]
        distance, duration = self.object.get_total_distance_and_duration(
            routes)
        data = {'total_time': duration, 'total_distance': distance}
        return JsonResponse(data)


class ExpenseListView(GroupAccessMixin, ListView):
    model = Expense
    access_groups = [GROUP_DECLARATIONS]

    def get_queryset(self):
        qs = Expense.objects.non_allowances()
        return qs.filter(declaration_id=self.kwargs.get('pk'))


class ExpenseCreateView(GroupAccessMixin, CreateView):
    model = Expense
    form_class = ExpenseModelForm
    access_groups = [GROUP_DECLARATIONS]

    def form_valid(self, form):
        form.instance.declaration_id = self.kwargs.get('pk')
        return super().form_valid(form=form)

    def get_success_url(self):
        declaration_id = self.kwargs.get('pk')
        url_name = 'declarations:expenses-list'
        return reverse(url_name, args=(declaration_id,))


class ExpenseDeleteView(GroupAccessMixin, DeleteView):
    model = Expense
    access_groups = [GROUP_DECLARATIONS]

    def get_success_url(self):
        declaration_id = self.kwargs.get('declaration')
        url_name = 'declarations:expenses-list'
        return reverse(url_name, args=(declaration_id,))


class EmployeeBonusListView(GroupAccessMixin, ListView):
    model = EmployeeBonus
    access_groups = [GROUP_DECLARATIONS]

    def get_queryset(self):
        qs = super().get_queryset()
        return qs.filter(declaration_id=self.kwargs.get('pk'))


class EmployeeBonusCreateView(GroupAccessMixin, CreateView):
    model = EmployeeBonus
    form_class = EmployeeBonusModelForm
    access_groups = [GROUP_DECLARATIONS]

    def form_valid(self, form):
        form.instance.declaration_id = self.kwargs.get('pk')
        return super().form_valid(form=form)

    def get_success_url(self):
        declaration_id = self.kwargs.get('pk')
        url_name = 'declarations:employee-bonuses-list'
        return reverse(url_name, args=(declaration_id,))


class EmployeeBonusDeleteView(GroupAccessMixin, DeleteView):
    model = EmployeeBonus
    access_groups = [GROUP_DECLARATIONS]

    def get_success_url(self):
        declaration_id = self.kwargs.get('declaration')
        url_name = 'declarations:employee-bonuses-list'
        return reverse(url_name, args=(declaration_id,))


class ClientBonusListView(GroupAccessMixin, ListView):
    model = ClientBonus
    access_groups = [GROUP_DECLARATIONS]

    def get_queryset(self):
        qs = super().get_queryset()
        return qs.filter(declaration_id=self.kwargs.get('pk'))


class ClientBonusCreateView(GroupAccessMixin, CreateView):
    model = ClientBonus
    form_class = ClientBonusModelForm
    access_groups = [GROUP_DECLARATIONS]

    def form_valid(self, form):
        invoice_number = form.cleaned_data['invoice_number']
        if self.invoice_exists(invoice_number):
            return HttpResponse(status=400)
        form.instance.declaration_id = self.kwargs.get('pk')
        bonus = form.save()
        NewClientBonusNotification(bonus).send()
        return redirect(self.get_success_url())

    def get_success_url(self):
        declaration_id = self.kwargs.get('pk')
        url_name = 'declarations:client-bonuses-list'
        return reverse(url_name, args=(declaration_id,))

    def invoice_exists(self, invoice_number):
        """
        Check if user has already declared an instance with this invoice number
        """
        qs = ClientBonus.objects.filter(
            declaration__user=self.request.user,
            invoice_number=invoice_number
        )
        return qs.exists()


class ClientBonusDeleteView(GroupAccessMixin, DeleteView):
    model = ClientBonus
    access_groups = [GROUP_DECLARATIONS]

    def get_success_url(self):
        declaration_id = self.kwargs.get('declaration')
        url_name = 'declarations:client-bonuses-list'
        return reverse(url_name, args=(declaration_id,))


class AllowanceListView(ExpenseListView):
    template_name = 'declarations/allowance_list.html'

    def get_queryset(self):
        qs = Expense.objects.allowances()
        return qs.filter(declaration_id=self.kwargs.get('pk'))


class AllowanceCreateView(GroupAccessMixin, CreateView):
    template_name = 'declarations/allowance_bonus.html'
    model = Expense
    form_class = AllowanceModelForm
    access_groups = [GROUP_DECLARATIONS]

    def form_valid(self, form):
        declaration = self.get_declaration()
        int_meetings = form.cleaned_data.get('internal_meetings')
        ext_meetings = form.cleaned_data.get('external_meetings')
        description = self.get_description(int_meetings, ext_meetings)

        expense = form.save(commit=False)
        expense.declaration = declaration
        expense.description = description
        return super().form_valid(form)

    @staticmethod
    def get_description(int_meetings, ext_meetings):
        """
        Calculate the expense description from selected meetings
        """
        end_times = [m.end_time for m in int_meetings]
        end_times += [m.end_time for m in ext_meetings]
        hours = [end_time.hour for end_time in end_times if end_time]

        description = 'Lunchvergoeding'
        if any(h >= 20 for h in hours) and any(h <= 13 for h in hours):
            description = 'Dagvergoeding'
        elif any(h > 20 for h in hours) and any(h > 13 for h in hours):
            description = 'Dinervergoeding'

        return description

    def get_success_url(self):
        declaration_id = self.kwargs.get('pk')
        url_name = 'declarations:allowances-list'
        return reverse(url_name, args=(declaration_id,))

    def get_template_names(self):
        date = self.get_date()
        if date:
            return 'declarations/components/forms/form-allowance.html'
        return self.template_name

    def get_declaration(self):
        declaration_pk = self.kwargs.get('pk')
        declaration = Declaration.objects.get(pk=declaration_pk)
        return declaration

    def get_date(self):
        date_str = self.request.GET.get('date')
        if date_str:
            return pendulum.parse(date_str, tz=settings.TIME_ZONE).date()
        return

    def get_initial(self):
        initial = super().get_initial()
        initial['date'] = self.get_date()
        return initial

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update({
            'declaration': self.get_declaration(),
            'date': self.get_date()
        })
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['declaration'] = self.get_declaration()
        return context


class BaseMeetingAllowanceDeleteView(GroupAccessMixin, DeleteView):
    template_name = 'declarations/allowance_confirm_delete.html'
    model = Expense
    access_groups = [GROUP_DECLARATIONS]

    def get_success_url(self):
        allowance = self.get_object()
        declaration_id = allowance.declaration.pk
        url_name = 'declarations:allowances-list'
        return reverse(url_name, args=(declaration_id, ))


class InternalAllowanceDeleteView(BaseMeetingAllowanceDeleteView):
    def delete(self, request, *args, **kwargs):
        allowance = self.get_object()
        meeting_id = self.kwargs.get('meeting')
        meeting = InternalMeeting.objects.get(id=meeting_id)
        allowance.internal_meetings.remove(meeting)
        return redirect(self.get_success_url())


class ExternalAllowanceDeleteView(BaseMeetingAllowanceDeleteView):
    def delete(self, request, *args, **kwargs):
        allowance = self.get_object()
        meeting_id = self.kwargs.get('meeting')
        meeting = ExternalMeeting.objects.get(id=meeting_id)
        allowance.external_meetings.remove(meeting)
        return redirect(self.get_success_url())


class ELearningItemListView(GroupAccessMixin, ListView):
    model = ELearningItem
    access_groups = [GROUP_DECLARATIONS]

    def get_queryset(self):
        qs = super().get_queryset()
        return qs.filter(declaration_id=self.kwargs.get('pk'))


class ELearningItemCreateView(GroupAccessMixin, CreateView):
    model = ELearningItem
    form_class = ELearningItemModelForm
    access_groups = [GROUP_DECLARATIONS]

    def form_valid(self, form):
        if self.elearning_exists(form.cleaned_data['name']):
            return HttpResponse(status=400)
        form.instance.declaration_id = self.kwargs.get('pk')
        return super().form_valid(form=form)

    def get_success_url(self):
        declaration_id = self.kwargs.get('pk')
        url_name = 'declarations:elearning-items-list'
        return reverse(url_name, args=(declaration_id,))

    def elearning_exists(self, elearning_name):
        """
        Check if user has already declared an elearning instance with the same
        name.
        """
        qs = ELearningItem.objects.filter(
            declaration__user=self.request.user,
            name=elearning_name
        )
        return qs.exists()


class ELearningItemDeleteView(GroupAccessMixin, DeleteView):
    model = ELearningItem
    access_groups = [GROUP_DECLARATIONS]

    def get_success_url(self):
        declaration_id = self.kwargs.get('declaration')
        url_name = 'declarations:e-learning-update'
        return reverse(url_name, args=(declaration_id,))


class ExternalMeetingTimeDifferencesListView(ListView):
    template_name_suffix = '_time_difference_list'
    model = ExternalMeeting

    def get_queryset(self):
        webcrm_delivery_id = self.kwargs.get('pk')
        kwargs = {'webcrm_delivery_id': webcrm_delivery_id}
        return ExternalMeeting.objects.filter(**kwargs).all()

    def get_context_data(self, **kwargs):
        kwargs.update({'type': self.request.GET.get('type')})
        return super().get_context_data(**kwargs)


class BaseDeclarationEmailView(BaseEmailView):
    url = 'admin:declarations_declaration_change'
    model = Declaration


class BaseClientBonusEmailView(BaseEmailView):
    url = 'admin:declarations_clientbonus_change'
    model = ClientBonus


class DeclarationReminderEmailView(BaseDeclarationEmailView):
    email_class = DeclarationReminder
    email_name = 'declaration submission reminder'


class NewClientBonusEmailView(BaseClientBonusEmailView):
    email_class = NewClientBonusNotification
    email_name = 'new client bonus notification'
