from django.contrib import messages
from django.contrib.admin.views.decorators import staff_member_required
from django.shortcuts import redirect
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.views import View

from jotform.helpers import sync_form, sync_webhooks, sync_questions, \
    sync_submissions


@method_decorator(staff_member_required, name='dispatch')
class SyncFormView(View):
    def get(self, request, pk):
        sync_form(pk)
        messages.success(request, 'Form was synced.')
        return redirect(reverse('admin:jotform_form_change', args=(pk,)))


@method_decorator(staff_member_required, name='dispatch')
class SyncQuestionsView(View):
    def get(self, request, pk):
        sync_questions(pk)
        messages.success(request, 'Questions were synced.')
        return redirect(reverse('admin:jotform_form_change', args=(pk,)))


@method_decorator(staff_member_required, name='dispatch')
class SyncSubmissionsView(View):
    def get(self, request, pk):
        sync_submissions(pk)
        messages.success(request, 'Submissions were synced.')
        return redirect(reverse('admin:jotform_form_change', args=(pk,)))


@method_decorator(staff_member_required, name='dispatch')
class SyncWebhooksView(View):
    def get(self, request, pk):
        sync_webhooks(pk)
        messages.success(request, 'Webhooks were synced.')
        return redirect(reverse('admin:jotform_form_change', args=(pk,)))
