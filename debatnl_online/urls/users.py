from django.conf.urls import url
from django.contrib.auth.decorators import login_required

from debatnl_online.views.overviews import UserOverviewView
from debatnl_online.views.users import UserCreate, UserUpdate, UserDelete

urlpatterns = (
    url(
        r'^$',
        login_required(UserOverviewView.as_view()),
        name='overview'
    ),
    url(
        r'^create/$',
        login_required(UserCreate.as_view()),
        name='create'
    ),
    url(
        r'^update/(?P<pk>[-\w]+)/$',
        login_required(UserUpdate.as_view()),
        name='update'
    ),
    url(
        r'^delete/(?P<pk>[-\w]+)/$',
        login_required(UserDelete.as_view()),
        name='delete'
    )
)
