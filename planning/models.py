import datetime

from django.conf import settings
from django.contrib.auth.models import User
from django.db import models

from backoffice.models import Employee


class PlanningRequest(models.Model):
    # `type` choice constants
    IMMEDIATE = 'IM'
    SHORT_TERM = 'ST'
    LONG_TERM = 'LT'
    OPTION = 'OP'

    # `status` choice constants
    PENDING = 'PE'
    WAITING_FOR_APPROVAL = 'WA'
    READY = 'RE'

    # `type` field choices
    TYPE_CHOICES = (
        (IMMEDIATE, 'IMMEDIATE'),
        (SHORT_TERM, 'SHORT TERM'),
        (LONG_TERM, 'LONG TERM'),
        (OPTION, 'OPTION')
    )

    # `status` field choices
    STATUS_CHOICES = (
        (PENDING, 'PENDING'),
        (WAITING_FOR_APPROVAL, 'WAITING FOR APPROVAL'),
        (READY, 'READY')
    )

    type = models.CharField(max_length=2, choices=TYPE_CHOICES)
    status = models.CharField(
        max_length=2,
        choices=STATUS_CHOICES,
        default=PENDING
    )
    is_confirmed_by_admin = models.BooleanField(
        'confirmed by admin',
        default=False
    )
    request_approval_date = models.DateTimeField(null=True, blank=True)
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name='planning_requests',
        on_delete=models.CASCADE
    )
    finalized_at = models.DateTimeField(blank=True, null=True)
    created_at = models.DateTimeField('created date', auto_now_add=True)
    updated_at = models.DateTimeField('last modified date', auto_now=True)

    def __str__(self):
        return f'Type: {self.get_type_display()}, '\
               f'Status: {self.get_status_display()}'

    def get_trainers(self):
        """
        Return a List of assigned trainers
        """
        schedules = Schedule.objects.filter(delivery__planning_request=self)
        trainers = []
        for schedule in schedules:
            if schedule.user in trainers:
                continue
            trainers.append(schedule.user)

        return trainers

    def get_date(self):
        """
        Return the earliest date of the related deliveries.
        """
        delivery = self.deliveries.order_by('date').first()
        return delivery.date if delivery else None

    def is_confirmed(self):
        schedules = Schedule.objects.filter(delivery__planning_request=self)
        confirmations = schedules.values_list('is_confirmed', flat=True).all()
        confirmations = list(confirmations)
        confirmations.append(self.is_confirmed_by_admin)
        return all(confirmations)


class Delivery(models.Model):
    planning_request = models.ForeignKey(
        PlanningRequest,
        null=True, blank=True,
        related_name='deliveries',
        on_delete=models.CASCADE
    )
    date = models.DateField()
    organization = models.CharField(max_length=255)
    time = models.CharField(max_length=100, default='')
    location = models.TextField()
    description = models.TextField()
    notes = models.TextField(blank=True, default='')
    trainer_amount = models.IntegerField()
    old_trainers = models.ManyToManyField(
        Employee,
        related_name='planning_deliveries',
        through='Schedule'
    )
    trainers = models.ManyToManyField(User, related_name='planning_deliveries',
                                      through='Schedule')

    class Meta:
        verbose_name = 'Delivery'
        verbose_name_plural = 'Deliveries'
        ordering = ['date']

    def __str__(self):
        return self.organization

    def previous_date(self):
        return self.date - datetime.timedelta(days=1)

    def next_date(self):
        return self.date + datetime.timedelta(days=1)

    def get_confirmed_trainers(self):
        return self.schedules.filter(
            is_confirmed=True
        ).values_list('user', flat=True)


class Schedule(models.Model):
    delivery = models.ForeignKey(
        Delivery,
        related_name='schedules',
        on_delete=models.CASCADE
    )
    employee = models.ForeignKey(
        Employee,
        verbose_name='Old Trainer',
        related_name='schedules',
        null=True, on_delete=models.SET_NULL,
        db_constraint=False
    )
    user = models.ForeignKey(
        User,
        verbose_name='Trainer',
        related_name='schedules',
        null=True, on_delete=models.SET_NULL,
        db_constraint=False
    )
    is_confirmed = models.BooleanField('Confirmed', default=False)
    created_at = models.DateTimeField('Created date', auto_now_add=True)
    updated_at = models.DateTimeField('Last modified date', auto_now=True)

    class Meta:
        order_with_respect_to = 'delivery'

    def __str__(self):
        return f'{ self.delivery } schedule'
