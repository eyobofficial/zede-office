from django.conf.urls import url

from .views import OverviewView, LearningGoalCreateView, \
    LearningGoalUpdateView, LearningGoalFinishedByUpdateView

urlpatterns = [
    url(r'^$', OverviewView.as_view(), name='overview'),
    url(r'^create/$', LearningGoalCreateView.as_view(), name='create'),
    url(
        r'^(?P<pk>[0-9]+)/update/$',
        LearningGoalUpdateView.as_view(),
        name='update'
    ),
    url(
        r'^(?P<pk>[0-9]+)/update-finished-by/$',
        LearningGoalFinishedByUpdateView.as_view(),
        name='update-finished-by'
    ),
]
