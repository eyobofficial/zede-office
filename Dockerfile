FROM python:latest
MAINTAINER CIRX Software, dwayne@cirxsoftware.com

# Add project to the app directory
ADD . /

# Expose Gunicorn port
EXPOSE 8080:8080

# Set access permissions
RUN chmod +x ./run_web.sh
RUN chmod +x ./run_celery.sh
RUN chmod +x ./prepare_start.sh
RUN chmod +x ./scheduled_emails.sh
RUN chmod +x ./scheduled_tasks.sh
RUN chmod +x ./scheduled_outtakes.sh