import factory
from django.contrib.auth.models import User

from users.models import Role


class RoleFactory(factory.django.DjangoModelFactory):
    name = factory.Faker('word')

    class Meta:
        model = Role


class UserFactory(factory.django.DjangoModelFactory):
    first_name = factory.Faker('first_name')
    last_name = factory.Faker('last_name')
    email = username = factory.Faker('email')

    class Meta:
        model = User
