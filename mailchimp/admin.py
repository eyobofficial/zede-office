from django.contrib import admin

from backoffice.admin import CustomURLModelAdmin
from jotform.models import Answer
from mailchimp.models import FeedbackSubmission, MailChimpMember
from mailchimp.views import AddToMailingListView


class AnswerInline(admin.TabularInline):
    model = Answer
    readonly_fields = [i.name for i in Answer._meta.get_fields()]
    can_delete = False
    extra = 0

    def has_delete_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request, obj=None):
        return False


@admin.register(FeedbackSubmission)
class FeedbackSubmissionAdmin(CustomURLModelAdmin):
    inlines = (AnswerInline,)
    list_display = ('id', 'get_trainee_name', 'get_trainee_email',
                    'get_trainee_sector')
    readonly_fields = ('id', 'status', 'new', 'flag', 'created_at',
                       'updated_at', 'form')
    search_fields = ('answers__answer',)
    custom_urls = [
        {
            'regex': r'^(?P<pk>.+)/add-to-mailing-list/$',
            'view': AddToMailingListView,
            'name': 'add_to_mailing_list'
        }
    ]

    def has_delete_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request, obj=None):
        return False


@admin.register(MailChimpMember)
class MailChimpMemberAdmin(admin.ModelAdmin):
    raw_id_fields = ('submission',)
    list_display = ('get_trainee_email', 'get_trainee_name', 'status')
    list_filter = ('status',)

    @staticmethod
    def get_trainee_name(obj):
        return obj.submission.get_trainee_name()

    @staticmethod
    def get_trainee_email(obj):
        return obj.submission.get_trainee_email()
