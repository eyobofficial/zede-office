from django.core.exceptions import ValidationError
from django.core.validators import validate_email
from django.db import models

from backoffice.helpers import split_person_name
from jotform.models import Answer, Submission
from mailchimp.client import MailChimpClient
from mailchimp.managers import FeedbackSubmissionManager


class FeedbackSubmission(Submission):
    DEFAULT_MAILING_LIST = '86c8325ef4'
    TEST_MAILING_LIST = '7225b049a1'
    MAILING_LISTS = {'Bedrijf': '635565c9af', 'Gemeente': 'e6aa1a9634'}

    objects = FeedbackSubmissionManager()

    class Meta:
        proxy = True

    def get_trainee_name(self):
        return self.answers.get(qid=7).answer

    def get_trainee_email(self):
        return self.answers.get(qid=11).answer

    def get_trainee_sector(self):
        return self.answers.get(qid=9).answer

    def get_mailing_list(self):
        sector = self.get_trainee_sector()
        return self.MAILING_LISTS.get(sector, self.DEFAULT_MAILING_LIST)

    def add_to_mailing_list(self, list_id=None):
        client = MailChimpClient()
        name = self.get_trainee_name()
        email = self.get_trainee_email()
        list_id = list_id if list_id else self.get_mailing_list()
        first_name, last_name = split_person_name(name)

        try:
            validate_email(email)
            payload = {
                'email_address': email,
                'status': 'subscribed',
                'merge_fields': {'FNAME': first_name, 'LNAME': last_name}
            }
            return client.create_or_update_member(list_id, payload)
        except ValidationError:
            return None


class MailChimpMember(models.Model):
    SUCCESS = 'SUCCESS'
    FAILED = 'FAILED'

    STATUS_CHOICES = (
        (SUCCESS, SUCCESS),
        (FAILED, FAILED),
    )

    submission = models.OneToOneField(FeedbackSubmission,
                                      related_name='mailchimp_member')
    status = models.CharField(max_length=10, blank=True, default=SUCCESS)
