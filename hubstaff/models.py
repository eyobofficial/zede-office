from django.db import models


class HubstaffAuthToken(models.Model):
    token = models.CharField(max_length=150)
    last_activity = models.DateTimeField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
