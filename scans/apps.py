from django.apps import AppConfig


class ScansConfig(AppConfig):
    name = 'scans'

    def ready(self):
        import scans.signals  # noqa
