import factory

from webcrm.factories import OpportunityFactory

from follow_ups.models import FollowUp, FollowUpEvent


class FollowUpFactory(factory.django.DjangoModelFactory):
    opportunity = factory.SubFactory(OpportunityFactory)

    class Meta:
        model = FollowUp


class FollowUpEventFactory(factory.django.DjangoModelFactory):
    follow_up = factory.SubFactory(FollowUpFactory)
    date = factory.Faker('date_this_year')
    subject = 'Test Subject'
    message = 'Hello World Message'
    button_text = 'Button Text'
    button_url = 'https://test.test/test'

    class Meta:
        model = FollowUpEvent
