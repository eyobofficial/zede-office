from .base import BaseConsumer


class ChallengeConsumer(BaseConsumer):
    uri = 'challenges'


class ChallengeSubmissionConsumer(BaseConsumer):
    uri = 'challenge-submissions'


class ParticipantConsumer(BaseConsumer):
    uri = 'users'
