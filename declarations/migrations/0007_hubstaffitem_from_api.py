# -*- coding: utf-8 -*-
# Generated by Django 1.11.25 on 2020-01-14 06:05
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('declarations', '0006_auto_20200113_1208'),
    ]

    operations = [
        migrations.AddField(
            model_name='hubstaffitem',
            name='from_api',
            field=models.BooleanField(default=False),
        ),
    ]
