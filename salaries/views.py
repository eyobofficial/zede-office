import pendulum
from django.shortcuts import get_object_or_404
from django.urls import reverse_lazy
from django.utils.datastructures import OrderedSet
from django.views.generic import ListView, CreateView, RedirectView

from backoffice.constants import GROUP_SALARIES
from backoffice.mixins import GroupAccessMixin
from declarations.models import Declaration, FeedbackItem
from declarations.views import BaseDeclarationDetailView
from salaries.emails import ApprovedDeclarationEmail, FeedbackNotificationEmail


class SalaryOverview(GroupAccessMixin, ListView):
    access_groups = [GROUP_SALARIES]
    model = Declaration
    ordering = ('-month', 'user__first_name')
    template_name = 'salaries/declaration_list.html'
    queryset = Declaration.objects.exclude(status=Declaration.OPEN)

    def get_queryset(self):
        selected_year = self.get_selected_year()
        ordering = self.get_ordering()
        return self.queryset.filter(year=selected_year).order_by(*ordering)

    def get_context_data(self, **kwargs):
        kwargs['year_list'] = self.get_years()
        kwargs['selected_year'] = self.get_selected_year()
        return super().get_context_data(**kwargs)

    def get_selected_year(self):
        current_year = pendulum.today().year
        return int(self.request.GET.get('year', current_year))

    def get_years(self):
        years = OrderedSet()
        for column in self.queryset.values('year').distinct():
            years.add(column['year'])

        # This will make sure that the current year is always available,
        # even when there are no reviews in the current year.
        years.add(pendulum.today().year)
        return sorted(years)


class SalaryRedirectView(GroupAccessMixin, RedirectView):
    access_groups = [GROUP_SALARIES]
    pattern_name = 'salaries:external-meeting-detail'


class SalaryBaseDetailView(BaseDeclarationDetailView):
    access_groups = [GROUP_SALARIES]

    def get_queryset(self):
        return self.model.objects.all()

    def get_context_data(self, **kwargs):
        kwargs['review_mode'] = True
        return super().get_context_data(**kwargs)


class TabExternalMeetingDetailView(SalaryBaseDetailView):
    template_name_suffix = '_external_meeting_detail'


class TabInternalMeetingDetailView(SalaryBaseDetailView):
    template_name_suffix = '_internal_meeting_detail'


class TabELearningDetailView(SalaryBaseDetailView):
    template_name_suffix = '_elearning_detail'


class TabHubstaffDetailView(SalaryBaseDetailView):
    template_name_suffix = '_hubstaff_detail'


class TabAbsenceDetailView(SalaryBaseDetailView):
    template_name_suffix = '_absence_detail'


class TabExpenseDetailView(SalaryBaseDetailView):
    template_name_suffix = '_expense_detail'


class DeclarationApprovalRedirectView(GroupAccessMixin, RedirectView):
    access_groups = [GROUP_SALARIES]
    url = reverse_lazy('salaries:overview')

    def get(self, request, *args, **kwargs):
        declaration_pk = self.kwargs.get('pk')
        declaration = get_object_or_404(Declaration, pk=declaration_pk)
        declaration.status = Declaration.SUBMITTED
        declaration.save()
        ApprovedDeclarationEmail(declaration).send()
        return super().get(request, *args, **kwargs)


class DeclarationFeedbackCreateView(GroupAccessMixin, CreateView):
    model = FeedbackItem
    access_groups = [GROUP_SALARIES]
    fields = ('feedback', )
    success_url = reverse_lazy('salaries:overview')

    def get_context_data(self, **kwargs):
        declaration_pk = self.kwargs.get('pk')
        declaration = get_object_or_404(Declaration, pk=declaration_pk)
        kwargs['declaration'] = declaration
        return super().get_context_data(**kwargs)

    def form_valid(self, form):
        declaration_pk = self.kwargs.get('pk')
        declaration = get_object_or_404(Declaration, pk=declaration_pk)
        form.instance.created_by = self.request.user
        form.instance.declaration = declaration

        response = super().form_valid(form)

        declaration.status = Declaration.NEED_CHANGES
        declaration.save()
        FeedbackNotificationEmail(self.object).send()
        return response
