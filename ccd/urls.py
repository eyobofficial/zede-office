from django.conf.urls import url

from .views import OverviewView, PurchaseOrderView, ExpiredPageView, \
    PersonUpdateView


urlpatterns = [
    url(r'^$', OverviewView.as_view(), name='overview'),
    url(
        r'^purchase-orders/(?P<pk>[0-9a-f-]+)/$',
        PurchaseOrderView.as_view(),
        name='purchase-orders'
    ),
    url(r'^expired/$', ExpiredPageView.as_view(), name='expired-page'),
    url(
        r'^persons/(?P<pk>[0-9]+)/$',
        PersonUpdateView.as_view(),
        name='person-update'
    )
]
