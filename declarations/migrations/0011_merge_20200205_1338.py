# -*- coding: utf-8 -*-
# Generated by Django 1.11.27 on 2020-02-05 12:38
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('declarations', '0010_auto_20200127_0830'),
        ('declarations', '0010_auto_20200129_1804'),
    ]

    operations = [
    ]
