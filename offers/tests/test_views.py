from datetime import datetime

import pendulum
from django.conf import settings
from django.contrib.auth.models import User
from django.core.files.uploadedfile import SimpleUploadedFile
from django.test import TestCase, Client
from django.urls import reverse
from post_office.models import Email, Attachment

from backoffice.constants import GROUP_OFFERS
from offers.models import Offer, OfferToken, OfferFile, OfferReview, \
    OfferInvestment
from offers.tests.factories import OfferPhotoFactory, OfferFactory, \
    OfferInvestmentFactory, OfferAbsenceScheduleFactory
from offers.views import OfferCreate
from shared.enums import RoleName
from users.models import AppPermission, Role
from users.tests.factories import UserFactory


class OfferURLTest(TestCase):
    """
    Test the URL links of the offers app.
    """

    def test_non_public_urls_as_anonymous_user(self):
        non_public_urls = [
            reverse('offers:overview'),
            reverse('offers:create'),
            reverse('offers:update', kwargs={'pk': 1}),
            reverse('offers:delete', kwargs={'pk': 1}),
        ]

        for url in non_public_urls:
            with self.subTest(url=url):
                expected_url = f'{reverse("login")}?next=/feedback/'
                response = self.client.get(url, follow=True)
                self.assertRedirects(response, expected_url)

    def test_public_urls_as_anonymous_user(self):
        public_urls = [
            reverse('offers:review_submission'),
            reverse('offers:upload'),
        ]

        for url in public_urls:
            with self.subTest(url=url):
                response = self.client.get(url, follow=True)
                self.assertEqual(response.status_code, 200)


class OfferOverviewTest(TestCase):
    """
    Test the overview.
    """
    fixtures = ['app_permissions']

    def setUp(self):
        self.user = UserFactory()
        app_permission = AppPermission.objects.get(app_name=GROUP_OFFERS)
        self.user.profile.app_permissions.add(app_permission)
        self.client = Client()

    def test_get(self):
        """
        Test a request to the overview view from an authenticated user.
        """

        ongoing_offers = OfferFactory.create_batch(4)
        finished_offers = OfferFactory.create_batch(2, finished=True)

        # Perform request.
        self.client.force_login(self.user)
        response = self.client.get(reverse('offers:overview'))
        context = response.context

        # Assert outcome
        self.assertEqual(response.status_code, 200)
        self.assertEqual(list(context['ongoing_offers']), ongoing_offers)
        self.assertEqual(list(context['finished_offers']), finished_offers)
        self.assertEqual(len(context['trainers']), 12)
        self.assertEqual(len(context['photos']), 6)
        self.assertTemplateUsed(response, 'offers/base.html')


class OfferCreateViewTest(TestCase):
    """
    Test the create view for offer.
    """
    fixtures = ['roles', 'app_permissions']

    def setUp(self):
        self.url = reverse('offers:create')

        self.user = UserFactory()
        app_permission = AppPermission.objects.get(app_name=GROUP_OFFERS)
        self.user.profile.app_permissions.add(app_permission)

        self.client = Client()
        self.client.force_login(self.user)

        role = Role.objects.get(name=RoleName.ADMIN.value)
        admin = UserFactory()
        admin.profile.roles.add(role)

        role = Role.objects.get(name=RoleName.EDITOR.value)
        self.editor = UserFactory()
        self.editor.profile.roles.add(role)

        role = Role.objects.get(name=RoleName.TRAINER.value)
        self.trainers = UserFactory.create_batch(2)
        for trainer in self.trainers:
            trainer.profile.roles.add(role)

        self.photo = OfferPhotoFactory()

        trainer_ids = sorted([t.pk for t in self.trainers])

        self.payload = {
            'organization': 'Test Organization',
            'contact_person': 'Test CP',
            'photo': self.photo.id,
            'type': '',
            'delivery_responsible_name': 'test dr',
            'delivery_responsible_email': 'dr@test.mail',
            'question_background': 'question 1',
            'program_structure': 'program structure 1',
            'is_trainer_proposed': '1',
            'editor': self.editor.pk,
            'trainers': trainer_ids,
            'investments-TOTAL_FORMS': 2,
            'investments-INITIAL_FORMS': 0,
            'investments-MIN_NUM_FORMS': 0,
            'investments-MAX_NUM_FORMS': 1000,
            'investments-0-subject': 'Subject 0',
            'investments-0-range': 'Range 0',
            'investments-1-subject': 'Subject 1',
            'investments-1-range': 'Range 1'
        }

    def test_get(self):
        response = self.client.get(self.url)
        context = response.context
        expected_template = 'offers/components/modals/modal-offer.html'

        self.assertEqual(response.status_code, 200)
        self.assertEqual(context['modal_title'], 'NIEUWE OFFERTE AANVRAGEN')
        self.assertEqual(context['button_text'], 'Offerte aanvragen')
        self.assertTemplateUsed(response, expected_template)

    def post_request_and_assert(self, attachments):
        response = self.client.post(self.url, self.payload, follow=True)

        self.assertEqual(response.status_code, 200)
        self.assertRedirects(response, reverse('offers:overview'))
        self.assertEqual(Offer.objects.count(), 1)
        self.assertEqual(OfferToken.objects.count(), 1)
        self.assertEqual(OfferToken.objects.filter(type='UPLOAD').count(), 1)
        self.assertEqual(Email.objects.count(), 1)
        self.assertEqual(Attachment.objects.count(), attachments)

    def test_post_with_photo(self):
        """
        Test a POST request with a photo selected.
        """
        self.post_request_and_assert(attachments=1)

    def test_post_without_photo(self):
        """
        Test a POST request with no photo selected.
        """
        self.payload['photo'] = ''
        self.post_request_and_assert(attachments=0)

    def test_get_available_employees_method(self):
        role = Role.objects.get(name=RoleName.EDITOR.value)
        editor_1 = UserFactory()
        editor_1.profile.roles.add(role)

        role = Role.objects.get(name=RoleName.EDITOR.value)
        editor_2 = UserFactory()
        editor_2.profile.roles.add(role)

        today = pendulum.today(tz=settings.TIME_ZONE)
        kwargs = {
            'start_date': today.add(days=2).date(),
            'end_date': today.add(days=4).date(),
            'employee': editor_1
        }
        OfferAbsenceScheduleFactory(**kwargs)
        OfferAbsenceScheduleFactory(employee=editor_2)

        editors = [self.editor, editor_1, editor_2]
        available_editors = OfferCreate.get_available_employees(editors)

        self.assertEqual(len(available_editors), 2)
        self.assertIn(self.editor, available_editors)
        self.assertIn(editor_1, available_editors)


class OfferUpdateViewTest(TestCase):
    """
    Test the update view for offer.
    """
    fixtures = ['roles', 'app_permissions']

    def setUp(self):
        self.user = UserFactory()
        app_permission = AppPermission.objects.get(app_name=GROUP_OFFERS)
        self.user.profile.app_permissions.add(app_permission)

        self.client = Client()
        self.client.force_login(self.user)

        role = Role.objects.get(name=RoleName.ADMIN.value)
        admin = UserFactory()
        admin.profile.roles.add(role)

        self.offer = OfferFactory()
        self.investments = OfferInvestmentFactory.create_batch(
            2, offer=self.offer
        )
        self.url = reverse('offers:update', kwargs={'pk': self.offer.id})

        dr_name = self.offer.delivery_responsible_name
        dr_email = self.offer.delivery_responsible_email
        trainers = self.offer.trainers.all()
        trainer_ids = sorted([t.pk for t in trainers])
        is_trainer_proposed = '1' if self.offer.is_trainer_proposed else '0'

        self.payload = {
            'organization': self.offer.organization,
            'contact_person': self.offer.contact_person,
            'photo': self.offer.photo.id,
            'type': '',
            'delivery_responsible_name': dr_name,
            'delivery_responsible_email': dr_email,
            'question_background': self.offer.question_background,
            'program_structure': self.offer.program_structure,
            'is_trainer_proposed': is_trainer_proposed,
            'editor': self.offer.editor.pk,
            'trainers': trainer_ids,
            'investments-TOTAL_FORMS': 2,
            'investments-INITIAL_FORMS': 1,
            'investments-MIN_NUM_FORMS': 0,
            'investments-MAX_NUM_FORMS': 1000,
            'investments-0-id': self.investments[0].id,
            'investments-0-subject': self.investments[0].subject,
            'investments-0-range': self.investments[0].range,
            'investments-1-id': self.investments[1].id,
            'investments-1-subject': self.investments[1].subject,
            'investments-1-range': self.investments[1].range,
        }

    def test_get(self):
        response = self.client.get(self.url)
        context = response.context
        expected_template = 'offers/components/modals/modal-offer.html'

        self.assertEqual(response.status_code, 200)
        self.assertEqual(context['modal_title'], 'OFFERTE WIJZIGEN')
        self.assertEqual(context['button_text'], 'Offerte wijzigen')
        self.assertTemplateUsed(response, expected_template)

    def post_request_and_assert(self, emails, attachments, investments):
        response = self.client.post(self.url, self.payload, follow=True)
        self.offer.refresh_from_db()

        self.assertEqual(response.status_code, 200)
        self.assertRedirects(response, reverse('offers:overview'))
        self.assertEqual(Offer.objects.count(), 1)
        self.assertEqual(OfferToken.objects.count(), 1)
        self.assertEqual(OfferToken.objects.filter(type='UPLOAD').count(), 1)
        self.assertEqual(OfferInvestment.objects.count(), investments)
        self.assertEqual(Email.objects.count(), emails)
        self.assertEqual(Attachment.objects.count(), attachments)

    def test_change_in_non_editor_fields(self):
        """
        Test a request to the view from an authenticated user.
        """
        self.payload['organization'] = 'update organization'
        self.payload['contact_person'] = 'update cp'

        # Perform request & assert outcome.
        self.post_request_and_assert(emails=1, attachments=1, investments=2)

    def test_change_in_editor_field(self):
        role = Role.objects.get(name=RoleName.EDITOR.value)
        new_editor = UserFactory()
        new_editor.profile.roles.add(role)
        self.payload['editor'] = new_editor.pk

        # Perform request & assert outcome.
        self.post_request_and_assert(emails=2, attachments=1, investments=2)

    def test_change_in_no_photo(self):
        self.payload['photo'] = ''

        # Perform request & assert outcome.
        self.post_request_and_assert(emails=1, attachments=0, investments=2)

    def test_an_addition_to_investments(self):
        self.payload['investments-TOTAL_FORMS'] = 3
        self.payload['investments-2-subject'] = 'Subject 3'
        self.payload['investments-2-range'] = 'Range 3'

        # Perform request & assert outcome.
        self.post_request_and_assert(emails=1, attachments=1, investments=3)

    def test_a_removal_from_investments(self):
        self.payload['investments-TOTAL_FORMS'] = 1
        del self.payload['investments-1-id']
        del self.payload['investments-1-subject']
        del self.payload['investments-1-range']

        # Perform request & assert outcome.
        self.post_request_and_assert(emails=1, attachments=1, investments=1)


class OfferDeleteViewTest(TestCase):
    """
    Test the delete view for offer.
    """
    fixtures = ['roles', 'app_permissions']

    def setUp(self):
        self.user = UserFactory()
        app_permission = AppPermission.objects.get(app_name=GROUP_OFFERS)
        self.user.profile.app_permissions.add(app_permission)

        self.client = Client()
        self.client.force_login(self.user)

        role = Role.objects.get(name=RoleName.ADMIN.value)
        admin = UserFactory()
        admin.profile.roles.add(role)

        self.offer = OfferFactory()
        self.investment = OfferInvestmentFactory(offer=self.offer)
        OfferToken.objects.create(offer=self.offer, type='UPLOAD')
        self.url = reverse('offers:delete', kwargs={'pk': self.offer.id})

    def test_get(self):
        response = self.client.get(self.url)
        expected_template = 'offers/components/modals/modal-delete.html'

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, expected_template)

    def test_delete(self):
        """
        Test a request to the view from an authenticated user.
        """
        self.assertEqual(Offer.objects.count(), 1)

        # Perform request.
        response = self.client.post(self.url, follow=True)

        # Assert outcome for the request.
        self.assertEqual(response.status_code, 200)
        self.assertRedirects(response, reverse('offers:overview'))
        self.assertEqual(Offer.objects.count(), 0)
        self.assertEqual(OfferToken.objects.count(), 0)
        self.assertEqual(Email.objects.count(), 1)


class UploadOfferViewTest(TestCase):
    """
    Test the upload offer view.
    """
    fixtures = ['roles']

    def setUp(self):
        self.user = User.objects.create_user(username='test_user')
        self.client = Client()

        # Create objects.
        role = Role.objects.get(name=RoleName.ADMIN.value)
        self.admin = UserFactory()
        self.admin.profile.roles.add(role)

        self.offer = OfferFactory(is_trainer_proposed=1)
        OfferInvestmentFactory(offer=self.offer)

        self.token = OfferToken.objects.create(
            email=self.offer.editor.email,
            offer=self.offer,
            type='UPLOAD'
        )

        file = SimpleUploadedFile(
            'test.docx',
            b'file_content',
            content_type='application/vnd.openxmlformats-officedocument.'
                         'wordprocessingml.document'
        )
        self.document = OfferFile.objects.create(offer=self.offer, file=file)

    def test_request_GET_with_invalid_token_key(self):
        """
        Test a GET request with invalid token key to the view.
        """

        url = '/offers/upload/?token={}&email={}'.format(
            str(self.token.key),
            'invalid@example.com'
        )

        response = self.client.get(url)

        # Assert outcome for the request.
        self.assertEqual(response.status_code, 200)
        self.assertEqual(OfferToken.objects.filter(type='UPLOAD').count(), 1)
        self.assertEqual(OfferToken.objects.filter(type='REVIEW').count(), 0)
        self.assertTemplateUsed(response, 'offers/public/expired_token.html')

    def test_request_GET_with_valid_token_key(self):
        url = '/offers/upload/?token={}&email={}'.format(
            str(self.token.key),
            self.offer.editor.email
        )

        response = self.client.get(url)

        # Assert outcome for the request.
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['token'], self.token.key)
        self.assertEqual(response.context['email'], self.token.email)
        self.assertEqual(response.context['offer'], self.token.offer)
        self.assertEqual(OfferToken.objects.filter(type='UPLOAD').count(), 1)
        self.assertEqual(OfferToken.objects.filter(type='REVIEW').count(), 0)
        self.assertEqual(Offer.objects.count(), 1)
        self.assertEqual(OfferFile.objects.filter(offer=self.offer).count(), 1)
        self.assertTemplateUsed(
            response,
            'offers/public/upload-offer/base.html'
        )

    def test_request_POST_with_invalid_token_key(self):
        url = '/offers/upload/?token={}&email={}'.format(
            'wrong-wrong-wrong-wrong-12345',
            'invalid@example.com'
        )

        response = self.client.post(url, {self.offer.id: self.document.file})

        self.assertEqual(response.status_code, 200)
        self.assertEqual(OfferToken.objects.filter(type='UPLOAD').count(), 1)
        self.assertEqual(OfferToken.objects.filter(type='REVIEW').count(), 0)
        self.assertTemplateUsed(response, 'offers/public/expired_token.html')

    def test_request_POST_with_valid_token_key(self):
        url = '/offers/upload/?token={}&email={}'.format(
            str(self.token.key),
            self.offer.editor.email
        )

        payload = {
            'file_location_name': 'test-file-name',
            'file_location_year': 'test-file-year'
        }

        response = self.client.post(url, payload)
        self.offer.refresh_from_db()

        # Assert outcome for the request.
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['token'], self.token.key)
        self.assertEqual(response.context['email'], self.token.email)
        self.assertEqual(response.context['offer'], self.token.offer)
        self.assertEqual(response.context['status'], 'SUCCESSFUL')
        self.assertEqual(
            self.offer.file_location_name,
            payload['file_location_name']
        )
        self.assertEqual(
            self.offer.file_location_year,
            payload['file_location_year']
        )
        self.assertEqual(OfferToken.objects.filter(type='UPLOAD').count(), 0)
        self.assertEqual(OfferToken.objects.filter(type='REVIEW').count(), 1)
        self.assertTemplateUsed(
            response,
            'offers/public/upload-offer/base.html'
        )

        # Assert outcome for the E-mail
        email = Email.objects.get(to=self.offer.delivery_responsible_email)
        self.assertIn('Offerte {} is binnen'.format(
            self.offer.organization),
            email.subject
        )
        self.assertListEqual(email.to, [self.offer.delivery_responsible_email])
        self.assertEqual(
            email.from_email,
            '{} {} <{}>'.format(
                self.admin.first_name,
                self.admin.last_name,
                self.admin.email
            )
        )


class ReviewOfferViewTest(TestCase):
    """
    Test the review offer view.
    """
    fixtures = ['roles']

    def setUp(self):
        self.client = Client()

        # Create objects.
        role = Role.objects.get(name=RoleName.ADMIN.value)
        self.admin = UserFactory()
        self.admin.profile.roles.add(role)

        self.offer = OfferFactory(is_trainer_proposed=1)
        OfferInvestmentFactory(offer=self.offer)

        self.token = OfferToken.objects.create(
            email=self.offer.delivery_responsible_email,
            offer=self.offer,
            type='REVIEW'
        )

    def test_request_GET_with_invalid_token_key(self):
        """
        Test a GET request with invalid token key to the view.
        """
        path = reverse('offers:review')
        url = f'{path}?token={self.token.key}&email=invalid@example.com'

        response = self.client.get(url)

        # Assert outcome for the request.
        self.assertEqual(response.status_code, 200)
        self.assertEqual(OfferToken.objects.filter(type='REVIEW').count(), 1)
        self.assertTemplateUsed(response, 'offers/public/expired_token.html')

    def test_request_GET_with_valid_token_key(self):
        email =  self.offer.delivery_responsible_email
        path = reverse('offers:review')
        url = f'{path}?token={self.token.key}&email={email}'

        response = self.client.get(url)

        # Assert outcome for the request.
        expected_template = 'offers/public/review-offer/base.html'
        expected_token_count = OfferToken.objects.filter(type='REVIEW').count()
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['token'], self.token)
        self.assertEqual(response.context['offer'], self.token.offer)
        self.assertEqual(expected_token_count, 1)
        self.assertTemplateUsed(response, expected_template)

    def test_request_POST_with_invalid_token_key(self):
        """
        Test a POST request with invalid token key to the view.
        """
        path = reverse('offers:review')
        url = f'{path}?token={self.token.key}&email=invalid@example.com'
        payload = {'status': 'ACCEPTED', 'reason': ''}

        response = self.client.post(url, payload)

        # Assert outcome for the request.
        self.assertEqual(response.status_code, 200)
        self.assertEqual(OfferToken.objects.filter(type='REVIEW').count(), 1)
        self.assertTemplateUsed(response, 'offers/public/expired_token.html')

    def test_request_POST_with_ACCEPTED_review(self):
        email = self.offer.delivery_responsible_email
        path = reverse('offers:review')
        url = f'{path}?token={self.token.key}&email={email}'

        payload = {'status': 'ACCEPTED', 'reason': ''}

        response = self.client.post(url, payload)
        self.offer.refresh_from_db()

        # Assert outcome for the request.
        expected_url = f'{reverse("offers:review_submission")}?status=ACCEPTED'

        self.assertRedirects(response, expected_url)
        self.assertEqual(OfferToken.objects.filter(type='REVIEW').count(), 0)
        self.assertEqual(self.offer.status, Offer.FINISHED)
        self.assertEqual(self.offer.finished_at.date(), datetime.now().date())

        # Assert outcome for the E-mail
        email = Email.objects.get(to=self.offer.editor.email)
        self.assertIn('Offerte goedgekeurd', email.subject)
        self.assertListEqual(
            email.to,
            [self.offer.editor.email]
        )
        self.assertEqual(
            email.from_email,
            '{} {} <{}>'.format(
                self.admin.first_name,
                self.admin.last_name,
                self.admin.email
            )
        )

    def test_request_POST_with_DENIED_review(self):
        email = self.offer.delivery_responsible_email
        path = reverse('offers:review')
        url = f'{path}?token={self.token.key}&email={email}'

        payload = {'status': 'DENIED', 'reason': 'The reason is unknown'}

        response = self.client.post(url, payload)

        # Assert outcome for the request.
        expected_url = f'{reverse("offers:review_submission")}?status=DENIED'
        self.assertRedirects(response, expected_url)
        self.assertEqual(OfferToken.objects.filter(type='REVIEW').count(), 0)
        self.assertEqual(OfferReview.objects.count(), 1)

        # Assert outcome for the review
        review = OfferReview.objects.get(offer=self.offer)
        self.assertEqual(review.review, payload['reason'])

        # Assert outcome for the E-mail
        email = Email.objects.get(to=self.offer.editor.email)
        self.assertIn(
            'Offerte {} is niet goedgekeurd'.format(self.offer.organization),
            email.subject
        )
        self.assertListEqual(email.to, [self.offer.editor.email])
        self.assertEqual(
            email.from_email,
            '{} {} <{}>'.format(
                self.admin.first_name,
                self.admin.last_name,
                self.admin.email
            )
        )
