from reportlab.platypus import Paragraph, PageBreak

from intakes.reports.bases import BaseIntakeReport


class SubmissionReport(BaseIntakeReport):
    title = 'Overzicht inzendingen'

    def get_content(self):
        question_style = self.styles['question']
        body_style = self.styles['body']

        content = []
        for submission in self.intake.form.submissions.all():
            answers = self.get_answers(submission)

            for answer in answers:
                content += [Paragraph(answer['question'], question_style)]
                content += [Paragraph(answer['answer'], body_style)]

            content += [PageBreak()]

        return content

    def get_answers(self, submission):
        answers = []
        for answer in submission.answers.all():
            question = answer.get_question()

            if not question:
                continue

            value = self.evaluate_value(answer.answer)

            answers.append({
                'order': question.order,
                'question': question.text,
                'answer': value
            })

        # Order the list of answers based on the order attribute.
        answers = sorted(answers, key=lambda ans: ans['order'])
        return answers
