from django import forms
from django.contrib.auth.models import User

from backoffice.utilities.vimeo.client import VimeoClient
from shared.enums import RoleName


class OrganizationForm(forms.Form):
    name = forms.CharField(label='Naam Organisatie', label_suffix='')

    def save(self, consumer, object_id=None):
        if object_id:
            return consumer.update_object(object_id, self.cleaned_data)
        return consumer.create_object(self.cleaned_data)


class TrainingForm(forms.Form):
    name = forms.CharField(label='Naam', label_suffix='')
    date = forms.DateField(label='Start datum', label_suffix='')
    trainer = forms.ModelChoiceField(
        queryset=User.objects.filter(
            profile__roles__name=RoleName.TRAINER.value,
            is_active=True
        ).order_by('first_name', 'last_name'),
        label='Trainer',
        label_suffix=''
    )

    def save(self, consumer, organization_id, object_id=None):
        data = {
            'name': self.cleaned_data['name'],
            'date': self.cleaned_data['date'].strftime('%Y-%m-%d'),
            'trainer_name': self.cleaned_data['trainer'].get_full_name(),
            'trainer_email': self.cleaned_data['trainer'].email,
            'organization': organization_id
        }

        if object_id:
            return consumer.update_object(object_id, data)
        return consumer.create_object(data)


class ChallengeForm(forms.Form):
    title = forms.CharField(label='Titel Challenge', max_length=60)
    order = forms.IntegerField(label='Volgorde')
    file_s3_location = forms.CharField(required=False)
    file_name = forms.CharField(required=False)
    file_size = forms.IntegerField(required=False, initial=0)
    description = forms.CharField(
        label='Tekst challenge (maximaal 200 tekens)',
        widget=forms.Textarea,
        required=False
    )

    def save(self, consumer, training_id, object_id=None):
        data = {
            'title': self.cleaned_data['title'],
            'description': self.cleaned_data['description'],
            'order': self.cleaned_data['order'],
            'training': training_id,
            'video_url': '',
            'video_name': '',
            'video_size': None
        }

        s3_path = self.cleaned_data['file_s3_location']
        if 'file_s3_location' in self.changed_data and s3_path:
            s3_host = 'https://s3.eu-central-1.amazonaws.com/debatnl-mobile'
            s3_location = f'{s3_host}/{s3_path.replace(" ", "-")}'

            data['vimeo_video_uri'] = self.request_pull(s3_location)
            data['video_name'] = self.cleaned_data['file_name']
            data['video_size'] = self.cleaned_data['file_size']

        if object_id:
            return consumer.update_object(object_id, data)
        else:
            return consumer.create_object(data)

    def request_pull(self, s3_location):
        client = VimeoClient()
        data = client.request_pull(s3_location)
        uri = data['uri']

        client.assign_preset_to_video(uri)
        client.add_video_to_project(723740, uri)
        return uri


class ParticipantForm(forms.Form):
    first_name = forms.CharField(label='Voornaam', max_length=60)
    last_name = forms.CharField(label='Achternaam', max_length=60)
    email = forms.EmailField(label='E-mail', max_length=120)

    def save(self, consumer, training_id, object_id=None):
        data = {
            'first_name': self.cleaned_data['first_name'],
            'last_name': self.cleaned_data['last_name'],
            'email': self.cleaned_data['email'],
            'username': self.cleaned_data['email'],
            'profile': {
                'training': training_id
            }
        }

        if object_id:
            return consumer.update_object(object_id, data)
        return consumer.create_object(data)


class FeedbackForm(forms.Form):
    challenge_score = forms.FloatField(max_value=99.99, min_value=0)
    feedback = forms.CharField(
        label='Feedback voor deelnemer',
        widget=forms.Textarea
    )

    def save(self, consumer, object_id):
        data = {
            'feedback': self.cleaned_data['feedback'],
            'score': self.cleaned_data['challenge_score']
        }
        return consumer.update_object(object_id, data)
