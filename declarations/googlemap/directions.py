import requests
from django.conf import settings
from django.utils.translation import gettext as _

from .exceptions import LocationNotFoundError, APILimitError


class GoogleMapAPIClient:
    API_URL = 'https://maps.googleapis.com/maps/api/directions/json'
    API_KEY = settings.GOOGLE_MAP_API_KEY

    def get_travel_distance_and_duration(self, origin, destination,
                                         departure_time,  mode):
        """
        Get travel distance & duration between `origin` & `destination` in
        meters & seconds respectively
        """
        parameters = {
            'origin': origin,
            'destination': destination,
            'mode': self.get_transit_mode(mode),
            'departure_time': departure_time,
            'key': self.API_KEY
        }
        response = requests.get(self.API_URL, params=parameters)
        response.raise_for_status()
        data = response.json()
        status = data.get('status')
        self.raise_errors(status)
        data = data['routes'][0]['legs'][0]
        duration = data.get('duration_in_traffic', data['duration'])
        duration_value = duration['value']  # in seconds
        distance_value = data['distance']['value']  # in meters
        return distance_value, duration_value

    @staticmethod
    def get_transit_mode(mode):
        modes = {
            'CAR': 'driving',
            'BIKE': 'bicycling',
            'TRANSIT': 'transit',
            'WALKING': 'walking',
        }
        return modes[mode]

    @staticmethod
    def raise_errors(status):
        """
        Raise validation errors from the API calls.
        """
        if status == 'OK':
            return
        elif status == 'OVER_DAILY_LIMIT':
            msg = 'You have reached your daily API call limit.'
            raise APILimitError(_(msg))
        else:
            msg = 'Requested location could not be found.'
            raise LocationNotFoundError(_(msg))
