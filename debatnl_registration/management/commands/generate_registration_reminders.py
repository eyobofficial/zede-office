from datetime import datetime

import pytz
from django.core.management import BaseCommand

from debatnl_registration.tasks import \
    generate_travel_direction_reminder, \
    generate_extend_invitation_notification, generate_certificate_email, \
    generate_hall_availability_reminder, generate_update_website_reminder, \
    generate_attendees_list_reminder, \
    generate_attendees_amount_notification, \
    generate_attendees_amount_reminder, \
    generate_book_hall_reminder, \
    generate_hall_cancellation_reminder, \
    generate_training_preparation_notifications, finalize_training_dates


class Command(BaseCommand):
    help = 'Generate Offers e-mail reminders'

    def handle(self, *args, **options):
        now = datetime.now(tz=pytz.timezone('Europe/Amsterdam'))

        # Send @ 00.00 (Europe/Amsterdam)
        if now.hour == 0:
            finalize_training_dates()

        # Send @ 11.00 (Europe/Amsterdam)
        if now.hour == 11:
            generate_travel_direction_reminder()
            generate_extend_invitation_notification()
            generate_certificate_email()
            generate_attendees_amount_notification()
            generate_attendees_amount_reminder()
            generate_book_hall_reminder()
            generate_hall_cancellation_reminder()
            generate_training_preparation_notifications()

        # Send @ 20.00 (Europe/Amsterdam)
        if now.hour == 20:
            generate_hall_availability_reminder()
            generate_update_website_reminder()
            generate_attendees_list_reminder()
