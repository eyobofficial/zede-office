import uuid

from django.contrib.auth import get_user_model
from django.db import models
from tinymce.models import HTMLField

from briefings.models import Deliverable

User = get_user_model()


class Scan(models.Model):
    # Type Choice Constants
    DEBAT_SCAN = 'DS'
    CHAIRMAN_SCAN = 'CS'
    FRACTION_SCAN = 'FS'

    # Status Choice Constants
    PENDING = 'PE'
    SUBMITTED = 'SU'
    FINISHED = 'FI'

    TYPE_CHOICES = (
        (DEBAT_SCAN, 'Debate Scan'),
        (CHAIRMAN_SCAN, 'Chairman Scan'),
        (FRACTION_SCAN, 'Fraction Scan'),
    )

    STATUS_CHOICES = (
        (PENDING, 'Pending'),
        (SUBMITTED, 'Submitted'),
        (FINISHED, 'Finished')
    )

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    title = models.CharField(max_length=150)
    organization = models.CharField(max_length=150, null=True, blank=True)
    dr_name = models.CharField(
        'deliverable responsible name',
        max_length=150,
        default='', blank=True
    )
    dr_email = models.EmailField(
        'deliverable responsible email',
        max_length=100,
        null=True, blank=True
    )
    type = models.CharField(
        max_length=30,
        null=True, blank=True,
        choices=TYPE_CHOICES,
    )
    trainer = models.ForeignKey(
        User,
        blank=True, null=True,
        on_delete=models.SET_NULL
    )
    issues = HTMLField()
    status = models.CharField(
        max_length=30,
        default=PENDING,
        choices=STATUS_CHOICES
    )
    deliverables = models.ManyToManyField(
        Deliverable,
        blank=True,
        related_name='scans'
    )
    client_name = models.CharField(max_length=150, blank=True, default='')
    client_filename = models.CharField(max_length=150, blank=True, default='')
    deadline_at = models.DateTimeField(null=True, blank=True)
    finished_at = models.DateTimeField(null=True, blank=True)
    submitted_at = models.DateTimeField(null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['deadline_at']

    def __str__(self):
        return self.title


class ScanReview(models.Model):
    scan = models.ForeignKey(
        'Scan',
        on_delete=models.CASCADE,
        related_name='reviews'
    )
    feedback = HTMLField()
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['-created_at']

    def __str__(self):
        return self.scan.title
