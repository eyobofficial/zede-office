from django.conf.urls import url
from django.contrib.auth.decorators import login_required

from debatnl_online.views.learning_goals.roles import RoleCreate, RoleDelete

urlpatterns = [
    url(
        r'^create/$',
        login_required(RoleCreate.as_view()),
        name='create'
    ),
    url(
        r'^delete/(?P<pk>[-\w]+)/$',
        login_required(RoleDelete.as_view()),
        name='delete'
    )
]
