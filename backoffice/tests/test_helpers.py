from django.test import SimpleTestCase
from pendulum import Date

from backoffice.helpers import convert_to_workday


class WorkDayHelper(SimpleTestCase):
    def run_test(self, dates):
        for dt in dates:
            with self.subTest(day=dt[0]):
                workday = convert_to_workday(dt[1])
                self.assertEqual(workday, dt[2])

    def test_convert_to_workday(self):
        dates = [
            ['Monday', Date(2018, 11, 5), Date(2018, 11, 5)],
            ['Tuesday', Date(2018, 11, 6), Date(2018, 11, 6)],
            ['Wednesday', Date(2018, 11, 7), Date(2018, 11, 7)],
            ['Thursday', Date(2018, 11, 8), Date(2018, 11, 8)],
            ['Friday', Date(2018, 11, 9), Date(2018, 11, 9)],
            ['Saturday', Date(2018, 11, 10), Date(2018, 11, 12)],
            ['Sunday', Date(2018, 11, 11), Date(2018, 11, 12)],
        ]
        self.run_test(dates)

    def test_NL_holidays(self):
        dates = [
            ['Christmas', Date(2018, 12, 25), Date(2018, 12, 27)],
            ['New Year', Date(2019, 1, 1), Date(2019, 1, 2)],
        ]
        self.run_test(dates)
