from django import forms


class ParticipantForm(forms.Form):
    id = forms.CharField(required=False)
    user_id = forms.CharField(required=False)
    first_name = forms.CharField(required=False)
    last_name = forms.CharField(required=False)
