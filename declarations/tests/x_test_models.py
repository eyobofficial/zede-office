import datetime
from decimal import Decimal

from django.test import TestCase

from .factories import InternalMeetingFactory


class InternalMeetingTests(TestCase):
    """
    Tests for the InternalMeeting model
    """

    def setUp(self):
        self.meeting = InternalMeetingFactory(
            start_time=datetime.time(0, 0, 0),
            end_time=datetime.time(12, 0, 0),
            date=datetime.date(2019, 12, 15),
            declaration__internal_hourly_rate=Decimal(30),
            declaration__billable_hourly_rate=Decimal(10),
        )

    def test_get_declarable_travel_time_method_with_false_travel_expense(self):
        """
        Ensure the `get_declarable_travel_time` method returns 0 (zero) when
        the instance has False `has_travel_expenses` value.
        """

        self.assertEqual(self.meeting.get_declarable_travel_time(), 0)

    def test_get_declarable_travel_time_method_with_travel_expense(self):
        """
        Ensure the `get_declarable_travel_time` method returns a valid value
        when the instance has True `has_travel_expenses` value.
        """
        self.meeting.has_travel_expenses = True
        self.meeting.total_travel_time = 10
        self.meeting.save()

        # Assertions
        self.assertEqual(self.meeting.get_declarable_travel_time(), 24.00)

    def test_get_total_declarable_hours_method(self):
        """
        Ensure the `get_total_declarable_hours` method returns a valid value.
        """
        self.meeting.has_travel_expenses = True
        self.meeting.total_travel_time = 10
        self.meeting.save()

        # Assertions
        self.assertEqual(self.meeting.get_total_declarable_hours(), 60.00)
