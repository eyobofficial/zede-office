from django.conf.urls import url

from users.views import UserCreateView, UserDeactivateFormSetView, \
    UserDeactivateView, UserProfileUpdateView, UserUpdateView, \
    UserProfileHubstaffUpdateView, UserProfileRemoveHubstaffUpdateView, \
    UserProfileDisableHubstaffUpdateView


urlpatterns = [
    url(r'^create/$', UserCreateView.as_view(), name='create'),
    url(r'^(?P<pk>[0-9]+)/update/$', UserUpdateView.as_view(), name='update'),
    url(
        r'^(?P<pk>[0-9]+)/delete/$',
        UserDeactivateView.as_view(),
        name='deactivate'
    ),
    url(
        r'^deactivate/$',
        UserDeactivateFormSetView.as_view(),
        name='group-deactivate'
    ),
    url(
        r'^profiles/(?P<pk>[0-9]+)/update/$',
        UserProfileUpdateView.as_view(),
        name='profile-update'
    ),
    url(
        r'^profiles/(?P<pk>[0-9]+)/hubstaff/update/$',
        UserProfileHubstaffUpdateView.as_view(),
        name='profile-hubstaff-update'
    ),
    url(
        r'^profiles/(?P<pk>[0-9]+)/hubstaff/remove/$',
        UserProfileRemoveHubstaffUpdateView.as_view(),
        name='profile-hubstaff-remove'
    ),
    url(
        r'^profiles/(?P<pk>[0-9]+)/hubstaff/disable/$',
        UserProfileDisableHubstaffUpdateView.as_view(),
        name='profile-hubstaff-disable'
    ),
]
