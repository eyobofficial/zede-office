from urllib.parse import urljoin

import requests


class JOTFormAPI:
    def __init__(self, api_key, base_url, form_id, *args, **kwargs):
        self._api_key = api_key
        self._base_url = base_url
        self._form_id = form_id

    def _get_request_headers(self):
        headers = {
            'APIKEY': self._api_key
        }
        return headers

    def _get(self, path, params=None, json=True):
        url = urljoin(self._base_url, path)
        headers = self._get_request_headers()

        response = requests.get(url, params=params, headers=headers)
        response.raise_for_status()

        if json:
            return response.json()
        else:
            return response

    def _post(self, path, data=None, params=None):
        url = urljoin(self._base_url, path)
        headers = self._get_request_headers()

        response = requests.post(url, data=data, params=params,
                                 headers=headers)
        response.raise_for_status()
        return response.json()

    def _put(self, path, data=None, params=None):
        url = urljoin(self._base_url, path)
        headers = self._get_request_headers()

        response = requests.put(url, data=data, params=params, headers=headers)
        response.raise_for_status()
        return response.json()

    def _delete(self, path, params=None):
        url = urljoin(self._base_url, path)
        headers = self._get_request_headers()

        response = requests.delete(url, params=params, headers=headers)
        response.raise_for_status()
        return response.json()
