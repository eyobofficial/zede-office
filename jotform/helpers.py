import json

from django.conf import settings
from django.db.models.signals import pre_save, pre_delete

from jotform.client.client import JOTFormClient
from jotform.context_managers import disable_signal
from jotform.models import Form, Webhook, Question, Submission, Answer
from jotform.signals import create_webhook_on_api, remove_webhook_from_api


def init_kwargs(model, kwargs):
    fields = [f.name for f in model._meta.get_fields()]

    results = {}
    for k, v in kwargs.items():
        if k in fields:
            results[k] = v

    return results


def sync_form(form_id):
    client = JOTFormClient(api_key=settings.JOT_FORM_API_KEY, form_id=form_id)
    form = client.forms.get()
    Form.objects.filter(id=form_id).update(**form)


def sync_webhooks(form_id):
    client = JOTFormClient(api_key=settings.JOT_FORM_API_KEY, form_id=form_id)

    with disable_signal(pre_delete, Webhook, remove_webhook_from_api):
        Webhook.objects.filter(form_id=form_id).delete()

    webhooks = []
    for w in client.webhooks.list():
        webhooks.append(Webhook(form_id=form_id, url=w.url))

    with disable_signal(pre_save, Webhook, create_webhook_on_api):
        Webhook.objects.bulk_create(webhooks)


def sync_questions(form_id):
    client = JOTFormClient(api_key=settings.JOT_FORM_API_KEY, form_id=form_id)
    Question.objects.filter(form_id=form_id).delete()

    questions = []
    for q in client.questions.list():
        kwargs = init_kwargs(Question, q)
        questions.append(Question(form_id=form_id, **kwargs))

    Question.objects.bulk_create(questions)


def sync_submissions(form_id):
    client = JOTFormClient(api_key=settings.JOT_FORM_API_KEY, form_id=form_id)
    Submission.objects.filter(form_id=form_id).delete()
    submissions = client.submissions.list()
    persist_submission_data(form_id, submissions)


def process_submission_from_webhook(message):
    message = json.loads(message)
    form_id = message['formID']
    submission_id = message['submissionID']

    client = JOTFormClient(api_key=settings.JOT_FORM_API_KEY, form_id=form_id)
    submissions = client.submissions.get(submission_id)
    persist_submission_data(form_id, submissions)


def persist_submission_data(form_id, submission_list):
    submissions = []
    answers = {}

    for s in submission_list:
        answers[s.id] = s.pop('answers')
        kwargs = init_kwargs(Submission, s)
        submissions.append(Submission(form_id=form_id, **kwargs))

    Submission.objects.bulk_create(submissions)

    answers_list = []
    for key, values in answers.items():
        for answer in values:
            kwargs = init_kwargs(Answer, answer)
            answers_list.append(Answer(submission_id=key, **kwargs))

    Answer.objects.bulk_create(answers_list)
