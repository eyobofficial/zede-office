from django.test import TestCase, Client
from django.urls import reverse

from backoffice.constants import GROUP_SCANS
from briefings.tests.factories import DeliverableFactory
from scans.models import Scan, ScanReview
from scans.tests.factories import ScanFactory
from shared.enums import RoleName
from users.models import AppPermission, Role
from users.tests.factories import UserFactory
from webcrm.factories import OrganizationFactory


class ScanOverviewTest(TestCase):
    """
    Test the overview.
    """
    fixtures = ['app_permissions']

    def setUp(self):
        self.user = UserFactory()
        app_permission = AppPermission.objects.get(app_name=GROUP_SCANS)
        self.user.profile.app_permissions.add(app_permission)
        self.client = Client()
        self.url = reverse('scans:overview')

    def test_request_as_anonymous_user(self):
        """
        Test a request to the overview view from an anonymous user.
        """
        self.url = reverse('scans:overview')
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 302)

    def test_request_as_authenticated_user(self):
        """
        Test a request to the overview view from an authenticated user.
        """
        pending_scans_count = 4
        finished_scans_count = 2

        ScanFactory.create_batch(pending_scans_count, status=Scan.PENDING)
        ScanFactory.create_batch(finished_scans_count, status=Scan.FINISHED)

        # Perform request.
        self.client.force_login(self.user)
        response = self.client.get(self.url)
        context = response.context

        # Assert outcome
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(context['pending_scans']), pending_scans_count)
        self.assertEqual(len(context['finished_scans']), finished_scans_count)
        self.assertTemplateUsed(response, 'scans/base.html')


class ScanCreateViewTest(TestCase):
    """
    Tests for the `ScanCreateView` view
    """
    fixtures = ['app_permissions']

    def setUp(self):
        self.user = UserFactory()
        app_permission = AppPermission.objects.get(app_name=GROUP_SCANS)
        self.user.profile.app_permissions.add(app_permission)

        organization = OrganizationFactory(name='test organization')
        self.deliverable_1 = DeliverableFactory(
            delivery__organization=organization,
            delivery__product='Debatscan'
        )
        self.deliverable_2 = DeliverableFactory(
            delivery__organization=organization,
            delivery__product='Debatscan'
        )
        self.deliverable_3 = DeliverableFactory(
            delivery__organization=organization,
            delivery__product='Voorzittersscan'
        )
        self.scan = Scan.objects.create(
            title='test title',
            organization='test organization',
            dr_name='dr_first dr_last',
            dr_email='dr_email@test.email',
            type=Scan.DEBAT_SCAN,
            trainer=self.user,
            status=Scan.PENDING,
        )
        self.client = Client()

    def test_request_as_anonymous_user(self):
        """
        Test a request to the create scan view from an anonymous user.
        """
        response = self.client.get(
            reverse('scans:create', args=[self.deliverable_1.pk])
        )
        self.assertEqual(response.status_code, 302)

    def test_request_as_authenticated_user(self):
        """
        Test a request to the create scan view from authenticated user
        """
        self.client.force_login(self.user)
        response = self.client.get(
            reverse('scans:create', args=[self.deliverable_1.pk])
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.context['selected_deliverable'],
            self.deliverable_1
        )
        self.assertEqual(
            len(response.context['pending_deliverables']),
            2
        )
        self.assertTemplateUsed(
            response,
            'scans/components/modals/modal-create.html'
        )


class ScanUpdateViewTest(TestCase):
    """
    Tests for `ScanUpdateView` view
    """
    fixtures = ['app_permissions']

    def setUp(self):
        self.user = UserFactory()
        app_permission = AppPermission.objects.get(app_name=GROUP_SCANS)
        self.user.profile.app_permissions.add(app_permission)

        self.deliverable_1 = DeliverableFactory(
            delivery__organization__name='test organization',
            delivery__product='Debatscan'
        )
        self.scan = Scan.objects.create(
            title='test title',
            organization='test organization',
            dr_name='dr_first dr_last',
            dr_email='dr_email@test.email',
            type=Scan.DEBAT_SCAN,
            trainer=self.user,
            status=Scan.PENDING,
        )
        self.scan.deliverables.add(self.deliverable_1)
        self.scan.save()
        self.client = Client()

    def test_request_as_anonymous_user(self):
        """
        Test a request to the update scan view from an anonymous user.
        """
        response = self.client.get(
            reverse('scans:update', args=[self.deliverable_1.pk])
        )
        self.assertEqual(response.status_code, 302)

    def test_request_as_authenticated_user(self):
        """
        Test a request to the update scan view from authenticated user
        """
        self.client.force_login(self.user)
        response = self.client.get(
            reverse('scans:update', args=[self.deliverable_1.pk])
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.context['deliverable'],
            self.deliverable_1
        )
        self.assertEqual(len(response.context['trainers']), 1)
        self.assertTemplateUsed(
            response,
            'scans/components/modals/modal-update.html'
        )


class ScanDeleteViewTest(TestCase):
    """
    Test the delete scan view.
    """
    fixtures = ['app_permissions']

    def setUp(self):
        self.user = UserFactory()
        app_permission = AppPermission.objects.get(app_name=GROUP_SCANS)
        self.user.profile.app_permissions.add(app_permission)
        self.scan = ScanFactory(status=Scan.PENDING)
        self.client = Client()
        self.url = reverse('scans:delete', args=(self.scan.id,))

    def test_request_as_anonymous_user(self):
        """
        Test a request to the delete scan view from an anonymous user.
        """
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 302)

    def test_request_as_authenticated_user(self):
        """
        Test an valid request to the delete scan view from
        an authenticated user.
        """
        self.client.force_login(self.user)
        response = self.client.get(self.url)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['scan'], self.scan)
        self.assertEqual(
            response.templates[0].name,
            'scans/components/modals/modal-delete.html'
        )

    def test_deletion_of_scan_with_valid_request(self):
        """
        Test the deletion of a scan with a valid request from
        an authenticated user.
        """
        self.assertEqual(Scan.objects.count(), 1)

        self.client.force_login(self.user)
        response = self.client.post(self.url)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.url, reverse('scans:overview'))
        self.assertEqual(Scan.objects.count(), 0)


class ScanApproveViewTest(TestCase):
    """
    Test the `ScanApproveView` view.
    """
    fixtures = ['app_permissions', 'roles']

    def setUp(self):
        self.client = Client()
        trainer = UserFactory()
        self.scan = ScanFactory(status=Scan.SUBMITTED, trainer=trainer)
        self.url = reverse('scans:approve', args=(self.scan.id,))
        role = Role.objects.get(name=RoleName.ADMIN.value)
        admin = UserFactory()
        admin.profile.roles.add(role)

    def test_request_as_anonymous_user(self):
        """
        Test a request to the `ScanApproveView` view from an
        anonymous user.
        """
        expected_template = 'scans/public/approval.html'
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'form')
        self.assertEqual(response.context['scan'], self.scan)
        self.assertTemplateUsed(response, expected_template)

    def test_user_approves_scan(self):
        payload = {
            'status': Scan.FINISHED
        }

        response = self.client.post(self.url, payload)

        self.scan.refresh_from_db()

        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, reverse('scans:confirm'))
        self.assertEqual(self.scan.status, payload['status'])

    def test_user_denies_scan(self):
        payload = {
            'status': Scan.PENDING,
            'feedback': 'HELLO THIS IS A REVIEW'
        }

        response = self.client.post(self.url, payload)
        review = ScanReview.objects.first()

        self.scan.refresh_from_db()

        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, reverse('scans:confirm'))
        self.assertEqual(self.scan.status, payload['status'])
        self.assertEqual(ScanReview.objects.count(), 1)
        self.assertEqual(review.feedback, payload['feedback'])
        self.assertEqual(review.scan, self.scan)

    def test_redirect_to_expired_page(self):
        expected_template = 'common/public/expired_page.html'
        for status in [Scan.PENDING, Scan.FINISHED]:
            with self.subTest(status=status):
                self.scan.status = status
                self.scan.save()

                response = self.client.get(self.url)
                self.assertEqual(response.status_code, 200)
                self.assertTemplateUsed(response, expected_template)


class ScanConfirmViewTest(TestCase):
    """
    Test the `ScanConfirmView` view.
    """
    fixtures = ['app_permissions']

    def setUp(self):
        self.client = Client()
        self.url = reverse('scans:confirm')
        self.template = 'scans/public/approval_confirmation.html'

    def test_request_as_anonymous_user(self):
        """
        Test a request to the `ScanConfirmView` view from an
        anonymous user.
        """
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Succes met de scan')
        self.assertTemplateUsed(response, self.template)
