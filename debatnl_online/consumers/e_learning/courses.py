from debatnl_online.consumers.base import BaseConsumer


class CourseConsumer(BaseConsumer):
    def __init__(self):
        self.url = '{}/api/e-learning/courses'.format(self.base_url)
